<?php
namespace App\Http\Controllers;
use DB;
use Auth;
use Session;
use Mail;
use File;
use Socialite;
use bcrypt;
use Config;
use Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Helpers;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class TestimonialsController  extends Controller {
	 public function addtestimonial(Request $request){
		if ($request->isMethod('post')){
		$rules = array(
            'name'       => 'required',
            'image'      => 'required',
            'message'      => 'required',
		);
        $validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
            return Redirect::back()
                ->withErrors($validator)
				 ->withInput(Input::except('password'));
        } 
		else{
           $inputt = Input::all();
		   $input = $request->input();
		   unset($input['_token']);
		   $file = $inputt['image'];
		   if(!empty($inputt['image'])){
				if(!empty($file)){
				  $destinationPath = 'uploads/testimonials'; 
				  $fileName = 's2w-testimonial-'.rand(100,999);
				  $imageName = Helpers::imageUpload($file,$destinationPath,$fileName);
				  $input['image'] = $imageName;
				}
			}
			DB::table('testimonials')->insert($input);
			Session::flash('message', 'Successfully added testimonial!');
            return Redirect::back();
        }	  
		}
		return view('testimonials.addtestimonial');
	}
	public function edittestimonials($id,Request $request)
   {
	   $id = unserialize(base64_decode($id));
		$categories = DB::table('testimonials')->where('id',$id)->first();
		if ($request->isMethod('post')){
			$input = Input::all();
			$catname = $input['name'];
			$catdescription = $input['message'];
			$image = $categories->image;
			$filee = array_filter($input['image']);
			unset($input['image']);
			unset($input['_token']);
			if(!empty($filee)){
				 $destinationPath = 'uploads/testimonials';
				 if($image!=""){
					 File::delete($destinationPath.'/'.$image);
				 }
				if(!empty($filee)){
				  $fileName = rand(1000000, 99999999);
				  $imageName = Helpers::imageUpload($filee,$destinationPath,$fileName);
				  $input['image'] = $imageName;
				}
			 }
			 $rowCOllection = DB::table('testimonials')->where('id',$id)->update($input);
			 Session::flash('message', 'Successfully updated testimonials!');
             return Redirect::back();
			}
		if(!empty($categories)){
			return view('testimonials.edittestimonials')->with('categories', $categories);
		}
		else{
			return redirect()->action('TestimonialsController@index')->withErrors('Invalid Id Provided');
		}
   }
    public function index(){
		$categories = DB::table('testimonials')->get();
		return view('testimonials.index')->with('categories', $categories);;
	} 
	public function deletetestimonials($id){
	  $id = unserialize(base64_decode($id));
	  $categories = DB::table('testimonials')->where('id',$id)->first();
	  if(!empty($categories)){
		 $image = $categories->image;
		 $destinationPath = 'uploads/testimonials';
		 if($image!=""){
			File::delete($destinationPath.'/'.$image);
		 }
		 DB::table('testimonials')->where('id',$id)->delete();
		 Session::flash('message', 'Successfully deleted testimonials!');
         return Redirect::back();
	  }else{
		  return redirect()->action('TestimonialsController@index')->withErrors('Invalid Id Provided');
	  }
    }
}
?>