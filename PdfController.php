<?php namespace App\Http\Controllers;
	use DB;
	use Session;
	use bcrypt;
	use Config;
	use Redirect;
	use Helpers;
	use Hash;
	use PDF;
	use URL;
	use Mpdf;
	use Carbon\Carbon;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use View;
	use App\Http\Requests;
	use Illuminate\Support\Facades\Validator;
	use Illuminate\Support\Facades\Input;
	class PdfController extends Controller {
		public function getpdf($userid,$matchkey,$joinedleauge){
			$fndmatchdetails = DB::table('list_matches')->where('matchkey',$matchkey)->first();
			$findchallengedetail = DB::table('match_challenges')->where('matchkey',$matchkey)->first();
		   $findplayers = DB::table('match_players')->where('matchkey',$matchkey)->get();
		   
		}
		public function findallmatches(){
		    ini_set('memory_limit', '-1');
		    date_default_timezone_set("Asia/Kolkata");
			$currentdate = Carbon::now();
			$locktime = Carbon::now()->addMinutes(30);
			$matchtimings = date('Y-m-d H:i:s', strtotime('+30 minutes', strtotime(date('Y-m-d H:i:s'))));
			$findallmatch = DB::table('list_matches')->where('start_date','<=',$matchtimings)->where('launch_status','launched')->select('matchkey')->where('pdfstatus',0)->get();
// 			echo '<pre>'; print_r($findallmatch); die;
		    if(!empty($findallmatch)){
				foreach($findallmatch as $match){
					$this->createpdfnew($match->matchkey);
				}
			}
		}
		
		public function createpdfnew($matchdetails){
			date_default_timezone_set("Asia/Kolkata");
			$decodematchdetails = DB::table('list_matches')->where('matchkey',$matchdetails)->select('matchkey','title','short_name','start_date','format','name')->first();
			$matchkey = $decodematchdetails->matchkey;
			$findallchallenges = DB::table('match_challenges')->where('matchkey',$matchkey)->where('pdf_created',0)->where('status','!=','canceled')->select('*')->get();
			$findmatchplayers = DB::table('match_players')->where('matchkey',$matchkey)->select('name','playerid')->get();
			$chid = array();
			if(!empty($findallchallenges)){
				foreach($findallchallenges as $chllenge){
					$chid[] = $chllenge->id;
				}
			}
		    if(!empty($chid)){
				$findallusers = DB::table('joined_leauges')->whereIn('challengeid',$chid)->join('register_users','register_users.id','=','joined_leauges.userid')->join('join_teams','join_teams.id','=','joined_leauges.teamid')->select('register_users.team','register_users.email','join_teams.players','join_teams.captain','join_teams.vicecaptain','join_teams.teamnumber','joined_leauges.id as joinedid','pdfcreate','joined_leauges.challengeid')->get();
				$findallusers = json_decode(json_encode((array) $findallusers), true);
				$countusers = count($findallusers);
				if(!empty($findallusers)){
					foreach($findallchallenges as $chllenge){
						$filterusers=array();
						$filterBy = $chllenge->id;
						$filterusers = array_filter($findallusers, function ($var) use ($filterBy) {
						    return ($var['challengeid'] == $filterBy);
						});
						if(!empty($filterusers)){
						    $this->getPdfDownload($filterusers,$chllenge,$findmatchplayers,$decodematchdetails);
						}
						$challengepdf['pdf_created'] = 1;
						DB::table('match_challenges')->where('id',$chllenge->id)->update($challengepdf);
					}
				}
				$pdfcreatedata['pdfstatus'] = 1;
				DB::table('list_matches')->where('matchkey',$matchkey)->update($pdfcreatedata);
			}
		}
		public function getPdfDownload($findjoinedleauges,$findchallenge,$findmatchplayers,$findmatchdetails){
		    require_once("./mpdf/mpdf.php");
			$mpdf = new Mpdf();
		
			$mpdf->useSubstitutions = false;
			$mpdf->simpleTables = true;
			$mpdf->SetCompression(true);
			$content="";
			$content='<div class="col-md-12 col-sm-12" style="margin-top:20px;">
				
					<div class="col-md-12 col-sm-12 text-center" style="margin-top:20px;text-align:center">
						<div class="col-md-12 col-sm-12">
							<p> Fanadda Fantasy Cricket </p>
						</div>';
						$content.='<div class="col-md-12 col-sm-12">
							<p> <strong>Pdf Generated On: </strong>'.date('Y-m-d H:i:s a').'</p>
						</div>
					</div>
				</div>';
				$content.='<div class="col-md-12 col-sm-12" style="margin-top:20px;">
							<table style="width:100%" border="1">
							 <tr style="background:#3C7CC4;color:#fff;text-align:center">';
								$challengename = "";
									if($findchallenge->name==""){
										if($findchallenge->win_amount==0){
											$challengename = 'Net Practice';
										}else{
											$challengename = 'Win-'.$findchallenge->win_amount;
										}
									}else{
										$challengename = $findchallenge->name;
									}
								$content.='<th style="color:#fff !important;" colspan="'.(count($findmatchplayers)+1).'">'.$challengename.'( '.$findmatchdetails->short_name.' '.$findmatchdetails->format.' '.$findmatchdetails->start_date.')</th>
								
							  </tr>
							  <tr style="background:#ccc;color:#333;text-align:center">
								<th>Display User Name</th>';
								if(!empty($findmatchplayers)){
										$pn = 1;
										foreach($findmatchplayers as $player1){
											if($pn < 12) {
												//$content.='<th>'.ucwords($player1->name).'</th>';
												$content.='<th>Player '.$pn.'</th>';
											}
											$pn++;
									}
								}
				$content.='</tr>';
				if(!empty($findjoinedleauges)){
					foreach($findjoinedleauges as $joinleauge){
						
						$content.='<tr>
							<td style="text-align:center">';
							if($joinleauge['team']!=""){ 
								$content.=ucwords($joinleauge['team']).'<br> ( '.$joinleauge['teamnumber'].' )';
							} 
							else{
								 $content.= ucwords($joinleauge['email']).'<br> ( '.$joinleauge['teamnumber'].' )';
							}
							$content.='</td>';
							$jointeam = $joinleauge['players'];
							$explodeplayers = explode(',',$jointeam);
							foreach($findmatchplayers as $player2){
								
								
								if(in_array($player2->playerid,$explodeplayers)){
									$content.='<td class="text-center" style="text-align:center;">';
									$content.= $player2->name;
									
									if($player2->playerid==$joinleauge['vicecaptain']){
										$content.= '(VC)';
									}
									if($player2->playerid==$joinleauge['captain']){
										$content.= '(C)';
									}
									$content.='</td>';
								}
								
							} 
						  $content.='</tr>';
					}	
				}
				$content.='</table>
				</div>';
				//echo $content; exit;
				$mpdf->WriteHTML($content);
				$filename = $findchallenge->id;
				//$mpdf->Output();
				$mpdf->Output('pdffolders/join-leauges-'.$filename.'.pdf');
				return 'join-challenges-'.$filename.'.pdf';
		}

		public function getPdfDownloadBkp($findjoinedleauges,$findchallenge,$findmatchplayers,$findmatchdetails){
		    require_once("./mpdf/mpdf.php");
			$mpdf = new Mpdf();
		
			$mpdf->useSubstitutions = false;
			$mpdf->simpleTables = true;
			$mpdf->SetCompression(true);
			$content="";
			$content='<div class="col-md-12 col-sm-12" style="margin-top:20px;">
				
					<div class="col-md-12 col-sm-12 text-center" style="margin-top:20px;text-align:center">
						<div class="col-md-12 col-sm-12">
							<p> Fanadda Fantasy Cricket </p>
						</div>';
						$content.='<div class="col-md-12 col-sm-12">
							<p> <strong>Pdf Generated On: </strong>'.date('Y-m-d H:i:s a').'</p>
						</div>
					</div>
				</div>';
				$content.='<div class="col-md-12 col-sm-12" style="margin-top:20px;">
							<table style="width:100%" border="1">
							 <tr style="background:#3C7CC4;color:#fff;text-align:center">';
								$challengename = "";
									if($findchallenge->name==""){
										if($findchallenge->win_amount==0){
											$challengename = 'Net Practice';
										}else{
											$challengename = 'Win-'.$findchallenge->win_amount;
										}
									}else{
										$challengename = $findchallenge->name;
									}
								$content.='<th style="color:#fff !important;" colspan="'.(count($findmatchplayers)+1).'">'.$challengename.'( '.$findmatchdetails->short_name.' '.$findmatchdetails->format.' '.$findmatchdetails->start_date.')</th>
								
							  </tr>
							  <tr style="background:#ccc;color:#333;text-align:center">
								<th>Display User Name</th>';
								if(!empty($findmatchplayers)){
										foreach($findmatchplayers as $player1){
											$content.='<th>'.ucwords($player1->name).'</th>';
									}
								}
				$content.='</tr>';
				if(!empty($findjoinedleauges)){
					foreach($findjoinedleauges as $joinleauge){
						
						$content.='<tr>
							<td style="text-align:center">';
							if($joinleauge['team']!=""){ 
								$content.=ucwords($joinleauge['team']).'<br> ( '.$joinleauge['teamnumber'].' )';
							} 
							else{
								 $content.= ucwords($joinleauge['email']).'<br> ( '.$joinleauge['teamnumber'].' )';
							}
							$content.='</td>';
							$jointeam = $joinleauge['players'];
							$explodeplayers = explode(',',$jointeam);
							foreach($findmatchplayers as $player2){
								
								$content.='<td class="text-center" style="text-align:center;">';
								if(in_array($player2->playerid,$explodeplayers)){
									$content.= 'Y';
									
									if($player2->playerid==$joinleauge['vicecaptain']){
										$content.= '(VC)';
									}
									if($player2->playerid==$joinleauge['captain']){
										$content.= '(C)';
									}
								}
								$content.='</td>';
							} 
						  $content.='</tr>';
					}	
				}
				$content.='</table>
				</div>';
				echo $content; exit;
				$mpdf->WriteHTML($content);
				$filename = $findchallenge->id;
				//$mpdf->Output();
				//$mpdf->Output('pdffolders/join-leauges-'.$filename.'.pdf');
				return 'join-challenges-'.$filename.'.pdf';
		}
		public function downloadpdf1($userid,$matchkey,$challengeid,$joinid){
			 // $userid = $_GET['userid'];
			 // $matchkey = $_GET['matchkey'];
			 // $challengeid = $_GET['challengeid'];
			 $findmatchdetails = $matchdetails = DB::table('list_matches')->where('matchkey',$matchkey)->select('title','short_name','start_date','format','name')->first();
			 $findmatchplayers = DB::table('match_players')->where('matchkey',$matchkey)->select('name','playerid')->get();
			 $findjoinedleauges=array();
			 if(!empty($findmatchdetails)){
				 $findchallenge = DB::table('match_challenges')->where('id',$challengeid)->first();
				 if(!empty($findchallenge)){
					 $findjoinedleauges = DB::table('joined_leauges')->where('challengeid',$findchallenge->id)->join('register_users','register_users.id','=','joined_leauges.userid')->join('join_teams','join_teams.id','=','joined_leauges.teamid')->select('register_users.team','register_users.email','join_teams.*','joined_leauges.id as joinedid')->get();
				 }
				 $finduserdetails = DB::table('register_users')->where('id',$userid)->select('team','email','state','username')->first();
				 if(($findchallenge->confirmed_challenge==1) || ($findchallenge->maximum_user==$findchallenge->joinedusers)){
						//entry in notification table//
						$notificationdata['userid'] = $userid;
						$notificationdata['title'] = 'Challenge entry fees Rs.'.$findchallenge->entryfee;
						DB::table('notifications')->insert($notificationdata);
						$titleget = 'Confirmation - joined challenge!';
						Helpers::sendnotification($titleget,$notificationdata['title'],'',$userid);
						//end entry in notification table//
						 //start send mail section//
							$email = $finduserdetails->email;
							$emailsubject = 'Confirmation - You have joined a challenge !';
							$content='<p><strong>Hello '.ucwords($finduserdetails->username).'</strong></p>';
							$content.='<p>You have successfully joined the challenge with an entry fee of Rs. '.$findchallenge->entryfee.' for the '.date('d/M/Y h:i:s a',strtotime($matchdetails->start_date)).' '.ucwords($matchdetails->name).' match. Watch the action unfold on your Fanadda dashboard as your team fight for top spot !!</p>';
							$msg = Helpers::mailheader();
							$msg.= Helpers::mailbody($content);
							$msg.= Helpers::mailfooter();
							// Helpers::mailsentFormat($email,$emailsubject,$msg);
						//end send mail section//
				 }
				 // return view('pdf.downloadpdf',compact('findmatchdetails','findmatchplayers','findchallenge','findjoinedleauges','finduserdetails'));
				 // $pdf = PDF::loadView('pdf.downloadpdf',compact('findmatchdetails','findmatchplayers','findchallenge','findjoinedleauges','finduserdetails'));
				 // $pdf->setPaper('a1', 'landscape');
				 // return $pdf->download($findchallenge->id.'.pdf');
				 require_once("./mpdf/mpdf.php");
				 $mpdf = new Mpdf();
				 $content='<div class="col-md-12 col-sm-12" style="margin-top:20px;">
					<div class="col-md-12 col-sm-12 text-center" style="text-align:center">
						<img src="'.URL::asset('images/logo.png').'">
						
					</div>
					<div class="col-md-12 col-sm-12 text-center" style="margin-top:20px;text-align:center">
						<div class="col-md-12 col-sm-12">
							<p> Fanadda Fantasy Cricket </p>
						</div>';
						// <div class="col-md-12 col-sm-12">
							// <p> <strong>Email Address: </strong>'.$finduserdetails->email.'</p>
						// </div>
						$content.='<div class="col-md-12 col-sm-12">
							<p> <strong>Pdf Generated On: </strong>'.date('Y-m-d H:i:s a').'</p>
						</div>
					</div>
				</div>';
				$content.='<div class="col-md-12 col-sm-12" style="margin-top:20px;">
							<table style="width:100%" border="1">
							 <tr style="background:#3C7CC4;color:#fff;text-align:center">';
								$challengename = "";
									if($findchallenge->name==""){
										if($findchallenge->win_amount==0){
											$challengename = 'Net Practice';
										}else{
											$challengename = 'Win-'.$findchallenge->win_amount;
										}
									}else{
										$challengename = $findchallenge->name;
									}
								$content.='<th style="color:#fff !important;" colspan="'.(count($findmatchplayers)+1).'">'.$challengename.'( '.$findmatchdetails->short_name.' '.$findmatchdetails->format.' '.$findmatchdetails->start_date.')</th>
								
							  </tr>
							  <tr style="background:#ccc;color:#333;text-align:center">
								<th>Display User Name</th>';
								if(!empty($findmatchplayers)){
										foreach($findmatchplayers as $player1){
											$content.='<th>'.ucwords($player1->name).'</th>';
									}
								}
				$content.='</tr>';
				if(!empty($findjoinedleauges)){
					foreach($findjoinedleauges as $joinleauge){
						$content.='<tr>
							<td style="text-align:center">';
							if($joinleauge->team!=""){ 
								$content.=ucwords($joinleauge->team).'<br> ( '.$joinleauge->teamnumber.' )';
							} 
							else{
								 $content.= ucwords($joinleauge->email).'<br> ( '.$joinleauge->teamnumber.' )';
							}
							$content.='</td>';
							$jointeam = $joinleauge->players;
							$explodeplayers = explode(',',$jointeam);
							foreach($findmatchplayers as $player2){
								$content.='<td class="text-center" style="text-align:center;">';
								if(in_array($player2->playerid,$explodeplayers)){
									$content.= '<img src="'.URL::asset('images/tick_green.png').'" style="width:30px;text-align:center">';
								}
								$content.='</td>';
							} 
						  $content.='</tr>';
					}	
				}
				$content.='</table>
				</div>';
				 $mpdf->WriteHTML($content);
				 $filename = $findchallenge->id;
				 $mpdf->Output('pdffolders/join-challenges-'.$filename.'-'.$userid.'-'.$joinid.'.pdf', 'F');
				 return 'join-challenges-'.$filename.'-'.$userid.'-'.$joinid.'.pdf';
				 // file_put_contents('pdffolders/'.$filename.'/'.$findchallenge->id.".pdf", $mpdf->Output());
				 // $mpdf->Output($findchallenge->id.'.pdf', 'D');
			 }
			
			
		}
		public function challengeinvoice(){
			$query = DB::table('transactions');
			$getlist = array();
			if(isset($_GET['start_date'])){
				$start_date = $_GET['start_date'];
				$start_date = date('Y-m-d H:i:s', strtotime('-1 hours', strtotime($_GET['start_date'])));
				if($start_date!=""){
					$query->whereDate('transactions.created', '>=',date('Y-m-d h:i:s',strtotime($start_date)));
				}
			}
			if(isset($_GET['end_date'])){
				$end_date = $_GET['end_date'];
				if($end_date!=""){
					$query->whereDate('transactions.created', '<=',date('Y-m-d h:i:s',strtotime($end_date)));
				}
			}
			if(isset($_GET['userid'])){
				$userid = $_GET['userid'];
				if($userid!=""){
					$query->where('transactions.userid',$userid);
				}
			}
			$query->where('transactions.challengeid','<>',0);
			if((isset($_GET['start_date'])) || (isset($_GET['end_date'])) || (isset($_GET['userid']))){
				$getlist = $query->where(function ($query){
							$query->where('list_matches.launch_status','launched')->where('list_matches.final_status','winnerdeclared');
						})->where(function($query) {
							$query->where('match_challenges.maximum_user','=',DB::raw('match_challenges.joinedusers'))->orWhere('match_challenges.confirmed_challenge',1);	
						})->join('joined_leauges','joined_leauges.transaction_id','=','transactions.transaction_id')->join('match_challenges','match_challenges.id','=','transactions.challengeid')->join('list_matches','list_matches.matchkey','=','match_challenges.matchkey')->join('register_users','register_users.id','=','transactions.userid')->orderBY('transactions.id','DESC')->select('match_challenges.name as name','match_challenges.entryfee as entryfee','match_challenges.matchkey','match_challenges.win_amount','register_users.username as username','register_users.email as useremail','transactions.created as createddate','joined_leauges.id as jid','transactions.userid as userid')->get();

			}
			return view('pdf.challengeinvoice',compact('getlist'));
		}
		public function downloadinvoice($id){
			$findfulldetails = DB::table('joined_leauges')->where('joined_leauges.id',$id)->join('match_challenges','match_challenges.id','=','joined_leauges.challengeid')->join('register_users','register_users.id','=','joined_leauges.userid')->select('register_users.*','match_challenges.*','joined_leauges.*','joined_leauges.created_at as joinedate')->first();
			
			require_once("./mpdf/mpdf.php");
			// require_once __DIR__ . '/mpdf/mpdf.php';
			$mpdf = new Mpdf();
			$win_amount = $findfulldetails->win_amount;
			$entryfee = $findfulldetails->entryfee;
			$maximum_user = $findfulldetails->maximum_user;
			$getpoolprize = round($findfulldetails->win_amount/$findfulldetails->maximum_user,2);
			$pfee = ($findfulldetails->entryfee-$getpoolprize)/1.18;
			$platformfee = round($pfee,2);
			$igst="";
			$cgst="";
			$sgst="";
			if($findfulldetails->state!='haryana' && $findfulldetails->state!='Haryana'){
				$igst = round(($platformfee*18)/100,2);
			}
			if($findfulldetails->state=='haryana' || $findfulldetails->state=='Haryana'){
				$cgst = round(($platformfee*9)/100,2);
			}
			if($findfulldetails->state=='haryana' || $findfulldetails->state=='Haryana'){
				$sgst = round(($platformfee*9)/100,2);
			}
			$amountinwords = $this->numberTowords($findfulldetails->entryfee);
			$mpdf->WriteHTML('<div style="width:100%; height:auto; margin-right:auto; margin-left:auto; background:#fff;">
				<div class="col-md-12 col-sm-12 text-center" style="text-align:center; margin-right:auto; margin-left:auto; ">
						<img src="'.URL::asset('images/logo.png').'">
				</div>
									<div style="width:100%;float:left; text-align:center;">
										<h3 style="font-size:20px; font-weight:500;">Tax Invoice</h3>
									</div>
								<div style="width:100%; float:left;">
									<div style="width:50%; float:left; margin-top:10px;">
										<h3 style="font-size:14px; margin:0px;">'.ucwords($findfulldetails->username).'</h3>
										<p style="font-size:12px; margin: 5px 0px 0px;">EmailID: '.$findfulldetails->email.'</p>
										<p style="font-size:12px; margin: 5px 0px 0px;">State:'.ucwords($findfulldetails->state).'</p>
									</div>
									
									<div style="width:50%; float:left;text-align:right;">
										<h3 style="font-size:14px; margin:0px;text-align:right">Fanadda Fantasy Cricket</h3>
										<p style="font-size:12px; margin: 5px 0px 0px;text-align:right">Invoice No:'.$findfulldetails->transaction_id.'</p>
										<p style="font-size:12px; margin: 5px 0px 0px;text-align:right">Date:'.$findfulldetails->joinedate.'</p>
										<p style="font-size:12px; margin: 5px 0px 0px;text-align:right">GSTN:06AAXCS9514P1Z9</p>
									</div>
								</div>
								<div style="width:100%; height:auto; float:left; margin-top: 30px;">
								<table style="width:100%;">
								<thead style="float:left;width:100%">
					   <tr style="float:left;width:100%;">
							<th style="font-size:11px;width:15%;text-align:center">Description</th>
							<th style="font-size:11px;width:15%;text-align:center">Prize Pool <br> (Actionable <br> Claim)</th>
							<th style="font-size:11px;width:15%;text-align:center">Platform <br> Fees</th>
							<th colspan="3" style="font-size:11px;width:40%;text-align:center;border-bottom:1px solid #333;">GST Tax Detail</th>
							<th style="font-size:11px;width:15%;text-align:center">Total Amount</th>
					   </tr>
					   <tr style="margin-bottom:20px;float:left;width:100%;">
							<td></td>
							<td></td>
							<td></td>
							<td style="font-size:11px;text-align:center">CGST @9%</td>
							<td style="font-size:11px;text-align:center">SGST @9%</td>
							<td style="font-size:11px;text-align:center">IGST @18%</td>
							<td></td>
					   </tr>
					   </thead>
					   <tbody style="border:1px solid #000000;float:left;width:100%;">
						<tr style="padding-top:30px;border-top:1px solid #333;border-bottom:1px solid #333;float:left;width:100%;" border=1>
							<td style="font-size:11px;text-align:center"></td>
							<td style="font-size:11px;text-align:center"></td>
							<td style="font-size:11px;text-align:center"></td>
							<td style="font-size:11px;text-align:center"></td>
							<td style="font-size:11px;text-align:center"></td>
							<td style="font-size:11px;text-align:center"></td>
							<td style="font-size:11px;text-align:center"></td>
					   </tr>
					   <tr style="padding-top:30px;border-top:1px solid #333;border-bottom:1px solid #333;float:left;width:100%;" border=1>
							<td style="font-size:11px;text-align:center"></td>
							<td style="font-size:11px;text-align:center"></td>
							<td style="font-size:11px;text-align:center"></td>
							<td style="font-size:11px;text-align:center"></td>
							<td style="font-size:11px;text-align:center"></td>
							<td style="font-size:11px;text-align:center"></td>
							<td style="font-size:11px;text-align:center"></td>
					   </tr>
					 <tr style="padding-top:30px;border-top:1px solid #333;border-bottom:1px solid #333;float:left;width:100%;" border=1>
							<td style="font-size:11px;text-align:center">S2W-1079047546</td>
							<td  style="font-size:11px;text-align:center">Rs.'.$getpoolprize.'</td>
							<td style="font-size:11px;text-align:center">Rs.'.$platformfee.'</td>
							<td style="font-size:11px;text-align:center">'.$cgst.'</td>
							<td style="font-size:11px;text-align:center">'.$sgst.'</td>
							<td style="font-size:11px;text-align:center">'.$igst.'</td>
							<td style="font-size:11px;text-align:center">'.$findfulldetails->entryfee.'</td>
					   </tr>
					   </tbody>
				
				</table>
								
							<div style="width:100%; height:auto; float:left;margin-top:20px;">
								<div style="width:400px; height:100px; float:left;">
									<h3 style="margin-bottom: 0px; font-size:13px; font-weight:500;"><strong>Amount In Words:Rs</strong> '.ucwords($amountinwords).'</h3>
									<h4 style="margin-bottom: 0px; font-size:13px;    margin-top: 5px"><strong>Terms & Conditions -</strong></h4>
									<p style="    margin-top: 5px;font-size:11px;">Refer to <a href=" " target="_blank"></a></p>
								</div>
							</div>
							
							<div style="width:100%; height:auto; float:left; margin-top:30px; border-bottom:1px solid #ccc; margin-bottom:100px; padding-bottom:30px;">
								<h5 style=" margin-bottom:0px; margin-top:5px;text-align:center;font-weight: 500;"><strong>Registered Office:</strong> C121 Ground Floor,Sushant Shopping Arcade,Sushant Lok 1,
									<br>Gurgaon ,Haryana 122002.
								</h5>
								<h5 style=" margin-bottom:0px; margin-top:5px;text-align:center;font-weight:500;font-size:13px;"><strong>Email Id: Contact@fanadda.com</strong></h5>
								<h5 style=" margin-bottom:0px; margin-top:5px;text-align:center;font-weight:500;font-size:13px;"><strong>Website:</strong> https://fanadda.com</h5>
								<h5 style=" margin-bottom:0px;  margin-top:5px; text-align:center;font-weight:500;font-size:13px;"><strong>CIN: U74999HR2016PTC066835</strong></h5>
								<h5 style=" margin-bottom:0px; margin-top:5px;text-align:center; font-weight:500;font-size:13px;">*Service Category-Online Information and Database Access pr Retrieval Services.SAC Code: 00440153 </h5>
								<h5 style=" margin-bottom:0px;  margin-top:5px; text-align:center;font-weight:500;font-size:13px;">* This is a computer generated invoice. No Signature required</h5>
								</div>
								<div style="width:25%; height:auto; float:left; margin-top:20px;"></div>
							</div>
							
						</div>');
						$mpdf->Output($findfulldetails->userid.'_'.$findfulldetails->transaction_id.'_'.date('Y-M-d',strtotime($findfulldetails->joinedate)).'_'.date('h-i-a',strtotime($findfulldetails->joinedate)).'.pdf', 'D');
						
						
		
			// $joinleuges = DB::table('joined_leauges')->where('joined_leauges.id',$id)->join('register_users','register_users.id','=','joined_leauges.userid')->join('match_challenges','match_challenges.id','=','joined_leauges.challengeid')->select('register_users.username','register_users.state','register_users.email','joined_leauges.*')->first();
			// if(!empty($joinleuges)){
				 // $pdf = PDF::loadView('pdf.downloadinvoice',compact('joinleuges'));
				 // $pdf->setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
				 // $pdf->setPaper('a1', 'landscape');
				 // return $pdf->stream('invoice.pdf');
			// }
			
		}
		public function  numberTowords($num)
	{ 
	$ones = array( 
	1 => "one", 
	2 => "two", 
	3 => "three", 
	4 => "four", 
	5 => "five", 
	6 => "six", 
	7 => "seven", 
	8 => "eight", 
	9 => "nine", 
	10 => "ten", 
	11 => "eleven", 
	12 => "twelve", 
	13 => "thirteen", 
	14 => "fourteen", 
	15 => "fifteen", 
	16 => "sixteen", 
	17 => "seventeen", 
	18 => "eighteen", 
	19 => "nineteen" 
	); 
	$tens = array( 
	1 => "ten",
	2 => "twenty", 
	3 => "thirty", 
	4 => "forty", 
	5 => "fifty", 
	6 => "sixty", 
	7 => "seventy", 
	8 => "eighty", 
	9 => "ninety" 
	); 
	$hundreds = array( 
	"hundred", 
	"thousand", 
	"million", 
	"billion", 
	"trillion", 
	"quadrillion" 
	); //limit t quadrillion 
	$num = number_format($num,2,".",","); 
	$num_arr = explode(".",$num); 
	$wholenum = $num_arr[0]; 
	$decnum = $num_arr[1]; 
	$whole_arr = array_reverse(explode(",",$wholenum)); 
	krsort($whole_arr); 
	$rettxt = ""; 
	foreach($whole_arr as $key => $i){ 
	if($i < 20){ 
	$rettxt .= $ones[$i]; 
	}elseif($i < 100){ 
	$rettxt .= $tens[substr($i,0,1)]; 
	$rettxt .= " ".$ones[substr($i,1,1)]; 
	}else{ 
	$rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
	$rettxt .= " ".$tens[substr($i,1,1)]; 
	$rettxt .= " ".$ones[substr($i,2,1)]; 
	} 
	if($key > 0){ 
	$rettxt .= " ".$hundreds[$key]." "; 
	} 
	} 
	if($decnum > 0){ 
	$rettxt .= " and "; 
	if($decnum < 20){ 
	$rettxt .= $ones[$decnum]; 
	}elseif($decnum < 100){ 
	$rettxt .= $tens[substr($decnum,0,1)]; 
	$rettxt .= " ".$ones[substr($decnum,1,1)]; 
	} 
	} 
	return $rettxt; 
	} 
	}
	?>