<?php
	namespace App\Http\Controllers;
	use DB;
	use Session;
	use bcrypt;
	use Config;
	use Redirect;
	use Helpers;
	use File;
	use Hash;
	use Mail;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use App\Http\Requests;
	use Carbon\Carbon;
	use Illuminate\Support\Facades\Validator;
	use Illuminate\Support\Facades\Input;
	use App\Http\Controllers\CricketapiController;
	class UpdateapiController extends Controller {
		public function updatestatus(){
			$currentdate = Carbon::now();
			$findallmatches = DB::table('list_matches')->where('status','!=','completed')->where('launch_status','launched')->whereDate('start_date','<=',$currentdate)->select('id','matchkey')->get();
			
			if(!empty($findallmatches)){
				foreach($findallmatches as $getli){
					$findsquaddetials = CricketapiController::getmatchdetails($getli->matchkey);
					
					if(!empty($findsquaddetials)){
						$squadteam1players = $findsquaddetials['data']['card']['teams']['a']['match']['players'];
						$squadteam2players = $findsquaddetials['data']['card']['teams']['b']['match']['players'];
						if((!empty($squadteam1players)) && (!empty($squadteam2players))){
							$matchadata['squadstatus'] = 'yes';
						}
						$matchadata['status'] = $findsquaddetials['data']['card']['status'];
						if($matchadata['status']=='completed'){
							$winner_team = $findsquaddetials['data']['card']['winner_team'];
							if($winner_team!=""){
								$key = $winner_team;
								$winnerteamname = $findsquaddetials['data']['card']['teams'][$key]['key'];
								$matchadata['winner_team'] = $winnerteamname;
								$findmanmatch = $findsquaddetials['data']['card']['man_of_match'];
								if($findmanmatch!=""){
									$manmatch['man_of_match'] = 1;
									DB::table('result_matches')->where('player_key',$findmanmatch)->where('match_key',$getli->matchkey)->update($manmatch);
								}
							}
						}
						DB::table('list_matches')->where('id',$getli->id)->update($matchadata);
					}
				}
			}
		}
		
		public function updatemarathons(){
			$locktime = Carbon::now()->addHours(1);
			$findmatchesmarathons = DB::table('list_matches')->where('list_matches.start_date','>',$locktime)->join('series','series.id','=','list_matches.series')->join('marathon','marathon.series','=','list_matches.series')->get();
			if(!empty($findmatchesmarathons)){
				
			}
		}
		
	    public function updatematchstatus(){
	        $getdayslist = CricketapiController::recentseasons();
			$getseasons = $getdayslist['data'];
			foreach($getseasons as $seasons){
				$seasonmatches = CricketapiController::seasonmatches($seasons['key']);
				$getmatch = $seasonmatches['data']['season']['matches'];
				foreach($getmatch as $getli){
					$findmatchexist = DB::table('list_matches')->where('matchkey',$getli['key'])->first();
					$startdatematch = date('Y-m-d h:i:s',strtotime($getli['start_date']['iso']));
					if($startdatematch>=date('Y-m-d h:i:s')){
						if(empty($findmatchexist)){
							$findsquaddetials = CricketapiController::getmatchdetails($getli['key']);
							if(!empty($findsquaddetials)){
								$squadteam1players = $findsquaddetials['data']['card']['teams']['a']['match']['players'];
								$squadteam2players = $findsquaddetials['data']['card']['teams']['b']['match']['players'];
								if((!empty($squadteam1players)) && (!empty($squadteam2players))){
									$matchadata['squadstatus'] = 'yes';
								}
							}
							// team details update//
							$team1key = $getli['teams']['a']['key'];
							$team2key = $getli['teams']['b']['key'];
							// insert team 1//
							$findteam1 = DB::table('teams')->where('team_key',$team1key)->select('id')->first();
							if(empty($findteam1)){
								$data['team_key'] = $team1key;
								$data['team'] = $getli['teams']['a']['name'];
								$team1id = DB::table('teams')->insertGetId($data);
							}else{
								$team1id = $findteam1->id;
							}
							// insert team 2//
							$findteam2 = DB::table('teams')->where('team_key',$team2key)->select('id')->first();
							if(empty($findteam2)){
								$data1['team_key'] = $team2key;
								$data1['team'] = $getli['teams']['b']['name'];
								$team2id = DB::table('teams')->insertGetId($data1);
							}
							else{
								$team2id = $findteam2->id;
							}
							// insert match data//
							$matchadata['status'] = $getli['status'];
							$matchadata['name'] = $getli['name'];
							$matchadata['short_name'] = $getli['short_name'];
							$matchadata['season'] = $getli['season']['name'];
							$matchadata['title'] = $getli['title'];
							$matchadata['format'] = $getli['format'];
							$matchadata['team1'] = $team1id;
							$matchadata['team2'] = $team2id;
							$matchadata['team1display'] = $getli['teams']['a']['key'];
							$matchadata['team2display'] = $getli['teams']['b']['key'];
							$matchadata['matchkey'] = $getli['key'];
							$matchadata['start_date'] = date('Y-m-d h:i:s',strtotime($getli['start_date']['iso']));
							DB::table('list_matches')->insert($matchadata);
						}
						else{
							$findsquaddetials = CricketapiController::getmatchdetails($getli['key']);
							if(!empty($findsquaddetials)){
								$squadteam1players = $findsquaddetials['data']['card']['teams']['a']['match']['players'];
								$squadteam2players = $findsquaddetials['data']['card']['teams']['b']['match']['players'];
								if((!empty($squadteam1players)) && (!empty($squadteam2players))){
									$matchadata['squadstatus'] = 'yes';
								}
							}
							$matchadata['status'] = $getli['status'];
							$matchadata['start_date'] = date('Y-m-d h:i:s',strtotime($getli['start_date']['iso']));
							DB::table('list_matches')->where('id',$findmatchexist->id)->update($matchadata);
						}
					}
				}
			}
	    }
		// public function updatecompletematchstatus(){
			// $findlistmatches = DB::table('list_matches')->where('status','completed')->where('launch_status','launched')
		// }
	     public function recentseries($key){
		    $getdayslist = CricketapiController::getmatchdetails($key);
	        echo "<pre>";
	       print_r($getdayslist);die;
	    }
	    public function allmatches(){
	        $getdayslist = CricketapiController::getscedulematches();
	        echo "<pre>";
	        print_r($getdayslist);die;
	    }
		public function playerpoints(){
			$findallplayers = DB::table('players')->select('player_key','id')->get();
			if(!empty($findallplayers)){
				foreach($findallplayers as $playr){
					$getid = $playr->id;
					// $getid = 349;
					$findallmatchplayers = array();
					$findallmatchplayers = DB::table('result_matches')->where('player_id',$getid)->get();
					
					$points = 0;
					if(!empty($findallmatchplayers)){
						foreach($findallmatchplayers as $play){
							$points+=$play->total_points;
						}
					}
					$data['points'] = $points;
					DB::table('players')->where('id',$getid)->update($data);
				}
			}
		}
		
	}
	?>