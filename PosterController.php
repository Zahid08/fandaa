<?php 
	namespace App\Http\Controllers;
	use DB;
	use Session;
	use bcrypt;
	use Config;
	use Redirect;
	use Helpers;
	use Hash;
	use PDF;
	use URL;
	use Mpdf;
	use Carbon\Carbon;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use View;
	use File;
	use App\Http\Requests;
	use Illuminate\Support\Facades\Validator;
	use Illuminate\Support\Facades\Input;
	class PosterController extends Controller {
		public function addposter(Request $request){
			date_default_timezone_set("Asia/Kolkata");
			if ($request->isMethod('post')){
				$inputt = Input::all();
				$file = $inputt['image'];
				if(!empty($inputt['image'])){
					if(!empty($file)){
					   $destinationPath = 'uploads/posters'; 
					   $fileName = 'poster-image-'.rand(1000,9999);
					   $imageName = Helpers::imageUpload($file,$destinationPath,$fileName);
					   $input['image'] = $imageName;
					   unset($input['_token']);
					   $packageid = DB::table('posters')->insertGetId($input);
					   Session::flash('message', 'Successfully added poster!');
					   return Redirect::back();
					}
				}
			}
			return view('posters.addposter');
			
		}
		public function editposter(Request $request, $id){
			date_default_timezone_set("Asia/Kolkata");
			$findimages = DB::table('posters')->where('id',$id)->first();
			if(!empty($findimages)){
				if ($request->isMethod('post')){
					$inputt = Input::all();
					
					$file = $inputt['image'];
					if(!empty($inputt['image'])){
						if(!empty($file)){
						   $findimage = DB::table('posters')->where('id',$id)->first();
						   $fimage = $findimage->image;
						   $destinationPath = 'uploads/posters'; 
						   if($fimage!=""){
								File::delete($destinationPath.'/'.$fimage);
						   }
						   $fileName = 'poster-image-'.rand(1000,9999);
						   $imageName = Helpers::imageUpload($file,$destinationPath,$fileName);
						   $input['image'] = $imageName;
						   unset($input['_token']);
						   $packageid = DB::table('posters')->where('id',$id)->update($input);
						   Session::flash('message', 'Successfully update poster!');
						   return Redirect::back();
						}
					}
				}
				return view('posters.editposter',compact('findimages'));
			}else{
				Session::flash('message', 'Invalid Id');
			    return Redirect::back();
			}
			
		}
		public function viewposters(){
			$findallposters = DB::table('posters')->get();
			return view('posters.viewposter',compact('findallposters'));
		}
		public function deleteposter($id){
			$findimages = DB::table('posters')->where('id',$id)->first();
			$fimage = $findimages->image;
		   $destinationPath = 'uploads/posters'; 
		   if($fimage!=""){
				File::delete($destinationPath.'/'.$fimage);
		   }
		   DB::table('posters')->where('id',$id)->delete();
			Session::flash('message', 'Successfully deleted poster!');
						   return Redirect::back();
		}
	}
	?>