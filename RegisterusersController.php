<?php
namespace App\Http\Controllers;
use DB;
use Auth;
use Session;
use bcrypt;
use Config;
use Redirect;
use Helpers;
use File;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\CricketapiController;
use App\Http\Controllers\ApiController;
class RegisterusersController extends Controller {
    public function getajaxusers(Request $request){
        $gettypevalue="";
		if(isset($_POST['gettypevalue'])){
            $gettypevalue = $_POST['gettypevalue'];
            $getusers = explode(',',$_POST['userspresent']);
        }
		$qy = DB::table('register_users');
		$findusers = $qy->whereNOTIn('id',$getusers)->where(function ($query) use ($gettypevalue) {
						$query->where('username','LIKE','%'.$gettypevalue.'%')->orwhere('team','LIKE','%'.$gettypevalue.'%')->orwhere('email','LIKE','%'.$gettypevalue.'%');
					})->get();
		$option="";
		$Json=array();
		$i=0;
		if(!empty($findusers)){
			foreach($findusers as $user){
				$showname = $user->username.'('.$user->team.')'.' | '.$user->email;
				$option.='<li style="cursor:pointer;" onclick="set_item('.$user->id.')" id="userid'.$user->id.'">'.$showname.'</li>';
				$Json[$i]['text'] = $user->username;
				$Json[$i]['value'] = $user->id;
			 $i++;
			}
		}
	echo $option;die;
    }
	public function updatepanurls(){
		$findallpandetails = DB::table('pan_cards')->select('image','id')->get();
		if(!empty($findallpandetails)){
			foreach($findallpandetails as $pandeta){
				$dataupdate = array();
				if($pandeta->image!=""){
					$dataupdate['image'] = str_replace('http://', 'https://', $pandeta->image);
					DB::table('pan_cards')->where('id',$pandeta->id)->update($dataupdate);
				}
			}
		}
		$findallbankdetails = DB::table('banks')->select('image','id')->get();
		if(!empty($findallbankdetails)){
			foreach($findallbankdetails as $bankdata){
				$datanak = array();
				if($bankdata->image!=""){
					$datanak['image'] = str_replace('http://', 'https://', $bankdata->image);
					DB::table('banks')->where('id',$bankdata->id)->update($datanak);
				}
			}
		}
		$findalluserdetails = DB::table('register_users')->select('image','id')->get();
		if(!empty($findalluserdetails)){
			foreach($findalluserdetails as $userdata){
				$userdataupdate = array();
				if($userdata->image!=""){
					$userdataupdate['image'] = str_replace('http://', 'https://', $userdata->image);
					DB::table('register_users')->where('id',$userdata->id)->update($userdataupdate);
				}
			}
		}
	}
	
	
	
    public function viewregisterusers(){
		
		$query = DB::table('register_users');
		if(request()->has('name')){
			$name=request('name');
			if($name!=""){
				$query->where('team', 'LIKE', '%'.$name.'%');
			}
		}
		if(request()->has('email')){
			$email=request('email');
			if($email!=""){
				$query->where('email', 'LIKE', '%'.$email.'%');
			}
		}
		if(request()->has('mobile')){
			$mobile=request('mobile');
			if($mobile!=""){
				$query->where('mobile', 'LIKE', '%'.$mobile.'%');
			}
		}
		if(request()->has('status')){
			$status=request('status');
			if($status!=""){
				$query->where('activation_status',$status);
			}
		}
		if(request()->has('userid')){
			$userid=request('userid');
			if($userid!=""){
				$query->where('id',$userid);
			}
		}
		if(request()->has('refercode')){
			$refercode=request('refercode');
			if($refercode!=""){
				$query->where('refercode', 'LIKE', '%'.$refercode.'%');
			}
		}
		$query->where('role', '<', '4');
		$getlist = $query->orderBY('id','DESC')->paginate(20);
		return view('registerusers.viewregisterusers')->with('allplayers', $getlist);
    }

	public function userswallet(){
		$query = DB::table('register_users')->where('role',0);
		if(request()->has('name')){
			$name=request('name');
			if($name!=""){
				$query->where('username', 'LIKE', '%'.$name.'%');
			}
		}
		if(request()->has('email')){
			$email=request('email');
			if($email!=""){
				$query->where('email', 'LIKE', '%'.$email.'%');
			}
		}
		if(request()->has('id')){
			$id=request('id');
			if($id!=""){
				$query->where('register_users.id',$id);
			}
		}
		$getlist = $query->join('user_balances','user_balances.user_id','=','register_users.id')->orderBY('id','DESC')->select('register_users.username','register_users.email','register_users.team','user_balances.*')->paginate(20);
		$totals = DB::table('user_balances')->select(DB::raw('SUM(winning) as total_winning, SUM(balance) as total_cash, SUM(bonus) as total_bonus'))->where('refer_id','>',-1)->first();
		//print_r($totals); exit;
		return view('registerusers.userswallet')->with('allplayers', $getlist)->with('balance', $totals);
	}
	
	public function downloadusers(){
		$output1 = "";
		$output1 .='"User Id",';
		$output1 .='"Team name",';
		$output1 .='"Email Id",';
		$output1 .='"User Name",';
		$output1 .='"DOB",';
		$output1 .='"Mobile num",';
		$output1 .='"Gender",';
		$output1 .='"State",';
		$output1 .='"Referal Code",';
		$output1 .='"PAN Uploaded",';
		$output1 .='"Bank Uploaded",';
		$output1 .='"PAN Number",';
		$output1 .='"Bank A/C Num",';
		$output1 .='"Bank Name",';
		$output1 .='"Bank Branch",';
		$output1 .="\n";
		$query = DB::table('register_users');
		if(request()->has('name')){
			$name=request('name');
			if($name!=""){
				$query->where('username', 'LIKE', '%'.$name.'%');
			}
		}
		if(request()->has('email')){
			$email=request('email');
			if($email!=""){
				$query->where('email', 'LIKE', '%'.$email.'%');
			}
		}
		$getlist = $query->orderBY('username','ASC')->get();
		if(!empty($getlist)){
			foreach($getlist as $get){
				$output1 .='"'.$get->id.'",';
				$output1 .='"'.$get->team.'",';
				$output1 .='"'.$get->email.'",';
				$output1 .='"'.$get->username.'",';
				$output1 .='"'.$get->dob.'",';
				$output1 .='"'.$get->mobile.'",';
				$output1 .='"'.$get->gender.'",';
				$output1 .='"'.$get->state.'",';
				$output1 .='"'.$get->refercode.'",';
				if($get->pan_verify==1){
					$output1 .='"Yes",';
				}else{
					$output1 .='"No",';
				}
				if($get->bank_verify==1){
					$output1 .='"Yes",';
				}else{
					$output1 .='"No",';
				}
				if($get->pan_verify==1){
					$findpandetails = DB::table('pan_cards')->where('userid',$get->id)->first();
					if(!empty($findpandetails)){
						$output1 .='"'.$findpandetails->pan_number.'",';
					}else{
						$output1 .='"",';
					}
				}else{
					$output1 .='"",';
				}
				if($get->bank_verify==1){
					$findbankdetails = DB::table('banks')->where('userid',$get->id)->first();
					if(!empty($findbankdetails)){
						$output1 .='"'.$findbankdetails->accno.'",';
						$output1 .='"'.$findbankdetails->bankname.'",';
						$output1 .='"'.$findbankdetails->bankbranch.'",';
					}else{
						$output1 .='"",';
						$output1 .='"",';
						$output1 .='"",';
					}
				}else{
					$output1 .='"",';
				}
				$output1 .="\n";
			}
		}
		$filename =  "Details-userwallet.csv";
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $output1;
		exit;
	}
	public function verifybankaccount(){
		$query =DB::table('banks')->join('register_users','register_users.id','=','banks.userid')->where('banks.status','0')->where('banks.hold','0')->where('register_users.mobile_verify','1')->where('register_users.email_verify','1')->select('register_users.email','register_users.username','register_users.mobile_verify','register_users.email_verify','register_users.pan_verify','banks.*')->orderBY('banks.id');


		$query->where(function($q){
			$q->where('register_users.pan_verify','=','0')
			->orWhere('register_users.pan_verify','=','1');
			});

		// print_r($query->toSql());


		if(request()->has('name')){
			$name=request('name');
			if($name!=""){
				$query->where('username', 'LIKE', '%'.$name.'%');
			}
		}
		if(request()->has('email')){
			$email=request('email');
			if($email!=""){
				$query->where('email', 'LIKE', '%'.$email.'%');
			}
		}
		if(request()->has('status')){
			$status=request('status');
			if($status!=""){
				$query->where('status',$status);
			}
		}
		if(request()->has('accnumber')){
			$accnumber=request('accnumber');
			if($accnumber!=""){
				$query->where('accno',$accnumber);
			}
		}
		if(request()->has('ifcnumber')){
			$ifcnumber=request('ifcnumber');
			if($ifcnumber!=""){
				$query->where('ifsc',$ifcnumber);
			}
		}
		$getlist = $query->orderBY('created','ASC')->paginate(20);
		return view('registerusers.verifybankaccount')->with('allplayers', $getlist);
    }
	public function viewpandetails($id){
		$pancarddetails = DB::table('pan_cards')->where('pan_cards.id',$id)->join('register_users','register_users.id','=','pan_cards.userid')->select('pan_cards.*','register_users.email')->first();
		if(!empty($pancarddetails)){		
		return view('registerusers.viewpandetails',compact('pancarddetails')); 
		}else{
			return Redirect::back();
		}
	}
	public function viewbankdetails($id){
		$pancarddetails = DB::table('banks')->where('banks.id',$id)->join('register_users','register_users.id','=','banks.userid')->select('banks.*','register_users.email','register_users.username')->first();
		if(!empty($pancarddetails)){	 
		return view('registerusers.viewbankdetails',compact('pancarddetails')); 
		}else{
			return Redirect::back();
		}
	}
	public function verifypan(){
		$query =DB::table('pan_cards')->join('register_users','register_users.id','=','pan_cards.userid')->where('pan_cards.status','0')->where('pan_cards.hold','0')->where('register_users.mobile_verify','1')->where('register_users.email_verify','1')->select('register_users.email','register_users.username','register_users.mobile_verify','register_users.email_verify','pan_cards.*')->orderBy('pan_cards.id');
		if(request()->has('name')){
			$name=request('name');
			if($name!=""){
				$query->where('username', 'LIKE', '%'.$name.'%');
			}
		}
		if(request()->has('email')){
			$email=request('email');
			if($email!=""){
				$query->where('email', 'LIKE', '%'.$email.'%');
			}
		}
		if(request()->has('status')){
			$status=request('status');
			if($status!=""){
				$query->where('status',$status);
			}
		}
		if(request()->has('pannumber')){
			$pannumber=request('pannumber');
			if($pannumber!=""){
				$query->where('pan_number',$pannumber);
			}
		}
		$getlist = $query->orderBY('created','ASC')->paginate(20);
// 		echo '<pre>'; print_r($getlist); die;
		return view('registerusers.verifypan')->with('allplayers', $getlist);
    }
	public function withdraw_amount(){
		date_default_timezone_set('Asia/Kolkata');
		$query =DB::table('withdraws')->join('register_users','register_users.id','=','withdraws.user_id')->join('banks','banks.userid','=','register_users.id')->join('pan_cards','pan_cards.userid','=','register_users.id')->select('register_users.*','banks.*','pan_cards.*','withdraws.*','register_users.id as reg_id','register_users.state as user_state','banks.id as bank_id','pan_cards.id as pan_id','withdraws.id as withdraw_id','register_users.activation_status as reg_status','banks.status as bank_status','pan_cards.status as pan_status','withdraws.status as withdraw_status','withdraws.amount as withdraw_amount','withdraws.created as withdraw_request')->where('withdraws.type','');
		if(request()->has('start_date')){
			$start_date = request('start_date');
			$start_date = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime(request('start_date'))));
			if($start_date!=""){
				$query->whereDate('withdraws.created', '>=',date('Y-m-d',strtotime($start_date)));
			}
		}
		if(request()->has('end_date')){
			$end_date = request('end_date');
			if($end_date!=""){
				$query->whereDate('withdraws.created', '<=',date('Y-m-d',strtotime($end_date)));
			}
		}
		if(request()->has('email')){
			$email = request('email');
			if($email!=""){
				$query->where('register_users.email',$email);
			}
		}
		if(request()->has('status')){
			$status = request('status');
			if($status!=""){
				$query->where('withdraws.status',$status);
			}
		}else{
			$query->where('withdraws.status',0);
		}
		$getlist = $query->orderBY('withdraws.created','DESC')->get();
		return view('registerusers.withdraw_amount')->with('allplayers', $getlist);
    }
	
	public function withdraw_payout(){
		date_default_timezone_set('Asia/Kolkata');
		$query =DB::table('withdraws')->join('register_users','register_users.id','=','withdraws.user_id')->join('banks','banks.userid','=','register_users.id')->join('pan_cards','pan_cards.userid','=','register_users.id')->select('register_users.*','banks.*','pan_cards.*','withdraws.*','register_users.id as reg_id','banks.id as bank_id','pan_cards.id as pan_id','withdraws.id as withdraw_id','register_users.activation_status as reg_status','banks.status as bank_status','pan_cards.status as pan_status','withdraws.status as withdraw_status','withdraws.amount as withdraw_amount','withdraws.created as withdraw_request')->where('withdraws.type','');
		if(request()->has('start_date')){
			$start_date = request('start_date');
			$start_date = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime(request('start_date'))));
			if($start_date!=""){
				$query->whereDate('withdraws.created', '>=',date('Y-m-d',strtotime($start_date)));
			}
		}
		if(request()->has('end_date')){
			$end_date = request('end_date');
			if($end_date!=""){
				$query->whereDate('withdraws.created', '<=',date('Y-m-d',strtotime($end_date)));
			}
		}
		if(request()->has('email')){
			$email = request('email');
			if($email!=""){
				$query->where('register_users.email',$email);
			}
		}
		if(request()->has('status')){
			$status = request('status');
			if($status!=""){
				$query->where('withdraws.status',$status);
			}
		}else{
			$query->where('withdraws.status',0);
		}
		$getlist = $query->orderBY('withdraws.created','DESC')->get();
		return view('registerusers.withdraw_payout')->with('allplayers', $getlist);
    }
	
	public function downloadwithdrawl(){
		$output1		= "";
		$output1 .='"User Id",';
		$output1 .='"Account Number",';
		$output1 .='"Withdrawl Amount",';
		$output1 .='"User Name",';
		$output1 .='"Withdraw RequestId",';
		$output1 .='"Bank IFSC Code",';
		$output1 .='"Bank Name",';
		$output1 .='"Bank Branch",';
		$output1 .='"User Email",';
		$output1 .='"Requested Date",';
		$output1 .='"Approved Date",';
		$output1 .='"Admin comment",';
		$output1 .="\n";
		$query =DB::table('withdraws')->join('register_users','register_users.id','=','withdraws.user_id')->join('bank','bank.userid','=','register_users.id')->join('pan_cards','pan_cards.userid','=','register_users.id')->where('withdraws.status','0')->select('register_users.*','bank.*','pan_cards.*','withdraws.*','register_users.id as reg_id','bank.id as bank_id','pan_cards.id as pan_id','withdraws.id as withdraws_id','register_users.activation_status as reg_status','bank.status as bank_status','pan_cards.status as pan_status','withdraws.status as withdraws_status','withdraws.amount as withdraws_amount','withdraws.created as withdraws_request');
		if(request()->has('start_date')){
			$start_date = request('start_date');
			$start_date = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime(request('start_date'))));
			if($start_date!=""){
				$query->whereDate('withdraws.created', '>=',date('Y-m-d',strtotime($start_date)));
			}
		}
		if(request()->has('end_date')){
			$end_date = request('end_date');
			if($end_date!=""){
				$query->whereDate('withdraws.created', '<=',date('Y-m-d',strtotime($end_date)));
			}
		}
		$getlist = $query->orderBY('withdraws.created','DESC')->get();
		if(!empty($getlist)){
			foreach($getlist as $get){ 
				$output1 .='"'.$get->reg_id.'",';
				$output1 .="'".$get->accno.",";
				$output1 .='"'.$get->withdraw_amount.'",';
				$output1 .='"'.strtoupper($get->username).'",';
				$output1 .='"'.$get->withdraw_request_id.'",';
				$output1 .='"'.strtoupper($get->ifsc).'",';
				$output1 .='"'.strtoupper($get->bankname).'",';
				$output1 .='"'.strtoupper($get->bankbranch).'",';
				$output1 .='"'.$get->email.'",';
				$output1 .='"'.date('d-M-Y',strtotime($get->withdraw_request)).'",';
				$output1 .='"",';
				$output1 .='"",';
				$output1 .="\n";
			}
		}
		$filename =  "Details-userwithdrawl.csv";
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $output1;
		exit;
	}

	public function paytm_withdraw_amount() {
		date_default_timezone_set('Asia/Kolkata');
		$query =DB::table('withdraws')->join('register_users','register_users.id','=','withdraws.user_id')->join('pan_cards','pan_cards.userid','=','register_users.id')->select('register_users.*','pan_cards.*','withdraws.*','register_users.id as reg_id','register_users.state as user_state','pan_cards.id as pan_id','withdraws.id as withdraw_id','register_users.activation_status as reg_status','pan_cards.status as pan_status','withdraws.status as withdraw_status','withdraws.amount as withdraw_amount','withdraws.created as withdraw_request')->where('withdraws.type','<>','');
		if(request()->has('start_date')){
			$start_date = request('start_date');
			$start_date = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime(request('start_date'))));
			if($start_date!=""){
				$query->whereDate('withdraws.created', '>=',date('Y-m-d',strtotime($start_date)));
			}
		}
		if(request()->has('end_date')){
			$end_date = request('end_date');
			if($end_date!=""){
				$query->whereDate('withdraws.created', '<=',date('Y-m-d',strtotime($end_date)));
			}
		}
		if(request()->has('email')){
			$email = request('email');
			if($email!=""){
				$query->where('register_users.email',$email);
			}
		}
		if(request()->has('status')){
			$status = request('status');
			if($status!=""){
				$query->where('withdraws.status',$status);
			}
		}else{
			$query->where('withdraws.status',0);
		}
		$getlist = $query->orderBY('withdraws.created','DESC')->get();
		return view('registerusers.paytm_withdraw_amount')->with('allplayers', $getlist);
    }

	public function uploadwithdrawlexcel(Request $request){
		// require_once("./phpexcel/PHPExcel.php");
		 // $objPHPExcel = new PHPExcel;
		 // $inputFileName = 'discussdesk.xlsx'; 
			$input = $request->all();
			if(isset($input['file'])){
				$image = $request->file('file');
				$filename=$image->getClientOriginalName();
				$realpath = $image->getRealPath();
				$file = fopen($realpath, "r"); 
				$header = fgetcsv($file);
				//print_r($header);
				$escapedHeader=[];
				//validate
				/*foreach ($header as $key => $value) {
					$lheader=strtolower($value);
					$escapedItem=preg_replace('/[^a-z]/', '', $lheader);
					array_push($escapedHeader, $escapedItem);
				}*/
				//looping through othe columns
				
				while($columns=fgetcsv($file))
				{
					if($columns[0]=="")
					{
						continue;
					}
					//trim data
					// foreach ($columns as $key => &$value) {
						// $value=preg_replace('/\D/','',$value);
					// }
					//print_r($columns);
				   $data= array_combine($header, $columns);
				   // setting type
				  if($data['Add Details 4']!=""){
					   $findapproveddate = $dataupdate['approved_date'] =  date('Y-m-d H:i:s');
					    $amount=$data['Amt'];
						$dataupdate['comment'] = $data['Add Details 4'];
						$withdrawrequestid = $data['Add Details 1'];
						$dataupdate['status']=1;
						$finduserdetails = DB::table('withdraws')->where('withdraws.withdraw_request_id',$withdrawrequestid)->where('withdraws.status',0)->join('register_users','register_users.id','=','withdraws.user_id')->select('user_id','register_users.email','register_users.team','withdraw_request_id')->first();
						if(!empty($finduserdetails)){
							$rowCOllection = DB::table('withdraws')->where('withdraws.withdraw_request_id',$withdrawrequestid)->update($dataupdate);
							$findtransactiondate = DB::table('transactions')->where('transaction_id',$withdrawrequestid)->where('paymentstatus','pending')->first();
							if(!empty($findtransactiondate)){
								//echo $withdrawrequestid;
								$tdata['paymentstatus'] = 'Confirmed';
								$tdata['created'] = $findtransactiondate->created;
								$findtransactiondetails  = DB::table('transactions')->where('transaction_id',$finduserdetails->withdraw_request_id)->update($tdata);
								//mail//
								$email = $finduserdetails->email;
								echo $email;
								$emailsubject = 'Withdraw Request Approved';
								$content='<p><strong>Hello '.ucwords($finduserdetails->team).' </strong></p>';
								//$content.='<p>Your withdrawl request of Rs.'.$data['withdrawlamount'].' has been approved successfully.</p>';
								/*thank you for the patience of two days bank holidays on saturday and sunday. <br/><br/>*/  
								$content.='<p>Dear Fanadda user, Your withdrawal request of Rs.'.$amount.' has been processed with  UTR no <strong>'.$dataupdate['comment'].'</strong>. Kindly check your bank balance. It should be credited within 24 hours.</p>';
								//$content.='<p><strong>'.$input['comment'].'</strong></p>';
								$content.='<p></p>';
								//$msg = Helpers::mailheader();
								//$msg.= Helpers::mailbody($content);
								//$msg.= Helpers::mailfooter();
								//Helpers::mailsentFormat($email,$emailsubject,$msg); 
								//notifications//
								$notificationdata['userid'] = $finduserdetails->user_id;
								$notificationdata['title'] = 'Withdraw Request is approved successfully of amount Rs.'.$amount;
								DB::table('notifications')->insert($notificationdata);
								//push notifications//
								require 'vendor/autoload.php';    
								$emailGrid = new \SendGrid\Mail\Mail(); 
								$emailGrid->setFrom("noreply@fanadda.com", "Fanadda");
								$emailGrid->setSubject($emailsubject);
								$emailGrid->addTo($email, "User");
								//$emailGrid->addBcc('admin@fanadda.com');
								$emailGrid->addContent(
									"text/html", $content
								);
								$sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));


								try {
									 $response = $sendgrid->send($emailGrid);
								} catch (Exception $e) {
									//error message
								}
								$titleget = 'Withdrawl Request Approved!';
								//Helpers::sendnotification($titleget,$notificationdata['title'],'',$finduserdetails->user_id);
								//end push notifications//
							}
						}
					}
				}
				//message show//
				Session::flash('message', 'Withdraw Request Approved successfully!');
				Session::flash('alert-class', 'alert-success');
				return Redirect::back();
			}	 
			die;
	}
	public function downloadwithdrawl1(){
		require_once("./phpexcel/PHPExcel.php");
		 $objPHPExcel = new PHPExcel;
            // set default font
            $objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');
            // set default font size
            $objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
            // create the writer
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            // currency format, € with < 0 being in red color
            $currencyFormat = '#,#0.## \€;[Red]-#,#0.## \€';
            // number format, with thousands separator and two decimal points.
            $numberFormat = '#,#0.##;[Red]-#,#0.##';
            // writer already created the first sheet for us, let's get it
            $objSheet = $objPHPExcel->getActiveSheet();
            // rename the sheet
            $objSheet->setTitle('My sales report');
            // let's bold and size the header font and write the header
            // as you can see, we can specify a range of cells, like here: cells from A1 to A4
            $objSheet->getStyle('A1:L1')->getFont()->setBold(true)->setSize(12);
           
            // write header
            $objSheet->getCell('A1')->setValue('Sno');
            $objSheet->getCell('B1')->setValue('User id');
            $objSheet->getCell('C1')->setValue('User Name');
            $objSheet->getCell('D1')->setValue('Total Amount');
            $objSheet->getCell('E1')->setValue('Due Amount');
            $objSheet->getCell('F1')->setValue('Complete Amount');
            $objSheet->getCell('G1')->setValue('Account No.');
            $objSheet->getCell('H1')->setValue('IFSC Code');
            // we could get this data from database, but here we are writing for simplicity
	}
	
	public function viewtransactions($uid){
		date_default_timezone_set("Asia/Kolkata"); 
// 		$query =DB::table('transactions');
// 		$query->where('transactions.userid',$uid);
		
		// $query = DB::table('transactions')->where('transactions.userid',$uid)
		// ->where('paymentstatus','confirmed');

		//last working
		// $query = DB::table('transactions')
		// ->where('transactions.userid',$uid)
		// ->where('paymentstatus','confirmed')
		// ->Where('paymentstatus','')
		// ;
 
		$query = DB::table('transactions')->where('transactions.userid',$uid)->where('transactions.amount','>',0)->where('paymentstatus','confirmed')->where('transactions.type','<>','Join league affiliation')->where('transactions.type','<>','bonus on pan verify.');
		// $query->where(function($q){
		// 	$q->where('transactions.paymentstatus','=','confirmed')
		// 	->orWhere('transactions.paymentstatus','=','');
		// 	});

		 

		// print_r($query->toSql());

		if(request()->has('start_date')){
			$start_date = request('start_date');
			if($start_date!=""){
				$query->whereDate('transactions.created', '>=',date('Y-m-d',strtotime($start_date)));
			}
		}
		
		if(request()->has('end_date')){
			$end_date = request('end_date');
			if($end_date!=""){
				$query->whereDate('transactions.created', '<=',date('Y-m-d',strtotime($end_date)));
			}
		}
		$allplayers = $query->orderBY('transactions.created','DESC')->orderBY('transactions.total_available_amt','DESC')->join('register_users','register_users.id','=','transactions.userid')->join('user_balances','user_balances.user_id','=','transactions.userid')->select('register_users.email','user_balances.winning','user_balances.balance','user_balances.bonus','transactions.*')->get();
		
		$data = array('uid'   => $uid);
		return view('registerusers.viewtransactions',compact('allplayers'))->with($data);
		// return view('registerusers.viewtransactions',compact('allplayers','uid'));
		// return view('registerusers.viewtransactions',compact('allplayers','data'));
    }
	
	public function approve(Request $request){
		if ($request->isMethod('post')){
			$input = Input::all();
			$uid=$input['uid'];
			$amount=$input['amount'];
			$data['comment']=$input['comment'];
			$data['approved_date']=date('Y-m-d');
			$data['status']=1;
			// if(Helpers::cf_request_transfer($user_id, $amount)) {

			// } else {
			// 	Session::flash('message', 'There is some error please check status in your cashfree account!');
			// 	Session::flash('alert-class', 'alert-danger');
			// 	return Redirect::back();
			// }
			$finduserdetails = DB::table('withdraws')->where('withdraws.id',$input['id'])->join('register_users','register_users.id','=','withdraws.user_id')->select('user_id','register_users.email','register_users.team','withdraw_request_id')->first();
			$rowCOllection = DB::table('withdraws')->where('id',$input['id'])->update($data);
			$findtransactiondate = DB::table('transactions')->where('transaction_id',$finduserdetails->withdraw_request_id)->first();
			$tdata['paymentstatus'] = 'Confirmed';
			$tdata['created'] = $findtransactiondate->created;
			$findtransactiondetails  = DB::table('transactions')->where('transaction_id',$finduserdetails->withdraw_request_id)->update($tdata);
			//mail//
			$email = $finduserdetails->email;
			$emailsubject = 'Withdraw Request approved';
			$content='<p><strong>Hello '.ucwords($finduserdetails->team).' </strong></p>';
			//$content.='<p>Your withdrawl request of Rs.'.$input['amount'].' has been approved successfully.</p>';
			$content.='<p>Dear Fanadda user, your withdrawal request of Rs.'.$input['amount'].' has been credited to your bank account with  UTR no <strong>'.$input['comment'].'</strong>. It should be credited within 24 hours. Please check your bank balance</p>';
			//$content.='<p><strong>'.$input['comment'].'</strong></p>';
			$content.='<p></p>';
			/*$msg = Helpers::mailheader();
			$msg.= Helpers::mailbody($content);
			$msg.= Helpers::mailfooter();
			Helpers::mailsentFormat($email,$emailsubject,$msg);*/
			//notifications//
			$notificationdata['userid'] = $finduserdetails->user_id;
			$notificationdata['title'] = 'Withdraw Request Approved successfully of amount Rs.'.$input['amount'];
			DB::table('notifications')->insert($notificationdata);
			//push notifications//
			require 'vendor/autoload.php';    
			$emailGrid = new \SendGrid\Mail\Mail(); 
			$emailGrid->setFrom("noreply@fanadda.com", "Fanadda");
			$emailGrid->setSubject($emailsubject);
			$emailGrid->addTo($email, "User");
			//$emailGrid->addBcc('admin@fanadda.com');
			$emailGrid->addContent(
				"text/html", $content
			);
			$sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));


			try {
				 $response = $sendgrid->send($emailGrid);
			} catch (Exception $e) {
				//error message
			}
			$titleget = 'Withdrawl Request Approved!';
			// Helpers::sendnotification($titleget,$notificationdata['title'],'',$finduserdetails->user_id);
			//end push notifications//
			//message show//
			Session::flash('message', 'Withdraw Request Approved successfully!');
			Session::flash('alert-class', 'alert-success');
			return Redirect::back();
		}
	}

	public function approve_paytm(Request $request){
		if ($request->isMethod('post')){
			$input = Input::all();
			$uid=$input['uid'];
			$amount=$input['amount'];
			$data['comment']=$input['comment'];
			$data['approved_date']=date('Y-m-d');
			$data['status']=1;
			$final_amount = $amount * 0.97;
			if(Helpers::cf_request_paytm_transfer($uid, $final_amount)) {

			} else {
				Session::flash('message', 'There is some error please check status in your cashfree account!');
				Session::flash('alert-class', 'alert-danger');
				return Redirect::back();
			}
			$finduserdetails = DB::table('paytm_withdraw')->where('paytm_withdraw.id',$input['id'])->join('register_users','register_users.id','=','paytm_withdraw.user_id')->select('user_id','register_users.email','register_users.team','withdraw_request_id')->first();
			$rowCOllection = DB::table('paytm_withdraw')->where('id',$input['id'])->update($data);
			$findtransactiondate = DB::table('transactions')->where('transaction_id',$finduserdetails->withdraw_request_id)->first();
			$tdata['paymentstatus'] = 'Confirmed';
			$tdata['created'] = $findtransactiondate->created;
			$findtransactiondetails  = DB::table('transactions')->where('transaction_id',$finduserdetails->withdraw_request_id)->update($tdata);
			//mail//
			$email = $finduserdetails->email;
			$emailsubject = 'Withdraw Request approved';
			$content='<p><strong>Hello '.ucwords($finduserdetails->team).' </strong></p>';
			$content.='<p>Your withdrawl request of Rs.'.$input['amount'].' has been approved successfully.</p>';
			//$content.='<p><strong>'.$input['comment'].'</strong></p>';
			$content.='<p></p>';
			$msg = Helpers::mailheader();
			$msg.= Helpers::mailbody($content);
			$msg.= Helpers::mailfooter();
			Helpers::mailsentFormat($email,$emailsubject,$msg);
			//notifications//
			$notificationdata['userid'] = $finduserdetails->user_id;
			$notificationdata['title'] = 'Withdraw Request Approved successfully of amount Rs.'.$input['amount'];
			DB::table('notifications')->insert($notificationdata);
			//push notifications//
			$titleget = 'Withdrawl Request Approved!';
			Helpers::sendnotification($titleget,$notificationdata['title'],'',$finduserdetails->user_id);
			//end push notifications//
			//message show//
			Session::flash('message', 'Withdraw Request Approved successfully!');
			Session::flash('alert-class', 'alert-success');
			return Redirect::back();
		}
	}
	
	public function reject_withdraw(Request $request){
	    if ($request->isMethod('post')){
			$input = Input::all();
			$uid=$input['uid'];
			$amount=$input['amount'];
			$data['comment']=$input['comment'];
			$data['approved_date']=date('Y-m-d');
			$data['status']=-1;
			$finduserdetails = DB::table('withdraws')->where('withdraws.id',$input['id'])->join('register_users','register_users.id','=','withdraws.user_id')->select('user_id','register_users.email','register_users.team','withdraw_request_id')->first();
			$rowCOllection = DB::table('withdraws')->where('id',$input['id'])->update($data);
			
			//add balance back to wallet winning amount
			$userbalance = DB::table('user_balances')->where('user_id',$uid)->select('winning')->first();
			
			$returndata['winning']= $userbalance->winning + $amount;
			DB::table('user_balances')->where('user_id',$uid)->update($returndata);
			
			$data_withdaw_cancel['userid'] = $uid;
			$data_withdaw_cancel['amount'] = $amount;
			$data_withdaw_cancel['withdraw_id'] = $finduserdetails->withdraw_request_id;
			
			DB::table('rejected_withdraw')->insert($data_withdaw_cancel);
			
			$findtransactiondate = DB::table('transactions')->where('transaction_id',$finduserdetails->withdraw_request_id)->first();
			$tdata['paymentstatus'] = 'Canceled';
			$tdata['created'] = $findtransactiondate->created;
			$findtransactiondetails  = DB::table('transactions')->where('transaction_id',$finduserdetails->withdraw_request_id)->update($tdata);
			
			//mail//
			$email = $finduserdetails->email;
			$emailsubject = 'Withdraw Request rejected';
			$content='<p><strong>Hello '.ucwords($finduserdetails->team).' </strong></p>';
			$content.='<p>Your withdrawl request of Rs.'.$input['amount'].' has been rejected due to '.$data['comment'].'.</p>';
			//$content.='<p><strong>'.$input['comment'].'</strong></p>';
			$content.='<p></p>';
			$msg = Helpers::mailheader();
			$msg.= Helpers::mailbody($content);
			$msg.= Helpers::mailfooter();
			Helpers::mailsentFormat($email,$emailsubject,$msg);
			//notifications//
			$notificationdata['userid'] = $finduserdetails->user_id;
			$notificationdata['title'] = 'Withdraw Request rejected of amount Rs.'.$input['amount'];
			DB::table('notifications')->insert($notificationdata);
			//push notifications//
			$titleget = 'Withdrawl Request Rejected!';
			Helpers::sendnotification($titleget,$notificationdata['title'],'',$finduserdetails->user_id);
			//end push notifications//
			//message show//
			Session::flash('message', 'Withdraw Request Rejected successfully!');
			Session::flash('alert-class', 'alert-success');
			return Redirect::back();
		}
	}

	public function reject_withdraw_paytm(Request $request){
	    if ($request->isMethod('post')){
			$input = Input::all();
			$uid=$input['uid'];
			$amount=$input['amount'];
			$data['comment']=$input['comment'];
			$data['approved_date']=date('Y-m-d');
			$data['status']=-1;
			$finduserdetails = DB::table('withdraw_paytm')->where('withdraw_paytm.id',$input['id'])->join('register_users','register_users.id','=','withdraw_paytm.user_id')->select('user_id','register_users.email','register_users.team','withdraw_request_id')->first();
			$rowCOllection = DB::table('withdraw_paytm')->where('id',$input['id'])->update($data);
			
			//add balance back to wallet winning amount
			$userbalance = DB::table('user_balances')->where('user_id',$uid)->select('winning')->first();
			
			$returndata['winning']= $userbalance->winning + $amount;
			DB::table('user_balances')->where('user_id',$uid)->update($returndata);
			
			$data_withdaw_cancel['userid'] = $uid;
			$data_withdaw_cancel['amount'] = $amount;
			$data_withdaw_cancel['withdraw_id'] = $finduserdetails->withdraw_request_id;
			
			DB::table('rejected_withdraw')->insert($data_withdaw_cancel);
			
			$findtransactiondate = DB::table('transactions')->where('transaction_id',$finduserdetails->withdraw_request_id)->first();
			$tdata['paymentstatus'] = 'Canceled';
			$tdata['created'] = $findtransactiondate->created;
			$findtransactiondetails  = DB::table('transactions')->where('transaction_id',$finduserdetails->withdraw_request_id)->update($tdata);
			
			//mail//
			$email = $finduserdetails->email;
			$emailsubject = 'Withdraw Request rejected';
			$content='<p><strong>Hello '.ucwords($finduserdetails->team).' </strong></p>';
			$content.='<p>Your withdrawl request of Rs.'.$input['amount'].' has been rejected due to '.$data['comment'].'.</p>';
			//$content.='<p><strong>'.$input['comment'].'</strong></p>';
			$content.='<p></p>';
			$msg = Helpers::mailheader();
			$msg.= Helpers::mailbody($content);
			$msg.= Helpers::mailfooter();
			Helpers::mailsentFormat($email,$emailsubject,$msg);
			//notifications//
			$notificationdata['userid'] = $finduserdetails->user_id;
			$notificationdata['title'] = 'Withdraw Request rejected of amount Rs.'.$input['amount'];
			DB::table('notifications')->insert($notificationdata);
			//push notifications//
			$titleget = 'Withdrawl Request Approved!';
			Helpers::sendnotification($titleget,$notificationdata['title'],'',$finduserdetails->user_id);
			//end push notifications//
			//message show//
			Session::flash('message', 'Withdraw Request Rejected successfully!');
			Session::flash('alert-class', 'alert-success');
			return Redirect::back();
		}
	}
	
	
	public function updateuserstatus($id,$status){
		$id = unserialize(base64_decode($id));
		$input['id']=$id;
		$input['activation_status']=$status;
		DB::table('register_users')->where('id',$id)->update($input);
		Session::flash('message', 'User '.$status.' successfully!');
		Session::flash('alert-class', 'alert-success');
		return Redirect::back();
	}

	public function deductAmount(Request $request, $id=''){
		if ($request->isMethod('post')){
			$input = Input::all();
			$amount = $input['amount'];
			$type = $input['type'];
			$id = $input['id'];
			if(!empty($amount) && !empty($type) && !empty($id)) {
				$find_balance = DB::table('user_balances')->where('user_id', $id)->first();
				if(!empty($find_balance)) {
					$balance = $find_balance->balance;
					$winning = $find_balance->winning;
					$bonus = $find_balance->bonus;
					if($type=='winning') {
						if($amount > $winning) {
							Session::flash('message', 'User balance low to deduct!');
							Session::flash('alert-class', 'alert-danger');
							return Redirect::back();
						} else {
							$update['winning'] = $winning - $amount;
							DB::table('user_balances')->where('user_id',$id)->update($update);

							$datatr['transaction_id'] = 'deduct';
							$datatr['type'] = 'amount deduct by admin';
							$datatr['transaction_by'] = 'BBF';
							$datatr['amount'] = $amount;
							$datatr['paymentstatus'] = 'confirmed';
							$datatr['challengeid'] = '';
							$datatr['win_amt'] = '';
							$datatr['bal_bonus_amt'] = $bonus;
							$datatr['bal_win_amt'] = $update['winning'];
							$datatr['bal_fund_amt'] = $balance;
							$datatr['userid'] = $id;
							$datatr['total_available_amt'] = $balance+$update['winning']+$bonus;
							DB::table('transactions')->insert($datatr);

							Session::flash('message', 'User balance updated!');
							Session::flash('alert-class', 'alert-success');
							return Redirect::back();
						}
					}

					if($type=='unutilize') {
						if($amount > $balance) {
							Session::flash('message', 'User balance low to deduct!');
							Session::flash('alert-class', 'alert-danger');
							return Redirect::back();
						} else {
							$update['balance'] = $balance - $amount;
							DB::table('user_balances')->where('user_id',$id)->update($update);

							$datatr['transaction_id'] = 'deduct';
							$datatr['type'] = 'amount deduct by admin';
							$datatr['transaction_by'] = 'BBF';
							$datatr['amount'] = $amount;
							$datatr['paymentstatus'] = 'confirmed';
							$datatr['challengeid'] = '';
							$datatr['win_amt'] = '';
							$datatr['bal_bonus_amt'] = $bonus;
							$datatr['bal_win_amt'] = $winning;
							$datatr['bal_fund_amt'] = $update['balance'];
							$datatr['userid'] = $id;
							$datatr['total_available_amt'] = $update['balance']+$winning+$bonus;
							DB::table('transactions')->insert($datatr);

							Session::flash('message', 'User balance updated!');
							Session::flash('alert-class', 'alert-success');
							return Redirect::back();
						}
					}

					if($type=='bonus') {
						if($amount > $bonus) {
							Session::flash('message', 'User balance low to deduct!');
							Session::flash('alert-class', 'alert-danger');
							return Redirect::back();
						} else {
							$update['bonus'] = $bonus - $amount;
							DB::table('user_balances')->where('user_id',$id)->update($update);

							$datatr['transaction_id'] = 'deduct';
							$datatr['type'] = 'amount deduct by admin';
							$datatr['transaction_by'] = 'BBF';
							$datatr['amount'] = $amount;
							$datatr['paymentstatus'] = 'confirmed';
							$datatr['challengeid'] = '';
							$datatr['win_amt'] = '';
							$datatr['bal_bonus_amt'] = $update['bonus'];
							$datatr['bal_win_amt'] = $winning;
							$datatr['bal_fund_amt'] = $balance;
							$datatr['userid'] = $id;
							$datatr['total_available_amt'] = $balance+$winning+$update['bonus'];
							DB::table('transactions')->insert($datatr);

							Session::flash('message', 'User balance updated!');
							Session::flash('alert-class', 'alert-success');
							return Redirect::back();

						}
					}
				} else {
					Session::flash('message', 'There is some error!');
					Session::flash('alert-class', 'alert-danger');
					return Redirect::back();
				}
			} else {
				Session::flash('message', 'There is some error!');
				Session::flash('alert-class', 'alert-danger');
				return Redirect::back();
			}
		} else if(!empty($id)) {
			$id = unserialize(base64_decode($id));
			$input['id']=$id;
			return view('registerusers.deduct_amount')->with('user_id', $id);
		}
	}

	public function updatebanktatus(Request $request){
		if ($request->isMethod('post')){
			$input = Input::all();
			$id=$input['id'];
			$status=$input['status'];
			$input['verifier']=Auth::user()->email;
			unset($input['_token']);
			
			$req['bank_verify']=$status;
			if(isset($input['accno'])){
				$input['accno'] = strtoupper($input['accno']);
			}
			if(isset($input['ifsc'])){
				$input['ifsc'] = strtoupper($input['ifsc']);
			}
			if(isset($input['bankname'])){
				$input['bankname'] = strtoupper($input['bankname']);
			}
			if(isset($input['bankbranch'])){
				$input['bankbranch'] = strtoupper($input['bankbranch']);
			}
			// if(isset($input['state'])){
			// 	$input['state'] = $input['state'];
			// }
			if(isset($input['image'])){
				$filee = array_filter($input['image']);
				unset($input['image']);
				if(!empty($filee)){
					$destinationPath = 'uploads';
					$oldimage = $input['oldimage'];
					if($oldimage!=""){
						File::delete($oldimage);
					}
					unset($input['oldimage']);
					if(!empty($filee)){
					  $fileName  = 'select2win-bank-'.rand(100,999);
					  $imageName = Helpers::imageUpload($filee,$destinationPath,$fileName);
					  $input['image'] = Config::get('constants.PROJECT_URL').'uploads/'.$imageName;
					}
				 }
			}
			unset($input['oldimage']);
			DB::table('banks')->where('id',$id)->update($input);
			$findlastow = DB::table('banks')->where('id',$id)->first();
			if(!empty($findlastow)){
				$userid = $findlastow->userid;
			}
			$finduserbonus = DB::table('register_users')->where('id',$userid)->select('bankbonus','email','team')->first();
			$st='';
			if($status==1){
				$st='Verified';
				  // $randum
				  // $referer = RegisterUser::where('refer_id',$id)->get();
				  // $referer_bl = UserBalance::where('user_id',$referer->id)->get();
				  // $referer_bl->bonus = 50;
				  // $referer_bl->save();
				  // $trans = DB::table('transactions');
			   //    $ins_tran['userid'] = $referer->id;
			   //    $ins_tran['refer_id'] = $id;
			   //    $ins_tran['transaction_id'] = 'L11-EMAILREFER-'.$randum;
			   //    $ins_tran['type'] = 'refer bonus on pan verification.';
			   //    $ins_tran['transaction_by'] = 'Fanadda';
			   //    $ins_tran['total_available_amt'] = $ub_referer_get_again->winning+$ub_referer_get_again->balance+$ub_referer_get_again->bonus;
			   //    $ins_tran['bal_bonus_amt'] = $ub_referer_get_again->bonus;
			   //    $ins_tran['bonus_amt'] = 50;
			   //    $trans->insert($ins_tran);
			}
			if($status==2){
				$st='rejected';
				$email = $finduserbonus->email;
				$emailsubject = 'Sorry, your Bank Documents were not Approved';
				$content='<p><strong>Hello '.ucwords($finduserbonus->team).' </strong></p>';
				$content.='<p>Bank Card Documents uploaded by you have not been approved</p>';
				$content.='<p>Reason: <strong>'.$input['comment'].'</strong></p>';
				$content.='<p></p>';
				$msg = Helpers::mailheader();
				$msg.= Helpers::mailbody($content);
				$msg.= Helpers::mailfooter();
				Helpers::mailsentFormat($email,$emailsubject,$msg);
			}
			if($st=='Verified'){
				$req['bankbonus']=1;
				// $req['state']=$findlastow->state;
				//if($finduserbonus->bankbonus==0){ 
					//ApiController::getbonus($userid,'Bank Account');
				//}
			}
			DB::table('register_users')->where('id',$userid)->update($req);
				
			$notificationdata['userid'] = $userid;
			$notificationdata['title'] = 'Your Bank documents request is '.$st;
			DB::table('notifications')->insert($notificationdata);
			//push notifications//
			$titleget = 'Bank Documents Verification Request!';
			Helpers::sendnotification($titleget,$notificationdata['title'],'',$userid);
			//end push notifications//
			
			Session::flash('message', 'Bank Account Request is '.$st.'!');
			Session::flash('alert-class', 'alert-success');
			return Redirect::back();
		}
	}
	public function updatepantatus(Request $request){
		$status="";
		if($request->isMethod('post')){
			$input = Input::all();
			$id=$input['id'];
			$status=$input['status'];
			$input['verifier']=Auth::user()->email;
			if(isset($input['pan_name'])){
				$input['pan_name'] = strtoupper($input['pan_name']);
			}
			if(isset($input['pan_number'])){
				$input['pan_number'] = strtoupper($input['pan_number']);
			}
			if(isset($input['image'])){
				$filee = array_filter($input['image']);
				unset($input['image']);
				if(!empty($filee)){
					$destinationPath = 'uploads';
					$oldimage = $input['oldimage'];
					if($oldimage!=""){
						File::delete($oldimage);
					}
					unset($input['oldimage']);
					if(!empty($filee)){
					  $fileName  = 'select2win-pancard-'.rand(100,999);
					  $imageName = Helpers::imageUpload($filee,$destinationPath,$fileName);
					  $input['image'] = Config::get('constants.PROJECT_URL').'uploads/'.$imageName;
					}
				 }
			}
			unset($input['oldimage']);
		}
		if($status!=""){
			if($status==1){
				$st='Verified';  

				// $balance['bonus'] = 50;  
				// $user_balances = DB::table('user_balances')->where('user_id',$id)->update($balance);
				// $balance['panbonus'] = 1; 
			//Diposit in referer on pan verification
			$id_data = DB::table('pan_cards')->where('id',$id)->first();
			$id_user = $id_data->userid;

			$referer_u = DB::table('register_users')->where('id',$id_user)->first();
			$referer = DB::table('register_users')->where('id',$referer_u->refer_id)->where('is_youtuber', 0)->first();
			// print_r($referer);die;
			if (!empty($referer)) {
				
				$ub_referer_get = DB::table('user_balances')->where('user_id',$referer->id)->first();
				$bonus_ref['bonus'] = $ub_referer_get->bonus + 50;  
				$ub_referer = DB::table('user_balances')->where('user_id',$referer->id)->update($bonus_ref); 

				$ub_referer_get_again = DB::table('user_balances')->where('user_id',$referer->id)->first();

				//entry in refer bonus list
				$ref_data['user_id'] = $id;
				$ref_data['refered_by'] = $referer->id;
				$ref_data['amount'] = 50;
				DB::table('refer_bonus')->insert($ref_data);


				//entry in transaction table
				// $referes_id = DB::table('register_users')->where('id',)
				$get_user_bal = DB::table('user_balances')->where('user_id',$referer->id)->first();
				$random = rand(1000000,99999999);
				  $trans = DB::table('transactions');
			      $ins_tran['userid'] = $referer->id;
			      $ins_tran['refer_id'] = $id_user; 
			      $ins_tran['transaction_id'] = 'L11-SIGNUP-'.$random;
			      $ins_tran['type'] = 'bonus on pan verify.';
			      $ins_tran['paymentstatus'] = 'confirmed';
			      $ins_tran['total_available_amt'] = $get_user_bal->winning+$get_user_bal->balance+$get_user_bal->bonus;
			      $ins_tran['bal_bonus_amt'] = $get_user_bal->bonus;
			      $ins_tran['bonus_amt'] = 50;
			      $ins_tran['amount'] = 50;
			      $trans->insert($ins_tran);

			}
			//affiliator
			$referer2 = DB::table('register_users')->where('id',$referer_u->refer_id)->where('is_youtuber', '>' , 0)->first();
			// print_r($referer2);die;
			if (!empty($referer2)) {
				if($referer2->pan_affiliation>0){
					$ub_referer_get = DB::table('user_balances')->where('user_id',$referer2->id)->first();
					$bonus_ref['winning'] = $ub_referer_get->winning + $referer2->pan_affiliation;  
					$ub_referer = DB::table('user_balances')->where('user_id',$referer2->id)->update($bonus_ref); 

					$ub_referer_get_again = DB::table('user_balances')->where('user_id',$referer2->id)->first();

					//entry in refer bonus list
					$ref_data['user_id'] = $id;
					$ref_data['refered_by'] = $referer2->id;
					$ref_data['amount'] = $referer2->pan_affiliation;
					DB::table('refer_bonus')->insert($ref_data);


					//entry in transaction table
					// $referes_id = DB::table('register_users')->where('id',)
					$get_user_bal = DB::table('user_balances')->where('user_id',$referer2->id)->first();
					$random = rand(1000000,99999999);
					$trans = DB::table('transactions');
					$ins_tran['userid'] = $referer2->id;
					$ins_tran['refer_id'] = $id_user; 
					$ins_tran['transaction_id'] = 'FA-PAN-AFFILIATION-'.$random;
					$ins_tran['type'] = 'Refer amount after pan verification';
					$ins_tran['paymentstatus'] = 'confirmed';
					$ins_tran['total_available_amt'] = $get_user_bal->winning+$get_user_bal->balance+$get_user_bal->bonus;
					$ins_tran['bal_bonus_amt'] = $get_user_bal->bonus;
					$ins_tran['win_amt'] = $referer2->pan_affiliation;
					$ins_tran['amount'] = $referer2->pan_affiliation; 
					$trans->insert($ins_tran); 
				}
			}
			
			}
		}
		unset($input['_token']);

		$req['pan_verify']=$status;
		DB::table('pan_cards')->where('id',$id)->update($input);
		$findlastow = DB::table('pan_cards')->where('id',$id)->first();
		if(!empty($findlastow)){
			$userid = $findlastow->userid;
		}
		$finduserbonus = DB::table('register_users')->where('id',$userid)->select('panbonus','email','team')->first();
		$st='';
		if($status==1){
			$st='Verified';
		}
		if($status==2){
			$st='rejected';
			$email = $finduserbonus->email;
			$emailsubject = 'Sorry, your PAN Card Documents were not Approved';
			$content='<p><strong>Hello '.ucwords($finduserbonus->team).' </strong></p>';
			$content.='<p>PAN Card Documents uploaded by you have not been approved</p>';
			$content.='<p>Reason: <strong>'.$input['comment'].'</strong></p>';
			$content.='<p></p>';
			$msg = Helpers::mailheader();
			$msg.= Helpers::mailbody($content);
			$msg.= Helpers::mailfooter();
			Helpers::mailsentFormat($email,$emailsubject,$msg);
		}
		if($st=='Verified'){
			$req['username']=$findlastow->pan_name;
			$req['dob']=date('Y-m-d',strtotime($findlastow->pan_dob));
			$req['panbonus']=1;
			if($finduserbonus->panbonus==0){ 
				RegisterusersController::getbonus($userid,'Pan Card');
			}
		}
		DB::table('register_users')->where('id',$userid)->update($req);
		$notificationdata['userid'] = $userid;
		$notificationdata['title'] = 'Your PAN card verification request is '.$st;
		DB::table('notifications')->insert($notificationdata);
		//push notifications//
		$titleget = 'Verification Request!';
		// Helpers::sendnotification($titleget,$notificationdata['title'],'',$userid);
		//end push notifications//
		Session::flash('message', 'PAN Card Request is '.$st.'!');
		Session::flash('alert-class', 'alert-success');
		return Redirect::back();
		
	}

	public static function getbonus($userid,$type){
// 		date_default_timezone_set('Asia/Kolkata'); 
// 		if(/*$type=='Mobile' ||*/ $type=='Email'){ 
// 			$data['bonus'] =$amount =  25;
// 		}
// 		// else if($type=='Pan Card'){
// 		// 	$data['bonus'] =$amount =  25;
// 		// }
// 		/*else{
// 			$data['bonus'] =$amount =  0;
// 		}*/
// 		$userid = $getdata['userid'] = $userid;
// 		$finduser  = DB::table('register_users')->where('id',$userid)->select('refer_id')->first();
// 		if($finduser->refer_id!=0){
// 			$userdata = array(); $datainseert=array();$findlastow=array();$notificationdata=array();$transactionsdata=array();
// 			$referid = $finduser->refer_id;
// 			if($type=='Mobile' || $type=='Email'){ 
// 				$refdata['bonus'] =$referamount =  50;
// 			}

// 			if($type=='Pan Card') {
// 				$refdata['bonus'] =$referamount =  25;
// 			}
	
// 			$refer_b['user_id']= $userid;
// 			$refer_b['refered_by']= $referid;
// 			$refer_b['amount'] = $referamount;
			
// // 			echo '<pre>'; print_r($refer_b); die;
			
// // 			DB::table('refer_bonus')->insert($refer_b);
			
// 			$userdata = DB::table('user_balances')->where('user_id',$referid)->first();
// 			if(!empty($userdata)){
// 				$datainseert['user_id'] = $referid;
// 				$datainseert['bonus'] = $userdata->bonus+$refdata['bonus'];
// 				DB::table('user_balances')->where('user_id',$referid)->update($datainseert);
// 			}
// 			$bal_bonus_amt=0;$bal_win_amt=0;$bal_fund_amt=0;$total_available_amt=0;
// 			$findlastow = DB::table('user_balances')->where('user_id',$referid)->first();
// 			if(!empty($findlastow)){
// 				$total_available_amt = $findlastow->balance+$findlastow->winning+$findlastow->bonus;
// 				$bal_fund_amt = $findlastow->balance;
// 				$bal_win_amt = $findlastow->winning;
// 				$bal_bonus_amt = $findlastow->bonus;
// 			}
// 			$notificationdata['userid'] = $referid;
// 			$notificationdata['title'] = $type.' Verification Referral Bonus Of  Rs '.$referamount;
// 			DB::table('notifications')->insert($notificationdata);
// 			//push notifications//
// 			$titleget = 'Verification Referral Bonus!';
// 			Helpers::sendnotification($titleget,$notificationdata['title'],'',$referid);
// 			//end push notifications//
// 			$transactionsdata['userid'] = $referid;
// 			$transactionsdata['refer_id'] = $userid;
// 			$transactionsdata['type'] = $type.' Verification Referral Bonus';
// 			$transactionsdata['transaction_id'] = 'BBF-VBONUS-'.rand(1000,9999);
// 			$transactionsdata['transaction_by'] = 'Fanadda';
// 			$transactionsdata['amount'] = $referamount;
// 			$transactionsdata['bonus_amt'] = $referamount;
// 			$transactionsdata['paymentstatus'] = 'confirmed';
// 			$transactionsdata['bal_fund_amt'] = $bal_fund_amt;
// 			$transactionsdata['bal_win_amt'] = $bal_win_amt;
// 			$transactionsdata['bal_bonus_amt'] = $bal_bonus_amt;
// 			// $transactionsdata['cons_amount'] = $referamount;
// 			$transactionsdata['total_available_amt'] = $total_available_amt;
// 			DB::table('transactions')->insert($transactionsdata);
// 		}
		
// 		//end of reffer amount bonus
// 		if(@$amount) {
// 		$userdata = array(); $datainseert=array();$findlastow=array();$notificationdata=array();$transactionsdata=array();
// 		$userdata = DB::table('user_balances')->where('user_id',$userid)->first();
// 		if(!empty($userdata)){
// 			$datainseert['user_id'] = $getdata['userid'];
// 			$datainseert['bonus'] = $userdata->bonus+$data['bonus'];
// 			DB::table('user_balances')->where('user_id',$userid)->update($datainseert);
// 		}
// 		$bal_bonus_amt=0;$bal_win_amt=0;$bal_fund_amt=0;$total_available_amt=0;
// 		$findlastow = DB::table('user_balances')->where('user_id',$userid)->first();
// 		if(!empty($findlastow)){
// 			$total_available_amt = $findlastow->balance+$findlastow->winning+$findlastow->bonus;
// 			$bal_fund_amt = $findlastow->balance;
// 			$bal_win_amt = $findlastow->winning;
// 			$bal_bonus_amt = $findlastow->bonus;
// 		}
// 		$notificationdata['userid'] = $userid;
// 		$notificationdata['title'] = $type.' Verification Bonus Of  Rs '.$amount;
// 		DB::table('notifications')->insert($notificationdata);
// 		//push notifications//
// 		$titleget = 'Verification Bonus!';
// 		Helpers::sendnotification($titleget,$notificationdata['title'],'',$userid);
// 		//end push notifications//
// 		$transactionsdata['userid'] = $userid;
// 		$transactionsdata['type'] = $type.' Verification Bonus';
// 		$transactionsdata['transaction_id'] = 'BBF-EBONUS-'.rand(1000,9999);
// 		$transactionsdata['transaction_by'] = 'Fanadda';
// 		$transactionsdata['amount'] = $amount;
// 		$transactionsdata['bonus_amt'] = $amount;
// 		$transactionsdata['paymentstatus'] = 'confirmed';
// 		$transactionsdata['bal_fund_amt'] = $bal_fund_amt;
// 		$transactionsdata['bal_win_amt'] = $bal_win_amt;
// 		$transactionsdata['bal_bonus_amt'] = $bal_bonus_amt;
// 		// $transactionsdata['cons_amount'] = $amount;
// 		$transactionsdata['total_available_amt'] = $total_available_amt;
// 		DB::table('transactions')->insert($transactionsdata);
// 		}
	}
	
	public function editpandetails(Request $request,$id){
		$editpandetails = DB::table('pan_cards')->where('id',$id)->first();
		return view('registerusers.editpandetails',compact('editpandetails'));
	}
	public function editbankdetails(Request $request,$id){
		$editpandetails = DB::table('banks')->where('id',$id)->first();
		return view('registerusers.editbankdetails',compact('editpandetails'));
	}
	public function details($id){
		$allplayers =DB::table('register_users')->where('register_users.id',$id)->get();
		
		$bank =DB::table('banks')->join('register_users','register_users.id','=','banks.userid')->where('register_users.id',$id)->where('banks.status','0')->where('register_users.pan_verify','1')->where('register_users.mobile_verify','1')->where('register_users.email_verify','1')->select('register_users.email','register_users.username','register_users.mobile_verify','register_users.email_verify','register_users.pan_verify','banks.*')->get();
		
		$pancard =DB::table('pan_cards')->join('register_users','register_users.id','=','pan_cards.userid')->where('register_users.id',$id)->where('pan_cards.status','0')->where('register_users.pan_verify','0')->where('register_users.mobile_verify','1')->where('register_users.email_verify','1')->select('register_users.email','register_users.username','register_users.mobile_verify','register_users.email_verify','pan_cards.*')->get();
		// echo '<pre>';
		// print_r($allplayers);
		// print_r($bank);
		// print_r($pancard);die;
		// return view('registerusers.details')->with(['allplayers',$getlist]);
		return view('registerusers.details',compact('allplayers','bank','pancard'));
    }

	public function adminwalletaction(){
		$allplayers = array();
		if(isset($_GET['searchuser'])){
			$query = DB::table('register_users');
			if(isset($_GET['email'])){
				$email=$_GET['email'];
				if($email!=""){
					$query->where('email', 'LIKE', '%'.$email.'%');
				}
			}
			if(isset($_GET['name'])){
				$name=$_GET['name'];
				if($name!=""){
					$query->where('team', 'LIKE', '%'.$name.'%');
				}
			}
			if(isset($_GET['id'])){
				$id=$_GET['id'];
				if($id!=""){
					$query->where('id', '=', $id);
				}
			}
			$allplayers = $query->orderBY('id','DESC')->paginate(20);
		}
		return view('registerusers.adminwalletaction',compact('allplayers'));
	}

	public function adminwalletinfo(){
		
		$adminwalletinfo = DB::table('adminwallets')->orderBy('id','DESC')->join('register_users','register_users.id','=','adminwallets.userid')->select('adminwallets.*','register_users.username','register_users.email')->get();
		return view('registerusers.adminwalletinfo',compact('adminwalletinfo'));
	}

	public function addmoneyinwallet(Request $request){
		if($request->isMethod('post')){
			$input = Input::all();
			$getuserid = $input['userid'];
			$finduserbalanace = DB::table('user_balances')->where('user_id',$getuserid)->first();
			$bonusbal = $finduserbalanace->bonus;
			$balancebal = $finduserbalanace->balance;
			$winningbal = $finduserbalanace->winning;
			unset($input['_token']);
			if(($input['bonustype']=='addfund') || ($input['bonustype']=='Unutilized')  || ($input['bonustype']=='promotional') || ($input['bonustype']=='refund_Unutilized')){
				$balancebal+=$input['amount'];
				$transactiondata['addfund_amt'] = $input['amount'];
			}
			else if(($input['bonustype']=='specialbonus') || ($input['bonustype']=='refund_Bonus')){
				$bonusbal+=$input['amount'];
				$transactiondata['bonus_amt'] = $input['amount'];
			}else{
				$winningbal+=$input['amount'];
				$transactiondata['win_amt'] = $input['amount'];
			}
			$update['bonus'] = $bonusbal;
			$update['balance'] = $balancebal;
			$update['winning'] = $winningbal;
			$nowtotalbal = $bonusbal+$balancebal+$winningbal;
			DB::table('user_balances')->where('user_id',$getuserid)->update($update);
			DB::table('adminwallets')->insert($input);
			//entry in transactions//
			$getlasttransactionid = DB::table('transactions')->select('id')->orderBy('id','DESC')->first();
			if(!empty($getlasttransactionid)){
				$tid = $getlasttransactionid->id+1;
			}else{
				$tid = 1;
			}
			if(($input['bonustype']=='addfund') || ($input['bonustype']=='Unutilized')){
				$transactiondata['type']= 'Add Fund';
			}
			else if($input['bonustype']=='specialbonus'){
				$transactiondata['type']= 'Add Bonus';
			}
			else if($input['bonustype']=='promotional'){
				$transactiondata['type']= 'Promotional Offering';
			}
			else if($input['bonustype']=='refund_Unutilized'){
				$transactiondata['type']= 'Challenge Joining Fee Refund';
			}
			else if($input['bonustype']=='refund_Winning'){
				$transactiondata['type']= 'Challenge Joining Fee Refund';
			}
			else if($input['bonustype']=='refund_Bonus'){
				$transactiondata['type']= 'Refund Bonus';
			}
			else if($input['bonustype']=='leaderboard_Winning'){
				$transactiondata['type']= 'Leaderboard Winning';
			}
			else{
				$transactiondata['type'] = 'Add Winning';
			}
			$transactiondata['amount'] = $input['amount'];
			$transactiondata['total_available_amt'] = $nowtotalbal;
			$transactiondata['transaction_id'] = 'S2W-'.$tid;
			$transactiondata['transaction_by'] = 'admin';
			$transactiondata['challengeid'] = "";
			$transactiondata['userid'] = $getuserid;
			$transactiondata['paymentstatus'] = 'confirmed';
			$transactiondata['bal_bonus_amt'] = $bonusbal;
			$transactiondata['bal_win_amt'] = $winningbal;
			$transactiondata['bal_fund_amt'] = $balancebal;
			DB::table('transactions')->insert($transactiondata);
			Session::flash('message', 'Money has been successfully transferred to user wallet');
			Session::flash('alert-class', 'alert-success');
			return Redirect::back();
		}
	}


	public function updateis_youtuber($id,$status){
		$id = unserialize(base64_decode($id));
		$input['id']=$id;
		$input['is_youtuber']=$status;
		// /print_r($input);die;
		DB::table('register_users')->where('id',$id)->update($input);
		Session::flash('message', 'User Youtuber Status successfully!');
		Session::flash('alert-class', 'alert-success');
		return Redirect::back();
	}

	public function updateyoutuber_bonus($id,$status){
		$id = unserialize(base64_decode($id));
		$input['id']=$id;
		$input['youtuber_bonus']=$status;
		DB::table('register_users')->where('id',$id)->update($input);
		Session::flash('message', 'User Youtuber Bonus Status successfully!');
		Session::flash('alert-class', 'alert-success');
		return Redirect::back();
	}

	public function updatebonus_percent($id){
		$id = unserialize(base64_decode($id));
		$input['id']=$id;
		$formData       = Input::all();
		$input['refercode']=$formData['refercode'];
		$input['bonus_percent']=$formData['bonus_percent'];
		$input['second_level_affiliation']=$formData['second_level_affiliation'];
		$input['pan_affiliation']=$formData['pan_affiliation'];
		$input['paytm_mobile']=$formData['paytm_mobile'];
		DB::table('register_users')->where('id',$id)->update($input);
		Session::flash('message', 'User Bonus Percent Status successfully!');
		Session::flash('alert-class', 'alert-success');
		return Redirect::back();
	}


}
?>