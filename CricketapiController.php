<?php
namespace App\Http\Controllers;
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Hash;
use Cache;
use Mail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class CricketapiController extends Controller {
	public function accessrules(){
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Credentials: true");
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Authorization');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	}
	public static function getaccesstoken(){
		date_default_timezone_set("Asia/Kolkata");
        
	$fields = array(
            'access_key' => '06abc5e67c043056cbcdcc07f8ae31d7',
            'secret_key' => '41e501fec95aa5f97e3d9b122116f146',
            'app_id' => 'com.fanadda',
            'device_id' => 'developer'
        );
	//L11
	/*$fields = array(
            'access_key' => '4c5da27aaf19d52c0cc400dde1d7425a',
            'secret_key' => '04ce46f17d5a166ba506b5e10b0cea6c',
            'app_id' => '1534849006100',
            'device_id' => 'developer'
        );*/
        /*RG labs API key
         *
         * $fields = array(
            'access_key' => '49b078c93034ab310ce096c161208c7c',
            // 'access_key' => '4c5da27aaf19d52c0cc400dde1d7425a',
            'secret_key' => 'e5d403eff1d74cfbd535c140e52dc532',
            // 'secret_key' => '04ce46f17d5a166ba506b5e10b0cea6c',
            // 'app_id' => '1534849006100',
            'app_id' => 'fav-11',
            'device_id' => 'developer'
        );*/
		$url = 'https://rest.cricketapi.com/rest/v2/auth/';
        $fields_string="";
		$todate = Carbon::now();
		$findtoken = DB::table('api_tokens')->whereDate('date','=',date('Y-m-d',strtotime($todate)))->first();
		if(!empty($findtoken)){
			$match_fields = array(
				'access_token' => $findtoken->token,
			);
		}
		else{
			foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
			$fields_string=rtrim($fields_string, '&');
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
// 			echo '<pre>'; print_r($result); die;
			$result_arrs = json_decode($result, true);
			$access_token = $result_arrs['auth']['access_token'];
			$match_fields = array(
				'access_token' => $access_token,
			);
			$matchtoken['token'] = $access_token;
			DB::table('api_tokens')->where('id',1)->update($matchtoken);
		}
		 return $match_fields;
		die;
	}
	public static function recentmatches(){
	   //	$data = array('name'=>"Our Code World");
        // $template_path = 'emails.resetpassword';
       // Mail::send($template_path, $data, function($message) {
          //  $message->to('sonali.img2@gmail.com', 'Receiver Name')->subject('Laravel HTML Mail');
          //  $message->from('aanchal.img@gmail.com','Our Code World');
        // });

      //  return "Basic email sent, check your inbox.";
      //  die;
	    $match_fields = CricketapiController::getaccesstoken();
		$access_token = $match_fields['access_token'];
	    $rmatch_url="https://rest.cricketapi.com/rest/v4/recent_matches/?access_token=$access_token";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $rmatch_url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch,CURLOPT_ENCODING , "gzip");
		// Execute Match request
		$rmatch_result = curl_exec($ch);
// 	    echo "<pre>";
// 		print_r($rmatch_result);

		curl_close($ch);
//  		$rmatch_arrs = gzinflate(substr($rmatch_result,10));
		$rmatch_arrs = json_decode($rmatch_result, true);
// echo "<pre>";
// print_r($rmatch_arrs);
// die;
		return $rmatch_arrs['data']['cards'];
	}
	public static function responseget(){
		$match_fields = CricketapiController::getaccesstoken();
		$access_token = $match_fields['access_token'];
	    $rmatch_url="https://rest.cricketapi.com/rest/v2/schedule/?access_token=$access_token";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $rmatch_url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
		// Execute Match request
		$rmatch_result = curl_exec($ch);
		curl_close($ch);

		$rmatch_arrs = gzinflate(substr($rmatch_result,10));
		$rmatch_arrs = json_decode($rmatch_arrs, true);
		// echo "<pre>";
		// print_r($rmatch_arrs);die;
		return $rmatch_arrs['data']['cards'];
	}
	public static function getscedulematches(){
	    $match_fields = CricketapiController::getaccesstoken();
		$access_token = $match_fields['access_token'];
	    $rmatch_url="https://rest.cricketapi.com/rest/v2/schedule/?access_token=".$access_token."&date=2021-07";  
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $rmatch_url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
		// Execute Match request
		$rmatch_result = curl_exec($ch);
		// echo '<pre>'; print_r($rmatch_result); die;
		curl_close($ch);
		/*print_r($rmatch_result);
		die;*/
		$rmatch_arrs = gzinflate(substr($rmatch_result,10));
		$rmatch_arrs = json_decode($rmatch_arrs, true);
		// echo '<pre>'; print_r($rmatch_arrs); die;
		return $rmatch_arrs['data']['months'][0]['days'];
	}
	/*
	public static function getmatchdetails($match_key){
	    // $match_fields = CricketapiController::getaccesstoken();
		$match_url = 'http://api.rglabs.net/api/cricket/getmatchdetails/'.$match_key;

	    $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $match_url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch,CURLOPT_ENCODING , "gzip");
		// Execute Match request
		$match_result = curl_exec($ch);
		curl_close($ch);
// 		$rmatch_arrs = gzinflate(substr($match_result,10));
// 		echo '<pre>'; print_r($rmatch_arrs);
		$match_arrs = json_decode($match_result, true);
		// print_r($match_arrs); die;
	    return $match_arrs ;
	}
	*/

	public static function getmatchdetailsV2($match_key){
	    return Cache::remember('cricket_matchdetails_v2_'.$match_key, 3, function () use ($match_key) {
            $match_fields = CricketapiController::getaccesstoken();
            $match_url = 'https://rest.cricketapi.com/rest/v2/match/' . $match_key . '/?' . http_build_query($match_fields) . '&card_type=full_card';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $match_url);
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_ENCODING, "gzip");
            // Execute Match request
            $match_result = curl_exec($ch);
            // 		$rmatch_arrs = gzinflate(substr($match_result,10));
            // 		echo '<pre>'; print_r($rmatch_arrs);
            $match_arrs = json_decode($match_result, true);
            // 		print_r($match_arrs); die;
            curl_close($ch);
            return $match_arrs;
            });
		}

	public static function forfull_data($match_key){
		$match_fields = CricketapiController::getaccesstoken();
		$match_url = 'https://rest.cricketapi.com/rest/v2/match/'.$match_key .'/?' . http_build_query($match_fields).'&card_type=full_card';
	    $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $match_url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
		// Execute Match request
		$match_result = curl_exec($ch);
		$match_arrs = gzinflate(substr($match_result, 10));
		$match_arrs = json_decode($match_arrs, true);
		if(!empty($match_arrs)){
		    if(isset($match_arrs['data']['card']['players'])){
		        return $match_arrs['data']['card']['players'] ;
		    }
		}

	}
	public static function getplayerinfo($playerkey){
		$match_fields = CricketapiController::getaccesstoken();
		$access_token = $match_fields['access_token'];
		$match_url = 'https://rest.cricketapi.com/rest/v2/player/'.$playerkey .'/league/icc/stats/?access_token='.$access_token;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $match_url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );

		// Execute Match request
		$match_result = curl_exec($ch);
		$match_arrs = gzinflate(substr($match_result, 10));
		$match_arrs = json_decode($match_arrs, true);
		return $match_arrs['data']['player'];
	}
	public static function recentseasons(){
	    $match_fields = CricketapiController::getaccesstoken();
		$access_token = $match_fields['access_token'];
		$match_url = 'https://rest.cricketapi.com/rest/v2/recent_seasons/?access_token='.$access_token;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $match_url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );

		// Execute Match request
		$match_result = curl_exec($ch);
		$match_arrs = gzinflate(substr($match_result, 10));
		$match_arrs = json_decode($match_arrs, true);
		return $match_arrs;
	}
    public static function seasonmatches($sesaonkey){
	    $match_fields = CricketapiController::getaccesstoken();
		$access_token = $match_fields['access_token'];
		$match_url = 'https://rest.cricketapi.com/rest/v4/season/'.$sesaonkey.'/recent_matches/?access_token='.$access_token;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $match_url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );

		// Execute Match request
		$match_result = curl_exec($ch);
		$match_arrs = gzinflate(substr($match_result, 10));
		$match_arrs = json_decode($match_arrs, true);
		//print_r($match_arrs); exit;
		return $match_arrs;
	}

	public static function overByOver($matchkey){
	    $match_fields = CricketapiController::getaccesstoken();
		$access_token = $match_fields['access_token'];
		$match_url = 'https://rest.cricketapi.com/rest/v2/match/'.$matchkey.'/overs_summary/?access_token='.$access_token; //echo $match_url; exit;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $match_url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );

		// Execute Match request
		$match_result = curl_exec($ch); //echo $match_result; exit;
		$match_arrs = gzinflate(substr($match_result, 10));
		$match_arrs = json_decode($match_arrs, true);
		return $match_arrs;
	}

	public static function getmatchcredit($matchkey){
	    $match_fields = CricketapiController::getaccesstoken();
		$access_token = $match_fields['access_token'];
		$match_url = 'https://rest.cricketapi.com/rest/v3/fantasy-match-credits/'.$matchkey.'/?access_token='.$access_token; //echo $match_url; exit;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $match_url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );

		// Execute Match request
		$match_result = curl_exec($ch);
		$match_arrs = gzinflate(substr($match_result, 10)); //echo $match_arrs; exit;
		$match_arrs = json_decode($match_arrs, true);
		// print_r($match_arrs); exit;
		return $match_arrs;
	}

    public static function get_coverage_v4() {
        //return Cache::remember('get_coverage_v4', 1440, function() {
            $match_fields = CricketapiController::getaccesstoken();
            $access_token = $match_fields['access_token'];
            $rmatch_url="https://rest.cricketapi.com/rest/v4/coverage/?access_token=$access_token";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $rmatch_url);
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt($ch,CURLOPT_ENCODING , "gzip");
            // Execute Match request
            $rmatch_result = curl_exec($ch);
            curl_close($ch);
            return json_decode($rmatch_result,true);
            //print_r($rmatch_result); exit;
       // });

    }
    /*
    public static function get_coverage_v4() {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.rglabs.net/api/cricket/get_coverage_v4",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }
    */

    public static function get_seasons_v4($board_key) {
        //return Cache::remember('get_season_v4', 100, function() use ($board_key) {
            $match_fields = CricketapiController::getaccesstoken();
            $access_token = $match_fields['access_token'];
            $rmatch_url = "https://rest.cricketapi.com/rest/v4/board/" . $board_key . "/schedule/?access_token=$access_token";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $rmatch_url);
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_ENCODING, "gzip");
            // Execute Match request
            $rmatch_result = curl_exec($ch);
            curl_close($ch);
            return json_decode($rmatch_result, true);
            //print_r($rmatch_result); exit;
        //});
    }
/*
    public static function get_seasons_v4($board_key) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.rglabs.net/api/cricket/get_seasons_v4/".$board_key,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }
*/

    public static function getmatchdetails($match_key) {
        return Cache::remember('cricket_matchdetails_'.$match_key, 3, function () use ($match_key) {
            $match_fields = CricketapiController::getaccesstoken();
			print_r($match_fields);
            $match_url = 'https://rest.cricketapi.com/rest/v4/match/' . $match_key . '/?' . http_build_query($match_fields) . '&card_type=metric_101';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $match_url);
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_ENCODING, "gzip");
            // Execute Match request
            $match_result = curl_exec($ch);
            curl_close($ch);
            // if($match_key=='c.match.ins_vs_stockholm_mumbai_indians.69309') {
            // echo '123';print_r($match_result); exit;
            // }
//    $rmatch_arrs = gzinflate(substr($match_result,10));
//    echo '<pre>'; print_r($rmatch_arrs);
            // $match_arrs = json_decode($match_result, true);
            // print_r($match_arrs); die;
            return json_decode($match_result, true);
        });
    }
/*
    public static function getmatchdetails($match_key){
        // $match_fields = CricketapiController::getaccesstoken();
        $match_url = 'http://api.rglabs.net/api/cricket/getmatchdetails/'.$match_key;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $match_url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch,CURLOPT_ENCODING , "gzip");
        // Execute Match request
        $match_result = curl_exec($ch);
        curl_close($ch);
//    $rmatch_arrs = gzinflate(substr($match_result,10));
//    echo '<pre>'; print_r($rmatch_arrs);
        $match_arrs = json_decode($match_result, true);
        // print_r($match_arrs); die;
        return $match_arrs ;
    }
*/
}
?>
