<?php
namespace App\Http\Controllers;
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Hash;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
class AnalyticsController extends Controller {
	
	public function profitloss(){
		$query = DB::table('joined_leagues');
		$getlist = array();
		if(isset($_GET['start_date'])){
			$start_date = $_GET['start_date'];
			$start_date = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($_GET['start_date'])));
			if($start_date!=""){
				$query->whereDate('joined_leagues.created_at', '>=',date('Y-m-d h:i:s',strtotime($start_date)));
			}
		}
		if(isset($_GET['end_date'])){
			$end_date = $_GET['end_date'];
			if($end_date!=""){
				$query->whereDate('joined_leagues.created_at', '<=',date('Y-m-d h:i:s',strtotime($end_date)));
			}
		}
		if(isset($_GET['matchkey'])){
			$matchkey = $_GET['matchkey'];
			if($matchkey!=""){
				$query->where('joined_leagues.matchkey',$matchkey);
			}
		}
		if((isset($_GET['start_date'])) || (isset($_GET['end_date']))){
			$getlist = $query->where('list_matches.final_status','winnerdeclared')->where(function($query) {
							$query->where('match_challenges.maximum_user','=',DB::raw('match_challenges.joinedusers'))->orWhere('match_challenges.confirmed_challenge',1);	
						})->where('match_challenges.win_amount','!=',0)->where('match_challenges.challenge_type','!=','%')->join('match_challenges','match_challenges.id','=','joined_leagues.challengeid')->join('list_matches','list_matches.matchkey','=','joined_leagues.matchkey')->orderBY('joined_leagues.id','DESC')->select('joined_leagues.matchkey','joined_leagues.challengeid','match_challenges.joinedusers','match_challenges.win_amount','match_challenges.entryfee')->groupBy('joined_leagues.challengeid')->get(); 
					
		}
		return view('analytics.profitloss',compact('getlist'));
	}
	
	public function paymnetInOut(){
	    return view('analytics.payment'); 
	}


	public function payment_in() {
		
           //////////////////////////
		$query= DB::table('transactions');
        $query->join('register_users','transactions.userid','=','register_users.id');
        // $query->where('transactions.type','Add Fund'); 
        $query->where(function($q){
		$q->where('transactions.transaction_by','razorPay')
		->orWhere('transactions.transaction_by','web_easebuzz')
		->orWhere('transactions.transaction_by','cashfree');
		});
        $query->where('transactions.paymentstatus','confirmed');

		$getlist = array();     
        if(isset($_GET['start_date'])){
            $start_date = $_GET['start_date'];
            $start_date = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($_GET['start_date'])));
            if($start_date!=""){
                $query->whereDate('created', '>=',date('Y-m-d h:i:s',strtotime($start_date)));
            }
        }
        if(isset($_GET['end_date'])){
            $end_date = $_GET['end_date'];
            if($end_date!=""){
                $query->whereDate('created', '<=',date('Y-m-d h:i:s',strtotime($end_date)));
            }
        }
        if(isset($_GET['user_id'])){
            $user_id = $_GET['user_id'];
            if($user_id!=""){
                $query->where('userid',$user_id);
            }
        }
        $details = $query->orderBy('created','desc')->select('transactions.*','register_users.email')->paginate(10);

        //Casyh Free
        $cashquery= DB::table('transactions');
        if(isset($_GET['start_date'])){
            $start_date = $_GET['start_date'];
            $start_date = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($_GET['start_date'])));
            if($start_date!=""){
                $cashquery->whereDate('created', '>=',date('Y-m-d h:i:s',strtotime($start_date)));
            }
        }
        if(isset($_GET['end_date'])){
            $end_date = $_GET['end_date'];
            if($end_date!=""){
                $cashquery->whereDate('created', '<=',date('Y-m-d h:i:s',strtotime($end_date)));
            }
        }
        if(isset($_GET['user_id'])){
            $user_id = $_GET['user_id'];
            if($user_id!=""){
                $cashquery->where('userid',$user_id);
            }
        }
        // print_r($cashquery->get());die;

        // $cashquery->where('transactions.type','Add Fund');
		$cashquery->where(function($q){
		$q->where('transactions.transaction_by','razorPay')
		->orWhere('transactions.transaction_by','cashfree')
		->orWhere('transactions.transaction_by','web_easebuzz');  
		});
		$cashquery->where('transactions.paymentstatus','confirmed');
        $cashdetails = $cashquery->orderBy('created','desc')->get();
        ///
        $damount=0;
       // print_r($cashdetails);die;
        foreach($cashdetails as $fmatch){            	
            		$damount+=$fmatch->amount;                
            }
            if (isset($damount)) {
            	$gr = "₹ ".$damount;
            }
           // print_r($gr);die;
		return view('analytics.payment_in',compact('details','gr'));
	}


	//Paytem Total Amount
	public function cashfree_total(){
	    ini_set('memory_limit', '-1');
		$query= DB::table('transactions');
		 if(isset($_GET['start_date'])){
			$start_date = $_GET['start_date'];
			$start_date = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($_GET['start_date'])));
			if($start_date!=""){
				$query->whereDate('created', '>=',date('Y-m-d h:i:s',strtotime($start_date)));
			}
		}
		if(isset($_GET['end_date'])){
			$end_date = $_GET['end_date'];
			if($end_date!=""){
				$query->whereDate('created', '<=',date('Y-m-d h:i:s',strtotime($end_date)));
			}
		}
		if(isset($_GET['user_id'])){
			$user_id = $_GET['user_id'];
			if($user_id!=""){
				$query->where('userid',$user_id);
			}
		}
		
		$query->where(function($q){
		$q->where('transactions.transaction_by','razorPay')
		->orWhere('transactions.transaction_by','web_easebuzz')
		->orWhere('transactions.transaction_by','cashfree');
		});

		$query->where('transactions.paymentstatus','confirmed');
	    $details = $query->orderBy('created','desc')->get(); 
	    
	    $i=1;
        $JsonFinal=array();
        if(!empty($details))
        {
            $camount = 0;
					$damount = 0;
					$gr = 0;
					$tf = 0;
			foreach($details as $fmatch){
            	
            		$damount+=$fmatch->amount;
                
            }
            if (isset($damount)) {
            	$gr = "₹ ".$damount;
            } 
        }
        echo $gr;  //.'((*#))'.$tf;
        die;
	}
	

	public function payment_paytm_in() {
		return view('analytics.payment_paytm_in'); 
	}

	public function payment_out() {
		return view('analytics.payment_out'); 
	}

	public function paymnetInOut_table(){
	    ini_set('memory_limit', '-1');
		$query= DB::table('transactions');
	    $getlist = array();
	    if(isset($_GET['start_date'])){
			$start_date = $_GET['start_date'];
			$start_date = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($_GET['start_date'])));
			if($start_date!=""){
				$query->whereDate('created', '>=',date('Y-m-d h:i:s',strtotime($start_date)));
			}
		}
		if(isset($_GET['end_date'])){
			$end_date = $_GET['end_date'];
			if($end_date!=""){
				$query->whereDate('created', '<=',date('Y-m-d h:i:s',strtotime($end_date)));
			}
		}
		if(isset($_GET['user_id'])){
			$user_id = $_GET['user_id'];
			if($user_id!=""){
				$query->where('userid',$user_id);
			}
		}
	    $details = $query->orderBy('created','desc')->get();
	    $i=1;
        $JsonFinal=array();
        if(!empty($details))
        {
					
			foreach($details as $fmatch){  $a=0; $b=0; $c=0;
				$camount = 0;
				$damount = 0;
				$oij= '₹ '. 0;
            	if($fmatch->cons_amount != 0 || $fmatch->cons_win != 0 || $fmatch->cons_bonus != 0){  $dfg = '₹ '.$fmatch->amount;   }  else{ $dfg = '-'; }
				if($fmatch->cons_amount != 0 || $fmatch->cons_win != 0 || $fmatch->cons_bonus != 0){ $jhgfd = '-'; }  else{ $jhgfd = '₹ '.$fmatch->amount; }
				if($fmatch->addfund_amt != 0){ $ytyh = '₹ '.$fmatch->addfund_amt; $a=$fmatch->addfund_amt; } else{ $ytyh = '₹ '.$fmatch->cons_amount; $a=$fmatch->cons_amount; }
				if($fmatch->win_amt != 0){ $erfg = '₹ '.$fmatch->win_amt; $b=$fmatch->win_amt; } else{ $erfg = '₹ '.$fmatch->cons_win; $b=$fmatch->cons_win; }
				if($fmatch->bonus_amt != 0){ $iuh = '₹ '.$fmatch->bonus_amt; $c=$fmatch->bonus_amt; } else{ $iuh = '₹ '.$fmatch->cons_bonus; $c=$fmatch->cons_bonus; }
                if($a != '' || $b !='' || $c !=''){ $oij = '₹ '.($a+$b+$c); }
                $gr = "₹ ".$damount;
                $tf = "₹ ".$camount;
                $data=array(
                    $fmatch->userid,
                    date('d M,Y',strtotime($fmatch->created)),
                    $fmatch->transaction_by,
                    $fmatch->transaction_id,
                    $jhgfd,
                    $dfg,
                    $ytyh,
                    $erfg,
                    $iuh,
                    $oij,
                    
                ); 
                $i++;
                $JsonFinal[]=$data;
            }
        }
        
        $jsonFinal1 = json_encode(array('data' => $JsonFinal));
        echo $jsonFinal1;
        die;

	}
     
    //Cashfree Data 
	public function payment_in_data(){
	    ini_set('memory_limit', '-1');
		$query= DB::table('transactions');
		$query->join('register_users','transactions.userid','=','register_users.id');
		// $query->where('transactions.type','Add Fund');
		$query->where('transactions.transaction_by','razorpay');
		$query->orWhere('transactions.transaction_by','web_easebuzz');
		$query->orWhere('transactions.transaction_by','cashfree');
		$query->where('transactions.paymentstatus','confirmed');
	    $getlist = array();
	    if(isset($_GET['start_date'])){
			$start_date = $_GET['start_date'];

			$start_date = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($_GET['start_date'])));
			if($start_date!=""){
				$query->whereDate('created', '>=',date('Y-m-d h:i:s',strtotime($start_date)));
			}
		}
		if(isset($_GET['end_date'])){
			$end_date = $_GET['end_date'];
			if($end_date!=""){
				$query->whereDate('created', '<=',date('Y-m-d h:i:s',strtotime($end_date)));
			}
		}
		if(isset($_GET['user_id'])){
			$user_id = $_GET['user_id'];
			if($user_id!=""){
				$query->where('userid',$user_id);
			}
		}
	    $details = $query->orderBy('created','desc')->select('transactions.*','register_users.email')->get(); 
	    $i=1;
        $JsonFinal=array();
        if(!empty($details))
        {
					
			foreach($details as $fmatch){  $a=0; $b=0; $c=0;
				$camount = 0;
				$damount = 0;
				$oij= '₹ '. 0;
            	if($fmatch->cons_amount != 0 || $fmatch->cons_win != 0 || $fmatch->cons_bonus != 0){  $dfg = '₹ '.$fmatch->amount;   }  else{ $dfg = '-'; }
				if($fmatch->cons_amount != 0 || $fmatch->cons_win != 0 || $fmatch->cons_bonus != 0){ $jhgfd = '-'; }  else{ $jhgfd = '₹ '.$fmatch->amount; }
				if($fmatch->addfund_amt != 0){ $ytyh = '₹ '.$fmatch->addfund_amt; $a=$fmatch->addfund_amt; } else{ $ytyh = '₹ '.$fmatch->cons_amount; $a=$fmatch->cons_amount; }
				if($fmatch->win_amt != 0){ $erfg = '₹ '.$fmatch->win_amt; $b=$fmatch->win_amt; } else{ $erfg = '₹ '.$fmatch->cons_win; $b=$fmatch->cons_win; }
				if($fmatch->bonus_amt != 0){ $iuh = '₹ '.$fmatch->bonus_amt; $c=$fmatch->bonus_amt; } else{ $iuh = '₹ '.$fmatch->cons_bonus; $c=$fmatch->cons_bonus; }
                if($a != '' || $b !='' || $c !=''){ $oij = '₹ '.($a+$b+$c); }
                $gr = "₹ ".$damount;
                $tf = "₹ ".$camount;
                $data=array(
                    $fmatch->userid,
                    $fmatch->email,
                    $fmatch->amount,
                    $fmatch->transaction_by,
                    // $fmatch->transaction_id,
                    date('d M,Y',strtotime($fmatch->created)),
                    // $jhgfd,
                    // $dfg,
                    // $ytyh,
                    // $erfg,
                    // $iuh,
                    // $oij,                    
                ); 
                $i++;
                $JsonFinal[]=$data;
            }
        }        
        $jsonFinal1 = json_encode(array('data' => $JsonFinal));
        echo $jsonFinal1;
        die;
	}
    
    //Paytem Data 
	public function payment_paytm_in_data(){
	    ini_set('memory_limit', '-1');
		$query= DB::table('transactions');
		$query->join('register_users','transactions.userid','=','register_users.id');
		$query->where('transactions.type','Add Fund');
		$query->where('transactions.transaction_by','paytm');
		$query->where('transactions.paymentstatus','confirmed');
	    $getlist = array();	    
	    if(isset($_GET['start_date'])){
			$start_date = $_GET['start_date'];
			$start_date = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($_GET['start_date'])));
			if($start_date!=""){
				$query->whereDate('created', '>=',date('Y-m-d h:i:s',strtotime($start_date)));
			}
		}
		if(isset($_GET['end_date'])){
			$end_date = $_GET['end_date'];
			if($end_date!=""){
				$query->whereDate('created', '<=',date('Y-m-d h:i:s',strtotime($end_date)));
			}
		}
		if(isset($_GET['user_id'])){
			$user_id = $_GET['user_id'];
			if($user_id!=""){
				$query->where('userid',$user_id);
			}
		}
	    $details = $query->orderBy('created','desc')->select('transactions.*','register_users.email')->get();

	    
	    $i=1;
        $JsonFinal=array();
        if(!empty($details))
        {
					
			foreach($details as $fmatch){  $a=0; $b=0; $c=0;
				$camount = 0;
				$damount = 0;
				$oij= '₹ '. 0;
            	if($fmatch->cons_amount != 0 || $fmatch->cons_win != 0 || $fmatch->cons_bonus != 0){  $dfg = '₹ '.$fmatch->amount;   }  else{ $dfg = '-'; }
				if($fmatch->cons_amount != 0 || $fmatch->cons_win != 0 || $fmatch->cons_bonus != 0){ $jhgfd = '-'; }  else{ $jhgfd = '₹ '.$fmatch->amount; }
				if($fmatch->addfund_amt != 0){ $ytyh = '₹ '.$fmatch->addfund_amt; $a=$fmatch->addfund_amt; } else{ $ytyh = '₹ '.$fmatch->cons_amount; $a=$fmatch->cons_amount; }
				if($fmatch->win_amt != 0){ $erfg = '₹ '.$fmatch->win_amt; $b=$fmatch->win_amt; } else{ $erfg = '₹ '.$fmatch->cons_win; $b=$fmatch->cons_win; }
				if($fmatch->bonus_amt != 0){ $iuh = '₹ '.$fmatch->bonus_amt; $c=$fmatch->bonus_amt; } else{ $iuh = '₹ '.$fmatch->cons_bonus; $c=$fmatch->cons_bonus; }
                if($a != '' || $b !='' || $c !=''){ $oij = '₹ '.($a+$b+$c); }
                $gr = "₹ ".$damount;
                $tf = "₹ ".$camount;
                $data=array(
                    $fmatch->userid,
                    $fmatch->email,
                    $fmatch->amount,
                    $fmatch->transaction_by,
                    date('d M,Y',strtotime($fmatch->created)),                   
                ); 
                $i++;
                $JsonFinal[]=$data;
            }
        }
        
        $jsonFinal1 = json_encode(array('data' => $JsonFinal));
        echo $jsonFinal1;
        die;

	}

	public function payment_out_data(){
	    ini_set('memory_limit', '-1');
		$query= DB::table('withdraws');
		$query->join('register_users','withdraws.user_id','=','register_users.id');
		// $query->where('transactions.type','Add Fund');
		$query->where('withdraws.status',1);
	    $getlist = array();
	    if(isset($_GET['start_date'])){
			$start_date = $_GET['start_date'];
			$start_date = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($_GET['start_date'])));
			if($start_date!=""){
				$query->whereDate('approved_date', '>=',date('Y-m-d h:i:s',strtotime($start_date)));
			}
		}
		if(isset($_GET['end_date'])){
			$end_date = $_GET['end_date'];
			if($end_date!=""){
				$query->whereDate('approved_date', '<=',date('Y-m-d h:i:s',strtotime($end_date)));
			}
		}
		if(isset($_GET['user_id'])){
			$user_id = $_GET['user_id'];
			if($user_id!=""){
				$query->where('userid',$user_id);
			}
		}
	    $details = $query->orderBy('approved_date','desc')->select('withdraws.*','register_users.email')->get();
	    $i=1;
        $JsonFinal=array();
        if(!empty($details))
        {   

			foreach($details as $fmatch){  $a=0; $b=0; $c=0;
				$camount = 0;
				$damount = 0;
				$oij= '₹ '. 0;
            
                $gr = "₹ ".$damount;
                $tf = "₹ ".$camount;
                $data=array(
                    $fmatch->user_id,
                    $fmatch->email,
                    $fmatch->amount,
					$fmatch->type,
                    $fmatch->comment,
                    // $fmatch->transaction_id,
                    date('d M,Y',strtotime($fmatch->approved_date)),
                    // $jhgfd,
                    // $dfg,
                    // $ytyh,
                    // $erfg,
                    // $iuh,
                    // $oij,                    
                ); 
                $i++;
                $JsonFinal[]=$data;
            }
        }        
        $jsonFinal1 = json_encode(array('data' => $JsonFinal));
        echo $jsonFinal1;
        die;

	}

	public function paymnetInOut_data(){
	    ini_set('memory_limit', '-1');
		$query= DB::table('withdraws');
		 if(isset($_GET['start_date'])){
			$start_date = $_GET['start_date'];
			$start_date = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($_GET['start_date'])));
			if($start_date!=""){
				$query->whereDate('approved_date', '>=',date('Y-m-d h:i:s',strtotime($start_date)));
			}
		}
		if(isset($_GET['end_date'])){
			$end_date = $_GET['end_date'];
			if($end_date!=""){
				$query->whereDate('approved_date', '<=',date('Y-m-d h:i:s',strtotime($end_date)));
			}
		}
		if(isset($_GET['user_id'])){
			$user_id = $_GET['user_id'];
			if($user_id!=""){
				$query->where('userid',$user_id);
			}
		}
		 $query->where('status',1);
	    $details = $query->orderBy('approved_date','desc')->get();
	    $i=1;
        $JsonFinal=array();
        if(!empty($details))
        {
            $camount = 0;
					$damount = 0;
					$gr = 0;
					$tf = 0;
			foreach($details as $fmatch){ $a=0; $b=0; $c=0;
            	$camount += $fmatch->amount;
            }
            $gr = "₹ ".$damount;
            $tf = "₹ ".$camount;            
        }
        echo $gr.'((*#))'.$tf;
        die;
	}
    
    //Paytem Total Amount
	public function paytem_total(){
	    ini_set('memory_limit', '-1');
		$query= DB::table('transactions');
		 if(isset($_GET['start_date'])){
			$start_date = $_GET['start_date'];
			$start_date = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($_GET['start_date'])));
			if($start_date!=""){
				$query->whereDate('created', '>=',date('Y-m-d h:i:s',strtotime($start_date)));
			}
		}
		if(isset($_GET['end_date'])){
			$end_date = $_GET['end_date'];
			if($end_date!=""){
				$query->whereDate('created', '<=',date('Y-m-d h:i:s',strtotime($end_date)));
			}
		}
		if(isset($_GET['user_id'])){
			$user_id = $_GET['user_id'];
			if($user_id!=""){
				$query->where('userid',$user_id);
			}
		}
		$query->where('transactions.transaction_by','paytm');
		$query->where('transactions.type','Add Fund');
		$query->where('transactions.paymentstatus','confirmed');
	    $details = $query->orderBy('created','desc')->get();
	    $i=1;
        $JsonFinal=array(); 
        if(!empty($details))
        {
            $camount = 0;
					$damount = 0;
					$gr = 0;
					$tf = 0;
			foreach($details as $fmatch){
				
            		$damount+=$fmatch->amount;
               	
            }
            $gr = "₹ ".$damount;
           // $tf = "₹ ".$camount;            
        }
        echo $gr;//.'((*#))'.$tf;
        die;
	}

	
	
	public function gstreport(){
		$query = DB::table('transactions');
		$getlist = array();
		$query->where('transactions.challengeid','<>',0);
		if(isset($_GET['start_date'])){
			$start_date = $_GET['start_date'];
			$start_date = date('Y-m-d H:i:s', strtotime('-1 day', strtotime($_GET['start_date'])));
			if($start_date!=""){
				$query->whereDate('transactions.created', '>=',date('Y-m-d h:i:s',strtotime($start_date)));
			}
		}
		if(isset($_GET['end_date'])){
			$end_date = $_GET['end_date'];
			if($end_date!=""){
				$query->whereDate('transactions.created', '<=',date('Y-m-d h:i:s',strtotime($end_date)));
			}
		}
		if(isset($_GET['start_date']) || isset($_GET['end_date'])){
			$getlist =$query->where(function ($query) {
				$query->where('list_matches.launch_status','launched')->where('list_matches.final_status','winnerdeclared');
			})->where(function($query) {
				$query->where('match_challenges.maximum_user','=',DB::raw('match_challenges.joinedusers'))->orWhere('match_challenges.confirmed_challenge',1);	
			})->where('match_challenges.win_amount','!=',0)->join('match_challenges','match_challenges.id','=','transactions.challengeid')->join('list_matches','list_matches.matchkey','=','match_challenges.matchkey')->join('register_users','register_users.id','=','transactions.userid')->orderBY('transactions.id','DESC')->select('transactions.challengeid','match_challenges.matchkey','list_matches.final_status','match_challenges.joinedusers','match_challenges.maximum_user','match_challenges.marathon','transactions.userid as userid','match_challenges.entryfee','match_challenges.win_amount','transactions.created as joineddate','transactions.transaction_id','register_users.state')->get();
		}
		// if(isset($_GET['start_date'])){
			// $start_date = $_GET['start_date'];
			// $start_date = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($_GET['start_date'])));
			// if($start_date!=""){
				// $query->whereDate('joined_leagues.created_at', '>=',date('Y-m-d h:i:s',strtotime($start_date)));
			// }
		// }
		// if(isset($_GET['end_date'])){
			// $end_date = $_GET['end_date'];
			// if($end_date!=""){
				// $query->whereDate('joined_leagues.created_at', '<=',date('Y-m-d h:i:s',strtotime($end_date)));
			// }
		// }
		// if(isset($_GET['start_date']) || isset($_GET['end_date'])){
			// $getlist = $query->where(function ($query) {
				// $query->where('list_matches.launch_status','launched')->where('list_matches.final_status','winnerdeclared');
			// })->where(function($query) {
				// $query->where('match_challenges.maximum_user','=',DB::raw('match_challenges.joinedusers'))->orWhere('match_challenges.confirmed_challenge',1);	
			// })->where('match_challenges.win_amount','!=',0)->join('transactions','')->join('match_challenges','match_challenges.id','=','joined_leagues.challengeid')->join('list_matches','list_matches.matchkey','=','joined_leagues.matchkey')->join('register_users','register_users.id','=','joined_leagues.userid')->orderBY('joined_leagues.id','DESC')->select('joined_leagues.*','match_challenges.*','register_users.state','joined_leagues.created_at as joineddate','list_matches.*')->get();
		// }
		return view('analytics.gstreport',compact('getlist'));
	}
	public function winningreport()
	{
		$query = DB::table('final_results');
		$getdata=array(); 
		if(isset($_GET['start_date'])){
			$start_date = $_GET['start_date'];
			$start_date = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($_GET['start_date'])));
			if($start_date!=""){
				$query->whereDate('final_results.created', '>=',date('Y-m-d h:i:s',strtotime($start_date)));
			}
		}
		if(isset($_GET['end_date'])){
			$end_date = $_GET['end_date'];
			if($end_date!=""){
				$query->whereDate('final_results.created', '<=',date('Y-m-d h:i:s',strtotime($end_date)));
			}
		}
		if(isset($_GET['team'])){
			$team = $_GET['team'];
			if($team!=""){
				$query->where('register_users.team','like','%'.$team.'%');
			}
		} 
		if(isset($_GET['start_date']) || isset($_GET['end_date']) || isset($_GET['team'])){
			$getdata = 	$query->join('register_users','register_users.id','=','final_results.userid')->select('final_results.userid','final_results.amount as win_amt','register_users.team','register_users.id')->where('final_results.amount','>',0)->get();
			
		}
		return view('analytics.winningreport',compact('getdata')); 
	}
	public function downloadgstreport(){
		
		$output1 = "";
		$output1 .= '"Date",';
		$output1 .= '"Challenge Id",';
		$output1 .= '"User Id",';
		$output1 .= '"Match Key",';
		$output1 .= '"Invoice No",';
		$output1 .= '"Max Allowed Users",';
		$output1 .= '"Joined Users",';
		$output1 .= '"Entry Fees",';
		$output1 .= '"Win Amount",';
		$output1 .= '"Pool Prize",';
		$output1 .= '"Platform Fee",';
		$output1 .= '"IGST",';
		$output1 .= '"CGST",';
		$output1 .= '"SGST",';
		$output1 .= '"User State",';
		$output1 .= "\n";
		$query = DB::table('transactions2');
		$getlist = array();
		$query->where('transactions2.challengeid','<>',0);
		if(isset($_GET['start_date'])){
			$start_date = $_GET['start_date'];
			$start_date = date('Y-m-d H:i:s', strtotime($_GET['start_date']));
			if($start_date!=""){
				$query->where('transactions2.created', '>=',date('Y-m-d h:i:s',strtotime($start_date)));
			}
		}
		if(isset($_GET['end_date'])){
			$end_date = $_GET['end_date'];
			$end_date = date('Y-m-d H:i:s', strtotime('+1 day', strtotime($_GET['end_date'])));
			if($end_date!=""){
				$query->where('transactions2.created', '<=',date('Y-m-d h:i:s',strtotime($end_date)));
			}
		}
		if(isset($_GET['start_date']) || isset($_GET['end_date'])){
			$getlist =$query->where(function ($query) {
				$query->where('list_matches.launch_status','launched')->where('list_matches.final_status','winnerdeclared');
			})->where(function($query) {
				$query->where('match_challenges.maximum_user','=',DB::raw('match_challenges.joinedusers'))->orWhere('match_challenges.confirmed_challenge',1);	
			})->where('match_challenges.win_amount','!=',0)->where('match_challenges.challenge_type','!=','%')->join('match_challenges','match_challenges.id','=','transactions2.challengeid')->join('list_matches','list_matches.matchkey','=','match_challenges.matchkey')->join('register_users','register_users.id','=','transactions2.userid')->orderBY('transactions2.id','DESC')->select('transactions2.challengeid','match_challenges.matchkey','list_matches.final_status','match_challenges.joinedusers','match_challenges.maximum_user','match_challenges.marathon','transactions2.userid as userid','match_challenges.entryfee','match_challenges.win_amount','transactions2.created as joineddate','transactions2.transaction_id','register_users.state')->get();
		}
		if(!empty($getlist)){
			$totalbonusget=0;
			$totalbalanceget=0;
			$totalwiningget=0;
			$totaligst=0;
			$totalcgst=0;
			$totalsgst=0;
			$i = count($getlist);
			foreach($getlist as $fmatch){
				$output1 .='"'.date('Y-m-d', strtotime($fmatch->joineddate)).'",';
				$output1 .='"'.ucwords($fmatch->challengeid).'",';
				$output1 .='"'.ucwords($fmatch->userid).'",';
				$output1 .='"'.ucwords($fmatch->matchkey).'",';
				$output1 .='"'.date('Y-m-d', strtotime($fmatch->joineddate)).'-'.$i.'",';
				$output1 .='"'.ucwords($fmatch->maximum_user).'",';
				$output1 .='"'.ucwords($fmatch->joinedusers).'",';
				$output1 .='"'.ucwords($fmatch->entryfee).'",';
				$output1 .='"'.ucwords($fmatch->win_amount).'",';
				$poolprize=0;
				$platformfee=0;
				$poolprize = $fmatch->win_amount/$fmatch->joinedusers;
				$platformfee = round((($fmatch->entryfee-$poolprize)/1.18),2);
				$igst=0;
				$cgst=0;
				$sgst=0;
				if($fmatch->state!='West Bengal' && $fmatch->state!='West Bengal'){
					 $igst = ($platformfee*18)/100;
					 if($igst<0){
						 $igst=0;
					 }
					 $igst =  round($igst,2);
					 $totaligst= $totaligst + $igst;
				}	
				if($fmatch->state=='West Bengal' || $fmatch->state=='West Bengal'){ 
					$cgst = ($platformfee*9)/100;
					if($cgst<0){
						 $cgst=0;
					 }
					$cgst =  round($cgst,2);
					$totalcgst = $totalcgst + $cgst;
				}
				if($fmatch->state=='West Bengal' || $fmatch->state=='West Bengal'){
					$sgst = ($platformfee*9)/100;
					if($sgst<0){
						 $sgst=0;
					 }
					$sgst =  round($sgst,2)	;
					$totalsgst = $totalsgst + $sgst;
				}
				$output1 .='"'.$poolprize.'",';
				$output1 .='"'.$platformfee.'",';
				$output1 .='"'.$igst.'",';
				$output1 .='"'.$cgst.'",';
				$output1 .='"'.$sgst.'",';
				$output1 .='"'.$fmatch->state.'",';
				$output1 .="\n";
				$i--;
			}

		}
		$filename =  "gst-report.csv";
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $output1;
		exit;
	}
	
	public function downloadgstreport2(){
		
		$output1 = "";
		$output1 .= '"Date",';
		$output1 .= '"Challenge Id",';
		$output1 .= '"User Id",';
		$output1 .= '"Match Key",';
		$output1 .= '"Invoice No",';
		$output1 .= '"Max Allowed Users",';
		$output1 .= '"Joined Users",';
		$output1 .= '"Entry Fees",';
		$output1 .= '"Win Amount",';
		$output1 .= '"Pool Prize",';
		$output1 .= '"Platform Fee",';
		$output1 .= '"IGST",';
		$output1 .= '"CGST",';
		$output1 .= '"SGST",';
		$output1 .= '"User State",';
		$output1 .= "\n";
		$query = DB::table('transactions');
		$getlist = array();
		$query->where('transactions.challengeid','<>',0);
		if(isset($_GET['start_date'])){
			$start_date = $_GET['start_date'];
			$start_date = date('Y-m-d H:i:s', strtotime($_GET['start_date']));
			if($start_date!=""){
				$query->where('transactions.created', '>=',date('Y-m-d h:i:s',strtotime($start_date)));
			}
		}
		if(isset($_GET['end_date'])){
			$end_date = $_GET['end_date'];
			$end_date = date('Y-m-d H:i:s', strtotime('+1 day', strtotime($_GET['end_date'])));
			if($end_date!=""){
				$query->where('transactions.created', '<=',date('Y-m-d h:i:s',strtotime($end_date)));
			}
		}
		if(isset($_GET['start_date']) || isset($_GET['end_date'])){
			$getlist =$query->where(function ($query) {
				$query->where('list_matches.launch_status','launched')->where('list_matches.final_status','winnerdeclared');
			})->where(function($query) {
				$query->where('match_challenges.maximum_user','=',DB::raw('match_challenges.joinedusers'))->orWhere('match_challenges.confirmed_challenge',1);	
			})->where('match_challenges.win_amount','!=',0)->where('match_challenges.challenge_type','!=','%')->where('match_challenges.maximum_user','<','10')->join('match_challenges','match_challenges.id','=','transactions.challengeid')->join('list_matches','list_matches.matchkey','=','match_challenges.matchkey')->join('register_users','register_users.id','=','transactions.userid')->orderBY('transactions.id','DESC')->select('transactions.challengeid','match_challenges.matchkey','list_matches.final_status','match_challenges.joinedusers','match_challenges.maximum_user','match_challenges.marathon','transactions.userid as userid','match_challenges.entryfee','match_challenges.bonus_percent','match_challenges.win_amount','transactions.created as joineddate','transactions.transaction_id','register_users.state')->get();
		}
		if(!empty($getlist)){
			$totalbonusget=0;
			$totalbalanceget=0;
			$totalwiningget=0;
			$totaligst=0;
			$totalcgst=0;
			$totalsgst=0;
			$i = count($getlist);
			foreach($getlist as $fmatch){
				$output1 .='"'.date('Y-m-d', strtotime($fmatch->joineddate)).'",';
				$output1 .='"'.ucwords($fmatch->challengeid).'",';
				$output1 .='"'.ucwords($fmatch->userid).'",';
				$output1 .='"'.ucwords($fmatch->matchkey).'",';
				$output1 .='"2020-21-09-'.$i.'",';
				$output1 .='"'.ucwords($fmatch->maximum_user).'",';
				$output1 .='"'.ucwords($fmatch->joinedusers).'",';
				$output1 .='"'.ucwords($fmatch->entryfee).'",';
				$output1 .='"'.ucwords($fmatch->win_amount).'",';
				$poolprize=0;
				$platformfee=0;
				$entryfee=$fmatch->entryfee-($fmatch->entryfee*$fmatch->bonus_percent/100);
				$poolprize = $fmatch->win_amount/$fmatch->joinedusers;
				$platformfee = round((($entryfee-$poolprize)/1.18),2);
				$igst=0;
				$cgst=0;
				$sgst=0;
				if($fmatch->state!='West Bengal' && $fmatch->state!='West Bengal'){
					 $igst = ($platformfee*18)/100;
					 if($igst<0){
						 $igst=0;
					 }
					 $igst =  round($igst,2);
					 $totaligst= $totaligst + $igst;
				}	
				if($fmatch->state=='West Bengal' || $fmatch->state=='West Bengal'){ 
					$cgst = ($platformfee*9)/100;
					if($cgst<0){
						 $cgst=0;
					 }
					$cgst =  round($cgst,2);
					$totalcgst = $totalcgst + $cgst;
				}
				if($fmatch->state=='West Bengal' || $fmatch->state=='West Bengal'){
					$sgst = ($platformfee*9)/100;
					if($sgst<0){
						 $sgst=0;
					 }
					$sgst =  round($sgst,2)	;
					$totalsgst = $totalsgst + $sgst;
				}
				$output1 .='"'.$poolprize.'",';
				$output1 .='"'.$platformfee.'",';
				$output1 .='"'.$igst.'",';
				$output1 .='"'.$cgst.'",';
				$output1 .='"'.$sgst.'",';
				$output1 .='"'.$fmatch->state.'",';
				$output1 .="\n";
				$i--;
			}

		}
		$filename =  "gst-report.csv";
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $output1;
		exit;
	}
}
?>