<?php 
namespace App\Http\Controllers;
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Helpers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
class LeaugeslistController extends Controller {

	public function all_leauges(){
 		$start = "2018-09-07";
 		$end = "2018-09-18";
		$data = DB::table('leagues_transactions')->whereBetween('created_at',[$start,$end])->where('winning','!=',0)->get();
foreach ($data as $value) {
	$uid= $value->user_id;
	$cid= $value->challengeid;
	$tdata=DB::table('transactions')->where('userid','=',$uid)->where('challengeid','=',$cid)->where('type','=','Refund amount')->first();
		//echo "<pre>"; print_r($data); die;
	$entr= DB::table('match_challenges')->where('id','=',$cid)->select('entryfee')->first();
if(!isset($entry->entryfee)){
	continue;
}
	$entry=0;$bonus=0;$ua=0;$win=0;
		$entry=$entr->entryfee;
		$bonus=$tdata->bonus_amt;
		$ua=$tdata->addfund_amt;
		$win=$tdata->win_amt;


		$updated_amt =0;
		$updated_amt = $entry - $bonus - $ua - $win;

		$win_amt = $win + $updated_amt;
		$b=DB::table('transactions')->where('userid','=',$uid)->where('challengeid','=',$cid)->where('type','=','Refund amount')->select('bal_win_amt','total_available_amt')->first();
		$t_w_amt=$b->bal_win_amt + $win_amt;
		$total_amount=$b->total_available_amt + $win_amt;
		DB::table('transactions')->where('userid','=',$uid)->where('challengeid','=',$cid)->where('type','=','Refund amount')->update(['win_amt' => $win_amt, 'bal_win_amt'=>$t_w_amt, 'total_available_amt'=>$total_amount]);




	$a= DB::table('user_balances')->where('user_id','=',$uid)->select('winning')->first();
		$win_a = $a->winning + $win_amt;

		DB::table('user_balances')->where('user_id','=',$uid)->update(['winning' => $win_a]);




		$c= DB::table('leagues_transactions')->where('user_id','=',$uid)->where('challengeid','=',$cid)->select('winning')->first();
		$win_c = $c->winning + $win_amt;

		DB::table('leagues_transactions')->where('user_id','=',$uid)->where('challengeid','=',$cid)->update(['winning' => $win_c]);

	
}
		return view('leauge.all_leauges',compact('data'));

	}

	public function editleauges($uid,$cid){
		$data=DB::table('transactions')->where('userid','=',$uid)->where('challengeid','=',$cid)->where('type','=','Refund amount')->first();
		//echo "<pre>"; print_r($data); die;
	$entry= DB::table('match_challenges')->where('id','=',$cid)->select('entryfee')->first();
		//$data=DB::table('transactions')->where('type','=','Refund amount')->get();
		
		return view('leauge.editleauges',compact('data','entry'));

	}

	public function edit(Request $request,$uid,$cid){
		$input = $request->all();
		$entry=0;$bonus=0;$ua=0;$win=0;
		$entry=$input['entryfee'];
		$bonus=$input['bonus_amt'];
		$ua=$input['addfund_amt'];
		$win=$input['win_amt'];
		unset($input['_token']);
		unset($input['entryfee']);
		$updated_amt =0;
		$updated_amt = $entry - $bonus - $ua - $win;

		$win_amt = $win + $updated_amt;
		$b=DB::table('transactions')->where('userid','=',$uid)->where('challengeid','=',$cid)->where('type','=','Refund amount')->select('bal_win_amt','total_available_amt')->first();
		$t_w_amt=$b->bal_win_amt + $win_amt;
		$total_amount=$b->total_available_amt + $win_amt;
		DB::table('transactions')->where('userid','=',$uid)->where('challengeid','=',$cid)->where('type','=','Refund amount')->update(['win_amt' => $win_amt, 'bal_win_amt'=>$t_w_amt, 'total_available_amt'=>$total_amount]);




	$a= DB::table('user_balances')->where('user_id','=',$uid)->select('winning')->first();
		$win_a = $a->winning + $win_amt;

		DB::table('user_balances')->where('user_id','=',$uid)->update(['winning' => $win_a]);




		$c= DB::table('leagues_transactions')->where('user_id','=',$uid)->where('challengeid','=',$cid)->select('winning')->first();
		$win_c = $c->winning + $win_amt;

		DB::table('leagues_transactions')->where('user_id','=',$uid)->where('challengeid','=',$cid)->update(['winning' => $win_c]);




	}
}