<?php
namespace App\Http\Controllers;
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Hash;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
class CricketapiController extends Controller {
	public function accessrules(){
		header('Access-Control-Allow-Origin: *'); 
		header("Access-Control-Allow-Credentials: true");
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Authorization');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	}
	public static function getaccesstoken(){
		date_default_timezone_set("Asia/Kolkata");
	
        // $fields = array(
        //     'access_key' => '6b79e0f61953b6a594c78b809ceadb87',
        //     'secret_key' => 'd90b5c534dd940bfbb045e5a4c9801a0',
        //     'app_id' => 'fanatasypower11960818',
        //     'device_id' => '0341098034349'
        // );
		
		$fields = array(
            'access_key' => '4c5da27aaf19d52c0cc400dde1d7425a',
            'secret_key' => '04ce46f17d5a166ba506b5e10b0cea6c',
            'app_id' => '1534849006100',
            'device_id' => '0341098034555'
        );
		
		$url = 'https://rest.cricketapi.com/rest/v2/auth/';
        $fields_string="";
		$todate = Carbon::now();
		$findtoken = DB::table('api_tokens')->whereDate('date','=',date('Y-m-d',strtotime($todate)))->first();
		
		if(!empty($findtoken)){
			$match_fields = array(
				'access_token' => $findtoken->token,
			);
		}
		else{
			foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
			$fields_string=rtrim($fields_string, '&');
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			$result_arrs = json_decode($result, true);
			$access_token = $result_arrs['auth']['access_token'];
			$match_fields = array(
				'access_token' => $access_token,
			);
			$matchtoken['token'] = $access_token;
			DB::table('api_tokens')->where('id',1)->update($matchtoken);
		}
		 return $match_fields;
		die;
	}
	public static function recentmatches(){
	    $match_fields = CricketapiController::getaccesstoken();
		$access_token = $match_fields['access_token'];
	    $rmatch_url="https://rest.cricketapi.com/rest/v2/recent_matches/?access_token=$access_token";
	    
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $rmatch_url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch,CURLOPT_ENCODING , "gzip");
		// Execute Match request
		$rmatch_result = curl_exec($ch);
	
		curl_close($ch);
// 		$rmatch_arrs = gzinflate(substr($rmatch_result,10));
		$rmatch_arrs = json_decode($rmatch_result, true);
// 		echo "<pre>";
// 		print_r($rmatch_arrs);die;
return $rmatch_arrs['data']['cards'];
	}
	public static function responseget(){
		$match_fields = CricketapiController::getaccesstoken();
		$access_token = $match_fields['access_token'];
	    $rmatch_url="https://rest.cricketapi.com/rest/v2/schedule/?access_token=$access_token";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $rmatch_url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
		// Execute Match request
		$rmatch_result = curl_exec($ch);
		curl_close($ch);
		echo "<pre>";
		print_r($rmatch_result);
		die;
		$rmatch_arrs = gzinflate(substr($rmatch_result,10));
		$rmatch_arrs = json_decode($rmatch_arrs, true);
		// echo "<pre>";
		// print_r($rmatch_arrs);die;
		return $rmatch_arrs['data']['cards'];
	} 
	public static function getscedulematches(){
	    $match_fields = CricketapiController::getaccesstoken();
		$access_token = $match_fields['access_token'];
	    $rmatch_url="https://rest.cricketapi.com/rest/v2/schedule/?access_token=".$access_token."&date=2020-06";
	    // echo $rmatch_url;exit;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $rmatch_url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
		// Execute Match request
		$rmatch_result = curl_exec($ch);
		curl_close($ch);
		$rmatch_arrs = gzinflate(substr($rmatch_result,10));
		$rmatch_arrs = json_decode($rmatch_arrs, true);
		// print_r($rmatch_arrs);die;
		return $rmatch_arrs['data']['months'][0]['days'];
	}
	public static function getmatchdetails($match_key){
	    $match_fields = CricketapiController::getaccesstoken();
		$match_url = 'https://rest.cricketapi.com/rest/v2/match/'.$match_key .'/?' . http_build_query($match_fields).'&card_type=full_card';
		
	    $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $match_url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch,CURLOPT_ENCODING , "gzip");
		// Execute Match request
		$match_result = curl_exec($ch);
// 		$rmatch_arrs = gzinflate(substr($match_result,10));
// 		echo '<pre>'; print_r($rmatch_arrs);
		$match_arrs = json_decode($match_result, true);
// 		print_r($match_arrs); die;
	    return $match_arrs ;
		curl_close($ch);
	}
	public static function forfull_data($match_key){
		$match_fields = CricketapiController::getaccesstoken();
		$match_url = 'https://rest.cricketapi.com/rest/v2/match/'.$match_key .'/?' . http_build_query($match_fields).'&card_type=full_card';
	    $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $match_url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
		// Execute Match request
		$match_result = curl_exec($ch);
		$match_arrs = gzinflate(substr($match_result, 10));
		$match_arrs = json_decode($match_arrs, true);
		if(!empty($match_arrs)){
		    if(isset($match_arrs['data']['card']['players'])){
		        return $match_arrs['data']['card']['players'] ;
		    }
		}
		
	} 
	public static function getplayerinfo($playerkey){
		$match_fields = CricketapiController::getaccesstoken();
		$access_token = $match_fields['access_token'];
		$match_url = 'https://rest.cricketapi.com/rest/v2/player/'.$playerkey .'/league/icc/stats/?access_token='.$access_token;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $match_url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch,CURLOPT_ENCODING , "gzip");
		// Execute Match request
		$match_result = curl_exec($ch);
		//$match_arrs = gzinflate(substr($match_result, 10));
		$match_arrs = json_decode($match_result, true);
		return $match_arrs['data']['player'];
	}
	public static function recentseasons(){
	    $match_fields = CricketapiController::getaccesstoken();
		$access_token = $match_fields['access_token'];
		$match_url = 'https://rest.cricketapi.com/rest/v2/recent_seasons/?access_token='.$access_token;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $match_url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );

		// Execute Match request
		$match_result = curl_exec($ch);
		$match_arrs = gzinflate(substr($match_result, 10));
		$match_arrs = json_decode($match_arrs, true);
		return $match_arrs;
	}
	public static function seasonmatches($sesaonkey){
	    $match_fields = CricketapiController::getaccesstoken();
		$access_token = $match_fields['access_token'];
		$match_url = 'https://rest.cricketapi.com/rest/v2/season/'.$sesaonkey.'/?access_token='.$access_token;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $match_url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );

		// Execute Match request
		$match_result = curl_exec($ch);
		$match_arrs = gzinflate(substr($match_result, 10));
		$match_arrs = json_decode($match_arrs, true);
		return $match_arrs;
	}
}
?>