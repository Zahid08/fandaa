<?php
namespace App\Http\Controllers;
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Helpers;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\CricketapiController;
class TeamsController extends Controller {
	// public function addseries(Request $request){
		// if ($request->isMethod('post')){
		// $rules = array(
            // 'name' => 'required|unique:series'
		// );
		// $validator = Validator::make(Input::all(), $rules);
		// if($validator->fails()){
				// return Redirect::back()
					// ->withErrors('This series is already exist.')
					// ->withInput(Input::except('password'));
        // } 
		// else{
           // $inputt = Input::all();
		   // $input = $request->input();
		   // unset($input['_token']);
		   // $input['start_date'] = date('Y-m-d h:i:s',strtotime($input['start_date']));
		   // $input['end_date'] = date('Y-m-d h:i:s',strtotime($input['end_date']));
		   // DB::table('series')->insert($input);
		   // Session::flash('message', 'Successfully added series!');
		   // Session::flash('alert-class', 'alert-success');
           // return Redirect::back();
		// }
	  // }
	  // return view('series.addseries');
	// }
	public function editteams($id,Request $request){
		$id = unserialize(base64_decode($id));
		$series = DB::table('teams')->where('id',$id)->first();
		if ($request->isMethod('post')){
			$input = Input::all();
			$catname = $input['team'];
			$findseries = DB::table('teams')->where('team',$catname)->where('id','!=',$id)->first();
			if(!empty($findseries)){
				return Redirect::back()
					->withErrors('This Team is already exist.');
					// ->withInput(Input::except('password'));
			}
			$data['team']= $input['team'];
			$data['team_key']= $input['team_key'];
			$data['short_name']= $input['short_name'];
			//$data['color']= $input['color'];
			$file = $input['image'];
			// echo '<pre>';print_r($input);die;
			if(isset($input['image'])){
				if(!empty($input['image'][0])){
					if(!empty($file)){
						// if(isset($input['image']['name'])){
							// echo 'hui';die;
						  $destinationPath = 'uploads/teams'; 
						  $fileName = 'Fanadda-team-'.str_replace(' ', '_', $series->team);
						  $imageName = Helpers::imageUpload($file,$destinationPath,$fileName);
						  if($series->logo!=""){
							 // if (!file_exists(dirname(dirname(dirname(dirname(__FILE__)))).$destinationPath.$series->logo)){
								// File::delete($destinationPath.'/'.$series->logo);
							 // }
						  }
						  $data['logo'] = $imageName;
						// }
					}
				}
			}
			$rowCOllection = DB::table('teams')->where('id',$id)->update($data);
			Session::flash('message', 'Team successfully updated !');
			Session::flash('alert-class', 'alert-success');
            return Redirect::back();
		}
		if(!empty($series)){
			return view('teams.editteams')->with('series', $series);
		}
		else{
			return redirect()->action('TeamsController@viewteams')->withErrors('Invalid Id Provided');
		}
	}
	public function viewteams(){
		$query = DB::table('teams');
		if(request()->has('name')){
			$name=request('name');
			if($name!=""){
				$query->where('team', 'LIKE', '%'.$name.'%');
			}
		}
		// if(request()->has('start_date')){
			// $start_date = request('start_date');
			// if($start_date!=""){
				// $query->whereDate('start_date', '>=',date('Y-m-d h:i:s',strtotime($start_date)));
			// }
		// }
		// if(request()->has('end_date')){
			// $end_date = request('end_date');
			// if($end_date!=""){
				// $query->whereDate('end_date', '<=',date('Y-m-d h:i:s',strtotime($end_date)));
			// }
		// }
		$getlist = $query->orderBY('id','DESC')->paginate(20);
		return view('teams.viewteams')->with('getlist', $getlist);
	}
	// public function deleteseries($id){
		// $id = unserialize(base64_decode($id));
		// $series = DB::table('series')->where('id',$id)->first();
		// if(!empty($series)){
			// $findmatchexist = DB::table('listmatches')->where('series',$series->id)->get();
			// if(!empty($findmatchexist)){
				// return Redirect::back()
					// ->withErrors('You cannot delete this series as match is added into this series');
			// }else{
				 // DB::table('series')->where('id',$id)->delete();
				 // Session::flash('message', 'Successfully deleted series!');
				// Session::flash('alert-class', 'alert-success');
			// }
		// }
		// else{
			// return redirect()->action('ServiceController@viewseries')->withErrors('Invalid Id Provided');
		// }
	// }
}
?>