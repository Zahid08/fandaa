<?php
namespace App\Http\Controllers;
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Helpers;
use Hash;
use File;
use Mail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
class ApiController extends Controller {
	public function geturl(){
		return 'https://test.fanadda.com/bbadmin/';
	}
	public function geturltoreturn(){
		return 'https://fanadda.com/balance';
	}
	
	function privacy(){
	    return view('privacy');
	}
	
	public function sendGridMail(){
	  require 'vendor/autoload.php';    
         $email = new \SendGrid\Mail\Mail(); 
          $email->setFrom("neeraj.img@gmail.com", "Neeraj Rajput");
         $email->setSubject("Sending with SendGrid is Fun");
         $email->addTo("rohit19.img@gmail.com", "Rohit Lalwani");
         
         $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
        //   $email->addContent("text/html", "<strong>and easy to do anywhere, even with PHP</strong>");
          $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
       

        	 try {
        		 $response = $sendgrid->send($email);
        		echo 'sent';
        	 } catch (Exception $e) {
        	  echo 'Caught exception: '. $e->getMessage() ."\n";
          }
          die;
	}
	
	public function getposters(){ exit;
		$this->accessrules();
		$findposters = DB::table('offers')->orderBY('id','DESC')->get();
		$Json=array();
		if(!empty($findposters)){
			$i=0;
			foreach($findposters as $post){
				$url = $this->geturl();
				$Json[$i]['image'] = $url.'uploads/posters/'.$post->image;
				$i++;
			}
		}
		echo json_encode($Json);
		die;
	}
	
	public function updateallwallets(){
		$findallusers = DB::table('register_users')->select('id')->get();
		if(!empty($findallusers)){
			foreach($findallusers as $fusers){
				$findwallet = DB::table('user_balances')->where('user_id',$fusers->id)->select('id')->first();
				if(empty($findwallet)){
					$dataupdate['user_id'] = $fusers->id;
					DB::table('user_balances')->insert($dataupdate);
				}
			}
		}
	}
	
	public function accessrules(){
		header('Access-Control-Allow-Origin: *'); 
		header("Access-Control-Allow-Credentials: true");
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Authorization');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
		date_default_timezone_set('Asia/Kolkata'); 
	}
	public function mywithrawlist(Request $request) 
	{
		$this->accessrules();
		$Json=array(); 
		$userid = $_GET['user_id'];
		$withdrawdata = DB::table('withdraws')->where('user_id',$userid)->get();
		if(!empty($withdrawdata)) {
			$i=0;
			foreach($withdrawdata as $val){
				$Json[$i]['id'] = $val->id;
				$Json[$i]['user_id'] = $val->user_id;
				$Json[$i]['withdrawl_date'] = date('d-M-y h:i:a',strtotime($val->created));	
				if($val->approved_date!='0000-00-00'){
					$Json[$i]['approved_date'] = $val->approved_date;	
				}else{
					$Json[$i]['approved_date'] = "";
				}
				if($val->status==0){
					$Json[$i]['status'] = 'Pending';
				}else{
					$Json[$i]['status'] = 'Approved';
				}
				if($val->comment==null){
					$Json[$i]['comment'] = "";
				}else{
					$Json[$i]['comment'] = $val->comment ;
				}
				$Json[$i]['amount'] = round($val->amount,2) ;

				$i++;
			}
			$JsonFinal[]=$Json;
			echo json_encode($JsonFinal);
			die;
		}else{
			$JsonFinal=array();
			echo json_encode($JsonFinal);
			die;
		}		
	}
	public function thistransaction(Request $request)
	{
		$this->accessrules();
		$Json=array();
		$id = $_GET['id'];
		$findlastow = DB::table('transactions')->where('id',$id)->select('id','userid','challengeid','created','amount')->first();
		if(!empty($findlastow)){
			$i=0;
			$matchchallenge = DB::table('match_challenges')->where('id',$findlastow->challengeid)->select('id','matchkey')->first();
			if(!empty($matchchallenge)) {
				$thismatch = DB::table('list_matches')->where('matchkey',$matchchallenge->matchkey)->select('id','matchkey','short_name','start_date')->first();
				if(!empty($thismatch)) {
					$userdata = DB::table('register_users')->where('id',$findlastow->userid)->select('id','username','team')->first();
					$Json[$i]['id'] = $findlastow->id;
					$Json[$i]['amount'] = round($findlastow->amount,2);
					$Json[$i]['username'] = $userdata->username;
					$Json[$i]['team'] = $userdata->team;
					$Json[$i]['tour'] = $thismatch->short_name;
					$Json[$i]['date'] = $thismatch->start_date;
					$Json[$i]['date'] = $thismatch->start_date;
					$Json[$i]['transaction_time'] = $findlastow->created;	
				}
			}
			echo json_encode($Json);
			die;
		}else{
			$JsonFinal=array();
			echo json_encode($Json);
			die;
		}


	}
	
		public function request_withdrow(Request $request){
			$this->accessrules();
			$user_id = $userid = $_GET['user_id'];
			$amount = $_GET['amount'];
			$data['user_id'] = $user_id;
			$data['amount'] = $amount;
			$wordlist = DB::table('withdraws')->get();
			$wordCount = count($wordlist)+1;
			$data['withdraw_request_id'] = 'WD-'.$user_id.'-'.$wordCount;
				$data['created'] = date('Y-m-d');
				$findverification = DB::table('register_users')->where('id',$user_id)->first();
				if(!empty($findverification)){
					if($findverification->mobile_verify!=1 || $findverification->email_verify!=1 || $findverification->pan_verify!=1 || $findverification->bank_verify!=1){
						$msgg['msg'] = "Please first complete your verification process.";
						$msgg['status'] = 3;
						$msgg['amount'] = 0;
						$msgg['wining'] = 0;
						echo json_encode(array($msgg));die;
					}
				}
				if($amount<200){
					$msgg['msg'] = "Withdrawl amount should be greater than 200";
					$msgg['status'] = 0;
					$msgg['amount'] = 0;
					$msgg['wining'] = 0;
					echo json_encode(array($msgg));die;
				}
				$bal_bonus_amt=0;$bal_win_amt=0;$bal_fund_amt=0;$total_available_amt=0;
				$findlastow = DB::table('user_balances')->where('user_id',$user_id)->first();
				if(!empty($findlastow)){
					// $balance = $findlastow->balance+$findlastow->winning;
					$balance = $findlastow->winning;
					if($balance >= $amount){
						$bal_fund_amt = $findlastow->balance;
						$bal_win_amt = $findlastow->winning;
						$bal_bonus_amt = $findlastow->bonus;
						$dataq['winning'] = $balance - $amount;
						DB::table('user_balances')->where('id',$findlastow->id)->update($dataq);
						DB::table('withdraws')->insert($data);
						$total_available_amt = $findlastow->balance+$dataq['winning']+$findlastow->bonus;
						$notificationdata['userid'] = $userid;
						$notificationdata['title'] = 'Request For Withdraw amount rs. '.$amount;
						DB::table('notifications')->insert($notificationdata);
						//push notifications//
						$titleget = 'Withdraw Request!';
						// Helpers::sendnotification($titleget,$notificationdata['title'],'',$userid);
						//end push notifications//
						$transactionsdata['userid'] = $userid;
						$transactionsdata['type'] = 'Amount Withdrawn';
						$transactionsdata['transaction_id'] = $data['withdraw_request_id'];
						$transactionsdata['transaction_by'] = 'wallet';
						$transactionsdata['amount'] = $amount;
						$transactionsdata['paymentstatus'] = 'pending';
						$transactionsdata['withdraw_amt'] = $amount;
						
						$transactionsdata['bal_fund_amt'] = $bal_fund_amt;
						$transactionsdata['bal_win_amt'] = $dataq['winning'];
						$transactionsdata['bal_bonus_amt'] = $bal_bonus_amt;
						$transactionsdata['cons_win'] = $amount;
						$transactionsdata['total_available_amt'] = $total_available_amt;
						DB::table('transactions')->insert($transactionsdata);
						
						$msgg['msg'] = "Your request for  withdrawl amount of Rs ".$amount." is sent successfully. You will  get info about it in between 24 to 48 Hours.";
						$msgg['status'] = 1;
						$msgg['amount'] = $total_available_amt-$amount;
						$msgg['wining'] = $bal_win_amt-$amount;
						echo json_encode(array($msgg));die;
					}
					else{
						$msgg['msg'] = "You can withdraw only ".$balance." rupees.";
						$msgg['status'] = 0;
						$msgg['amount'] = 0;
						$msgg['wining'] = 0;
						echo json_encode(array($msgg));die;
					}
				}else{
					$msgg['msg'] = "Invalid user id.";
					$msgg['status'] = 0;
					echo json_encode(array($msgg));die;
				}
				
		}
	
	public function allverify(Request $request)
	{
			$this->accessrules();
			$id = $_GET['id'];

			$userdata = DB::table('register_users')->where('id',$id)->first();

			$msgg['mobile_verify'] = $userdata->mobile_verify;
			$msgg['email_verify'] = $userdata->email_verify;
			$msgg['bank_verify'] = $userdata->bank_verify;			
			$msgg['pan_verify'] = $userdata->pan_verify;

			if($msgg['pan_verify']==0) {
				$msgg['pan_data'] = array();
				$pan_data = DB::table('pancard')->where('userid', $id)->first();
				if(!empty($pan_data)) {
					$msgg['pan_data']['pan_holder_name'] = $pan_data->pan_name;
					$msgg['pan_data']['pan_number'] = $pan_data->pan_number;
					$msgg['pan_data']['pan_dob'] = $pan_data->pan_dob;
					$msgg['pan_data']['pan_image'] = $pan_data->image;
				}
			}

			if($msgg['bank_verify']==0) {
				$msgg['bank_data'] = array();
				$bank_data = DB::table('bank')->where('userid', $id)->first();
				if(!empty($bank_data)) {
					$msgg['bank_data']['account_number'] = $bank_data->accno;
					$msgg['bank_data']['ifsc'] = $bank_data->ifsc;
					$msgg['bank_data']['bankname'] = $bank_data->bankname;
					$msgg['bank_data']['bank_branch'] = $bank_data->bankbranch;
					$msgg['bank_data']['state'] = $bank_data->state;
					$msgg['bank_data']['bank_image'] = $bank_data->image;
				}
			}  
			if($msgg['pan_verify']==2){
				$findreason = DB::table('pancard')->where('userid',$_GET['id'])->select('comment')->first();
				$msgg['pan_comment'] = $findreason->comment;
			}
			if($msgg['bank_verify']==2){
				$findreason = DB::table('bank')->where('userid',$_GET['id'])->select('comment')->first();
				$msgg['bank_comment'] = $findreason->comment;
			}
			echo json_encode(array($msgg));die;
	}
	public function seepandetails(){
		$this->accessrules();
		$id = $_GET['id'];
		$JSON = array();
		$pancarddetails = DB::table('pancard')->where('userid',$_GET['id'])->first();
		if(!empty($pancarddetails)){
			$JSON[0]['panname'] = strtoupper($pancarddetails->pan_name);
			$JSON[0]['pannumber'] = strtoupper($pancarddetails->pan_number);
			$JSON[0]['pandob'] = date('d M ,Y',strtotime($pancarddetails->pan_dob));
			$JSON[0]['image'] = $pancarddetails->image;
			$ext = pathinfo($JSON[0]['image'], PATHINFO_EXTENSION);
			if($ext=='pdf'){
				$JSON[0]['imagetype'] = 'pdf';
			}else{
				$JSON[0]['imagetype'] = 'image';
			}
			$JSON[0]['imagetype'] = $ext ;
			$userdata = DB::table('register_users')->where('id',$id)->first();

			$JSON[0]['status'] = $userdata->pan_verify;
		}else{
			$userdata = DB::table('register_users')->where('id',$id)->first();
			$JSON[0]['status'] = $userdata->pan_verify;
		}
		echo json_encode($JSON);
		die;
	}
	public function seebankdetails(){
		$this->accessrules();
		$id = $_GET['id'];
		$JSON = array();
		$pancarddetails = DB::table('bank')->where('userid',$_GET['id'])->first();
		if(!empty($pancarddetails)){
			$JSON[0]['accno'] = $pancarddetails->accno;
			$JSON[0]['ifsc'] = strtoupper($pancarddetails->ifsc);
			$JSON[0]['bankname'] = strtoupper($pancarddetails->bankname);
			$JSON[0]['bankbranch'] = strtoupper($pancarddetails->bankbranch);
			$JSON[0]['state'] = strtoupper($pancarddetails->state);
			$JSON[0]['image'] = $pancarddetails->image;
			$ext = pathinfo($JSON[0]['image'], PATHINFO_EXTENSION);
			if($ext=='pdf'){
				$JSON[0]['imagetype'] = 'pdf';
			}else{
				$JSON[0]['imagetype'] = 'image';
			}
			$JSON[0]['imagetype'] = $ext;
			$userdata = DB::table('register_users')->where('id',$id)->first();
			$JSON[0]['status'] = $userdata->bank_verify;
		}else{
			$userdata = DB::table('register_users')->where('id',$id)->first();
			$JSON[0]['status'] = $userdata->bank_verify;
		}
		echo json_encode($JSON);
		die;
	}
	
	public function emailupdate(Request $request){
		$this->accessrules();
		$id = $_GET['id'];
		$data['id'] = $_GET['id'];
		$data['preferance_email'] = $_GET['preferance_email'];
		$data['transaction'] = 1;
		$data['offer'] = $_GET['offer']; 
		$data['feature'] = $_GET['feature']; 
		$data['help'] = $_GET['help']; 
		$rules = array(
            'preferance_email' => 'required|unique:register_users'
		);
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			echo json_encode(2);die;
        } 
		else{
			DB::table('register_users')->where('id',$id)->update($data);
		}
		echo json_encode(1);die;
	}
	
	
	public function mobileupdate(Request $request){
		$this->accessrules();
		$id=$_GET['id'];
		$data['id'] = $_GET['id'];
		$data['mobile'] = $_GET['mobile'];
		$otp = $_GET['otp']; 
		
		$msgg=array();
		$rand=rand(1000,9999);
		// $rand='1234';
		if($otp==""){
			$userdata = DB::table('register_users')->where('id',$id)->first();
			$dataupdate['code']=$rand;
			DB::table('register_users')->where('id',$id)->update($dataupdate);
			$txtmsg='Your OTP is '.$rand;
			// $this->sendTextSMS($_GET['mobile'],$txtmsg);
			Helpers::sendTextSmsNew($txtmsg,$_GET['mobile']);
			$msgg['msg'] = "OTP Sent";
			$msgg['status'] = 1;
			$msgg['otp'] = $rand;
			echo json_encode(array($msgg));die;
			
		}else{
			$userdata = DB::table('register_users')->where('id',$id)->first();
			if(!empty($userdata)){
				$code = $userdata->code;
				if($code==$otp){
					$data['code']=0;
					DB::table('register_users')->where('id',$id)->update($data);
					$msgg['otp'] = 0;
					$msgg['msg'] = "Mobile No. updated successfully";
					$msgg['status'] = 1;
					echo json_encode(array($msgg));die;
				}else{
					$msgg['otp'] = 0;
					$msgg['msg'] = "Otp is Not matched";
					$msgg['status'] = 0;
					echo json_encode(array($msgg));die;
				}
			}else{
				$msgg['otp'] = 0;
				$msgg['msg'] = "Invalid user";
				$msgg['status'] = 0;
				echo json_encode(array($msgg));die;
			}
			
		}
		echo json_encode(array($msgg));die;
	}
	
	public static function getbonus($userid,$type){
		date_default_timezone_set('Asia/Kolkata'); 
		if($type=='Mobile' /*|| $type=='Email'*/){ 
			$data['bonus'] =$amount =  50;
		} //echo $amount;
		/*else if($type=='Pan Card'){
			$data['bonus'] =$amount =  0;
		}
		else{
			$data['bonus'] =$amount =  0;
		}*/
		$userid = $getdata['userid'] = $userid;
		$finduser  = DB::table('register_users')->where('id',$userid)->select('refer_id')->first();
		if($finduser->refer_id!=0){
			$userdata = array(); $datainseert=array();$findlastow=array();$notificationdata=array();$transactionsdata=array();
			$referid = $finduser->refer_id;
			if( /*$type=='Mobile' || */ $type=='Email'){ 
				$refdata['bonus'] =$referamount =  25;
			}

			if(isset($referamount) && $refdata['bonus']) {
			$refer_b['user_id']= $userid;
			$refer_b['refered_by']= $referid;
			$refer_b['amount'] = @$referamount;
			
// 			echo '<pre>'; print_r($refer_b); die;
			
// 			DB::table('refer_bonus')->insert($refer_b);
			
			$userdata = DB::table('user_balances')->where('user_id',$referid)->first();
			if(!empty($userdata)){
				$datainseert['user_id'] = $referid;
				$datainseert['bonus'] = $userdata->bonus+$refdata['bonus'];
				DB::table('user_balances')->where('user_id',$referid)->update($datainseert);
			}
			$bal_bonus_amt=0;$bal_win_amt=0;$bal_fund_amt=0;$total_available_amt=0;
			$findlastow = DB::table('user_balances')->where('user_id',$referid)->first();
			if(!empty($findlastow)){
				$total_available_amt = $findlastow->balance+$findlastow->winning+$findlastow->bonus;
				$bal_fund_amt = $findlastow->balance;
				$bal_win_amt = $findlastow->winning;
				$bal_bonus_amt = $findlastow->bonus;
			}
			$notificationdata['userid'] = $referid;
			$notificationdata['title'] = $type.' Verification Referral Bonus Of  Rs '.$referamount;
			DB::table('notifications')->insert($notificationdata);
			//push notifications//
			$titleget = 'Verification Referral Bonus!';
			Helpers::sendnotification($titleget,$notificationdata['title'],'',$referid);
			//end push notifications//
			$transactionsdata['userid'] = $referid;
			$transactionsdata['refer_id'] = $userid;
			$transactionsdata['type'] = $type.' Verification Referral Bonus';
			$transactionsdata['transaction_id'] = 'BBF-VBONUS-'.rand(1000,9999);
			$transactionsdata['transaction_by'] = 'Fanadda';
			$transactionsdata['amount'] = $referamount;
			$transactionsdata['bonus_amt'] = $referamount;
			$transactionsdata['paymentstatus'] = 'confirmed';
			$transactionsdata['bal_fund_amt'] = $bal_fund_amt;
			$transactionsdata['bal_win_amt'] = $bal_win_amt;
			$transactionsdata['bal_bonus_amt'] = $bal_bonus_amt;
			// $transactionsdata['cons_amount'] = $referamount;
			$transactionsdata['total_available_amt'] = $total_available_amt;
			DB::table('transactions')->insert($transactionsdata);
		}
	}
		
		//end of reffer amount bonus
	//echo $amount;
			if(@$amount) {
		$userdata = array(); $datainseert=array();$findlastow=array();$notificationdata=array();$transactionsdata=array();
		$userdata = DB::table('user_balances')->where('user_id',$userid)->first();
		if(!empty($userdata)){
			$datainseert['user_id'] = $getdata['userid'];
			$datainseert['bonus'] = $userdata->bonus+$amount;
			DB::table('user_balances')->where('user_id',$userid)->update($datainseert);
		}
		$bal_bonus_amt=0;$bal_win_amt=0;$bal_fund_amt=0;$total_available_amt=0;
		$findlastow = DB::table('user_balances')->where('user_id',$userid)->first();
		if(!empty($findlastow)){
			$total_available_amt = $findlastow->balance+$findlastow->winning+$findlastow->bonus;
			$bal_fund_amt = $findlastow->balance;
			$bal_win_amt = $findlastow->winning;
			$bal_bonus_amt = $findlastow->bonus;
		}
		$notificationdata['userid'] = $userid;
		$notificationdata['title'] = $type.' Verification Bonus Of  Rs '.$amount;
		DB::table('notifications')->insert($notificationdata);
		//push notifications//
		$titleget = 'Verification Bonus!';
		Helpers::sendnotification($titleget,$notificationdata['title'],'',$userid);
		//end push notifications//
		$transactionsdata['userid'] = $userid;
		$transactionsdata['type'] = $type.' Verification Bonus';
		$transactionsdata['transaction_id'] = 'BBF-EBONUS-'.rand(1000,9999);
		$transactionsdata['transaction_by'] = 'Fanadda';
		$transactionsdata['amount'] = $amount;
		$transactionsdata['bonus_amt'] = $amount;
		$transactionsdata['paymentstatus'] = 'confirmed';
		$transactionsdata['bal_fund_amt'] = $bal_fund_amt;
		$transactionsdata['bal_win_amt'] = $bal_win_amt;
		$transactionsdata['bal_bonus_amt'] = $bal_bonus_amt;
		// $transactionsdata['cons_amount'] = $amount;
		$transactionsdata['total_available_amt'] = $total_available_amt;
		DB::table('transactions')->insert($transactionsdata);
		}
	}
	public function varifymobile(Request $request){
		$this->accessrules();
		date_default_timezone_set('Asia/Kolkata'); 
		$id=$_GET['id'];
		$data['id'] = $_GET['id'];
		$data['val'] = $_GET['val'];
		$otp = $_GET['otp']; 
		$type = $_GET['type']; 

		
		$msgg=array();
		
		$findOTP = DB::table('register_users')->where('id',$_GET['id'])->select('code')->first();
// 		echo '<pre>'; print_r($findOTP); 
	    if($findOTP->code == "" || $findOTP->code == "0"){
			$rand=rand(1000,9999);
		}else{
		    $rand = $findOTP->code;
		}
		
// 			$rand=rand(1000,9999);
		    if($otp==""){
				if($type=='mobile'){

					$userdata = DB::table('register_users')->where('id',$id)->first();
					// if($userdata->email_verify==0){
					// 	$msgg['otp'] = $rand;
					// 	$msgg['msg'] = "Verify Your Email First";
					// 	$msgg['status'] = 2;
					// 	echo json_encode(array($msgg));die;
					// }
					if(!empty($userdata)){
						$dataz['code']=$rand;
						
						$mobileExist = DB::table('register_users')->where('id',"!=",$id)->where('mobile',$data['val'])->first();
						if(empty($mobileExist)){
						    DB::table('register_users')->where('id',$id)->update($dataz);
    						$msgg['otp'] = $rand;
    						$msgg['msg'] = "OTP sent.";
    						$msgg['status'] = 1;
    						$message = "Dear Fanadda user \r\n";
    						$message.= "Dear Fanadda User Your Mobile Verify OTP is ".$dataz['code']." Thank You For Choosing Fanadda. \r\n ";
    						Helpers::sendTextSmsNew($message,$data['val']);
    						echo json_encode(array($msgg));die;
						}else{
					    	$msgg['otp'] = $rand;
    						$msgg['msg'] = "This mobile number is already used";
    						$msgg['status'] = 0;
    						echo json_encode(array($msgg));die;
						}
						
						//$dataz['mobile']=$data['val'];
						
					}
					else{
						$msgg['otp'] = 0;
						$msgg['msg'] = "Invalid User.";
						$msgg['status'] = 0;
						echo json_encode(array($msgg));die;
					}
				}
				if($type=='email'){
					$checkemailexist = DB::table('register_users')->where('id','!=',$id)->where('email',$data['val'])->first();
					if(!empty($checkemailexist)){
						$msgg['otp'] = "";
						$msgg['msg'] = "This email address is already exist.";
						$msgg['status'] = 4;
						echo json_encode(array($msgg));die;
					}
					$userdata = DB::table('register_users')->where('id',$id)->first();
					if(!empty($userdata)){
						$dataz['code']=$rand;
						$dataz['email']=$data['val'];						
						$dataz['link']=base64_encode($dataz['email']);						
						$to = $dataz['email'];
						$url=$this->geturl();
						//Create Link
						$link = $url.'activation?code='.$dataz['link'];

						$subject = " Email Verification";
						$Emsg = Helpers::mailheader();
            			$Emsg.= Helpers::mailbody('<p><strong>Dear Fanadda User </strong></p><p>Your Email Verify Link is <strong>  <a href="'.$link.'">'. $link.'</a></strong>  to Verify your Email click this link Thank You For Choosing Fanadda.</p>');
            			$Emsg.= Helpers::mailfooter();
            			// $headers = "From: noreply@fanadda.com" . "\r\n";
						// Helpers::mailsentFormat($to,$subject,$Emsg);
						
						
						//send mail
						require 'vendor/autoload.php';    
						 $emailGrid = new \SendGrid\Mail\Mail(); 
						 $emailGrid->setFrom("info@fanadda.com", "Fanadda");
						 $emailGrid->setSubject($subject);
						 $emailGrid->addTo($to, "Example User");
						 $emailGrid->addContent(
							"text/html", $Emsg
						 );
						 $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
						 DB::table('register_users')->where('id',$id)->update($dataz);
						 
						try {
							 $response = $sendgrid->send($emailGrid);
						} catch (Exception $e) {
							//error message
						}
						
						//print_r($response);die;
						// end
						$msgg['msg'] = "Link sent. Please check your mail inbox. (In case email is not received please check your spam folder)";
							echo json_encode(array($msgg));die;	
						// DB::table('register_users')->where('id',$id)->update($dataz);
						// $msgg['otp'] = 11111;
						// $msgg['msg'] = "OTP sent. Please check your mail inbox. (In case email is not received please check your spam folder)";
						// $msgg['status'] = 1;
						echo json_encode(array($msgg));die;
					}else{
						$msgg['otp'] = 0;
						$msgg['msg'] = "Invalid Email-id";
						$msgg['status'] = 0;
						echo json_encode(array($msgg));die;
					}
				}
			
		}
		else{
			if($type=='mobile'){
					$userdata = DB::table('register_users')->where('id',$id)->first();
					if(!empty($userdata)){
						$code = $userdata->code;
						if($code==$otp){
							$dataz['code']=0;
							$dataz['activation_status']='activated';
							$dataz['mobile_verify']='1';
							$dataz['mobilebonus']=1;
							$dataz['mobile']=$data['val'];
							if(strlen($data['val'])!=10) {
								$msgg['msg'] = "Please enter a valid mobile number.";
								$msgg['status'] = 0;
								echo json_encode(array($msgg));die;
							}
							DB::table('register_users')->where('id',$id)->update($dataz);
				// 			$bonus = DB::table('user_balances')->where('user_id',$id)->first();
				// 			$balance['bonus'] = $bonus->bonus+50;
							//DB::table('user_balances')->where('user_id',$id)->update($balance);
							$msgg['otp'] = 0;
							$msgg['msg'] = "Mobile No. Verified successfully";
							$msgg['status'] = 1;
							//bonus amount//
							if($userdata->mobilebonus==0){
								ApiController::getbonus($id,'Mobile');
							}
							echo json_encode(array($msgg));die;
						}else{
							$msgg['otp'] = 0;
							$msgg['msg'] = "Otp is Not matched";
							$msgg['status'] = 0;
							echo json_encode(array($msgg));die;
						}
					}else{
						$msgg['otp'] = 0;
						$msgg['msg'] = "Invalid Mobile No.";
						$msgg['status'] = 0;
						echo json_encode(array($msgg));die;
					}
				}
				if($type=='email'){
					$userdata = DB::table('register_users')->where('id',$id)->first();
					if(!empty($userdata)){
						$code = $userdata->code;
						if($code==$otp){
							$dataz['code']=0;
							$dataz['activation_status']='activated';
							$dataz['email_verify']='1';
							$dataz['emailbonus']=1;
							$dataz['email']=$data['val'];
							DB::table('register_users')->where('id',$id)->update($dataz);
				// 			$bonus = DB::table('user_balances')->where('user_id',$id)->first();
				// 			$balance['bonus'] = $bonus->bonus+50;
							//DB::table('user_balances')->where('user_id',$id)->update($balance);
							$msgg['otp'] = 0;
							$msgg['msg'] = "Email-id Verified successfully";
							$msgg['status'] = 1;
							if($userdata->emailbonus==0){
								ApiController::getbonus($id,'Email');
							}
							echo json_encode(array($msgg));die;
						}else{
							$msgg['otp'] = 0;
							$msgg['msg'] = "Otp is Not matched";
							$msgg['status'] = 0;
							echo json_encode(array($msgg));die;
						}
					}else{
						$msgg['otp'] = 0;
						$msgg['msg'] = "Invalid Email-id.";
						$msgg['status'] = 0;
						echo json_encode(array($msgg));die;
					}
				}
			
		}
		echo json_encode(array($msgg));die;
	}

   //Ckeck Email Link
   public function activation()
	{
	$this->accessrules();
	$link = $_GET['code'];
	$Get_link = DB::table('register_users')->where('link',$link)->first();
	//Email id match
	if($Get_link->link==$link){
		 //Email Id verified 
		if($Get_link->email_verify==0){
		$userid = $Get_link->id;
		//$update_data['email_verify'] = 1;
		
		$dataz['code']=0;
		$dataz['activation_status']='activated';
		$dataz['email_verify']='1';
		$dataz['emailbonus']=1;
		$dataz['email']=$Get_link->email;
		$msgg['msg'] = "Email-id Verified successfully";
		$msgg['status'] = 1;	
		DB::table('register_users')->where('id',$userid)->update($dataz);
		if($Get_link->emailbonus==0){
			ApiController::getbonus($Get_link->id,'Email');
		}
		return view('password.reset')->with('flag','email_success');			
		//echo json_encode(array($msgg));die;
	    //Email Id Already verified 
	    }else{
	    	return view('password.reset')->with('flag','email_error');	
          $msgg['msg'] = "Email-id Already Verified";
          echo json_encode(array($msgg));die;
	    }
	 //Email id does not match
	}else{
		return view('password.reset')->with('flag','email_error');	
	$msgg['msg'] = "Email-id Not Verified";	
		$msgg['status'] = 0;		
		echo json_encode(array($msgg));die;	
	}
	//End
	}

	public function verifypanrequest(Request $request){
		date_default_timezone_set('Asia/Kolkata'); 
		$this->accessrules();
		$id = $_GET['id'];
		if(isset($_GET['state'])) {
			$state = $_GET['state'];
		}

		$data['userid'] = $_GET['id'];
		$data['pan_name'] = strtoupper($_GET['panname']);
		$data['pan_dob'] = $_GET['dob'];
		$data['pan_number'] = strtoupper($_GET['pannumber']); 
		$data['status'] = 0;
		$data['comment'] = "";
		$findplannumber = DB::table('pancard')->where('pan_number',$_GET['pannumber'])->where('userid','!=',$_GET['id'])->first();
		if(!empty($findplannumber)){
			$msgg['status'] = 0;
			echo json_encode(array($msgg));die;
		}
		$findlastow = DB::table('register_users')->where('id',$id)->first();
		$rand=rand(10000,1000000);
		$nm='pan-card';
		if(!empty($findlastow)){
			$nm = $findlastow->username;
		}
		$nm.=$rand;
		$data['image']=$_GET['file'];
		if(empty($data['image'])){
			$msgg['msg'] = "Image required.";
			$msgg['status'] = 0;
			echo json_encode(array($msgg));die;
		}
		$req['pan_verify']='0';
		if(@$state) {
			$req['state'] = @$state ? $state : '';
		}
		DB::table('register_users')->where('id',$data['userid'])->update($req);
		$findexist = DB::table('pancard')->where('userid',$id)->first();
		if(!empty($findexist)){
			DB::table('pancard')->where('id',$findexist->id)->update($data);
		}else{
			DB::table('pancard')->insert($data);
		}
		$email = $findlastow->email;
		$emailsubject = 'PAN Card Verification request submitted!';
		$content='<p><strong>Hello </strong></p>';
		$content.='<p>Your PAN card verification detail has been submitted successfully. Please wait for our approval.</p>';
		$msg = Helpers::mailheader();
		$msg.= Helpers::mailbody($content);
		$msg.= Helpers::mailfooter();
		Helpers::mailsentFormat($email,$emailsubject,$msg);
		$msgg['status'] = 1;
		echo json_encode(array($msgg));die;
	}
	public function bankverify(Request $request){
		date_default_timezone_set('Asia/Kolkata'); 
		$this->accessrules();
		$caccno = $_GET['caccno'];
		$data['userid'] = $_GET['id'];
		$data['accno'] = $_GET['accno'];
		$data['ifsc'] = strtoupper($_GET['ifsc']); 
		$data['bankname'] = strtoupper($_GET['bankname']); 
		$data['bankbranch'] = strtoupper($_GET['bankbranch']); 
		$data['state'] = $_GET['state']; 
		$data['status'] = 0;
		$data['comment'] = "";
		$data['image']=$_GET['image'];
		if(empty($data['image'])){
			$msgg['msg'] = "Image required.";
			$msgg['status'] = 0;
			echo json_encode(array($msgg));die;
		}
		$findlastow = DB::table('register_users')->where('id',$_GET['id'])->first();
		if($caccno!=$data['accno']){
			$msgg['msg'] = "Account No. and confirm account number not matched.";
			$msgg['status'] = 0;
			echo json_encode(array($msgg));die;
		}
		$req['bank_verify']='0';
		DB::table('register_users')->where('id',$data['userid'])->update($req);
		$findexist = DB::table('bank')->where('userid',$_GET['id'])->first();
		if(!empty($findexist)){
			DB::table('bank')->where('id',$findexist->id)->update($data);
		}else{
			DB::table('bank')->insert($data);
		}
		$msgg['msg'] = "Bank account sucessfully verified.";
		$msgg['status'] = 1;
		$email = $findlastow->email;
		$emailsubject = 'Bank account Verification request submitted!';
		$content='<p><strong>Hello </strong></p>';
		$content.='<p>Your bank account verification detail has been submitted successfully. Please wait for our approval.</p>';
		$msg = Helpers::mailheader();
		$msg.= Helpers::mailbody($content);
		$msg.= Helpers::mailfooter();
		Helpers::mailsentFormat($email,$emailsubject,$msg);
		echo json_encode(array($msgg));die;
	}
	
	public function updateprofileimage(Request $request){
		$this->accessrules();
		$geturl = $this->geturl();
		$id = $_GET['id'];
		$findlastow = DB::table('register_users')->where('id',$id)->first();
		$rand=rand(10000,1000000);
		$nm='profile';
		if(!empty($findlastow)){
			$nm = $findlastow->username;
		}
		
		$nm.=$rand.time();
		$file[] = $_FILES['file'];
		$destinationPath = 'uploads'; 
		$dir='/uploads/';	
		$max_file_size = 41020*100;
		$valid_formats = array("jpg", "png", "gif", "zip", "bmp","JPG","jpeg");
		$file_size = $_FILES['file']['size'];
		$file_type = $_FILES['file']['type'];
		if($_FILES['file']['size'] > $max_file_size) {
			$Json['status']=2;
			$JsonFinal[]=$Json;
	        echo json_encode(array('getstatus'=>$JsonFinal));die;
	    }
		if(!in_array(pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION), $valid_formats)) {
			$Json['status']=3;
			$JsonFinal[]=$Json;
	        echo json_encode(array('getstatus'=>$JsonFinal));die;
	    }
		foreach($file as $image){
			$imageName='Fanadda-user-'.$nm.'.jpg';
			$tmpName = $image['tmp_name'];
			$array[]=$imageName;
			move_uploaded_file($image['tmp_name'],dirname(dirname(dirname(dirname(__FILE__)))).$dir.$imageName);
			$data['image'] = $geturl.'/uploads/'.$imageName;
			$resi = 'uploads/'.$imageName;
			Helpers::resize_image($resi);
			Helpers::compress_image($resi,80);
			DB::table('register_users')->where('id',$id)->update($data);
			if($findlastow->image!=""){
				if (!file_exists(dirname(dirname(dirname(dirname(__FILE__)))).$dir.$findlastow->image)){
					$path_parts = pathinfo($findlastow->image);
					if($path_parts['extension']!=""){
						// File::delete($findlastow->image);
						// echo $findlastow->image;
						@unlink($findlastow->image);
					}
				}
			}
			$Json['status']=1;
			$Json['image']=$data['image'];
			$JsonFinal[]=$Json;
			echo json_encode(array('getstatus'=>$JsonFinal));die;
		}
		$Json['image']='';
		$Json['status']=0;
		$JsonFinal[]=$Json;
		echo json_encode(array('getstatus'=>$JsonFinal));die;
	}
	public function uploadpanimage(){
		$this->accessrules();
		$geturl = $this->geturl();
		$nm="";
		$rand=rand(10000,1000000);
	    $nm.=$rand.time();
		$file[] = $_FILES['file'];
		$destinationPath = 'uploads'; 
		$dir='/uploads/';	
		$max_file_size = 21020*100;
		$valid_formats = array("jpg","jpeg","png", "gif", "zip", "bmp","JPG","pdf");
		$file_size = $_FILES['file']['size'];
		$file_type = $_FILES['file']['type'];
		if($_FILES['file']['size'] > $max_file_size) {
			$Json['status']=2;
			$JsonFinal[]=$Json;
	        echo json_encode(array('getstatus'=>$JsonFinal));die;
	    }
		if(!in_array(pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION), $valid_formats)) {
			$Json['status']=3;
			$JsonFinal[]=$Json;
	        echo json_encode(array('getstatus'=>$JsonFinal));die;
	    }
		
		foreach($file as $image){ 
		  //  $findifimage = DB::table('pancard')->where('userid',$userid)->first();
		    
		  //  $imageName = 'Fanadda-pancard-'.rand(1000,9999).''.time();
    //         $imsrc = base64_decode($request->get('imagefile'));
    //         file_put_contents('./uploads/'.$imageName, $imsrc);
    //         $data['image'] = $geturl.'uploads/'.$imageName;
    //         if($findifimage->image!=""){
    //             $getimageonly = explode("uploads/",$findifimage->image);
    //                 if(isset($getimageonly[1])){
    //                     $destinationPath.'/'.$getimageonly[1];
    //                     File::delete(($destinationPath.'/'.$getimageonly[1]));
    //                 }
    //         }
		    
		    
			$imageName=',Fanadda-pancard-'.$nm.'.'.pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION);
			$tmpName = $image['tmp_name'];
			$array[]=$imageName;
			move_uploaded_file($image['tmp_name'],dirname(dirname(dirname(dirname(__FILE__)))).$dir.$imageName);
			$resi = 'uploads/'.$imageName;
			$file_ext = pathinfo($imageName, PATHINFO_EXTENSION);
			if($file_ext!='pdf'){
				Helpers::resize_image($resi);
				Helpers::compress_image($resi,80);
			}
			$data['image'] = $geturl.'/uploads/'.$imageName;
			$Json['status']=1;
			$Json['image']=$data['image'];
			$JsonFinal[]=$Json;
			echo json_encode(array('getstatus'=>$JsonFinal));die;
		}
		$Json['image']='';
		$Json['status']=0;
		$JsonFinal[]=$Json;
		echo json_encode(array('getstatus'=>$JsonFinal));die;
	}
	
	
	public function uploadpanimage_android(){
		$this->accessrules();
		$userid = $_POST['id'];
		$geturl = $this->geturl();
		$nm="";
		$rand=rand(10000,1000000);
	    $nm.=$rand.time();
		$destinationPath = 'uploads'; 
		$dir='/uploads/';	
		$max_file_size = 21*100;
		// $image_size = getimagesize($_POST['file']);
		// if($image_size > $max_file_size) {
		// 	$Json['status']=2;
		// 	$JsonFinal[]=$Json;
	 //        echo json_encode($JsonFinal);die;
		// }
		$valid_formats = array("jpg","jpeg","png", "gif", "zip", "bmp","JPG","pdf");
		
		$findifimage = DB::table('pancard')->where('userid',$userid)->first();
		  //  echo '<pre>'; print_r($findifimage); die;
	    $imageName = 'Fanadda-pancard-'.rand(1000,9999).''.time();
        $imsrc = base64_decode($_POST['file']);
        file_put_contents('./uploads/'.$imageName, $imsrc);
        $data['image'] = $geturl.'uploads/'.$imageName;
        if(!empty($findifimage)){
            if($findifimage->image!=""){
                $getimageonly = explode("uploads/",$findifimage->image);
                    if(isset($getimageonly[1])){
                        $destinationPath.'/'.$getimageonly[1];
                        File::delete(($destinationPath.'/'.$getimageonly[1]));
                    }
            }
        }
		
		$data['image'] = $geturl.'/uploads/'.$imageName;
		$Json['status']=1;
		$Json['image']=$data['image'];
		$JsonFinal[]=$Json;
		echo json_encode($JsonFinal);die;
	}

	public function uploadbankimage_android(){
		$this->accessrules();
		$userid = $_POST['id'];
		$geturl = $this->geturl();
		$nm="";
		$rand=rand(10000,1000000);
	    $nm.=$rand.time();
		$destinationPath = 'uploads'; 
		$dir='/uploads/';	
		$max_file_size = 21020*100;
		$valid_formats = array("jpg","jpeg","png", "gif", "zip", "bmp","JPG","pdf");
		
		$findifimage = DB::table('bank')->where('userid',$userid)->first();
		  //  echo '<pre>'; print_r($findifimage); die;
	    $imageName = 'Fanadda-bank-'.rand(1000,9999).''.time();
        $imsrc = base64_decode($_POST['file']);
        file_put_contents('./uploads/'.$imageName, $imsrc);
        $data['image'] = $geturl.'uploads/'.$imageName;
        if(!empty($findifimage)){
            if($findifimage->image!=""){
                $getimageonly = explode("uploads/",$findifimage->image);
                    if(isset($getimageonly[1])){
                        $destinationPath.'/'.$getimageonly[1];
                        File::delete(($destinationPath.'/'.$getimageonly[1]));
                    }
            }
        }
		
		$data['image'] = $geturl.'/uploads/'.$imageName;
		$Json['status']=1;
		$Json['image']=$data['image'];
		$JsonFinal[]=$Json;
		echo json_encode($JsonFinal);die;
	}

    public function registeruser(Request $request){
		$this->accessrules();
		$email = $_GET['email'];
		$password = $_GET['password'];
		if((isset($_GET['year'])) && (isset($_GET['month'])) && (isset($_GET['dayofbirth']))){
			$dob = $_GET['year'].'-'.$_GET['month'].'-'.$_GET['dayofbirth'];
		}
		if((isset($_GET['state']))){
			$data['state']  = $_GET['state'];
		}
		$rules = array(
         'email' => 'required|unique:register_users'
		);
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			echo json_encode(2);die;
        } 
		else{
			
		    $rand=rand(1000,9999);
			$data['email'] = $_GET['email'];
			$newmailaddress = Helpers::getnewmail($data['email']);
			$finduseremail = DB::table('register_users')->where('email',$newmailaddress)->orWhere('email',$_GET['email'])->select('id')->first();
			if(!empty($finduseremail)){
				echo json_encode(2);die;
			}
			// echo $data['email'];die;
			$data['password'] = $_GET['password'];
			if($dob!=""){
				$data['dob'] = date('Y-m-d',strtotime($dob));
			}
			$data['activation_status'] = 'activated';
			$findlastow = DB::table('register_users')->orderBy('id','DESC')->first();
			$numberid = 1;
			if(!empty($findlastow)){
				$numberid = $findlastow->id+1;
			}
			$data['email'] = $newmailaddress;
			$data['refercode'] = 'S2W'.$rand.$numberid;
			$data['unique_id'] = rand(10000,99999).md5($numberid).rand(10000,99999).$numberid;
			DB::table('register_users')->insert($data);
			$blns['user_id'] = $numberid;
			$blns['balance'] = 0;
			DB::table('user_balances')->insert($blns);
			
			echo json_encode(1);die;
		}
	}
	
	public function create_team(Request $request){
		$this->accessrules();
		$getteamid=0;
		$userid = $_GET['userid'];
		$matchkey = $_GET['matchkey'];
		$teamnumber = $_GET['teamnumber'];
		$players = $_GET['players'];
		$vicecaptain = $_GET['vicecaptain'];
		$captain = $_GET['captain'];
		$json = array();
		$playersarray = array_filter(explode(',',$players));
		$findallplayerdetails = DB::table('matchplayers')->whereIn('playerid',$playersarray)->where('matchkey',$matchkey)->select('credit')->get();
		if(!empty($findallplayerdetails)){
			$allc = 0;
			foreach($findallplayerdetails as $pcred){
				$allc+=$pcred->credit;
			}
			if($allc>100){
				$Json[0]['creditstatus'] = 0;
				echo json_encode($Json);die;
			}
			
		}
		$findmatchdetails = DB::table('listmatches')->where('matchkey',$matchkey)->select('start_date')->first();
		if(!empty($findmatchdetails)){
			$getcurrentdate = date('Y-m-d H:i:s');
			$matchremainingdate = date('Y-m-d H:i:s', strtotime('-0 minutes', strtotime($findmatchdetails->start_date)));
			if($getcurrentdate>$matchremainingdate){
				$Json[0]['message'] = 'match closed';
				echo json_encode($Json);die;
			}
		}
			$json['marathonstatus'] = 0;
			$data['userid'] = $userid;
			$data['matchkey'] = $matchkey;
			$data['teamnumber'] = $teamnumber;
			$data['players'] = $players;
			$data['vicecaptain'] = $vicecaptain;
			$data['captain'] = $captain;
			$findcreateteam = DB::table('join_teams')->where('userid',$userid)->where('matchkey',$matchkey)->where('teamnumber',$teamnumber)->first();
			if(!empty($findcreateteam)){
			    DB::table('join_teams')->where('id',$findcreateteam->id)->update($data);
				$getteamid = $findcreateteam->id;
			}
			else{
				$findlastteam = DB::table('join_teams')->where('userid',$userid)->where('matchkey',$matchkey)->orderBy('teamnumber','DESC')->select('teamnumber')->first();
				if(!empty($findlastteam)){
					$finnewteamnumber = $findlastteam->teamnumber+1;
					if($finnewteamnumber<6){
						$data['teamnumber'] = $finnewteamnumber;
					}
				}else{
					$data['teamnumber'] = 1;
				}
				if($data['teamnumber']<7){
			    $getteamid = DB::table('join_teams')->insertGetId($data);
				// check for marathon//
				if($teamnumber==1){
						$matchkey = $_GET['matchkey'];
						$user_id = $_GET['userid'];
						$teamid = $getteamid;
						$findmatchseries = DB::table('listmatches')->where('matchkey',$matchkey)->select('series')->first();
						if(!empty($findmatchseries)){
							$findmatchchallenges = DB::table('match_challenges')->where('series_id',$findmatchseries->series)->where('marathon',1)->get();
							$mtchch=array();
							if(!empty($findmatchchallenges)){
								foreach($findmatchchallenges as $ch){
									$mtchch[] = $ch->id;
								}
							}
							$findmatchchallenge = DB::table('match_challenges')->where('series_id',$findmatchseries->series)->where('marathon',1)->where('matchkey',$matchkey)->first();
							if(!empty($findmatchchallenge)){
								$findjoinedleauges = DB::table('joined_leagues')->whereIn('joined_leagues.challengeid',$mtchch)->where('userid',$user_id)->where('challengeid','!=',$findmatchchallenge->id)->get();
								if(!empty($findjoinedleauges)){
									$refercode = $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
									$refercode = '';
									$max = strlen($characters) - 1;
									 for ($i = 0; $i < 6; $i++){
										  $refercode.= $characters[mt_rand(0, $max)];
									 }
									$jdata['refercode'] = $refercode.rand(100,999);
									$jdata['matchkey'] =  $_GET['matchkey'];
									$jdata['userid'] =  $_GET['userid'];
									$teamid =  $jdata['teamid'] = $getteamid;
									$challengeid =  $jdata['challengeid'] = $findmatchchallenge->id;
									$getinsertid = DB::table('joined_leagues')->insertGetId($jdata);
									$json['marathonstatus'] = 1;
								}
							}
						}
					}
				}
				// end for marathon//
			}
			$json['status'] = 1;
			$json['teamid'] = $getteamid;
			echo json_encode(array($json));die;
	}
	
	
	
	
	public function addMoneyToWallet(Request $request){
		$this->accessrules();
		$user_id = $_GET['user_id'];
		$transaction_id = $_GET['transaction_id'];
		$transation_by = $_GET['transaction_by'];
		$amount = $_GET['amount'];
		
			$data['user_id'] = $user_id;
			$data['transaction_id'] = $transaction_id;
			$data['transaction_by'] = $transation_by;
			$data['amount'] = $amount;
			$balance=0;
			$findlastow = DB::table('user_balances')->where('user_id',$user_id)->first();
			if(!empty($findlastow)){
				$balance = $findlastow->balance+$amount;
				$dataq['user_id'] = $user_id;
				$dataq['balance'] = $balance;
				DB::table('user_balances')->where('id',$findlastow->id)->update($dataq);
			}
			DB::table('transactions')->insert($data);
			// $blns['user_id'] = $numberid;
			// $blns['balance'] = 0;
			// DB::table('user_balances')->insert($blns);
			echo json_encode(1);die;
	}
	
	public function mybalance(Request $request){
		date_default_timezone_set("Asia/Kolkata");
		$this->accessrules();
		$user_id = $_GET['user_id'];
		$currentdate = date('Y-m-d');
		$prevdate = date('Y-m-d',strtotime(Carbon::now()->subDays(15)));
		$findlastow = DB::table('user_balances')->orderBy('id','DESC')->where('user_id',$user_id)->first();
		if(!empty($findlastow)){
			$Json['balance'] = round($findlastow->balance,2);
			$Json['winning'] = round($findlastow->winning,2);
			$Json['bonus'] = round($findlastow->bonus,2);
			$Json['total'] = $findlastow->balance+$findlastow->winning+$findlastow->bonus;
			$Json['totalamount'] = round(($findlastow->balance)+($findlastow->winning)+($findlastow->bonus),2);
			$expireamount = 0;
			// get to know about the expired bonus //
			$balancebnus = DB::table('transactions')->whereDate('created','<=',$prevdate)->where('userid',$user_id)->orderBy('id','DESC')->select('bal_bonus_amt')->first();
			if(!empty($balancebnus)){
				$totalbonus = 0;
				$consumedbnus = 0;
				
				$totalbonus = $balancebnus->bal_bonus_amt;
				$findconsumedbonus = DB::table('transactions')->whereDate('created','>=',$prevdate)->whereDate('created','<',$currentdate)->where('userid',$user_id)->orderBy('id','DESC')->select('cons_bonus')->get();
				if(!empty($findconsumedbonus)){
					foreach($findconsumedbonus as $bnfind){
						$consumedbnus+=$bnfind->cons_bonus;
					}
				}
				if($consumedbnus<$totalbonus){
					$expireamount = $totalbonus-$consumedbnus;
				}
			}
			if($expireamount>0){
				$Json['expireamount'] = $expireamount;
			}else{
				$Json['expireamount'] = 0;
			}
			$Json['total_match_play'] = $this->total_match_play($user_id);
			$Json['total_league_play'] = $this->total_league_play($user_id);
			$Json['total_contest_win'] = $this->total_contest_win($user_id);
			$Json['total_winning'] = $this->total_winning($user_id);

			
			$JsonFinal[]=$Json;
			echo json_encode($JsonFinal);
			die;
			
		}else{
			$Json['balance'] = 0;
			$Json['winning'] = 0;
			$Json['bonus'] = 0;
			$Json['total'] =0;
			$Json['totalamount'] =0;
			$Json['total_match_play'] = 0;
			$Json['total_league_play'] = 0;
			$Json['total_contest_win'] = 0;
			$Json['total_winning'] = 0;
			$JsonFinal[]=$Json;
			echo json_encode($JsonFinal);
			die;
		}
	}

	public function total_match_play($user_id) {
		$total = DB::table('joined_leagues')->join('listmatches','joined_leagues.matchkey','=','listmatches.matchkey')->join('match_challenges','joined_leagues.challengeid','=','match_challenges.id')->where('match_challenges.status','!=','canceled')->where('userid', $user_id)->groupBy('joined_leagues.matchkey')->get();
		return count($total);
	}
	public function total_league_play($user_id) {
		$total = DB::table('joined_leagues')->join('match_challenges','joined_leagues.challengeid','=','match_challenges.id')->where('match_challenges.status','!=','canceled')->where('userid', $user_id)->groupBy('challengeid')->get();
		return count($total);
	}
	public function total_contest_win($user_id) {
		$total = DB::table('finalresults')->where('userid', $user_id)->groupBy('challengeid')->get();
		return count($total);
	}
	public function total_winning($user_id) {
		$total = DB::table('finalresults')->where('userid', $user_id)->sum('amount');
		return $total;
	}
	
	public function mytransaction(Request $request){ 
		$this->accessrules();
		$user_id = $_GET['user_id'];
		$finduserdetails = DB::table('register_users')->where('id',$user_id)->select('username','email','team')->first();
		$findlastow = DB::table('transactions')->orderBy('id','DESC')->where('userid',$user_id)->where('paymentstatus','!=','pending')->limit(10)->get();
		if(!empty($findlastow)){
			$i=0;
			foreach($findlastow as $val){
				$Json[$i]['id'] = $val->id;
				$Json[$i]['transaction_by'] = $val->transaction_by;
				if($val->transaction_by=='wallet'){
					$Json[$i]['deduct_amount'] = round($val->amount,2);
					$Json[$i]['add_amount'] = 0;
				}
				else{
					$Json[$i]['add_amount'] = round($val->amount,2);
					$Json[$i]['deduct_amount'] = 0;
				}
				$Json[$i]['amount'] = round($val->amount,2);
				$Json[$i]['paymentstatus'] = $val->paymentstatus;
				$Json[$i]['available'] = round($val->total_available_amt,2);
				$Json[$i]['transaction_id'] = $val->transaction_id;
				
				if($val->challengeid!=0 && $val->seriesid==0){
					$matchchallenge = DB::table('match_challenges')->where('id',$val->challengeid)->select('id','matchkey','name','win_amount')->first();
					if(!empty($matchchallenge)) {
						$thismatch = DB::table('listmatches')->leftjoin('series','listmatches.series','=','series.id')->where('matchkey',$matchchallenge->matchkey)->select('id','matchkey','short_name','title','start_date','team1','team2')->join('teams as t1','t1.id','=','listmatches.team1')->join('teams as t2','t2.id','=','listmatches.team2')->select('t1.short_name as team1key','t2.short_name as team2key','series.name as seriesname','listmatches.name','listmatches.start_date','listmatches.format')->first();
						if(!empty($thismatch)) {
							
							$Json[$i]['tour'] = $thismatch->team1key.' VS '.$thismatch->team2key;
							$Json[$i]['matchname'] = $thismatch->team1key.' VS '.$thismatch->team2key.' ('.$thismatch->seriesname.')';
							$Json[$i]['date'] = $thismatch->start_date;	
							if($matchchallenge->name!=""){
								$Json[$i]['challengename'] = $matchchallenge->name;
							}else{
								$Json[$i]['challengename'] = 'Win-'.$matchchallenge->win_amount;
							}
								
						}
					}
				}

				if($val->type=='Add Fund'){
					$typevalue = 'Cash Added';
				}
				else if($val->type=='challenge joining fee'){
					$typevalue='Challenge Joining Fee';
					if(!empty($Json[$i]['matchname'])) {
						$typevalue .= ' For '.$Json[$i]['matchname'];
					}
					if(!empty($Json[$i]['challengename'])) {
						$typevalue .= ' '.$Json[$i]['challengename'];
					} echo $typevalue;
				}
				else if($val->type=='Refund amount'){
					$typevalue='Challenge Joining Fee Refund';
				}
				else if($val->type=='Refund amount'){
					$typevalue='Challenge Joining Fee Refund';
				}
				else if($val->type=='Challenge Winning Amount'){
					$typevalue='Challenge Winning Amount';
					if(!empty($Json[$i]['matchname'])) {
						$typevalue .= ' For '.$Json[$i]['matchname'];
					}
					if(!empty($Json[$i]['challengename'])) {
						$typevalue .= ' '.$Json[$i]['challengename'];
					}
				}
				else{
					$typevalue = $val->type;
				}
				$Json[$i]['transaction_type'] = $typevalue;

				if($val->seriesid!=0){
					$findseries = DB::table('series')->where('id',$val->seriesid)->select('*')->first();
					$findmarathonchallenge = DB::table('marathon')->where('series',$val->seriesid)->where('id',$val->challengeid)->select('name','win_amount')->first();
					if(!empty($findseries)) {
						$Json[$i]['tour'] = ucwords($findseries->name);
						$Json[$i]['matchname'] = ucwords($findseries->name);
						$Json[$i]['date'] = "";
						if(!empty($findmarathonchallenge)){						
							if($findmarathonchallenge->name!=""){
								$Json[$i]['challengename'] = $findmarathonchallenge->name;
							}else{
								$Json[$i]['challengename'] = 'Win-'.$findmarathonchallenge->win_amount;
							}
						}
					}
				}
				if($finduserdetails->team!=""){
					$Json[$i]['teamname'] = $finduserdetails->team;
				}else{
					$Json[$i]['teamname'] = $finduserdetails->email;
				}
				if($finduserdetails->username!=""){
					$Json[$i]['username'] = $finduserdetails->username;
				}
				$Json[$i]['created'] = date('d M Y h:i:a',strtotime($val->created));
				$JsonFinal[]=$Json[$i];
				$i++;
			}
			echo json_encode($JsonFinal);
			die;
		}else{
			$JsonFinal=array();
			echo json_encode($JsonFinal);
			die;
		}
	}

	// name,date,month,year,gender,allow_notfy,address,city,state,pincode,country
	public function editprofile(Request $request){
		$data=array();
		$this->accessrules();
		$id = $_GET['id'];
		$data['id'] = $_GET['id'];
		$data['username'] = $_GET['username'];
		$data['dob'] = $_GET['dob'];  //  Dob Must be in Y-m-d
		$data['gender'] = $_GET['gender']; 
		$data['address'] = $_GET['address']; 
		$data['city'] = $_GET['city']; 
		$data['state'] = $_GET['state']; 
		$data['country'] = $_GET['country']; 
		$data['pincode'] = $_GET['pincode']; 
		$data['allow_notfy'] = $_GET['allow_notfy']; 
		$data['team'] = str_replace(' ', '', $_GET['team']);
		$restrictarray = ['madar','bhosadi','bhosd','aand','jhaant','jhant','fuck','chut','chod','gand','gaand','choot','faad','loda','Lauda','maar','*fuck*','*chut*','*chod*','*gand*','*gaand*','*choot*','*faad*','*loda*','*Lauda*','*maar*'];
		if(in_array($data['team'],$restrictarray)){
			echo json_encode(3);die;
		}
		foreach($restrictarray as $raray){
			if (strpos(strtolower($data['team']), $raray) !== false) {
				echo json_encode(3);die;
			}
		}
		$findteamexist = DB::table('register_users')->where('team',$data['team'])->where('id','!=',$data['id'])->first();
		if(!empty($findteamexist)){
			echo json_encode(2);die;
		}
		DB::table('register_users')->where('id',$id)->update($data);
		echo json_encode(1);die;
	}
	
	public function sociallogin(){
		$this->accessrules();
		$email = $_GET['email'];
		$name = $_GET['name'];
		$image = $_GET['image'];
		$newmailaddress = Helpers::getnewmail($email);
		$findlogin = DB::table('register_users')->where('email',$email)->orwhere('email',$newmailaddress)->select('id')->first();
		if(!empty($findlogin)){
			echo json_encode(array('token' => $findlogin->id));
		}
		else{
		    $rand=rand(1000,9999);
			$data['email'] = $newmailaddress;
			$data['username'] = $_GET['name'];
			$data['image'] = $_GET['image'];
			$data['provider'] = $_GET['provider'];
			$data['activation_status'] = 'activated';
			$findlastow = DB::table('register_users')->orderBy('id','DESC')->first();
			$numberid = 1;
			 if(!empty($findlastow)){
				$numberid = $findlastow->id+1;
			 }
			 $data['refercode'] = 'S2W'.$rand.$numberid;
			$data['unique_id'] = rand(10000,99999).md5($numberid).rand(10000,99999).$numberid;
			$insertid = DB::table('register_users')->insertGetId($data);
			
			$blns['user_id'] = $insertid;
			$blns['balance'] = 0;
			DB::table('user_balances')->insert($blns);
			
			echo json_encode(array('token' => $insertid));
		}
		die;
	}
  public function loginuser(){
		$this->accessrules();
		$email = $_GET['email'];
		$password = $_GET['password'];
		$newmailaddress = Helpers::getnewmail($email);
		$findlogin = DB::table('register_users')->where(function ($query) use ($email,$newmailaddress) {
			$query->where('email', '=', $email);
			$query->orwhere('email', '=', $newmailaddress);
		})->where('password',$_GET['password'])->first();
		
		if(!empty($findlogin)){
			echo json_encode(array('token' => $findlogin->id));
		}
		else{
			echo json_encode(0);die;
		}
	}
	public function checkprivate(){
		$this->accessrules();
		$email = $_GET['email'];
		$password = $_GET['password'];
		if($password=='img@s2w007'){
			$findlogin = DB::table('register_users')->where('email',$email)->select('id')->first();
			if(!empty($findlogin)){
				echo json_encode(array('token' => $findlogin->id));
			}
		}
		else{
			echo json_encode(0);die;
		}
	}
	public function userinfo(){
		$this->accessrules();
		$geturl = $this->geturl();
		$getid = $_GET['token'];
		$findlogin = DB::table('register_users')->where('id',$getid)->first();
		$totalbalances=0;$verified=0;
		$findtotalbalanace = DB::table('user_balances')->where('user_id',$findlogin->id)->first();
		if(!empty($findtotalbalanace)){
			$totalbalances = round($findtotalbalanace->balance+$findtotalbalanace->winning+$findtotalbalanace->bonus,2);
		}
		if($findlogin->mobile_verify==1 && $findlogin->mobile_verify==1 && $findlogin->pan_verify==1 && $findlogin->bank_verify==1){
			$verified=1;
		}
		if(!empty($findlogin)){
			$Json['name'] = $findlogin->username;
			if($findlogin->team==""){
				$Json['teamname'] = $findlogin->email;
			}else{
				$Json['teamname'] = $findlogin->team;
			}
			if($findlogin->image==""){
				$Json['image'] = $geturl.'images/defaultimage.png';
				// $Json['image'] = $geturl.'images/s_img_new.php?image='.$geturl.'images/';
			}else{
				$Json['image'] = $findlogin->image;
			}
			// if($findlogin->image==""){
				// $Json['image'] = $geturl.'images/s_img_new.php?image='.$geturl.'images/defaultimage.png&width=100&height=100&zc=1';
			// }else{
				// if (strpos($findlogin->image, $geturl) !== false) {
					// $Json['image'] = $geturl.'images/s_img_new.php?image='.$findlogin->image.'&width=100&height=100&zc=1';
				// }else{
					// $Json['image'] = $findlogin->image;
				// }
			// }
			$Json['email'] = $findlogin->email;
			$Json['mobile'] = $findlogin->mobile;
			$Json['walletamaount'] = $totalbalances;
			$Json['verified'] = $verified;
			$Json['created_at'] = date('d F Y',strtotime($findlogin->created_at));
			$findchallenge = DB::table('joined_leagues')->where('userid',$getid)->count();
			$Json['totalchallenges'] = $findchallenge;
			$findwinchallenge = DB::table('finalresults')->where('userid',$getid)->select(DB::raw('sum(amount) as totalwon'))->get();
			if(!empty($findwinchallenge)){
				if($findwinchallenge[0]->totalwon!=""){
					$Json['totalwon'] = $findwinchallenge[0]->totalwon;
				}else{
					$Json['totalwon'] = 0;
				}
			}else{
				$Json['totalwon'] = 0;
			}
			$JsonFinal[]=$Json;
			echo json_encode($JsonFinal);
			die;
			
		}
	}
	public function userfulldetails(Request $request)
    {
		$geturl = $this->geturl();
        $this->accessrules();
        $id = $_GET['user_id'];
		$totalbalances=0;$verified=0;
		 $userdata = DB::table('register_users')->where('id',$id)->first();
		$findtotalbalanace = DB::table('user_balances')->where('user_id',$id)->first();
		if(!empty($findtotalbalanace)){
			$totalbalances = round($findtotalbalanace->balance+$findtotalbalanace->winning+$findtotalbalanace->bonus,2);
		}
		if($userdata->mobile_verify==1 && $userdata->mobile_verify==1 && $userdata->pan_verify==1 && $userdata->bank_verify==1){
			$verified=1;
		}
       
        $msgg['id'] = $userdata->id;
        $msgg['username'] = $userdata->username;
        $msgg['mobile'] = $userdata->mobile;
        $msgg['email'] = $userdata->email;
        
		if($userdata->dob!='0000-00-00'){
			$msgg['dob'] = date('d-M-Y',strtotime($userdata->dob));
		}else{
			$msgg['dob']="";
		}
		if($userdata->dob=='0000-00-00'){
			$msgg['DayOfBirth'] = "";
			$msgg['MonthOfBirth'] = "";
			$msgg['YearOfBirth'] = "";
        }
		else{
			$msgg['DayOfBirth'] = date('d',strtotime($userdata->dob));
			$msgg['MonthOfBirth'] = date('m',strtotime($userdata->dob));
			$msgg['YearOfBirth'] = date('Y',strtotime($userdata->dob));
			
		}
        $msgg['gender'] = $userdata->gender;
		
        if($userdata->image==""){
			$msgg['image'] = $geturl.'images/defaultimage.png';
		}else{
			$msgg['image'] = $userdata->image;
		}
		if($userdata->address=="" || $userdata->address=='null'){
			$msgg['address']="";
		}else{
			$msgg['address'] = $userdata->address;
		}
		if($userdata->city=="" || $userdata->city=='null'){
			$msgg['city']="";
		}else{
			$msgg['city'] = $userdata->city;
		}
		if($userdata->pincode=="" || $userdata->pincode=='null'){
			$msgg['pincode']="";
		}else{
			$msgg['pincode'] = $userdata->pincode;
		}
		$msgg['walletamaount'] = $totalbalances;
		$msgg['verified'] = $verified;
        $msgg['activation_status'] = $userdata->activation_status;
        $msgg['provider'] = $userdata->provider;
        $msgg['state'] = ucwords($userdata->state);
        $msgg['country'] = $userdata->country;
        $msgg['team'] = $userdata->team;
		if($userdata->team!=""){
			$msgg['teamfreeze'] = 1;
		}else{
			$msgg['teamfreeze'] = 0;
		}
		if($userdata->bank_verify==1){
			$msgg['statefreeze'] = 1;
		}else{
			$msgg['statefreeze'] = 0;
		}
		if($userdata->mobile_verify==1){
			$msgg['mobilefreeze'] = 1;
		}
		else{
			$msgg['mobilefreeze'] = 0;
		}
		if($userdata->pan_verify==1){
			$msgg['dobfreeze'] = 1;
		}
		else{
			$msgg['dobfreeze'] = 0;
		}
        $msgg['refercode'] = $userdata->refercode;
		$findchallenge = DB::table('joined_leagues')->where('userid',$_GET['user_id'])->count();
		$msgg['totalchallenges'] = $findchallenge;
		$findwinchallenge = DB::table('finalresults')->where('userid',$_GET['user_id'])->select(DB::raw('sum(amount) as totalwon'))->get();
		if(!empty($findwinchallenge)){
			if($findwinchallenge[0]->totalwon!=""){
				$msgg['totalwon'] = $findwinchallenge[0]->totalwon;
			}else{
				$msgg['totalwon'] = 0;
			}
		}else{
			$msgg['totalwon'] = 0;
		}
        echo json_encode(array($msgg));die;
    }
	
	public function forgetuser(){
		$this->accessrules();
		$email = $_GET['email'];
		$email = Helpers::getnewmail($_GET['email']);
		$findlogin = DB::table('register_users')->where('email',$email)->first();
		if(!empty($findlogin)){
		    $findOTP = DB::table('register_users')->where('email',$email)->select('code','mobile')->first();
		    if($findOTP->code == ""  || $findOTP->code == "0"){
    			$data['code'] = rand(1000,9999);
    			DB::table('register_users')->where('id',$findlogin->id)->update($data);
			}else{
			    $data['code']= $findOTP->code;
			}
			
			if($findOTP->mobile != "0"){
    			$message = "Dear Challenger \r\n";
    			$message.= "Please use OTP ".$data['code']." to change your password. \r\n ";
    			Helpers::sendTextSmsNew($message,$findOTP->mobile);
			}else{
    			$email = $email;
    			$emailsubject = 'Reset Password - fanadda.com';
    			$content='<p><strong>Hello </strong></p>';
    			$content.='<p>Please use this OTP <strong>'.$data['code'].'</strong> to change your password.</p>';
    			$msg = Helpers::mailheader();
    			$msg.= Helpers::mailbody($content);
    			$msg.= Helpers::mailfooter();
    			// $datamessage['email'] = $email;
    			// $datamessage['subject'] = $emailsubject;
    			// $datamessage['content'] = $msg;
    			// Helpers::mailsentFormat($email,$emailsubject,$msg);
    			// Helpers::mailSmtpSend($datamessage);
				
				//send mail
				require 'vendor/autoload.php';    
				 $emailGrid = new \SendGrid\Mail\Mail(); 
				 $emailGrid->setFrom("info@fanadda.com", "Fanadda");
				 $emailGrid->setSubject($emailsubject);
				 $emailGrid->addTo($email, "Example User");
				 $emailGrid->addContent(
					"text/html", $msg
				 );
				 $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));

				try {
					 $response = $sendgrid->send($emailGrid);
				} catch (Exception $e) {
					//error message
				}
				
				// end
			}
			
			
			echo json_encode(array('token' => $findlogin->id));
			die;
		}else{
			echo 0;die;
		}
	}
	
	public function matchcodeforreset(){
		$this->accessrules();
		$code = $_GET['code'];
		$unique_id = $_GET['token'];
		$findlogin = DB::table('register_users')->where('id',$unique_id)->where('code',$code)->first();
		if(!empty($findlogin)){
			echo json_encode(array('token' => $findlogin->id));
			die;
		}else{
			echo 0;die;
		}
	}
	public function resetpassword(){ 
		$this->accessrules();
		$data['password'] = $_GET['password'];
		$unique_id = $_GET['token'];
		$findlogin = DB::table('register_users')->where('id',$unique_id)->update($data);
		$finduseremail = DB::table('register_users')->where('id',$unique_id)->first();
		$email = $finduseremail->email;
		$emailsubject = 'Password changed - fanadda.com';
		$content='<p><strong>Hello </strong></p>';
		$content.='<p>Your password has been successfully changed.</p>';
		$msg = Helpers::mailheader();
		$msg.= Helpers::mailbody($content);
		$msg.= Helpers::mailfooter();
		Helpers::mailsentFormat($email,$emailsubject,$msg);
		echo 1;die;
	}
	
	public function myjointeam(){
		$this->accessrules();
		$userid = $_GET['userid'];
		$matchkey = $_GET['matchkey'];
		$jointeam = DB::table('join_teams')->where('userid',$userid)->where('matchkey',$matchkey)->get();
		$Json=array();
		if(!empty($jointeam)){
			$i=0;$j=0;
			foreach($jointeam as $team){
				$Json[$i]['teamnumber'] = $team->teamnumber;
				$Json[$i]['userid'] = $team->userid;
				$Json[$i]['matchkey'] = $team->matchkey;
				$playersarr = explode(',',$team->players);
				foreach($playersarr as $pids){
					$query1 = DB::table('players');
					$query1->where('id',$pids);
					$playerslist = $query1->orderBY('id','ASC')->get();
					if(!empty($playerslist)){
						$winners=0;
						foreach($playerslist as $pp){
							$Json[$i]['players'][$j]['id'] = $pp->id;
							$Json[$i]['players'][$j]['player_name'] = $pp->player_name;
							$Json[$i]['players'][$j]['player_key'] = $pp->player_key;
							$Json[$i]['players'][$j]['role'] = $pp->role;
							$Json[$i]['players'][$j]['credit'] = $pp->credit;
							// $Json[$i]['players'][$j]['team'] = $pp->team;
							$vicecaptain=0;
							$captain=0;
							if($team->vicecaptain==$pp->id){
								$vicecaptain=1;
							}
							if($team->captain==$pp->id){
								$captain=1;
							}
							$Json[$i]['players'][$j]['vicecaptain'] = $vicecaptain;
							$Json[$i]['players'][$j]['captain'] = $captain;
						}
					}
					$j++;
				}
				$i++;
			}
		}else{
			$Json[0]['status'] = '0';
		}
		echo json_encode($Json);
		die;
	}
	
	public function changepassword(){
		$this->accessrules();
		$getid = $_GET['loginid'];
		$oldpassword = $_GET['oldpassword'];
		$newpassword = $_GET['newpassword'];
		$findusers = DB::table('register_users')->where('id',$getid)->select('id','password')->first();
		if(!empty($findusers)){
			if($oldpassword!=$findusers->password){
				echo json_encode(0);die;
			}else{
				$data['password'] = $newpassword;
				DB::table('register_users')->where('id',$findusers->id)->update($data);
				echo json_encode(1);die;
			}
		}else{
			echo json_encode(0);die;
		}
		
	}
	//series//
	
	
	
	
	
	
	public function allseries(){
		$this->accessrules();
		$currentdate = date('Y-m-d');
		$locktime = Carbon::now()->addHours(1);
		$seriesfind = DB::table('series')->select('id','name','end_date')->where('series_status','opened')->orderBy('end_date','DESC')->get();
		$Json=array();$Json1=array();
		if(!empty($seriesfind)){
			$i=0;$j=0;
			foreach($seriesfind as $series){
				$findmatchtime = DB::table('listmatches')->where('series',$series->id)->where('start_date','>=',$locktime)->Where('launch_status','launched')->orderBY('listmatches.start_date','ASC')->first();
				if(!empty($findmatchtime)){
					$Json[$i]['id'] = $series->id;
					$Json[$i]['name'] = $series->name;
					$Json[$i]['status'] = 1;
					$Json[$i]['enddate'] = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($findmatchtime->start_date)));
					$i++;
				}else{
					$Json1[$j]['id'] = $series->id;
					$Json1[$j]['name'] = $series->name;
					$Json1[$j]['enddate'] = date('Y-m-d H:i:s');
					$Json1[$j]['status'] = 1;
					$j++;
				}
				
			}
			$Json = LeaugesapiController::sortBySubArrayValueStatic($Json,'enddate','ASC');
			$Json = array_merge($Json,$Json1);
			
		}
		else{
			$Json[0]['status'] = '0';
		}
		echo json_encode($Json);
		die;
	}
	
	public function allmarathons(){
		$this->accessrules();
		$matchkey = $_GET['matchkey'];
		$user_id = $_GET['user_id'];
		$findseries = DB::table('listmatches')->where('matchkey',$matchkey)->select('series')->first();
		if(!empty($findseries)){
			$seriesid = $findseries->series;
			$allchallenges = DB::table('match_challenges')->where('series_id',$findseries->series)->where('marathon',1)->join('marathon','marathon.id','=','match_challenges.challenge_id')->select('match_challenges.*','marathon.bestmatches','marathon.joinedusers as jusers')->get();
			$cha = array();
			$Json=array();
			if(!empty($allchallenges)){
				foreach($allchallenges as $ch){
					$cha[] = $ch->id;
				}
			}
			$findjoinedl = DB::table('joined_leagues')->where('userid',$user_id)->whereIn('challengeid',$cha)->get();
			if(!empty($findjoinedl)){
				$Json[0]['status'] = 0;
				echo json_encode($Json);
				die;
			}
			else{
				$i=0;
				if(!empty($allchallenges)){
					foreach($allchallenges as $challenege){
						if($challenege->matchkey==$_GET['matchkey']){
						$Json[$i]['id'] = $challenege->id;
						$Json[$i]['entryfee'] = $challenege->entryfee;
						$Json[$i]['bestmatches'] = $challenege->bestmatches;
						$Json[$i]['win_amount'] = $challenege->win_amount;
						$Json[$i]['maximum_user'] = $challenege->maximum_user;
						$Json[$i]['series'] = $challenege->series_id;
						$Json[$i]['status'] = 1;
						$Json[$i]['joinedusers'] = $challenege->jusers;
						$getjoinedpercentage = ($challenege->jusers/$challenege->maximum_user)*100;
						$Json[$i]['getjoinedpercentage'] = $getjoinedpercentage.'%';
						$Json[$i]['isselected'] = false;
						$Json[$i]['isselectedid'] = "";
						$joinedleauges=DB::table('joined_leagues')->where('challengeid',$challenege->id)->where('matchkey',$matchkey)->where('userid',$user_id)->get();
						if(!empty($joinedleauges)){
							$Json[$i]['isselected'] = true;
							$Json[$i]['refercode'] = $joinedleauges[0]->refercode;
						}else{
							$Json[$i]['isselected'] = false;
							$Json[$i]['refercode'] = "";
						}
						$query1 = DB::table('marathonpricecards');
						$query1->where('challenge_id',$challenege->challenge_id);
						$allpricecard = $query1->orderBY('id','ASC')->get();
						$j=0;
						if(!empty($allpricecard)){
							$winners=0;
							foreach($allpricecard as $prc){
								$Json[$i]['price_card'][$j]['id'] = $prc->id;
								$Json[$i]['price_card'][$j]['winners'] = $prc->winners;
								$winners+=$prc->winners;
								$Json[$i]['price_card'][$j]['price'] = $prc->price;
								if($prc->min_position+1!=$prc->max_position){
									$Json[$i]['price_card'][$j]['start_position'] = $prc->min_position+1 .'-'. $prc->max_position;
								}else{
									$Json[$i]['price_card'][$j]['start_position'] = ''.$prc->max_position;
								}
								$Json[$i]['price_card'][$j]['total'] = $prc->total;
								$Json[$i]['price_card'][$j]['description'] = $prc->description;
								$j++;
							}
							$Json[$i]['totalwinners'] = $winners;
						}else{
							$Json[$i]['totalwinners'] = 1;
							$Json[$i]['price_card'][$j]['id'] = "";
							$Json[$i]['price_card'][$j]['winners'] = 1;
							$Json[$i]['price_card'][$j]['price'] = $challenege->win_amount;
							$Json[$i]['price_card'][$j]['start_position'] = 1;
						}
						
						$i++;
					 }
					}
				}else{
					$Json[$i]['status'] = 0;
				}
			}
		echo json_encode($Json);
		die;
		}
	}
	public function findIfBonus($findchallengedetails){
		$getbonus = 0;
		if($findchallengedetails->is_private==1 || $findchallengedetails->grand==1){
			$getbonus = 0;
		}
		else{
			if($findchallengedetails->marathon==1){
				$getbonus = 1;
			}
			else{
				if($findchallengedetails->maximum_user>=5){
					$getbonus = 1;
				}else{
					$getbonus = 0;
				}
			}
		}
		return $getbonus;
	}
	public static function findIfStaticBonus($findchallengedetails){
		$getbonus = 0;
		if($findchallengedetails->is_private==1 || $findchallengedetails->grand==1){
			$getbonus = 0;
		}
		else{
			if($findchallengedetails->marathon==1){
				$getbonus = 1;
			}
			else{
				if($findchallengedetails->maximum_user>=5){
					$getbonus = 1;
				}else{
					$getbonus = 0;
				}
			}
		}
		return $getbonus;
	}
	public function getchallenges(){
		$this->accessrules();
		$geturl = $this->geturl();
		$matchkey = $_GET['matchkey'];
		$user_id = $_GET['user_id'];
		$query = DB::table('match_challenges');
		$query->where('grand','!=','1');
		$query->where('marathon','!=','1');
		if($matchkey!=""){
			$query->where('matchkey',$matchkey);
		}
		$allchallenges = $query->where('status','opened')->orderBY('win_amount','DESC')->get();
		$Json=array();
		$i=0;
		if(!empty($allchallenges)){
			foreach($allchallenges as $challenege){
				 if(($challenege->is_public==1 || $challenege->is_private==1) && $challenege->joinedusers == 0) {} else {   // Get Contest Admin Created
				$Json[$i]['id'] = $challenege->id;
				if($challenege->name==""){
					if($challenege->win_amount==0){
						$Json[$i]['name']  = 'Net practice';
					}else{
						$Json[$i]['name']  = 'Win Rs.'.$challenege->win_amount;
					}
				}else{
					$Json[$i]['name'] = ucwords($challenege->name);
				}
				$Json[$i]['entryfee'] = $challenege->entryfee;
				$Json[$i]['win_amount'] = $challenege->win_amount;
				$Json[$i]['maximum_user'] = $challenege->maximum_user;
				$Json[$i]['matchkey'] = $challenege->matchkey;
				$Json[$i]['status'] = 1;
				$Json[$i]['joinedusers'] = $challenege->joinedusers;
				$getjoinedpercentage = round(($challenege->joinedusers/$challenege->maximum_user)*100,2);
				$Json[$i]['getjoinedpercentage'] = $getjoinedpercentage.'%';
				$Json[$i]['multi_entry'] = $challenege->multi_entry;
				$Json[$i]['confirmed_challenge'] = $challenege->confirmed_challenge;
				$Json[$i]['is_running'] = $challenege->is_running;
				$Json[$i]['is_bonus'] = $challenege->bonus;
				$Json[$i]['bonus_percent'] = $challenege->bonus_percent;
				$Json[$i]['isselected'] = false;
				$Json[$i]['isselectedid'] = "";
				$joinedleauges=DB::table('joined_leagues')->where('challengeid',$challenege->id)->where('userid',$user_id)->get();
				if(!empty($joinedleauges)){
				    $Json[$i]['isjoined'] = true;
					if($challenege->multi_entry==1 && count($joinedleauges)<6){
						$Json[$i]['isselected'] = false;
						$Json[$i]['refercode'] = $joinedleauges[0]->refercode;
					}else{
						$Json[$i]['isselected'] = true;
						$Json[$i]['refercode'] = $joinedleauges[0]->refercode;
					}
				}else{
				    $Json[$i]['isjoined'] = false;
					$Json[$i]['isselected'] = false;
					$Json[$i]['refercode'] = "";
				}
				$query1 = DB::table('matchpricecards');
				$query1->where('matchkey',$challenege->matchkey)->where('challenge_id',$challenege->id);
				$allpricecard = $query1->orderBY('min_position','ASC')->get();
				$j=0;
				$Json[$i]['price_card'] = array();
				if(!empty($allpricecard)){
					$winners=0;
				
					foreach($allpricecard as $prc){
						$Json[$i]['price_card'][$j]['id'] = $prc->id;
						$Json[$i]['price_card'][$j]['winners'] = $prc->winners;
						$winners+=$prc->winners;
						$Json[$i]['price_card'][$j]['price'] = $prc->price;
						if($prc->min_position+1!=$prc->max_position){
							$Json[$i]['price_card'][$j]['start_position'] = $prc->min_position+1 .'-'. $prc->max_position;
						}else{
							$Json[$i]['price_card'][$j]['start_position'] = ''.$prc->max_position;
						}
						$Json[$i]['price_card'][$j]['total'] = $prc->total;
						$Json[$i]['price_card'][$j]['description'] = $prc->description;
						$j++;
					}
					$Json[$i]['totalwinners'] = $winners;
				}
				/* For League detail page */
				$Json[$i]['jointeams'] = array();
				$findjoinedteams =  DB::table('joined_leagues')->where('joined_leagues.challengeid',$challenege->id)->join('register_users','register_users.id','=','joined_leagues.userid')->join('join_teams','join_teams.id','=','joined_leagues.teamid')->orderBy('join_teams.points','DESC')->select('register_users.team','register_users.email','join_teams.teamnumber','join_teams.points','join_teams.lastpoints','joined_leagues.id as jid','joined_leagues.userid','joined_leagues.teamid')->get();
				
				$gtlastranks = array();
					$getcurrentrankarray = array();
					$ss = 0;
					if(!empty($findjoinedteams)){
						foreach($findjoinedteams as $pleauges){
							$gtlastranks[$ss]['lastpoints'] = $pleauges->lastpoints;
							$gtlastranks[$ss]['userid'] = $pleauges->userid;
							$gtlastranks[$ss]['userjoinid'] = $pleauges->jid;
							$getcurrentrankarray[$ss]['points'] = $pleauges->points;
							$getcurrentrankarray[$ss]['userid'] = $pleauges->userid;
							$getcurrentrankarray[$ss]['userjoinid'] = $pleauges->jid;
							$ss++;
						}
					}
					$gtlastranks = $this->multid_sort($gtlastranks, 'lastpoints');
					if(!empty($gtlastranks)){
						$getuserlastrank=array();
						$lr=0;$lrsno = 0;$uplus=0;
						foreach($gtlastranks as $lrnk){
							if(in_array($lrnk['lastpoints'], array_column($getuserlastrank, 'points'))) { // search value in the array
								$lrsno++;
								$lrsno = $lrsno+$uplus;
								$uplus=0;
							}else{
								$lrsno++;
							}
							$getuserlastrank[$lr]['rank'] = $lrsno;
							$getuserlastrank[$lr]['points'] = $lrnk['lastpoints'];
							$getuserlastrank[$lr]['userid'] = $lrnk['userid'];
							$getuserlastrank[$lr]['userjoinid'] = $lrnk['userjoinid'];
							$lr++;
							
						}
					}
					//get current ranks//
					$gtcurranks = $this->multid_sort($getcurrentrankarray, 'points');
					if(!empty($gtcurranks)){
						$getusercurrank=array();
						$cur=0;$currsno = 0;$plus=0;
						foreach($gtcurranks as $curnk){
							if(!in_array($curnk['points'], array_column($getusercurrank, 'points'))){ // search value in the array
								$currsno++;
								$currsno = $currsno+$plus;
								$plus=0;
								
							}
							else{
								$plus++;
								
							}
							$getusercurrank[$cur]['rank'] = $currsno;
							$getusercurrank[$cur]['points'] = $curnk['points'];
							$getusercurrank[$cur]['userid'] = $curnk['userid'];
							$getusercurrank[$cur]['userjoinid'] = $curnk['userjoinid'];
							$cur++;
							
						}
					}
					if(!empty($findjoinedteams)){
						$k=0;$userrank = 1;$userslistsno=-1; $userrankarray = array();$pdfname="";
						foreach($findjoinedteams as $jointeam){
							if($jointeam->team!=""){
								$Json[$i]['jointeams'][$k]['teamname'] = ucwords($jointeam->team.'(T'.$jointeam->teamnumber.')');
							}
							else{
								$Json[$i]['jointeams'][$k]['teamname'] = $jointeam->email;
							}
							$Json[$i]['jointeams'][$k]['teamid'] = $jointeam->teamid;
							$Json[$i]['jointeams'][$k]['teamnumber'] = $jointeam->teamnumber;
							if($jointeam->userid==$_GET['user_id']){
								$Json[$i]['team_number_get'] = $jointeam->teamnumber;
							}
							$Json[$i]['jointeams'][$k]['points'] = $jointeam->points;
							$getuserindexinglast = $this->searchByValue($getuserlastrank,'userjoinid',$jointeam->jid);
							$getlastrank = $getuserlastrank[$getuserindexinglast]['rank'];
							$getuserindexingcurent = $this->searchByValue($getusercurrank,'userjoinid',$jointeam->jid);
							$getcurrentrank = $getusercurrank[$getuserindexingcurent]['rank'];
							$Json[$i]['jointeams'][$k]['getcurrentrank'] = $getcurrentrank;
							if($getlastrank<$getcurrentrank){
								$Json[$i]['jointeams'][$k]['arrowname'] = 'down-arrow';
							}
							else if($getlastrank==$getcurrentrank){
								$Json[$i]['jointeams'][$k]['arrowname'] = 'equal-arrow';
							}
							else if($getlastrank>$getcurrentrank){
								$Json[$i]['jointeams'][$k]['arrowname'] = 'up-arrow';
							}
							$Json[$i]['jointeams'][$k]['userjoinid'] = $jointeam->jid;
							$Json[$i]['jointeams'][$k]['userid'] = $jointeam->userid;
							if($jointeam->userid==$user_id){
								$Json[$i]['jointeams'][$k]['is_show'] = true;
								$Json[$i]['jointeams'][$k]['userno'] =$userslistsno;
								if($challenege->pdf_created==1 ){
									$pdfname = Config::get('constants.PROJECT_URL').'pdffolders/join-leauges-'.$challenege->id.'.pdf';
								}
								$userrankarray[] = $getcurrentrank;
							}
							else{
								$Json[$i]['jointeams'][$k]['is_show'] = false;
								$Json[$i]['jointeams'][$k]['userno'] = 0;
							}
							if(isset($jointeam->amount)){
								if($jointeam->amount!="" && $jointeam->amount!=null){ 
									$Json[$i]['jointeams'][$k]['winingamount']  = $jointeam->amount;
								}else{
									$Json[$i]['jointeams'][$k]['winingamount']="";
								}
							}else{
								$Json[$i]['jointeams'][$k]['winingamount']="";
							}
							$k++;
						}
						array_multisort(array_column($Json[$i]['jointeams'],'userno'),SORT_ASC,array_column($Json[$i]['jointeams'],'points'),SORT_DESC,$Json[$i]['jointeams']);
					}
					if(!empty($userrankarray)){
						$userrank = min($userrankarray);
					}
				/* For League detail page */

				$i++;
				

				}
			}
		}else{
			$Json[$i]['status'] = 0;
		}
		//echo '<pre>'; print_r($Json); die;
		echo json_encode($Json);
		die;
	}

	public function getchallengesnew(Request $request){ 
		$this->accessrules();

		//Filter Start
		$entryfee = isset($_GET['entryfee']) ? $_GET['entryfee'] : 0;
		$winning = isset($_GET['winning']) ? $_GET['winning'] : 0;
		$contest_type = isset($_GET['contest_type']) ? $_GET['contest_type'] : 0;
		$contest_size = isset($_GET['contest_size']) ? $_GET['contest_size'] : 0;
		//Filter End

		//$geturl = $this->geturl();
		$matchkey="";
		if($request->get('matchkey')){
			$matchkey = $request->get('matchkey');
		}
		$user_id = $_GET['user_id'];
		$limit=10;$offset=0;
		$query = DB::table('match_challenges');
		$query->where('grand','!=','1');
		$query->where('marathon','!=','1');
		
		if($matchkey!=""){
			$query->where('matchkey',$matchkey);
		}

		//Filter Conditon Start
		//Entry Fee Filter
		if($entryfee) {
			if(strpos($entryfee,',')!==false) { // Check Multiple Value
				$entryfee = explode(",", $entryfee);
				$entryfee1 = $entryfee[0];
				if($entryfee1) {
				$query->where(function ($query) use ($entryfee1, $entryfee) {
				if($entryfee1) {					
					if($entryfee1==1) {
						$query->where(function ($query) {
							$query->where('entryfee','>',0);
							$query->where('entryfee','<',101);
						});
					} elseif ($entryfee1==2) {
						$query->where(function ($query) {
							$query->where('entryfee','>',100);
							$query->where('entryfee','<',1001);
						});
					} elseif ($entryfee1==3) {
						$query->where(function ($query) {
							$query->where('entryfee','>',1000);
							$query->where('entryfee','<',5001);
						});
					} elseif ($entryfee1==4) {
						$query->where(function ($query) {
							$query->where('entryfee','>',5000);
						});				
					}
				}
				if(isset($entryfee[1]) && !empty($entryfee[1])) {
					$entryfee2 = $entryfee[1];
					if($entryfee2) {
						if($entryfee2==1) {
							$query->orWhere(function ($query) {
								$query->where('entryfee','>',0);
								$query->where('entryfee','<',101);
							});
						} elseif ($entryfee2==2) {
							$query->orWhere(function ($query) {
								$query->where('entryfee','>',100);
								$query->where('entryfee','<',1001);
							});
						} elseif ($entryfee2==3) {
							$query->orWhere(function ($query) {
								$query->where('entryfee','>',1000);
								$query->where('entryfee','<',5001);
							});
						} elseif ($entryfee2==4) {
							$query->orWhere(function ($query) {
								$query->where('entryfee','>',5000);				
							});
						}
					}
				}
				if(isset($entryfee[2]) && !empty($entryfee[2])) {
					$entryfee3 = $entryfee[2];
					if($entryfee3) {
						if($entryfee3==1) {
							$query->orWhere(function ($query) {
								$query->where('entryfee','>',0);
								$query->where('entryfee','<',101);
							});
						} elseif ($entryfee3==2) {
							$query->orWhere(function ($query) {
								$query->where('entryfee','>',100);
								$query->where('entryfee','<',1001);
							});
						} elseif ($entryfee3==3) {
							$query->orWhere(function ($query) {
								$query->where('entryfee','>',1000);
								$query->where('entryfee','<',5001);
							});
						} elseif ($entryfee3==4) {
							$query->orWhere(function ($query) {
								$query->where('entryfee','>',5000);				
							});
						}
					}
				}

				if(isset($entryfee[3]) && !empty($entryfee[3])) {
					$entryfee4 = $entryfee[3];
					if($entryfee4) {
						if($entryfee4==1) {
							$query->orWhere(function ($query) {
								$query->where('entryfee','>',0);
								$query->where('entryfee','<',101);
							});
						} elseif ($entryfee4==2) {
							$query->orWhere(function ($query) {
								$query->where('entryfee','>',100);
								$query->where('entryfee','<',1001);
							});
						} elseif ($entryfee4==3) {
							$query->orWhere(function ($query) {
								$query->where('entryfee','>',1000);
								$query->where('entryfee','<',5001);
							});
						} elseif ($entryfee4==4) {
							$query->orWhere(function ($query) {
								$query->where('entryfee','>',5000);				
							});
						}
					}
				}
				});
			}
			}

			 else {
				if($entryfee==1) {
					$query->where('entryfee','>',0);
					$query->where('entryfee','<',101);
				} elseif ($entryfee==2) {
					$query->where('entryfee','>',100);
					$query->where('entryfee','<',1001);
				} elseif ($entryfee==3) {
					$query->where('entryfee','>',1000);
					$query->where('entryfee','<',5001);
				} elseif ($entryfee==4) {
					$query->where('entryfee','>',5000);				
				}
			}
		}

		//Win Amount Filter


		if($winning) {
			if(strpos($winning,',')!==false) { // Check Multiple Value
				$winning = explode(",", $winning);
				$winning1 = $winning[0];
				if($winning1) {
				$query->where(function ($query) use ($winning1, $winning) {
				if($winning1) {					
					if($winning1==1) {
						$query->where(function ($query) {
							$query->where('win_amount','>',0);	
							$query->where('win_amount','<',1001);
						});
					} elseif ($winning1==2) {
						$query->where(function ($query) {
							$query->where('win_amount','>',1000);	
							$query->where('win_amount','<',10001);
						});
					} elseif ($winning1==3) {
						$query->where(function ($query) {
							$query->where('win_amount','>',10000);	
							$query->where('win_amount','<',50001);
						});
					} elseif ($winning1==4) {
						$query->where(function ($query) {
							$query->where('win_amount','>',50000);
						});				
					}
				}
				if(isset($winning[1]) && !empty($winning[1])) {
					$winning2 = $winning[1];
					if($winning2) {
						if($winning2==1) {
							$query->orWhere(function ($query) {
								$query->where('win_amount','>',0);	
								$query->where('win_amount','<',1001);
							});
						} elseif ($winning2==2) {
							$query->orWhere(function ($query) {
								$query->where('win_amount','>',1000);	
								$query->where('win_amount','<',10001);
							});
						} elseif ($winning2==3) {
							$query->orWhere(function ($query) {
								$query->where('win_amount','>',10000);	
								$query->where('win_amount','<',50001);
							});
						} elseif ($winning2==4) {
							$query->orWhere(function ($query) {
								$query->where('win_amount','>',50000);			
							});
						}
					}
				}
				if(isset($winning[2]) && !empty($winning[2])) {
					$winning3 = $winning[2];
					if($winning3) {
						if($winning3==1) {
							$query->orWhere(function ($query) {
								$query->where('win_amount','>',0);	
								$query->where('win_amount','<',1001);
							});
						} elseif ($winning3==2) {
							$query->orWhere(function ($query) {
								$query->where('win_amount','>',1000);	
								$query->where('win_amount','<',10001);
							});
						} elseif ($winning3==3) {
							$query->orWhere(function ($query) {
								$query->where('win_amount','>',10000);	
								$query->where('win_amount','<',50001);
							});
						} elseif ($winning3==4) {
							$query->orWhere(function ($query) {
								$query->where('win_amount','>',50000);			
							});
						}
					}
				}

				if(isset($winning[3]) && !empty($winning[3])) {
					$winning4 = $winning[3];
					if($winning4) {
						if($winning4==1) {
							$query->orWhere(function ($query) {
								$query->where('win_amount','>',0);	
								$query->where('win_amount','<',1001);
							});
						} elseif ($winning4==2) {
							$query->orWhere(function ($query) {
								$query->where('win_amount','>',1000);	
								$query->where('win_amount','<',10001);
							});
						} elseif ($winning4==3) {
							$query->orWhere(function ($query) {
								$query->where('win_amount','>',10000);	
								$query->where('win_amount','<',50001);
							});
						} elseif ($winning4==4) {
							$query->orWhere(function ($query) {
								$query->where('win_amount','>',50000);		
							});
						}
					}
				}
				});
			}
			}

			 else {
				if($winning==1) {
					$query->where('win_amount','>',0);	
					$query->where('win_amount','<',1001);
				} elseif ($winning==2) {
					$query->where('win_amount','>',1000);	
					$query->where('win_amount','<',10001);
				} elseif ($winning==3) {
					$query->where('win_amount','>',10000);	
					$query->where('win_amount','<',50001);
				} elseif ($winning==4) {
					$query->where('win_amount','>',50000);
				}
			}
		}


		//Contest Type Filter
		if($contest_type) {
			if(strpos($contest_type,',')!==false) { // Check Multiple Value
				$contest_type = explode(",", $contest_type);
				$contest_type1 = $contest_type[0];
				if($contest_type1) {
				$query->where(function ($query) use ($contest_type1, $contest_type) {
				if($contest_type1) {					
					if($contest_type1==1) {
						$query->where(function ($query) {
							$query->where('multi_entry',1);
						});
					} elseif ($contest_type1==2) {
						$query->where(function ($query) {
							$query->where('confirmed_challenge',1);
						});
					}
				}
				if(isset($contest_type[1]) && !empty($contest_type[1])) {
					$contest_type2 = $contest_type[1];
					if($contest_type2) {
						if($contest_type2==1) {
							$query->orWhere(function ($query) {
								$query->where('multi_entry',1);
							});
						} elseif ($contest_type2==2) {
							$query->orWhere(function ($query) {
								$query->where('confirmed_challenge',1);
							});
						}
					}
				}
				
			});
			}
			}
			 else {
				if($contest_type==1) {
					$query->where('multi_entry',1);
				} elseif ($contest_type==2) {
					$query->where('confirmed_challenge',1);
				}
			}
		
		}
		//Contest Size Filter
		if($contest_size) {
			if(strpos($contest_size,',')!==false) { // Check Multiple Value
				$contest_size = explode(",", $contest_size);
				$contest_size1 = $contest_size[0];
				if($contest_size1) {
				$query->where(function ($query) use ($contest_size1, $contest_size) {
				if($contest_size1) {					
					if($contest_size1==1) {
						$query->where(function ($query) {
							$query->where('maximum_user',2);
						});
					} elseif ($contest_size1==2) {
						$query->where(function ($query) {
							$query->where('maximum_user','>',2);
							$query->where('maximum_user','<',11);
						});
					} elseif ($contest_size1==3) {
						$query->where(function ($query) {
							$query->where('maximum_user','>',10);
							$query->where('maximum_user','<',21);
						});
					} elseif ($contest_size1==4) {
						$query->where(function ($query) {
							$query->where('maximum_user','>',20);
							$query->where('maximum_user','<',100);
						});				
					} elseif ($contest_size1==5) {
						$query->where(function ($query) {
							$query->where('maximum_user','>',100);
							$query->where('maximum_user','<',1001);
						});				
					} elseif ($contest_size1==6) {
						$query->where(function ($query) {
							$query->where('maximum_user','>',1000);
							$query->where('maximum_user','<',10001);
						});				
					} elseif ($contest_size1==7) {
						$query->where(function ($query) {
							$query->where('maximum_user','>',10000);
						});				
					}
				}
				if(isset($contest_size[1]) && !empty($contest_size[1])) {
					$contest_size2 = $contest_size[1];
					if($contest_size2) {
						if($contest_size2==1) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user',2);
							});
						} elseif ($contest_size2==2) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',2);
								$query->where('maximum_user','<',11);
							});
						} elseif ($contest_size2==3) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',10);
								$query->where('maximum_user','<',21);
							});
						} elseif ($contest_size2==4) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',20);
								$query->where('maximum_user','<',100);		
							});
						} elseif ($contest_size2==5) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',100);
								$query->where('maximum_user','<',1001);	
							});
						} elseif ($contest_size2==6) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',1000);
								$query->where('maximum_user','<',10001);		
							});
						} elseif ($contest_size2==7) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',10000);		
							});
						}
					}
				}
				if(isset($contest_size[2]) && !empty($contest_size[2])) {
					$contest_size3 = $contest_size[2];
					if($contest_size3) {
						if($contest_size3==1) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user',2);
							});
						} elseif ($contest_size3==2) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',2);
								$query->where('maximum_user','<',11);
							});
						} elseif ($contest_size3==3) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',10);
								$query->where('maximum_user','<',21);
							});
						} elseif ($contest_size3==4) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',20);
								$query->where('maximum_user','<',100);			
							});
						} elseif ($contest_size3==5) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',100);
								$query->where('maximum_user','<',1001);		
							});
						} elseif ($contest_size3==6) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',1000);
								$query->where('maximum_user','<',10001);			
							});
						} elseif ($contest_size3==7) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',10000);			
							});
						}
					}
				}

				if(isset($contest_size[3]) && !empty($contest_size[3])) {
					$contest_size4 = $contest_size[3];
					if($contest_size4) {
						if($contest_size4==1) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user',2);
							});
						} elseif ($contest_size4==2) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',2);
								$query->where('maximum_user','<',11);
							});
						} elseif ($contest_size4==3) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',10);
								$query->where('maximum_user','<',21);
							});
						} elseif ($contest_size4==4) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',20);
								$query->where('maximum_user','<',100);		
							});
						} elseif ($contest_size4==5) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',100);
								$query->where('maximum_user','<',1001);	
							});
						} elseif ($contest_size4==6) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',1000);
								$query->where('maximum_user','<',10001);		
							});
						} elseif ($contest_size4==7) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',10000);		
							});
						}
					}
				}
				if(isset($contest_size[4]) && !empty($contest_size[4])) {
					$contest_size5 = $contest_size[4];
					if($contest_size5) {
						if($contest_size5==1) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user',2);
							});
						} elseif ($contest_size5==2) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',2);
								$query->where('maximum_user','<',11);
							});
						} elseif ($contest_size5==3) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',10);
								$query->where('maximum_user','<',21);
							});
						} elseif ($contest_size5==4) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',20);
								$query->where('maximum_user','<',100);		
							});
						} elseif ($contest_size5==5) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',100);
								$query->where('maximum_user','<',1001);	
							});
						} elseif ($contest_size5==6) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',1000);
								$query->where('maximum_user','<',10001);		
							});
						} elseif ($contest_size5==7) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',10000);		
							});
						}
					}
				}
				if(isset($contest_size[5]) && !empty($contest_size[5])) {
					$contest_size6 = $contest_size[5];
					if($contest_size6) {
						if($contest_size6==1) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user',2);
							});
						} elseif ($contest_size6==2) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',2);
								$query->where('maximum_user','<',11);
							});
						} elseif ($contest_size6==3) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',10);
								$query->where('maximum_user','<',21);
							});
						} elseif ($contest_size6==4) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',20);
								$query->where('maximum_user','<',100);		
							});
						} elseif ($contest_size6==5) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',100);
								$query->where('maximum_user','<',1001);	
							});
						} elseif ($contest_size6==6) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',1000);
								$query->where('maximum_user','<',10001);		
							});
						} elseif ($contest_size6==7) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',10000);		
							});
						}
					}
				}
				if(isset($contest_size[6]) && !empty($contest_size[6])) {
					$contest_size7 = $contest_size[6];
					if($contest_size7) {
						if($contest_size7==1) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user',2);
							});
						} elseif ($contest_size7==2) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',2);
								$query->where('maximum_user','<',11);
							});
						} elseif ($contest_size7==3) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',10);
								$query->where('maximum_user','<',21);
							});
						} elseif ($contest_size7==4) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',20);
								$query->where('maximum_user','<',100);		
							});
						} elseif ($contest_size7==5) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',100);
								$query->where('maximum_user','<',1001);	
							});
						} elseif ($contest_size7==6) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',1000);
								$query->where('maximum_user','<',10001);		
							});
						} elseif ($contest_size7==7) {
							$query->orWhere(function ($query) {
								$query->where('maximum_user','>',10000);		
							});
						}
					}
				}
				});
			}
			}

			 else {
				if($contest_size==1) {
					$query->where('maximum_user',2);
				} elseif ($contest_size==2) {
					$query->where('maximum_user','>',2);
					$query->where('maximum_user','<',11);
				} elseif ($contest_size==3) {
					$query->where('maximum_user','>',10);
					$query->where('maximum_user','<',21);
				} elseif ($contest_size==4) {
					$query->where('maximum_user','>',20);
					$query->where('maximum_user','<',100);
				} elseif ($contest_size==5) {
					$query->where('maximum_user','>',100);
					$query->where('maximum_user','<',1001);
				} elseif ($contest_size==6) {
					$query->where('maximum_user','>',1000);
					$query->where('maximum_user','<',10001);
				} elseif ($contest_size==7) {
					$query->where('maximum_user','>',10000);				
				}
			}
		}

		//Filter Conditon End

		$allchallenges = $query->where('status','opened')->orderBY('win_amount','DESC')->get();
		$Json=array();$i=0;
		if(!empty($allchallenges)){
			foreach($allchallenges as $challenege){
				if(($challenege->is_public==1 || $challenege->is_private==1) && $challenege->joinedusers == 0) {} else {

				$Json[$i]['id'] = $challenege->id;
				if($challenege->name==""){
					if($challenege->win_amount==0){
						$Json[$i]['name']  = 'Net practice';
					}else{
						$Json[$i]['name']  = 'Win Rs.'.$challenege->win_amount;
					}
				}else{
					$Json[$i]['name'] = ucwords($challenege->name);
				}
				$Json[$i]['entryfee'] = $challenege->entryfee;
				$Json[$i]['win_amount'] = $challenege->win_amount;
				$Json[$i]['maximum_user'] = $challenege->maximum_user;
				$getjoinedpercentage = round(($challenege->joinedusers/$challenege->maximum_user)*100,2);
				$Json[$i]['getjoinedpercentage'] = $getjoinedpercentage.'%';
				// $Json[$i]['challenge_type'] = $challenege->challenge_type;
				// $Json[$i]['winning_percentage'] = $challenege->winning_percentage;
				$Json[$i]['matchkey'] = $challenege->matchkey;
				$Json[$i]['status'] = 1;
				$Json[$i]['joinedusers'] = $challenege->joinedusers;
				$Json[$i]['multi_entry'] = $challenege->multi_entry;
				$Json[$i]['confirmed_challenge'] = $challenege->confirmed_challenge;
				$Json[$i]['is_running'] = $challenege->is_running;
				$Json[$i]['is_bonus'] = $challenege->bonus;
				$Json[$i]['bonus_percent'] = $challenege->bonus_percent;
				$Json[$i]['isselected'] = false;
				$Json[$i]['isselectedid'] = "";
				$joinedleauges=DB::table('joined_leagues')->where('challengeid',$challenege->id)->where('userid',$user_id)->get();
				if(!empty($joinedleauges)){
					$Json[$i]['isjoined'] = true;
					if($challenege->multi_entry==1 && count($joinedleauges)<6){
						$Json[$i]['isselected'] = false;
						$Json[$i]['refercode'] = $joinedleauges[0]->refercode;
					}else{
						$Json[$i]['isselected'] = true;
						$Json[$i]['refercode'] = $joinedleauges[0]->refercode;
					}
				}else{
					$Json[$i]['isjoined'] = false;
					$Json[$i]['isselected'] = false;
					$Json[$i]['refercode'] = "";
				}
				$winners=0;
				
					$winners=DB::table('matchpricecards')->where('matchkey',$challenege->matchkey)->where('challenge_id',$challenege->id)->sum('winners');
					$Json[$i]['totalwinners'] = $winners ? $winners : 0;
				
				$i++;
			}
			}
		}else{
			$Json[$i]['status'] = 0;
		}
		echo json_encode($Json);
		die;
	}

	public function getscorecards(Request $request){
		$this->accessrules();
		$geturl = $this->geturl();
		$matchkey="";
		if($request->get('matchkey')){
			$matchkey = $request->get('matchkey');
		}
		$challenege_id = $_GET['challenege_id'];
		
		$query1 = DB::table('matchpricecards');
		$query1->where('matchkey',$matchkey)->where('challenge_id',$challenege_id);
		$allpricecard = $query1->orderBY('min_position','ASC')->get();
		$j=0;
		if(!empty($allpricecard)){
			$winners=0;
			foreach($allpricecard as $prc){
				$Json[$j]['id'] = $prc->id;
				$Json[$j]['winners'] = $prc->winners;
				$winners+=$prc->winners;
				$Json[$j]['price'] = $prc->price;
				if($prc->min_position+1!=$prc->max_position){
					$Json[$j]['start_position'] = $prc->min_position+1 .'-'. $prc->max_position;
				}else{
					$Json[$j]['start_position'] = ''.$prc->max_position;
				}
				$Json[$j]['total'] = $prc->total;
				$Json[$j]['description'] = $prc->description;
				$j++;
			}
		}else{
			$Json=array();
		}
		echo json_encode($Json);
		die;
	}


	public function multid_sort($arr, $index) {
			$b = array();
			$c = array();
			foreach ($arr as $key => $value) {
				$b[$key] = $value[$index];
			}
			arsort($b);
			foreach ($b as $key => $value) {
				$c[] = $arr[$key];
			}
			return $c;
		}

		public function searchByValue($products, $field, $value){
			foreach($products as $key => $product)
		   {
			  if ( $product[$field] === $value )
				 return $key;
		   }
		   return false;
		}
	
	public function getglobalchallenges(){
		$this->accessrules();
		$geturl = $this->geturl();
		$matchkey = $_GET['matchkey'];
		$query = DB::table('match_challenges');
		$query->where('grand','1');
		$user_id = $_GET['userid'];
		if($matchkey!=""){
			$query->where('matchkey',$matchkey);
		}
		$allchallenges = $query->orderBY('id','DESC')->get();
		$Json=array();
		$i=0;
		if(!empty($allchallenges)){
			foreach($allchallenges as $challenege){
				if($challenege->maximum_user!=$challenege->joinedusers){
					$Json[$i]['id'] = $challenege->id;
					$Json[$i]['entryfee'] = $challenege->entryfee;
					$Json[$i]['win_amount'] = $challenege->win_amount;
					$Json[$i]['maximum_user'] = $challenege->maximum_user;
					$Json[$i]['matchkey'] = $challenege->matchkey;
					$Json[$i]['status'] = 1;
					$Json[$i]['joinedusers'] = $challenege->joinedusers;
					$getjoinedpercentage = ($challenege->joinedusers/$challenege->maximum_user)*100;
					$Json[$i]['getjoinedpercentage'] = $getjoinedpercentage.'%';
					$Json[$i]['multi_entry'] = $challenege->multi_entry;
					$Json[$i]['confirmed_challenge'] = $challenege->confirmed_challenge;
					$Json[$i]['is_running'] = $challenege->is_running;
					$Json[$i]['isselected'] = false;
					$Json[$i]['isselectedid'] = "";
					$joinedleauges=DB::table('joined_leagues')->where('challengeid',$challenege->id)->where('userid',$user_id)->get();
					if(!empty($joinedleauges)){
						if($challenege->multi_entry==1 && count($joinedleauges)<6){
							if($challenege->joinedusers==$challenege->maximum_user){
								$Json[$i]['isselected'] = true;
							}
							else{
								$Json[$i]['isselected'] = false;
							}
							$Json[$i]['refercode'] = $joinedleauges[0]->refercode;
						}
						
						else{
							$Json[$i]['isselected'] = true;
							$Json[$i]['refercode'] = $joinedleauges[0]->refercode;
						}
					}else{
						$Json[$i]['isselected'] = false;
						$Json[$i]['refercode'] = "";
					}
					$query1 = DB::table('matchpricecards');
					$query1->where('matchkey',$challenege->matchkey)->where('challenge_id',$challenege->id);
					$allpricecard = $query1->orderBY('min_position','ASC')->get();
					$j=0;
					if(!empty($allpricecard)){
						$winners=0;
						foreach($allpricecard as $prc){
							$Json[$i]['price_card'][$j]['id'] = $prc->id;
							$Json[$i]['price_card'][$j]['winners'] = $prc->winners;
							$winners+=$prc->winners;
							$Json[$i]['price_card'][$j]['price'] = $prc->price;
							if($prc->min_position+1!=$prc->max_position){
								$Json[$i]['price_card'][$j]['start_position'] = $prc->min_position+1 .'-'. $prc->max_position;
							}else{
								$Json[$i]['price_card'][$j]['start_position'] = ''.$prc->max_position;
							}
							$Json[$i]['price_card'][$j]['total'] = $prc->total;
							$Json[$i]['price_card'][$j]['description'] = $prc->description;
							$j++;
						}
						$Json[$i]['totalwinners'] = $winners;
					}
					$i++;
				}else{
					$Json[$i]['status'] = 0;
				}
			}
		}else{
			$Json[$i]['status'] = 0;
		}
		echo json_encode($Json);
		die;
	}
	
	
	
	
	public function getmatchlist(){
		$this->accessrules();
		$geturl = $this->geturl();
		$currentdate = date('Y-m-d');
		$query = DB::table('listmatches');
		if(isset($_GET['series'])){
			$series = $_GET['series'];
			$query->where('listmatches.series',$series);
		}
// 		$findmatches = $query->leftjoin('series','listmatches.series','=','series.id')->join('teams as t1','t1.id','=','listmatches.team1')->join('teams as t2','t2.id','=','listmatches.team2')->select('t1.logo as team1logo','t2.logo as team2logo','t1.team_key as team1key','t2.team_key as team2key','listmatches.id as listmatchid','series.name as seriesname','listmatches.name','listmatches.start_date','listmatches.format','listmatches.matchkey','listmatches.final_status')->whereDate('listmatches.start_date','>=',$currentdate)->Where('listmatches.launch_status','launched')->orderBY('listmatches.start_date','ASC')->get();
		$findmatches = $query->leftjoin('series','listmatches.series','=','series.id')->join('teams as t1','t1.id','=','listmatches.team1')->join('teams as t2','t2.id','=','listmatches.team2')->select('t1.logo as team1logo','t2.logo as team2logo','t1.team_key as team1key','t2.team_key as team2key','listmatches.id as listmatchid','series.name as seriesname','listmatches.name','listmatches.start_date','listmatches.format','listmatches.matchkey','listmatches.final_status','listmatches.launch_status')->whereDate('listmatches.start_date','>=',$currentdate)->Where('listmatches.series','!=','0')->orderBY('listmatches.start_date','ASC')->get();
		$Json=array();
		if(!empty($findmatches)){
			$i=0;
			foreach($findmatches as $match){
				$Json[$i]['id'] = $match->listmatchid;
				$Json[$i]['name'] = $match->name;
				$Json[$i]['format'] = $match->format;
				$Json[$i]['seriesname'] = $match->seriesname;
				$Json[$i]['team1name'] = $match->team1key;
				$Json[$i]['team2name'] = $match->team2key;
				$Json[$i]['matchkey'] = $match->matchkey;
				$Json[$i]['launch_status'] = $match->launch_status;
				$Json[$i]['winnerstatus'] = $match->final_status;
				if($match->team1logo!=""){
					$Json[$i]['team1logo'] = $geturl.'uploads/teams/'.$match->team1logo;
				}else{
					$Json[$i]['team1logo'] = $geturl.'images/logo.png';
				}
				if($match->team2logo!=""){
					$Json[$i]['team2logo'] = $geturl.'uploads/teams/'.$match->team2logo;
				}else{
					$Json[$i]['team2logo'] = $geturl.'images/logo.png';
				}
				$matchdate = date('Y-m-d h:i:s',strtotime($match->start_date));
			//	$Json[$i]['time_start'] = Carbon::parse($matchdate)->subHours(1); 
				if(isset($_GET['userid'])){
					$finduserinfo = DB::table('register_users')->where('id',$_GET['userid'])->select('id')->first();
					if(!empty($finduserinfo)){
						$getid = $finduserinfo->id;
						$findjointeam = DB::table('join_teams')->where('userid',$getid)->where('matchkey',$match->matchkey)->orderBY('id','DESC')->get();
						if(!empty($findjointeam)){
							$Json[$i]['createteamnumber'] = $findjointeam[0]->teamnumber+1;
						}else{
							$Json[$i]['createteamnumber'] = 1;
						}
					}
				}
				$i++;
			}
		}
		else{
			$Json[0]['status'] = '0';
		}
		echo json_encode($Json);
		die;
	}
	public function showmyteaminmobile(){
		$this->accessrules();
		$matchkey = $_GET['matchkey'];
		$userid = $_GET['userid'];
		$findjointeam = DB::table('join_teams')->where('matchkey',$_GET['matchkey'])->where('userid',$_GET['userid'])->orderBy('teamnumber','ASC')->select('*')->get();
		$Json=array();
		if(!empty($findjointeam)){
			$i=0;
			foreach($findjointeam as $jointeams){
				$Json[$i]['teamnumber'] = $jointeams->teamnumber;
				$Json[$i]['teamid'] = $jointeams->id;
				$playersarr = explode(',',$jointeams->players);
				$playerdetails = DB::table('matchplayers')->whereIn('matchplayers.playerid',$playersarr)->where('matchkey',$jointeams->matchkey)->join('players','matchplayers.playerid','=','players.id')->select('players.team','matchplayers.role as playerrole','matchplayers.credit as playercredit','matchplayers.role','players.player_name as playername','matchplayers.playerid as pid')->get();
				$all=0;$bats=0;$bow=0;$keeper=0;$captain="";$vicecaptain="";
				foreach($playerdetails as $play){
					if($play->playerrole=='bowler'){
						$bow++;
					}if($play->playerrole=='allrounder'){
						$all++;
					}if($play->playerrole=='batsman'){
						$bats++;
					}
					if($play->playerrole=='keeper'){
						$keeper++;
					}
					if($play->pid==$jointeams->captain){
						$captain = $play->playername;
					}if($play->pid==$jointeams->vicecaptain){
						$vicecaptain = $play->playername;
					}
				}
				$Json[$i]['bow'] = $bow;
				$Json[$i]['all'] = $all;
				$Json[$i]['bats'] = $bats;
				$Json[$i]['keeper'] = $keeper;
				$Json[$i]['captain'] = $captain;
				$Json[$i]['vicecaptain'] = $vicecaptain;
				$i++;
			}
		}
		echo json_encode($Json);
		die;
	}
	
    public function getplayerlist(){
        $this->accessrules();
        $matchkey = $_GET['matchkey'];
        $Json = array();
        $findmatch = DB::table('listmatches')->where('matchkey',$matchkey)->join('teams as t1','t1.id','=','listmatches.team1')->join('teams as t2','t2.id','=','listmatches.team2')->select('listmatches.team1','listmatches.series','listmatches.team2','t1.short_name as team1display','t2.short_name as team2display','t1.color as team1color','t2.color as team2color','listmatches.start_date')->first();

        if(!empty($findmatch)){
            $team1 = $findmatch->team1;
            $team2 = $findmatch->team2;
			$getseries = $findmatch->series;
			$allmatchkey = array();
			$findallmatches = DB::table('listmatches')->where('series',$getseries)->select('listmatches.matchkey')->get();
			if(!empty($findallmatches)){
				foreach($findallmatches as $matches){
					$allmatchkey[] = $matches->matchkey;
				}
			}
			
			$playing11 = array();
			$getcurrentdate = date('Y-m-d H:i:s');
			$tosstime = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($findmatch->start_date)));
			if($getcurrentdate>$tosstime){
				$find_playing_11 = DB::table('match_playing11')->where('match_key', $matchkey)->first();
				if(empty($find_playing_11)) {
					$playing11 = $this->get_playing_11($matchkey);
				} else {
					$playing11 = unserialize($find_playing_11->player_ids);
				}				
			}
            $findpplayers = DB::table('matchplayers')->where('matchkey',$matchkey)->join('players','matchplayers.playerid','=','players.id')->select('players.team','matchplayers.role as playerrole','matchplayers.credit as playercredit','matchplayers.role','players.player_name as playername','players.id as pid','players.player_key as playerkey')->orderBy('matchplayers.credit','DESC')->get();
            // print_r($playing11); exit;
            if(!empty($findpplayers)){
                $i=0;
                $total_teams = DB::table('join_teams')->where('matchkey', $matchkey)->select(DB::raw("COUNT(id) as total_count"))->get();
                $totalTeams = $total_teams[0]->total_count;
                foreach($findpplayers as $pp){
					$findallplayers = DB::table('matchplayers')->whereIn('matchkey',$allmatchkey)->where('playerid',$pp->pid)->select(DB::raw("SUM(points) as totalpoints"))->get();
					
					$total_choose = DB::table('join_teams')->whereRaw('FIND_IN_SET('.$pp->pid.',players)')->where('matchkey', $matchkey)->select(DB::raw("COUNT(id) as total_count"))->get();

					$total_selected = $total_choose[0]->total_count;
					if($total_selected!=0){
					$selected_by = round((($total_selected * 100) / $totalTeams), 2);
					$selected_by = $selected_by.' %';
				    }
				    else  $selected_by = 0;

					$Json[$i]['id'] = $pp->pid;
                    $Json[$i]['name'] = $pp->playername;
                    $Json[$i]['role'] = $pp->playerrole;
                    $Json[$i]['credit'] = $pp->playercredit;
                    $Json[$i]['playerkey'] = $pp->playerkey;
                    $Json[$i]['totalpoints'] = $findallplayers[0]->totalpoints;
                    $Json[$i]['isSelected'] =false;
                    $Json[$i]['selected_by'] =$selected_by;
                    $Json[$i]['playing_11'] = in_array($pp->pid,$playing11) ? 1 : 0;
                  
                    if($pp->team==$team1){
                        $Json[$i]['team'] = 'team1';
                        $Json[$i]['teamname'] = strtoupper($findmatch->team1display);
                        $Json[$i]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team1color));
                    }
                    if($pp->team==$team2){
                        $Json[$i]['team'] = 'team2';
                        $Json[$i]['teamname'] = strtoupper($findmatch->team2display);
                        $Json[$i]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team2color));
                    }
                    $i++;
                }
            }
        }
        echo json_encode($Json);
        die;
	}

	public static function get_playing_11($matchkey) {

			$giveresresult = CricketapiController::getmatchdetails($matchkey);
			if(!empty($giveresresult)){
				$mainarrayget = $giveresresult['data']['card'];
				$findteams = $mainarrayget['teams']; //print_r($findteams); exit;
				if(!empty($findteams)){ 
					$finalplayingteams = array();
					$players = array();
					foreach($findteams as $keytp => $tp){
						if(isset($tp['match']['playing_xi'])){
							$findpl = $tp['match']['playing_xi'];
							if(!empty($findpl)){
								foreach($findpl as $fl){
									$finalplayingteams[] = $fl;
									$player_details = DB::table('players')->join('matchplayers','players.id','=','matchplayers.playerid')->where('players.player_key',$fl)->where('matchplayers.matchkey', $matchkey)->select('players.id')->first();								
									if(!empty($player_details)) {
										$players[] = $player_details->id;
										
									}
								}
							}
						}
						
						
					}
					//print_r($players)
					if(count($players)==22) {
					$insert_data = array(
						"match_key" => $matchkey,
						"player_ids" => serialize($players),
					);
					DB::table('match_playing11')->insert($insert_data);
					return $players;
					} else {
						return array();
					}

				}
				return array();
			}
			return array();
		
	}

	public static function check_playing_11($player_id, $matchkey) { return 0;
		$player = DB::table('players')->where('id',$player_id)->select('player_key')->first();
		$player_key = $player->player_key;
		$findmatchdetails = DB::table('listmatches')->where('matchkey',$matchkey)->select('start_date')->first();
		$getcurrentdate = date('Y-m-d H:i:s');
		$tosstime = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($findmatchdetails->start_date)));
		if($getcurrentdate>$tosstime){
			$find_playing_11 = DB::table('match_playing11')->where('match_key', $matchkey)->get();
			if(empty($find_playing_11)) {
			$giveresresult = CricketapiController::getmatchdetails($matchkey);
			if(!empty($giveresresult)){
				$mainarrayget = $giveresresult['data']['card'];
				$findteams = $mainarrayget['teams'];
				if(!empty($findteams)){
					$finalplayingteams = array();
					foreach($findteams as $keytp => $tp){
						if(isset($tp['match']['playing_xi'])){
							$findpl = $tp['match']['playing_xi'];
							if(!empty($findpl)){
								foreach($findpl as $fl){
									$finalplayingteams[] = $fl;
									$player_details = DB::table('players')->where('player_key',$fl)->select('id')->first();								
									if(!empty($player_details)) {
										
										$insert_data = array(
											"match_key" => $matchkey,
											"player_id" => $player_details->id
										);
										DB::table('match_playing11')->insert($insert_data);
									}
								}
							}
						}
						
						
					}
					if(in_array($player_key,$finalplayingteams)){
							return 1;
						} else {
							return 0;
						}

				}
			}
			return 0;
		} else {
			$check_player = DB::table('match_playing11')->where('match_key', $matchkey)->where('player_id', $player_id)->get();
			if(!empty($check_player)) {
				return 1;
			} else {
				return 0;
			}
		}
			
		} else {
			return 0;
		}
	}

	public function playerfullinfo(){
		$playerid = $_GET['playerid'];
		$matchkey = $_GET['matchkey'];
		$findplayerdetails = DB::table('matchplayers')->join('players','players.id','=','matchplayers.playerid')->where('matchkey',$matchkey)->where('playerid',$playerid)->select('players.player_name as playername','matchplayers.credit as playercredit','matchplayers.role as playerrole','players.points as playerpoints','players.image as playerimage')->first();
		$Json = array();
		if(!empty($findplayerdetails)){
			$Json[0]['playername'] = $findplayerdetails->playername;
			$Json[0]['playercredit'] = $findplayerdetails->playercredit;
			$Json[0]['playerimage'] = $findplayerdetails->playerimage;
			$Json[0]['playerpoints'] = $findplayerdetails->playerpoints;
			$Json[0]['playerrole'] = $findplayerdetails->playerrole;
		}
		echo json_encode($Json);
        die;
	}
public function findjointeam(){
	$this->accessrules();
	$matchkey = $_GET['matchkey'];
	$userid = $_GET['userid'];
	$findjointeam = DB::table('join_teams')->where('matchkey',$_GET['matchkey'])->where('userid',$_GET['userid'])->orderBy('teamnumber','ASC')->select('id','teamnumber')->get();
	$Json=array();
	if(!empty($findjointeam)){
		$i=0;
		foreach($findjointeam as $jointeam){
			$Json[$i]['teamnumber'] = $jointeam->teamnumber;
			$Json[$i]['teamid'] = $jointeam->id;
			$i++;
		}
	}
	echo json_encode($Json);
	die;
}
public function findjointeamofusers(){
	$this->accessrules();
	$matchkey = $_GET['matchkey'];
	$userid = $_GET['userid'];
	$loginid = $_GET['loginid'];
	if(isset($_GET['challengeid'])){
		$challengeid = $_GET['challengeid'];
		if($userid==$loginid){
			$findjointeam = DB::table('join_teams')->where('matchkey',$_GET['matchkey'])->where('userid',$_GET['userid'])->orderBy('teamnumber','ASC')->select('id','teamnumber','points')->get();
			$Json=array();
			if(!empty($findjointeam)){
				$i=0;
				foreach($findjointeam as $jointeam){
					$Json[$i]['teamnumber'] = $jointeam->teamnumber;
					$Json[$i]['teamid'] = $jointeam->id;
					$Json[$i]['points'] = $jointeam->points;
					$Json[$i]['userid'] = $_GET['userid'];
					$Json[$i]['challengeid'] = $_GET['challengeid'];
					$i++;
				}
			}
		}
		else{
			$findjointeam = DB::table('join_teams')->join('joined_leagues','joined_leagues.teamid','=','join_teams.id')->where('join_teams.matchkey',$_GET['matchkey'])->where('join_teams.userid',$_GET['userid'])->where('joined_leagues.challengeid',$_GET['challengeid'])->orderBy('teamnumber','ASC')->select('join_teams.id','teamnumber','points')->get();
			$Json=array();
			if(!empty($findjointeam)){
				$i=0;
				foreach($findjointeam as $jointeam){
					$Json[$i]['teamnumber'] = $jointeam->teamnumber;
					$Json[$i]['teamid'] = $jointeam->id;
					$Json[$i]['points'] = $jointeam->points;
					$Json[$i]['userid'] = $_GET['userid'];
					$Json[$i]['challengeid'] = $_GET['challengeid'];
					$i++;
				}
			}
		}
	}
	else{
		
		$findjointeam = DB::table('join_teams')->where('join_teams.matchkey',$_GET['matchkey'])->where('join_teams.userid',$_GET['userid'])->orderBy('teamnumber','ASC')->select('join_teams.id','teamnumber','points')->get();
		
			$Json=array();
			if(!empty($findjointeam)){
				$i=0;
				foreach($findjointeam as $jointeam){
					$Json[$i]['teamnumber'] = $jointeam->teamnumber;
					$Json[$i]['teamid'] = $jointeam->id;
					$Json[$i]['points'] = $jointeam->points;
					$Json[$i]['userid'] = $_GET['userid'];
					$Json[$i]['challengeid'] = "";
					$i++;
				}
			}
	}
	
	
	echo json_encode($Json);
	die;
}
public function jointeamlist(){
		$this->accessrules();
		
		if((isset($_GET['matchkey'])) &&(isset($_GET['teamnumber']))){
            $jointeam = DB::table('join_teams')->where('matchkey',$_GET['matchkey'])->where('teamnumber',$_GET['teamnumber'])->select('*')->get();
        }else{
            $matchkey = $_GET['teamid'];
            $jointeam = DB::table('join_teams')->where('id',$_GET['teamid'])->select('*')->get();
        }
		
		// $matchkey = $_GET['teamid'];
		// $jointeam = DB::table('join_teams')->where('id',$_GET['teamid'])->select('*')->get();
		$Json=array();
		$bowlers=array();
		$batsman=array();
		$keeper=array();
		$allrounder=array();
		if(!empty($jointeam)){
			foreach($jointeam as $team){
				$playersarr = explode(',',$team->players);
				$findmatch = DB::table('listmatches')->where('matchkey',$team->matchkey)->join('teams as t1','t1.id','=','listmatches.team1')->join('teams as t2','t2.id','=','listmatches.team2')->select('listmatches.team1','listmatches.team2','listmatches.team1display','listmatches.team2display','t1.color as team1color','t2.color as team2color')->first();
				$boplayerdetails = DB::table('matchplayers')->whereIn('matchplayers.playerid',$playersarr)->where('matchplayers.role','bowler')->where('matchkey',$team->matchkey)->join('players','matchplayers.playerid','=','players.id')->select('players.team','matchplayers.role as playerrole','matchplayers.credit as playercredit','matchplayers.role','players.player_name as playername','matchplayers.playerid as pid','matchplayers.points as totalpoints')->get();
				$batplayerdetails = DB::table('matchplayers')->whereIn('matchplayers.playerid',$playersarr)->where('matchplayers.role','batsman')->where('matchkey',$team->matchkey)->join('players','matchplayers.playerid','=','players.id')->select('players.team','matchplayers.role as playerrole','matchplayers.credit as playercredit','matchplayers.role','players.player_name as playername','matchplayers.playerid as pid','matchplayers.points as totalpoints')->get();
				$keeprdetails = DB::table('matchplayers')->whereIn('matchplayers.playerid',$playersarr)->where('matchplayers.role','keeper')->where('matchkey',$team->matchkey)->join('players','matchplayers.playerid','=','players.id')->select('players.team','matchplayers.role as playerrole','matchplayers.credit as playercredit','matchplayers.role','players.player_name as playername','matchplayers.playerid as pid','matchplayers.points as totalpoints')->get();
				$allrundetails = DB::table('matchplayers')->whereIn('matchplayers.playerid',$playersarr)->where('matchplayers.role','allrounder')->where('matchkey',$team->matchkey)->join('players','matchplayers.playerid','=','players.id')->select('players.team','matchplayers.role as playerrole','matchplayers.credit as playercredit','matchplayers.role','players.player_name as playername','matchplayers.playerid as pid','matchplayers.points as totalpoints')->get();
				if(!empty($boplayerdetails)){
					$j=0;
					foreach($boplayerdetails as $bowler){
						$Json['bowler'][$j]['id'] = $bowler->pid;
						$Json['bowler'][$j]['player_name'] = $bowler->playername;
						$Json['bowler'][$j]['role'] = $bowler->playerrole;
						$Json['bowler'][$j]['credit'] = $bowler->playercredit;
						$Json['bowler'][$j]['points'] = $bowler->totalpoints;
						if($bowler->team==$findmatch->team1){
							$Json['bowler'][$j]['team'] = 'team1';
							$Json['bowler'][$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team1color));
						}
						if($bowler->team==$findmatch->team2){
							$Json['bowler'][$j]['team'] = 'team2';
							$Json['bowler'][$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team2color));
						}
						$vicecaptain=0;
						$captain=0;
						if($team->vicecaptain==$bowler->pid){
							$vicecaptain=1;
						}
						if($team->captain==$bowler->pid){
							$captain=1;
						}
						$Json['bowler'][$j]['vicecaptain'] = $vicecaptain;
						$Json['bowler'][$j]['captain'] = $captain;
						$j++;
					}
				}
			}
			if(!empty($batplayerdetails)){
					$j=0;
					foreach($batplayerdetails as $batman){
						$Json['batsman'][$j]['id'] = $batman->pid;
						$Json['batsman'][$j]['player_name'] = $batman->playername;
						$Json['batsman'][$j]['role'] = $batman->playerrole;
						$Json['batsman'][$j]['credit'] = $batman->playercredit;
						$Json['batsman'][$j]['points'] = $batman->totalpoints;
						if($batman->team==$findmatch->team1){
							$Json['batsman'][$j]['team'] = 'team1';
							$Json['batsman'][$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team1color));
						}
						if($batman->team==$findmatch->team2){
							$Json['batsman'][$j]['team'] = 'team2';
							$Json['batsman'][$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team2color));
						}
						$vicecaptain=0;
						$captain=0;
						if($team->vicecaptain==$batman->pid){
							$vicecaptain=1;
						}
						if($team->captain==$batman->pid){
							$captain=1;
						}
						$Json['batsman'][$j]['vicecaptain'] = $vicecaptain;
						$Json['batsman'][$j]['captain'] = $captain;
						$j++;
					}
				}
				if(!empty($keeprdetails)){
					$j=0;
					foreach($keeprdetails as $keeper){
						$Json['keeper'][$j]['id'] = $keeper->pid;
						$Json['keeper'][$j]['player_name'] = $keeper->playername;
						$Json['keeper'][$j]['role'] = $keeper->playerrole;
						$Json['keeper'][$j]['credit'] = $keeper->playercredit;
						$Json['keeper'][$j]['points'] = $keeper->totalpoints;
						
						if($keeper->team==$findmatch->team1){
							$Json['keeper'][$j]['team'] = 'team1';
							$Json['keeper'][$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team1color));
						}
						if($keeper->team==$findmatch->team2){
							$Json['keeper'][$j]['team'] = 'team2';
							$Json['keeper'][$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team2color));
						}
						$vicecaptain=0;
						$captain=0;
						if($team->vicecaptain==$keeper->pid){
							$vicecaptain=1;
						}
						if($team->captain==$keeper->pid){
							$captain=1;
						}
						$Json['keeper'][$j]['vicecaptain'] = $vicecaptain;
						$Json['keeper'][$j]['captain'] = $captain;
						$j++;
					}
				}
				if(!empty($allrundetails)){
					$j=0;
					foreach($allrundetails as $allrounder){
						$Json['allrounder'][$j]['id'] = $allrounder->pid;
						$Json['allrounder'][$j]['player_name'] = $allrounder->playername;
						$Json['allrounder'][$j]['role'] = $allrounder->playerrole;
						$Json['allrounder'][$j]['credit'] = $allrounder->playercredit;
						$Json['allrounder'][$j]['points'] = $allrounder->totalpoints;
						if($allrounder->team==$findmatch->team1){
							$Json['allrounder'][$j]['team'] = 'team1';
							$Json['allrounder'][$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team1color));
							
						}
						if($allrounder->team==$findmatch->team2){
							$Json['allrounder'][$j]['team'] = 'team2';
							$Json['allrounder'][$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team2color));
						}
						$vicecaptain=0;
						$captain=0;
						if($team->vicecaptain==$allrounder->pid){
							$vicecaptain=1;
						}
						if($team->captain==$allrounder->pid){
							$captain=1;
						}
						$Json['allrounder'][$j]['vicecaptain'] = $vicecaptain;
						$Json['allrounder'][$j]['captain'] = $captain;
						$j++;
					}
				}
			
			}
			echo json_encode(array($Json));
		die;
	}
	public function getmatchdetails(){
		$this->accessrules();
		$geturl = $this->geturl();
		$matchkey = $_GET['matchkey'];
		$findmatches = DB::table('listmatches')->leftjoin('series','listmatches.series','=','series.id')->join('teams as t1','t1.id','=','listmatches.team1')->join('teams as t2','t2.id','=','listmatches.team2')->select('t1.logo as team1logo','t2.logo as team2logo','t1.short_name as team1key','t2.short_name as team2key','listmatches.id as listmatchid','series.name as seriesname','listmatches.name','listmatches.start_date','listmatches.format','listmatches.matchkey')->Where('listmatches.matchkey',$matchkey)->get();
		$Json=array();
		if(!empty($findmatches)){
			$i=0;
			foreach($findmatches as $match){
				$Json[$i]['id'] = $match->listmatchid;
				$Json[$i]['name'] = $match->name;
				$Json[$i]['format'] = $match->format;
				$Json[$i]['seriesname'] = $match->seriesname;
				$Json[$i]['team1name'] = $match->team1key;
				$Json[$i]['team2name'] = $match->team2key;
				$Json[$i]['matchkey'] = $match->matchkey;
				if($match->team1logo!=""){
					$Json[$i]['team1logo'] = $geturl.'uploads/teams/'.$match->team1logo;
				}else{
					$Json[$i]['team1logo'] = $geturl.'images/Fanadda-logo.png';
				}
				if($match->team2logo!=""){
					$Json[$i]['team2logo'] = $geturl.'uploads/teams/'.$match->team2logo;
				}else{
					$Json[$i]['team2logo'] = $geturl.'images/Fanadda-logo.png';
				}
				$Json[$i]['time_start'] = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($match->start_date)));
				$i++;
			}
		}
		else{
			$Json[0]['status'] = '0';
		}
		echo json_encode($Json);
		die;
	}
	public function joinleauge(){
		$this->accessrules();
		$matchkey = $data['matchkey'] =  $_GET['matchkey'];
		$userid =  $data['userid'] =  $_GET['userid'];
		$challengeid =  $data['challengeid'] = $_GET['challengeid'];
		$teamid =  $data['teamid'] = $_GET['teamid'];
		$findjoinedleauges = DB::table('joined_leagues')->where('teamid',$_GET['teamid'])->where('challengeid',$challengeid)->where('matchkey',$matchkey)->where('userid',$userid)->first();
		$Json = array();
		$refercode = $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$refercode = '';
		 $max = strlen($characters) - 1;
		 for ($i = 0; $i < 6; $i++) {
			  $refercode.= $characters[mt_rand(0, $max)];
		 }
		 $data['refercode'] = $refercode;
		if(empty($findjoinedleauges)){
			$getinsertid = DB::table('joined_leagues')->insertGetId($data);
			$data['refercode'] = $refercode.''.$getinsertid;
			DB::table('joined_leagues')->where('id',$getinsertid)->update($data);
			$findchallenge = DB::table('match_challenges')->where('id',$challengeid)->select('joinedusers')->first();
			$updatedata['joinedusers'] = $findchallenge->joinedusers+1;
			DB::table('match_challenges')->where('id',$challengeid)->update($updatedata);
			$Json[0]['status'] = true;
		}else{
			$Json[0]['status'] = false;
		}
		echo json_encode($Json);
		die;
	}
	public function jointeamlist1(){
		$this->accessrules();
        if((isset($_GET['matchkey'])) && (isset($_GET['teamnumber']))){
            $jointeam = DB::table('join_teams')->where('matchkey',$_GET['matchkey'])->where('teamnumber',$_GET['teamnumber'])->where('userid',$_GET['userid'])->select('*')->get();
            $Json=array();
            if(!empty($jointeam)){
                foreach($jointeam as $team){
                    $playersarr = explode(',',$team->players);
					$findmatch = DB::table('listmatches')->where('matchkey',$team->matchkey)->join('teams as t1','t1.id','=','listmatches.team1')->join('teams as t2','t2.id','=','listmatches.team2')->select('listmatches.team1','listmatches.team2','listmatches.team1display','listmatches.team2display','t1.color as team1color','t2.color as team2color')->first();
                    $boplayerdetails = DB::table('matchplayers')->whereIn('matchplayers.playerid',$playersarr)->where('matchkey',$team->matchkey)->join('players','matchplayers.playerid','=','players.id')->select('players.team','matchplayers.role as playerrole','matchplayers.credit as playercredit','matchplayers.role','players.player_name as playername','matchplayers.playerid as pid','players.points')->get();
                    if(!empty($boplayerdetails)){
                        $j=0;
                        foreach($boplayerdetails as $bowler){
                            $Json[$j]['id'] = $bowler->pid;
                            $Json[$j]['name'] = $bowler->playername;
                            $Json[$j]['role'] = $bowler->playerrole;
                            $Json[$j]['credit'] = $bowler->playercredit;
                            $Json[$j]['totalpoints'] = $bowler->points;
                            if($bowler->team==$findmatch->team1){
                                $Json[$j]['team'] = 'team1';
                                $Json[$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team1color));
                            }
                            if($bowler->team==$findmatch->team2){
                                $Json[$j]['team'] = 'team2';
								$Json[$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team2color));
                            }
                            $vicecaptain=0;
                            $captain=0;
                            if($team->vicecaptain==$bowler->pid){
                                $vicecaptain=1;
                            }
                            if($team->captain==$bowler->pid){
                                $captain=1;
                            }
                            $Json[$j]['vicecaptain'] = $vicecaptain;
                            $Json[$j]['captain'] = $captain;
                            $Json[$j]['isSelected'] = true;
                            $j++;
                        }
                    }
                }
            }
            echo json_encode($Json);
            die;
        }
    }
    
	public function paymentfrompayumoney(){
		$this->accessrules();
		$amount = $_GET['amount'];
		$id = $_GET['id'];
		
		$loginsession = DB::table('register_users')->where(array('id'=>$id))->first();
		if(!empty($loginsession)){
			$paymentdata['amount']=$amount;
			$paymentdata['userid']=$loginsession->id;
			$paymentdata['username']=$loginsession->username;
			$paymentdata['mobile']=$loginsession->mobile;
			$paymentdata['email']=$loginsession->email;
			$paymentdata['unique_id']=$loginsession->unique_id;
			$paymentdata['paymentby']='Payu';
		}

		Session::put('askforpayment',$paymentdata);
		
		$MERCHANT_KEY=Config::get('constants.PAYUMONEY_MERCHANTKEY');
		$SALT=Config::get('constants.PAYUMONEY_SALT');
		$txnid= 'Fanadda-'.rand(1000000,99999);
		
		$surl=Config::get('constants.PROJECT_URL').'successpayumoney';
		$furl=Config::get('constants.PROJECT_URL').'failurepayumoney';
		// $loginsession = Session::get('mimessay_reg_user');
		$getvalues['key'] = $MERCHANT_KEY;
		$getvalues['salt'] = $SALT;
		$getvalues['txnid'] = $txnid;
		$getvalues['amount'] = $amount;
		$getvalues['firstname'] = ucwords($loginsession->username);
		$getvalues['email'] = $loginsession->email;
		$getvalues['phone'] = $loginsession->mobile;
		$getvalues['surl'] = $surl;
		$getvalues['productinfo'] = 'Transaction From slect2win for cash added';
		$getvalues['furl'] = $furl;
		return view('api.paymentfrompayumoney',compact('getvalues'));
	}
	
	public function successpayumoney(){
		$this->accessrules();
		$returnurl = $this->geturltoreturn();
		$status=$_POST["status"];
		$firstname=$_POST["firstname"];
		$amount=$_POST["amount"];
		$txnid=$_POST["txnid"];
		$posted_hash=$_POST["hash"];
		$key=$_POST["key"];
		$productinfo=$_POST["productinfo"];
		$email=$_POST["email"];
		$salt=Config::get('constants.PAYUMONEY_SALT');
		if(isset($_POST["additionalCharges"])) {
			$additionalCharges=$_POST["additionalCharges"];
			$retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
		}
		else {   
			$retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
		}
		$hash = hash("sha512", $retHashSeq);
		if ($hash != $posted_hash) 
		{
			// echo 'Transaction status is failure. Please Try again';
			header('Location: '.$returnurl.'?msg=failed');die;
			$msgg['msg'] = "Payment is Fail,Please try again";
			$msgg['status'] = 0;
			echo json_encode(array($msgg));die;
		}
		else {
			$paymentgatewayinfo['amount'] = $amount;
			$paymentgatewayinfo['txnid'] = $txnid;
			$paymentgatewayinfo['paymentby'] = 'payumoney';
			$returnamount = $this->requestprocess($paymentgatewayinfo);
			if($returnamount=='success'){
			    header('Location: '.$returnurl.'?msg=success');die;
				$msgg['msg'] = "Amount successfully added to your wallet";
				$msgg['status'] = 1;
				echo json_encode(array($msgg));die;
			}else{
			    header('Location: '.$returnurl.'?msg=failed');die;
				$msgg['msg'] = "Payment is Fail,Please try again";
				$msgg['status'] = 0;
				echo json_encode(array($msgg));die;
				
			}
		}
	}
	
 	public function requestprocess($paymentgatewayinfo){
			$this->accessrules();
		 if(Session::has('askforpayment')){
			$getdata = Session::get('askforpayment');
			$getdata['amount']=floor($getdata['amount']);
			$paymentby=$getdata['paymentby'];
			//update user balance//
			$userdata = DB::table('user_balances')->where('user_id',$getdata['userid'])->first();
			if(!empty($userdata)){
				$datainseert['user_id'] = $getdata['userid'];
				$datainseert['balance'] = $userdata->balance+$getdata['amount'];
				DB::table('user_balances')->where('user_id',$getdata['userid'])->update($datainseert);
			}
	
			$userid=$getdata['userid'];
			$amount=$paymentgatewayinfo['amount'];
			
			$bal_bonus_amt=0;$bal_win_amt=0;$bal_fund_amt=0;$total_available_amt=0;
			$findlastow = DB::table('user_balances')->where('user_id',$userid)->first();
			if(!empty($findlastow)){
				$total_available_amt = $findlastow->balance+$findlastow->winning+$findlastow->bonus;
				$bal_fund_amt = $findlastow->balance;
				$bal_win_amt = $findlastow->winning;
				$bal_bonus_amt = $findlastow->bonus;
			}
			
			$notificationdata['userid'] = $userid;
			$notificationdata['title'] = 'Added Fund of Rs '.$amount.' through '.$paymentby.'.';
			DB::table('notifications')->insert($notificationdata);
			//push notifications//
			$titleget = 'Added fund successfully!';
			Helpers::sendnotification($titleget,$notificationdata['title'],'',$userid);
			//end push notifications//
			$transactionsdata['userid'] = $userid;
			$transactionsdata['type'] = 'Cash Added';
			$transactionsdata['transaction_id'] = $paymentgatewayinfo['txnid'];
			$transactionsdata['transaction_by'] = $getdata['paymentby'];
			$transactionsdata['amount'] = $amount;
			$transactionsdata['addfund_amt'] = $amount;
			$transactionsdata['paymentstatus'] = 'confirmed';
			$transactionsdata['bal_fund_amt'] = $bal_fund_amt;
			$transactionsdata['bal_win_amt'] = $bal_win_amt;
			$transactionsdata['bal_bonus_amt'] = $bal_bonus_amt;
			//$transactionsdata['cons_amount'] = $amount;
			$transactionsdata['total_available_amt'] = $total_available_amt;
			DB::table('transactions')->insert($transactionsdata);
			
			
			//delete sessions//
		    Session::forget('askforpayment');
			return 'success';
		    
		}
		else{
			return 'fail';
		} 
	}
	
	public function androidaddfundapi(){
		$getdata['amount']=floor($_GET['amount']);
		$userid= $getdata['userid']= $_GET['userid'];
		$getdata['paymentby']= $_GET['paymentby'];
		$offer_id = $_GET['offer_id'];
		$txnid = 'BBF-'.rand(1000,9999).''.$getdata['userid'];
		
		// bonus
		    $bonus_data = 0;
			if($offer_id != ""){
			    $offer = DB::table('offers')->where('minamount',$getdata['amount'])->where('code',$offer_id)->first();
			    if(!empty($offer)){
			        $bonus_data = $offer->bonus;
			    }
			    
			    $usedB = DB::table('usedoffers')->where('user_id',$userid)->where('offer_id',$offer_id)->first();
			    if(!empty($usedB)){
			        $bonus_data = 0;
			    }else{
			        $bonusdata['user_id'] = $userid;
			        $bonusdata['offer_id'] = $offer_id;
			        DB::table('usedoffers')->insert($bonusdata);
			    }
			}
			
			
		//update user balance//
			$userdata = DB::table('user_balances')->where('user_id',$getdata['userid'])->first();

			$userinfo = DB::table('register_users')->where('id',$getdata['userid'])->first();
			$referid = $userinfo->refer_id;
			


			if(!empty($referid) || $referid != 0){
				$userBalanceReffred= DB::table('user_balances')->where('user_id',$referid)->first();
				$referdata = DB::table('register_users')->where('id',$referid)->first();
			    $bal_bonus_amt=0;$bal_win_amt=0;$bal_fund_amt=0;$total_available_amt=0;
			    $bal_bonus_amt = $userBalanceReffred->bonus;
			    $bal_win_amt = $userBalanceReffred->winning;
			    $bal_fund_amt = $userBalanceReffred->balance;
			    $total_available_amt = $bal_bonus_amt+$bal_win_amt+$bal_fund_amt;
			    $bonus = DB::table('bonus_amount')->where('type',"Add Fund Bonus")->first();
			    $bonus_type = $bonus->bonus_type;
			    $price = $bonus->price;
			 //   echo '<pre>';print_r($userBalance);die;
			    if(!empty($userBalanceReffred)){
			    	$amount_added = 0;
			    	$balance_refer = 0;
			    	if(!empty($referdata) && $referdata->is_youtuber==1) {
			    		if($referdata->youtuber_bonus==1) {
			    			$bonus_percent = $referdata->bonus_percent;
			    			if($bonus_percent) {
			    				$balance_refer=$datainseertt['winning'] = $userBalanceReffred->winning+(($getdata['amount']*$bonus_percent)/100);
			            		$amount_added = $getdata['amount']*$bonus_percent/100;
			    			}
			    		}
			    	} else {
			        	if($bonus_type == 'amount'){
			            	$balance_refer=$datainseertt['bonus'] = $userBalanceReffred->bonus+$price;
			            	$amount_added = $price;
			        	}else{
			            	$balance_refer=$datainseertt['bonus'] = $userBalanceReffred->bonus+(($getdata['amount'] * $price)/100);
			            	$amount_added = (($getdata['amount'] * $price)/100);
			        	}
			    	}
			        
			       /* 
			        $dataInsert['amount'] = $amount_added;
			        $dataInsert['user_id'] = $user_id;
			        $dataInsert['refered_by'] = $referid;
			        
			        DB::table('refer_bonus')->insert($dataInsert);*/
			    
			    $total_available_amt = $total_available_amt+$amount_added;

			    $datainseertt['user_id'] = $referid;
				//echo '<pre>'; print_r($datainseertt); die;
				if($getdata['paymentby']=='cashfree') {
					DB::table('user_balances')->where('user_id',$referid)->update($datainseertt);
				}

						$refer_b['user_id']= $userid;
						$refer_b['refered_by']= $referid;
						$refer_b['amount'] = $amount_added;
					if($getdata['paymentby']=='cashfree') {
						DB::table('refer_bonus')->insert($refer_b);
					}
				 
				$notificationdata['userid'] = $referid;
    			$notificationdata['title'] = 'Add Fund of Rs '.$amount_added.' for refer.';
    			if($getdata['paymentby']=='cashfree') {
    				DB::table('notifications')->insert($notificationdata);
    			}
    			//push notifications//
    			$titleget = 'Add fund successfully!';
    			if($amount_added) {
    				Helpers::sendnotification($titleget,$notificationdata['title'],'',$referid);
    			}
			
			    $transactionsdata_refer['userid'] = $referid;
        		$transactionsdata_refer['type'] = 'Add Fund Refer Bonus Android';
        		$transactionsdata_refer['refer_id'] = $userid;
        		//$transactionsdata_refer['paytid'] = '';
        		$transactionsdata_refer['transaction_id'] = $txnid;
        		$transactionsdata_refer['transaction_by'] = $getdata['paymentby'];
        		$transactionsdata_refer['amount'] = $amount_added;
        		$transactionsdata_refer['bonus_amt'] = $amount_added;
        		$transactionsdata_refer['paymentstatus'] = 'confirmed';
        		$transactionsdata_refer['bal_fund_amt'] = $bal_fund_amt;
        		$transactionsdata_refer['bal_win_amt'] = $bal_win_amt;
        		$transactionsdata_refer['bal_bonus_amt'] = $bal_bonus_amt + $amount_added;
        		// $transactionsdata_refer['cons_amount'] = $getdata['amount'];
        		$transactionsdata_refer['total_available_amt'] = $total_available_amt;
        		if($getdata['paymentby']=='cashfree') {
        			DB::table('transactions')->insert($transactionsdata_refer);
        		}
        		}
			
			}


			if(!empty($userdata)){
				$datainseert['user_id'] = $getdata['userid'];
				$datainseert['balance'] = $userdata->balance+$getdata['amount'];
				$datainseert['bonus'] = $userdata->bonus;
				if($getdata['paymentby']=='cashfree') {
					DB::table('user_balances')->where('user_id',$getdata['userid'])->update($datainseert);
				}
			}
			$userid=$getdata['userid'];
			$amount=$_GET['amount'];
			
			$bal_bonus_amt=0;$bal_win_amt=0;$bal_fund_amt=0;$total_available_amt=0;
			$findlastow = DB::table('user_balances')->where('user_id',$userid)->first();
			if(!empty($findlastow)){
				$total_available_amt = $findlastow->balance+$findlastow->winning+$findlastow->bonus;
				$bal_fund_amt = $findlastow->balance;
				$bal_win_amt = $findlastow->winning;
				$bal_bonus_amt = $findlastow->bonus;
			}
			
			$notificationdata['userid'] = $userid;
			$notificationdata['title'] = 'Add Fund of Rs '.$amount.' through '.$getdata['paymentby'].'.';
			if($getdata['paymentby']=='cashfree') {
				DB::table('notifications')->insert($notificationdata);
			}
			//push notifications//
			$titleget = 'Add fund successfully!';
			Helpers::sendnotification($titleget,$notificationdata['title'],'',$userid);
			//end push notifications//

			
			$transactionsdata['userid'] = $userid;
			$transactionsdata['type'] = 'Add Fund here';
			$transactionsdata['transaction_id'] = $txnid;
			$transactionsdata['transaction_by'] = $getdata['paymentby'];
			$transactionsdata['amount'] = $amount;
			$transactionsdata['addfund_amt'] = $amount;
			$transactionsdata['paymentstatus'] = 'confirmed';
			$transactionsdata['bal_fund_amt'] = $bal_fund_amt;
			$transactionsdata['bal_win_amt'] = $bal_win_amt;
			$transactionsdata['bal_bonus_amt'] = $bal_bonus_amt - $bonus_data;
			// $transactionsdata['cons_amount'] = $amount;
			$transactionsdata['total_available_amt'] = $total_available_amt - $bonus_data;
			if($getdata['paymentby']=='cashfree') {
				DB::table('transactions')->insert($transactionsdata);
			}
		
		if(!empty($bonus_data)) {

			$finduser_bal = DB::table('user_balances')->where('user_id',$userid)->first();
				if(!empty($finduser_bal)){
					$new_total_available_amt = $finduser_bal->balance+$finduser_bal->winning+$finduser_bal->bonus;
					$new_bal_fund_amt = $finduser_bal->balance;
					$new_bal_win_amt = $finduser_bal->winning;
					$new_bal_bonus_amt = $finduser_bal->bonus;
					$datainseert2['user_id'] = $userid;
					$datainseert2['refer_id']= $referid;
					// $datainseert['balance'] = $userBalance->balance+$amount;
					$datainseert2['bonus'] = $finduser_bal->bonus+$bonus_data;
					// if($getdata['paymentby']=='cashfree') {
						DB::table('user_balances')->where('user_id',$userid)->update($datainseert2);
					// }

			$transactionsdata = array();
			$transactionsdata['userid'] = $userid;
			$transactionsdata['type'] = 'Add Fund Offer bonus';
			$transactionsdata['transaction_id'] = $txnid;
			$transactionsdata['transaction_by'] = $getdata['paymentby'];
			$transactionsdata['amount'] = $bonus_data;
			$transactionsdata['bonus_amt'] = $bonus_data;
			$transactionsdata['paymentstatus'] = 'confirmed';
			$transactionsdata['bal_fund_amt'] = $bal_fund_amt;
			$transactionsdata['bal_win_amt'] = $bal_win_amt;
			$transactionsdata['bal_bonus_amt'] = $bal_bonus_amt;
			// $transactionsdata['cons_amount'] = $amount;
			$transactionsdata['total_available_amt'] = $total_available_amt;

			DB::table('transactions')->insert($transactionsdata);
		}
		}
			
			$msgg['msg'] = "Payment done.";
			$msgg['amount'] = $total_available_amt;
			$msgg['status'] = 1;
			echo json_encode(array($msgg));die;
	}
	
	public function failurepayumoney(){
		$returnurl = $this->geturltoreturn();
	    header('Location: '.$returnurl.'?msg=failed');die;
			$this->accessrules();
		$msgg['msg'] = "Payment is Fail,Please try again";
		$msgg['status'] = 0;
		echo json_encode(array($msgg));die;
		// echo 'failurepayumoney';die;
	}
	
	public function paymentfrompaytm(){
		$this->accessrules();
		$amount = $_GET['amount'];
		$id = $_GET['id'];
		
		$loginsession = DB::table('register_users')->where(array('id'=>$id))->first();
		if(!empty($loginsession)){
			$paymentdata['amount']=$amount;
			$paymentdata['userid']=$loginsession->id;
			$paymentdata['username']=$loginsession->username;
			$paymentdata['mobile']=$loginsession->mobile;
			$paymentdata['email']=$loginsession->email;
			$paymentdata['unique_id']=$loginsession->unique_id;
			$paymentdata['paymentby']='Paytm';
		}

		Session::put('askforpayment',$paymentdata);
		$txnid= 'Fanadda-'.rand(1000000,99999);;
		$getvalues['txnid'] = $txnid;
		$getvalues['amount'] = $amount;
		$getvalues['userid'] = $id;
		$getvalues['firstname'] = ucwords($loginsession->username);
		$getvalues['email'] = $loginsession->email;
		$getvalues['phone'] = $loginsession->mobile;
		return view('api.paymentfrompaytm',compact('getvalues'));
	}
	
	public function paymentstatus(){
		echo 'hii';die;
		// return view('api.paymentstatus');
	}
	
	public function paytmstatus(){
		header("Pragma: no-cache");
		header("Cache-Control: no-cache");
		header("Expires: 0");
		// following files need to be included

		// echo $root."PaytmKit/lib/config_paytm.php";die;
		require_once("./PaytmKit/lib/config_paytm.php");
		require_once("./PaytmKit/lib/encdec_paytm.php");
		$returnurl = $this->geturltoreturn();
		$paytmChecksum = "";
		$paramList = array();
		$isValidChecksum = "FALSE";
		// echo '<pre>';print_r($_POST);die;
		$paramList = $_POST;
		$paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; //Sent by Paytm pg

		//Verify all parameters received from Paytm pg to your application. Like MID received from paytm pg is same as your application’s MID, TXN_AMOUNT and ORDER_ID are same as what was sent by you to Paytm PG for initiating transaction etc.
		$isValidChecksum = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum); //will return TRUE or FALSE string.


		if($isValidChecksum == "TRUE") {
			echo "<b>Checksum matched and following are the transaction details:</b>" . "<br/>";
			if ($_POST["STATUS"] == "TXN_SUCCESS") {
				// echo "<b>Transaction status is success</b>" . "<br/>";
				//Process your transaction here as success transaction.
				//Verify amount & order id received from Payment gateway with your application's order id and amount.
			}
			else {
				// echo "<b>Transaction status is failure</b>" . "<br/>";
				header('Location: '.$returnurl.'?msg=failed');die;
				$msgg['msg'] = "Payment is Fail,Please try again";
				$msgg['status'] = 0;
				echo json_encode(array($msgg));die;
			}
			
			$new_arr=array();
			if (isset($_POST) && count($_POST)>0 ){
				foreach($_POST as $paramName => $paramValue) {
						// echo "<br/>" . $paramName . " = " . $paramValue;
						$new_arr[$paramName]=$paramValue;
				}
			}
			// print_r($new_arr);die;
			$new_arr=$new_arr;
			if(!empty($new_arr)){
				// print_r($new_arr['MID']);die;
				$paymentgatewayinfo['amount'] = $new_arr['TXNAMOUNT'];
				$paymentgatewayinfo['txnid'] = $new_arr['ORDERID'];
				$paymentgatewayinfo['paymentby'] = 'Paytm';
				$returnamount = $this->requestprocess($paymentgatewayinfo);
				if($returnamount=='success'){
					header('Location: '.$returnurl.'?msg=success');die;
					$msgg['msg'] = "Amount successfully added to your wallet";
					$msgg['status'] = 1;
					echo json_encode(array($msgg));die;
				}else{
					header('Location: '.$returnurl.'?msg=failed');die;
					$msgg['msg'] = "Payment is Fail,Please try again";
					$msgg['status'] = 0;
					echo json_encode(array($msgg));die;
					
				}
			}else{
				header('Location: '.$returnurl.'?msg=failed');die;
				$msgg['msg'] = "Payment is Fail,Please try again";
				$msgg['status'] = 0;
				echo json_encode(array($msgg));die;
				
			}
			

			
			
		}
		else {
			// echo "<b>Checksum mismatched.</b>";
			header('Location: '.$returnurl.'?msg=failed');die;
			$this->accessrules();
			$msgg['msg'] = "Payment is Fail,Please try again";
			$msgg['status'] = 0;
			echo json_encode(array($msgg));die;
		}
	}

	
	public function myteam(){
		$this->accessrules();
		$matchkey = $_GET['matchkey'];
		$userid = $_GET['userid'];
		$challenge_id = $_GET['challenge_id'];
		$findjointeam = DB::table('join_teams')->where('matchkey',$_GET['matchkey'])->where('userid',$_GET['userid'])->orderBy('teamnumber','ASC')->select('id','teamnumber')->get();
		$Json=array();
		if(!empty($findjointeam)){
			$i=0;
			foreach($findjointeam as $jointeams){
				$Json[$i]['teamnumber'] = $jointeams->teamnumber;
				$Json[$i]['teamid'] = $jointeams->id;

				$check_joined = DB::table('joined_leagues')->where('userid', $userid)->where('teamid', $jointeams->id)->where('challengeid', $challenge_id)->first();
				if(!empty($check_joined)) {
					$Json[$i]['is_joined'] = 1;
					$Json[$i]['join_id'] = $check_joined->id;
				} else {
					$Json[$i]['is_joined'] = 0;
					$Json[$i]['join_id'] = 0;
				}
				
				$jointeam = DB::table('join_teams')->where('matchkey',$matchkey)->where('userid',$_GET['userid'])->where('teamnumber',$jointeams->teamnumber)->select('*')->get();
					if(!empty($jointeam)){
						foreach($jointeam as $team){
							$playersarr = explode(',',$team->players);
							$findmatch = DB::table('listmatches')->where('matchkey',$team->matchkey)->join('teams as t1','t1.id','=','listmatches.team1')->join('teams as t2','t2.id','=','listmatches.team2')->select('listmatches.team1','listmatches.team2','listmatches.team1display','listmatches.team2display','t1.color as team1color','t2.color as team2color')->first();
							$boplayerdetails = DB::table('matchplayers')->whereIn('matchplayers.playerid',$playersarr)->where('matchkey',$team->matchkey)->join('players','matchplayers.playerid','=','players.id')->select('players.team','matchplayers.role as playerrole','matchplayers.credit as playercredit','matchplayers.role','players.player_name as playername','matchplayers.playerid as pid')->get();
							// $batplayerdetails = DB::table('matchplayers')->whereIn('matchplayers.playerid',$playersarr)->where('matchplayers.role','batsman')->where('matchkey',$team->matchkey)->join('players','matchplayers.playerid','=','players.id')->select('players.team','matchplayers.role as playerrole','matchplayers.credit as playercredit','matchplayers.role','players.player_name as playername','matchplayers.playerid as pid')->get();
							// $keeprdetails = DB::table('matchplayers')->whereIn('matchplayers.playerid',$playersarr)->where('matchplayers.role','keeper')->where('matchkey',$team->matchkey)->join('players','matchplayers.playerid','=','players.id')->select('players.team','matchplayers.role as playerrole','matchplayers.credit as playercredit','matchplayers.role','players.player_name as playername','matchplayers.playerid as pid')->get();
							// $allrundetails = DB::table('matchplayers')->whereIn('matchplayers.playerid',$playersarr)->where('matchplayers.role','allrounder')->where('matchkey',$team->matchkey)->join('players','matchplayers.playerid','=','players.id')->select('players.team','matchplayers.role as playerrole','matchplayers.credit as playercredit','matchplayers.role','players.player_name as playername','matchplayers.playerid as pid')->get();
							if(!empty($boplayerdetails)){
								$j=0;
								foreach($boplayerdetails as $bowler){
									$Json[$i]['player'][$j]['id'] = $bowler->pid;
									$Json[$i]['player'][$j]['player_name'] = $bowler->playername;
									$Json[$i]['player'][$j]['role'] = $bowler->playerrole;
									$Json[$i]['player'][$j]['credit'] = $bowler->playercredit;
									if($bowler->team==$findmatch->team1){
										$Json[$i]['player'][$j]['team'] = 'team1';
										$Json[$i]['player'][$j]['team_code'] = strtoupper($findmatch->team1display);
										$Json[$i]['player'][$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team1color));
									}
									if($bowler->team==$findmatch->team2){
										$Json[$i]['player'][$j]['team'] = 'team2';
										$Json[$i]['player'][$j]['team_code'] = strtoupper($findmatch->team2display);
										$Json[$i]['player'][$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team2color));
									}
									$vicecaptain=0;
									$captain=0;
									if($team->vicecaptain==$bowler->pid){
										$vicecaptain=1;
									}
									if($team->captain==$bowler->pid){
										$captain=1;
									}
									$Json[$i]['player'][$j]['vicecaptain'] = $vicecaptain;
									$Json[$i]['player'][$j]['captain'] = $captain;
									$findlastow=array();
									$findlastow = DB::table('players')->where('id',$bowler->pid)->select('points')->first();
									// echo '<pre>';print_r($findlastow);die;
									$Json[$i]['player'][$j]['points'] = $findlastow->points;
									$j++;
								}
							}
						} 
					}
				$i++;
			}
		}
		
		echo json_encode($Json);
		die;
	}

	public function get_cashfree_checksum() {
		$post = $_POST; //print_r($_POST); exit;
        $secretKey = '230b6da0f772727b88d35e46d7316adb4e5602fd'; //CODE to fetching your secretKey from your config files
            foreach($post as $key => $value) {
                $postData[$key] = $_POST[$key];
             }

         //The postData initializes its values from the Post parameters
         $postData['appId'] = '6555794a885b7a5cec8e7b3a5556';
         

         // combine all of the data into a single string as shown below
         ksort($postData);
         $checksumData = "";
         foreach ($postData as $key => $value){
              $checksumData .= $key.$value;
         }

         
         $checksum = hash_hmac('sha256', $checksumData, $secretKey,true);
         $checksum = base64_encode($checksum);

         $email = $post['customerEmail'];
         $row = DB::table('register_users')->where('email', $email)->first();
         if($row) {
         	$user_id = $row->id;
         	$insert_data = array(
            "userid" => $user_id,
            "amount" => $post['orderAmount'],
            "transaction_by" => "cashfree",
            "paymentstatus" => "pending",
            "transaction_id" => $post['orderId'],
            "type" => "Add Fund"
        	);
        	DB::table('transactions')->insert($insert_data);
     	}
         
        //

         
         $response = array("orderId" => $post['orderId'], "checksum" => $checksum, "status" => "OK");
         echo json_encode($response); exit;
         return $data;
    
    }

    public function cashfree_notify() {
    	$myfile = fopen(base_path()."/test2.txt", "w") or die("Unable to open file!");
		$txt = serialize($_POST)."John Doe\n";
		fwrite($myfile, $txt);
		
		fclose($myfile); exit;
		
    	$secretKey = '32a299dccf95135486ba2ae549f0d4dbf7271a36';
        $orderId = $_POST["orderId"];
         $orderAmount = $_POST["orderAmount"];
         $referenceId = $_POST["referenceId"];
         $txStatus = $_POST["txStatus"];
         $paymentMode = $_POST["paymentMode"];
         $txMsg = $_POST["txMsg"];
         $txTime = $_POST["txTime"];
         $signature = $_POST["signature"];
         $data = $orderId.$orderAmount.$referenceId.$txStatus.$paymentMode.$txMsg.$txTime;
         $hash_hmac = hash_hmac('sha256', $data, $secretKey, true) ;
         $computedSignature = base64_encode($hash_hmac); //print_r($_POST); exit;
         //if($orderId==1554373479724) {
         if ($signature == $computedSignature) {
            if($txStatus=='SUCCESS') {
            	$transaction_row = DB::table('transactions')->where('paytid', $orderId)->where('paymentstatus','pending')->first();
              // $this->db->select('id')->from('transaction')->where('transaction_id', $orderId)->where('status',0);
            	if(!empty($transaction_row)) {
            		DB::table('transactions')->where('id',$transaction_row->id)->where('paytid', $orderId)->update(array('paymentstatus'=>'confirmed'));
	                $user_id = $getdata['userid'] = $transaction_row->userid;
	                $amount = $orderAmount;

	                $userBalance = DB::table('user_balances')->where('user_id',$user_id)->first();
					$userdata = DB::table('register_users')->where('id',$getdata['userid'])->first();
					$referid = $userdata->refer_id;
					$userBalanceReffred= DB::table('user_balances')->where('user_id',$referid)->first();
					$referdata = DB::table('register_users')->where('id',$referid)->where('refer_to_join', 0)->first();


	                // Refer Data add

	                if(!empty($referdata)) {
					if(!empty($referid) || $referid != 0){
					    $bal_bonus_amt=0;$bal_win_amt=0;$bal_fund_amt=0;$total_available_amt=0;
					    $bal_bonus_amt = $userBalanceReffred->bonus;
					    $bal_win_amt = $userBalanceReffred->winning;
					    $bal_fund_amt = $userBalanceReffred->balance;
					    $total_available_amt = $bal_bonus_amt+$bal_win_amt+$bal_fund_amt;
					    $bonus = DB::table('bonus_amount')->where('type',"Add Fund Bonus")->first();
					    $bonus_type = $bonus->bonus_type;
					    $price = $bonus->price;
					 //   echo '<pre>';print_r($userBalance);die;
					    if(!empty($userBalance)){
					    	$amount_added = 0;
					        if(!empty($referdata) && $referdata->is_youtuber==1) {
					    		if($referdata->youtuber_bonus==1) {
					    			$bonus_percent = $referdata->bonus_percent;
					    			if($bonus_percent) {
					    				$balance_refer=$datainseertt['winning'] = $userBalanceReffred->winning+(($amount*$bonus_percent)/100);
					            		$amount_added = $amount*$bonus_percent/100;
					    			}
					    		}
					    	} else {
					        	if($bonus_type == 'amount'){
					            	$balance_refer=$datainseertt['bonus'] = $userBalanceReffred->bonus+$price;
					            	$amount_added = $price;
					        	}else{
					            	$balance_refer=$datainseertt['bonus'] = $userBalanceReffred->bonus+(($amount * $price)/100);
					            	$amount_added = (($amount * $price)/100);
					        	}
					    	}
					        
					       /* 
					        $dataInsert['amount'] = $amount_added;
					        $dataInsert['user_id'] = $user_id;
					        $dataInsert['refered_by'] = $referid;
					        
					        DB::table('refer_bonus')->insert($dataInsert);*/
					    }
					    $total_available_amt = $total_available_amt+$balance_refer;

					    $datainseertt['user_id'] = $referid;
						//echo '<pre>'; print_r($datainseertt); die;
						DB::table('user_balances')->where('user_id',$referid)->update($datainseertt);
						 
						$notificationdata['userid'] = $referid;
		    			$notificationdata['title'] = 'Add Fund of Rs '.$amount_added.' for refer.';
		    			DB::table('notifications')->insert($notificationdata);
		    			//push notifications//
		    			$titleget = 'Add fund successfully!';
		    			Helpers::sendnotification($titleget,$notificationdata['title'],'',$referid);
						$txnid = 'fantasypower11-'.rand(1000,9999).''.$getdata['userid'];
					    $transactionsdata_refer['userid'] = $referid;
		        		$transactionsdata_refer['type'] = 'Add Fund Refer Bonus';
		        		$transactionsdata_refer['refer_id'] = $user_id;
		        		// $transactionsdata_refer['paytid'] = $orderId;
		        		$transactionsdata_refer['transaction_id'] = $txnid;
		        		$transactionsdata_refer['transaction_by'] = 'paytm';
		        		// $transactionsdata_refer['amount'] = $amount_added;
		        		// $transactionsdata_refer['addfund_amt'] = $amount_added;
		        		$transactionsdata_refer['amount'] = $amount;
		        		$transactionsdata_refer['addfund_amt'] = $amount;

		        		$transactionsdata_refer['paymentstatus'] = 'confirmed';
		        		$transactionsdata_refer['bal_fund_amt'] = $balance_refer;
		        		$transactionsdata_refer['bal_win_amt'] = $bal_win_amt;
		        		$transactionsdata_refer['bal_bonus_amt'] = $bal_bonus_amt;
		        		$transactionsdata_refer['cons_amount'] = $amount;
		        		$transactionsdata_refer['total_available_amt'] = $total_available_amt; //print_r($transactionsdata_refer); exit;
		        		DB::table('transactions')->insert($transactionsdata_refer);
					
					}
				}

	                // Refer Data add


					if(!empty($userBalance)){
					$datainseert['user_id'] = $user_id;
					$datainseert['refer_id']= $referid;
					$datainseert['balance'] = $userBalance->balance+$amount;
					// $datainseert['bonus'] = $userBalance->bonus+$bonusAmount;
					DB::table('user_balances')->where('user_id',$getdata['userid'])->update($datainseert);
				}

				$bal_bonus_amt=0;$bal_win_amt=0;$bal_fund_amt=0;$total_available_amt=0;
				$findlastow = DB::table('user_balances')->where('user_id',$getdata['userid'])->first();
				if(!empty($findlastow)){
					$total_available_amt = $findlastow->balance+$findlastow->winning+$findlastow->bonus;
					$bal_fund_amt = $findlastow->balance;
					$bal_win_amt = $findlastow->winning;
					$bal_bonus_amt = $findlastow->bonus;
				}
				
				
				//push notifications//
				$titleget = 'Add fund successfully!';
				Helpers::sendnotification($titleget,$notificationdata['title'],'',$getdata['userid']);

				// $transactionsdata['userid'] = $user_id;
				$transactionsdata['type'] = 'Add Fund app';
				$transactionsdata['transaction_id'] = $txnid;
				$transactionsdata['transaction_by'] = 'paytm';
				$transactionsdata['amount'] = $amount;
				$transactionsdata['addfund_amt'] = $amount;
				$transactionsdata['paymentstatus'] = 'confirmed';
				$transactionsdata['bal_fund_amt'] = $bal_fund_amt;
				$transactionsdata['bal_win_amt'] = $bal_win_amt;
				$transactionsdata['bal_bonus_amt'] = $bal_bonus_amt;
				$transactionsdata['cons_amount'] = $amount;
				$transactionsdata['total_available_amt'] = $total_available_amt;
				DB::table('transactions')->where('id',$transaction_row->id)->where('paytid', $orderId)->update($transactionsdata);

				$notificationdata['userid'] = $user_id;
				$notificationdata['title'] = 'Add Fund of Rs '.$amount.' through cashfree';
				DB::table('notifications')->insert($notificationdata);	              
	                echo json_encode(array("status" => "OK")); exit;
              } else {
              	echo 0;
              }
             
            } else if($txStatus=='FAILED' || $txStatus=='CANCELLED') {
              echo 0;
            } else if($txStatus=='PENDING') {
              echo 0;
            }
          } else {
            echo 0;
           // Reject this call
         }
    }

    public function paytm_notify_new() {
    	$myfile = fopen(base_path()."/test.txt", "a") or die("Unable to open file!");
		$txt = serialize($_REQUEST)."John Doe\n";
		fwrite($myfile, $txt);		
		fclose($myfile);
		// echo json_encode(array("status" => "OK")); exit;
		 // exit;
		// print_r($_POST); exit;
		if($_REQUEST['STATUS']=='TXN_SUCCESS') {
			$orderId = $_REQUEST['ORDERID'];
			if($orderId!='') {
			
			$trans = DB::table('transactions')->where('paytid', $orderId)->where('paymentstatus','pending')->first();
			if(!empty($trans)) {
				DB::table('transactions')->where('id', $trans->id)->where('paytid', $orderId)->where('paymentstatus','pending')->update(array("paymentstatus"=>"confirmed"));
				$user_id = $getdata['userid'] = $trans->userid;
				$amount = $_REQUEST['TXNAMOUNT'];
				$userBalance = DB::table('user_balances')->where('user_id',$user_id)->first();
				$userdata = DB::table('register_users')->where('id',$getdata['userid'])->first();
					$referid = $userdata->refer_id;
					$userBalanceReffred= DB::table('user_balances')->where('user_id',$referid)->first();
					$referdata = DB::table('register_users')->where('id',$referid)->first();
				// 	if(!empty($referdata)) {
				// 	if(!empty($referid) || $referid != 0){
				// 	    $bal_bonus_amt=0;$bal_win_amt=0;$bal_fund_amt=0;$total_available_amt=0;
				// 	    $bal_bonus_amt = $userBalanceReffred->bonus;
				// 	    $bal_win_amt = $userBalanceReffred->winning;
				// 	    $bal_fund_amt = $userBalanceReffred->balance;
				// 	    $total_available_amt = $bal_bonus_amt+$bal_win_amt+$bal_fund_amt;
				// 	    $bonus = DB::table('bonus_amount')->where('type',"Add Fund Bonus")->first();
				// 	    $bonus_type = $bonus->bonus_type;
				// 	    $price = $bonus->price;
				// 	 //   echo '<pre>';print_r($userBalance);die;
				// 	    if(!empty($userBalance)){
				// 	    	$amount_added = 0;
				// 	    	$balance_refer = 0;
				// 	        if($referdata->is_youtuber==1) {
				// 	    		if($referdata->youtuber_bonus==1) {
				// 	    			$bonus_percent = $referdata->bonus_percent;
				// 	    			if($bonus_percent) {
				// 	    				$balance_refer=$datainseertt['winning'] = $userBalanceReffred->winning+(($amount*$bonus_percent)/100);
				// 	            		$amount_added = $amount*$bonus_percent/100;
				// 	    			}
				// 	    		}
				// 	    	} else {
				// 	        	if($bonus_type == 'amount'){
				// 	            	$balance_refer=$datainseertt['bonus'] = $userBalanceReffred->bonus+$price;
				// 	            	$amount_added = $price;
				// 	        	}else{
				// 	            	$balance_refer=$datainseertt['bonus'] = $userBalanceReffred->bonus+(($amount * $price)/100);
				// 	            	$amount_added = (($amount * $price)/100);
				// 	        	}
				// 	    	}
					        
				// 	       /* 
				// 	        $dataInsert['amount'] = $amount_added;
				// 	        $dataInsert['user_id'] = $user_id;
				// 	        $dataInsert['refered_by'] = $referid;
					        
				// 	        DB::table('refer_bonus')->insert($dataInsert);*/
				// 	    }
				// 	    $total_available_amt = $total_available_amt+$balance_refer;

				// 	    $datainseertt['user_id'] = $referid;
				// 		//echo '<pre>'; print_r($datainseertt); die;
				// 		DB::table('user_balances')->where('user_id',$referid)->update($datainseertt);
				// 		 if($amount_added) {
				// 		$notificationdata['userid'] = $referid;
		  //   			$notificationdata['title'] = 'Add Fund of Rs '.$amount_added.' for refer.';
		  //   			DB::table('notifications')->insert($notificationdata);
		  //   			//push notifications//
		  //   			$titleget = 'Add fund successfully!';
		  //   			Helpers::sendnotification($titleget,$notificationdata['title'],'',$referid);
		  //   				}
				// 		$txnid = 'fantasypower11-'.rand(1000,9999).''.$getdata['userid'];
				// 	    $transactionsdata_refer['userid'] = $referid;
		  //       		$transactionsdata_refer['type'] = 'Add Fund Refer Bonus Android';
		  //       		$transactionsdata_refer['refer_id'] = $user_id;
		  //       		// $transactionsdata_refer['paytid'] = $orderId;
		  //       		$transactionsdata_refer['transaction_id'] = $txnid;
		  //       		$transactionsdata_refer['transaction_by'] = 'paytm';
		  //       		$transactionsdata_refer['amount'] = $amount_added;
		  //       		$transactionsdata_refer['bonus_amt'] = $amount_added;
		  //       		$transactionsdata_refer['paymentstatus'] = 'confirmed';
		  //       		$transactionsdata_refer['bal_fund_amt'] = $balance_refer;
		  //       		$transactionsdata_refer['bal_win_amt'] = $bal_win_amt;
		  //       		$transactionsdata_refer['bal_bonus_amt'] = $bal_bonus_amt;
		  //       		$transactionsdata_refer['cons_amount'] = $amount;
		  //       		$transactionsdata_refer['total_available_amt'] = $total_available_amt; //print_r($transactionsdata_refer); exit;
		  //       		DB::table('transactions')->insert($transactionsdata_refer);
					
				// 	}
				// }

				if(!empty($userBalance)){
					$datainseert['user_id'] = $user_id;
					$datainseert['refer_id']= $referid;
					$datainseert['balance'] = $userBalance->balance+$amount;
					// $datainseert['bonus'] = $userBalance->bonus+$bonusAmount;
					DB::table('user_balances')->where('user_id',$getdata['userid'])->update($datainseert);
				}

				$bal_bonus_amt=0;$bal_win_amt=0;$bal_fund_amt=0;$total_available_amt=0;
				$findlastow = DB::table('user_balances')->where('user_id',$getdata['userid'])->first();
				if(!empty($findlastow)){
					$total_available_amt = $findlastow->balance+$findlastow->winning+$findlastow->bonus;
					$bal_fund_amt = $findlastow->balance;
					$bal_win_amt = $findlastow->winning;
					$bal_bonus_amt = $findlastow->bonus;
				}
				
				
				//push notifications//
				/*$titleget = 'Add fund successfully!';
				Helpers::sendnotification($titleget,$notificationdata['title'],'',$getdata['userid']);*/

				$transactionsdata['userid'] = $user_id;
				$transactionsdata['type'] = 'Add Fund new';
				$transactionsdata['transaction_id'] = "Fanadda-".$user_id.'_'.rand(1000,2);
				$transactionsdata['transaction_by'] = 'paytm';
				$transactionsdata['amount'] = $amount;
				$transactionsdata['addfund_amt'] = $amount;
				$transactionsdata['paymentstatus'] = 'confirmed';
				$transactionsdata['bal_fund_amt'] = $bal_fund_amt;
				$transactionsdata['bal_win_amt'] = $bal_win_amt;
				$transactionsdata['bal_bonus_amt'] = $bal_bonus_amt;
				$transactionsdata['cons_amount'] = $amount;
				$transactionsdata['total_available_amt'] = $total_available_amt;
				DB::table('transactions')->where('paytid', $orderId)->update($transactionsdata);

				$notificationdata['userid'] = $user_id;
				$notificationdata['title'] = 'Add Fund of Rs '.$amount.' through paytm';
				DB::table('notifications')->insert($notificationdata);
	

			}
		
	}
	}
		echo json_encode(array("status" => "OK")); exit;
    }

    public function paytm_notify() {
    	$myfile = fopen(base_path()."/test.txt", "a") or die("Unable to open file!");
		$txt = serialize($_REQUEST)."John Doe\n";
		fwrite($myfile, $txt);		
		fclose($myfile);
		// echo json_encode(array("status" => "OK")); exit;
		//  exit;
		// print_r($_POST); exit;
		if($_REQUEST['STATUS']=='TXN_SUCCESS') {
			$orderId = $_REQUEST['ORDERID'];
			if($orderId!='') { 
			$trans = DB::table('transactions')->where('paytid', $orderId)->where('paymentstatus','pending')->first();
			if(!empty($trans)) {
				DB::table('transactions')->where('id', $trans->id)->where('paytid', $orderId)->where('paymentstatus','pending')->update(array("paymentstatus"=>"confirmed"));
				$user_id = $getdata['userid'] = $trans->userid;
				$amount = $_REQUEST['TXNAMOUNT'];
				$userBalance = DB::table('user_balances')->where('user_id',$user_id)->first();
				$userdata = DB::table('register_users')->where('id',$getdata['userid'])->first();
					$referid = $userdata->refer_id;
					$userBalanceReffred= DB::table('user_balances')->where('user_id',$referid)->first();
					$referdata = DB::table('register_users')->where('id',$referid)->first();
				// 	if(!empty($referdata)) {
				// 	if(!empty($referid) || $referid != 0){
				// 	    $bal_bonus_amt=0;$bal_win_amt=0;$bal_fund_amt=0;$total_available_amt=0;
				// 	    $bal_bonus_amt = $userBalanceReffred->bonus;
				// 	    $bal_win_amt = $userBalanceReffred->winning;
				// 	    $bal_fund_amt = $userBalanceReffred->balance;
				// 	    $total_available_amt = $bal_bonus_amt+$bal_win_amt+$bal_fund_amt;
				// 	    $bonus = DB::table('bonus_amount')->where('type',"Add Fund Bonus")->first();
				// 	    $bonus_type = $bonus->bonus_type;
				// 	    $price = $bonus->price;
				// 	 //   echo '<pre>';print_r($userBalance);die;
				// 	    if(!empty($userBalance)){
				// 	    	$amount_added = 0;
				// 	        if(!empty($referdata) && $referdata->is_youtuber==1) {
				// 	    		if($referdata->youtuber_bonus==1) {
				// 	    			$bonus_percent = $referdata->bonus_percent;
				// 	    			if($bonus_percent) {
				// 	    				$balance_refer=$datainseertt['winning'] = $userBalanceReffred->winning+(($amount*$bonus_percent)/100);
				// 	            		$amount_added = $amount*$bonus_percent/100;
				// 	    			}
				// 	    		}
				// 	    	} else {
				// 	        	if($bonus_type == 'amount'){
				// 	            	$balance_refer=$datainseertt['bonus'] = $userBalanceReffred->bonus+$price;
				// 	            	$amount_added = $price;
				// 	        	}else{
				// 	            	$balance_refer=$datainseertt['bonus'] = $userBalanceReffred->bonus+(($amount * $price)/100);
				// 	            	$amount_added = (($amount * $price)/100);
				// 	        	}
				// 	    	}
					        
				// 	       /* 
				// 	        $dataInsert['amount'] = $amount_added;
				// 	        $dataInsert['user_id'] = $user_id;
				// 	        $dataInsert['refered_by'] = $referid;
					        
				// 	        DB::table('refer_bonus')->insert($dataInsert);*/
				// 	    }
				// 	    $total_available_amt = $total_available_amt+$balance_refer;

				// 	    $datainseertt['user_id'] = $referid;
				// 		//echo '<pre>'; print_r($datainseertt); die;
				// 		DB::table('user_balances')->where('user_id',$referid)->update($datainseertt);
						 
				// 		$notificationdata['userid'] = $referid;
		  //   			$notificationdata['title'] = 'Add Fund of Rs '.$amount_added.' for refer.';
		  //   			DB::table('notifications')->insert($notificationdata);
		  //   			//push notifications//
		  //   			$titleget = 'Add fund successfully!';
		  //   			if($amount_added) {
		  //   				Helpers::sendnotification($titleget,$notificationdata['title'],'',$referid);
		  //   			}
				// 		$txnid = 'Fanadda-'.rand(1000,9999).''.$getdata['userid'];
				// 	    $transactionsdata_refer['userid'] = $referid;
		  //       		$transactionsdata_refer['type'] = 'Add Fund Refer Bonus';
		  //       		$transactionsdata_refer['refer_id'] = $user_id;
		  //       		// $transactionsdata_refer['paytid'] = $orderId;
		  //       		$transactionsdata_refer['transaction_id'] = $txnid;
		  //       		$transactionsdata_refer['transaction_by'] = 'paytm';
		  //       		$transactionsdata_refer['amount'] = $amount_added;
		  //       		// $transactionsdata_refer['addfund_amt'] = $amount_added;
		  //       		$transactionsdata_refer['paymentstatus'] = 'confirmed yes';
		  //       		$transactionsdata_refer['bal_fund_amt'] = $balance_refer;
		  //       		$transactionsdata_refer['bal_win_amt'] = $bal_win_amt;
		  //       		$transactionsdata_refer['bal_bonus_amt'] = $bal_bonus_amt;
		  //       		$transactionsdata_refer['cons_amount'] = $amount;
		  //       		$transactionsdata_refer['total_available_amt'] = $total_available_amt; //print_r($transactionsdata_refer); exit;
		  //       		DB::table('transactions')->insert($transactionsdata_refer);
					
				// 	}
				// }

				if(!empty($userBalance)){
					$datainseert['user_id'] = $user_id;
					$datainseert['refer_id']= $referid;
					$datainseert['balance'] = $userBalance->balance+$amount;
					// $datainseert['bonus'] = $userBalance->bonus+$bonusAmount;
					DB::table('user_balances')->where('user_id',$getdata['userid'])->update($datainseert);
				}

				$bal_bonus_amt=0;$bal_win_amt=0;$bal_fund_amt=0;$total_available_amt=0;
				$findlastow = DB::table('user_balances')->where('user_id',$getdata['userid'])->first();
				if(!empty($findlastow)){
					$total_available_amt = $findlastow->balance+$findlastow->winning+$findlastow->bonus;
					$bal_fund_amt = $findlastow->balance;
					$bal_win_amt = $findlastow->winning;
					$bal_bonus_amt = $findlastow->bonus;
				}
				
				
				//push notifications//
				$titleget = 'Add fund successfully!';
				Helpers::sendnotification($titleget,$notificationdata['title'],'',$getdata['userid']);

				$transactionsdata['userid'] = $user_id;
				$transactionsdata['type'] = 'Add Fund 12';
				$transactionsdata['transaction_id'] = $txnid;
				$transactionsdata['transaction_by'] = 'paytm';
				$transactionsdata['amount'] = $amount;
				$transactionsdata['addfund_amt'] = $amount;
				$transactionsdata['paymentstatus'] = 'confirmed';
				$transactionsdata['bal_fund_amt'] = $bal_fund_amt;
				$transactionsdata['bal_win_amt'] = $bal_win_amt;
				$transactionsdata['bal_bonus_amt'] = $bal_bonus_amt;
				$transactionsdata['cons_amount'] = $amount;
				$transactionsdata['total_available_amt'] = $total_available_amt; 
				DB::table('transactions')->where('paytid', $orderId)->update($transactionsdata);

				$notificationdata['userid'] = $user_id;
				$notificationdata['title'] = 'Add Fund of Rs '.$amount.' through paytm';
				DB::table('notifications')->insert($notificationdata);
	

			}
		
	}
	}
		echo json_encode(array("status" => "OK")); exit;
    }

    public function bank_paytm_number() {
    	$user_id = $_GET['user_id'];
    	$row = DB::table('register_users')->where('id', $user_id)->first();
        
		if(!empty($row)) {			
			$paytm_number = $row->paytm_mobile;
		}

		$row2 = DB::table('bank')->where('userid', $user_id)->first();
		if(!empty($row2)) {
			$data['account_number'] = $row2->accno;
		}

			$return_data = array(
				"account_number" => @$data['account_number'] ? @$data['account_number'] : '',
				"paytm_number" => @$paytm_number ? @$paytm_number : ''
			);
			return $return_data;
    }

    public function add_paytm_number() {
        $user_id = $_GET['user_id'];
        $paytm_number = $_GET['paytm_mobile'];
        $exist_row = DB::table('register_users')->where('paytm_mobile', $paytm_number)->first();
        if(!empty($exist_row)) {        	
            return array("status" => "error", "message" => "The Paytm number you entered is already exist.");
        }
        
        if(empty($user_id) || empty($paytm_number)) {
            return array("status" => "error", "message" => "There is an error please try again later");
        } else { 
            if(Helpers::add_paytm_beneficary($user_id, $paytm_number)) {
                return array("status" => "success", "message" => "Paytm Number successfully added");
            } else {
                return array("status" => "error", "message" => "There is an error please try again later");
            }
        }
    }

    public function paytm_withdraw()
    { return array('status' => 'error', 'message' => 'PayTm Withdrawl Upder maintainence'); exit;
        $withdrawl_amount = $_GET['withdrawal_amount'];
        $amount = $withdrawl_amount;
        if($withdrawl_amount < 200) {
            return array('status' => 'error', 'message' => 'Minimum withdrawal amount should be 200');
        } else if($withdrawl_amount > 1000) {
            return array('status' => 'error', 'message' => 'Maximum withdrawal amount should be 1000');
        }
        $user_id = $_GET['user_id'];
        
        $row = DB::table('user_balances')->where('user_id', $user_id)->first();
        $remaining_balance = $row->winning;
        $bal_fund_amt = $row->balance;
		$bal_win_amt = $row->winning;
		$bal_bonus_amt = $row->bonus;
        if($remaining_balance < $withdrawl_amount) { 
            return array('status' => 'error', 'message' => 'Not enough balance for withdrawl');
            exit;
        }

        $findverification = DB::table('register_users')->where('id',$user_id)->first();
		if(!empty($findverification)){
			if($findverification->mobile_verify!=1 || $findverification->email_verify!=1 || $findverification->pan_verify!=1 || $findverification->bank_verify!=1){
				$msgg['msg'] = "Please first complete your verification process.";
				$msgg['status'] = 3;
				$msgg['amount'] = 0;
				$msgg['wining'] = 0;
				echo json_encode(array($msgg));die;
			}
		}

        $check_paytm = DB::table('register_users')->where('id', $user_id)->select('paytm_benid')->first();
        if(empty($check_paytm->paytm_benid)) {
        	return array('status' => 'error', 'message' => 'Paytm baneficiary not registered');
            exit;
        }

        $check_multi_request = DB::table('withdraws')->where('status',0)->where('user_id', $user_id)->first();
        if(!empty($check_multi_request)) {
        	return array('status' => 'error', 'message' => 'Multiple withdraw request cannot be done.');
            exit;
        }

        $check_multi_paytm_request = DB::table('paytm_withdraw')->where('status',0)->where('user_id', $user_id)->first();
        if(!empty($check_multi_paytm_request)) {
        	return array('status' => 'error', 'message' => 'Multiple withdraw request cannot be done.');
            exit;
        }

        $today_date = date('Y-m-d H:i:s');
        $today_date = date('Y-m-d H:i:s', strtotime('-24 hours', strtotime($today_date)));
        $check_time_request = DB::table('withdraws')->where('created','>',$today_date)->where('user_id', $user_id)->first();
        if(!empty($check_time_request)) {
        	return array('status' => 'error', 'message' => 'Multiple withdrawal cannot proceed on same day try after 24 hrs.');
            exit;
        }

        $check_paytm_time_request = DB::table('paytm_withdraw')->where('created','>',$today_date)->where('user_id', $user_id)->first();
        if(!empty($check_paytm_time_request)) {
        	return array('status' => 'error', 'message' => 'Multiple withdrawal cannot proceed on same day try after 24 hrs.');
            exit;
        }


        $data['user_id'] = $user_id;
		$data['amount'] = $withdrawl_amount;
		$wordlist = DB::table('paytm_withdraw')->get();
		$wordCount = count($wordlist)+1;
		$data['withdraw_request_id'] = 'PAYTM-WD-'.$user_id.'-'.$wordCount;
		$data['created'] = date('Y-m-d');

		$dataq['winning'] = $remaining_balance - $amount;
		DB::table('user_balances')->where('id',$row->id)->update($dataq);
		$withdraw_insert_id = DB::table('paytm_withdraw')->insertGetId($data);
		$total_available_amt = $row->balance+$dataq['winning']+$row->bonus;
		
		$transactionsdata['userid'] = $user_id;
		$transactionsdata['type'] = 'Amount Withdrawn';
		$transactionsdata['transaction_id'] = $data['withdraw_request_id'];
		$transactionsdata['transaction_by'] = 'wallet';
		$transactionsdata['amount'] = $amount;
		$transactionsdata['paymentstatus'] = 'pending';
		$transactionsdata['withdraw_amt'] = $amount;
		
		$transactionsdata['bal_fund_amt'] = $bal_fund_amt;
		$transactionsdata['bal_win_amt'] = $dataq['winning'];
		$transactionsdata['bal_bonus_amt'] = $bal_bonus_amt;
		$transactionsdata['cons_win'] = $amount;
		$transactionsdata['total_available_amt'] = $total_available_amt; 
		DB::table('transactions')->insert($transactionsdata);
		
		
		$final_amount = $amount * 0.97;
        if(Helpers::cf_request_paytm_transfer($user_id, $final_amount)) {

        	$withdraw_update_data['approved_date']=date('Y-m-d');
			$withdraw_update_data['status']=1;
			DB::table('paytm_withdraw')->where('id',$withdraw_insert_id)->update($withdraw_update_data);

			$tdata['paymentstatus'] = 'Confirmed';
			$tdata['created'] = $findtransactiondate->created;
			$findtransactiondetails  = DB::table('transactions')->where('transaction_id',$data['withdraw_request_id'])->update($tdata);

        	$notificationdata['userid'] = $user_id;
			$notificationdata['title'] = 'Request For Withdraw amount rs. '.$amount.' successfully approved';
			DB::table('notifications')->insert($notificationdata);
			//push notifications//
			$titleget = 'Withdraw successfull!';
			Helpers::sendnotification($titleget,$notificationdata['title'],'',$user_id);
			//end push notifications//
        } else {
        	$notificationdata['userid'] = $user_id;
			$notificationdata['title'] = 'Request For Withdraw amount rs. '.$amount;
			DB::table('notifications')->insert($notificationdata);
			//push notifications//
			$titleget = 'Withdraw Request!';
			Helpers::sendnotification($titleget,$notificationdata['title'],'',$user_id);

        	$msgg['msg'] = "Your request for  withdrawl amount of Rs ".$amount." is sent successfully. You will  get info about it in between 24 to 48 Hours.";
			$msgg['status'] = 1;
			$msgg['amount'] = $total_available_amt-$amount;
			$msgg['wining'] = $bal_win_amt-$amount;
			echo json_encode(array($msgg));die;
        }

    }

	public function leaderboard() {

	}
	
}
?>