<?php
namespace App\Http\Controllers;
use App\Http\Controllers\CricketapiController;
use App\Http\Controllers\v2\FootballController;
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Helpers;
use File;
use Hash;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class MatchesController extends Controller
{
    public function updateallplayerdetailss()
    {
        Helpers::sendnotification('abcd','abcd','',6);
        // $findallplayers = DB::table('players')->get();
        // if(!empty($findallplayers)){
        // foreach($findallplayers as $fplayer){
        // $findexist = DB::table('player_details')->where('player_key',$fplayer->player_key)->first();
        // if(empty($findexist)){
        // $findplayerdata['fullname'] = $fplayer->player_name;
        // $findplayerdata['player_key'] = $fplayer->player_key;
        // DB::table('player_details');
        // DB::table('player_details')->insert($findplayerdata);
        // }
        // }
        // }
    }
    public function updateallteamnames()
    {
        $findallteams = DB::table('teams')->get();
        if(!empty($findallteams)){
            foreach($findallteams as $team){
                $data=array();
                $data['short_name'] = $team->team_key;
                DB::table('teams');
                DB::table('teams')->where('id',$team->id)->update($data);
            }
        }
    }
    public function updatesquadstatus() 
    {
        $allmatches = DB::table('list_matches')->where('status','notstarted')->where('launch_status','pending')->get();
        if(!empty($allmatches)){
            foreach($allmatches as $mm){
                $findsquaddetials = CricketapiController::getmatchdetails($mm->matchkey);
                if(!empty($findsquaddetials)){
                    //$matchadata['squadstatus'] = 'no';
					$matchadata['squadstatus'] = 'yes';
                    $squadteam1players = array();$squadteam2players=array();
                    if(isset($findsquaddetials['data']['card']['teams']['a']['match']['players'])){
                        $squadteam1players = $findsquaddetials['data']['card']['teams']['a']['match']['players'];
                    }
                    if(isset($findsquaddetials['data']['card']['teams']['b']['match']['players'])){
                        $squadteam2players = $findsquaddetials['data']['card']['teams']['b']['match']['players'];
                    }
                    if((!empty($squadteam1players)) && (!empty($squadteam2players))){
                        $matchadata['squadstatus'] = 'yes';
                    }
                    $matchdate = date('Y-m-d h:i:s',strtotime($findsquaddetials['start_date']['iso']));
                    $cuurectdate = Carbon::parse($matchdate )->addHours(5)->addMinutes(30);
                    $matchadata['start_date'] = $cuurectdate;
                    DB::table('list_matches')->where('id',$mm->id)->update($matchadata);
                }
            }
            die;
        }
    }
	
	public function importFootballLeagues($country)
    {
		$tournament = FootballController::get_tournaments($country);
	}

    public function importscheduledatafromapi($sport_type = CRICKET)
    {
        $sport_type = strtolower($sport_type);
        // echo $sport_type;die;
        date_default_timezone_set('Asia/Kolkata');
        $sportType =  DB::table('sport_types')->where('sport_key', $sport_type)->first();
        // print_r($sport_type);die;

        switch ($sport_type) {
            case 'football':
                // echo "ok"; exit;
                $tournament = FootballController::get_tournament_matches();
                //echo "<pre>";				print_r($tournament); exit;
				//echo $tournament;
                $tournaments = $tournament['api']['fixtures'];  
				//print_r($tournaments);
                foreach ($tournaments as $key => $value) {
					if($value['status']!='Match Finished') {
						$match_key = $value['fixture_id'];
						$findmatchexist = DB::table('list_matches')->where('matchkey',$match_key)->first();
						if(empty($findmatchexist)) {
							$team1 = $value['homeTeam']['team_id'];
							$team2 = $value['awayTeam']['team_id'];
							$team1_details = DB::table('teams')->where('team_key', $team1)->first();
							if(empty($team1_details)) {
								$team_insert_data = array();
								$team_insert_data['team_key'] = $value['homeTeam']['team_id'];
								$team_insert_data['team'] = $value['homeTeam']['team_name'];
								$team_insert_data['sport_type'] = 2;
								$team_insert_data['src'] = 'RapidApi';
								$team1id = DB::table('teams')->insertGetId($team_insert_data);
								$team1name= $value['homeTeam']['team_name'];
							} else {
								$team1id = $team1_details->id;
								$team1name = $team1_details->team;
							}
							$team2_details = DB::table('teams')->where('team_key', $team2)->first();
							if(empty($team2_details)) {
								$team_insert_data = array();
								$team_insert_data['team_key'] = $value['awayTeam']['team_id'];
								$team_insert_data['team'] = $value['awayTeam']['team_name'];
								$team_insert_data['sport_type'] = 2;
								$team_insert_data['src'] = 'RapidApi';
								$team2id = DB::table('teams')->insertGetId($team_insert_data);
								$team2name= $value['awayTeam']['team_name'];
							} else {
								$team2id = $team2_details->id;
								$team2name = $team2_details->team;
							}
							$matchadata['status'] = 'notstarted';
							$matchadata['name'] = $team1name." vs ".$team2name;
							$matchadata['short_name'] = $team1name." vs ".$team2name;
							$matchadata['season'] = $value['league']['name'];
							$matchadata['title'] = $team1name." vs ".$team2name;
							$matchadata['format'] = 'other';
							$matchadata['team1'] = $team1id;
							$matchadata['team2'] = $team2id;
							$matchadata['sport_type'] = 2;
							$matchadata['matchkey'] = $match_key;
							$matchadata['src'] = 'RapidApi';
							$matchdate = date('Y-m-d H:i:s',$value['event_timestamp']);
							$matchadata['start_date'] = $matchdate;
							DB::table('list_matches')->insert($matchadata);
						}
						//$this->importsquad($value['match']['key'], 2); 
					}
                }
                break;


            default:
                // echo "ok"; exit;
                $getdayslist = CricketapiController::getscedulematches();
                // echo "<pre>";
                // echo print_r($getdayslist);
                // die;
                if(!empty($getdayslist)){
                    foreach($getdayslist  as $daylist){
                        $getlist = $daylist['matches'];
                        if(!empty($getlist)){
                            foreach($getlist as $getli){
                                $matchadata=array();
                                $findmatchexist = DB::table('list_matches')->where('matchkey',$getli['key'])->first();
                                if(empty($findmatchexist)){
                                    //find squad details//
                                    $findsquaddetials = CricketapiController::getmatchdetails($getli['key']);
                                    if(!empty($findsquaddetials)){
                                        $squadteam1players = array();$squadteam2players=array();
                                        if(isset($findsquaddetials['data']['card']['teams']['a']['match']['players'])){
                                            $squadteam1players = $findsquaddetials['data']['card']['teams']['a']['match']['players'];
                                        }
                                        if(isset($findsquaddetials['data']['card']['teams']['b']['match']['players'])){
                                            $squadteam2players = $findsquaddetials['data']['card']['teams']['b']['match']['players'];
                                        }
                                        if((!empty($squadteam1players)) && (!empty($squadteam2players))){
                                            $matchadata['squadstatus'] = 'yes';
                                        }else{
											$matchadata['squadstatus'] = 'yes';
                                            //$matchadata['squadstatus'] = 'no';
                                        }
                                    }
                                    //team details update//
                                    $team1key = $getli['teams']['a']['key'];
                                    $team2key = $getli['teams']['b']['key'];
									if($team1key!=null && $team2key!=null){
                                    //insert team 1//
                                    $findteam1 = DB::table('teams')->where('team_key',$team1key)->select('id')->first();
                                    if(empty($findteam1)){
                                        $data['team_key'] = $team1key;
                                        $data['team'] = $getli['teams']['a']['name'];
                                        $team1id = DB::table('teams')->insertGetId($data);
                                    }else{
                                        $team1id = $findteam1->id;
                                    }
                                    //insert team 2//
                                    $findteam2 = DB::table('teams')->where('team_key',$team2key)->select('id')->first();
                                    if(empty($findteam2)){
                                        $data1['team_key'] = $team2key;
                                        $data1['team'] = $getli['teams']['b']['name'];
                                        $team2id = DB::table('teams')->insertGetId($data1);
                                    }
                                    else{
                                        $team2id = $findteam2->id;
                                    }
                                    //insert match data//
                                    $matchadata['status'] = $getli['status'];
                                    $matchadata['name'] = $getli['name'];
                                    $matchadata['short_name'] = $getli['short_name'];
                                    $matchadata['season'] = $getli['season']['name'];
                                    $matchadata['title'] = $getli['title'];
                                    $matchadata['format'] = $getli['format'];
                                    $matchadata['team1'] = $team1id;
                                    $matchadata['team2'] = $team2id;
                                    $matchadata['team1display'] = $getli['teams']['a']['key'];
                                    $matchadata['team2display'] = $getli['teams']['b']['key'];
                                    $matchadata['matchkey'] = $getli['key'];
                                    $cuurectdate = date('Y-m-d H:i:s',strtotime($getli['start_date']['iso']));
                                    //$cuurectdate = Carbon::parse($matchdate )->addHours(5)->addMinutes(30);
                                    $matchadata['start_date'] = $cuurectdate;
                                    DB::table('list_matches')->insert($matchadata);
									}
                                }else if($findmatchexist->team1display== "tbc" || $findmatchexist->team2display== "tbc"){
                                    $findsquaddetials = CricketapiController::getmatchdetails($getli['key']);
                                    if(!empty($findsquaddetials)){
                                        $squadteam1players = array();$squadteam2players=array();
                                        if(isset($findsquaddetials['data']['card']['teams']['a']['match']['players'])){
                                            $squadteam1players = $findsquaddetials['data']['card']['teams']['a']['match']['players'];
                                        }
                                        if(isset($findsquaddetials['data']['card']['teams']['b']['match']['players'])){
                                            $squadteam2players = $findsquaddetials['data']['card']['teams']['b']['match']['players'];
                                        }
                                        if((!empty($squadteam1players)) && (!empty($squadteam2players))){
                                            $matchadata['squadstatus'] = 'yes';
                                        }else{
											$matchadata['squadstatus'] = 'yes';
                                            //$matchadata['squadstatus'] = 'no';
                                        }
                                    }
                                    //team details update//
                                    $team1key = $getli['teams']['a']['key'];
                                    $team2key = $getli['teams']['b']['key'];
                                    //insert team 1//
                                    $findteam1 = DB::table('teams')->where('team_key',$team1key)->select('id')->first();
                                    if(empty($findteam1)){
                                        $data['team_key'] = $team1key;
                                        $data['team'] = $getli['teams']['a']['name'];
                                        $team1id = DB::table('teams')->insertGetId($data);
                                    }else{
                                        $team1id = $findteam1->id;
                                    }
                                    //insert team 2//
                                    $findteam2 = DB::table('teams')->where('team_key',$team2key)->select('id')->first();
                                    if(empty($findteam2)){
                                        $data1['team_key'] = $team2key;
                                        $data1['team'] = $getli['teams']['b']['name'];
                                        $team2id = DB::table('teams')->insertGetId($data1);
                                    }
                                    else{
                                        $team2id = $findteam2->id;
                                    }
                                    //update match data//
                                    $matchadata['status'] = $getli['status'];
                                    $matchadata['name'] = $getli['name'];
                                    $matchadata['short_name'] = $getli['short_name'];
                                    $matchadata['season'] = $getli['season']['name'];
                                    $matchadata['title'] = $getli['title'];
                                    $matchadata['format'] = $getli['format'];
                                    $matchadata['team1'] = $team1id;
                                    $matchadata['team2'] = $team2id;
                                    $matchadata['team1display'] = $getli['teams']['a']['key'];
                                    $matchadata['team2display'] = $getli['teams']['b']['key'];
                                    $matchadata['matchkey'] = $getli['key'];
                                    $cuurectdate = date('Y-m-d H:i:s',strtotime($getli['start_date']['iso']));
                                    //$cuurectdate = Carbon::parse($matchdate )->addHours(5)->addMinutes(30);
                                    $matchadata['start_date'] = $cuurectdate;
                                    DB::table('list_matches')->where('matchkey',$getli['key'])->update($matchadata);
                                }
                            }
                        }
                    }
                }
                break;

        }

        return redirect()->action('MatchesController@matchmapping',$sport_type); 

    }

    public function refer_bonus($matchkey) { //exit;
        $joinedleauges = DB::table('leagues_transactions')
            ->join('register_users as r1','leagues_transactions.user_id','=','r1.id')
            ->join('match_challenges','match_challenges.id','=','leagues_transactions.challengeid')
            ->join('register_users as r2','r1.refer_id','=','r2.id')
            ->where('leagues_transactions.matchkey',$matchkey)
            ->where('r1.refer_id','!=',0)
            ->where('r2.is_youtuber',1)
            ->where('match_challenges.status','!=','canceled')
			//->where('match_challenges.challenge_type','!=','%')
            //->where('leaugestransactions.winning+leaugestransactions.balance','>',0)
            ->where(DB::raw('(leagues_transactions.winning+leagues_transactions.balance)'),'>',0)
            ->groupBy('leagues_transactions.id')
            ->select('leagues_transactions.*','r1.id as user_id', 'r2.id as refer_id', 'r2.bonus_percent as refer_percent','match_challenges.win_amount as win_amount','match_challenges.joinedusers','match_challenges.entryfee','match_challenges.challenge_type','match_challenges.winning_percentage')->get();
        // print_r($joinedleauges); exit;
        $refer_data = array();
        $i = 0;
        foreach ($joinedleauges as $key => $value) {
			$profit =0;
			if($value->challenge_type=="percentage" || $value->challenge_type=="%"){
				$toWin = round($value->joinedusers * $value->winning_percentage/100);
				$profit = ($value->entryfee * $value->joinedusers)-($toWin * $value->win_amount);
			}
			else{
				$profit = ($value->entryfee * $value->joinedusers)-($value->win_amount);
			}
            $per_user_profit = ($profit / $value->joinedusers)-$value->bonus;
            if ($per_user_profit < 0) {
                $per_user_profit = 0;
            }

            $refer_data[$i]['user_id'] = $value->user_id;
            $refer_data[$i]['refer_id'] = $value->refer_id;
            $refer_data[$i]['amount'] = ($per_user_profit) ? ((($per_user_profit) * $value->refer_percent / 100)*82)/100 : 0;
            $refer_data[$i]['challenge_id'] = $value->challengeid;
            $refer_data[$i]['join_id'] = $value->joinid;
            $i++;
        }
        if(!empty($refer_data)) {
            $this->insertOrUpdate($refer_data, 'league_refer_bonus');
        }
        return 1;
        // print_r($refer_data); exit;

    }

    public function refer_bonus_new($matchkey) { //echo 1; exit;
        $joinedleauges = DB::table('leagues_transactions')
            ->join('register_users as r1','leagues_transactions.user_id','=','r1.id')
            ->join('match_challenges','match_challenges.id','=','leagues_transactions.challengeid')
            ->join('register_users as r2','r1.refer_id','=','r2.id')
            ->where('leagues_transactions.matchkey',$matchkey)
            ->where('r1.refer_id','!=',0)
            ->where('r2.is_youtuber',2)
            ->where('match_challenges.status','!=','canceled')
			->where('match_challenges.challenge_type','!=','%') 
            //->where('leaugestransactions.winning+leaugestransactions.balance','>',0)
            ->where(DB::raw('(leagues_transactions.winning+leagues_transactions.balance)'),'>',0)
            ->groupBy('leagues_transactions.id')
            ->select('leagues_transactions.*','r1.id as user_id', 'r2.id as refer_id', 'r2.bonus_percent as refer_percent','match_challenges.win_amount as win_amount','match_challenges.joinedusers','match_challenges.entryfee')->get();
        // print_r($joinedleauges); exit;
        $refer_data = array();
        $i = 0;
        foreach ($joinedleauges as $key => $value) {
            $profit = ($value->winning + $value->balance);
            $per_user_profit = $profit;
            if ($per_user_profit < 0) {
                $per_user_profit = 0;
            }
            $profit_user =  (($per_user_profit) * $value->refer_percent / 100);
            // if($value->refer_id==1) {
            // 	print_r($profit_user); echo "<br>";
            // }
            // $profit_user =  $profit_user * 82 / 100;
            // if($value->refer_id==1) {
            // 	print_r($profit_user);
            // }
            $refer_data[$i]['user_id'] = $value->user_id;
            $refer_data[$i]['refer_id'] = $value->refer_id;
            $refer_data[$i]['amount'] = $profit_user;
            $refer_data[$i]['challenge_id'] = $value->challengeid;
            $refer_data[$i]['join_id'] = $value->joinid;
            $i++;
        }
        // exit;
        if(!empty($refer_data)) {
            $this->insertOrUpdate($refer_data, 'league_refer_bonus');
        }
        return 1;
        // print_r($refer_data); exit;

    }

    public function importdatafromapi()
    {

        $coverage = CricketapiController::get_coverage_v4();
        //      echo "<pre>";
        // print_r($coverage);exit;
        foreach ($coverage['data']['boards'] as $key => $value) {
            $board_key  = $value['key'];

            /*if($board_key!='c.board.ecc.2f218') {
	        continue;
	    }*/

            $seasons = CricketapiController::get_seasons_v4($board_key);


            if(!empty(@$seasons['data']['seasons'])) {
                foreach ($seasons['data']['seasons'] as $key2 => $value2) {
                    $season_key = $value2['key'];
                    /*if($season_key!='c.season.ecncss.4e417') {
	          continue;
	      }*/

                    //$getdayslist = CricketapiController::getscedulematches();
                    date_default_timezone_set('Asia/Kolkata');

                    $getdayslist = CricketapiController::seasonmatches($season_key);

                    // print_r($getdayslist);  die;
                    //echo "<pre>";print_r($getdayslist);die;
                    if(!empty($getdayslist)){
                        $mdata = $getdayslist['data']; //print_r($data); exit;
                        // foreach($getdayslist  as $daylist){
                        // $getroundlist = array_reverse($mdata['season']['rounds']);
                        // foreach ($getroundlist as $key => $value) {
                        // $getroundgroups = $value['groups'];
                        // foreach ($getroundgroups as $key2 => $value2) {
                        //echo "<pre>";
                        $getlist = $mdata['intelligent_order']; //print_r($getlist); exit;
                        if(!empty($getlist)){
                            foreach($getlist as $getli){
                                $matchadata=array();
                                //echo $getli;
                                $findsquaddetials = CricketapiController::getmatchdetails($getli);
                                // print_r($findsquaddetials);
                                $findmatchexist = DB::table('list_matches')->where('matchkey',$getli)->first();


                                if(empty($findmatchexist)){
                                    //find squad details//
                                    $findsquaddetials = CricketapiController::getmatchdetails($getli);
                                    //print_r($findsquaddetials); exit;
                                    if(!empty($findsquaddetials)){
                                        $squadteam1players = array();$squadteam2players=array();
                                        if(isset($findsquaddetials['data']['card']['teams']['a']['match']['players'])){
                                            $squadteam1players = $findsquaddetials['data']['card']['teams']['a']['match']['players'];
                                        }
                                        if(isset($findsquaddetials['data']['card']['teams']['b']['match']['players'])){
                                            $squadteam2players = $findsquaddetials['data']['card']['teams']['b']['match']['players'];
                                        }
                                        if((!empty($squadteam1players)) && (!empty($squadteam2players))){
                                            $matchadata['squadstatus'] = 'yes';

                                        }else{
											$matchadata['squadstatus'] = 'yes';
                                            //$matchadata['squadstatus'] = 'no';
                                        }
                                    }
                                    //team details update//
                                    $team1key = $findsquaddetials['data']['card']['teams']['a']['key'];
                                    $team2key = $findsquaddetials['data']['card']['teams']['b']['key'];
                                    //insert team 1//
                                    $findteam1 = DB::table('teams')->where('team_key',$team1key)->select('id')->first();
                                    if(empty($findteam1)){
                                        $data['team_key'] = $team1key;
                                        if($team1key == '' || is_null($team1key)){
                                            continue;
                                        }
                                        $data['team'] = $findsquaddetials['data']['card']['teams']['a']['name']; //print_r($data); exit;
                                        $team1id = DB::table('teams')->insertGetId($data);
                                    }else{
                                        $team1id = $findteam1->id;
                                    }
                                    //insert team 2//
                                    $findteam2 = DB::table('teams')->where('team_key',$team2key)->select('id')->first();
                                    if(empty($findteam2)){
                                        $data1['team_key'] = $team2key;
                                        if($team2key == '' || is_null($team2key)){
                                            continue;
                                        }
                                        $data1['team'] = $findsquaddetials['data']['card']['teams']['b']['name'];
                                        $team2id = DB::table('teams')->insertGetId($data1);
                                    }
                                    else{
                                        $team2id = $findteam2->id;
                                    }
                                    //insert match data//
                                    $matchadata['status'] = 'notstarted';
                                    $matchadata['name'] = $findsquaddetials['data']['card']['name'];
                                    $matchadata['short_name'] = $findsquaddetials['data']['card']['short_name'];
                                    $matchadata['season'] = $findsquaddetials['data']['card']['season']['name'];
                                    $matchadata['title'] = $findsquaddetials['data']['card']['title'];
                                    $matchadata['format'] = $findsquaddetials['data']['card']['format'];
                                    $matchadata['team1'] = $team1id;
                                    $matchadata['team2'] = $team2id;
                                    $matchadata['team1display'] = $findsquaddetials['data']['card']['teams']['a']['short_name'];
                                    $matchadata['team2display'] = $findsquaddetials['data']['card']['teams']['b']['short_name'];
                                    $matchadata['matchkey'] = $findsquaddetials['data']['card']['key'];
                                    $cuurectdate = date('Y-m-d H:i:s',strtotime($findsquaddetials['data']['card']['start_date']['iso']));
                                    //$cuurectdate = Carbon::parse($matchdate )->addHours(5)->addMinutes(30);
                                    $matchadata['start_date'] = $cuurectdate;
                                    DB::table('list_matches')->insert($matchadata);
                                }else if($findmatchexist->team1display== "TBC A" || $findmatchexist->team2display== "TBC B"){

                                    $findsquaddetials = CricketapiController::getmatchdetails($getli);

                                    if(!empty($findsquaddetials)){
                                        $squadteam1players = array();$squadteam2players=array();
                                        if(isset($findsquaddetials['data']['card']['teams']['a']['match']['players'])){
                                            $squadteam1players = $findsquaddetials['data']['card']['teams']['a']['match']['players'];
                                        }
                                        if(isset($findsquaddetials['data']['card']['teams']['b']['match']['players'])){
                                            $squadteam2players = $findsquaddetials['data']['card']['teams']['b']['match']['players'];
                                        }

                                        if((!empty($squadteam1players)) && (!empty($squadteam2players))){
                                            $matchadata['squadstatus'] = 'yes';
                                        }else{
											$matchadata['squadstatus'] = 'yes';
                                            //$matchadata['squadstatus'] = 'no';
                                        }
                                    }
                                    //team details update//
                                    $team1key = $findsquaddetials['data']['card']['teams']['a']['key'];
                                    $team2key = $findsquaddetials['data']['card']['teams']['b']['key'];
                                    //insert team 1//

                                    $findteam1 = DB::table('teams')->where('team_key',$team1key)->select('id')->first();

                                    if(empty($findteam1)){

                                        $data['team_key'] = $team1key;

                                        $data['team'] = $findsquaddetials['data']['card']['teams']['a']['name'];

                                        $team1id = DB::table('teams')->insertGetId($data);
                                    }else{
                                        $team1id = $findteam1->id;

                                    }
                                    //insert team 2//
                                    $findteam2 = DB::table('teams')->where('team_key',$team2key)->select('id')->first();

                                    if(empty($findteam2)){
                                        $data1['team_key'] = $team2key;
                                        // echo $team2key
                                        $data1['team'] = $findsquaddetials['data']['card']['teams']['b']['name'];
                                        $team2id = DB::table('teams')->insertGetId($data1);
                                    }
                                    else{
                                        $team2id = $findteam2->id;
                                    }
                                    //update match data//
                                    $matchadata['status'] = $findsquaddetials['data']['card']['status'];

                                    $matchadata['name'] = $findsquaddetials['data']['card']['name'];
                                    $matchadata['short_name'] = $findsquaddetials['data']['card']['short_name'];
                                    $matchadata['season'] = $findsquaddetials['data']['card']['season']['name'];
                                    $matchadata['title'] = $findsquaddetials['data']['card']['title'];
                                    $matchadata['format'] = $findsquaddetials['data']['card']['format'];
                                    $matchadata['team1'] = $team1id;
                                    $matchadata['team2'] = $team2id;
                                    $matchadata['team1display'] = $findsquaddetials['data']['card']['teams']['a']['short_name'];
                                    $matchadata['team2display'] = $findsquaddetials['data']['card']['teams']['b']['short_name'];
                                    $matchadata['matchkey'] = $findsquaddetials['data']['card']['key'];
                                    $cuurectdate = date('Y-m-d H:i:s',strtotime($findsquaddetials['data']['card']['start_date']['iso']));
                                    //$cuurectdate = Carbon::parse($matchdate )->addHours(5)->addMinutes(30);
                                    $matchadata['start_date'] = $cuurectdate;
                                    DB::table('list_matches')->where('matchkey',$findsquaddetials['data']['card']['key'])->update($matchadata);
                                }
                                //$this->importsquad($getli, 1);
                            }
                            //die;
                        }
                    }

                }

            }


            // return redirect::back();
        }
        return redirect()->action('MatchesController@matchmapping');
        die;

    }

    public function matchmapping($sport_type = 'cricket')
    {
        $sportTypes = DB::table('sport_types')->get();
        // $sports = [];
        // foreach ($sportTypes as $sport) {
        //     $sports[$sport->id] = $sport->sport_name;
        // }

        $sportsKey = [];
        foreach ($sportTypes as $sport) {
            $sportsKey[$sport->id] = $sport->sport_key;
        }

        // $locktime = Carbon::now()->addMinutes(30);
        $locktime = date('Y-m-d H:i:s', strtotime('-10 minutes'));
        $currentdate = date('Y-m-d H:i:s');
        // print_r($currentdate);die;
        // $findalllistmatches = DB::table('list_matches')
        //     ->leftjoin('series','list_matches.series','=','series.id')
        //     ->join('teams as t1','t1.id','=','list_matches.team1')
        //     ->join('teams as t2','t2.id','=','list_matches.team2')
        //     ->join('sport_types as st','st.id','=','list_matches.sport_type')
        //     ->select('t1.id as team1id','t2.id as team2id','t1.short_name as team1name','t2.short_name as team2name','t1.team_key as team1key','t2.team_key as team2key','t1.logo as team1logo','t2.logo as team2logo','t2.team_key as team2key','list_matches.id as listmatchid','st.sport_name as sport_name','series.name as seriesname','list_matches.*')
        //     ->where('list_matches.status','!=','completed')
        //     ->where('list_matches.start_date','>',$locktime)
        //     ->orderBY('start_date','ASC')
        //     ->get();

        $findalllistmatches_cricket = DB::table('list_matches')
            ->leftjoin('series','list_matches.series','=','series.id')
            ->join('teams as t1','t1.id','=','list_matches.team1')
            ->join('teams as t2','t2.id','=','list_matches.team2')
            ->join('sport_types as st','st.id','=','list_matches.sport_type')
            ->select('t1.id as team1id','t2.id as team2id','t1.short_name as team1name','t2.short_name as team2name','t1.team_key as team1key','t2.team_key as team2key','t1.logo as team1logo','t2.logo as team2logo','t2.team_key as team2key','list_matches.id as listmatchid','st.sport_name as sport_name','series.name as seriesname','list_matches.*')
            ->where('list_matches.status','!=','completed')
            ->where('list_matches.start_date','>',$locktime)
            ->where('list_matches.sport_type','1')
			->whereDate('list_matches.start_date','<=',date('Y-m-d',strtotime("+10 day")))
            ->orderBY('start_date','ASC')
            ->get();
 
        $findalllistmatches_football = DB::table('list_matches')
            ->leftjoin('series','list_matches.series','=','series.id')
            ->join('teams as t1','t1.id','=','list_matches.team1')
            ->join('teams as t2','t2.id','=','list_matches.team2')
            ->join('sport_types as st','st.id','=','list_matches.sport_type')
            ->select('t1.id as team1id','t2.id as team2id','t1.short_name as team1name','t2.short_name as team2name','t1.team_key as team1key','t2.team_key as team2key','t1.logo as team1logo','t2.logo as team2logo','t2.team_key as team2key','list_matches.id as listmatchid','st.sport_name as sport_name','series.name as seriesname','list_matches.*')
            ->where('list_matches.status','!=','completed')
            ->where('list_matches.start_date','>',$locktime)
            ->where('list_matches.sport_type','2')
			->whereDate('list_matches.start_date','<=',date('Y-m-d',strtotime("+3 day"))) 
            ->orderBY('start_date','ASC')
            ->get();
			 
		$findalllistmatches_basketball = DB::table('list_matches')
            ->leftjoin('series','list_matches.series','=','series.id')
            ->join('teams as t1','t1.id','=','list_matches.team1')
            ->join('teams as t2','t2.id','=','list_matches.team2')
            ->join('sport_types as st','st.id','=','list_matches.sport_type')
            ->select('t1.id as team1id','t2.id as team2id','t1.short_name as team1name','t2.short_name as team2name','t1.team_key as team1key','t2.team_key as team2key','t1.logo as team1logo','t2.logo as team2logo','t2.team_key as team2key','list_matches.id as listmatchid','st.sport_name as sport_name','series.name as seriesname','list_matches.*')
            ->where('list_matches.status','!=','completed')
            ->where('list_matches.start_date','>',$locktime)
            ->where('list_matches.sport_type','3')
			->whereDate('list_matches.start_date','<=',date('Y-m-d',strtotime("+3 day"))) 
            ->orderBY('start_date','ASC')
            ->get();
			
		$findalllistmatches_nfl = DB::table('list_matches')
            ->leftjoin('series','list_matches.series','=','series.id')
            ->join('teams as t1','t1.id','=','list_matches.team1')
            ->join('teams as t2','t2.id','=','list_matches.team2')
            ->join('sport_types as st','st.id','=','list_matches.sport_type')
            ->select('t1.id as team1id','t2.id as team2id','t1.short_name as team1name','t2.short_name as team2name','t1.team_key as team1key','t2.team_key as team2key','t1.logo as team1logo','t2.logo as team2logo','t2.team_key as team2key','list_matches.id as listmatchid','st.sport_name as sport_name','series.name as seriesname','list_matches.*')
            ->where('list_matches.status','!=','completed')
            ->where('list_matches.start_date','>',$locktime)
            ->where('list_matches.sport_type','4')
			->whereDate('list_matches.start_date','<=',date('Y-m-d',strtotime("+3 day"))) 
            ->orderBY('start_date','ASC')
            ->get();
			
		$findalllistmatches_kabaddi = DB::table('list_matches')
            ->leftjoin('series','list_matches.series','=','series.id')
            ->join('teams as t1','t1.id','=','list_matches.team1')
            ->join('teams as t2','t2.id','=','list_matches.team2')
            ->join('sport_types as st','st.id','=','list_matches.sport_type')
            ->select('t1.id as team1id','t2.id as team2id','t1.short_name as team1name','t2.short_name as team2name','t1.team_key as team1key','t2.team_key as team2key','t1.logo as team1logo','t2.logo as team2logo','t2.team_key as team2key','list_matches.id as listmatchid','st.sport_name as sport_name','series.name as seriesname','list_matches.*')
            ->where('list_matches.status','!=','completed')
            ->where('list_matches.start_date','>',$locktime)
            ->where('list_matches.sport_type','5')
			->whereDate('list_matches.start_date','<=',date('Y-m-d',strtotime("+3 day"))) 
            ->orderBY('start_date','ASC')
            ->get();
 
        // echo "<pre>";print_r($findalllistmatches->sport_type);exit;
        // $sports = DB::table('sport_types')->where('id',$findalllistmatches->sport_type)->get();
        // return view('matches.matchmapping',compact('findalllistmatches','sportTypes', 'sports'));
        // return view('matches.matchmapping',compact('findalllistmatches','sportTypes'));
        return view('matches.matchmapping',compact('findalllistmatches_cricket','findalllistmatches_football','findalllistmatches_basketball','findalllistmatches_nfl','findalllistmatches_kabaddi','sportTypes','sport_type')); 
    }

    public function updateFootballScore($match_key) {
		$match_detail=FootballController::getFootballScore($match_key);
		if($match_detail)
		{
			$showpoints = MatchesController::player_point($match_key, 'other' ,'football');
            $refunded = MatchesController::refund_amount($match_key, 2);
		}
		die;
        //$sportType = DB::table('sport_types')->where('id',2)->first();
        //$match_detail = FootballController::getmatchdetails($match_key);
        //$match_detail = FootballController::getmatchdetails(1241284158878257157);
       /* echo "<pre>";
        print_r($match_detail);
        die;*/
        /*if(!empty($match_detail)) {
            $match_detail = $match_detail['data'];
            $players = @$match_detail['players'];
            $getmtdatastatus['status'] = @$match_detail['match']['status'];
            if($getmtdatastatus['status']=='completed'){
                $getmtdatastatus['final_status'] = 'IsReviewed';
            }
            if(@$match_detail['match']['matchdata_review_checkpoint']=='post-match-validated'){
                $getmtdatastatus['is_verified'] = 1;
            }
            if(!empty($players)) {
                DB::table('result_matches')->where('match_key', $match_key)->where('sport_type', $sportType->id)->delete();
                DB::table('list_matches')->where('matchkey', $match_key)->where('final_status','<>','winnerdeclared')->update($getmtdatastatus);
                $i=0;
                foreach ($players as $player_key => $playing_player) {
                    $stats = @$playing_player['stats'];
                    if(!empty($stats)) { //print_r($playing_player);
                        // $datasv = array();
                        $player_id = $playing_player['key'];
                        $datasvv['player_key'] = $player_id;
                        $datasvv['starting11'] = 1;
                        $datasvv['match_key'] = $match_key;
                        $datasvv['sport_type'] = $sportType->id;
                        $datasvv['innings'] = 1;
                        $player = DB::table('match_players')->join('players', 'players.id', '=', 'match_players.playerid')->where('matchkey', $match_key)->where('match_players.sport_type', $sportType->id)->where('players.sport_type', $sportType->id)->select('match_players.*', 'players.player_key', 'players.role as playerrole')->where('players.player_key', $player_id)->first();
                        if (!empty($player)) {
                            $datasvv['player_id'] = $player->playerid;
                            // $findplayerex = DB::table('result_matches')->where('player_key', $player_id)->where('match_key', $match_key)->where('innings', 1)->select('id')->first();

                            if (!empty($player)) {
                                $playingTime = $stats['minutes_played'];
                                $goals = $stats['goal']['scored'];
                                $assist = $stats['goal']['assist'];
                                $passes = @$stats['passes'] ? $stats['passes'] : 0;
                                $shots = @$stats['shot_on_target'] ? $stats['shot_on_target'] : 0;
                                $cleansheet = @$stats['clean_sheet'] ? 1 : 0;
                                $saveShots = @$stats['goal']['saved'] ? $stats['goal']['saved'] : 0;
                                $penaltySave = $stats['penalty']['saved'];
                                $tackles = @$stats['tackles'] ? $stats['tackles'] : 0;
                                $yellowCard = $stats['card']['YC'];
                                $redCard = $stats['card']['RC'];
                                $goalconceded = @$stats['goal']['conceded'] ? $stats['goal']['conceded'] : 0;
                                $ownGoal = @$stats['own_goal_conceded'] ? $stats['own_goal_conceded'] : 0;
                                $penaltyMiss = $stats['penalty']['missed'];
                                if($playingTime>0) {
                                    $datasv[$i]['minutesplayed'] = $playingTime;
                                    $datasv[$i]['goals'] = $goals;
                                    $datasv[$i]['assist'] = $assist;
                                    $datasv[$i]['totalpass'] = $passes;
                                    $datasv[$i]['shotsontarget'] = $shots;
                                    $datasv[$i]['cleansheet'] = $cleansheet;
                                    $datasv[$i]['shotsblocked'] = $saveShots;
                                    $datasv[$i]['penaltysave'] = $penaltySave;
                                    $datasv[$i]['tacklesuccess'] = $tackles;
                                    $datasv[$i]['yellowcard'] = $yellowCard;
                                    $datasv[$i]['redcard'] = $redCard;
                                    $datasv[$i]['owngoals'] = $ownGoal;
                                    $datasv[$i]['penaltymiss'] = $penaltyMiss;
                                    $datasv[$i]['goalconceded'] = $goalconceded;
                                    $datasv[$i]['sport_type'] = $sportType->id;
                                    $datasv[$i]['match_key'] = $match_key;
                                    $datasv[$i]['sport_type'] = $sportType->id;
                                    $datasv[$i]['player_key'] = $player_id;
                                    $datasv[$i]['player_id'] = $player->playerid;
                                    $datasv[$i]['total_points'] = 0;//($playingTime + $goals + $assist + $passes + $shots + $cleansheet + $saveShots + $penaltySave + $tackles + $yellowCard + $redCard + $ownGoal + $penaltyMiss + $goalconceded);
                                    $datasv[$i]['starting11'] = 1;
                                }
                                $i++;
                            }
                        }
                    }
                }

                try {
                    if(isset($datasv)) {
                        DB::table('result_matches')->insert($datasv);
                    }
                    // }
                } catch(Exception $e) {

                }

                $showpoints = MatchesController::player_point($match_key, 'other' ,'football');

                $refunded = MatchesController::refund_amount($match_key,$sportType->id);

            }
        }*/
    }

    public function importsquad($matchid,$sport_key = '1'){
        $sportType = DB::table('sport_types')->where('id',$sport_key)->first();
        $sport_key = (isset($sportType->sport_key) ? $sportType->sport_key : $sport_key);
        switch ($sport_key){
            case "football":{
				/*$findmatch = DB::table('list_matches')->where('matchkey',$matchid)->first();
				$team1key = DB::table('teams')->where('id',$findmatch->team1)->first()->team_key;
				$team2key = DB::table('teams')->where('id',$findmatch->team2)->first()->team_key;
				echo $team1key." ".$team2key;
                $squads1 = FootballController::getSquad($team1key); 
				$squads2 = FootballController::getSquad($team2key);
				*/
				$squads1 = FootballController::getSquads($matchid);
                /*print_r($squads1);
				print_r($squads2); 
                die;
                if(!empty($squads1)) {
					foreach ($squads1 as $key2 => $value2) {
						$player_key = $value2;
						$playerData = @$squads['players'][$player_key];
						if(!empty($playerData)) {
							$find_player = DB::table('players')->where('player_key', $player_key)->first();
							if(empty($find_player)) {
								$player_insert = array();
								$player_insert['player_name'] = $playerData['name'];
								$player_insert['player_key'] = $playerData['key'];
								$player_insert['sport_type'] = 2;
								$player_insert['team'] = $findTeam->id;
								$player_insert['credit'] = 6;
								$player_insert['role'] = $playerData['role'];
								$name = $playerData['name'];
								$role = $playerData['role'];
								//$player_id = DB::table('players')->insertGetId($player_insert);
							} else {
								$player_insert = array();
								$player_insert['player_name'] = $playerData['name'];
								$player_insert['player_key'] = $playerData['key'];
								$player_insert['sport_type'] = 2;
								$player_insert['team'] = $findTeam->id;
								$player_insert['role'] = $playerData['role'];
								DB::table('players')->where('id',$find_player->id)->update($player_insert);
								$player_id = $find_player->id;
								$role = $playerData['role'];
								$name = $playerData['name'];
							}
							$credit = 8;
							$find_match_player = DB::table('match_players')->where('playerid', $player_id)->where('matchkey', $matchid)->first();
							if(empty($find_match_player)) {
								$matchplayerdata['matchkey'] = $matchid;
								$matchplayerdata['playerid'] = $player_id;
								$matchplayerdata['role'] = $role;
								$matchplayerdata['name'] = $name;
								$matchplayerdata['credit'] = $credit;
								$matchplayerdata['player_key'] = $player_key;
								$matchplayerdata['sport_type'] = 2;
								//DB::table('match_players')->insert($matchplayerdata);
							} else {
								$matchplayerdata = [];
								$matchplayerdata['role'] = $role;
								$matchplayerdata['name'] = $name;
								$matchplayerdata['credit'] = $credit;
								DB::table('match_players')->where('id',$find_match_player->id)->update($matchplayerdata);
							}
						}
					}
					DB::table('list_matches')->where('matchkey', $matchid)->update(array("squadstatus" => "yes"));
				}
				*/ 
				DB::table('list_matches')->where('matchkey', $matchid)->update(array("squadstatus" => "yes"));
                //return Redirect::back();
                break;
            }
            default :{
                $getdetails = CricketapiController::getmatchdetails($matchid);
                if($getdetails['status_code'] == 404){
                    $getdetails = CricketapiController::getmatchdetailsV2($matchid);
                }
                // echo '<pre>'; print_r($getdetails); die;

                if(!empty($getdetails)){
                    $matchkikey = $getdetails['data']['card']['key'];
                    $team1players = $getdetails['data']['card']['teams']['a']['match']['players'];
                    $team2players = $getdetails['data']['card']['teams']['b']['match']['players'];

                    if((!empty($team1players)) && (!empty($team2players))){
                        if(!empty($team1players)){
                            foreach($team1players as $players1){
                                $playerkey = $players1;
                                // insert players details which we get from api//
                                $teamkey = $getdetails['data']['card']['teams']['a']['key'];
                                $findmatchexist = DB::table('teams')->where('team_key',$teamkey)->select('id')->first();
                                if(!empty($findmatchexist)){
                                    $findp1 = DB::table('player_details')->where('player_key',$players1)->first();
                                    $findplayerexist = DB::table('players')->where('player_key',$players1)->where('team',$findmatchexist->id)->first();
                                    $data['player_name'] = $getdetails['data']['card']['players'][$players1]['fullname'];
                                    $data['player_key'] = $playerkey;
                                    $plaerdetailsdata['fullname'] = $getdetails['data']['card']['players'][$players1]['fullname'];
                                    $plaerdetailsdata['player_key'] = $playerkey;
                                    if(empty($findplayerexist)){
                                        $data['team'] = $findmatchexist->id;
                                        if($getdetails['data']['card']['players'][$players1]['seasonal_role']==""){
                                            $data['role'] = 'allrounder';
                                        }
                                        else{
                                            $data['role'] =  $getdetails['data']['card']['players'][$players1]['seasonal_role'];
                                        }
                                        $playerid = DB::table('players')->insertGetId($data);
                                        $credit=9;
                                    }
                                    else{
                                        $playerid = $findplayerexist->id;
                                        $credit = $findplayerexist->credit;
                                        $data['role'] = $findplayerexist->role;
                                        $getdetails['data']['card']['players'][$players1]['seasonal_role']= $findplayerexist->role;
                                    }
                                    /* insert in player details table*/
                                    if(empty($findp1)){
                                        DB::table('player_details')->insert($plaerdetailsdata);
                                    }
                                    // insert players for a match//
                                    $findplayer1entry = DB::table('match_players')->where('matchkey',$matchkikey)->where('playerid',$playerid)->first();
                                    if(empty($findplayer1entry)){
                                        $matchplayerdata['matchkey'] = $matchkikey;
                                        $matchplayerdata['playerid'] = $playerid;
                                        $matchplayerdata['role'] = $data['role'];
                                        $matchplayerdata['name'] = $data['player_name'];
                                        $matchplayerdata['credit'] = $credit;
                                        DB::table('match_players')->insert($matchplayerdata);
                                    }
                                }
                            }
                        }
                        if(!empty($team2players)){
                            foreach($team2players as $players2){
                                $playerkey2 = $players2;
                                $playerid="";
                                $findplayer2exist=array();
                                $data=array();
                                $team2key = $getdetails['data']['card']['teams']['b']['key'];
                                $findmatchexist = DB::table('teams')->where('team_key',$team2key)->select('id')->first();
                                if(!empty($findmatchexist)){
                                    $findp2 = DB::table('player_details')->where('player_key',$players2)->first();
                                    $findplayer2exist = DB::table('players')->where('player_key',$players2)->where('team',$findmatchexist->id)->first();
                                    $data['player_name'] = $getdetails['data']['card']['players'][$players2]['fullname'];
                                    $data['player_key'] = $playerkey2;
                                    $plaerdetailsdata2['fullname'] = $getdetails['data']['card']['players'][$players2]['fullname'];
                                    $plaerdetailsdata2['player_key'] = $playerkey2;
                                    if(empty($findplayer2exist)){
                                        $data['team'] = $findmatchexist->id;
                                        if($getdetails['data']['card']['players'][$players2]['seasonal_role']==""){
                                            $data['role'] =  'allrounder';
                                        }else{
                                            $data['role']=$getdetails['data']['card']['players'][$players2]['seasonal_role'];
                                        }
                                        $playerid =  DB::table('players')->insertGetId($data);
                                        $credit=9;
                                    }
                                    else{
                                        $playerid = $findplayer2exist->id;
                                        $credit = $findplayer2exist->credit;
                                        $getdetails['data']['card']['players'][$players2]['seasonal_role']= $findplayer2exist->role;
                                        $data['role'] =  $findplayer2exist->role;
                                    }

                                    /* insert in player details table*/
                                    if(empty($findp2)){
                                        DB::table('player_details')->insert($plaerdetailsdata2);
                                    }
                                    $findplayer2entry = DB::table('match_players')->where('matchkey',$matchkikey)->where('playerid',$playerid)->first();
                                    if(empty($findplayer2entry)){
                                        $matchplayerdata['matchkey'] = $matchkikey;
                                        $matchplayerdata['playerid'] = $playerid;
                                        if($data['role']!=""){
                                            $matchplayerdata['role'] = $data['role'];
                                        }
                                        $matchplayerdata['name'] = $data['player_name'];
                                        $matchplayerdata['credit'] = $credit;
                                        DB::table('match_players')->insert($matchplayerdata);
                                    }
                                }
                            }
                        }



                        $matchadata['squadstatus'] = 'yes';
                        DB::table('list_matches')->where('matchkey',$matchid)->update($matchadata);
                        Session::flash('message', 'Successfully updated!');
                        Session::flash('alert-class', 'alert-success');
                        return redirect()->action('MatchesController@matchmapping');
                    }else{
                        return redirect()->action('MatchesController@matchmapping');
                    }
                }
            }
        }




    }

    public function testmatch($matchid){
        $getdetails = CricketapiController::getmatchdetails($matchid);
        echo '<pre>'; print_r($getdetails); die;
    }


    public function viewmatchdetails($matchid){
        $findmatch = DB::table('list_matches')->where('matchkey',$matchid)->first();
// 			echo '<pre>'; print_r($findmatch); die;
        if(!empty($findmatch)){
            if($findmatch->launch_status!='launched'){
                $getdetails = CricketapiController::getmatchdetails($matchid);
                // echo '<pre>'; print_r($getdetails); die;
                if(!empty($getdetails)){
                    $matchkikey = $getdetails['data']['card']['key'];
                    $team1players=array();
                    $team2players=array();
                    if(isset($getdetails['data']['card']['teams']['a']['match']['players'])){
                        $team1players = $getdetails['data']['card']['teams']['a']['match']['players'];
                    }
                    if(isset($getdetails['data']['card']['teams']['b']['match']['players'])){
                        $team2players = $getdetails['data']['card']['teams']['b']['match']['players'];
                    }

                    // 		echo '<pre>'; print_r($team1players);
                    // 		echo '<pre>'; print_r($team2players);
                    // 		die;
                    if(!empty($team1players)){
                        foreach($team1players as $players1){
                            $playerkey = $players1;
                            // insert players details which we get from api//
                            $teamkey = $getdetails['data']['card']['teams']['a']['key'];
                            $findmatchexist = DB::table('teams')->where('team_key',$teamkey)->select('id')->first();
                            if(!empty($findmatchexist)){
                                $findp1 = DB::table('player_details')->where('player_key',$players1)->first();
                                $findplayerexist = DB::table('players')->where('player_key',$players1)->where('team',$findmatchexist->id)->first();
                                $data['player_name'] = $getdetails['data']['card']['players'][$players1]['fullname'];
                                $data['player_key'] = $playerkey;
                                $plaerdetailsdata['fullname'] = $getdetails['data']['card']['players'][$players1]['fullname'];
                                $plaerdetailsdata['player_key'] = $playerkey;
                                if(empty($findplayerexist)){
                                    $data['team'] = $findmatchexist->id;
                                    if($getdetails['data']['card']['players'][$players1]['seasonal_role']==""){
                                        $data['role'] = 'allrounder';
                                    }
                                    else{
                                        $data['role'] =  $getdetails['data']['card']['players'][$players1]['seasonal_role'];
                                    }
                                    $playerid = DB::table('players')->insertGetId($data);
                                    $credit=9;
                                }
                                else{
                                    $playerid = $findplayerexist->id;
                                    $credit = $findplayerexist->credit;
                                    $data['role'] = $findplayerexist->role;
                                    $getdetails['data']['card']['players'][$players1]['seasonal_role']= $findplayerexist->role;
                                }
                                /* insert in player details table*/
                                if(empty($findp1)){
                                    DB::table('player_details')->insert($plaerdetailsdata);
                                }
                                // insert players for a match//
                                $findplayer1entry = DB::table('match_players')->where('matchkey',$matchkikey)->where('playerid',$playerid)->first();
                                if(empty($findplayer1entry)){
                                    $matchplayerdata['matchkey'] = $matchkikey;
                                    $matchplayerdata['playerid'] = $playerid;
                                    $matchplayerdata['role'] = $data['role'];
                                    $matchplayerdata['name'] = $data['player_name'];
                                    $matchplayerdata['credit'] = $credit;
                                    DB::table('match_players')->insert($matchplayerdata);
                                }
                            }
                        }
                    }
                    if(!empty($team2players)){
                        foreach($team2players as $players2){
                            $playerkey2 = $players2;
                            $playerid="";
                            $findplayer2exist=array();
                            $data=array();
                            $team2key = $getdetails['data']['card']['teams']['b']['key'];
                            $findmatchexist = DB::table('teams')->where('team_key',$team2key)->select('id')->first();
                            if(!empty($findmatchexist)){
                                $findp2 = DB::table('player_details')->where('player_key',$players2)->first();
                                $findplayer2exist = DB::table('players')->where('player_key',$players2)->where('team',$findmatchexist->id)->first();
                                $data['player_name'] = $getdetails['data']['card']['players'][$players2]['fullname'];
                                $data['player_key'] = $playerkey2;
                                $plaerdetailsdata2['fullname'] = $getdetails['data']['card']['players'][$players2]['fullname'];
                                $plaerdetailsdata2['player_key'] = $playerkey2;
                                if(empty($findplayer2exist)){
                                    $data['team'] = $findmatchexist->id;
                                    if($getdetails['data']['card']['players'][$players2]['seasonal_role']==""){
                                        $data['role'] =  'allrounder';
                                    }else{
                                        $data['role']=$getdetails['data']['card']['players'][$players2]['seasonal_role'];
                                    }
                                    $playerid =  DB::table('players')->insertGetId($data);
                                    $credit=9;
                                }
                                else{
                                    $playerid = $findplayer2exist->id;
                                    $credit = $findplayer2exist->credit;
                                    $getdetails['data']['card']['players'][$players2]['seasonal_role']= $findplayer2exist->role;
                                    $data['role'] =  $findplayer2exist->role;
                                }

                                /* insert in player details table*/
                                if(empty($findp2)){
                                    DB::table('player_details')->insert($plaerdetailsdata2);
                                }
                                $findplayer2entry = DB::table('match_players')->where('matchkey',$matchkikey)->where('playerid',$playerid)->first();
                                if(empty($findplayer2entry)){
                                    $matchplayerdata['matchkey'] = $matchkikey;
                                    $matchplayerdata['playerid'] = $playerid;
                                    if($data['role']!=""){
                                        $matchplayerdata['role'] = $data['role'];
                                    }
                                    $matchplayerdata['name'] = $data['player_name'];
                                    $matchplayerdata['credit'] = $credit;
                                    DB::table('match_players')->insert($matchplayerdata);
                                }
                            }
                        }
                    }
                }
            }
            else{
                return redirect()->action('MatchesController@launchmatch',compact('matchid'))->withErrors('already imported');
            }
        }else{
            return redirect()->action('MatchesController@launchmatch',compact('matchid'))->withErrors('Invalid match Provided');
        }

        return redirect()->action('MatchesController@launchmatch',compact('matchid'));
    }
    public function editmatch($matchkey,Request $request){
        $currentdate = date('Y-m-d');
        if ($request->isMethod('post')){
            $input= Input::all();
            unset($input['_token']);
            DB::table('list_matches')->where('matchkey',$matchkey)->update($input);
            Session::flash('message', 'Successfully updated match details!');
            Session::flash('alert-class', 'alert-success');
            $match = DB::table('list_matches')->where('matchkey',$matchkey)->first();
            $type = 'cricket';
            if($match->sport_type == 2){
                $type = 'football';
            } 
			$findNotifications = DB::table('bulk_notifications')->where('matchkey', $matchkey)->where('preToss', 0)->get();
			if(empty($findNotifications)){
				//$newData1 = array();
				$newData2 = array();
				//$newData3 = array();
				/*$newData1['matchkey']=$matchkey;
				$newData1['minutes']=32;
				$newData1['display']=30;*/
				$newData2['matchkey']=$matchkey;
				$newData2['minutes']=12;
				$newData2['display']=10;
				/*$newData3['matchkey']=$matchkey;
				$newData3['minutes']=7;
				$newData3['display']=5;*/
				//DB::table('bulk_notifications')->insert($newData1);
				DB::table('bulk_notifications')->insert($newData2);
				//DB::table('bulk_notifications')->insert($newData3);
				/*$newData1 = array();
				$newData1['matchkey']=$matchkey;
				$newData1['minutes']=62;
				$newData1['display']=60;
				DB::table('bulk_notifications')->insert($newData1);*/
			}
			if($input['PreTossAvailable']==1)
			{
				$findNotifications2 = DB::table('bulk_notifications')->where('matchkey', $matchkey)->where('preToss', 1)->get();
				if(empty($findNotifications2)){
					$newData4 = array();
					//$newData5 = array();
					//$newData6 = array();
					$newData4['matchkey']=$matchkey;
					$newData4['minutes']=32;
					$newData4['display']=30;
					$newData4['preToss']=1;
					/*$newData5['matchkey']=$matchkey;
					$newData5['minutes']=12;
					$newData5['display']=10;
					$newData5['preToss']=1;*/
					$newData6['matchkey']=$matchkey;
					$newData6['minutes']=7;
					$newData6['display']=5;
					$newData6['preToss']=1;
					$newData7['matchkey']=$matchkey;
					$newData7['minutes']=7;
					$newData7['display']=5;
					$newData7['preToss']=0;
					DB::table('bulk_notifications')->insert($newData4);
					//DB::table('bulk_notifications')->insert($newData5);
					DB::table('bulk_notifications')->insert($newData6);
					DB::table('bulk_notifications')->insert($newData7);
					/*$newData4 = array();
					$newData4['matchkey']=$matchkey;
					$newData4['minutes']=62;
					$newData4['display']=60;
					$newData4['preToss']=1;
					DB::table('bulk_notifications')->insert($newData4);*/
				}
			}
			else if($input['PreTossAvailable']==0)
			{
				$findNotifications2 = DB::table('bulk_notifications')->where('matchkey', $matchkey)->where('preToss', 1)->get();
				if(!empty($findNotifications2)){
					DB::table('bulk_notifications')->where('matchkey', $matchkey)->where('preToss', 1)->delete();
				} 
			}
            return redirect()->to('/main-admin/matchmapping/'.$type);//->action('MatchesController@matchmapping');
        }
        $findmatchdetails = DB::table('list_matches')->join('teams as t1','t1.id','=','list_matches.team1')->join('teams as t2','t2.id','=','list_matches.team2')->where('matchkey',$matchkey)->select('t1.id as team1id','t2.id as team2id','t1.team as team1name','t2.team as team2name','t1.team_key as team1key','t2.team_key as team2key','list_matches.id as listmatchid','list_matches.*')->first();
        $findmatchseries = DB::table('series')->where('end_date','>=',$currentdate)->get();
        return view('matches.editmatch',compact('findmatchdetails','findmatchseries'));
    }
    public function teammapping($matchkey){
        $findmatchdetails = DB::table('list_matches')->join('teams as t1','t1.id','=','list_matches.team1')->join('teams as t2','t2.id','=','list_matches.team2')->where('matchkey',$matchkey)->select('t1.id as team1id','t2.id as team2id','t1.team as team1name','t2.team as team2name','t1.team_key as team1key','t2.team_key as team2key','list_matches.name','list_matches.id as listmatchid','list_matches.start_date','list_matches.matchkey')->first();
        return view('matches.teammapping',compact('findmatchdetails'));

    }
    public function launchmatch($matchkey){
        $findmatchdetails = DB::table('list_matches')->join('teams as t1','t1.id','=','list_matches.team1')->join('teams as t2','t2.id','=','list_matches.team2')->where('matchkey',$matchkey)->select('t1.id as team1id','t2.id as team2id','t1.team as team1name','t2.team as team2name','t1.team_key as team1key','t2.team_key as team2key','t2.team_key as team2key','t1.logo as team1logo','t2.logo as team2logo','list_matches.id as listmatchid','list_matches.*')->first();
        $batsman1 = 0;$batsman2 = 0;$bowlers1 = 0;$bowlers2 = 0;$allrounder1 = 0;$allrounder2 = 0;$wk1 = 0;$wk2 = 0;$criteria=1;
        if(!empty($findmatchdetails)){
			$match_playing11=array();
			$playing11=DB::table('match_playing11')->where('match_key',$matchkey)->first();
			if(!empty($playing11)){
				$match_playing11=unserialize($playing11->player_ids); 
			}
            $team1 = $findmatchdetails->team1;
            $team2 = $findmatchdetails->team2;
            $findallmatchplayers = DB::table('match_players')->where('matchkey',$matchkey)->join('players','match_players.playerid','=','players.id')->select('players.team','match_players.*')->get();
            if(!empty($findallmatchplayers)){
                foreach($findallmatchplayers as $matchplay){
                    if($matchplay->team==$team1){
                        if($matchplay->role=='bowler'){
                            $bowlers1++;
                        }
                        if($matchplay->role=='batsman'){
                            $batsman1++;
                        }
                        if($matchplay->role=='allrounder'){
                            $allrounder1++;
                        }
                        if($matchplay->role=='keeper'){
                            $wk1++;
                        }
                        if($matchplay->role==""){
                            $criteria=0;
                            Session::flash('message', 'You cannot launch this match because the role of '.ucwords($matchplay->name).' is not defined.');
                            Session::flash('alert-class', 'alert-danger');
                            // return Redirect()->action('MatchesController@launchmatch',$matchkey);
                        }
                    }
                    if($matchplay->team==$team2){
                        if($matchplay->role=='bowler'){
                            $bowlers2++;
                        }
                        if($matchplay->role=='batsman'){
                            $batsman2++;
                        }
                        if($matchplay->role=='allrounder'){
                            $allrounder2++;
                        }
                        if($matchplay->role=='keeper'){
                            $wk2++;
                        }
                        if($matchplay->role==""){
                            $criteria=0;
                            Session::flash('message', 'You cannot launch this match because the role of '.ucwords($matchplay->name).' is not defined.');
                            Session::flash('alert-class', 'alert-danger');
                            // return Redirect()->action('MatchesController@launchmatch',$matchkey);
                        }
                    }
                }
            }
        }
        return view('matches.launchmatch',compact('findmatchdetails','match_playing11', 'batsman1','batsman2','bowlers1','bowlers2','allrounder1','allrounder2','wk1','wk2'));
    }
	public function setLineup(Request $request){ 
		if ($request->isMethod('post')){
            $input = Input::all();
            $match_key = $input['matchkey']; 
			//$sport_type = $input['sport_type'];
			//$lineup = explode(",", $input['lineup']);
			/*$lineupint = array();
			foreach($playerid as $lineup){
				array_push($lineupint,int($playerid));
			}*/
			$insert_data = array(
				"match_key" => $match_key,
				"player_ids" => serialize(array_map('intval', explode(',', $input['lineup']))),
				"isLocked" => 1
			);
			$findExist = DB::table('match_playing11')->where('match_key', $match_key)->first();
			if(!empty($findExist)) {
				DB::table('match_playing11')->where('match_key', $match_key)->update($insert_data);
			} else {
				DB::table('match_playing11')->insert($insert_data);
				$findmatch = DB::table('list_matches')->where('matchkey',$match_key)->where('only_admin',0)->first();
				$findseries = DB::table('series')->where('id',$findmatch->series)->first();
				if(!empty($findseries)){
					$seriesName=str_replace($findseries->name,"? ","");
					$pos = strpos($findseries->name,"|");
					$seriesName= $pos !== FALSE ? substr($findseries->name, $pos + strlen("|"), strlen($findseries->name)) : "";
					$timeNow = date('Y-m-d H:i:s');
					$timeBefore = date('Y-m-d H:i:s',strtotime('-1 minutes', strtotime($findmatch->start_date)));
					if(!empty($findmatch) && ($timeBefore>=$timeNow)) {
						Helpers::sendnotificationBulk("Lineup Out 📢 ".$findmatch->short_name,"🏏🏏".$seriesName." 🏏🏏\n".$findmatch->name."\nMatch Time ⏰: ".date('h:i A',strtotime($findmatch->start_date)),'FANADDA2');
					}
				}
			}
		}
	}
	
	public function unlockLineup($match_key,$sport_type = 1){
		$insert_data = array(
			"isLocked" => 0
		);
		$findExist = DB::table('match_playing11')->where('match_key', $match_key)->first();
		if(!empty($findExist)) {
			DB::table('match_playing11')->where('match_key', $match_key)->update($insert_data);
		}
		self::get_playing_11($match_key);
		Session::flash('message', 'API Lineup is taken now'); 
		Session::flash('alert-class', 'alert-success');
		return Redirect()->action('MatchesController@launchmatch',$match_key);
	}
	
    public function launch($matchkey,$sport_type = 1){ 
        $datastatus['launch_status'] = 'launched';
		$datastatus['only_admin'] = 1; 
        $findmatch = DB::table('list_matches')->where('matchkey',$matchkey)->where('sport_type',$sport_type)->first();
        if(!empty($findmatch)){
            if($findmatch->series==0 || $findmatch->series==""){
                Session::flash('message', 'You cannot launch this match. Series is required in this match.');
                Session::flash('alert-class', 'alert-danger');
                return Redirect()->action('MatchesController@launchmatch',$matchkey);
            }
            $team1 = $findmatch->team1;
            $team2 = $findmatch->team2;
            $criteria = 1;
            if($sport_type == 1) {
                $batsman1 = 0;
                $batsman2 = 0;
                $bowlers1 = 0;
                $bowlers2 = 0;
                $allrounder1 = 0;
                $allrounder2 = 0;
                $wk1 = 0;
                $wk2 = 0;
            $findallmatchplayers = DB::table('match_players')->where('matchkey',$matchkey)->join('players','match_players.playerid','=','players.id')->select('players.team','match_players.*')->get();
            if(!empty($findallmatchplayers)){
                foreach($findallmatchplayers as $matchplay){
                    if($matchplay->team==$team1){
                        if($matchplay->role=='bowler'){
                            $bowlers1++;
                        }
                        if($matchplay->role=='batsman'){
                            $batsman1++;
                        }
                        if($matchplay->role=='allrounder'){
                            $allrounder1++;
                        }
                        if($matchplay->role=='keeper'){
                            $wk1++;
                        }
                        if($matchplay->role==""){
                            $criteria=0;
                            Session::flash('message', 'You cannot launch this match because the role of '.ucwords($matchplay->name).' is not defined.');
                            Session::flash('alert-class', 'alert-danger');
                            return Redirect()->action('MatchesController@launchmatch',$matchkey);
                        }
                    }
                    if($matchplay->team==$team2){
                        if($matchplay->role=='bowler'){
                            $bowlers2++;
                        }
                        if($matchplay->role=='batsman'){
                            $batsman2++;
                        }
                        if($matchplay->role=='allrounder'){
                            $allrounder2++;
                        }
                        if($matchplay->role=='keeper'){
                            $wk2++;
                        }
                        if($matchplay->role==""){
                            $criteria=0;
                            return Redirect()->action('MatchesController@launchmatch',$matchkey)->withErrors('You cannot launch this match because the role of '.ucwords($matchplay->name).' is not defined.');
                        }
                    }
                }
                if($bowlers1<3){
                    $criteria=0;
                    return Redirect()->action('MatchesController@launchmatch',$matchkey)->withErrors('Minimum 3 bowlers are required in team1 to launch this match');
                    } else if ($bowlers2 < 3) {
                    $criteria=0;
                    return Redirect()->action('MatchesController@launchmatch',$matchkey)->withErrors('Minimum 3 bowlers are required in team2 to launch this match');
                    } else if ($batsman1 < 3) {
                    $criteria=0;
                    return Redirect()->action('MatchesController@launchmatch',$matchkey)->withErrors('Minimum 3 batman are required in team1 to launch this match');
                    } else if ($batsman2 < 3) {
                    $criteria=0;
                    return Redirect()->action('MatchesController@launchmatch',$matchkey)->withErrors('Minimum 3 batman are required in team2 to launch this match');
                    } else if ($wk1 < 1) {
                    $criteria=0;
                    return Redirect()->action('MatchesController@launchmatch',$matchkey)->withErrors('Minimum 1 wicketkeeper is required in team1 to launch this match');
                    } else if ($wk2 < 1) {
                    $criteria=0;
                    return Redirect()->action('MatchesController@launchmatch',$matchkey)->withErrors('Minimum 1 wicketkeeper is required in team2 to launch this match');
                    } else if ($allrounder1 < 1) {
                    $criteria=0;
                    return Redirect()->action('MatchesController@launchmatch',$matchkey)->withErrors('Minimum 1 all rounder are required in team1 to launch this match');
                    } else if ($allrounder2 < 1) {
                    $criteria=0;
                    return Redirect()->action('MatchesController@launchmatch',$matchkey)->withErrors('Minimum 1 all rounder are required in team2 to launch this match');
                    }
                }
            }
            if($criteria==1){
                DB::table('list_matches')->where('matchkey',$matchkey)->update($datastatus);
                //insert marathon challenge//
                $findseriesid = $findmatch->series;
                $findmarathon = DB::table('marathon')->where('series',$findseriesid)->first();
                if(!empty($findmarathon)){
                    $mdata['name'] = $findmarathon->name;
                    $mdata['challenge_id'] = $findmarathon->id;
                    $mdata['entryfee'] = $findmarathon->entryfee;
                    $mdata['win_amount'] = $findmarathon->win_amount;
                    $mdata['maximum_user'] = $findmarathon->maximum_user;
                    $mdata['bestmatches'] = $findmarathon->bestmatches;
                    $mdata['marathon'] = 1;
                    $mdata['confirmed_challenge'] = 1;
                    $mdata['series_id'] = $findseriesid;
                    $mdata['matchkey'] = $findmatch->matchkey;
                    $findmchallenge = DB::table('match_challenges')->where('challenge_id',$findmarathon->id)->where('marathon',1)->where('matchkey',$matchkey)->where('series_id',$findseriesid)->first();
                    if(empty($findmchallenge)){
                        $getmcid = DB::table('match_challenges')->insertGetId($mdata);
                        // $findmpricecrads = DB::table('marathonpricecards')->where('challenge_id',$findmarathon->id)->get();
                        // if(!empty($findmpricecrads)){
                        // foreach($findmpricecrads as $mpricec){
                        // $mpdata['challenge_id'] = $getmcid;
                        // $mpdata['matchkey'] = $findmatch->matchkey;
                        // $mpdata['winners'] = $mpricec->winners;
                        // $mpdata['price'] = $mpricec->price;
                        // $mpdata['min_position'] = $mpricec->min_position;
                        // $mpdata['max_position'] = $mpricec->max_position;
                        // $mpdata['description'] = $mpricec->description;
                        // $mpdata['total'] = $mpricec->total;
                        // DB::table('matchpricecards');
                        // DB::table('matchpricecards')->insert($mpdata);
                        // }
                        // }
                    }
                }
                // insert normal challnege//
                $challengesCategory = DB::table('challenges_category')->where('sport_type', $sport_type)->get();
                $categories = [];
                foreach ($challengesCategory  as $category) {
                    $categories[] = $category->id;
                }


                $findleauges = DB::table('challenges')->where('global', 1)->whereIn('challenge_category_id', $categories)->get();

                $charray = array();
                if(!empty($findleauges)){
                    foreach($findleauges as $ll){
                        $data['challenge_id'] = $ll->id;
						$data['sport_type'] = $ll->sport_type;
                        $data['challenge_category_id'] = $ll->challenge_category_id;
                        $data['entryfee'] = $ll->entryfee;
						$data['challenge_type'] = $ll->challenge_type;
						$data['winning_percentage'] = $ll->winning_percentage;
                        $data['win_amount'] = $ll->win_amount;
                        $data['maximum_user'] = $ll->maximum_user;
						$data['minimum_user'] = $ll->minimum_user; 
                        $data['multi_entry'] = $ll->multi_entry;
						$data['multi_entry_limit'] = $ll->multi_entry_limit;
                        $data['confirmed_challenge'] = $ll->confirmed_challenge;
                        $data['is_running'] = $ll->is_running;
                        $data['bonus'] = $ll->bonus;
                        $data['bonus_percent'] = $ll->bonus_percent;
						$data['preToss'] = $ll->preToss;
						$data['is_deletable'] = $ll->is_deletable;
						$data['delete_before'] = $ll->delete_before;
						$data['unlimited'] = $ll->unlimited;
						$data['refund_unutilized_percentage'] = $ll->refund_unutilized_percentage;
						$data['refund_winning_percentage'] = $ll->refund_winning_percentage;
						$data['refund_bonus_percentage'] = $ll->refund_bonus_percentage;
						$data['only_admin'] = $ll->only_admin;
                        $data['matchkey'] = $findmatch->matchkey;
                        $findchallenge = DB::table('match_challenges')->where('challenge_id',$ll->id)->where('matchkey',$matchkey)->first();
                        if(empty($findchallenge)){
                            DB::table('match_challenges');
                            $getcid = DB::table('match_challenges')->insertGetId($data);
                            $findpricecrads = DB::table('price_cards')->where('challenge_id',$ll->id)->get();
                            if(!empty($findpricecrads)){
                                foreach($findpricecrads as $pricec){
                                    $pdata['challenge_id'] = $getcid;
                                    $pdata['matchkey'] = $findmatch->matchkey;
                                    $pdata['winners'] = $pricec->winners;
                                    $pdata['price'] = $pricec->price;
                                    $pdata['min_position'] = $pricec->min_position;
                                    $pdata['max_position'] = $pricec->max_position;
                                    $pdata['description'] = $pricec->description;
                                    $pdata['total'] = $pricec->total;
                                    DB::table('match_price_cards');
                                    DB::table('match_price_cards')->insert($pdata);
                                }
                            }
                        }
                    }
                }
                Session::flash('message', 'Successfully launched this match!');
                Session::flash('alert-class', 'alert-success');
                // 	return Redirect::back();
                return redirect()->action('MatchesController@matchmapping');
            }
        }else{
            return redirect()->action('MatchesController@matchmapping')->withErrors('Invalid match Provided');
        }
    }
	public function importLastMatchPlayers($matchkey,$sport_type = 1,$team=1){
        $findmatch = DB::table('list_matches')->where('matchkey',$matchkey)->where('sport_type',$sport_type)->first();
        if(!empty($findmatch)){
			$teamId=0;
			if($team==1)
				$teamId = $findmatch->team1;
			else
				$teamId = $findmatch->team2;
			$id = $findmatch->id;
			$findTeamLastMatchKey='UNKNOWN';
			$findTeamLastMatch1 = DB::table('list_matches')->where('list_matches.id','<',$id)->where('sport_type',$sport_type)->where('team1',$teamId)->orderBy('list_matches.id','DESC')->first();
			$findTeamLastMatch2 = DB::table('list_matches')->where('list_matches.id','<',$id)->where('sport_type',$sport_type)->where('team2',$teamId)->orderBy('list_matches.id','DESC')->first();
			if(!empty($findTeamLastMatch1) || !empty($findTeamLastMatch2)){ 
				if(empty($findTeamLastMatch1))
					$findTeamLastMatchKey = $findTeamLastMatch2->matchkey;
				else if(empty($findTeamLastMatch2))
					$findTeamLastMatchKey = $findTeamLastMatch1->matchkey;
				else{
					if($findTeamLastMatch1->start_date>$findTeamLastMatch2->start_date)
						$findTeamLastMatchKey = $findTeamLastMatch1->matchkey;
					else
						$findTeamLastMatchKey = $findTeamLastMatch2->matchkey;
				} 
				$findTeamLastMatchKey="rswst20_2020_g13";
				$findTeamLastMatchPlayers = DB::table('match_players')->where('matchkey', $findTeamLastMatchKey)->where('sport_type',$sport_type)->get();
				$findTeamCurrentMatchPlayers = DB::table('match_players')->where('matchkey', $matchkey)->where('sport_type',$sport_type)->get();
				if(!empty($findTeamLastMatchPlayers)){
					//clear current match team data first
					if(!empty($findTeamCurrentMatchPlayers)){
						foreach($findTeamCurrentMatchPlayers as $player1){
							$findPlayerDetails1 = DB::table('players')->where('id', $player1->playerid)->where('sport_type',$sport_type)->where('team',$teamId)->get();
							if(!empty($findPlayerDetails1)){
								DB::table('match_players')->where('matchkey', $matchkey)->where('sport_type',$sport_type)->where('playerid',$player1->playerid)->delete();
							}
						}
					}
					//then import from last match
					foreach($findTeamLastMatchPlayers as $player2){ 
						$newData = array();
						$findPlayerDetails2 = DB::table('players')->where('id', $player2->playerid)->where('sport_type',$sport_type)->where('team',$teamId)->get();
						if(!empty($findPlayerDetails2)){
							$newData['sport_type']=$player2->sport_type;
							$newData['matchkey']=$matchkey;
							$newData['player_key']=$player2->player_key;
							$newData['playerid']=$player2->playerid;
							$newData['points']=0;
							$newData['role']=$player2->role;
							$newData['credit']=$player2->credit;
							$newData['name']=$player2->name;
							DB::table('match_players')->insert($newData);
						}
					}
				}
            }
			Session::flash('message', 'Successfully imported last match '.$findTeamLastMatchKey.' players!'); 
			Session::flash('alert-class', 'alert-success');
			return Redirect()->action('MatchesController@launchmatch',$matchkey);
        }else{
            return redirect()->action('MatchesController@launchmatch',$matchkey)->withErrors('Invalid match Provided'); 
        }
    }
	public function deleteMatchPlayers($matchkey,$sport_type = 1,$team=1){
        $findmatch = DB::table('list_matches')->where('matchkey',$matchkey)->where('sport_type',$sport_type)->first();
        if(!empty($findmatch)){
			$teamId=0;
			if($team==1)
				$teamId = $findmatch->team1;
			else
				$teamId = $findmatch->team2;
			$findTeamCurrentMatchPlayers = DB::table('match_players')->where('matchkey', $matchkey)->where('sport_type',$sport_type)->get();
			//clear current match team data first
			if(!empty($findTeamCurrentMatchPlayers)){
				foreach($findTeamCurrentMatchPlayers as $player1){
					$findPlayerDetails1 = DB::table('players')->where('id', $player1->playerid)->where('sport_type',$sport_type)->where('team',$teamId)->get();
					if(!empty($findPlayerDetails1)){
						DB::table('match_players')->where('matchkey', $matchkey)->where('sport_type',$sport_type)->where('playerid',$player1->playerid)->delete();
					}
				}
			}
			Session::flash('message', 'Successfully cleared !');
			Session::flash('alert-class', 'alert-success');
			return Redirect()->action('MatchesController@launchmatch',$matchkey);
		}
		else{
            return redirect()->action('MatchesController@launchmatch',$matchkey)->withErrors('Invalid match Provided'); 
        }
    }
    public function activatematch($matchkey){
        $datastatus['launch_status'] = 'activated';
        $findmatch = DB::table('list_matches')->where('matchkey',$matchkey)->first();
        if(!empty($findmatch)){
            DB::table('list_matches')->where('matchkey',$matchkey)->update($datastatus);
            Session::flash('message', 'Successfully activated this match!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->action('MatchesController@matchmapping');
        }else{
            return redirect()->action('MatchesController@matchmapping')->withErrors('Invalid match Provided');
        }
    }
    public function playerroles($playerid,Request $request){
        $findplayerdetails = DB::table('match_players')->where('id',$playerid)->first();
        if ($request->isMethod('post')){
            $input= Input::all();
            $data['role'] = $input['role'];
            $data['credit'] = $input['credit'];
            DB::table('match_players')->where('id',$playerid)->update($data);
            if(isset($input['global'])){
                DB::table('players')->where('id',$findplayerdetails->playerid)->update($data);
            }
        } 
        Session::flash('message', 'Successfully updated the player roles and credit!');
        Session::flash('alert-class', 'alert-success');
        return Redirect::back();
    }
    public function updatelogo($teamid,Request $request){
        if ($request->isMethod('post')){
            $findteam = DB::table('teams')->where('id',$teamid)->select('team','logo')->first();
            $inputt = Input::all();
            $input = $request->input();
            unset($input['_token']);
            $file = $inputt['image'];
            if(!empty($inputt['image'])){
                if(!empty($file)){
                    $destinationPath = 'uploads/teams';
                    $fileName = 'Fanadda-team-'.str_replace(' ', '_', $findteam->team);
                    $imageName = Helpers::imageUpload($file,$destinationPath,$fileName);
                    if($findteam->logo!=""){
                        File::delete($destinationPath.'/'.$findteam->logo);
                    }
                    $datateam['logo'] = $imageName;
                    DB::table('teams')->where('id',$teamid)->update($datateam);
                }
            }
            Session::flash('message', 'Successfully updated the team logo!');
            Session::flash('alert-class', 'alert-success');
            return Redirect::back();
        }
    }
    public function updatescores($matchkey,$sport_type = 1){
        if($sport_type == 1)
        $getdata = $this->getscoresupdates($matchkey);
        else
            $getdata = $this->updateFootballScore($matchkey);
        if($getdata==1){
            Session::flash('message', 'Scores has been refreshed.!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->action('MatchesController@match_points',$matchkey);
        }else{
            Session::flash('message', 'Scores has been refreshed.!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->action('MatchesController@match_points',$matchkey);
        }
    }
    public function update_results_of_matches()
    {
        date_default_timezone_set('Asia/Kolkata');
        // $findmatchexist = DB::table('list_matches')->where('matchkey','irepak_2018_test_01')->get();
        $findmatchexist = DB::table('list_matches')->whereDate('start_date','<=',date('Y-m-d',strtotime("+1 day")))->where('launch_status','launched')->where('status','!=','completed')->where('sport_type','1')->where('match_type','1')->where('final_status','!=','winnerdeclared')->get();
        if(!empty($findmatchexist)){
            foreach($findmatchexist as $val){
				//echo "(".$val->matchkey.")....."; 
                $getcurrentdate = date('Y-m-d H:i:s'); 
				$squadSearchTimings = date('Y-m-d H:i:s', strtotime('-90 minutes', strtotime($val->start_date)));
				$matchtimings = date('Y-m-d H:i:s', strtotime('-0 minutes', strtotime($val->start_date)));
				if($getcurrentdate>$squadSearchTimings && $getcurrentdate<$matchtimings){
					echo "{".$val->matchkey."}.....";
                    self::get_playing_11($val->matchkey);
                }
                if($getcurrentdate>$matchtimings){
					echo "[".$val->matchkey."].....";
                    $this->getscoresupdates($val->matchkey);
                }
            }
            return 1;
        }
    }

    public function update_results_of_football_matches() { 
        //$this->updateFootballScore('1241284158970531854');
        date_default_timezone_set('Asia/Kolkata');
        // $findmatchexist = DB::table('list_matches')->where('matchkey','irepak_2018_test_01')->get();
        //$findmatchexist = DB::table('list_matches')->whereDate('start_date','<=',date('Y-m-d',strtotime("+5 day")))->where('launch_status','launched')->where('status','!=','completed')->where('sport_type','2')->where('final_status','pending')->get();
		$findmatchexist = DB::table('list_matches')->where('launch_status','launched')->where('status','!=','completed')->where('sport_type','2')->where('final_status','!=','winnerdeclared')->get();
        if(!empty($findmatchexist)){
            foreach($findmatchexist as $val){
                FootballController::get_playing_11_football($val->matchkey);
                $match_type = $val->format;
                $getcurrentdate = date('Y-m-d H:i:s');
                $matchtimings = date('Y-m-d H:i:s', strtotime('-0 minutes', strtotime($val->start_date)));
                if($getcurrentdate>$matchtimings){  
					echo "<br/><br/>".$val->matchkey."----";
                    $match_detail=FootballController::getFootballScore($val->matchkey); 
					if($match_detail)
					{
						$showpoints = MatchesController::player_point($val->matchkey, 'other' ,'football');
						$refunded = MatchesController::refund_amount($val->matchkey, 2);
					}
                }
            } 
            return 1;
        }
    }

    public static function get_playing_11_football($match_key) {
        $findmatchtype = DB::table('list_matches')->where('matchkey',$match_key)->select('format','series')->first();
        $giveresresult = FootballController::getmatchdetails($match_key);
       //echo "<pre>";

        $finalplayingteams = array();
        if(!empty($giveresresult)){
            $lineupplayers = @$giveresresult['data']['players'];
            if(!empty($lineupplayers)){
                foreach($lineupplayers as $fl){
                    if(@$fl['in_playing_squad']) {
                        $finalplayingteams[] = $fl;
                    }
                }
            }
            if(!empty($finalplayingteams)){
                foreach($finalplayingteams as $fl){
                    $player_details = DB::table('players')->join('match_players','players.id','=','match_players.playerid')->where('players.player_key',$fl['key'])->where('match_players.sport_type',2)->where('match_players.matchkey', $match_key)->select('players.id')->first();
                    if(!empty($player_details)) {
                        $players[] = $player_details->id;
                    }
                }

                // DB::table('matchplayers')->where('matchkey',   )
                if(!empty($finalplayingteams)) {
                    $insert_data = array(

                        "match_key" => $match_key,

                        "player_ids" => serialize($players),

                    );
                    $findExist = DB::table('match_playing11')->where('match_key', $match_key)->first();
                    if(!empty($findExist)) {

                        DB::table('match_playing11')->where('match_key', $match_key)->update($insert_data);
                    } else {
                        DB::table('match_playing11')->insert($insert_data);
                    }
                }
            }
        }
    }

    public function getspecificscoresupdates($match_key,$matchplayers,$giveres,$finalplayingteams,$winnerplayers,$getmtdatastatus,$team1=0,$innings_count=0){
		$findmatch = DB::table('list_matches')->where('matchkey',$match_key)->first();
		foreach($matchplayers as $player){
			$datasv=array();
			$pid = $player->playerid;
			$playr = DB::table('players')->where('id',$pid)->first();
			$pact_name = $player->player_key;
			$batting_point=1;$bowling_point=1;
			if($innings_count>0 and $team1!=$playr->team)
			{
				$batting_point=0;
			}
			if($innings_count>0 and $team1==$playr->team)
			{
				$bowling_point=0;
			}
			if(isset($giveres[$pact_name]['match']['innings'])){
				$inning = $giveres[$pact_name]['match']['innings'];
				if(!empty($inning)){
					$k=1;
					foreach($inning as $innings){
						$hitterarray = array();
						$throwerarray = array();
						//starting player points//
						$catch = 0;$runouts = 0;$stumbed = 0;$batdots = 0;$balldots = 0;$six = 0;$runs = 0;$fours = 0;$miletone_run = 0;$bball = 0;
						$strike = 0;$acstrike = 0;$grun = 0;$balls = 0;$bballs = 0;$maiden_over = 0;$wicket = 0;$extra = 0;$overs = 0;
						$economy = 0;$duck = 0;$mile_wicket=0; $bowled=0; $lbw=0;
						$datasv = array();
						// if($k==1){
						if(in_array($pact_name,$finalplayingteams)){
							$datasv['starting11']=1;
						}
						else{
							$starting11=0;
						}
						// check for winner players //
						if(in_array($pact_name,$winnerplayers)){
							$datasv['winning']=1;
						}
						// }
						if($bowling_point==1){
							//fielding points//
							$fielding = $inning[$k]['fielding'];
							if(isset($fielding['catches'])){
								$datasv['catch'] = $catch = $catch + $fielding['catches'];
							}
							$findAssociateRunouts = DB::table('wicket_records')->where('bowler_key',$pact_name)->where('wicket_type','AssociateRunout')->where('matchkey',$match_key)->where('innings',$k)->select('id')->get();
							$datasv['runouts'] = $runouts = $runouts + count($findAssociateRunouts);
							/*if(isset($fielding['runouts'])){
								$datasv['runouts'] = $runouts = $runouts + $fielding['runouts'];
							}*/
							$findDirectRunouts = DB::table('wicket_records')->where('bowler_key',$pact_name)->where('wicket_type','DirectRunout')->where('matchkey',$match_key)->where('innings',$k)->select('id')->get();
							if(isset($fielding['stumbeds'])){
								$datasv['stumbed'] = $stumbed = $stumbed + $fielding['stumbeds'] + count($findDirectRunouts);
							}
							else{
								$datasv['stumbed'] = $stumbed = $stumbed + count($findDirectRunouts);
							}
						}
						else{
							$datasv['catch']=0;
							$datasv['runouts']=0;
							$datasv['stumbed']=0;
						}
						//batting fields//
						if($batting_point==1){
							$batting = $inning[$k]['batting'];
							if((!empty($batting))){
								if(isset($batting['strike_rate']) && $player->playerrole!='bowler'){
									$datasv['batting'] = 1;
									$datasv['strike_rate'] = $batting['strike_rate'];
								}
								else{
									$datasv['batting'] = 0;
								}
							}
							if(isset($batting['dots'])){
								$datasv['battingdots'] = $batdots = $batdots + $batting['dots'];
							}
							if(isset($batting['sixes'])){
								$datasv['six'] = $six = $six + $batting['sixes'];
							}
							if(isset($batting['runs'])){
								$datasv['runs'] = $runs = $runs +  $batting['runs'];
							}
							if(isset($batting['fours'])){
								$datasv['fours'] = $fours = $fours + $batting['fours'];
							}
							if(isset($batting['balls'])){
								$datasv['bball'] = $bball = $bball + $batting['balls'];
							}
							//duck out//
							if(isset($batting['dismissed'])){
								if($player->playerrole!='bowler'){
									if(($runs == 0) && ($batting['dismissed'] == 1)){
										$datasv['duck'] = $duck = 1;
									}
								}else{
									$datasv['duck'] = $duck = 0;
								}
								if($batting['dismissed'] == 1){
									$datasv['out_str'] = $batting['out_str'];
								}else{
									$datasv['out_str'] = 'not out';
								}
								// if($batting['ball_of_dismissed']['wicket_type']=='runout') {
								// 	if(!empty($batting['ball_of_dismissed']['other_fielder'])) {
								// 		$run_thrower = $batting['ball_of_dismissed']['other_fielder'];
								// 		$find_thrower_player = DB::table('result_matches')->where('player_key',$run_thrower)->where('match_key',$match_key)->where('innings',$k)->select('id')->first();
								// 		if(!empty($find_thrower_player)){
								// 			//$data_runout_thrower['runouts'] = $1;
								// 			//DB::table('result_matches')->where('id',$findplayerex->id)->update($datasv);
								// 		}
								// 	}
								// }
							}

							// checkk for not out //
							if($getmtdatastatus['status']=='completed'){
								if($player->playerrole!='bowler'){
									if(isset($batting['dismissed']) && isset($batting['balls'])){
										if(empty($batting['dismissed'])){
											$datasv['notout'] = 1;
										}
									}
								}
							}else{
								$datasv['notout'] = 0;
							}
							// check for run out //
							if(isset($batting['ball_of_dismissed'])){
								$d_smm = $batting['ball_of_dismissed'];
								if($batting['ball_of_dismissed']['wicket_type']=='runout'){
									if(isset($d_smm['other_fielder']) && !empty($d_smm['other_fielder'])){
										if(isset($d_smm['fielder']['key']) && !empty($d_smm['fielder']['key'])){
											$findplayerex1 = DB::table('wicket_records')->where('batsman_key',$pact_name)->where('matchkey',$match_key)->where('innings',$k)->where('innings',$k)->where('bowler_key',$d_smm['fielder']['key'])->select('id')->first();
											if(empty($findplayerex1)){
												$findplayerex1list=array();
												$findplayerex1list["batsman_key"]=$pact_name;
												$findplayerex1list["matchkey"]=$match_key;
												$findplayerex1list["innings"]=$k;
												$findplayerex1list["bowler_key"]=$d_smm['fielder']['key'];
												$findplayerex1list["wicket_type"]="AssociateRunout";
												DB::table('wicket_records')->insert($findplayerex1list);
											}
											$findplayerex1 = DB::table('wicket_records')->where('batsman_key',$pact_name)->where('matchkey',$match_key)->where('innings',$k)->where('innings',$k)->where('bowler_key',$d_smm['other_fielder'])->select('id')->first();
											if(empty($findplayerex1)){
												$findplayerex1list=array();
												$findplayerex1list["batsman_key"]=$pact_name;
												$findplayerex1list["matchkey"]=$match_key;
												$findplayerex1list["innings"]=$k;
												$findplayerex1list["bowler_key"]=$d_smm['other_fielder'];
												$findplayerex1list["wicket_type"]="AssociateRunout";
												DB::table('wicket_records')->insert($findplayerex1list);
											}
										}
									}
									else if(!isset($d_smm['other_fielder']) || (isset($d_smm['other_fielder']) && empty($d_smm['other_fielder']))){
										if(isset($d_smm['fielder']['key']) && !empty($d_smm['fielder']['key'])){
											$findplayerex1 = DB::table('wicket_records')->where('batsman_key',$pact_name)->where('matchkey',$match_key)->where('innings',$k)->where('innings',$k)->where('bowler_key',$d_smm['fielder']['key'])->select('id')->first();
											if(empty($findplayerex1)){
												$findplayerex1list=array();
												$findplayerex1list["batsman_key"]=$pact_name;
												$findplayerex1list["matchkey"]=$match_key;
												$findplayerex1list["innings"]=$k;
												$findplayerex1list["bowler_key"]=$d_smm['fielder']['key'];
												$findplayerex1list["wicket_type"]="DirectRunout";
												DB::table('wicket_records')->insert($findplayerex1list);
											}
										}
									}
								}
								if($batting['ball_of_dismissed']['wicket_type']=='bowled' || $batting['ball_of_dismissed']['wicket_type']=='lbw'){
									if(isset($d_smm['bowler']['key']) && !empty($d_smm['bowler']['key'])){
										$findplayerex1 = DB::table('wicket_records')->where('batsman_key',$pact_name)->where('matchkey',$match_key)->where('innings',$k)->select('id')->first();
										if(empty($findplayerex1)){
											$findplayerex1list=array();
											$findplayerex1list["batsman_key"]=$pact_name;
											$findplayerex1list["matchkey"]=$match_key;
											$findplayerex1list["innings"]=$k;
											$findplayerex1list["bowler_key"]=$d_smm['bowler']['key'];
											$findplayerex1list["wicket_type"]=$batting['ball_of_dismissed']['wicket_type'];
											DB::table('wicket_records')->insert($findplayerex1list);
										}
									}
								}
							}
						}
						else{
							$datasv['batting']=0;
							$datasv['strike_rate']=0;
							$datasv['battingdots']=0;
							$datasv['six']=0;
							$datasv['runs']=0;
							$datasv['fours']=0;
							$datasv['bball']=0;
							$datasv['duck']=0;
							$datasv['out_str']=0;
							$datasv['notout']=0;
						}
						
						if($bowling_point==1){
							//bowling fields//
							$bowling = $inning[$k]['bowling'];
							if(!empty($bowling)){
								$datasv['bowling'] = 1;
								$datasv['economy_rate'] = $bowling['economy'];
							}
							if(isset($bowling['runs'])){
								$datasv['grun'] = $grun = $grun + $bowling['runs'];
							}
							if(isset($bowling['dots'])){
								$datasv['balldots'] = $balldots = $balldots + $bowling['dots'];
							}

							if(isset($bowling['balls'])){
								$datasv['balls'] = $balls = $balls + $bowling['balls'];
							}
							if(isset($bowling['maiden_overs'])){
								$datasv['maiden_over'] = $maiden_over = $maiden_over + $bowling['maiden_overs'];
							}
							if(isset($bowling['wickets'])){
								$datasv['wicket'] = $wicket = $wicket + $bowling['wickets'];
							}
							$findBowled = DB::table('wicket_records')->where('bowler_key',$pact_name)->where('wicket_type','bowled')->where('matchkey',$match_key)->where('innings',$k)->select('id')->get();
							$datasv['bowled'] = $bowled = $bowled + count($findBowled);
							$findLbw = DB::table('wicket_records')->where('bowler_key',$pact_name)->where('wicket_type','lbw')->where('matchkey',$match_key)->where('innings',$k)->select('id')->get();
							$datasv['lbw'] = $lbw = $lbw + count($findLbw);
							if(isset($bowling['extras'])){
								$datasv['extra'] = $extra = $extra + $bowling['extras'];
							}
							if(isset($bowling['overs'])){
								$datasv['overs'] = $overs = $overs + $bowling['overs'];
							}
						}
						else{
							$datasv['bowling']=0;
							$datasv['economy_rate']=0;
							$datasv['grun']=0;
							$datasv['balldots']=0;
							$datasv['balls']=0;
							$datasv['maiden_over']=0;
							$datasv['wicket']=0;
							$datasv['extra']=0;
							$datasv['overs']=0;
						}
						$datasv['match_key'] =$match_key;
						$datasv['player_key'] =$pact_name;
						$datasv['player_id'] =$pid;
						$datasv['innings'] =$k;
						//$datasv['runout_hitter'] =0;
						$findplayerex = DB::table('result_matches')->where('player_key',$pact_name)->where('match_key',$match_key)->where('innings',$k)->select('id')->first();
						if(!empty($findplayerex)){
							DB::table('result_matches')->where('id',$findplayerex->id)->update($datasv);
						}else{
							DB::table('result_matches')->insert($datasv);
						}
						$k++;
						//update points//
						// if(!empty($hitterarray)){
						// foreach($hitterarray as $hr){
						// $dplay['match_key'] =$match_key;
						// $explodarr = explode('$$$$',$hr);
						// $dplay['player_key'] =$explodarr[0];
						// $dplay['innings'] =$explodarr[1];
						// $findplayenewentry = DB::table('result_matches')->where('player_key',$dplay['player_key'])->where('match_key',$match_key)->where('innings',$dplay['innings'])->select('id','runout_hitter')->first();
						// if(!empty($findplayenewentry)){
						// $dplay['runout_hitter'] = $findplayenewentry->runout_hitter+1;
						// DB::table('result_matches')->where('id',$findplayenewentry->id)->update($dplay);
						// }
						// }
						// }
					}
				} else {
					$datasv = [];
					if(in_array($pact_name,$finalplayingteams)){
						$starting11=1;
					}
					else{
						$starting11=0;
					}
					if($starting11){
						$datasv['starting11']=1;
						$datasv['match_key'] =$match_key;
						$datasv['player_key'] =$pact_name;
						$datasv['player_id'] =$pid;
						$datasv['innings'] =1;
						//$datasv['runout_hitter'] =0;
						$findplayerex = DB::table('result_matches')->where('player_key',$pact_name)->where('match_key',$match_key)->where('innings',1)->select('id')->first();
						if(!empty($findplayerex)){
							DB::table('result_matches')->where('id',$findplayerex->id)->update($datasv);
						}else{
							DB::table('result_matches')->insert($datasv);
						}
					}
				}
			}
		}

		// exit;
		// updates for hitter and thrower//

		$showpoints = MatchesController::player_point($match_key,$findmatch->format);
		$getcurrentdate = date('Y-m-d H:i:s');
		$matchtimings = date('Y-m-d H:i:s', strtotime('+10 minutes', strtotime($findmatch->start_date)));
		if($getcurrentdate>$matchtimings){
			$refunded = MatchesController::refund_amount($match_key);
		}
		return 1;
	}

    public function getscoresupdates($match_key){
        $findmatch = DB::table('list_matches')->where('matchkey',$match_key)->first();
        $giveresresult = CricketapiController::getmatchdetails($match_key);
        if($giveresresult['status_code'] == '404'){
            $giveresresult = CricketapiController::getmatchdetailsV2($match_key);
        }
        //echo '<pre>'; print_r($giveresresult); //exit;
        if(!empty($giveresresult)){
            $mainarrayget = $giveresresult['data']['card'];
            $getmtdatastatus['status'] = $mainarrayget['status'];
            if($getmtdatastatus['status']=='completed'){
                $getmtdatastatus['final_status'] = 'IsReviewed';
            }
            $winnerteam = "";
            if(isset($mainarrayget['winner_team'])){
                $winnerteam = $mainarrayget['winner_team'];
            }
            DB::table('list_matches')->where('matchkey',$match_key)->where('final_status','<>','winnerdeclared')->update($getmtdatastatus);
            $findteams = $mainarrayget['teams'];
            $winnerplayers = array();

            $finalplayingteams = array();
            if(!empty($findteams)){
                foreach($findteams as $keytp => $tp){
                    if(isset($tp['match']['playing_xi'])){
                        $findpl = $tp['match']['playing_xi'];
                        if(!empty($findpl)){
                            foreach($findpl as $fl){
                                $finalplayingteams[] = $fl;
                                if($keytp==$winnerteam){
                                    $winnerplayers[] = $fl;
                                }
                            }
                        }
                    }


                }
            }

            if(isset($mainarrayget['players'])){
                $giveres = $mainarrayget['players'];
                $matchplayers = DB::table('match_players')->join('players','players.id','=','match_players.playerid')->where('matchkey',$match_key)->select('match_players.*','players.player_key','players.role as playerrole')->get();
                if(!empty($matchplayers)){
                    MatchesController::getspecificscoresupdates($match_key,$matchplayers,$giveres,$finalplayingteams,$winnerplayers,$getmtdatastatus,0,0);
					$findassocitedmatches = DB::table('list_matches')->where('source_match',$match_key)->where('status','<>','completed')->where('match_type',2)->get();
					
					foreach ($findassocitedmatches as $findassocitedmatch) {
						$getmtdatastatus2 = array();
						if($getmtdatastatus['status']=='completed'){
							$getmtdatastatus2['status'] = 'completed';
							$getmtdatastatus2['final_status'] = 'IsReviewed';
							DB::table('list_matches')->where('matchkey',$findassocitedmatch->matchkey)->where('status','<>','completed')->update($getmtdatastatus2);
						}
						echo "<br/><br/><br/>".$findassocitedmatch->matchkey;
						$team_a_key=$findteams['a']['key'];
						$team_b_key=$findteams['b']['key'];
						$team_a_id = DB::table('teams')->where('team_key',$team_a_key)->where('sport_type',1)->first()->id;
						$team_b_id = DB::table('teams')->where('team_key',$team_b_key)->where('sport_type',1)->first()->id;
						echo $team_a_id." ".$team_b_id;
						//print_r($mainarrayget['batting_order']);
						//echo $findassocitedmatch->matchtypecount;
						//print($mainarrayget['batting_order'][$findassocitedmatch->matchtypecount-1][0]);
						if(isset($mainarrayget['batting_order'][$findassocitedmatch->matchtypecount-1][0]) && $mainarrayget['batting_order'][$findassocitedmatch->matchtypecount-1][0]=="a")
							MatchesController::getspecificscoresupdates($findassocitedmatch->matchkey,$matchplayers,$giveres,$finalplayingteams,$winnerplayers,$getmtdatastatus,$team_a_id,$findassocitedmatch->matchtypecount);
						else if(isset($mainarrayget['batting_order'][$findassocitedmatch->matchtypecount-1][0]) && $mainarrayget['batting_order'][$findassocitedmatch->matchtypecount-1][0]=="b")
							MatchesController::getspecificscoresupdates($findassocitedmatch->matchkey,$matchplayers,$giveres,$finalplayingteams,$winnerplayers,$getmtdatastatus,$team_b_id,$findassocitedmatch->matchtypecount);
					}
                }
            }
        }
        return 1;
    } 



    public function player_point($match_key,$match,$sportType = 'cricket'){
    $matchplayers = DB::table('result_matches')->where('match_key', $match_key)->get();
    switch ($sportType) {
     case "football":
         DB::table('result_points')->where('matchkey', $match_key)->delete();
         // print_r($matchplayers); exit;
         $i = 0;
         foreach ($matchplayers as $row) {
             $findplayerid = DB::table('players')->where('id', $row->player_id)->where('sport_type', 2)->first();
             /*
             //print_r($findplayerid);
             $role = $findplayerid->role;
             $playingTime = (((int)$row->minutesplayed >= 55)  ? 2 : 1 );
             $assist = (int)$row->assist * 5;
             $passes = ((int)$row->totalpass * 0.5) / 10;
             $shots = ((int)$row->shotsontarget * 1) / 2;
             $cleansheet = 0;
             $saveShots = 0;
             $penaltySave = 0;
             $tackles = ((int)$row->tacklesuccess / 3);
             $yellowCard = ((int)$row->yellowcard * (-1));
             $redCard = ((int)$row->redcard * (-3));
             $goalconceded = 0;
             $ownGoal = ((int)$row->owngoals * (-2));
             $penaltyMiss = ((int)$row->penaltymiss * (-2));

             if($role == 'Forward'){
                 $goals = (int)$row->goals * 8;
             } else if($role == 'Midfielder'){
                 $goals = (int)$row->goals * 9;
                 $cleansheet = (int)$row->cleansheet;
             } else {
                 $goals = (int)$row->goals * 10;
                 $cleansheet = (int)$row->cleansheet * 5;
             }

             if($role == 'GK'){
                 $saveShots = ((int)$row->shotsblocked * 2) / 3;
                 $penaltySave = (int)$row->penaltysave * 9;
                 $goalconceded = ((int)$row->goalconceded *  (-1));
             }


             $total = ($playingTime + $goals + $assist + $passes + $shots + $cleansheet + $saveShots + $penaltySave + $tackles + $yellowCard + $redCard + $ownGoal + $penaltyMiss + $goalconceded);
             $role = $findplayerid->role;
             $datasv[$i] = ['matchkey' => $match_key, 'playerid' => $findplayerid->id, 'resultmatch_id' => $row->id, 'total' => $total];
             $datasv[$i]['minutesplayed'] = $playingTime;
             $datasv[$i]['goals'] = $goals;
             $datasv[$i]['assist'] = $assist;
             $datasv[$i]['totalpass'] = $passes;
             $datasv[$i]['shotsontarget'] = $saveShots;
             $datasv[$i]['cleansheet'] = $cleansheet;
             $datasv[$i]['shotsblocked'] = $shots;
             $datasv[$i]['penaltysave'] = $penaltySave;
             $datasv[$i]['tacklesuccess'] = $tackles;
             $datasv[$i]['yellowcard'] = $yellowCard;
             $datasv[$i]['redcard'] = $redCard;
             $datasv[$i]['owngoals'] = $ownGoal;
             $datasv[$i]['penaltymiss'] = $penaltyMiss;
             $datasv[$i]['goalconceded'] = $goalconceded;
             $datasv[$i]['total'] = $total;*/

             $role = $findplayerid->role;
             $datasv[$i] = ['matchkey' => $match_key , 'playerid' => $findplayerid->id ,'resultmatch_id' => $row->id ,'total' => $row->total_points];

			$playingTime = 0;
			
             /*if((int)$row->minutesplayed > 0){
				$playingTime = (((int)$row->minutesplayed >= 55)  ? 2 : 1 );
			 } */
             $assist = (int)$row->assist * 20;
             $passes = (int)($row->totalpass/5)*1;
             $shots = (int)($row->shotsontarget) * 6;
             $cleansheet = 0;
             $saveShots = 0;
             $penaltySave = 0;
             $tackles = (int)($row->tacklesuccess)*4;
             $yellowCard = ((int)$row->yellowcard * (-4));
             $redCard = ((int)$row->redcard * (-10));
             $goalconceded = 0;
             $ownGoal = ((int)$row->owngoals * (-8));
             $penaltyMiss = ((int)$row->penaltymiss * (-20));
			 $playing11= ((int)$row->playing11 * 4);
			 $substitute= ((int)$row->substitute * 2);
			 $chanceCreated= ((int)$row->chanceCreated * 3);
			 $interceptionsWon= ((int)$row->interceptionsWon * 4);
			 $block= ((int)$row->block * 0); 
			 $totalclearance= ((int)$row->totalclearance * 0);

             if($role == 'Forward' ||$role == 'forward' ||$role == 'striker'){
                 $goals = (int)$row->goals * 40;
             } else if($role == 'Midfielder' || $role == 'midfielder'){
                 $goals = (int)$row->goals * 50;
                 // $goalconceded = ((int)$row->goalconceded *  (-2));
             } elseif ($role == 'Defender' || $role == 'defender') {
                 $goalconceded = ((int)($row->goalconceded) *  (-2));
                 $cleansheet = (int)$row->cleansheet*20;
                 $goals = (int)$row->goals * 60;
             } else {
                 $goals = (int)$row->goals * 60;
                 $cleansheet = (int)$row->cleansheet * 20;
             }

             if($role == 'Goalkeeper' || $role == 'goalkeeper'){
                 $saveShots = (int)($row->shotsblocked ) * 6;
                 $penaltySave = (int)$row->penaltysave * 50;
                 $goalconceded = ((int)($row->goalconceded) *  (-2));
             }
			 if((int)$row->minutesplayed > 0){
				$total = ($playing11 + $substitute + $playingTime + $goals + $assist + $passes + $shots + $cleansheet + $saveShots + $penaltySave + $tackles + $yellowCard + $redCard + $ownGoal + $penaltyMiss + $goalconceded + $chanceCreated + $interceptionsWon + $block + $totalclearance);
			 } 
			 else{
				 $total = 0;
			 }
             $datasv[$i]['minutesplayed'] = $playingTime;
             $datasv[$i]['goals'] = $goals;
             $datasv[$i]['assist'] = $assist;
             $datasv[$i]['totalpass'] = $passes;
             $datasv[$i]['shotsontarget'] = $shots;
             $datasv[$i]['cleansheet'] = $cleansheet;
             $datasv[$i]['shotsblocked'] = $saveShots;
             $datasv[$i]['penaltysave'] = $penaltySave;
             $datasv[$i]['tacklesuccess'] = $tackles;
             $datasv[$i]['yellowcard'] = $yellowCard;
             $datasv[$i]['redcard'] = $redCard;
             $datasv[$i]['owngoals'] = $ownGoal;
             $datasv[$i]['penaltymiss'] = $penaltyMiss;
             $datasv[$i]['goalconceded'] = $goalconceded;
			 $datasv[$i]['playing11'] = $playing11;
			 $datasv[$i]['substitute'] = $substitute;
			 $datasv[$i]['chanceCreated'] = $chanceCreated;
			 $datasv[$i]['interceptionsWon'] = $interceptionsWon;
			 $datasv[$i]['block'] = $block;
			 $datasv[$i]['totalclearance'] = $totalclearance;
             $datasv[$i]['total'] = $total;

             $i++;
             DB::table('result_matches')->where('id', $row->id)->update(['total_points' => $total]);
         }
         if (!empty($datasv)) {
             $result = DB::table('result_points')->insert($datasv);
         }
         $this->updateplayerpoints($match_key,2);
         break;
     default :
         if (!empty($matchplayers)) {
             foreach ($matchplayers as $row) {
                 $resultmatchupdate = array();
                 $result = array();
                 $duck = $row->duck;
                 $player_key = $row->player_key;
                 $findplayerid = DB::table('players')->select('id')->where('id', $row->player_id)->first();
                 $findplayerrole = DB::table('match_players')->where('playerid', $findplayerid->id)->where('matchkey', $match_key)->select('role')->first();
                 $runs = $row->runs;
                 $findplayerrole = $findplayerrole->role;
                 $wicket = $row->wicket;
                 $catch = $row->catch;
                 $stumbed = $row->stumbed;
                 $boundary = $row->boundary;
                 $six = $row->six;
                 $fours = $row->fours;
                 $maiden_over = $row->maiden_over;
                 $overs = $row->overs;
                 $balls = $row->balls;
                 $bballs = $row->bball;    //batting balls
                 $grun = $row->grun;            // given runs
                 $economyrate = $row->economy_rate;
                 $strikerate = $row->strike_rate;
                 $runoutpoints = $row->runouts;

                 $startingPoint = 0;
                 $duckpoint = 0;
                 $wkpoints = 0;   //points for wickets
                 $catchpoint = 0;
                 $stpoint = 0;   // points for stumb
                 $boundrypoints = 0;
                 $sixpoints = 0;
                 $runpoints = 0;
                 $centuryPoints = 0;
                 $point150 = 0;
                 $point200 = 0;
                 $halcenturypoints = 0;
                 $maidenpoints = 0;
                 $economypoints = 0;
                 $strikePoint = 0;
                 $extrapoints = 0;
                 $total_points = 0;
                 $batting_points = 0;
                 $bowling_points = 0;
                 $fielding_points = 0;
                 $extra_points = 0;
                 $negative_points = 0;
                 $notoutpoints = 0;
                 $runoutptsss = 0;
                 $winningpoints = 0;
                 $bonuspoints = 0;
                 if ($match == 't20') {
                     if ($row->starting11 == 1 && $row->innings == 1) {
                         $startingpoint = 4;
                     } else {
                         $startingpoint = 0;
                     }

                     if ($findplayerrole != 'bowler') {
                         if ($row->duck != 0 || $row->duck != "") {
                             $duckpoint = -2;
                         }
                     }
                     if ($wicket == 4) {

                         //$extrapoints = $extrapoints + 8;

                         $bonuspoints = $bonuspoints + 8;
                     }

                     if ($wicket >= 5) {

                         //$extrapoints = $extrapoints + 16;

                         $bonuspoints = $bonuspoints + 16;
                     }

                     $wkpoints = $wicket * 25;
                     $catchpoint = $catch * 8;
                     $stpoint = $stumbed * 12;
                     $boundrypoints = $fours * 1;
                     $sixpoints = $six * 2;
                     $runoutptsss = $runoutpoints * 6;

                     if (($runs >= 50) && ($runs < 100)) {
                         //$runpoints = $runpoints+ 8;
                         $halcenturypoints = 8;
                     } else if ($runs >= 100) {
                         //$runpoints =$runpoints + 16;
                         $centuryPoints = 16;
                     }

                     $maidenpoints = $maiden_over * 8;
                     $runpoints = $runpoints + $runs * 1;

                     if ($overs >= 2) {


                         if ($economyrate > 11) {
                             $economypoints = -6;
                         } else if ($economyrate >= 10.1 && $economyrate <= 11) {
                             $economypoints = -4;
                         } else if ($economyrate >= 9 && $economyrate <= 10) {
                             $economypoints = -2;
                         } else if ($economyrate >= 5 && $economyrate <= 6) {
                             $economypoints = 2;
                         } else if ($economyrate >= 4 && $economyrate <= 4.99) {
                             $economypoints = 4;
                         } else if ($economyrate < 4) {
                             $economypoints = 6;
                         }
                     }
                     if (($bballs >= 10) && ($findplayerrole != 'bowler')) {

                         if ($strikerate >= 60 && $strikerate <= 70) {

                             $strikePoint = '-2';
                         } else if ($strikerate >= 50 && $strikerate <= 59.9) {

                             $strikePoint = '-4';
                         } else if ($strikerate < 50) {

                             $strikePoint = '-6';

                             //End

                         }

                     }
                     if ($row->notout == 1) {
                         $notoutpoints = 0;
                     }
                     if ($row->winning == 1) {
                         $winningpoints = 0;
                     }
                 } else if ($match == 't10') {
                     if ($row->starting11 == 1 && $row->innings == 1) {
                         $startingpoint = 4;
                     } else {
                         $startingpoint = 0;
                     }

                     if ($findplayerrole != 'bowler') {
                         if ($row->duck != 0 || $row->duck != "") {
                             $duckpoint = -2;
                         }
                     }
                     if ($wicket == 2) {

                         //$extrapoints = $extrapoints + 8;

                         $bonuspoints = $bonuspoints + 8;
                     }

                     if ($wicket >= 3) {

                         //$extrapoints = $extrapoints + 16;

                         $bonuspoints = $bonuspoints + 16;
                     }

                     $wkpoints = $wicket * 25;
                     $catchpoint = $catch * 8;
                     $stpoint = $stumbed * 12;
                     $boundrypoints = $fours * 1;
                     $sixpoints = $six * 2;
                     $runoutptsss = $runoutpoints * 6;

                     if (($runs >= 30) && ($runs < 50)) {
                         //$runpoints = $runpoints;
                         $halcenturypoints = 8;
                     } else if ($runs >= 50 && $runs < 150) {
                         //$runpoints =$runpoints;
                         $centuryPoints = 16;
                     }

                     $maidenpoints = $maiden_over * 16;
                     $runpoints = $runpoints + $runs * 1;

                     if ($overs >= 1) {


                         if ($economyrate > 13) {
                             $economypoints = -6;
                         } else if ($economyrate >= 12.01 && $economyrate <= 13) {
                             $economypoints = -4;
                         } else if ($economyrate >= 11 && $economyrate <= 12) {
                             $economypoints = -2;
                         } else if ($economyrate >= 7 && $economyrate <= 8) {
                             $economypoints = 2;
                         } else if ($economyrate >= 6 && $economyrate <= 6.99) {
                             $economypoints = 4;
                         } else if ($economyrate < 6) {
                             $economypoints = 6;
                         }
                     }
                     if (($bballs >= 5) && ($findplayerrole != 'bowler')) {

                         if ($strikerate >= 90 && $strikerate < 100) {

                             $strikePoint = '-2';
                         } else if ($strikerate >= 80 && $strikerate < 90) {

                             $strikePoint = '-4';
                         } else if ($strikerate < 80) {

                             $strikePoint = '-6';

                             //End

                         }

                     }
                     if ($row->notout == 1) {
                         $notoutpoints = 0;
                     }
                     if ($row->winning == 1) {
                         $winningpoints = 0;
                     }
                 } else if ($match == 'one-day') {
                     if ($row->starting11 == 1 && $row->innings == 1) {
                         $startingpoint = 4;
                     } else {
                         $startingpoint = 0;
                     }
                     if ($findplayerrole != 'bowler') {
                         if ($row->duck != 0 || $row->duck != "") {
                             $duckpoint = -3;
                         }
                     }

                     if ($wicket == 4) {

                         //$wkpoints = $wkpoints + 4;

                         $bonuspoints = $bonuspoints + 4;
                     }

                     if ($wicket >= 5) {

                         //$wkpoints = $wkpoints + 8;

                         $bonuspoints = $bonuspoints + 8;
                     }
                     $wkpoints = $wicket * 25;

                     $catchpoint = $catch * 8;

                     $stpoint = $stumbed * 12;


                     $boundrypoints = $fours * 1;
                     $sixpoints = $six * 2;
                     $runoutptsss = $runoutpoints * 6;
                     if (($runs >= 50) && ($runs < 100)) {
                         // 			$runpoints = $runpoints+ 2;
                         $halcenturypoints = 4;
                     }
                     if ($runs >= 100) {
                         // 			$runpoints =$runpoints + 4;
                         $centuryPoints = 8;
                     }

                     $runpoints = $runpoints + $runs * 1;
                     $maidenpoints = $maiden_over * 4;

                     if ($overs >= 5) {

                         //$economyrate =($balls - $grun);

                         $economyrate = $row->economy_rate;


                         if ($economyrate > 9) {

                             $economypoints = '-6';
                         } else if ($economyrate >= 8.1 && $economyrate <= 9) {

                             $economypoints = '-4';
                         } else if ($economyrate >= 7 && $economyrate <= 8) {

                             $economypoints = '-2';
                         } else if ($economyrate >= 3.5 && $economyrate <= 4.5) {

                             $economypoints = '2';
                         } else if ($economyrate >= 2.5 && $economyrate <= 3.49) {

                             $economypoints = '4';
                         } else if ($economyrate < 2.5) {

                             $economypoints = '6';
                         }

                         //End

                     }


                     if ($bballs >= 20 && $findplayerrole != 'bowler') {

                         if ($strikerate >= 50 && $strikerate <= 60) {

                             $strikePoint = '-2';
                         } else if ($strikerate >= 40 && $strikerate <= 49.9) {

                             $strikePoint = '-4';
                         } else if ($strikerate < 40) {

                             $strikePoint = '-6';

                             //End

                         }

                     }
                     if ($row->notout == 1) {
                         $notoutpoints = 0;
                     }
                     if ($row->winning == 1) {
                         $winningpoints = 0;
                     }
                 } else {      //test
                     if ($row->starting11 == 1 && $row->innings == 1) {
                         $startingpoint = 4;
                     } else {
                         $startingpoint = 0;
                     }

                     if ($findplayerrole != 'bowler') {
                         if ($row->duck != 0 || $row->duck != "") {
                             $duckpoint = -4;
                         }
                     }


                     if ($wicket == 4) {

                         $wkpoints = $wkpoints + 4;

                         $bonuspoints = $bonuspoints + 4;
                     }

                     if ($wicket >= 5) {

                         $wkpoints = $wkpoints + 8;

                         $bonuspoints = $bonuspoints + 8;
                     }

                     $wkpoints = $wicket * 16;
                     $catchpoint = $catch * 8;
                     $stpoint = $stumbed * 12;
                     $boundrypoints = $fours * 1;
                     $sixpoints = $six * 2;
                     $runoutptsss = $runoutpoints * 6;

                     if (($runs >= 50) && ($runs <= 100)) {

                         $halcenturypoints = $halcenturypoints + 4;
                     }

                     if ($runs >= 100) {

                         $halcenturypoints = $halcenturypoints + 8;


                     }


                     $runpoints = $runpoints + $runs;
                     $maidenpoints = 0;
                     if ($row->notout == 1) {
                         $notoutpoints = 0;
                     }
                     if ($row->winning == 1 && $row->innings == 1) {
                         $winningpoints = 0;
                     }
                 }
                 /*echo 'start point :'. $startingpoint; echo '<br>';
                     echo 'runs :'. $runpoints; echo '<br>';
                     echo 'Wickets :'. $wkpoints; echo '<br>';
                     echo 'Catch :'. $catchpoint; echo '<br>';
                     echo 'stumb :'. $stpoint; echo '<br>';
                     echo 'boundary :'. $boundrypoints; echo '<br>';
                     echo 'six :'. $sixpoints; echo '<br>';
                     echo 'economy :'. $economypoints; echo '<br>';
                     echo 'strike rate :'. $strikePoint; echo '<br>';
                     echo 'maiden over :'. $maidenpoints; echo '<br>';
                     echo 'duck :'. $duckpoint; echo '<br>';*/


                 if ($row->starting11 == 1) {
                     $result['batting_points'] = $point150 + $point200 + $centuryPoints + $halcenturypoints + $runpoints + $sixpoints + $boundrypoints + $strikePoint;
                     $result['fielding_points'] = $catchpoint + $stpoint + $runoutptsss;
                     $result['bowling_points'] = $wkpoints + $maidenpoints + $economypoints;
                     $result['negative_points'] = $duckpoint;
                     $result['extra_points'] = $startingpoint + $extrapoints;
                     $total_points = $result['total_points'] = $point150 + $point200 + $centuryPoints + $halcenturypoints + $startingpoint + $runpoints + $sixpoints + $boundrypoints + $strikePoint + $catchpoint + $stpoint + $wkpoints + $maidenpoints + $economypoints + $duckpoint + $notoutpoints + $winningpoints + $runoutptsss + $bonuspoints;
                 } else {
                     $result['batting_points'] = 0;
                     $result['fielding_points'] = 0;
                     $result['bowling_points'] = 0;
                     $result['negative_points'] = 0;
                     $result['extra_points'] = 0;
                     $total_points = $result['total_points'] = 0;
                 }
                 // 	echo $player_key;
                 // 	echo '<pre>'; print_r($result);
                 DB::table('result_matches')->where('player_key', $player_key)->where('match_key', $match_key)->where('innings', $row->innings)->update($result);
                 //insert in result points//
                 $resultpoints['matchkey'] = $match_key;
                 $resultpoints['playerid'] = $findplayerid->id;
                 $resultpoints['resultmatch_id'] = $row->id;
                 if ($row->starting11 == 1) {
                     $resultpoints['startingpoints'] = $startingpoint;
                     $resultpoints['runs'] = $runpoints;
                     $resultpoints['fours'] = $boundrypoints;
                     $resultpoints['sixs'] = $sixpoints;
                     $resultpoints['strike_rate'] = $strikePoint;
                     $resultpoints['halcentury'] = $halcenturypoints;
                     $resultpoints['century'] = $centuryPoints;
                     $resultpoints['point150'] = $point150;
                     $resultpoints['point200'] = $point200;
                     $resultpoints['wickets'] = $wkpoints;
                     $resultpoints['maidens'] = $maidenpoints;
                     $resultpoints['economy_rate'] = $economypoints;
                     $resultpoints['catch'] = $catchpoint;
                     $resultpoints['stumping'] = $stpoint;
                     $resultpoints['duck'] = $duckpoint;
                     $resultpoints['bonus'] = $bonuspoints;
                     $resultpoints['runouts'] = $runoutptsss;
                     $resultpoints['not_out'] = $notoutpoints;
                     $resultpoints['negative'] = $duckpoint;
                     $resultpoints['winner_point'] = $winningpoints;
                     $resultpoints['total'] = $total_points;
                 } else {
                     $resultpoints['startingpoints'] = 0;
                     $resultpoints['runs'] = 0;
                     $resultpoints['fours'] = 0;
                     $resultpoints['sixs'] = 0;
                     $resultpoints['strike_rate'] = 0;
                     $resultpoints['halcentury'] = 0;
                     $resultpoints['century'] = 0;
                     $resultpoints['point150'] = 0;
                     $resultpoints['point200'] = 0;
                     $resultpoints['duck'] = 0;
                     $resultpoints['wickets'] = 0;
                     $resultpoints['maidens'] = 0;
                     $resultpoints['economy_rate'] = 0;
                     $resultpoints['catch'] = 0;
                     $resultpoints['stumping'] = 0;
                     $resultpoints['bonus'] = 0;
                     $resultpoints['not_out'] = 0;
                     $resultpoints['runouts'] = 0;
                     $resultpoints['winner_point'] = 0;
                     $resultpoints['negative'] = 0;
                     $resultpoints['total'] = 0;
                 }

                 $finde = DB::table('result_points')->where('matchkey', $match_key)->where('resultmatch_id', $row->id)->where('playerid', $findplayerid->id)->select('id')->first();
                 if (empty($finde)) {
                     $resultpoints['resultmatch_id'] = $row->id;
                     DB::table('result_points')->insert($resultpoints);
                 } else {
                     DB::table('result_points')->where('id', $finde->id)->update($resultpoints);
                 }
             }

             $this->updateplayerpoints($match_key);
         }
     }

    }

    public function razorpayWebhook(Request $request){
        \Log::info($request->all());
        \Log::info('Razorpay payment event received '. $request->event);
        $order_id = $request->payload['payment']['entity']['order_id'];
        $status = $request->payload['payment']['entity']['status'];
        $transaction = DB::table('transactions')->where('transaction_id',$order_id)->first();
        if(isset($transaction) && !empty($transaction)){
            if($status == 'authorized'){
                $status = 'confirmed';
            } else if($status == 'failed'){
                $status = 'failed';
            }
            if(isset($status)){
                DB::table('transactions')->where('id',$transaction->id)->update(['paymentstatus' => $status]);
                if( $status == 'confirmed'){
                    $userdata = DB::table('user_balances')->where('user_id',$transaction->userid)->first();
                    $datainseert['balance'] = $userdata->balance+$transaction->amount;
                    DB::table('user_balances')->where('user_id',$transaction->userid)->update($datainseert);
                }
            }
        }
        return 1;
    }

    // public function player_point($match_key,$match){

    // 	$matchplayers = DB::table('result_matches')->where('match_key',$match_key)->get();

    // 	if(!empty($matchplayers)){
    // 		foreach($matchplayers as $row){
    // 			$resultmatchupdate = array();
    // 			$result = array();
    // 			$duck= $row->duck;
    // 			$player_key = $row->player_key;
    // 			$findplayerid = DB::table('players')->select('id')->where('id',$row->player_id)->first();
    // 			$findplayerrole = DB::table('match_players')->where('playerid',$findplayerid->id)->where('matchkey',$match_key)->select('role')->first();
    // 			$runs = $row->runs;
    // 			$wicket = $row->wicket;
    // 			$catch = $row->catch;
    // 			$stumbed = $row->stumbed;
    // 			$boundary = $row->boundary;
    // 			$six = $row->six;
    // 			$fours = $row->fours;
    // 			$maiden_over = $row->maiden_over;
    // 			$overs = $row->overs;
    // 			$balls = $row->balls;
    // 			$bballs = $row->bball;    //batting balls
    // 			$grun = $row->grun;			// given runs
    // 			$economyrate = $row->economy_rate;
    // 			$strikerate = $row->strike_rate;
    // 			$runoutpoints = $row->runouts;

    // 			$startingPoint = 0;
    // 			$duckpoint = 0;
    // 			$wkpoints = 0;   //points for wickets
    // 			$catchpoint = 0;
    // 			$stpoint = 0;   // points for stumb
    // 			$boundrypoints = 0;
    // 			$sixpoints = 0;
    // 			$runpoints = 0;
    // 			$centuryPoints = 0;
    // 			$point150 = 0;
    // 			$point200 = 0;
    // 			$halcenturypoints = 0;
    // 			$maidenpoints = 0;
    // 			$economypoints = 0;
    // 			$strikePoint = 0;

    // 			$total_points=0;
    // 			$batting_points = 0;
    // 			$bowling_points = 0;
    // 			$fielding_points = 0;
    // 			$extra_points = 0;
    // 			$negative_points = 0;
    // 			$notoutpoints = 0;
    // 			$runoutptsss = 0;
    // 			$winningpoints = 0;
    // 			if($match == 't20'){
    // 				if($row->starting11==1 && $row->innings==1){
    // 					$startingpoint = 2;
    // 				}
    // 				else{
    // 					$startingpoint = 0;
    // 				}

    // 				if($findplayerrole!='bowler'){
    // 					if($row->duck!=0 || $row->duck!=""){
    // 						$duckpoint = -2;
    // 					}
    // 				}
    // 				if($wicket >=3 && $wicket<5){
    // 					$wkpoints = $wkpoints+4;
    // 				}if($wicket >=5 && $wicket<7){
    // 					$wkpoints = $wkpoints+8;
    // 				}
    // 				if($wicket>=7){
    // 					$wkpoints = $wkpoints+10;
    // 				}

    // 				$wkpoints = $wkpoints+$wicket*10;
    // 				$catchpoint = $catch*4;
    // 				$stpoint = $stumbed*3;
    // 				$boundrypoints = $fours*0.5;
    // 				$sixpoints = $six*1;
    // 				$runoutptsss = $runoutpoints*3;

    // 				if(($runs>=50) && ($runs<100)){
    // 		// 			$runpoints = $runpoints+ 4;
    // 					$halcenturypoints = 4;
    // 				}
    // 				else if($runs>=100 && $runs<150){
    // 		// 			$runpoints =$runpoints + 8;
    // 					$centuryPoints = 8;
    // 				}
    // 				else if($runs>=150 && $runs<200){
    // 		// 			$runpoints =$runpoints + 10;
    // 					$point150 = 10;
    // 				}
    // 				else if($runs>=200 ){
    // 		// 			$runpoints =$runpoints + 15;
    // 					$point200 = 15;
    // 				}
    // 				$maidenpoints = $maiden_over*4;
    // 				$runpoints = $runpoints+$runs*0.5;

    // 				if($overs>=2){


    // 					if($economyrate >11){
    // 						$economypoints= -4;
    // 					}else if($economyrate >=10.1 && $economyrate <=11){
    //                               $economypoints= -3;
    // 					}else if($economyrate >=9.1 && $economyrate <=10){
    //                               $economypoints= -2;
    // 					}else if($economyrate >=8 && $economyrate <=9){
    //                               $economypoints= -1;
    // 					}else if($economyrate >=5 && $economyrate <=6){
    //                               $economypoints= 1;
    // 					}else if($economyrate >=4 && $economyrate <=4.99){
    // 					    $economypoints= 2;
    // 					}else if($economyrate < 4){
    //                               $economypoints= +3;
    // 					}
    // 				}
    // 				if($bballs>=10){
    // 					if($strikerate < 50){
    // 						$strikePoint = $strikePoint-3;
    // 					}else if($strikerate < 60){
    // 						$strikePoint = $strikePoint-2;
    // 					}else if($strikerate <= 70){
    // 						$strikePoint = $strikePoint-1;
    // 					}else if($strikerate < 100){
    // 						$strikePoint = $strikePoint-0;
    // 					}else if($strikerate <= 120){
    // 						$strikePoint = $strikePoint+1;
    // 					}else if($strikerate <= 150){
    // 						$strikePoint = $strikePoint+2;
    // 					}else if($strikerate <= 175){
    // 						$strikePoint = $strikePoint+3;
    // 					}else if($strikerate > 175){
    // 						$strikePoint = $strikePoint+4;
    // 					}
    // 				}
    // 				if($row->notout==1){
    // 					$notoutpoints = 0;
    // 				}
    // 				if($row->winning==1){
    // 					$winningpoints = 0;
    // 				}
    // 			}
    // 			else if($match == 'one-day'){
    // 				if($row->starting11==1 && $row->innings==1){
    // 					$startingpoint = 2;
    // 				}else{
    // 					$startingpoint = 0;
    // 				}
    // 				if($findplayerrole!='bowler'){
    // 					if($row->duck!=0 || $row->duck!=""){
    // 						$duckpoint = -3;
    // 					}
    // 				}
    // 				if($wicket>=3 && $wicket <5){
    // 					$wkpoints = $wkpoints+2;
    // 				}
    // 				if($wicket>=5 && $wicket <7){
    // 					$wkpoints = $wkpoints+4;
    // 				}
    // 				if($wicket>=7){
    // 					$wkpoints = $wkpoints+8;
    // 				}
    // 				$wkpoints = $wkpoints+$wicket*12;
    // 				$catchpoint = $catch*4;
    // 				$stpoint = $stumbed*3;
    // 				$boundrypoints = $fours*0.5;
    // 				$sixpoints = $six*1;
    // 				$runoutptsss = $runoutpoints*3;
    // 				if(($runs>=50) && ($runs<100)){
    // 		// 			$runpoints = $runpoints+ 2;
    // 					$halcenturypoints = 2;
    // 				}
    // 				if($runs>=100 && ($runs<150)){
    // 		// 			$runpoints =$runpoints + 4;
    // 					$centuryPoints =4;
    // 				}
    // 				if($runs>=150 && ($runs<200)){
    // 		// 			$runpoints =$runpoints + 8;
    // 					$point150=8;
    // 				}
    // 				if($runs>=200 ){
    // 		// 			$runpoints =$runpoints + 10;
    // 					$point200=10;
    // 				}
    // 				$runpoints = $runpoints+$runs*0.5;
    // 				$maidenpoints = $maiden_over*2;

    // 				if($overs>=4){
    // 					if($economyrate < 3.0){
    // 						$economypoints = $economypoints+3;
    // 					}else if($economyrate < 4.1 ){
    // 						$economypoints = $economypoints+2;
    // 					}else if($economyrate < 5.1 ){
    // 						$economypoints = $economypoints+1;
    // 					}else if($economyrate < 7.1 ){
    // 						$economypoints = $economypoints+0;
    // 					}else if($economyrate < 8.1 ){
    // 						$economypoints = $economypoints-1;
    // 					}else if($economyrate < 9.1 ){
    // 						$economypoints = $economypoints-2;
    // 					}else if($economyrate < 10.1 ){
    // 						$economypoints = $economypoints-3;
    // 					}else if($economyrate > 10.0 ){
    // 						$economypoints = $economypoints-4;
    // 					}


    // 				}
    // 				if($bballs>=20){
    // 					if($strikerate < 40){
    // 						$strikePoint = $strikePoint-3;
    // 					}else if($strikerate <= 50){
    // 						$strikePoint = $strikePoint-2;
    // 					}else if($strikerate <= 60){
    // 						$strikePoint = $strikePoint-1;
    // 					}else if($strikerate < 100){
    // 						$strikePoint = $strikePoint-0;
    // 					}else if($strikerate <= 125){
    // 						$strikePoint = $strikePoint+1;
    // 					}else if($strikerate <= 150){
    // 						$strikePoint = $strikePoint+2;
    // 					}else if($strikerate <= 175){
    // 						$strikePoint = $strikePoint+3;
    // 					}else if($strikerate > 175){
    // 						$strikePoint = $strikePoint+4;
    // 					}
    // 				}
    // 				if($row->notout==1){
    // 					$notoutpoints = 0;
    // 				}
    // 				if($row->winning==1){
    // 					$winningpoints = 0;
    // 				}
    // 			}
    // 			else{    //test
    // 				if($row->starting11==1 && $row->innings==1){
    // 					$startingpoint = 2;
    // 				}
    // 				else{
    // 					$startingpoint = 0;
    // 				}

    // 				if($findplayerrole!='bowler'){
    // 					if($row->duck!=0 || $row->duck!=""){
    // 						$duckpoint = -4;
    // 					}
    // 				}
    // 				if($wicket>=3 && $wicket <5){
    // 					$wkpoints = $wkpoints+2;
    // 				}
    // 				if($wicket>=5 && $wicket <7){
    // 					$wkpoints = $wkpoints+4;
    // 				}
    // 				if($wicket>=7 && $wicket <10){
    // 					$wkpoints = $wkpoints+6;
    // 				}
    // 				if($wicket>=10){
    // 					$wkpoints = $wkpoints+8;
    // 				}
    // 				$wkpoints = $wkpoints+$wicket*8;
    // 				$catchpoint = $catch*4;
    // 				$stpoint = $stumbed*3;
    // 				$boundrypoints = $fours*0.5;
    // 				$sixpoints = $six*1;
    // 				$runoutptsss = $runoutpoints*3;
    // 				if(($runs>=50) && ($runs<100)){
    // 		// 			$runpoints = $runpoints+ 2;
    // 					$halcenturypoints = 2;
    // 				}
    // 				if($runs>=100 && ($runs<150)){
    // 		// 			$runpoints =$runpoints + 4;
    // 					$centuryPoints =4;
    // 				}
    // 				if($runs>=150 && ($runs<200)){
    // 		// 			$runpoints =$runpoints + 6;
    // 					$point150 =6;
    // 				}
    // 				if($runs>=200 && ($runs<300)){
    // 		// 			$runpoints =$runpoints + 8;
    // 					$point200 =8;
    // 				}
    // 				if($runs>=300){
    // 		// 			$runpoints =$runpoints + 10;
    // 					$point200 =10;
    // 				}
    // 				$runpoints = $runpoints+$runs*0.5;
    // 				$maidenpoints = $maiden_over*0;
    // 				if($row->notout==1){
    // 					$notoutpoints = 0;
    // 				}
    // 				if($row->winning==1 && $row->innings==1){
    // 					$winningpoints = 0;
    // 				}
    // 			}
    // 			/*echo 'start point :'. $startingpoint; echo '<br>';
    // 			echo 'runs :'. $runpoints; echo '<br>';
    // 			echo 'Wickets :'. $wkpoints; echo '<br>';
    // 			echo 'Catch :'. $catchpoint; echo '<br>';
    // 			echo 'stumb :'. $stpoint; echo '<br>';
    // 			echo 'boundary :'. $boundrypoints; echo '<br>';
    // 			echo 'six :'. $sixpoints; echo '<br>';
    // 			echo 'economy :'. $economypoints; echo '<br>';
    // 			echo 'strike rate :'. $strikePoint; echo '<br>';
    // 			echo 'maiden over :'. $maidenpoints; echo '<br>';
    // 			echo 'duck :'. $duckpoint; echo '<br>';*/


    // 			if($row->starting11==1){
    // 				$result['batting_points'] = $point150+$point200+$centuryPoints+$halcenturypoints+$runpoints+$sixpoints+$boundrypoints+$strikePoint;
    // 				$result['fielding_points'] = $catchpoint+$stpoint+$runoutptsss;
    // 				$result['bowling_points'] = $wkpoints+$maidenpoints+$economypoints;
    // 				$result['negative_points'] = $duckpoint;
    // 				$result['extra_points'] = $startingpoint;
    // 				$total_points = $result['total_points'] = $point150+$point200+$centuryPoints+$halcenturypoints+$startingpoint+$runpoints+$sixpoints+$boundrypoints+$strikePoint+$catchpoint+$stpoint+$wkpoints+$maidenpoints+$economypoints+$duckpoint+$notoutpoints+$winningpoints+$runoutptsss;
    // 			}
    // 			else{
    // 				$result['batting_points'] = 0;
    // 				$result['fielding_points'] = 0;
    // 				$result['bowling_points'] = 0;
    // 				$result['negative_points'] = 0;
    // 				$result['extra_points'] = 0;
    // 				$total_points = $result['total_points'] = 0;
    // 			}
    // 		// 	echo $player_key;
    // 		// 	echo '<pre>'; print_r($result);
    // 			DB::table('result_matches')->where('player_key',$player_key)->where('match_key',$match_key)->where('innings',$row->innings)->update($result);
    // 				//insert in result points//
    // 				$resultpoints['matchkey']= $match_key;
    // 				$resultpoints['playerid']= $findplayerid->id;
    // 				if($row->starting11==1){
    // 					$resultpoints['startingpoints']= $startingpoint;
    // 					$resultpoints['runs']= $runpoints;
    // 					$resultpoints['fours']= $boundrypoints;
    // 					$resultpoints['sixs']= $sixpoints;
    // 					$resultpoints['strike_rate']= $strikePoint;
    // 					$resultpoints['halcentury']= $halcenturypoints;
    // 					$resultpoints['century']= $centuryPoints;
    // 					$resultpoints['point150']= $point150;
    // 					$resultpoints['point200']= $point200;
    // 					$resultpoints['wickets']= $wkpoints;
    // 					$resultpoints['maidens']= $maidenpoints;
    // 					$resultpoints['economy_rate']= $economypoints;
    // 					$resultpoints['catch']= $catchpoint;
    // 					$resultpoints['stumping']= $stpoint;
    // 					$resultpoints['duck']= $duckpoint;
    // 					$resultpoints['bonus']= 0;
    // 					$resultpoints['runouts']= $runoutptsss;
    // 					$resultpoints['not_out']= $notoutpoints;
    // 					$resultpoints['negative']= $duckpoint;
    // 					$resultpoints['winner_point']= $winningpoints;
    // 					$resultpoints['total']= $total_points;
    // 				}else{
    // 					$resultpoints['startingpoints']= 0;
    // 					$resultpoints['runs']= 0;
    // 					$resultpoints['fours']= 0;
    // 					$resultpoints['sixs']= 0;
    // 					$resultpoints['strike_rate']= 0;
    // 					$resultpoints['halcentury']= 0;
    // 					$resultpoints['century']= 0;
    // 					$resultpoints['point150']= 0;
    // 					$resultpoints['point200']= 0;
    // 					$resultpoints['duck']= 0;
    // 					$resultpoints['wickets']= 0;
    // 					$resultpoints['maidens']= 0;
    // 					$resultpoints['economy_rate']= 0;
    // 					$resultpoints['catch']= 0;
    // 					$resultpoints['stumping']= 0;
    // 					$resultpoints['bonus']= 0;
    // 					$resultpoints['not_out']=0;
    // 					$resultpoints['runouts']= 0;
    // 					$resultpoints['winner_point']= 0;
    // 					$resultpoints['negative']= 0;
    // 					$resultpoints['total']= 0;
    // 				}
    // 				$finde = DB::table('result_points')->where('matchkey',$match_key)->where('playerid',$findplayerid->id)->where('resultmatch_id',$row->id)->select('id')->first();
    // 			if(empty($finde)){
    // 				$resultpoints['resultmatch_id']= $row->id;
    // 				DB::table('result_points')->insert($resultpoints);
    // 			}
    // 			else{
    // 				DB::table('result_points')->where('id',$finde->id)->update($resultpoints);
    // 			}
    // 		}

    // 		$this->updateplayerpoints($match_key);
    // 	}

    // }

    public function updateplayerpoints($match_key,$sportType = 1){
        $findallplayers = DB::table('match_players')->where('matchkey',$match_key)->get();
        if(!empty($findallplayers)){
            foreach($findallplayers as $player){
                $findtotalpoints = DB::table('result_points')->where('matchkey',$match_key)->where('playerid',$player->playerid)->sum('result_points.total');
                $data['points'] = $findtotalpoints;
                DB::table('match_players')->where('id',$player->id)->update($data);
            }
            MatchesController::userpoints($match_key);
        }
    }



    public function userpoints($match_key){
        $joinlist = DB::table('join_teams')->where('matchkey',$match_key)->get();
        if(!empty($joinlist)){
            foreach($joinlist as $row2){
                $user_points = 0;
                $players = explode(',',$row2->players);
                $matchplayers = DB::table('match_players')->where('matchkey',$match_key)->get();
                if(!empty($matchplayers)){
                    foreach($matchplayers as $row){
                        $pid = $row->playerid;
                        if(in_array($pid,$players)){
                            if($row2->captain == $pid){
                                $user_points =$user_points + ($row->points*2);
                            }else if ($row2->vicecaptain== $pid){
                                $user_points =$user_points + ($row->points*1.5);
                            }else {
                                $user_points =$user_points + $row->points;
                            }
                        }else{
                            $user_points = $user_points;
                        }
                    }
                }
                if($row2->points!=$user_points){
                    $result['lastpoints']=$row2->points;
                }
                $result['points']=$user_points;
                DB::table('join_teams')->where('id',$row2->id)->update($result);
            }
            MatchesController::update_leaderboard($match_key);
        }
    }

    public function update_leaderboard($matchkey) {
        //$matchkey = $data['matchkey'] =  $_GET['matchkey'];
        $listmatchdetail = DB::table('list_matches')->where('matchkey',$matchkey)->select('final_status','status')->first();
        if($listmatchdetail->final_status=='winnerdeclared'){} else {
            $matchchallenges = DB::table('match_challenges')->where('match_challenges.status','!=','canceled')->where('match_challenges.matchkey', $matchkey)->get();
            $challarr = array();
            foreach ($matchchallenges as $key => $value) {
                $challarr[]=$value->id;
            }
            $resjoinedteams =  DB::table('joined_leagues')->whereIn('joined_leagues.challengeid',$challarr)->join('register_users','register_users.id','=','joined_leagues.userid')->join('join_teams','join_teams.id','=','joined_leagues.teamid')->orderBy('join_teams.points','DESC')->select('joined_leagues.challengeid','register_users.team','register_users.email','join_teams.teamnumber','join_teams.points','join_teams.lastpoints','joined_leagues.id as jid','joined_leagues.userid','joined_leagues.teamid')->get();
            foreach($resjoinedteams as $row){
                $joinedteamsarr[$row->challengeid][]=$row;
            }
            $i = 0;
            $Json = array();
            foreach ($matchchallenges as $key2 => $joined) {
                $i = $joined->id;
                $findjoinedteams =(isset($joinedteamsarr[$joined->id]))?$joinedteamsarr[$joined->id]:[];

                $gtlastranks = array();
                $getcurrentrankarray = array();
                $ss = 0;
                if(!empty($findjoinedteams)){
                    foreach($findjoinedteams as $pleauges){
                        $gtlastranks[$ss]['lastpoints'] = $pleauges->lastpoints;
                        $gtlastranks[$ss]['userid'] = $pleauges->userid;
                        $gtlastranks[$ss]['userjoinid'] = $pleauges->jid;
                        $getcurrentrankarray[$ss]['points'] = $pleauges->points;
                        $getcurrentrankarray[$ss]['userid'] = $pleauges->userid;
                        $getcurrentrankarray[$ss]['userjoinid'] = $pleauges->jid;
                        $ss++;
                    }
                }
                $gtlastranks = $this->multid_sort($gtlastranks, 'lastpoints');
                if(!empty($gtlastranks)){
                    $getuserlastrank=array();
                    $lr=0;$lrsno = 0;$uplus=0;
                    foreach($gtlastranks as $lrnk){
                        if(in_array($lrnk['lastpoints'], array_column($getuserlastrank, 'points'))) { // search value in the array
                            $lrsno++;
                            $lrsno = $lrsno+$uplus;
                            $uplus=0;
                        }else{
                            $lrsno++;
                        }
                        $getuserlastrank[$lr]['rank'] = $lrsno;
                        $getuserlastrank[$lr]['points'] = $lrnk['lastpoints'];
                        $getuserlastrank[$lr]['userid'] = $lrnk['userid'];
                        $getuserlastrank[$lr]['userjoinid'] = $lrnk['userjoinid'];
                        $lr++;

                    }
                }
                //get current ranks//
                $gtcurranks = $this->multid_sort($getcurrentrankarray, 'points');
                if(!empty($gtcurranks)){
                    $getusercurrank=array();
                    $cur=0;$currsno = 0;$plus=0;
                    foreach($gtcurranks as $curnk){
                        if(!in_array($curnk['points'], array_column($getusercurrank, 'points'))){ // search value in the array
                            $currsno++;
                            $currsno = $currsno+$plus;
                            $plus=0;

                        }
                        else{
                            $plus++;

                        }
                        $getusercurrank[$cur]['rank'] = $currsno;
                        $getusercurrank[$cur]['points'] = $curnk['points'];
                        $getusercurrank[$cur]['userid'] = $curnk['userid'];
                        $getusercurrank[$cur]['userjoinid'] = $curnk['userjoinid'];
                        $cur++;

                    }
                }

                /* Genrate Lederboard */
                if(!empty($findjoinedteams)){
                    $k=0;$userrank = 1;$userslistsno=-1; $userrankarray = array();$pdfname="";
                    foreach($findjoinedteams as $jointeam){
                        if($jointeam->team!=""){
                            $Json[$i]['jointeams'][$k]['team_name'] = ucwords($jointeam->team);
                        }
                        else{
                            $Json[$i]['jointeams'][$k]['team_name'] = $jointeam->email;
                        }
                        $Json[$i]['jointeams'][$k]['team_id'] = $jointeam->teamid;
                        $Json[$i]['jointeams'][$k]['team_number'] = $jointeam->teamnumber;

                        $Json[$i]['jointeams'][$k]['points'] = $jointeam->points;
                        $getuserindexinglast = $this->searchByValue($getuserlastrank,'userjoinid',$jointeam->jid);
                        $getlastrank = $getuserlastrank[$getuserindexinglast]['rank'];
                        $getuserindexingcurent = $this->searchByValue($getusercurrank,'userjoinid',$jointeam->jid);
                        $getcurrentrank = $getusercurrank[$getuserindexingcurent]['rank'];
                        $Json[$i]['jointeams'][$k]['rank'] = $getcurrentrank;
                        if($getlastrank<$getcurrentrank){
                            $Json[$i]['jointeams'][$k]['arrowname'] = 'down-arrow';
                        }
                        else if($getlastrank==$getcurrentrank){
                            $Json[$i]['jointeams'][$k]['arrowname'] = 'equal-arrow';
                        }
                        else if($getlastrank>$getcurrentrank){
                            $Json[$i]['jointeams'][$k]['arrowname'] = 'up-arrow';
                        }
                        $Json[$i]['jointeams'][$k]['join_id'] = $jointeam->jid;
                        $Json[$i]['jointeams'][$k]['user_id'] = $jointeam->userid;
                        $Json[$i]['jointeams'][$k]['challenge_id'] = $i;


                        $k++;
                    }
                    //array_multisort(array_column($Json[$i]['jointeams'],'userno'),SORT_ASC,array_column($Json[$i]['jointeams'],'points'),SORT_DESC,$Json[$i]['jointeams']);
                } //print_r($Json); exit;
                /* Genrate Lederboard */
                if(!empty($Json[$i]['jointeams'])) {
                    //$leaderboard_exist = DB::table('leaderboards')->where('challenge_id',$i)->get();
                    //if(empty($leaderboard_exist)) {
                    $this->insertOrUpdate($Json[$i]['jointeams'],'leaderboards');
                    // DB::table('leaderboards')->insert($Json[$i]['jointeams']);
                    //} else {
                    // foreach ($Json[$i]['jointeams'] as $teams) { //print_r($teams);
                    // 	DB::table('leaderboards')->where('join_id', $teams['join_id'])->update($teams);
                    // }
                    //}
                }
            }
            //print_r($Json); exit;
            return;
        }
    }

    public function insertOrUpdate(array $rows, $table){
        // $table = \DB::getTablePrefix().with(new self)->getTable();
        // $table = 'finalresults';


        $first = reset($rows);

        $columns = implode( ',',
            array_map( function( $value ) { return "`$value`"; } , array_keys($first) )
        );

        $values = implode( ',', array_map( function( $row ) {
                return '('.implode( ',',
                        array_map( function( $value ) { return '"'.str_replace('"', '""', $value).'"'; } , $row )
                    ).')';
            } , $rows )
        );

        $updates = implode( ',',
            array_map( function( $value ) { return "`$value` = VALUES(`$value`)"; } , array_keys($first) )
        );
        // print_r($updates);die;
        $sql = "INSERT INTO `{$table}` ({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";
        return \DB::statement( $sql );
    }


    public function multid_sort($arr, $index) {
        $b = array();
        $c = array();
        foreach ($arr as $key => $value) {
            $b[$key] = $value[$index];
        }
        arsort($b);
        foreach ($b as $key => $value) {
            $c[] = $arr[$key];
        }
        return $c;
    }

    public function searchByValue($products, $field, $value){
        foreach($products as $key => $product)
        {
            if ( $product[$field] === $value )
                return $key;
        }
        return false;
    }

    public function refund_amount($match_key,$sportType = 1){ //echo $match_key; exit;
        $joinedleauges = DB::table('joined_leagues')->where('matchkey',$match_key)->select('challengeid','userid','id')->groupBy('challengeid')->get();
        $listmatches = DB::table('list_matches')->where('matchkey',$match_key)->first(); //print_r($listmatches);
        foreach($joinedleauges as $val1){
            $matchchallenges = DB::table('match_challenges')->where('id',$val1->challengeid)
                ->where('status','!=','canceled')
                ->get();
            foreach($matchchallenges as $val2){
                // print_r($val2); exit;
                if(($val2->maximum_user > $val2->joinedusers && $val2->challenge_type != 'money' ) || (($val2->challenge_type == "percentage" || $val2->challenge_type == "%") &&  $val2->joinedusers < $val2->minimum_user)){
                    if($val2->confirmed_challenge==0 || (($val2->challenge_type == "percentage" || $val2->challenge_type == "%") &&  $val2->joinedusers < $val2->minimum_user)){
                        $leaugestransactions = DB::table('leagues_transactions')->where('matchkey',$match_key)->where('challengeid',$val1->challengeid)->get();
                        foreach($leaugestransactions as $val3){
                            $entryfee=$val2->entryfee;
                            // $findlastow = DB::connection('mysql-write')->table('userbalances')->where('user_id',$val3->user_id)->first();
                            if(1){


                                $dasts['status']='canceled';
                                DB::table('match_challenges')->where('id',$val2->id)->update($dasts);
                                // entry in refund //
                                $refunddata['userid']=$val3->user_id;
                                $refunddata['amount']=$entryfee;
                                $refunddata['joinid']=$val3->joinid;
                                $refunddata['challengeid']=$val3->challengeid ;
                                $refunddata['reason']='Challenge cancel';
                                $refunddata['matchkey']=$match_key;
                                //$findifalreadyrefund = DB::table('refunds')->where('joinid',$val3->joinid)->first();
                                try {
                                    DB::table('refunds')->insert($refunddata); // end entry in refund data//

                                    //Update User Balance
                                    /*$dataq['balance'] =  $findlastow->balance+$val3->balance;
									$dataq['winning'] =  $findlastow->winning+$val3->winning;
									$dataq['bonus'] =  $findlastow->bonus+$val3->bonus;
									DB::connection('mysql-write')->table('userbalances')->where('id',$findlastow->id)->update($dataq);*/



                                    //transactions//
                                    /*$registeruserdetails = DB::table('registerusers')->where('id',$val3->user_id)->first();
									$datatr['transaction_id'] = 'FP11-REFUND-'.rand(100,999).$val3->user_id;
									$datatr['type'] = 'Refund amount';
									$datatr['transaction_by'] = 'FP11';
									$datatr['amount'] = $entryfee;
									$datatr['paymentstatus'] = 'confirmed';
									$datatr['challengeid'] = $val3->challengeid;
									$datatr['bonus_amt'] = $val3->bonus;
									$datatr['win_amt'] = $val3->winning;
									$datatr['addfund_amt'] = $val3->balance;
									$datatr['bal_bonus_amt'] = $dataq['bonus'];
									$datatr['bal_win_amt'] = $dataq['winning'];
									$datatr['bal_fund_amt'] = $dataq['balance'];
									$datatr['userid'] = $val3->user_id;
									$datatr['total_available_amt'] = $dataq['balance']+$dataq['winning']+$dataq['bonus'];
									DB::connection('mysql-write')->table('transactions')->insert($datatr);*/
                                    //mail//
                                    $challengename = "";
                                    if($val2->name!=""){
                                        $challengename = $val2->name;
                                    }else{
                                        $challengename = 'Win-'.$val2->win_amount;
                                    }
                                    // $email = $registeruserdetails->email;
                                    // $emailsubject = 'Challenge Cancelled-Refund initiated!';
                                    // $content='<p><strong>Dear Challenger </strong></p>';
                                    // $content.='<p>Unfortunately the challenge '.$challengename.' of match '.date('M d Y h:i:a',strtotime($listmatches->start_date)).' '.$listmatches->title.' has cancelled due to lack of users and your entry fee for the match is getting refunded back to your FP11 wallet.</p>';
                                    // $msg = Helpers::mailheader();
                                    // $msg.= Helpers::mailbody($content);
                                    // $msg.= Helpers::mailfooter();
                                    // Helpers::mailsentFormat($email,$emailsubject,$msg);
                                    //notifications//
                                    $datan['title'] = 'Refund Amount of Rs.'.$val2->entryfee.' for challenge cancellation';
                                    $datan['userid'] = $val3->user_id;
                                    DB::table('notifications')->insert($datan);

                                } catch(\Illuminate\Database\QueryException $ex) {
                                    echo $ex->getMessage(); exit;
                                }

                            }
                        }
                    }
                }
            }
        }
        return 1;
    }
    public function sortBySubArrayValue(&$array, $key, $dir='asc') {

        $sorter=array();
        $rebuilt=array();

        //make sure we start at the beginning of $array
        reset($array);

        //loop through the $array and store the $key's value
        foreach($array as $ii => $value) {
            $sorter[$ii]=$value[$key];
        }

        //sort the built array of key values
        if ($dir == 'asc') asort($sorter);
        if ($dir == 'desc') arsort($sorter);

        //build the returning array and add the other values associated with the key
        foreach($sorter as $ii => $value) {
            $rebuilt[$ii]=$array[$ii];
        }

        //assign the rebuilt array to $array
        $array=$rebuilt;
    }


    public function distribute_winning_amount1($matchkey){
        $listmatches = DB::table('list_matches')->where('matchkey',$matchkey)->where('status','completed')->get();
        if(!empty($listmatches)){
            $matchchallenges = DB::table('match_challenges')->where('matchkey',$matchkey)->get();
            if(!empty($matchchallenges)){
                foreach($matchchallenges as $val2){
                    $flag=0;
                    if($val2->confirmed_challenge==1){
                        $flag=1;
                    }else{
                        if($val2->maximum_user == $val2->joinedusers){
                            $flag=1;
                        }
                    }
                    $challengeid=$val2->id;
                    $user_points=array();
                    $joinedleauges = DB::table('joined_leagues')->where('matchkey',$matchkey)->where('challengeid',$val2->id)->select('userid','teamid')->get();
                    $lp=0;
                    if(!empty($joinedleauges)){
                        foreach($joinedleauges as $jntm){
                            $newpnts = DB::table('join_teams')->where('matchkey',$matchkey)->where('id',$jntm->teamid)->where('userid',$jntm->userid)->select('points','userid')->first();
                            $user_points[$lp]['id']=$newpnts->userid;
                            $user_points[$lp]['points']=$newpnts->points;
                            $lp++;
                        }
                    }

                    // print_r($user_points);
                    // $this->sortBySubArrayValue($user_points, 'points');


                    // arsort($user_points);
                    // print_r($user_points);die;

                    if(!empty($user_points)){
                        if($flag=1){
                            $matchpricecards = DB::table('match_price_cards')->where('matchkey',$matchkey)->where('challenge_id',$val2->id)->get();
                            $prc_arr=array();
                            if(!empty($matchpricecards)){
                                foreach($matchpricecards as $prccrd){
                                    $min_position=$prccrd->min_position;
                                    $max_position=$prccrd->max_position;
                                    for($i=$min_position;$i<$max_position;$i++){
                                        $prc_arr[$i+1]=$prccrd->price;
                                    }
                                }
                            }else{
                                $prc_arr[1]=$matchchallenges->win_amount;
                            }
                            $count=count($prc_arr);

                            // $ppt=0;
                            // foreach($user_points as $usr){
                            // $pntsrt[$ppt]=$usr['points'];
                            // $ppt++;
                            // }
                            // arsort($pntsrt);
                            // echo '<pre>';print_r($pntsrt);die;

                            $poin_user=array();
                            foreach($user_points as $usr){
                                if (array_key_exists("'".$usr['points']."'",$poin_user)){
                                    $ids_str=implode(',',$poin_user["'".$usr['points']]['id']."'");
                                    $ids_str=$ids_str.','.$usr['id'];
                                    $ids_arr=explode(',',$ids_str);
                                    $poin_user["'".$usr['points']."'"]['id']=$ids_arr;
                                    $poin_user["'".$usr['points']."'"]['points']=$usr['points'];
                                }else{
                                    $poin_user["'".$usr['points']."'"]['id'][0]=$usr['id'];
                                    $poin_user["'".$usr['points']."'"]['points']=$usr['points'];
                                }
                            }
                            // echo '<pre>';print_r($poin_user);
                            $this->sortBySubArrayValue($poin_user, 'points', 'desc');
                            // krsort($poin_user);

                            $win_usr=array();
                            $win_cnt=0;
                            foreach($poin_user as $kk=>$pu){
                                if($win_cnt < $count){
                                    $win_usr[$kk]['min']=$win_cnt+1;
                                    $win_cnt=$win_cnt+count($pu['id']);
                                    $win_usr[$kk]['max']=$win_cnt;
                                    $win_usr[$kk]['count']=count($pu['id']);
                                    $win_usr[$kk]['id']=$pu['id'];
                                }else{
                                    break;
                                }
                            }

                            $final_poin_user=array();
                            foreach($win_usr as $ks=>$ps){
                                if($ps['count']==1){
                                    $final_poin_user[$ps['id'][0]]['points']=$ks;
                                    $final_poin_user[$ps['id'][0]]['amount']=$prc_arr[$ps['min']];
                                }else{
                                    $ttl=0;$avg_ttl=0;
                                    for($jj=$ps['min'];$jj<=$ps['max'];$jj++){
                                        $sm=0;
                                        if(isset($prc_arr[$jj])){
                                            $sm=$prc_arr[$jj];
                                        }
                                        $ttl=$ttl+$sm;
                                    }
                                    $avg_ttl=$ttl/$ps['count'];
                                    foreach($ps['id'] as $fnl){
                                        $final_poin_user[$fnl]['points']=$ks;
                                        $final_poin_user[$fnl]['amount']=$avg_ttl;
                                    }
                                }
                            }
                            foreach($final_poin_user as $fpusk=>$fpusv){
                                $fres['userid'] = $fpusk;
                                $fres['points'] = $fpusv['points'];
                                $fres['amount'] = $fpusv['amount'];
                                $fres['matchkey'] = $matchkey;
                                $fres['challengeid'] = $challengeid;
                                DB::table('final_results')->insert($fres);

                                $findlastow = DB::table('user_balances')->where('user_id',$fpusk)->first();
                                $dataqs['winning'] =  $findlastow->winning+$fpusv['amount'];
                                DB::table('user_balances')->where('id',$findlastow->id)->update($dataqs);
                            }

                            // print_r($fres);die;
                        }
                    }
                }
            }
        }
    }
    public  function marathon_winner_declared($series){

        $findmarathon = DB::table('marathon')->where('series',$series)->first();
        $findseriesdeatils = DB::table('series')->where('id',$series)->first();
        if(!empty($findmarathon)){
            if($findmarathon->status=='pending'){
                $findbestmatches = $findmarathon->bestmatches;
                $findallchallenges = DB::table('match_challenges')->where('challenge_id',$findmarathon->id)->where('series_id',$series)->get();
                $challengarary = array();
                if(!empty($findallchallenges)){
                    foreach($findallchallenges as $ch){
                        $challengarary[] = $ch->id;
                    }
                }
                $usersarray = array();
                $findjoinedleauges = DB::table('joined_leagues')->whereIn('challengeid',$challengarary)->groupBy('joined_leagues.userid')->get();
                if(!empty($findjoinedleauges)){
                    foreach($findjoinedleauges as $jleaug){
                        $usersarray[] = $jleaug->userid;
                    }
                }
                $ulastrank = 0;$i=0;
                $user_points = array();
                if(!empty($usersarray)){
                    foreach($usersarray as $uaray){
                        $bestscores = 0;
                        $finduserpoints = DB::table('joined_leagues')->whereIn('joined_leagues.challengeid',$challengarary)->where('joined_leagues.userid',$uaray)->join('join_teams','join_teams.id','=','joined_leagues.teamid')->orderBy('join_teams.points','DESC')->select('join_teams.points')->limit($findbestmatches)->get();
                        if(!empty($finduserpoints)){
                            foreach($finduserpoints as $point){
                                $bestscores+=$point->points;
                            }
                        }
                        $user_points[$i]['points'] = $bestscores;
                        $user_points[$i]['id'] = $uaray;
                        $i++;
                    }
                    $this->sortBySubArrayValue($user_points,'points','desc');
                    if(!empty($user_points)){
                        $matchpricecards = DB::table('marathonpricecards')->where('challenge_id',$findmarathon->id)->get();
                        $prc_arr=array();
                        if(!empty($matchpricecards)){
                            foreach($matchpricecards as $prccrd){
                                $min_position=$prccrd->min_position;
                                $max_position=$prccrd->max_position;
                                for($i=$min_position;$i<$max_position;$i++){
                                    $prc_arr[$i+1]=$prccrd->price;
                                }
                            }
                        }
                        else{
                            $prc_arr[1]=$findmarathon->win_amount;
                        }
                        $count=count($prc_arr);
                        $poin_user=array();
                        foreach($user_points as $usr){
                            $ids_str="";
                            $ids_arr = array();
                            if(array_key_exists("'".$usr['points']."'",$poin_user)){
                                if(isset($poin_user["'".$usr['points']."'"]['id'])){
                                    $ids_str=implode(',',$poin_user["'".$usr['points']."'"]['id']);
                                }
                                $ids_str=$ids_str.','.$usr['id'];
                                $ids_arr=explode(',',$ids_str);
                                $poin_user["'".$usr['points']."'"]['id']=$ids_arr;
                                $poin_user["'".$usr['points']."'"]['points']=$usr['points'];
                            }
                            else{
                                $poin_user["'".$usr['points']."'"]['id'][0]=$usr['id'];
                                $poin_user["'".$usr['points']."'"]['points']=$usr['points'];
                            }

                        }
                        $this->sortBySubArrayValue($poin_user, 'points', 'desc');
                        $win_usr=array();
                        $win_cnt=0;
                        foreach($poin_user as $kk=>$pu){
                            if($win_cnt < $count){
                                $win_usr[$kk]['min']=$win_cnt+1;
                                $win_cnt=$win_cnt+count($pu['id']);
                                $win_usr[$kk]['max']=$win_cnt;
                                $win_usr[$kk]['count']=count($pu['id']);
                                $win_usr[$kk]['id']=$pu['id'];
                            }else{
                                break;
                            }
                        }
                        $final_poin_user=array();
                        foreach($win_usr as $ks=>$ps){
                            if($ps['count']==1){
                                $final_poin_user[$ps['id'][0]]['points']=$ks;
                                $final_poin_user[$ps['id'][0]]['amount']=$prc_arr[$ps['min']];
                                $final_poin_user[$ps['id'][0]]['rank']=$ps['min'];
                            }else{
                                $ttl=0;$avg_ttl=0;
                                for($jj=$ps['min'];$jj<=$ps['max'];$jj++){
                                    $sm=0;
                                    if(isset($prc_arr[$jj])){
                                        $sm=$prc_arr[$jj];
                                    }
                                    $ttl=$ttl+$sm;
                                }
                                $avg_ttl=$ttl/$ps['count'];
                                foreach($ps['id'] as $fnl){
                                    $final_poin_user[$fnl]['points']=$ks;
                                    $final_poin_user[$fnl]['amount']=$avg_ttl;
                                    $final_poin_user[$fnl]['rank']=$ps['min'];
                                }
                            }
                        }

                        foreach($final_poin_user as $fpusk=>$fpusv){
                            $transactionidsave = 'S2W-MARATHON-'.rand(1000,99999);
                            $fres['userid'] = $fpusk;
                            $fres['points'] = str_replace("'", "", $fpusv['points']);
                            $fres['amount'] = round($fpusv['amount'],2);
                            $fres['rank'] = $fpusv['rank'];
                            $fres['seriesid'] = $series;
                            $fres['challengeid'] = $findmarathon->id;
                            $fres['transaction_id'] = $transactionidsave;
                            DB::table('final_results')->insert($fres);
                            $fpusv['amount'] = round($fpusv['amount'],2);
                            if($fpusv['amount']>=10000){
                                $tdsdata['tds_amount'] = (30/100)*$fpusv['amount'];
                                $tdsdata['amount'] = $fpusv['amount'];
                                $remainingamount = $fpusv['amount']-$tdsdata['tds_amount'];
                                $tdsdata['userid'] = $fpusk;
                                $tdsdata['seriesid'] = $series;
                                $tdsdata['challengeid'] = $findmarathon->id;
                                DB::table('tdsdetails')->insert($tdsdata);
                                $fpusv['amount'] = $remainingamount;
                                //user balance//
                                $registeruserdetails = DB::table('register_users')->where('id',$fpusk)->first();
                                $findlastow = DB::table('user_balances')->where('user_id',$fpusk)->first();
                                $dataqs['winning'] =  $findlastow->winning+$fpusv['amount'];
                                DB::table('user_balances')->where('id',$findlastow->id)->update($dataqs);
                                //transactions entry//
                                $datatr['transaction_id'] = $transactionidsave;
                                $datatr['type'] = 'Win amount';
                                $datatr['transaction_by'] = 'BBF';
                                $datatr['amount'] = $fpusv['amount'];
                                $datatr['paymentstatus'] = 'confirmed';
                                $datatr['challengeid'] = $findmarathon->id;
                                $datatr['seriesid'] = $series;
                                $datatr['win_amt'] = $fpusv['amount'];
                                $datatr['bal_bonus_amt'] = $findlastow->bonus;
                                $datatr['bal_win_amt'] = $dataqs['winning'];
                                $datatr['bal_fund_amt'] = $findlastow->balance;
                                $datatr['userid'] = $fpusk;
                                $datatr['total_available_amt'] = $findlastow->balance+$dataqs['winning']+$findlastow->bonus;
                                DB::table('transactions')->insert($datatr);
                                //notifications entry//
                                $datanot['title'] = 'You won amount Rs.'.$fpusv['amount'].' and 30% amount of '.$tdsdata['amount'].' deducted due to TDS.';
                                $datanot['userid'] = $fpusk;
                                DB::table('notifications')->insert($datanot);
                                //mail//
                                $challengename = "";
                                if($findmarathon->name==""){
                                    if($findmarathon->win_amount==0){
                                        $challengename = 'Net Practice';
                                    }else{
                                        $challengename = 'Win-'.$findmarathon->win_amount;
                                    }
                                }else{
                                    $challengename = $findmarathon->name;
                                }
                                $email = $registeruserdetails->email;
                                $emailsubject = 'Congrats you won a marathon challenge!';
                                $content='<p><strong>Dear Challenger </strong></p>';
                                $content.='<p>Congratulations. You have won a marathon challenge of '.$challengename.' for series '.$findseriesdeatils->name.'  with points '.$fres['points'].'. An amount of Rs. '.$fpusv['amount'].' is transfered to your Fanadda wallet and 30% amount is deducted due to Tax deduction as per government regulations. Enjoy!</p>';
                                $msg = Helpers::mailheader();
                                $msg.= Helpers::mailbody($content);
                                $msg.= Helpers::mailfooter();
                                // Helpers::mailsentFormat($email,$emailsubject,$msg);
                            }else{
                                //user balance//
                                $registeruserdetails = DB::table('register_users')->where('id',$fpusk)->first();
                                $findlastow = DB::table('user_balances')->where('user_id',$fpusk)->first();
                                $dataqs['winning'] =  $findlastow->winning+$fpusv['amount'];
                                DB::table('user_balances')->where('id',$findlastow->id)->update($dataqs);
                                if($fpusv['amount']>0){
                                    //transactions entry//
                                    $datatr['transaction_id'] = $transactionidsave;;
                                    $datatr['type'] = 'Win amount';
                                    $datatr['transaction_by'] = 'BBF';
                                    $datatr['amount'] = $fpusv['amount'];
                                    $datatr['paymentstatus'] = 'confirmed';
                                    $datatr['challengeid'] = $findmarathon->id;
                                    $datatr['seriesid'] = $series;
                                    $datatr['win_amt'] = $fpusv['amount'];
                                    $datatr['bal_bonus_amt'] = $findlastow->bonus;
                                    $datatr['bal_win_amt'] = $dataqs['winning'];
                                    $datatr['bal_fund_amt'] = $findlastow->balance;
                                    $datatr['userid'] = $fpusk;
                                    $datatr['total_available_amt'] = $findlastow->balance+$dataqs['winning']+$findlastow->bonus;
                                    DB::table('transactions')->insert($datatr);
                                    //notifications entry//
                                    $datanot['title'] = 'You won amount Rs.'.$fpusv['amount'];
                                    $datanot['userid'] = $fpusk;
                                    DB::table('notifications')->insert($datanot);
                                    //mail//
                                    $challengename = "";
                                    if($findmarathon->name==""){
                                        if($findmarathon->win_amount==0){
                                            $challengename = 'Net Practice';
                                        }else{
                                            $challengename = 'Win-'.$findmarathon->win_amount;
                                        }
                                    }else{
                                        $challengename = $findmarathon->name;
                                    }
                                    $email = $registeruserdetails->email;
                                    $emailsubject = 'Congrats you won a marathon challenge!';
                                    $content='<p><strong>Dear Challenger </strong></p>';
                                    $content.='<p>Congratulations. You have won a challenge of '.$challengename.' for series '.$findseriesdeatils->name.'  with points '.$fres['points'].'. An amount of Rs. '.$fpusv['amount'].' is transfered to your Fanadda wallet. Enjoy!</p>';
                                    $content.='<p> <strong> Note:- </strong> Winning amount will be credited in your Fanadda wallet after tax deductions as per government regulations.</p>';
                                    $content.='<p> For details , please check account balance.</p>';
                                    $msg = Helpers::mailheader();
                                    $msg.= Helpers::mailbody($content);
                                    $msg.= Helpers::mailfooter();
                                    // Helpers::mailsentFormat($email,$emailsubject,$msg);
                                }
                            }
                        }
                    }
                }
                $marathn['status'] = 'winnerdeclared';
                DB::table('marathon')->where('id',$findmarathon->id)->update($marathn);
                Session::flash('message', 'winner declared successfully!');
                Session::flash('alert-class', 'alert-success');
                return redirect()->action('MatchesController@marathon_result');
            }
            else{
                Session::flash('message', 'Already winner declared. You cannot redeclare the winner again!');
                Session::flash('alert-class', 'alert-danger');
                return redirect()->action('MatchesController@marathon_result');
            }
        }
    }

    public function refer_bonus_list()
    { //exit;
        $user_id = $_GET['userid'];

        // $data = DB::table('refer_bonus')->where('refered_by',$user_id)->get();
        $json = DB::table('register_users')->where('refer_id',$user_id)->select('id','email','username')->get();
        // 		echo '<pre>'; print_r($data); die;

        //$json= array();
        $i=0;
        foreach($json as $user){
            $userdata = DB::table('transactions')->where('refer_id',$user->id)->sum('amount');
            $json[$i]->name = $user->username;
            // $json[$i]['email'] = $userdata->email;
            $json[$i]->Amount = @$userdata ? @$userdata : 0;
            // $json[$i]['userid'] = $userdata->id;
            $i++;
        }
        echo json_encode($json);
        die;
    }

    public function distribute_winning_amount($matchkey,$match_type){
        // $firstupdate = MatchesController::player_point($matchkey,$match_type);
        $listmatches = DB::table('list_matches')->where('matchkey',$matchkey)->where('final_status','IsClosed')->orWhere('status','completed')->first();
        if(!empty($listmatches)){
            $matchchallenges = DB::table('match_challenges')->where('matchkey',$matchkey)->where('marathon','!=',1)->where('status','!=','canceled')->get();
            if(!empty($matchchallenges)){
                foreach($matchchallenges as $val2){
                    $ratio = 1;
                    $flag=0;
                    if($val2->confirmed_challenge==1){
                        $flag=1;
                        if($val2->minimum_user > $val2->joinedusers && ($val2->challenge_type == "percentage" || $val2->challenge_type == "%")){
                            $flag=0;
                        }
                    }
                    else{
                        if($val2->maximum_user == $val2->joinedusers){
                            $flag=1;
                        }
                        if($val2->minimum_user > $val2->joinedusers && ($val2->challenge_type == "percentage" || $val2->challenge_type == "%")){
                            $flag=0;
                        }
                    }
                    $challengeid=$val2->id;
                    $user_points=array();
                    if($flag==1){

                        //Bonus To users on refer
                        // MatchesController::refer_bonus($val2->id, $val2->entryfee);

                        $joinedleauges = DB::table('joined_leagues')->where('joined_leagues.matchkey',$matchkey)->where('joined_leagues.challengeid',$val2->id)->select('joined_leagues.userid','joined_leagues.teamid','points','joined_leagues.id as jid')->join('join_teams','join_teams.id','=','joined_leagues.teamid')->get();
                        if(!empty($joinedleauges)){
                            $total_joined_league = count($joinedleauges);
                            if($val2->maximum_user > $total_joined_league) {
                                $ratio =  $total_joined_league/ $val2->maximum_user;
                            } else {
                                $ratio = 1;
                            }
                            $lp=0;
                            foreach($joinedleauges as $jntm){
                                $user_points[$lp]['id']=$jntm->userid;
                                $user_points[$lp]['points']=$jntm->points;
                                $user_points[$lp]['joinedid']=$jntm->jid;
                                $lp++;
                            }
                        }


                        if(!empty($user_points)){
                            $prc_arr=array();
							if($val2->challenge_type == "percentage" || $val2->challenge_type == "%"){
                                $gtjnusers = $val2->joinedusers;
                                $userstowin = $val2->winning_percentage;
                                $this->sortBySubArrayValue($user_points, 'points', 'desc');
                                $toWin = round($gtjnusers*$userstowin/100); 
                                for($i = 0; $i< $toWin; $i++){
                                    $prc_arr[$i+1]=$val2->win_amount;
                                }
                                $ratio = 1;
                            }
                            else{
                                $matchpricecards = DB::table('match_price_cards')->where('matchkey',$matchkey)->where('challenge_id',$val2->id)->get();

                                if(!empty($matchpricecards)){
                                    foreach($matchpricecards as $prccrd){
                                        $min_position=$prccrd->min_position;
                                        $max_position=$prccrd->max_position;
                                        for($i=$min_position;$i<$max_position;$i++){
                                            $prc_arr[$i+1]=$prccrd->price;
                                        }
                                    }
                                }
                                else{
                                    $prc_arr[1]=$val2->win_amount;
                                }
                            }


                            $count=count($prc_arr);
                            $poin_user=array();
                            foreach($user_points as $usr){
                                $ids_str="";
                                $ids_arr = array();
                                if(array_key_exists("'".$usr['points']."'",$poin_user)){
                                    if(isset($poin_user["'".$usr['points']."'"]['joinedid'])){
                                        $ids_str=implode(',',$poin_user["'".$usr['points']."'"]['joinedid']);
                                    }
                                    $ids_str=$ids_str.','.$usr['joinedid'];
                                    $ids_arr=explode(',',$ids_str);
                                    $poin_user["'".$usr['points']."'"]['joinedid']=$ids_arr;
                                    $poin_user["'".$usr['points']."'"]['points']=$usr['points'];
                                    $poin_user["'".$usr['points']."'"]['id']=$usr['id'];
                                }
                                else{
                                    $poin_user["'".$usr['points']."'"]['id']=$usr['id'];
                                    $poin_user["'".$usr['points']."'"]['points']=$usr['points'];
                                    $poin_user["'".$usr['points']."'"]['joinedid'][0]=$usr['joinedid'];
                                }

                            }
                            $this->sortBySubArrayValue($poin_user, 'points', 'desc');

                            $win_usr=array();
                            $win_cnt=0;
                            foreach($poin_user as $kk=>$pu){
                                if($win_cnt < $count){
                                    $win_usr[$kk]['min']=$win_cnt+1;
                                    $win_cnt=$win_cnt+count($pu['joinedid']);
                                    $win_usr[$kk]['max']=$win_cnt;
                                    $win_usr[$kk]['count']=count($pu['joinedid']);
                                    $win_usr[$kk]['joinedid']=$pu['joinedid'];
                                }else{
                                    break;
                                }
                            }

                            $final_poin_user=array();
                            foreach($win_usr as $ks=>$ps){
                                if($ps['count']==1){
                                    $final_poin_user[$ps['joinedid'][0]]['points']=$ks;
                                    $final_poin_user[$ps['joinedid'][0]]['amount']=$prc_arr[$ps['min']]; //multiply ratio here
                                    $final_poin_user[$ps['joinedid'][0]]['rank']=$ps['min'];
                                }
                                else{
                                    $ttl=0;$avg_ttl=0;
                                    for($jj=$ps['min'];$jj<=$ps['max'];$jj++){
                                        $sm=0;
                                        if(isset($prc_arr[$jj])){
                                            $sm=$prc_arr[$jj];
                                        }
                                        $ttl=$ttl+$sm;
                                    }
                                    $avg_ttl=$ttl/$ps['count'];
                                    foreach($ps['joinedid'] as $fnl){
                                        $final_poin_user[$fnl]['points']=$ks;
                                        $final_poin_user[$fnl]['amount']=$avg_ttl; //multiply ratio here
                                        $final_poin_user[$fnl]['rank']=$ps['min'];
                                    }
                                }
                            }

                            foreach($final_poin_user as $fpuskjoinid=>$fpusv){
                                $finduserid = DB::table('joined_leagues')->where('id',$fpuskjoinid)->select('userid')->first();
                                $fpusk = $finduserid->userid; 
                                $transactionidsave = 'WIN-'.rand(1000,99999).$challengeid;
                                $fres['userid'] = $fpusk;
                                $fres['points'] = str_replace("'", "", $fpusv['points']);
                                $fres['amount'] = round($fpusv['amount'],2);
                                $fres['rank'] = $fpusv['rank'];
                                $fres['matchkey'] = $matchkey;
                                $fres['challengeid'] = $challengeid;
                                $fres['transaction_id'] = $transactionidsave;
                                $fres['joinedid'] = $fpuskjoinid;
                                $finalresults[] = $fres;
                                //$findalreexist = DB::table('finalresults')->where('matchkey',$matchkey)->where('joinedid',$fpuskjoinid)->first();
                                try {/*
											DB::table('finalresults')->insert($fres);


										$fpusv['amount'] = round($fpusv['amount'],2);
										if($fpusv['amount']>=10000){
											$tdsdata['tds_amount'] = (30/100)*$fpusv['amount'];
											$tdsdata['amount'] = $fpusv['amount'];
											$remainingamount = $fpusv['amount']-$tdsdata['tds_amount'];
											$tdsdata['userid'] = $fpusk;
											$tdsdata['challengeid'] = $val2->id;
											DB::table('tdsdetails')->insert($tdsdata);
											$fpusv['amount'] = $remainingamount;
											//user balance//
											$registeruserdetails = DB::table('registerusers')->where('id',$fpusk)->first();
											$findlastow = DB::table('userbalances')->where('user_id',$fpusk)->first();
											$dataqs['winning'] =  $findlastow->winning+$fpusv['amount'];
											DB::table('userbalances')->where('id',$findlastow->id)->update($dataqs);
											//transactions entry//
											$datatr['transaction_id'] = $transactionidsave;
											$datatr['type'] = 'Challenge Winning Amount';
											$datatr['transaction_by'] = 'FP11';
											$datatr['amount'] = $fpusv['amount'];
											$datatr['paymentstatus'] = 'confirmed';
											$datatr['challengeid'] = $val2->id;
											$datatr['win_amt'] = $fpusv['amount'];
											$datatr['bal_bonus_amt'] = $findlastow->bonus;
											$datatr['bal_win_amt'] = $dataqs['winning'];
											$datatr['bal_fund_amt'] = $findlastow->balance;
											$datatr['userid'] = $fpusk;
											$datatr['total_available_amt'] = $findlastow->balance+$dataqs['winning']+$findlastow->bonus;
											DB::table('transactions')->insert($datatr);
											//notifications entry//
											$datanot['message'] = 'You won amount Rs.'.$fpusv['amount'].' and 30% amount of '.$tdsdata['amount'].' deducted due to TDS.';
											$datanot['title'] = 'Congrats! You won a match.';
											$datanot['userid'] = $fpusk;
											$datanot['is_sent'] = 0;
											DB::table('notifications')->insert($datanot);
											//push notifications//

											//Helpers::sendnotification($titleget,$notificationdata['title'],'',$fpusk);
											//end push notifications//
											//mail//
											$challengename = "";
											if($val2->name==""){
												$challengename = 'Win-'.$val2->win_amount;
											}else{
												$challengename = $val2->name;
											}
											$email = $registeruserdetails->email;
											$emailsubject = 'Congrats you won a challenge!';
											$content='<p><strong>Dear Challenger </strong></p>';
											$content.='<p>Congratulations. You have won a challenge of '.$challengename.' for match '.$listmatches->title.'  with points '.$fres['points'].'. An amount of Rs. '.$fpusv['amount'].' is transfered to your wallet and 30% amount is deducted due to Tax deduction as per government regulations. Enjoy!</p>';
											$content.='<p> For details , please check account balance.</p>';
											$msg = Helpers::mailheader();
											$msg.= Helpers::mailbody($content);
											$msg.= Helpers::mailfooter();
										// Helpers::mailsentFormat($email,$emailsubject,$msg);
									}else{
									//user balance//
									$registeruserdetails = DB::table('registerusers')->where('id',$fpusk)->first();
									$findlastow = DB::table('userbalances')->where('user_id',$fpusk)->first();
									$dataqs['winning'] =  $findlastow->winning+$fpusv['amount'];
									DB::table('userbalances')->where('id',$findlastow->id)->update($dataqs);
									if($fpusv['amount']>0){
										//transactions entry//
										$datatr['transaction_id'] = $transactionidsave;
										$datatr['type'] = 'Challenge Winning Amount';
										$datatr['transaction_by'] = 'FP11';
										$datatr['amount'] = $fpusv['amount'];
										$datatr['paymentstatus'] = 'confirmed';
										$datatr['challengeid'] = $val2->id;
										$datatr['win_amt'] = $fpusv['amount'];
										$datatr['bal_bonus_amt'] = $findlastow->bonus;
										$datatr['bal_win_amt'] = $dataqs['winning'];
										$datatr['bal_fund_amt'] = $findlastow->balance;
										$datatr['userid'] = $fpusk;
										$datatr['total_available_amt'] = $findlastow->balance+$dataqs['winning']+$findlastow->bonus;
										DB::table('transactions')->insert($datatr);
										//notifications entry//
										$datanot['message'] = 'You won amount Rs.'.$fpusv['amount'];
										$datanot['title'] = 'Congrats! You Won a match!';
										$datanot['userid'] = $fpusk;
										$datanot['is_sent'] = 0;
										DB::table('notifications')->insert($datanot);
										//push notifications//

										//Helpers::sendnotification($titleget,$datanot['title'],'',$fpusk);
										//end push notifications//
										//mail//
										$challengename = "";
										if($val2->name==""){
											$challengename = 'Win-'.$val2->win_amount;
										}else{
											$challengename = $val2->name;
										}
										$email = $registeruserdetails->email;
										$emailsubject = 'Congrats you won a challenge!';
										$content='<p><strong>Dear Challenger </strong></p>';
										$content.='<p>Congratulations. You have won a challenge of '.$challengename.' for match '.$listmatches->title.'  with points '.$fres['points'].'. An amount of Rs. '.$fpusv['amount'].' is transfered to your wallet. Enjoy!</p>';
										$content.='<p> <strong> Note:- </strong> Winning amount will be credited in your wallet after tax deductions as per government regulations.</p>';
										$msg = Helpers::mailheader();
										$msg.= Helpers::mailbody($content);
										$msg.= Helpers::mailfooter();
										// Helpers::mailsentFormat($email,$emailsubject,$msg);
									 }
									}

										*/} catch(\Illuminate\Database\QueryException $ex) {

                                }

                            }

                        }
                    }
                }

                if(!empty($finalresults)) {
                    // print_r($finalresults);die;
                    // $table = \DB::getTablePrefix().with(new self)->getTable(); print_r($table); exit;
                    //DB::table('finalresults')->insertOrUpdate($finalresults);
                    $this->insertOrUpdate($finalresults,'final_results');
                }

            }
            MatchesController::refer_bonus_new($matchkey);
            MatchesController::refer_bonus($matchkey);
        }
    }
    public function refund_allamount($match_key){
        $joinedleauges = DB::table('joined_leagues')->where('matchkey',$match_key)->select('challengeid','userid','id')->get();
        $listmatches = DB::table('list_matches')->where('matchkey',$match_key)->first();
        foreach($joinedleauges as $val1){ 
            $matchchallenges = DB::table('match_challenges')->where('id',$val1->challengeid)->where('status','!=','canceled')->get();
            foreach($matchchallenges as $val2){ 
                $leaugestransactions = DB::table('leagues_transactions')->where('matchkey',$match_key)->where('challengeid',$val1->challengeid)->get();
                foreach($leaugestransactions as $val3){
                    $entryfee=$val2->entryfee;
                    // $findlastow = DB::table('userbalances')->where('user_id',$val3->user_id)->first();
                    $registeruserdetails = DB::table('register_users')->where('id',$val3->user_id)->first();
                    // if(!empty($findlastow)){
                    if(1){

                        $dasts['status']='canceled';
                        DB::table('match_challenges')->where('id',$val2->id)->update($dasts);
                        // entry in refund //
                        $findjoinid =
                        $refunddata['userid']=$val3->user_id;
                        $refunddata['amount']=$entryfee;
                        $refunddata['joinid']=$val3->joinid;
                        $refunddata['challengeid']=$val1->challengeid;
                        $refunddata['reason']='Match canceled or abandoned';
                        $refunddata['matchkey']=$match_key;
                        // $findifalreadyrefund = DB::table('refunds')->where('joinid',$val3->joinid)->first();
                        try {
                            DB::table('refunds')->insert($refunddata);
                            // end entry in refund data//

                            //Update User Balance
                            /*$dataq['balance'] =  $findlastow->balance+$val3->balance;
							$dataq['winning'] =  $findlastow->winning+$val3->winning;
							$dataq['bonus'] =  $findlastow->bonus+$val3->bonus;
							//transactions//
							$registeruserdetails = DB::table('registerusers')->where('id',$val3->user_id)->first();
							$datatr['transaction_id'] = 'FP11-REFUND-'.rand(100,999).$val3->user_id;
							$datatr['type'] = 'Challenge Joining Fee Refund';
							$datatr['transaction_by'] = 'FP11';
							$datatr['amount'] = $entryfee;
							$datatr['paymentstatus'] = 'confirmed';
							$datatr['challengeid'] = $val2->id;
							$datatr['bonus_amt'] = $val3->bonus;
							$datatr['win_amt'] = $val3->winning;
							$datatr['addfund_amt'] = $val3->balance;
							$datatr['bal_bonus_amt'] = $dataq['bonus'];
							$datatr['bal_win_amt'] = $dataq['winning'];
							$datatr['bal_fund_amt'] = $dataq['balance'];
							$datatr['userid'] = $val3->user_id;
							$datatr['total_available_amt'] = $dataq['balance']+$dataq['winning']+$dataq['bonus'];
							DB::table('transactions')->insert($datatr);


							DB::table('userbalances')->where('id',$findlastow->id)->update($dataq);
							//mail//*/
                            $email = $registeruserdetails->email;
                            $emailsubject = 'Match Cancelled / Abandoned-Refund initiated!';
                            $content='<p><strong>Dear Challenger </strong></p>';
                            $content.='<p>Unfortunately the '.date('M d Y h:i:a',strtotime($listmatches->start_date)).' '.$listmatches->title.' has been Abandoned / cancelled and your entry fee for the match is getting refunded back to your FP11 wallet.</p>';
                            $msg = Helpers::mailheader();
                            $msg.= Helpers::mailbody($content);
                            $msg.= Helpers::mailfooter();
                            // Helpers::mailsentFormat($email,$emailsubject,$msg);
                            //notifications//
                            $datan['title'] = 'Refund Amount of Rs.'.$val2->entryfee.' for challenge cancellation';
                            $datan['userid'] = $val3->user_id;
                            // DB::table('notifications')->insert($datan);
                            //push notifications//
                            $titleget = 'Refund Amount!';
                            // Helpers::sendnotification($titleget,$datan['title'],'',$val3->user_id);
                            //end push notifications//

                        } catch(\Illuminate\Database\QueryException $ex) {

                        }

                    }
                }

            }
        }
        return 1;
    }


    public function match_result()
    {
        // $findalllistmatches = DB::table('series')->where('series_status','opened')->join('list_matches','list_matches.series','=','series.id')->select('series.name as series_name','series.id as series_id','list_matches.title as listmatches_title','series.created_at as created_at','series.end_date as end_date','list_matches.matchkey as listmatches_matchkey')->groupBY('series.id')->get();

        $findalllistmatches_cricket = DB::table('series')->where('series_status','opened')->join('list_matches','list_matches.series','=','series.id')->where('list_matches.sport_type','1')->select('series.name as series_name','series.id as series_id','list_matches.title as listmatches_title','series.created_at as created_at','series.end_date as end_date','list_matches.matchkey as listmatches_matchkey')->groupBY('series.id')->get();
        $findalllistmatches_football = DB::table('series')->where('series_status','opened')->join('list_matches','list_matches.series','=','series.id')->where('list_matches.sport_type','2')->select('series.name as series_name','series.id as series_id','list_matches.title as listmatches_title','series.created_at as created_at','series.end_date as end_date','list_matches.matchkey as listmatches_matchkey')->groupBY('series.id')->get();

        // $findwinners = DB::table('series')->join('list_matches','list_matches.series','=','series.id')->select('series.name as series_name','series.id as series_id','list_matches.title as listmatches_title','series.created_at as created_at','series.end_date as end_date', DB::raw('count(list_matches.series) as total'), DB::raw("SELECT id ,COUNT(*) count FROM list_matches WHERE final_status != 'winnerdeclared'"))->groupBY('series.id')->get();

        // echo '<pre>';print_r($findwinners);die;

        // , DB::raw('count(joined_leagues.userid) as total'))->groupBY('match_challenges.id')

        // return view('matches.match_result',compact('findalllistmatches'));
        return view('matches.match_result',compact('findalllistmatches_cricket','findalllistmatches_football'));
    }

    public function match_detail($id){
        $findalllistmatches = DB::table('series')->join('list_matches','list_matches.series','=','series.id')->join('match_challenges','match_challenges.matchkey','=','list_matches.matchkey')->where('series.id',$id)->where('list_matches.launch_status','launched')->select('match_challenges.id as match_challenges_id','list_matches.title as listmatches_title','list_matches.start_date as listmatches_start_date','list_matches.matchkey as listmatches_matchkey','series.id as series_id','list_matches.status as listmatches_status','list_matches.launch_status as listmatches_launch_status','list_matches.final_status as listmatches_final_status', DB::raw('count(match_challenges.matchkey) as total_challenge'), DB::raw('sum(match_challenges.joinedusers) as total_joinedusers'))->groupBY('list_matches.matchkey')->orderBy('list_matches.start_date','DESC')->get();
        return view('matches.match_detail',compact('findalllistmatches'));
    }

    public function match_points($matchkey){
        $findmatchdetails = DB::table('list_matches')->where('matchkey',$matchkey)->first();
        $match_scores = DB::table('result_matches')->join('players','players.player_key','=','result_matches.player_key')->join('match_players','match_players.playerid','=','players.id')->join('teams','teams.id','=','players.team')->where('result_matches.match_key',$matchkey)->where('match_players.matchkey',$matchkey)->orderBy('result_matches.innings')->orderBy('players.team')->orderBy('match_players.playerid')->select('result_matches.*','match_players.matchkey as matchplayers_matchkey','match_players.points as matchplayers_points','match_players.role as matchplayers_role','match_players.name as matchplayers_name','players.player_key as players_player_key','players.team as players_team','teams.team as teams_team','teams.short_name as team_short_name','teams.team_key as teams_team_key','match_players.playerid as matchplayers_playerid')->get();
        $match_points = DB::table('result_points')->join('players','players.id','=','result_points.playerid')->join('result_matches','result_matches.id','=','result_points.resultmatch_id')->join('match_players','match_players.playerid','=','players.id')->join('teams','teams.id','=','players.team')->where('result_points.matchkey',$matchkey)->where('match_players.matchkey',$matchkey)->orderBy('result_matches.innings')->orderBy('players.team')->orderBy('match_players.playerid')->select('result_points.*','match_players.matchkey as matchplayers_matchkey','match_players.points as matchplayers_points','match_players.role as matchplayers_role','match_players.name as matchplayers_name','players.player_key as players_player_key','players.team as players_team','teams.team as teams_team','teams.team_key as teams_team_key','result_matches.innings','teams.short_name as team_short_name')->get();
        return view('matches.match_points',compact('match_scores','match_points','findmatchdetails')); 
    }

    public function join_users($matchkey){
        $findalllistmatches = DB::table('match_challenges')->leftjoin('joined_leagues','joined_leagues.challengeid','=','match_challenges.id')->where('match_challenges.matchkey',$matchkey)->select('match_challenges.id as matchchallenges_id','match_challenges.entryfee as matchchallenges_entryfee','match_challenges.win_amount as matchchallenges_win_amount','match_challenges.maximum_user as matchchallenges_maximum_user','match_challenges.grand as matchchallenges_grand', DB::raw('count(joined_leagues.userid) as total'))->groupBY('match_challenges.id')->get();
        return view('matches.join_users',compact('findalllistmatches'));
    }

    public function player_points($matchkey,$playerid,Request $request){
        $match_scores = DB::table('result_matches')->join('players','players.player_key','=','result_matches.player_key')->join('match_players','match_players.playerid','=','players.id')->join('teams','teams.id','=','players.team')->where('result_matches.match_key',$matchkey)->where('result_matches.player_id',$playerid)->where('match_players.matchkey',$matchkey)->select('result_matches.*','match_players.matchkey as matchplayers_matchkey','match_players.points as matchplayers_points','match_players.role as matchplayers_role','match_players.name as matchplayers_name','players.player_key as players_player_key','players.team as players_team','teams.team as teams_team','teams.team_key as teams_team_key','match_players.playerid as matchplayers_playerid')->get();
        if ($request->isMethod('post')){
            $input = Input::all();
            unset($input['_token']);


            $rowCOllection = DB::table('result_matches')->where('match_key',$matchkey)->where('player_id',$playerid)->update($input);
            Session::flash('message', 'Successfully updated challenges!');
            Session::flash('alert-class', 'alert-success');
            return Redirect::action('MatchesController@player_points', array('matchkey'=>$matchkey,'playerkey'=>$playerid));
        }
        return view('matches.player_points',compact('match_scores'));
    }

    public function updatepoints(Request $request){
        if ($request->isMethod('post')){
            $input = Input::all();
            $matchkey = $input['matchkey'];
            $playerid = $input['playerid'];
            $innings = $input['inning'];
            $field = $input['field'];
            $value = $input['value'];
            if($field == 'negative_points'){
                if($value){
                    $data['duck']=1;
                } else {
                    $data['duck']=0;
                }
            } else {
                $data[$field]=$value;
            }

            MatchesController::refund_amount($matchkey);

            DB::table('result_matches')->where('match_key',$matchkey)->where('player_id',$playerid)->where('innings',$innings)->update($data);
            $val = DB::table('list_matches')->where('matchkey',$matchkey)->first();
            $match_type = $val->format;
            if($val->sport_type == 1)
                $showpoints = MatchesController::player_point($matchkey,$match_type);
            else if($val->sport_type == 2)
                $showpoints = MatchesController::player_point($matchkey,$match_type,'football');
            echo 1;die;
        }
    }

    public function update_result_points()
    {
        $matchplayers = DB::table('result_matches')->get();
        if(!empty($matchplayers)){
            foreach($matchplayers as $row){
                $data['startingpoints']=$row->starting11;
                $data['runs']=$row->runs;
                $data['fours']=$row->fours;
                $data['sixs']=$row->six;
                $data['strike_rate']=$row->strike_rate;
                $data['wickets']=$row->wicket;
                $data['maidens']=$row->maiden_over;
                $data['economy_rate']=$row->economy_rate;
                $data['catch']=$row->catch;
                $data['stumping']=$row->stumbed;
                $data['negative']=$row->negative_points;
                $data['total']=$row->total_points;
                DB::table('result_points')->where('matchkey',$row->match_key)->where('playerid',$row->player_id)->update($data);
            }
        }
    }

    public function updatematchfinalstatus($matchkey,$status){
        $input['final_status']=$status;
        $findseries = DB::table('list_matches')->where('matchkey',$matchkey)->select('series','format')->first();
        if($status=='IsAbandoned' || $status=='IsCanceled'){
            // return Redirect()->action('MatchesController@refund_amount',$matchkey);
            $resultpoints = MatchesController::refund_allamount($matchkey);
        }

        if($status=='winnerdeclared'){
            $res = MatchesController::distribute_winning_amount($matchkey,$findseries->format);
        }
        DB::table('list_matches')->where('matchkey',$matchkey)->update($input);
        Session::flash('message', 'Match '.$status.' successfully!');
        Session::flash('alert-class', 'alert-success');
        return redirect()->action('MatchesController@match_detail',$findseries->series);
    }
    public function updatemanually(Request $request){
        if ($request->isMethod('post')){
            $input = Input::all();
            $matchkey = $input['matchkey'];
            if($input['final_status']!=""){
				$data['final_status']=$input['final_status'];
				if($input['final_status']=="IsReviewed"){
					$data['status']="completed";
				}
				else if($input['final_status']=="pending"){
					$data['status']="pending";
				}
                DB::table('list_matches')->where('matchkey',$matchkey)->update($data);
            }
            Session::flash('message', 'Match status successfully!');
            Session::flash('alert-class', 'alert-success');
            return Redirect::back();
        }
    }
    public function match_reviewed(Request $request){
        if ($request->isMethod('post')){
            $input = Input::all();
            $matchkey = $input['matchkey'];
            $data['final_status']=$input['final_status'];
            $data['review']=$input['review'];
            DB::table('list_matches')->where('matchkey',$matchkey)->update($data);
            Session::flash('message', 'Match Reviewed successfully!');
            Session::flash('alert-class', 'alert-success');
            return Redirect::back();
        }
    }
    public function marathon_result()
    {
        $finduserpoints = array();
        $findmarathon = array();
        $findallseries = DB::table('series')->where('series_status','opened')->get();
		$findbestmatches=null;
        if(isset($_GET['series'])){
            $findmarathon = DB::table('marathon')->where('series',$_GET['series'])->first();
            if(!empty($findmarathon)){
                $findbestmatches = $findmarathon->bestmatches;
            }
        }
        return view('matches.marathon_result',compact('findallseries','findmarathon','findbestmatches'));
    }
    public function viewwinners($matchkey){
        $findwinners = array();
        $finduserjoinedleauges = DB::table('joined_leagues')->where('joined_leagues.matchkey',$matchkey)->join('join_teams','join_teams.id','=','joined_leagues.teamid')->join('match_challenges','match_challenges.id','=','joined_leagues.challengeid')->join('register_users','register_users.id','=','joined_leagues.userid')->orderBy('joined_leagues.challengeid','ASC')->select('match_challenges.win_amount','match_challenges.entryfee','match_challenges.joinedusers','match_challenges.name','match_challenges.is_private','match_challenges.confirmed_challenge','match_challenges.maximum_user','match_challenges.grand','joined_leagues.*','register_users.username as username','register_users.email','register_users.team','join_teams.points')->get();
        return view('matches.viewwinners',compact('finduserjoinedleauges'));
    }

    public function deductmoney()
    {
        $matchkey = 'iplt20_2018_g56';
        $findallfinalresults = DB::table('final_results')->where('matchkey',$matchkey)->groupBy('joinedid')->get();
        if(!empty($findallfinalresults)){
            foreach($findallfinalresults as $fresults){
                $findifduplicate = DB::table('final_results')->where('matchkey',$matchkey)->where('joinedid',$fresults->joinedid)->where('id','<>',$fresults->id)->get();
                if(!empty($findifduplicate)){
                    foreach($findifduplicate as $duplicate){
                        $gettransactionid = $duplicate->transaction_id;
                        $findtransaction = DB::table('transactions')->where('transaction_id',$duplicate->transaction_id)->first();
                        if(!empty($findtransaction)){
                            DB::table('transactions')->where('id',$findtransaction->id)->delete();
                        }
                        $finduseramount = DB::table('user_balances')->where('user_id',$duplicate->userid)->first();
                        $findwinning['winning'] = $finduseramount->winning-$duplicate->amount;

                        if($findwinning['winning']<0){
                            $findwinning['winning'] = 0;
                        }
                        DB::table('user_balances')->where('user_id',$duplicate->userid)->update($findwinning);
                        DB::table('final_results')->where('id',$duplicate->id)->delete();

                    }
                }
            }
        }
    }

    public function remove_player($id='', $matchkey='') {
        if(!empty($id) && !empty($matchkey)) {
            DB::table('match_players')->where('id', $id)->delete();
            Session::flash('message', 'Player Deleted Successfully!');
            Session::flash('alert-class', 'alert-success');
            return Redirect::back();
        } else {
            return Redirect::back();
        }
    }
 
    public static function get_playing_11($match_key) {
        //$match_key = 't20blasts2_2020_g24';
		date_default_timezone_set('Asia/Kolkata');
		$timeNow = date('Y-m-d H:i:s');
		$findmatch = DB::table('list_matches')->where('matchkey',$match_key)->where('only_admin',0)->first();
		if(!empty($findmatch)){
			$findseries = DB::table('series')->where('id',$findmatch->series)->first();
			if(!empty($findseries)){
				$seriesName=str_replace($findseries->name,"? ","");
				$pos = strpos($findseries->name,"|");
				$seriesName= $pos !== FALSE ? substr($findseries->name, $pos + strlen("|"), strlen($findseries->name)) : "";
				$findnotifications = DB::table('bulk_notifications')->where('matchkey',$match_key)->where('isSent',0)->get();
				if(!empty($findnotifications)){
					foreach($findnotifications as $notification){
						if(!empty($notification->minutes)){
							$update_data = array(
								"isSent" => 1 
							);
							if($notification->preToss==0){
								$timeBefore1 = date('Y-m-d H:i:s',strtotime('-'.$notification->minutes.' minutes', strtotime($findmatch->start_date)));
								if($timeBefore1<=$timeNow)
								{
									DB::table('bulk_notifications')->where('matchkey', $match_key)->where('id',$notification->id)->update($update_data);
									Helpers::sendnotificationBulk($notification->display." minutes to go ⏰ ".$findmatch->short_name,"🏏🏏".$seriesName." 🏏🏏\n".$findmatch->name."\nMatch Time ⏰: ".date('h:i A',strtotime($findmatch->start_date)),'FANADDA2');
								} 
							} 
							else if($notification->preToss==1 && $findmatch->PreTossAvailable==1 && !empty($findmatch->PreTossClosingTime)){
								$timeBefore1 = date('Y-m-d H:i:s',strtotime('-'.$notification->minutes.' minutes', strtotime($findmatch->PreTossClosingTime)));
								if($timeBefore1<=$timeNow)
								{
									DB::table('bulk_notifications')->where('matchkey', $match_key)->where('id',$notification->id)->update($update_data);
									Helpers::sendnotificationBulk($notification->display." minutes to go ⏰ PreToss Reminder",$findmatch->short_name."\n🏏🏏".$seriesName." 🏏🏏\nPreToss Deadline ⏰: ".date('h:i A',strtotime($findmatch->PreTossClosingTime)),'FANADDA2');
								}
							}
						}
					}
				}
			}
		}
        $giveresresult = CricketapiController::getmatchdetails($match_key);
        if($giveresresult['status_code'] == 404){
            $giveresresult = CricketapiController::getmatchdetailsV2($match_key);
            //print_r($giveresresult); exit;
        }
        if(!empty($giveresresult)){
            $mainarrayget = $giveresresult['data']['card'];
            $getmtdatastatus['status'] = $mainarrayget['status'];
            if($getmtdatastatus['status']=='completed'){
                $getmtdatastatus['final_status'] = 'IsReviewed';
            }
            DB::table('list_matches')->where('matchkey',$match_key)->update($getmtdatastatus);
            $findteams = $mainarrayget['teams'];
            $finalplayingteams = array();
            if(!empty($findteams)){
                foreach($findteams as $tp){
                    $findpl = $tp['match']['playing_xi'];
                    if(!empty($findpl)){
                        foreach($findpl as $fl){
                            $finalplayingteams[] = $fl;
                        }
                    }
                }

                foreach($finalplayingteams as $fl){
                    $player_details = DB::table('players')->join('match_players','players.id','=','match_players.playerid')->where('players.player_key',$fl)->where('match_players.matchkey', $match_key)->select('players.id')->first();
                    if(!empty($player_details)) {
                        $players[] = $player_details->id;
                    }
                }

                // DB::table('match_players')->where('matchkey',   )
                if(!empty($finalplayingteams)) {
                    $insert_data = array(

                        "match_key" => $match_key,

                        "player_ids" => serialize($players),

                    );
                    $findExist = DB::table('match_playing11')->where('match_key', $match_key)->first();
                    if(!empty($findExist)) {
						if($findExist->isLocked==0){
							DB::table('match_playing11')->where('match_key', $match_key)->update($insert_data);
						}
                    } else {
                        DB::table('match_playing11')->insert($insert_data);
						//$findmatch = DB::table('list_matches')->where('matchkey',$match_key)->where('only_admin',0)->first();
						$timeBefore = date('Y-m-d H:i:s',strtotime('-1 minutes', strtotime($findmatch->start_date)));
						if(!empty($findmatch) && ($timeBefore>=$timeNow)) {
							Helpers::sendnotificationBulk("Lineup Out 📢 ".$findmatch->short_name,"🏏🏏".$seriesName." 🏏🏏\n".$findmatch->name."\nMatch Time ⏰: ".date('h:i A',strtotime($findmatch->start_date)),'FANADDA2');
						}
                    }
                }
            }
            return array();
        }
        return array();
    } 

    public function update_fantasy_credit($matchkey) {
        $credit_details = CricketapiController::getmatchcredit($matchkey);
        // print_r($credit_details); exit;
        $data = @$credit_details['data'];
        if(!empty($data)) {
            $findmatchexist = DB::table('list_matches')->where('matchkey',$matchkey)->select('team1','team2')->first();
            $fantasy_points = @$data['fantasy_points']; //print_r($fantasy_points);
            if(!empty($fantasy_points)) {
                foreach ($fantasy_points as $key => $value) {
                    $player = $value['player'];
                    $credit = $value['credit_value'];

                    $findp1 = DB::table('players')->where('player_key',$player)->where('team',$findmatchexist->team1)->first();
                    if(empty($findp1)) {
                        $findp1 = DB::table('players')->where('player_key',$player)->where('team',$findmatchexist->team2)->first();
                    }

                    // print_r($findp1); print_r($credit);
                    $player_id = $findp1->id;
                    // print_r($value);
                    // echo $player_id; echo $matchkey; exit;
                    DB::table('match_players')->where('matchkey', $matchkey)->where('playerid', $player_id)->update(array("credit" => $credit));
                }
                // exit;
            }
        }
        // exit;
        return true;
    }

}
?>
