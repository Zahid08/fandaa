<?php
namespace App\Http\Controllers;
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Challenge;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\CricketapiController;

class ChallengesController extends Controller
{
    public function addglobalchallenge(Request $request)
    {
        // $listchallenge = DB::table('challenges_category')->join('sport_types','sport_types.id','=','challenges_category.sport_type')->select('challenges_category.name','challenges_category.id','sport_types.sport_key as s_name','sport_types.id as sport_id')->orderBY('challenges_category.id','DESC')->get();
        // print_r($listchallenge);die;
        if($request->isMethod('post')){
            $rules = array(
                'entryfee' => 'required',
                'win_amount' => 'required',
                'maximum_user' => 'required',
                'sport_type' => 'required',
                'challenge_category_id' => 'required',

            );
            $validator = Validator::make(Input::all(), $rules);
            if($validator->fails()){
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput(Input::except('password'));
            }
            else{
                $input = $request->input();
                $categoryIds = DB::table('challenges_category')->where('sport_type',$request->sport_type)->pluck('id');
                $findchallenge = DB::table('challenges')->whereIn('challenge_category_id',$categoryIds)->where('entryfee',$input['entryfee'])->where('win_amount',$input['win_amount'])->where('maximum_user',$input['maximum_user'])->first();
                if(!empty($findchallenge)){
                    return Redirect::back()->withErrors('This challenge is already exits with the same wining amount, entry fees and maximum number of users.')->withInput(Input::except('password'));

                }
                unset($input['_token']);

                DB::table('challenges')->insert($input);
                Session::flash('message', 'Successfully added global challenge!');
                Session::flash('alert-class', 'alert-success');
                return Redirect::back();
            }
        }
        // $listchallenge = DB::table('challenges_category')->get();
        $listchallenge = DB::table('challenges_category')->select('challenges_category.name','challenges_category.id')->orderBY('challenges_category.id','DESC')->get();
        $sport_type = DB::table('sport_types')->get();
        return view('challenges.addglobalchallenge',compact('listchallenge','sport_type'));
    }

    public function addglobalchallengeccfilter()
    {
        if (isset($_GET['sp'])) {
            $cc = DB::table('challenges_category')->where('sport_type',$_GET['sp'])->select('id','name')->get();
            echo json_encode($cc);
        }
    }


    public function editglobalchallengeccfilter()
    {
        if (isset($_GET['sp'])) {
            $cc = DB::table('challenges_category')->where('sport_type',$_GET['sp'])->select('id','name')->get();
            echo json_encode($cc);
        }
    }

    public function editglobalchallenge($id,Request $request){
        $id = unserialize(base64_decode($id));
        $challenge = DB::table('challenges')->where('id',$id)->first();
        // $listchallenge = DB::table('challenges_category')->join('sport_types','sport_types.id','=','challenges_category.sport_type')->select('challenges_category.name','challenges_category.id','sport_types.sport_key as s_name')->orderBY('challenges_category.id','DESC')->get();
        // $listchallenge = DB::table('challenges_category')->select('challenges_category.name','challenges_category.id')->orderBY('challenges_category.id','DESC')->get();
        if ($request->isMethod('post')){
            $input = Input::all();

            if (count($input)>0){
                //Set
                $rules = array(
                    'entryfee' => 'required',
                    'win_amount' => 'required',
                    'maximum_user' => 'required',
                    'challenge_category_id' => 'required',
                    'sport_type' => 'required',
                );
                $validator = Validator::make(Input::all(), $rules);
                //Ckeck Validation
                if($validator->fails()){
                    return Redirect::back()->withErrors($validator)->withInput(Input::except('password'));
                    //Validation is Comp
                }else{
                    $findseries = DB::table('challenges')->where('entryfee',$input['entryfee'])->where('win_amount',$input['win_amount'])->where('maximum_user',$input['maximum_user'])->where('id','!=',$id)->where('sport_type',$input['sport_type'])->where('challenge_category_id',$input['challenge_category_id'])->first();
                    if(!empty($findseries)){
                        return Redirect::back()
                            ->withErrors('This challenge is already exist.')
                            ->withInput(Input::except('password'));
                    }

                    unset($input['_token']);
                    if(isset($input['multi_entry'])){
                        $input['multi_entry'] = 1;
                    }else{
                        $input['multi_entry'] = 0;
                    }
                    if(isset($input['confirmed_challenge'])){
                        $input['confirmed_challenge'] = 1;
                    }else{
                        $input['confirmed_challenge'] = 0;
                    }
                    if(isset($input['is_running'])){
                        $input['is_running'] = 1;
                    }else{
                        $input['is_running'] = 0;
                    }
                    if(isset($input['bonus'])){
                        $input['bonus'] = 1;
                    }else{
                        $input['bonus'] = 0;
                    }

                    $rowCOllection = DB::table('challenges')->where('id',$id)->update($input);
                    Session::flash('message', 'Successfully updated challenges!');
                    Session::flash('alert-class', 'alert-success');
                    return Redirect::back();
                    // $query = DB::table('challenges');
                    // $getlist = $query->orderBY('id','DESC')->paginate(100);
                    // return Redirect::route('challenges.viewchallenges')->with('getlist', $getlist);
                    // return Redirect::route('ChallengesController@viewchallenges');
                }
            }
        }
        if(!empty($challenge)){
            $listchallenge = DB::table('challenges_category')->where('sport_type',$challenge->sport_type)->select('challenges_category.name','challenges_category.id')->orderBY('challenges_category.id','DESC')->get();

            $sport_type = DB::table('sport_types')->get();
            return view('challenges.editglobalchallenge',compact('challenge','listchallenge','sport_type'));
        }else{
            return redirect()->action('ChallengesController@viewchallenges')->withErrors('Invalid Id Provided');
        }
    }

    public function viewchallenges(){
	    $cricketCategory = DB::table('challenges_category')->where('sport_type','1')->pluck('id');
	    $footballCategory = DB::table('challenges_category')->where('sport_type','2')->pluck('id');
        $query = DB::table('challenges');
        if(request()->has('win_amount')){
            $win_amount=request('win_amount');
            if($win_amount!=""){
                $query->where('win_amount',$win_amount);
            }
        }
        if(request()->has('entryfee')){
            $entryfee = request('entryfee');
            if($entryfee!=""){
                $query->where('entryfee',$entryfee);
            }
        }
        if(request()->has('maximum_user')){
            $maximum_user = request('maximum_user');
            if($maximum_user!=""){
                $query->where('maximum_user',$maximum_user);
            }
        }
		// $getlist = $query->orderBY('id','DESC')->paginate(100);
		$getlist_cricket = $query->whereIn('challenge_category_id',$cricketCategory)->orderBY('id','DESC')->paginate(100);
        $footballQuery = DB::table('challenges');
        if(request()->has('win_amount')){
            $win_amount=request('win_amount');
            if($win_amount!=""){
                $query->where('win_amount',$win_amount);
            }
        }
        if(request()->has('entryfee')){
            $entryfee = request('entryfee');
            if($entryfee!=""){
                $query->where('entryfee',$entryfee);
            }
        }
        if(request()->has('maximum_user')){
            $maximum_user = request('maximum_user');
            if($maximum_user!=""){
                $query->where('maximum_user',$maximum_user);
            }
        }

		$getlist_football = $footballQuery->whereIn('challenge_category_id',$footballCategory)->orderBY('id','DESC')->paginate(100);
		// $getlist_football = $query->orderBY('id','DESC')->paginate(100);
		// return view('challenges.viewchallenges')->with('getlist', $getlist);
		return view('challenges.viewchallenges',compact('getlist_cricket','getlist_football'));
    }
    public function deleteglobalchallenge($id){
        $id = unserialize(base64_decode($id));
        $challenge = DB::table('challenges')->where('id',$id)->first();
        if(!empty($challenge)){
            DB::table('challenges')->where('id',$id)->delete();
            Session::flash('message', 'Successfully deleted challenges!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->action('ChallengesController@viewchallenges');
        }
        else{
            return redirect()->action('ChallengesController@viewchallenges')->withErrors('Invalid Id Provided');
        }
    }


    public function addtogrand(Request $request){
        if ($request->isMethod('post')){
            $input = Input::all();
            // $input['id'] = '17';
            // $input['matchkey'] = 'bblt20_2017_g20';

            $matchid = $input['matchkey'];
            $query = DB::table('match_challenges');
            $query->where('matchkey',$matchid);
            $allchallenges = $query->get();
            $findallchallenge = DB::table('match_challenges')->where('matchkey',$matchid)->where('grand',1)->first();
            // if(!empty($findallchallenge)){
            // $findjoinedleauges = DB::table('joined_leagues')->where('challengeid',$findallchallenge->id)->get();
            // if(!empty($findjoinedleauges)){
            // echo 2;die;
            // }
            // }
            if(!empty($allchallenges)){
                foreach($allchallenges as $challenge){

                    if($input['id']==$challenge->id){
                        $data['grand'] = 1;
                    }else{
                        $data['grand'] = 0;
                    }
                    Session::flash('message', 'Successfully created grand challenge!');
                    Session::flash('alert-class', 'alert-success');
                    DB::table('match_challenges')->where('id',$challenge->id)->update($data);
                }
            }

            echo 1;die;
        }
    }

    public function createchallenge(){
        $allchallenges = array();
        if(request()->has('matchid')){
            $query = DB::table('match_challenges');
            $matchid=request('matchid');
            // print_r($matchid);die;
            $chalangematch = DB::table('challenges')
                ->join('match_challenges','match_challenges.challenge_id','=','challenges.id')

                ->select('match_challenges.id')->where('match_challenges.matchkey','=',$matchid)

                ->get();
            // echo "<pre>"; print_r($chalangematch);die;
            if($matchid!=""){
                $query->where('matchkey',$matchid);

            }
            $allchallenges = $query->orderBy('marathon',1)->orderBy('grand',1)->get();
        }

        $currentdate = date('Y-m-d h:i:s');
        $findalllistmatches = DB::table('list_matches')->select('list_matches.name as matchname','list_matches.matchkey as matchkey')->Where('launch_status','launched')->where('start_date','>=',$currentdate)->orderBY('start_date','ASC')->get();

        return view('challenges.createchallenge',compact('findalllistmatches','allchallenges'));
    }
    public function addmatchchallenge(Request $request){
        $currentdate = date('Y-m-d h:i:s');
        $findalllistmatches = DB::table('list_matches')->select('list_matches.name as matchname','list_matches.matchkey as matchkey')->Where('launch_status','launched')->where('start_date','>=',$currentdate)->orderBY('start_date','ASC')->get();
        if ($request->isMethod('post')){
            $input = Input::all();
            unset($input['_token']);
            DB::table('match_challenges')->insert($input);
            Session::flash('message', 'Successfully added challenges!');
            Session::flash('alert-class', 'alert-success');
            return Redirect::action('ChallengesController@createchallenge');
        }
        return view('challenges.addmatchchallenge',compact('findalllistmatches'));
    }
    public function editmatchchallenge($id,Request $request){
        $id = unserialize(base64_decode($id));
        $challenge = DB::table('match_challenges')->where('id',$id)->first();
        if(!empty($challenge)){
            $findmatchnames = DB::table('list_matches')->where('matchkey',$challenge->matchkey)->select('list_matches.name','list_matches.start_date')->first();
            if ($request->isMethod('post')){
                $input = Input::all();
                unset($input['_token']);
                if(isset($input['multi_entry'])){
                    $input['multi_entry'] = 1;
                }else{
                    $input['multi_entry'] = 0;
                }
				if(isset($input['bonus'])){
                    $input['bonus'] = 1; 
                }else{
                    $input['bonus'] = 0;
                }
                if(isset($input['confirmed_challenge'])){
                    $input['confirmed_challenge'] = 1;
                }else{
                    $input['confirmed_challenge'] = 0;
                }
                if(isset($input['is_running'])){
                    $input['is_running'] = 1;
                }else{
                    $input['is_running'] = 0;
                }
				if(isset($input['preToss'])){
                    $input['preToss'] = 1;
                }else{
                    $input['preToss'] = 0;
                }
				if(isset($input['only_admin'])){
                    $input['only_admin'] = 1; 
                }else{
                    $input['only_admin'] = 0;
                }
                /*$findjoinedleauges = DB::table('joined_leagues')->where('challengeid',$id)->get();
                if(!empty($findjoinedleauges)){
                    Session::flash('message', 'You cannot edit this challenge now!');
                    Session::flash('alert-class', 'alert-danger');
                    return redirect()->back();
                }*/
                $rowCOllection = DB::table('match_challenges')->where('id',$id)->update($input);
                Session::flash('message', 'Successfully updated challenges!');
                Session::flash('alert-class', 'alert-success');
                return Redirect::action('ChallengesController@createchallenge', array('matchid'=>$challenge->matchkey));
            }
            return view('challenges.editmatchchallenge',compact('challenge','findmatchnames'));

        }else{
            return redirect()->action('ChallengesController@createchallenge')->withErrors('Invalid match Provided');
        }
    }
    public function delmatchchallenge($id){
        $id = unserialize(base64_decode($id));
        $challenge = DB::table('match_challenges')->where('id',$id)->first();
        if(!empty($challenge)){
            $findpricecards = DB::table('match_price_cards')->where('challenge_id',$id)->get();
            if(!empty($findpricecards)){
                foreach($findpricecards as $pp){
                    DB::table('match_price_cards')->where('id',$pp->id)->delete();
                }
            }
            DB::table('match_challenges')->where('id',$id)->delete();
            Session::flash('message', 'Successfully deleted challenges!');
            Session::flash('alert-class', 'alert-success');
            return Redirect::action('ChallengesController@createchallenge', array('matchid'=>$challenge->matchkey));
        }else{
            return redirect()->action('ChallengesController@createchallenge')->withErrors('Invalid match Provided');
        }
    }

    public function importdata($id,$sport_type = 1){
        $findmatch = DB::table('list_matches')->where('matchkey',$id)->where('sport_type',$sport_type)->first();
        // import marathon challenge //
        $findseriesid = $findmatch->series;
        $findmarathon = DB::table('marathon')->where('series',$findseriesid)->first();
        if(!empty($findmarathon)){
            $mdata['name'] = $findmarathon->name;
            $mdata['challenge_id'] = $findmarathon->id;
            $mdata['entryfee'] = $findmarathon->entryfee;
            $mdata['win_amount'] = $findmarathon->win_amount;
            $mdata['maximum_user'] = $findmarathon->maximum_user;
            $mdata['bestmatches'] = $findmarathon->bestmatches;
            $mdata['confirmed_challenge'] = 1;
            $mdata['marathon'] = 1;
            $mdata['series_id'] = $findseriesid;
            $mdata['matchkey'] = $findmatch->matchkey;
            $findmchallenge = DB::table('match_challenges')->where('challenge_id',$findmarathon->id)->where('marathon',1)->where('matchkey',$findmatch->matchkey)->where('series_id',$findseriesid)->first();
            if(empty($findmchallenge)){
                $getmcid = DB::table('match_challenges')->insertGetId($mdata);
                // $findmpricecrads = DB::table('marathonpricecards')->where('challenge_id',$findmarathon->id)->get();
                // if(!empty($findmpricecrads)){
                // foreach($findmpricecrads as $mpricec){
                // $mpdata['challenge_id'] = $getmcid;
                // $mpdata['matchkey'] = $findmatch->matchkey;
                // $mpdata['winners'] = $mpricec->winners;
                // $mpdata['price'] = $mpricec->price;
                // $mpdata['min_position'] = $mpricec->min_position;
                // $mpdata['max_position'] = $mpricec->max_position;
                // $mpdata['description'] = $mpricec->description;
                // $mpdata['total'] = $mpricec->total;
                // DB::table('match_price_cards');
                // DB::table('match_price_cards')->insert($mpdata);
                // }
                // }
            }
        }

        if(!empty($findmatch)){
            $categoryIds = DB::table('challenges_category')->where('sport_type',$sport_type)->pluck('id');
            $findleauges	= DB::table('challenges')->whereIn('challenge_category_id',$categoryIds)->get();
            $charray = array();
            if(!empty($findleauges)){
                foreach($findleauges as $leauge){
                    $findchallengeexist = DB::table('match_challenges')->where('matchkey',$id)->where('challenge_id',$leauge->id)->where('sport_type',$sport_type)->first();
                    if(empty($findchallengeexist)){
                        $data['challenge_id'] = $leauge->id;
                        $data['entryfee'] = $leauge->entryfee;
                        $data['win_amount'] = $leauge->win_amount;
                        $data['maximum_user'] = $leauge->maximum_user;
                        $data['multi_entry'] = $leauge->multi_entry;
                        $data['confirmed_challenge'] = $leauge->confirmed_challenge;
                        $data['is_running'] = $leauge->is_running;
                        $data['sport_type'] = $sport_type;
                        $data['challenge_category_id'] = $leauge->challenge_category_id;
                        $data['matchkey'] = $id;
                        $getcid = DB::table('match_challenges')->insertGetId($data);
                        $findpricecrads = DB::table('price_cards')->where('challenge_id',$leauge->id)->get();
                        if(!empty($findpricecrads)){
                            foreach($findpricecrads as $pricec){
                                $pdata['challenge_id'] = $getcid;
                                $pdata['matchkey'] = $id;
                                $pdata['winners'] = $pricec->winners;
                                $pdata['price'] = $pricec->price;
                                $pdata['min_position'] = $pricec->min_position;
                                $pdata['max_position'] = $pricec->max_position;
                                $pdata['description'] = $pricec->description;
                                $pdata['total'] = $pricec->total;
                                DB::table('match_price_cards');
                                DB::table('match_price_cards')->insert($pdata);
                            }
                        }
                    }
                }
            }
            Session::flash('message', 'Challenges inported successfully');
            Session::flash('alert-class', 'alert-success');
            return redirect()->back();


        }
        else{
            return redirect()->action('ChallengesController@createchallenge')->withErrors('Invalid match Provided');
        }
    }
    public function addpricecard($id,Request $request){
        $addcardid = $id;
        $id = unserialize(base64_decode($id));
        $totalpriceamounts = DB::table('price_cards')->where('challenge_id',$id)->select(DB::raw('sum(price_cards.total) as totalpriceamount'))->get();
        $findallpricecards = DB::table('price_cards')->where('challenge_id',$id)->select('price_cards.*')->get();
        $findchallenge = DB::table('challenges')->where('id',$id)->get();
        if(!empty($findchallenge)){
            $min_position=0;
            $totalpriceamount=0;
            if(!empty($findallpricecards)){
                $findminposition = DB::table('price_cards')->where('challenge_id',$id)->orderBY('id','DESC')->select('max_position')->first();
                $min_position = $findminposition->max_position;
                $totalpriceamount = $totalpriceamounts[0]->totalpriceamount;
            }
            if($request->isMethod('post')){
                $input = Input::all();
                $input['max_position'] = $input['min_position']+$input['winners'];
                $input['total'] = $input['price']*$input['winners'];
                $input['challenge_id'] = $id;
                unset($input['_token']);
                $countamount = $totalpriceamount+$input['total'];
                if($countamount > $findchallenge[0]->win_amount){
                    return redirect()->action('ChallengesController@addpricecard',$addcardid)->withErrors('Your price cards amount is greater than the total wining amount');
                }
                if($input['max_position'] > $findchallenge[0]->maximum_user){
                    return redirect()->action('ChallengesController@addmatchpricecard',$addcardid)->withErrors('You cannot add more winners.');
                }
                DB::table('price_cards')->insert($input);
                Session::flash('message', 'Price Card Successfully add');
                Session::flash('alert-class', 'alert-success');
                return redirect()->action('ChallengesController@addpricecard',$addcardid);
            }
        }else{
            return redirect()->action('ChallengesController@viewchallenges')->withErrors('Invalid match Provided');
        }
        return view('challenges.addpricecard',compact('findallpricecards','addcardid','min_position','findchallenge'));
    }
    public function addmatchpricecard($id,Request $request){
        $addcardid = $id;
        $id = unserialize(base64_decode($id));
        $totalpriceamounts = DB::table('match_price_cards')->where('challenge_id',$id)->select(DB::raw('sum(matchpricecards.total) as totalpriceamount'))->get();
        $findallpricecards = DB::table('match_price_cards')->where('challenge_id',$id)->select('matchpricecards.*')->get();
        $findchallenge = DB::table('match_challenges')->where('id',$id)->get();
        if(!empty($findchallenge)){
            $min_position=0;
            $totalpriceamount=0;
            if(!empty($findallpricecards)){
                $findminposition = DB::table('match_price_cards')->where('challenge_id',$id)->orderBY('id','DESC')->select('max_position')->first();
                $min_position = $findminposition->max_position;
                $totalpriceamount = $totalpriceamounts[0]->totalpriceamount;
            }
            if($request->isMethod('post')){
                $input = Input::all();
                $input['max_position'] = $input['min_position']+$input['winners'];
                $input['total'] = $input['price']*$input['winners'];
                $input['challenge_id'] = $id;
                $input['matchkey'] = $findchallenge[0]->matchkey;
                unset($input['_token']);
                $countamount = $totalpriceamount+$input['total'];
                if($countamount > $findchallenge[0]->win_amount){
                    return redirect()->action('ChallengesController@addmatchpricecard',$addcardid)->withErrors('Your price cards amount is greater than the total wining amount');
                }

                if($input['max_position'] > $findchallenge[0]->maximum_user){
                    return redirect()->action('ChallengesController@addmatchpricecard',$addcardid)->withErrors('You cannot add more winners.');
                }
                DB::table('match_price_cards')->insert($input);
                Session::flash('message', 'Price Card Successfully add');
                Session::flash('alert-class', 'alert-success');
                return redirect()->action('ChallengesController@addmatchpricecard',$addcardid);
            }
        }
        else{
            return redirect()->action('ChallengesController@createchallenge')->withErrors('Invalid match Provided');
        }
        return view('challenges.addmatchpricecard',compact('findallpricecards','addcardid','min_position','findchallenge'));
    }
    public function deletepricecard($id){
        $id = unserialize(base64_decode($id));
        $findallpricecards = DB::table('price_cards')->where('id',$id)->first();
        if(!empty($findallpricecards)){
            DB::table('price_cards')->where('id',$id)->delete();
            Session::flash('message', 'Price Card Successfully deleted');
            Session::flash('alert-class', 'alert-success');
            return Redirect::back();
        }else{
            return redirect()->action('ChallengesController@viewchallenges')->withErrors('Invalid match Provided');
        }
    }
    public function deletematchpricecard($id){
        $id = unserialize(base64_decode($id));
        $findallpricecards = DB::table('match_price_cards')->where('id',$id)->first();
        if(!empty($findallpricecards)){
            DB::table('match_price_cards')->where('id',$id)->delete();
            Session::flash('message', 'Price Card Successfully deleted');
            Session::flash('alert-class', 'alert-success');
            return Redirect::back();
        }else{
            return redirect()->action('ChallengesController@viewchallenges')->withErrors('Invalid match Provided');
        }
    }
    public function deletemarathonpricecard($id){
        $id = unserialize(base64_decode($id));
        $findallpricecards = DB::table('marathonpricecards')->where('id',$id)->first();
        if(!empty($findallpricecards)){
            DB::table('marathonpricecards')->where('id',$id)->delete();
            Session::flash('message', 'Marathon price Cards Successfully deleted');
            Session::flash('alert-class', 'alert-success');
            return Redirect::back();
        }else{
            return redirect()->action('ChallengesController@viewallmarathons')->withErrors('Invalid match Provided');
        }
    }

    public function addmarathonchallenge(Request $request){
        $currentdate = date('Y-m-d h:i:s');
        $findalllistmatches = DB::table('series')->select('series.name as seriesname','series.id as seriesid')->whereDate('end_date','>',$currentdate)->orderBY('end_date','ASC')->get();
        if ($request->isMethod('post')){
            $input = Input::all();
            $findexistchallenge = DB::table('marathon')->where('series',$input['series_id'])->first();
            if(!empty($findexistchallenge)){
                Session::flash('message', 'Marathon challenge is already present in this series');
                Session::flash('alert-class', 'alert-danger');
                return Redirect::back();
            }
            $input['series']=$input['series_id'];
            // $input['maximum_user']='-99';
            unset($input['_token']);
            unset($input['series_id']);
            $marathon_id = DB::table('marathon')->insertGetId($input);
            Session::flash('message', 'Successfully added marathon challenges!');
            Session::flash('alert-class', 'alert-success');
            return Redirect::action('ChallengesController@createmarathonchallenge');
        }
        return view('challenges.addmarathonchallenge',compact('findalllistmatches'));
    }
    public function viewallmarathons(){
        $getlist = DB::table('marathon')->join('series','series.id','=','marathon.series')->select('series.name as seriesname','marathon.*')->get();
        return view('challenges.viewallmarathons',compact('getlist'));
    }
    public function createmarathonchallenge(){
        $allchallenges = array();
        if(request()->has('matchid')){
            $query = DB::table('match_challenges');
            $matchid=request('matchid');
            if($matchid!=""){
                $query->where('series_id',$matchid)->where('marathon',1);
            }
            $allchallenges = $query->get();
        }
        $currentdate = date('Y-m-d h:i:s');
        $findalllistmatches = DB::table('series')->orderBY('start_date','ASC')->get();
        return view('challenges.createmarathonchallenge',compact('findalllistmatches','allchallenges'));
    }
    public function addmarathonpricecard($id,Request $request){
        $addcardid = $id;
        $id = unserialize(base64_decode($id));
        $totalpriceamounts = DB::table('marathonpricecards')->where('challenge_id',$id)->select(DB::raw('sum(marathonpricecards.total) as totalpriceamount'))->get();
        $findallpricecards = DB::table('marathonpricecards')->where('challenge_id',$id)->select('marathonpricecards.*')->get();
        $findchallenge = DB::table('marathon')->where('marathon.id',$id)->join('series','series.id','=','marathon.series')->select('series.name as seriesname','marathon.*')->get();
        if(!empty($findchallenge)){
            $min_position=0;
            $totalpriceamount=0;
            if(!empty($findallpricecards)){
                $findminposition = DB::table('marathonpricecards')->where('challenge_id',$id)->orderBY('id','DESC')->select('max_position')->first();
                $min_position = $findminposition->max_position;
                $totalpriceamount = $totalpriceamounts[0]->totalpriceamount;
            }
            if($request->isMethod('post')){
                $input = Input::all();
                $input['max_position'] = $input['min_position']+$input['winners'];
                $input['total'] = $input['price']*$input['winners'];
                $input['challenge_id'] = $id;
                unset($input['_token']);
                $countamount = $totalpriceamount+$input['total'];
                if($countamount > $findchallenge[0]->win_amount){
                    return redirect()->action('ChallengesController@addmarathonpricecard',$addcardid)->withErrors('Your price cards amount is greater than the total wining amount');
                }

                if($input['max_position'] > $findchallenge[0]->maximum_user){
                    return redirect()->action('ChallengesController@addmarathonpricecard',$addcardid)->withErrors('You cannot add more winners.');
                }
                DB::table('marathonpricecards')->insert($input);
                Session::flash('message', 'Price Card Successfully add');
                Session::flash('alert-class', 'alert-success');
                return redirect()->action('ChallengesController@addmarathonpricecard',$addcardid);
            }
        }
        else{
            return redirect()->action('ChallengesController@addmarathonchallenge')->withErrors('Invalid match Provided');
        }
        return view('challenges.addmarathonpricecard',compact('findallpricecards','addcardid','min_position','findchallenge'));
    }

    public function challengscategory(){

        //Get sport types
        $sport_type = DB::table('sport_types')->get();
        return view ('challenges.challengesCategory',compact('sport_type'));

    }
    public function addcategorychallenges(Request $request)
    {
        if($request->isMethod('post')){
            $rules = array(
                'name' => 'required',
                'contest_sub_text' => 'required',
                'contest_type_image' => 'required',
                'sport_type' => 'required',
                'sort_order' => 'required',
                'status' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if($validator->fails()){
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput(Input::except('password'));
            }
            else{
                $input = $request->input();
                $findchallenge = DB::table('challenges_category')->first();
                $photo = $request->file('contest_type_image');
                $destinationPath = base_path() . '/images/';
                $image = $photo->getClientOriginalName();
                $photo->move($destinationPath, $image);
                unset($input['_token']);
                $input['contest_type_image'] = $image;
                DB::table('challenges_category')->insert($input);
                Session::flash('message', 'Successfully added challenge Category !');
                Session::flash('alert-class', 'alert-success');
                return Redirect::back();
            }
        }
        //Get sport types
        $sport_type = DB::table('sport_types')->get();
        return view('challenges.challengesCategory',compact('sport_type'));
    }

    public function viewcategorychallenges(Request $request)
    {
        $query = DB::table('challenges_category')->join('sport_types','sport_types.id','=','challenges_category.sport_type')->select('challenges_category.name','challenges_category.contest_sub_text','challenges_category.created_at','challenges_category.id','challenges_category.contest_type_image','challenges_category.sort_order','challenges_category.sport_type','sport_types.sport_key as s_name');

        if(request()->has('name')){
            $name=request('name');
            if($name!=""){
                $query->where('name', 'LIKE', '%'.$name.'%');
            }
        }
        if(request()->has('contest_sub_text')){
            $contest_sub_text=request('contest_sub_text');
            if($contest_sub_text!=""){
                $query->where('contest_sub_text', 'LIKE', '%'.$contest_sub_text.'%');
            }
        }
        if(request()->has('sport_type')){
            $sport_type=request('sport_type');
            if($sport_type!=""){
                $query->where('sport_types.sport_key', 'LIKE', '%'.$sport_type.'%');
            }
        }
        $getlist = $query->orderBY('id','DESC')->paginate(15);
        return view('challenges.viewcategorychallengs')->with('getlist', $getlist);
    }

    public function editchallengecategory($id,Request $request)
    {
        $challenge = DB::table('challenges_category')->where('id',unserialize(base64_decode($id)))->first();
        $matchs = DB::table('sport_types')->get();
        if ($request->isMethod('post')){
            $input = Input::all();
            unset($input['_token']);
            if(isset($input['contest_type_image'])){
                $photo = $request->file('contest_type_image');
                $destinationPath = base_path() . '/uploads/offers/';
                $image = $photo->getClientOriginalName();
                $photo->move($destinationPath, $image);
                $input['contest_type_image'] = $image;
            }
            DB::table('challenges_category')->where('id',unserialize(base64_decode($id)))->update($input);
            Session::flash('message', 'Challenge Category successfully updated !');
            Session::flash('alert-class', 'alert-success');
            return Redirect::back();
        }

        return view('challenges.editchallengecategory',compact('challenge','matchs'));


    }



}?>
