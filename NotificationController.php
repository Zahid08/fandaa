<?php
namespace App\Http\Controllers;
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Helpers;
use Hash;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
class NotificationController extends Controller {
	public function pushnotifications(Request $request){
		if ($request->isMethod('post')){
			$input = Input::all();
			$usertype = $input['usertype'];
			$uservalues = $input['uservalues'];
			if($usertype == 'specific'){
			    if($uservalues!=""){
			       $explodearray = explode(',',$uservalues);
			       if(!empty($explodearray)){
			           foreach($explodearray as $earray){
			               $finduseremail = DB::table('register_users')->where('id',$earray)->select('id','email')->first();
			               if(!empty($finduseremail)){
        						$titleget = $input['title'];
            					$msg = $input['message'];
            					Helpers::sendnotification($titleget,$msg,'',$finduseremail->id);
			               }
			           }
			       }
			   } 
			}else{
			    
			    $titleget = $input['title'];
				$msg = $input['message'];
				Helpers::sendnotificationBulk($titleget,$msg,'news');
			    
		        /*$totalUsers = DB::table('register_users')->count();
    			$loopSize = ceil($totalUsers / 1000);
    		    for($i = 0; $i < $loopSize; $i++){
    		        $findlallusers = DB::table('register_users')->offset($i * 1000)->limit(1000)->select('id','email')->get();
        			if(!empty($findlallusers)){
        				foreach($findlallusers as $user){
        					$titleget = $input['title'];
        					$msg = $input['message'];
        					Helpers::sendnotification($titleget,$msg,'',$user->id);
        				}
        			}
    		    }*/
			}
			Session::flash('message', 'notifications send!');
			Session::flash('alert-class', 'alert-success');
			return Redirect::back();
		}
		return view('notifications.sendnotifications');
	}
	
	public function smsnotifications(Request $request){
		if ($request->isMethod('post')){
			$input = Input::all();
			$usertype = $input['usertype'];
			$uservalues = $input['uservalues'];
			if($usertype == 'specific'){
			    $explodearray = explode(',',$uservalues);
			       if(!empty($explodearray)){
			           foreach($explodearray as $earray){
			               $finduseremail = DB::table('register_users')->where('id',$earray)->select('mobile')->first();
			               if(!empty($finduseremail)){
			                   	$msg = $input['message'];
			                   	if($finduseremail->mobile != "")
    					            Helpers::sendTextSmsNew($msg,$finduseremail->mobile);
			               }
			           }
			       }
			}else{
			   $findlallusers = DB::table('register_users')->select('mobile')->get();
    			if(!empty($findlallusers)){
    				foreach($findlallusers as $user){
    					//$titleget = $input['title'];
    					$msg = $input['message'];
    					Helpers::sendTextSmsNew($msg,$user->mobile);
    				}
    			} 
			}
			Session::flash('message', 'sms sent!');
			Session::flash('alert-class', 'alert-success');
			return Redirect::back();
		}
		return view('notifications.smsnotification');
	}
	
	public function emailnotifications(Request $request){
		if ($request->isMethod('post')){
			$input = Input::all();
			$usertype = $input['usertype'];
			$uservalues = $input['uservalues'];
			if($usertype == 'specific'){
			   if($uservalues!=""){
			       $explodearray = explode(',',$uservalues);
			       if(!empty($explodearray)){
			           foreach($explodearray as $earray){
			               $finduseremail = DB::table('register_users')->where('id',$earray)->select('email')->first();
			               if(!empty($finduseremail)){
			                   	$titleget = $input['title'];
					            $msg = $input['message'];
					           
				                // Helpers::mailsentFormat($finduseremail->email,$titleget,$msg);
				                
        				        require 'vendor/autoload.php';    
        						$emailGrid = new \SendGrid\Mail\Mail(); 
        						$emailGrid->setFrom("noreply@fanadda.com", "Fanadda");
        						$emailGrid->setSubject($input['title']);
        						$emailGrid->addTo($finduseremail->email, "Example User");
        						$emailGrid->addContent(
        							"text/html", $msg
        						);
        						$sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY')); 
        						try {
        							 $response = $sendgrid->send($emailGrid); //print_r($response); exit; 
        						} catch (Exception $e) {
        							//error message
        						}
			               }
			           }
			       }
			   } 
			}
			else{
			    $startId = $input['startId'];
			    $endId = $input['endId'];
			    if(!empty($startId) && !empty($endId) && ($startId<$endId))
			        $findlallusers = DB::table('register_users')->where('id','>=',$startId)->where('id','<=',$endId)->select('email')->get();
			    if(!empty($findlallusers)){
				    foreach($findlallusers as $user){
				    	$titleget = $input['title'];
				        $msg = $input['message'];
				        // Helpers::mailsentFormat($user->email,$titleget,$msg);
				        
				        require 'vendor/autoload.php';    
						$emailGrid = new \SendGrid\Mail\Mail(); 
						$emailGrid->setFrom("noreply@fanadda.com", "Fanadda");
						$emailGrid->setSubject($titleget);
						$emailGrid->addTo($user->email, "Example User");
						$emailGrid->addContent(
							"text/html", $msg
						);
						$sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));


						try {
							 $response = $sendgrid->send($emailGrid);
						} catch (Exception $e) {
							//error message
						}
			    	}
		    	}
			}
		    Session::flash('message', 'Email send!');
			Session::flash('alert-class', 'alert-success');
			return Redirect::back();
		}
		return view('notifications.emailnotification');
	}
	
	
}?>