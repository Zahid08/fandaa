<?php
namespace App\Http\Controllers\v2; 
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Helpers;
use Hash;
use Mail;
use Cache;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
class FootballController extends Controller {
	
	public static function sourceUrl(){
		return 'https://api-football-v1.p.rapidapi.com/';
	}
	
	public static function getConfigs(){
		/*return array(
            "x-rapidapi-host: api-football-v1.p.rapidapi.com",
			"x-rapidapi-key: 265a09e498msh9ca1507bfba5d09p123779jsnfd4c602375fd"
        );*/
		return array( 
            "x-rapidapi-host: api-football-v1.p.rapidapi.com",
			"x-rapidapi-key: ahahhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"/*60badfe3a7msh3703fdfdd44a896p1a1f9djsn163cf38c5225*/
        );
	}
	
	public static function get_tournaments($country) {
      $header_parameters = FootballController::getConfigs();
	  $sourceUrl = FootballController::sourceUrl();
      $curl = curl_init();

		curl_setopt_array($curl, [
			CURLOPT_URL => $sourceUrl.'v2/leagues/current/'.$country,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10, 
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => $header_parameters,
		]);

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			echo $response;
		}
    } 
	
	public static function get_tournament_matches($leagueId=3093) { 
      $header_parameters = FootballController::getConfigs();
	  $sourceUrl = FootballController::sourceUrl();
      $curl = curl_init();

		curl_setopt_array($curl, [
			//CURLOPT_URL => $sourceUrl.'v2/fixtures/league/'.$leagueId.'?timezone=Asia%2FKolkata',
			CURLOPT_URL => $sourceUrl.'v2/fixtures/league/'.$leagueId.'/next/100?timezone=Asia%2FKolkata',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10, 
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => $header_parameters,
		]);

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			return json_decode($response, true);; 
		}
    }
	
	public static function getMatchDetails($matchkey) {
	  //echo $matchkey;
      $header_parameters = FootballController::getConfigs();
	  $sourceUrl = FootballController::sourceUrl();
      $curl = curl_init();

		curl_setopt_array($curl, [
			CURLOPT_URL => $sourceUrl.'v2/fixtures/id/'.$matchkey,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10, 
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => $header_parameters,
		]);

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			//echo "<br/><br/><br/><br/><br/><br/>".$response."<br/><br/><br/><br/><br/>";
			$response=json_decode($response, true);
			return $response['api']['fixtures'][0];
		}
    }
	
	public static function getSquadDetails($teamkey,$api_type) {
	  echo $teamkey."--".$api_type.",";
      $header_parameters = FootballController::getConfigs();
	  $sourceUrl = FootballController::sourceUrl();
      $curl = curl_init();
		$year = date("Y"); 
		$next_year =date('Y', strtotime('+1 year'));
		$prev_year =date('Y', strtotime('-1 year'));
		$curl_url=$curl_url=$sourceUrl.'v2/players/squad/'.$teamkey.'/'.$prev_year;
		if($api_type==1)
			$curl_url=$sourceUrl.'v2/players/squad/'.$teamkey.'/'.$year;
		else if($api_type==2)
			$curl_url=$sourceUrl.'v2/players/squad/'.$teamkey.'/'.$year."-".$next_year;
		else if($api_type==3)
			$curl_url=$sourceUrl.'v2/players/squad/'.$teamkey.'/'.$prev_year."-".$year;
		curl_setopt_array($curl, [
			CURLOPT_URL => $curl_url, 
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10, 
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => $header_parameters,
		]);

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			//echo "<br/><br/><br/><br/><br/><br/>".$response."<br/><br/><br/><br/><br/>";
			$response=json_decode($response, true);
			return $response['api']['players'];
		}
    } 
	  
	public static function getSquads($matchkey) {
		$match=static::getMatchDetails($matchkey);
		$team1=$match['homeTeam']['team_id'];
		$team2=$match['awayTeam']['team_id'];
		$team1IDObject = DB::table('teams')->where('team_key', $team1)->first();
		$team1ID = $team1IDObject->id; 
		$team2IDObject = DB::table('teams')->where('team_key', $team2)->first();
		$team2ID = $team2IDObject->id;
		static::get_whole_squad($matchkey,$team1,$team1ID);
		static::get_whole_squad($matchkey,$team2,$team2ID);
		if($match['players']==NULL){
			$tournament = static::get_tournament_matches($match['league_id']);
			$tournaments = $tournament['api']['fixtures'];
			$team1last_match=0;
			$team1last_match_time=0;
			foreach ($tournaments as $key => $value) {
				if($value['status']=='Match Finished' && ($value['homeTeam']['team_id']==$team1 || $value['awayTeam']['team_id']==$team1)) {
					if($team1last_match_time<$value['event_timestamp'])
					{
						$team1last_match_time=$value['event_timestamp'];
						$team1last_match=$value['fixture_id'];
					}
				}
			}
			$team2last_match=0;
			$team2last_match_time=0;
			$value=null;
			foreach ($tournaments as $key => $value) {
				if($value['status']=='Match Finished' && ($value['homeTeam']['team_id']==$team2 || $value['awayTeam']['team_id']==$team2)) {
					//echo $value['fixture_id']."---"; 
					if($team2last_match_time<$value['event_timestamp']) 
					{
						$team2last_match_time=$value['event_timestamp'];
						$team2last_match=$value['fixture_id'];
					}
				}
			}
			//echo $team1last_match."<-->".$team2last_match;
			if($team1last_match!=0 && $team2last_match!=0)
			{
				//echo $team2last_match;
				$last_match1=static::getMatchDetails($team1last_match);
				$last_match2=static::getMatchDetails($team2last_match);
				if(isset($last_match1['players']) && !empty($last_match1['players'])){
					foreach ($last_match1['players'] as $key1 => $value1) {  
						if(!empty($value1['player_id']) && !empty($value1['position']) && !empty($value1['player_name']) && !empty($value1['team_id']) && $value1['team_id']==$team1){
							$player_key = $value1['player_id'];
							$position = $value1['position'];
							$name = $value1['player_name'];
							$role="";
							if($position=="G")
								$role="goalkeeper";
							else if($position=="F")
								$role="striker";
							else if($position=="D") 
								$role="defender";
							else
								$role="midfielder";
							$find_player = DB::table('players')->where('player_key', $value1['player_id'])->where('team', $team1ID)->first();
							if(empty($find_player)) {
								$player_insert = array();
								$player_insert['player_name'] = $value1['player_name'];
								$player_insert['player_key'] = $value1['player_id'];
								$player_insert['sport_type'] = 2;
								$player_insert['team'] = $team1ID;
								$player_insert['credit'] = 8.5;
								$player_insert['role'] = $role;
								$player_id = DB::table('players')->insertGetId($player_insert);
							} else {
								$player_insert = array();
								$player_insert['player_name'] = $find_player->player_name;
								$player_insert['player_key'] = $value1['player_id'];
								$player_insert['sport_type'] = 2;
								$player_insert['team'] = $team1ID;
								$player_insert['role'] = $role;
								//DB::table('players')->where('id',$find_player->id)->update($player_insert);
								$player_id = $find_player->id;
								$player_insert['credit'] = $find_player->credit;
							}
							$findp1 = DB::table('player_details')->where('player_key',$value1['player_id'])->first();
							if(empty($findp1)){
								$plaerdetailsdata['fullname'] = $value1['player_name'];
								$plaerdetailsdata['player_key'] = $value1['player_id'];
								$plaerdetailsdata['sport_type'] = 2;
								DB::table('player_details')->insert($plaerdetailsdata);
							} 
							/*else{
								$plaerdetailsdata['fullname'] = $value1['player_name'];
								DB::table('player_details')->where('player_key',$value1['player_id'])->where('sport_type',2)->update($plaerdetailsdata);
							}*/
							$find_match_player = DB::table('match_players')->where('playerid', $player_id)->where('matchkey', $matchkey)->where('sport_type',2)->first();
							if(empty($find_match_player)) {
								$matchplayerdata['matchkey'] = $matchkey;
								$matchplayerdata['playerid'] = $player_id;
								$matchplayerdata['role'] = $player_insert['role'];
								$matchplayerdata['name'] = $player_insert['player_name'];
								$matchplayerdata['credit'] = $player_insert['credit'];
								$matchplayerdata['player_key'] = $player_key;
								$matchplayerdata['sport_type'] = 2;
								DB::table('match_players')->insert($matchplayerdata);
							} /*else {
								$matchplayerdata = [];
								$matchplayerdata['name'] = $player_insert['player_name'];
								DB::table('match_players')->where('id',$find_match_player->id)->where('sport_type',2)->update($matchplayerdata);
							}*/
						}
					} 
				}
				if(isset($last_match1['lineups']) && !empty($last_match1['lineups'])){
					foreach ($last_match1['lineups'] as $key3 => $value1) {   
						//echo $key3;
						foreach ($last_match1['lineups'][$key3]['substitutes'] as $key1 => $value1) {
							if(!empty($value1['player_id']) && !empty($value1['pos']) && !empty($value1['player']) && !empty($value1['team_id']) && $value1['team_id']==$team1){
								$player_key = $value1['player_id'];
								$position = $value1['pos'];
								$name = $value1['player'];
								$role="";
								if($position=="G")
									$role="goalkeeper";
								else if($position=="F")
									$role="striker";
								else if($position=="D") 
									$role="defender";
								else
									$role="midfielder";
								$find_player = DB::table('players')->where('player_key', $value1['player_id'])->where('team', $team1ID)->first();
								if(empty($find_player)) {
									$player_insert = array();
									$player_insert['player_name'] = $value1['player'];
									$player_insert['player_key'] = $value1['player_id'];
									$player_insert['sport_type'] = 2;
									$player_insert['team'] = $team1ID;
									$player_insert['credit'] = 8.5;
									$player_insert['role'] = $role;
									$player_id = DB::table('players')->insertGetId($player_insert);
								} else {
									$player_insert = array();
									$player_insert['player_name'] = $find_player->player_name;
									$player_insert['player_key'] = $value1['player_id'];
									$player_insert['sport_type'] = 2;
									$player_insert['team'] = $team1ID;
									$player_insert['role'] = $role;
									//DB::table('players')->where('id',$find_player->id)->update($player_insert);
									$player_id = $find_player->id;
									$player_insert['credit'] = $find_player->credit;
								}
								$findp1 = DB::table('player_details')->where('player_key',$value1['player_id'])->first();
								if(empty($findp1)){
									$plaerdetailsdata['fullname'] = $value1['player'];
									$plaerdetailsdata['player_key'] = $value1['player_id'];
									$plaerdetailsdata['sport_type'] = 2;
									DB::table('player_details')->insert($plaerdetailsdata);
								} 
								/*else{
									$plaerdetailsdata['fullname'] = $value1['player'];
									DB::table('player_details')->where('player_key',$value1['player_id'])->where('sport_type',2)->update($plaerdetailsdata);
								}*/
								$find_match_player = DB::table('match_players')->where('playerid', $player_id)->where('matchkey', $matchkey)->where('sport_type',2)->first();
								if(empty($find_match_player)) {
									$matchplayerdata['matchkey'] = $matchkey;
									$matchplayerdata['playerid'] = $player_id;
									$matchplayerdata['role'] = $player_insert['role'];
									$matchplayerdata['name'] = $player_insert['player_name'];
									$matchplayerdata['credit'] = $player_insert['credit'];
									$matchplayerdata['player_key'] = $player_key;
									$matchplayerdata['sport_type'] = 2;
									DB::table('match_players')->insert($matchplayerdata);
								} /*else {
									$matchplayerdata = [];
									$matchplayerdata['name'] = $player_insert['player_name'];
									DB::table('match_players')->where('id',$find_match_player->id)->where('sport_type',2)->update($matchplayerdata);
								}*/
							}
						}
					}
				}
				if(isset($last_match2['players']) && !empty($last_match2['players'])){
					foreach ($last_match2['players'] as $key2 => $value2) { 
						if(!empty($value2['player_id']) && !empty($value2['position']) && !empty($value2['player_name']) && !empty($value2['team_id']) && $value2['team_id']==$team2){
							$player_key = $value2['player_id'];
							$position = $value2['position'];
							$name = $value2['player_name'];
							$role="";
							if($position=="G")
								$role="goalkeeper";
							else if($position=="F")
								$role="striker";
							else if($position=="D") 
								$role="defender";
							else
								$role="midfielder";
							$find_player = DB::table('players')->where('player_key', $value2['player_id'])->where('team', $team2ID)->first();
							if(empty($find_player)) { 
								$player_insert = array();
								$player_insert['player_name'] = $value2['player_name'];
								$player_insert['player_key'] = $value2['player_id'];
								$player_insert['sport_type'] = 2;
								$player_insert['team'] = $team2ID;
								$player_insert['credit'] = 8.5;
								$player_insert['role'] = $role;
								$player_id = DB::table('players')->insertGetId($player_insert);
							} else {
								$player_insert = array();
								$player_insert['player_name'] = $find_player->player_name;
								$player_insert['player_key'] = $value2['player_id'];
								$player_insert['sport_type'] = 2;
								$player_insert['team'] = $team2ID;
								$player_insert['credit'] = $find_player->credit;
								$player_insert['role'] = $find_player->role;
								//DB::table('players')->where('id',$find_player->id)->where('sport_type',2)->update($player_insert);
								$player_id = $find_player->id;
								$player_insert['credit'] = $find_player->credit;
							}
							$findp1 = DB::table('player_details')->where('player_key',$value2['player_id'])->first();
							if(empty($findp1)){
								$plaerdetailsdata['fullname'] = $value2['player_name'];
								$plaerdetailsdata['player_key'] = $value2['player_id'];
								$plaerdetailsdata['sport_type'] = 2;
								DB::table('player_details')->insert($plaerdetailsdata);
							}
							/*else{
								$plaerdetailsdata['fullname'] = $value2['player_name'];
								DB::table('player_details')->where('player_key',$value2['player_id'])->where('sport_type',2)->update($plaerdetailsdata);
							}*/
							$find_match_player = DB::table('match_players')->where('playerid', $player_id)->where('matchkey', $matchkey)->where('sport_type',2)->first();
							if(empty($find_match_player)) {
								$matchplayerdata['matchkey'] = $matchkey;
								$matchplayerdata['playerid'] = $player_id;
								$matchplayerdata['role'] = $player_insert['role'];
								$matchplayerdata['name'] = $player_insert['player_name'];
								$matchplayerdata['credit'] = $player_insert['credit'];
								$matchplayerdata['player_key'] = $player_key;
								$matchplayerdata['sport_type'] = 2;
								DB::table('match_players')->insert($matchplayerdata);
							} /*else {
								$matchplayerdata = [];
								$matchplayerdata['name'] = $player_insert['player_name']; 
								DB::table('match_players')->where('id',$find_match_player->id)->where('sport_type',2)->update($matchplayerdata);
							}*/
						}
					}
				}
				if(isset($last_match2['lineups']) && !empty($last_match2['lineups'])){
					foreach ($last_match2['lineups'] as $key4 => $value2) {  
						echo $key4;
						foreach ($last_match2['lineups'][$key4]['substitutes'] as $key2 => $value2) {
							if(!empty($value2['player_id']) && !empty($value2['pos']) && !empty($value2['player']) && !empty($value2['team_id']) && $value2['team_id']==$team2){
								//echo $value2['pos'];
								$player_key = $value2['player_id'];
								$position = $value2['pos'];
								$name = $value2['player'];
								$role="";
								if($position=="G")
									$role="goalkeeper";
								else if($position=="F")
									$role="striker";
								else if($position=="D") 
									$role="defender";
								else
									$role="midfielder";
								$find_player = DB::table('players')->where('player_key', $value2['player_id'])->where('team', $team2ID)->first();
								$player_insert = array(); 
								if(empty($find_player)) { 
									$player_insert['player_name'] = $value2['player'];
									$player_insert['player_key'] = $value2['player_id'];
									$player_insert['sport_type'] = 2;
									$player_insert['team'] = $team2ID;
									$player_insert['credit'] = 8.5;
									$player_insert['role'] = $role;
									$player_id = DB::table('players')->insertGetId($player_insert);
								} else {
									$player_insert['player_name'] = $find_player->player_name;
									$player_insert['player_key'] = $value2['player_id'];
									$player_insert['sport_type'] = 2;
									$player_insert['team'] = $team2ID;
									$player_insert['credit'] = $find_player->credit;
									$player_insert['role'] = $find_player->role;
									//DB::table('players')->where('id',$find_player->id)->where('sport_type',2)->update($player_insert);
									$player_id = $find_player->id;
									$player_insert['credit'] = $find_player->credit;
								}
								$findp1 = DB::table('player_details')->where('player_key',$value2['player_id'])->first();
								if(empty($findp1)){
									$plaerdetailsdata['fullname'] = $value2['player'];
									$plaerdetailsdata['player_key'] = $value2['player_id'];
									$plaerdetailsdata['sport_type'] = 2;
									DB::table('player_details')->insert($plaerdetailsdata);
								}
								/*else{
									$plaerdetailsdata['fullname'] = $value2['player'];
									DB::table('player_details')->where('player_key',$value2['player_id'])->where('sport_type',2)->update($plaerdetailsdata);
								}*/
								$find_match_player = DB::table('match_players')->where('playerid', $player_id)->where('matchkey', $matchkey)->where('sport_type',2)->first();
								if(empty($find_match_player)) {
									$matchplayerdata['matchkey'] = $matchkey;
									$matchplayerdata['playerid'] = $player_id;
									$matchplayerdata['role'] = $player_insert['role'];
									$matchplayerdata['name'] = $player_insert['player_name'];
									$matchplayerdata['credit'] = $player_insert['credit'];
									$matchplayerdata['player_key'] = $player_key;
									$matchplayerdata['sport_type'] = 2;
									DB::table('match_players')->insert($matchplayerdata);
								} /*else {
									$matchplayerdata = [];
									$matchplayerdata['name'] = $player_insert['player_name']; 
									DB::table('match_players')->where('id',$find_match_player->id)->where('sport_type',2)->update($matchplayerdata);
								}*/
							}
						}
					}
				}
			}
		}
	}
	
	public static function get_whole_squad($matchkey,$team,$team1ID){
		$api_type=1;
		while($api_type<5){
			$players=static::getSquadDetails($team,$api_type);
			if(isset($players) && is_array($players) && count($players)>0)
			{
				foreach ($players as $key1 => $value1) { 
					if(!empty($value1['player_id']) && !empty($value1['position']) && !empty($value1['player_name'])){
						$player_key = $value1['player_id'];
						$position = $value1['position'];
						$name = $value1['player_name'];
						$role="";
						if($position=="G" or $position=="Goalkeeper")
							$role="goalkeeper";
						else if($position=="F" or $position=="Attacker")
							$role="striker";
						else if($position=="D" or $position=="Defender") 
							$role="defender";
						else
							$role="midfielder";
						$find_player = DB::table('players')->where('player_key', $value1['player_id'])->where('team', $team1ID)->first();
						if(empty($find_player)) {
							$player_insert = array();
							$player_insert['player_name'] = $value1['player_name'];
							$player_insert['player_key'] = $value1['player_id'];
							$player_insert['sport_type'] = 2;
							$player_insert['team'] = $team1ID;
							$player_insert['credit'] = 8.5;
							$player_insert['role'] = $role;
							$player_id = DB::table('players')->insertGetId($player_insert);
						} else {
							$player_insert = array();
							$player_insert['player_name'] = $find_player->player_name;
							$player_insert['player_key'] = $value1['player_id'];
							$player_insert['sport_type'] = 2;
							$player_insert['team'] = $team1ID;
							$player_insert['role'] = $role;
							//DB::table('players')->where('id',$find_player->id)->update($player_insert);
							$player_id = $find_player->id;
							$player_insert['credit'] = $find_player->credit;
						}
						$findp1 = DB::table('player_details')->where('player_key',$value1['player_id'])->first();
						if(empty($findp1)){
							$plaerdetailsdata['fullname'] = $value1['player_name'];
							$plaerdetailsdata['player_key'] = $value1['player_id'];
							$plaerdetailsdata['sport_type'] = 2;
							DB::table('player_details')->insert($plaerdetailsdata);
						} 
						/*else{
							$plaerdetailsdata['fullname'] = $value1['player_name'];
							DB::table('player_details')->where('player_key',$value1['player_id'])->where('sport_type',2)->update($plaerdetailsdata);
						}*/
						$find_match_player = DB::table('match_players')->where('playerid', $player_id)->where('matchkey', $matchkey)->where('sport_type',2)->first();
						if(empty($find_match_player)) {
							$matchplayerdata['matchkey'] = $matchkey;
							$matchplayerdata['playerid'] = $player_id;
							$matchplayerdata['role'] = $player_insert['role'];
							$matchplayerdata['name'] = $player_insert['player_name'];
							$matchplayerdata['credit'] = $player_insert['credit'];
							$matchplayerdata['player_key'] = $player_key;
							$matchplayerdata['sport_type'] = 2;
							DB::table('match_players')->insert($matchplayerdata);
						} /*else {
							$matchplayerdata = [];
							$matchplayerdata['name'] = $player_insert['player_name'];
							DB::table('match_players')->where('id',$find_match_player->id)->where('sport_type',2)->update($matchplayerdata);
						}*/
					}
				}
				break;
			}
			else{
				$api_type++;
			}
		}
	}
	
	public static function importAfterLineup($match_key,$match) { 
		$team1=$match['homeTeam']['team_id'];
		$team2=$match['awayTeam']['team_id'];
		$team1IDObject = DB::table('teams')->where('team_key', $team1)->first();
		$team1ID = $team1IDObject->id; 
		$team2IDObject = DB::table('teams')->where('team_key', $team2)->first();
		$team2ID = $team2IDObject->id;
		foreach ($match['lineups'] as $key3 => $value3) {  
			//echo $key3;
			if(isset($match['lineups'][$key3]['startXI']) && !empty($match['lineups'][$key3]['startXI']))
			{
				foreach ($match['lineups'][$key3]['startXI'] as $key1 => $value1) {
					if(!empty($value1['player_id']) && !empty($value1['pos']) && !empty($value1['player']) && !empty($value1['team_id']) && $value1['team_id']==$team1){
						$player_key = $value1['player_id'];
						$position = $value1['pos'];
						$name = $value1['player'];
						$role="";
						if($position=="G")
							$role="goalkeeper";
						else if($position=="F")
							$role="striker";
						else if($position=="D") 
							$role="defender";
						else
							$role="midfielder";
						$find_player = DB::table('players')->where('player_key', $value1['player_id'])->where('team', $team1ID)->first();
						if(empty($find_player)) {
							$player_insert = array();
							$player_insert['player_name'] = $value1['player'];
							$player_insert['player_key'] = $value1['player_id'];
							$player_insert['sport_type'] = 2;
							$player_insert['team'] = $team1ID;
							$player_insert['credit'] = 8.5;
							$player_insert['role'] = $role;
							$player_id = DB::table('players')->insertGetId($player_insert);
						} else {
							$player_insert = array();
							$player_insert['player_name'] = $find_player->player_name;
							$player_insert['player_key'] = $value1['player_id'];
							$player_insert['sport_type'] = 2;
							$player_insert['team'] = $team1ID;
							$player_insert['role'] = $role;
							//DB::table('players')->where('id',$find_player->id)->update($player_insert);
							$player_id = $find_player->id;
							$player_insert['credit'] = $find_player->credit;
						}
						$findp1 = DB::table('player_details')->where('player_key',$value1['player_id'])->first();
						if(empty($findp1)){
							$plaerdetailsdata['fullname'] = $value1['player'];
							$plaerdetailsdata['player_key'] = $value1['player_id'];
							$plaerdetailsdata['sport_type'] = 2;
							DB::table('player_details')->insert($plaerdetailsdata);
						} 
						/*else{
							$plaerdetailsdata['fullname'] = $value1['player'];
							DB::table('player_details')->where('player_key',$value1['player_id'])->where('sport_type',2)->update($plaerdetailsdata);
						}*/
						$find_match_player = DB::table('match_players')->where('playerid', $player_id)->where('matchkey', $match_key)->where('sport_type',2)->first();
						if(empty($find_match_player)) {
							$matchplayerdata['matchkey'] = $match_key;
							$matchplayerdata['playerid'] = $player_id;
							$matchplayerdata['role'] = $player_insert['role'];
							$matchplayerdata['name'] = $player_insert['player_name'];
							$matchplayerdata['credit'] = $player_insert['credit'];
							$matchplayerdata['player_key'] = $player_key;
							$matchplayerdata['sport_type'] = 2;
							DB::table('match_players')->insert($matchplayerdata);
						}
					}
				}
			}
		}
		foreach ($match['lineups'] as $key4 => $value4) {  
			//echo $key4;
			if(isset($match['lineups'][$key4]['startXI']) && !empty($match['lineups'][$key4]['startXI']))
			{
				foreach ($match['lineups'][$key4]['startXI'] as $key2 => $value2) {
					if(!empty($value2['player_id']) && !empty($value2['pos']) && !empty($value2['player']) && !empty($value2['team_id']) && $value2['team_id']==$team2){
						$player_key = $value2['player_id'];
						$position = $value2['pos'];
						$name = $value2['player'];
						$role="";
						if($position=="G")
							$role="goalkeeper";
						else if($position=="F")
							$role="striker";
						else if($position=="D") 
							$role="defender";
						else
							$role="midfielder";
						$find_player = DB::table('players')->where('player_key', $value2['player_id'])->where('team', $team2ID)->first();
						if(empty($find_player)) { 
							$player_insert = array();
							$player_insert['player_name'] = $value2['player'];
							$player_insert['player_key'] = $value2['player_id'];
							$player_insert['sport_type'] = 2;
							$player_insert['team'] = $team2ID;
							$player_insert['credit'] = 8.5;
							$player_insert['role'] = $role;
							$player_id = DB::table('players')->insertGetId($player_insert);
						} else {
							$player_insert = array();
							$player_insert['player_name'] = $find_player->player_name;
							$player_insert['player_key'] = $value2['player_id'];
							$player_insert['sport_type'] = 2;
							$player_insert['team'] = $team2ID;
							$player_insert['credit'] = $find_player->credit;
							$player_insert['role'] = $find_player->role;
							//DB::table('players')->where('id',$find_player->id)->where('sport_type',2)->update($player_insert);
							$player_id = $find_player->id;
							$player_insert['credit'] = $find_player->credit;
						}
						$findp1 = DB::table('player_details')->where('player_key',$value2['player_id'])->first();
						if(empty($findp1)){
							$plaerdetailsdata['fullname'] = $value2['player'];
							$plaerdetailsdata['player_key'] = $value2['player_id'];
							$plaerdetailsdata['sport_type'] = 2;
							DB::table('player_details')->insert($plaerdetailsdata);
						}
						/*else{
							$plaerdetailsdata['fullname'] = $value2['player'];
							DB::table('player_details')->where('player_key',$value2['player_id'])->where('sport_type',2)->update($plaerdetailsdata);
						}*/
						$find_match_player = DB::table('match_players')->where('playerid', $player_id)->where('matchkey', $match_key)->where('sport_type',2)->first();
						if(empty($find_match_player)) {
							$matchplayerdata['matchkey'] = $match_key;
							$matchplayerdata['playerid'] = $player_id;
							$matchplayerdata['role'] = $player_insert['role'];
							$matchplayerdata['name'] = $player_insert['player_name'];
							$matchplayerdata['credit'] = $player_insert['credit'];
							$matchplayerdata['player_key'] = $player_key;
							$matchplayerdata['sport_type'] = 2;
							DB::table('match_players')->insert($matchplayerdata);
						} /*else {
							$matchplayerdata = [];
							$matchplayerdata['name'] = $player_insert['player_name']; 
							DB::table('match_players')->where('id',$find_match_player->id)->where('sport_type',2)->update($matchplayerdata);
						}*/
					}
				}
			}
		}
	}
	
	public static function get_playing_11_football($match_key) { 
		$match=static::getMatchDetails($match_key);
		date_default_timezone_set('Asia/Kolkata');
		$timeNow = date('Y-m-d H:i:s');
		$findmatch = DB::table('list_matches')->where('matchkey',$match_key)->where('only_admin',0)->first();
		if(!empty($findmatch)){
			$findseries = DB::table('series')->where('id',$findmatch->series)->first();
			if(!empty($findseries)){
				$seriesName=str_replace($findseries->name,"? ","");
				$pos = strpos($findseries->name,"|");
				$seriesName= $pos !== FALSE ? substr($findseries->name, $pos + strlen("|"), strlen($findseries->name)) : "";
				$findnotifications = DB::table('bulk_notifications')->where('matchkey',$match_key)->where('isSent',0)->get();
				if(!empty($findnotifications)){
					foreach($findnotifications as $notification){
						if(!empty($notification->minutes)){
							$update_data = array(
								"isSent" => 1 
							);
							if($notification->preToss==0){
								$timeBefore1 = date('Y-m-d H:i:s',strtotime('-'.$notification->minutes.' minutes', strtotime($findmatch->start_date)));
								if($timeBefore1<=$timeNow)
								{
									DB::table('bulk_notifications')->where('matchkey', $match_key)->where('id',$notification->id)->update($update_data);
									Helpers::sendnotificationBulk($notification->display." minutes to go ⏰ ".$findmatch->short_name,"⚽⚽".$seriesName." ⚽⚽\n".$findmatch->name."\nMatch Time ⏰: ".date('h:i A',strtotime($findmatch->start_date)),'FANADDA2');
								} 
							} 
							else if($notification->preToss==1 && $findmatch->PreTossAvailable==1 && !empty($findmatch->PreTossClosingTime)){
								$timeBefore1 = date('Y-m-d H:i:s',strtotime('-'.$notification->minutes.' minutes', strtotime($findmatch->PreTossClosingTime)));
								if($timeBefore1<=$timeNow)
								{
									DB::table('bulk_notifications')->where('matchkey', $match_key)->where('id',$notification->id)->update($update_data);
									Helpers::sendnotificationBulk($notification->display." minutes to go ⏰ PreToss Reminder",$findmatch->short_name."\n⚽⚽".$seriesName." ⚽⚽\nPreToss Deadline ⏰: ".date('h:i A',strtotime($findmatch->PreTossClosingTime)),'FANADDA2');
								}
							}
						}
					}
				}
			}
		} 
		if($match['lineups']!=NULL){
			static::importAfterLineup($match_key,$match);
			$startXI=array();
			foreach ($match['lineups'] as $key4 => $value4) {  
				//echo $key4;
				if(isset($match['lineups'][$key4]['startXI']) && !empty($match['lineups'][$key4]['startXI']))
				{
					foreach ($match['lineups'][$key4]['startXI'] as $key2 => $value2) {
						if(!empty($value2['player_id'])){
							$player_details = DB::table('players')->join('match_players','players.id','=','match_players.playerid')->where('players.player_key',$value2['player_id'])->where('match_players.sport_type',2)->where('match_players.matchkey', $match_key)->select('players.id')->first();
							if(!empty($player_details)) {
								array_push($startXI,$player_details->id); 
							}
						}
					} 
				}
			} 
			//echo count($startXI);  
			if(count($startXI)){
				$insert_data = array(
					"match_key" => $match_key,
					"player_ids" => serialize($startXI),
				);
				$findplayerex = DB::table('match_playing11')->where('match_key',$match_key)->where('type', 'main')->first();
				if(!empty($findplayerex)){
					if($findplayerex->isLocked==0){
						DB::table('match_playing11')->where('match_key', $match_key)->where('type', 'main')->update($insert_data);
					}
				}else{
					DB::table('match_playing11')->insert($insert_data);
					DB::table('list_matches')->where('matchkey',$match_key)->where('only_admin',0)->first();
					$timeBefore = date('Y-m-d H:i:s',strtotime('-1 minutes', strtotime($findmatch->start_date)));
					if(!empty($findmatch) && ($timeBefore>=$timeNow)) {
						Helpers::sendnotificationBulk("Lineup Out 📢 ".$findmatch->short_name,"⚽⚽".$seriesName." ⚽⚽\n".$findmatch->name."\nMatch Time ⏰: ".date('h:i A',strtotime($findmatch->start_date)),'FANADDA2'); 
					}
				}
			}
		}
	}
	
	public static function getFootballScore($match_key) {
		$match=static::getMatchDetails($match_key);
		if($match['status']=='Match Finished'){
			$getmtdatastatus['status'] = 'completed';
			$getmtdatastatus['final_status'] = 'IsReviewed';
			DB::table('list_matches')->where('matchkey',$match_key)->update($getmtdatastatus);
		}
		$findmatch = DB::table('list_matches')->where('matchkey',$match_key)->first();
		if($match['players']!=NULL){
			$i=0;
			foreach ($match['players'] as $key1 => $value1) {
				$datasvv['player_key'] = $value1['player_id'];
				$datasvv['starting11'] = 1;
				$datasvv['match_key'] = $match_key;
				$datasvv['sport_type'] = 2;
				$datasvv['innings'] = 1;
				$player = DB::table('match_players')->join('players', 'players.id', '=', 'match_players.playerid')->where('matchkey', $match_key)->where('match_players.sport_type', 2)->where('players.sport_type', 2)->select('match_players.*', 'players.player_key', 'players.role as playerrole' , 'players.team')->where('players.player_key', $value1['player_id'])->first();
				if (!empty($player)) {
					$datasvv['player_id'] = $player->playerid;
					// $findplayerex = DB::table('result_matches')->where('player_key', $player_id)->where('match_key', $match_key)->where('innings', 1)->select('id')->first();

					if (!empty($player)) {
						//echo $i." ".$value1['player_id']."}{";
						$goalconceded = $value1['goals']['conceded'] ? $value1['goals']['conceded'] : 0;
						$cleansheet = 0;
						//cleansheet score system 
						if($match['status']=='Match Finished' && !empty($findmatch)){
							if($match['goalsHomeTeam']==0 && $player->team==$findmatch->team2)
							{
								if($player->playerrole!='striker' && $player->playerrole!='midfielder' && $value1['minutes_played']>54)
								{
									$cleansheet = 1;
								}
							} 
							else if($match['goalsAwayTeam']==0 && $player->team==$findmatch->team1){
								if($player->playerrole!='striker' && $player->playerrole!='midfielder' && $value1['minutes_played']>54)
								{
									$cleansheet = 1;
								}
							}
						}
						//conceed score system
						if($match['goalsHomeTeam']>0 && $player->team==$findmatch->team2)
						{
							if($player->playerrole=='defender')
							{
								$goalconceded = $match['goalsHomeTeam'];
							}
						}
						else if($match['goalsAwayTeam']>0 && $player->team==$findmatch->team1){
							if($player->playerrole=='defender')
							{
								$goalconceded = $match['goalsAwayTeam'];
							}
						}
						$playingTime = $value1['minutes_played'] ? $value1['minutes_played'] : 0;
						$goals = $value1['goals']['total'] ? $value1['goals']['total'] : 0;
						$assist = $value1['goals']['assists'] ? $value1['goals']['assists'] : 0;
						$passes = $value1['passes']['accuracy'] ? $value1['passes']['accuracy'] : 0;
						$shots = $value1['shots']['on'] ? $value1['shots']['on'] : 0;
						$saveShots = $value1['goals']['saves'] ? $value1['goals']['saves'] : 0;
						$penaltySave = $value1['penalty']['saved'] ? $value1['penalty']['saved'] : 0;
						$tackles = $value1['tackles']['total'] ? $value1['tackles']['total'] : 0;
						$block = 0; 
						$interceptions = $value1['tackles']['interceptions'] ? $value1['tackles']['interceptions'] : 0;
						$totalclearance = 0;
						$substitute = $value1['substitute']=="True" ? 1 : 0;
						$chanceCreated = $value1['passes']['key'] ? $value1['passes']['key'] : 0;
						$yellowCard = $value1['cards']['yellow'] ? $value1['cards']['yellow'] : 0;
						$redCard = $value1['cards']['red'] ? $value1['cards']['red'] : 0;
						$ownGoal = 0;
						$penaltyMiss = $value1['penalty']['missed'] ? $value1['penalty']['missed'] : 0;
						if($playingTime>0) {
							$datasv[$i]['minutesplayed'] = $playingTime;
							$datasv[$i]['goals'] = $goals;
							$datasv[$i]['assist'] = $assist;
							$datasv[$i]['totalpass'] = $passes;
							$datasv[$i]['shotsontarget'] = $shots;
							$datasv[$i]['cleansheet'] = $cleansheet;
							$datasv[$i]['block'] = $block;
							$datasv[$i]['shotsblocked'] = $saveShots;
							$datasv[$i]['penaltysave'] = $penaltySave;
							$datasv[$i]['tacklesuccess'] = $tackles;
							if($redCard>0)
								$yellowCard=0;
							$datasv[$i]['yellowcard'] = $yellowCard;
							$datasv[$i]['redcard'] = $redCard; 
							$datasv[$i]['substitute'] = $substitute;
							$datasv[$i]['playing11'] = 1-$substitute;
							$datasv[$i]['owngoals'] = $ownGoal;
							$datasv[$i]['penaltymiss'] = $penaltyMiss;
							$datasv[$i]['goalconceded'] = $goalconceded;
							$datasv[$i]['interceptionsWon'] = $interceptions;
							$datasv[$i]['chanceCreated'] = $chanceCreated;
							$datasv[$i]['totalclearance'] = $totalclearance;
							$datasv[$i]['sport_type'] = 2;
							$datasv[$i]['match_key'] = $match_key;
							$datasv[$i]['player_key'] = $value1['player_id'];
							$datasv[$i]['player_id'] = $player->playerid;
							$datasv[$i]['total_points'] = 0;//($playingTime + $goals + $assist + $passes + $shots + $cleansheet + $saveShots + $penaltySave + $tackles + $yellowCard + $redCard + $ownGoal + $penaltyMiss + $goalconceded);
							$datasv[$i]['starting11'] = 1;
							$findplayerex = DB::table('result_matches')->where('player_key',$value1['player_id'])->where('match_key',$match_key)->select('id','isLocked')->first();
							if(!empty($findplayerex)){
								if($findplayerex->isLocked==0){
									DB::table('result_matches')->where('id',$findplayerex->id)->update($datasv[$i]);
								}
							}else{
								DB::table('result_matches')->insert($datasv[$i]);
							}
						}
						$i++;
					} 
				} 
			}
			return true;
		}
		return false;
	}
}
