<?php
	namespace App\Http\Controllers;
	use DB;
	use Session;
	use bcrypt;
	use Config;
	use Redirect;
	use Helpers;
	use Hash;
	use PDF;
	use Mpdf;
	use Cache;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use App\Http\Requests;
	use Carbon\Carbon;
	use Illuminate\Support\Facades\Validator;
	use Illuminate\Support\Facades\Input;
	use App\Http\Controllers\CricketapiController;
	class LeaugesapiController extends Controller {
		public function geturl(){
			return 'https://test.fanadda.com/bbadmin/';
		}
		public function accessrules(){
			date_default_timezone_set("Asia/Kolkata");
			header('Access-Control-Allow-Origin: *'); 
			header("Access-Control-Allow-Credentials: true");
			header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			header('Access-Control-Max-Age: 1000');
			header('Access-Control-Allow-Headers: Authorization');
			header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
		}
		public function matchalljoinedusers(){
			$findallchallenges = DB::table('matchchallenges')->get();
			if(!empty($findallchallenges)){
				foreach($findallchallenges as $chl){
					$countjoinleauges  = 0;
					$findjoinedleauges = DB::table('joinedleauges')->where('challengeid',$chl->id)->get();
					$countjoinleauges = count($findjoinedleauges);
					$data['joinedusers'] = $countjoinleauges;
					DB::table('matchchallenges');
					DB::table('matchchallenges')->where('id',$chl->id)->update($data);
				}
			}
		}
		public function seealltestimonials(){
			$this->accessrules();
			$geturl = $this->geturl();
			$findalltestimonials = DB::table('testimonials')->orderByRaw('RAND()')->get();
			$Json = array();
			if(!empty($findalltestimonials)){
				$i=0;
				foreach($findalltestimonials as $testi){
					$Json[$i]['name'] = $testi->name;
					$Json[$i]['message'] = $testi->message;
					$Json[$i]['date'] = date('d M, Y',strtotime($testi->created_at));
					$Json[$i]['image'] = $geturl.'uploads/testimonials/'.$testi->image;
					$Json[$i]['status'] = 1;
					$i++;
				}
				
			}else{
				$Json[0]['status'] = 0;
			}
			echo json_encode($Json);
			die;
		}
		
		public function countNotification(){
		    $this->accessrules();
			$userid = $_GET['user_id'];
			$currentdate = date('Y-m-d');
			$countnotifications = DB::table('notifications')->where('seen',0)->where('userid',$userid)->count();
			$json['status'] = $countnotifications;
			echo json_encode(array($json)); die;
		}
		
		public function usernotifications(){
			$this->accessrules();
			$userid = $_GET['user_id'];
			$currentdate = date('Y-m-d');
			$todaynotifications = DB::table('notifications')->whereDate('notifications.created_at','=',$currentdate)->where('userid',$userid)->orderBy('id','DESC')->get();
			$countnotifications = DB::table('notifications')->where('seen',0)->where('userid',$userid)->count();
			
			$notiupdate['seen'] =1;
			DB::table('notifications')->where('userid',$userid)->update($notiupdate);
			
			$Json = array();
			if(!empty($todaynotifications)){
				$i=0;
				foreach($todaynotifications as $notify){
					$Json['today'][$i]['title'] = ucwords($notify->title);
					$Json['today'][$i]['status'] = 1;
					$i++;
				}
			}
			else{
				$Json['today'][0]['title'] = 'No Data Found';
			}
			$previousnotifications = DB::table('notifications')->whereDate('notifications.created_at','<',$currentdate)->orderBy('id','DESC')->where('userid',$userid)->limit(20)->get();
			if(!empty($previousnotifications)){
				$i=0;
				foreach($previousnotifications as $notify){
					$Json['previous'][$i]['title'] = ucwords($notify->title);
					$Json['previous'][$i]['status'] = 1;
					$i++;
				}
			}else{
				$Json['previous'][0]['title'] = 'No Data Found';
			}
			$Json['notescount'] = $countnotifications;
			echo json_encode($Json);
			die;
		}
		public function androidusernotifications(){
			$this->accessrules();
			$userid = $_GET['user_id'];
			$currentdate = date('Y-m-d');
			$todaynotifications = DB::table('notifications')->whereDate('notifications.created_at','=',$currentdate)->where('userid',$userid)->orderBy('id','DESC')->get();
			$countnotifications = DB::table('notifications')->where('seen',0)->where('userid',$userid)->count();
			$Json = array();
			if(!empty($todaynotifications)){
				$i=0;
				foreach($todaynotifications as $notify){
					$Json['today'][$i]['title'] = ucwords($notify->title);
					$Json['today'][$i]['status'] = 1;
					$Json['today'][$i]['date'] = $notify->created_at;
					$i++;
				}
			}
			else{
				$Json['today'][0]['title'] = 'No Data Found';
				$Json['today'][0]['status'] = '';
			}
			$previousnotifications = DB::table('notifications')->whereDate('notifications.created_at','<',$currentdate)->where('userid',$userid)->orderBy('id','DESC')->limit(20)->get();
			if(!empty($previousnotifications)){
				$i=0;
				foreach($previousnotifications as $notify){
					$Json['previous'][$i]['title'] = ucwords($notify->title);
					$Json['previous'][$i]['status'] = 1;
					$Json['previous'][$i]['date'] = $notify->created_at;
					$i++;
				}
			}else{
				$Json['previous'][0]['title'] = 'No Data Found';
				$Json['previous'][0]['status'] = '';
			}
			$Json['notescount'] = $countnotifications;
			echo json_encode(array($Json));
			die;
		}
		public function seennotifications(){
			$this->accessrules();
			$userid = $_GET['user_id'];
			$find = DB::table('notifications')->where('seen',0)->where('userid',$userid)->get();
			
			if(!empty($find)){
				foreach($find as $ff){
					DB::table('notifications');
					$data['seen'] = 1;
					DB::table('notifications')->where('userid',$userid)->update($data);
				}
			}
			$Json[0]['status'] = 1;
			echo json_encode($Json);
			die;
		}
		public function teamsjoin(){
			$this->accessrules();
			$challengeid = $_GET['challengeid'];
			$findjoinedteams = DB::table('joinedleauges')->where('challengeid',$challengeid)->join('registerusers','registerusers.id','=','matchchallenges.userid')->select('registerusers.team','registerusers.email','joinedleauges.*')->get();
			$Json=array();
			if(!empty($findjoinedteams)){
				$i=0;
				foreach($findjoinedteams as $jointeam){
					if($jointeam->team!=""){
						$Json[$i]['teamname'] = ucwords($jointeam->team);
					}else{
						$Json[$i]['teamname'] = $jointeam->email;
					}
					$Json[$i]['teamid'] = $jointeam->teamid;
					
					$i++;
				}
			}
			echo json_encode($Json);
			die;
		}
		public function usablebalance(){
			$this->accessrules();
			$challengeid = $_GET['challengeid'];
			$userid = $_GET['user_id'];
			$findchallengedetails = DB::table('matchchallenges')->where('id',$challengeid)->select('matchchallenges.entryfee','matchchallenges.maximum_user','matchchallenges.matchkey','matchchallenges.marathon','matchchallenges.is_private','matchchallenges.grand')->first();
			if(!empty($findchallengedetails)){
				$entryfee = $findchallengedetails->entryfee;
				$maximumuser = $findchallengedetails->maximum_user;
				$findwalletamount = DB::table('userbalances')->where('user_id',$userid)->first();
				$findbonus = $findwalletamount->bonus;
				$findbal = $findwalletamount->balance;
				$findwining = $findwalletamount->winning;
				$findtotalbalance = $findwalletamount->bonus+$findwalletamount->balance+$findwalletamount->winning;
				$findusablebalance = $findwalletamount->balance+$findwalletamount->winning;
				$usedbonus=0;
				
				if($findchallengedetails->is_private!=1){
					$findbonusexist = DB::table('leaugestransactions')->where('matchkey',$findchallengedetails->matchkey)->where('user_id',$userid)->select('bonus')->get();
					if(!empty($findbonusexist)){
						foreach($findbonusexist as $bonus){
							$usedbonus+= $bonus->bonus;
						}
					}
					
					if($findchallengedetails->maximum_user > 2){
						$usable_bonus = $entryfee*0.75;
						if($usable_bonus >= $findbonus){
							$findusablebalance += $findbonus;
						}else{
							$findusablebalance += $usable_bonus;
						}
					}
				}
				if($findchallengedetails->marathon==1){
					$findusablebalance = $findwalletamount->balance+$findwalletamount->winning+$findwalletamount->bonus;
				}
				//$Json[0]['usablebalance'] = round($findusablebalance,2);
				$Json[0]['usablebalance'] = round($findtotalbalance,2);
				$Json[0]['usertotalbalance'] = round($findtotalbalance,2);
				$Json[0]['marathon'] = $findchallengedetails->marathon;
			}else{
				$Json[0]['status'] = 'Invalid details';
			}
			echo json_encode($Json);
			die;
		}
		public function refercodechallenege(){
			$this->accessrules();
			$matchkey = $data['matchkey'] =  $_GET['matchkey'];
			$userid =  $data['userid'] =  $_GET['userid'];
			$challengeid =  $data['challengeid'] =  $_GET['challengeid'];
			$finjoinleauge = DB::table('joinedleauges')->where('joinedleauges.matchkey',$matchkey)->where('joinedleauges.userid',$userid)->where('challengeid',$challengeid)->select('joinedleauges.refercode','joinedleauges.id')->first();
			if(!empty($finjoinleauge)){
				$refercode = $finjoinleauge->refercode;
				$Json[0]['refercode'] = $refercode;
				$Json[0]['id'] = $finjoinleauge->id;
			}else{
				$Json[0]['refercode'] = 0;
				$Json[0]['id'] = 0;
			}
			echo json_encode($Json);
			die;
		}
		
		
		public function sendinvite(){
			$this->accessrules();
			$url = $this->geturl();
			$email = $_GET['email'];
			$invitetext = $_GET['invitetext'];
			$iniviteid = $_GET['id'];
			$findleauges = DB::table('joinedleauges')->join('matchchallenges','matchchallenges.id','=','joinedleauges.challengeid')->join('registerusers','registerusers.id','=','joinedleauges.userid')->where('joinedleauges.id',$iniviteid)->select('joinedleauges.refercode','registerusers.username','registerusers.email as useremail','matchchallenges.*')->first();
			if($findleauges->marathon==1){
				$findseries = DB::table('series')->where('id',$findleauges->series_id)->first();
				if(!empty($findseries)){
					if($email==$findleauges->useremail){
						$Json[0]['status'] = 0;
					}
					else{
						$explodeemails = explode(',',$email);
						if(!empty($explodeemails)){
							foreach($explodeemails as $emai){ 
								$content='<p>Hello, Your friend '.ucwords($findleauges->username).' sent you an invite to join the marathon challenge for series '.ucwords($findseries->name).'.</p>';
								$content.='<p>Your invitation code is <strong>'.$findleauges->refercode.'</strong></p>';
								$content.='<p><strong>Message:- </strong> '.$_GET['invitetext'].'</p>';
								$content.='<p>Enter this inviation code to join the marathon challenge.</p>';
								$content.='<p style="width:100%;float:left;"><a href="'.Config::get('constants.FRONT_PROJECT_URL').'" style="background:#3C7CC4;color:white;padding:10px;margin-bottom:10px;float:left;">Join Marathon Challenge</a></p>';
								$subject = 'New Invitation to join the marathon challenge.';
								$msg = Helpers::mailheader();
								$msg.= Helpers::mailbody($content);
								$msg.= Helpers::mailfooter();
								Helpers::mailsentFormat($emai,$subject,$msg);
							}
						}
						$Json[0]['status'] = 1;
					}
				}
			}
			else{
				if($email==$findleauges->useremail){
					$Json[0]['status'] = 0;
				}
				else{
					$explodeemails = explode(',',$email);
					if(!empty($explodeemails)){
						foreach($explodeemails as $emai){ 
							$content='<p>Hello, Your friend '.ucwords($findleauges->username).' sent you an invite to join the challenge.</p>';
							$content.='<p>Your invitation code is <strong>'.$findleauges->refercode.'</strong></p>';
							$content.='<p><strong>Message:- </strong> '.$_GET['invitetext'].'</p>';
							$content.='<p>Enter this inviation code to join the challenge.</p>';
							$content.='<p style="width:100%;float:left;"><a href="'.Config::get('constants.FRONT_PROJECT_URL').'" style="background:#3C7CC4;color:white;padding:10px;margin-bottom:10px;float:left;">Join Challenge</a></p>';
							$subject = 'New Invitation to join the challenge.';
							$msg = Helpers::mailheader();
							$msg.= Helpers::mailbody($content);
							$msg.= Helpers::mailfooter();
							Helpers::mailsentFormat($emai,$subject,$msg);
						}
					}
					$Json[0]['status'] = 1;
				}
			}
			echo json_encode($Json);
			die;
		}
		public function abouttoexpire(){
			date_default_timezone_set("Asia/Kolkata");
			$currentdate = date('Y-m-d');
			// $userid = '20720';
			$prevdate = date('Y-m-d',strtotime(Carbon::now()->subDays(45)));   //364 days
			// $prevdate = date('Y-m-d',strtotime(Carbon::now()->subDays(59)));
			$findallusers = DB::table('registerusers')->select('id')->get();
			foreach($findallusers as $fusers){ 
				$userid = $fusers->id;
				$balancebnuss = array();
				$balancebnuss = DB::table('transactions')->where('created','<',$prevdate)->where('userid',$userid)->orderBy('id','DESC')->select('bal_bonus_amt','userid','created')->first();
				if(!empty($balancebnuss)){
						$totalbonus = 0;
						$consumedbnus = 0;
						$expireamount = 0;
						$totalbonus = $balancebnuss->bal_bonus_amt;
						$findconsumedbonus = DB::table('transactions')->whereDate('created','>=',$prevdate)->whereDate('created','<',$currentdate)->where('userid',$userid)->orderBy('id','DESC')->select('cons_bonus','created')->get();
						if(!empty($findconsumedbonus)){
							foreach($findconsumedbonus as $bnfind){
								$consumedbnus+=$bnfind->cons_bonus;
							}
						}
						if($consumedbnus<$totalbonus){
							$expireamount = $totalbonus-$consumedbnus;
						}
						if($expireamount>0){
							$findcurrentbalance = DB::table('userbalances')->where('user_id',$userid)->first();
							$getbonus = $findcurrentbalance->bonus;
							if($getbonus>=$expireamount){
								$dataupdate['bonus'] = $getbonus-$expireamount;
								$nowbalance = $dataupdate['bonus']+$findcurrentbalance->balance+$findcurrentbalance->winning;
								// echo "<pre>";
								// print_r($dataupdate);
								DB::table('userbalances')->where('user_id',$userid)->update($dataupdate);
								
								//start entry in transaction table//
								$findlasttransactionid= DB::table('transactions')->orderBy('id','DESC')->select('id')->first();
								$tranid = 1;
								if(!empty($findlasttransactionid)){
									$tranid = $findlasttransactionid->id+1;
								}else{
									$tranid = 1;
								}
								$transactiondata['type'] = 'Bonus Expired';
								$transactiondata['amount'] = $expireamount;
								$transactiondata['total_available_amt'] = $nowbalance;
								$transactiondata['transaction_by'] = 'wallet';
								$transactiondata['userid'] = $userid;
								$transactiondata['paymentstatus'] = 'confirmed';
								$transactiondata['cons_bonus'] = $expireamount;
								$transactiondata['bal_bonus_amt'] = $dataupdate['bonus'];
								$transactiondata['bal_win_amt'] = $findcurrentbalance->winning;
								$transactiondata['bal_fund_amt'] = $findcurrentbalance->balance;
								$transactiondata['bal_fund_amt'] = $findcurrentbalance->balance;
								$transactiondata['transaction_id'] = 'BBF-EX-'.$tranid.'-'.$userid;
								// to enter in joined leauges table//
								$data['transaction_id'] = 'BBF-JL-'.$tranid.'-'.$userid;
								DB::table('transactions')->insert($transactiondata);
								//end entry in transaction table//
							
							} 
					}
				}
			}
			die;
		}
		
		function bestTeam(){
			$this->accessrules();
			$matchkey = $_GET['matchkey'];
			$players=array();
			$best = DB::table('joinedleauges')->join('finalresults','joinedleauges.id','=','finalresults.joinedid')->join('jointeam','jointeam.id','=','joinedleauges.teamid')->where('finalresults.matchkey',$matchkey)->orderBy('finalresults.points')->first();
			if(!empty($best)){
				$bestPlayers = explode(",",$best->players);
				
				$i =0;
				foreach($bestPlayers as $bp){
					$playerDetail = DB::table('players')->join('result_matches','result_matches.player_id','=','players.id')->where('result_matches.match_key',$matchkey)->where('players.id',$bp)->first();
					$players[$i]['player_name'] = $playerDetail->player_name;
					$players[$i]['role'] = $playerDetail->role;
					$players[$i]['image'] = $playerDetail->image;
					$players[$i]['points'] = $playerDetail->total_points;
					$i++;
				}
			}
			echo json_encode($players);
			die;
		}
		
		public function joinbycode(){
			$this->accessrules();
			$invitecode = $_GET['getcode'];	
			$matchkey = $_GET['matchkey'];
			$userid = $_GET['userid'];
			$findchallenge = DB::table('joinedleauges')->join('matchchallenges','matchchallenges.id','=','joinedleauges.challengeid')->where('joinedleauges.matchkey',$matchkey)->where('joinedleauges.refercode',$invitecode)->select('matchchallenges.status','matchchallenges.entryfee','matchchallenges.marathon','matchchallenges.series_id','joinedleauges.*')->first();
			if(!empty($findchallenge)){
					$joinedchll = DB::table('joinedleauges')->where('challengeid',$findchallenge->challengeid)->where('userid',$userid)->select('id')->first();
					if(!empty($joinedchll)){
						$Json[0]['message'] = 'already used';
						$Json[0]['marathon'] = 0;
					}
					else{
						if($findchallenge->status=='closed'){
							$Json[0]['message'] = 'Challenge closed';
							$Json[0]['challengeid']="";
							$Json[0]['entryfee']="";
							$Json[0]['marathon'] = 0;
						}
						else{
							$Json[0]['message'] = 'Challenge opened';
							$Json[0]['challengeid'] = $findchallenge->challengeid;
							$Json[0]['entryfee'] = $findchallenge->entryfee;
							$Json[0]['marathon'] = 0;
						}
					}
			}else{
				$Json[0]['message'] = 'invalid code';
			}
			echo json_encode($Json);
			die;
		}
		public function getteamtoshow(){
			$this->accessrules();
			$challenge = $data['challenge'] =  $_GET['challenge'];
			$userid =  $data['userid'] =  $_GET['userid'];
			if(isset($_GET['joinid'])){
				$joinid =  $data['joinid'] =  $_GET['joinid'];
				if(!empty($challenge)){
				$finduserteam = $team = DB::table('joinedleauges')->where('joinedleauges.challengeid',$challenge)->where('joinedleauges.userid',$userid)->where('joinedleauges.id',$joinid)->join('jointeam','jointeam.id','=','joinedleauges.teamid')->join('registerusers','registerusers.id','=','joinedleauges.userid')->select('jointeam.*','registerusers.email','registerusers.team as userteam')->first();
				}
			}
			else{
				$teamid =  $data['teamid'] =  $_GET['teamid'];
				$finduserteam = $team = DB::table('jointeam')->where('jointeam.id',$teamid)->join('registerusers','registerusers.id','=','jointeam.userid')->select('jointeam.*','registerusers.email','registerusers.team as userteam')->first();
			}
			
			$Json=array();
			$bowlers=array();
			$batsman=array();
			$keeper=array();
			$allrounder=array();
			if(!empty($finduserteam)){
				if($finduserteam->userteam!=""){
					$Json['teamname'] = ucwords($finduserteam->userteam);
				}else{
					$Json['teamname'] = $finduserteam->email;
				}
				$Json['teamnumber'] = $finduserteam->teamnumber;
				$Json['points'] = $finduserteam->points;
				$Json['status'] = 1;
				$playersget = $finduserteam->players;
				if($playersget!=""){
					$playersarr = explode(',',$finduserteam->players);
					$findmatch = DB::table('listmatches')->where('matchkey',$finduserteam->matchkey)->join('teams as t1','t1.id','=','listmatches.team1')->join('teams as t2','t2.id','=','listmatches.team2')->select('listmatches.team1','listmatches.team2','listmatches.team1display','listmatches.team2display','t1.color as team1color','t2.color as team2color')->first();
				$boplayerdetails=$batplayerdetails=$keeprdetails=$allrundetails=[];
				$resplayerdetails = DB::table('matchplayers')->whereIn('matchplayers.playerid',$playersarr)->where('matchkey',$finduserteam->matchkey)->join('players','matchplayers.playerid','=','players.id')->select('players.team','matchplayers.role as playerrole','matchplayers.credit as playercredit','matchplayers.role','matchplayers.points as playerpoints','players.player_name as playername','matchplayers.playerid as pid')->get();
				 foreach($resplayerdetails as $row){
					   if($row->role=="bowler"){
						   $boplayerdetails[]=$row;
					   } elseif($row->role=="batsman") {
						   $batplayerdetails[]=$row;
					   } elseif($row->role=="keeper") {
						   $keeprdetails[]=$row;
					   } elseif($row->role=="allrounder") {
						   $allrundetails[]=$row;
					   }
				 }
				 $resultmatchesarr=[];
				 $resultset =  DB::table('result_points')->join('result_matches','result_matches.id','=','result_points.resultmatch_id')->join('players','players.id','=','result_points.playerid')->leftjoin('teams','players.team','=','teams.id')->whereIn('playerid',$playersarr)->where('matchkey',$finduserteam->matchkey)->selectRaw('playerid,players.player_key,teams.team,sum(result_points.runs) as runs,sum(result_points.fours) as fours,sum(startingpoints) as startingpoints,sum(result_points.sixs) as sixs, sum(result_points.strike_rate) as strike_rate,sum(halcentury) as halcentury, sum(point150) as point150,sum(point200) as point200,sum(century) as century,sum(wickets)as wickets, sum(maidens) as maidens,sum(result_points.economy_rate) as economy_rate,sum(result_points.duck) as duck,sum(result_points.runouts) as runouts,sum(result_points.negative) as negative,sum(result_points.catch) as catch_point,winner_point,sum(not_out) as not_out,sum(stumping) as stumping, sum(total) as total')->groupBy('players.player_key')->orderBy('total','desc')->get();
				  foreach($resultset as $row){
					  $resultmatchesarr[$row->playerid][]=$row;
				  }  
				//   echo '<pre>'; print_r($resultmatchesarr); die;
					
					if(!empty($boplayerdetails)){
						$j=0;
						foreach($boplayerdetails as $bowler){
							$Json['bowler'][$j]['id'] = $bowler->pid;
							$Json['bowler'][$j]['player_name'] = $bowler->playername;
							$Json['bowler'][$j]['role'] = $bowler->playerrole;
							$Json['bowler'][$j]['credit'] = $bowler->playercredit;
							if($bowler->team==$findmatch->team1){
								$Json['bowler'][$j]['team'] = 'team1';
								$Json['bowler'][$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team1color));
							}
							if($bowler->team==$findmatch->team2){
								$Json['bowler'][$j]['team'] = 'team2';
								$Json['bowler'][$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team2color));
							}
							$vicecaptain=0;
							$captain=0;
							if($team->vicecaptain==$bowler->pid){
								$vicecaptain=1;
							}
							if($team->captain==$bowler->pid){
								$captain=1;
							}
							$Json['bowler'][$j]['vicecaptain'] = $vicecaptain;
							$Json['bowler'][$j]['captain'] = $captain;
							if($vicecaptain==1){
								$Json['bowler'][$j]['points'] = 1.5*$bowler->playerpoints;
							}
							else if($captain==1){
								$Json['bowler'][$j]['points'] = 2*$bowler->playerpoints;
							}else{
								$Json['bowler'][$j]['points'] = $bowler->playerpoints;
							}
							 $calculatepoints=(isset($resultmatchesarr[$bowler->pid]))?$resultmatchesarr[$bowler->pid]:[];
							//$calculatepoints= DB::table('result_matches')->where('match_key',$finduserteam->matchkey)->where('player_id',$bowler->pid)->select('starting11','batting_points','bowling_points','fielding_points','negative_points','extra_points')->get();
				// 			echo '<pre>'; print_r($calculatepoints); die;
							if(!empty($calculatepoints)){
								foreach($calculatepoints as $cal){
									$Json['bowler'][$j]['startingpoints'] = $cal->startingpoints;
									$Json['bowler'][$j]['runs'] = $cal->runs;
									$Json['bowler'][$j]['fours'] = $cal->fours;
									$Json['bowler'][$j]['sixs'] = $cal->sixs;
									$Json['bowler'][$j]['strike_rate'] = $cal->strike_rate;
									$Json['bowler'][$j]['halfcenturyPoints'] = $cal->halcentury;
									$Json['bowler'][$j]['point150'] = $cal->point150;
									$Json['bowler'][$j]['point200'] = $cal->point200;
									$Json['bowler'][$j]['not_out'] = $cal->not_out;
									$Json['bowler'][$j]['winner_point'] = $cal->winner_point;
									$Json['bowler'][$j]['negative'] = $cal->duck;
									$Json['bowler'][$j]['century'] = $cal->century;
									$Json['bowler'][$j]['wickets'] = $cal->wickets;
									$Json['bowler'][$j]['maidens'] = $cal->maidens;
									$Json['bowler'][$j]['duck'] = $cal->duck;
									$Json['bowler'][$j]['economy_rate'] = $cal->economy_rate;
									$Json['bowler'][$j]['runouts'] = $cal->runouts;
									$Json['bowler'][$j]['catch_pt'] = $cal->catch_point;
									$Json['bowler'][$j]['stumping'] = $cal->stumping;
									$Json['bowler'][$j]['total'] = $cal->total;
								}
							}
							$j++;
						}
					}
				}
				if(!empty($batplayerdetails)){
						$j=0;
						foreach($batplayerdetails as $batman){
							$Json['batsman'][$j]['id'] = $batman->pid;
							$Json['batsman'][$j]['player_name'] = $batman->playername;
							$Json['batsman'][$j]['role'] = $batman->playerrole;
							$Json['batsman'][$j]['credit'] = $batman->playercredit;
							if($batman->team==$findmatch->team1){
								$Json['batsman'][$j]['team'] = 'team1';
								$Json['batsman'][$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team1color));
							}
							if($batman->team==$findmatch->team2){
								$Json['batsman'][$j]['team'] = 'team2';
								$Json['batsman'][$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team2color));
							}
							$vicecaptain=0;
							$captain=0;
							if($team->vicecaptain==$batman->pid){
								$vicecaptain=1;
							}
							if($team->captain==$batman->pid){
								$captain=1;
							}
							if($vicecaptain==1){
								$Json['batsman'][$j]['points'] = 1.5*$batman->playerpoints;
							}
							else if($captain==1){
								$Json['batsman'][$j]['points'] = 2*$batman->playerpoints;
							}else{
								$Json['batsman'][$j]['points'] = $batman->playerpoints;
							}
							$Json['batsman'][$j]['vicecaptain'] = $vicecaptain;
							$Json['batsman'][$j]['captain'] = $captain;
													
																								   $calculatepoints=(isset($resultmatchesarr[$batman->pid]))?$resultmatchesarr[$batman->pid]:[];
							//$calculatepoints= DB::table('result_matches')->where('match_key',$finduserteam->matchkey)->where('player_id',$batman->pid)->select('starting11','batting_points','bowling_points','fielding_points','negative_points','extra_points')->get();
							if(!empty($calculatepoints)){
								 foreach($calculatepoints as $cal){
									$Json['batsman'][$j]['startingpoints'] = $cal->startingpoints;
									$Json['batsman'][$j]['runs'] = $cal->runs;
									$Json['batsman'][$j]['fours'] = $cal->fours;
									$Json['batsman'][$j]['sixs'] = $cal->sixs;
									$Json['batsman'][$j]['strike_rate'] = $cal->strike_rate;
									$Json['batsman'][$j]['halfcenturyPoints'] = $cal->halcentury;
									$Json['batsman'][$j]['century'] = $cal->century;
									$Json['batsman'][$j]['not_out'] = $cal->not_out;
									$Json['batsman'][$j]['winner_point'] = $cal->winner_point;
									$Json['batsman'][$j]['negative'] = $cal->duck;
									$Json['batsman'][$j]['point150'] = $cal->point150;
									$Json['batsman'][$j]['point200'] = $cal->point200;
									$Json['batsman'][$j]['wickets'] = $cal->wickets;
									$Json['batsman'][$j]['maidens'] = $cal->maidens;
									$Json['batsman'][$j]['economy_rate'] = $cal->economy_rate;
									$Json['batsman'][$j]['runouts'] = $cal->runouts;
									$Json['batsman'][$j]['catch_pt'] = $cal->catch_point;
									$Json['batsman'][$j]['stumping'] = $cal->stumping;
									$Json['batsman'][$j]['total'] = $cal->total;   
									$Json['batsman'][$j]['negative'] = $cal->negative;   
								 }
							}
							$j++;
						}
					}
					if(!empty($keeprdetails)){
						$j=0;
						foreach($keeprdetails as $keeper){
							$Json['keeper'][$j]['id'] = $keeper->pid;
							$Json['keeper'][$j]['player_name'] = $keeper->playername;
							$Json['keeper'][$j]['role'] = $keeper->playerrole;
							$Json['keeper'][$j]['credit'] = $keeper->playercredit;
							if($keeper->team==$findmatch->team1){
								$Json['keeper'][$j]['team'] = 'team1';
								$Json['keeper'][$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team1color));
							}
							if($keeper->team==$findmatch->team2){
								$Json['keeper'][$j]['team'] = 'team2';
								$Json['keeper'][$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team2color));
							}
							$vicecaptain=0;
							$captain=0;
							if($team->vicecaptain==$keeper->pid){
								$vicecaptain=1;
							}
							if($team->captain==$keeper->pid){
								$captain=1;
							}
							if($vicecaptain==1){
								$Json['keeper'][$j]['points'] = 1.5*$keeper->playerpoints;
							}
							else if($captain==1){
								$Json['keeper'][$j]['points'] = 2*$keeper->playerpoints;
							}else{
								$Json['keeper'][$j]['points'] = $keeper->playerpoints;
							}
							$Json['keeper'][$j]['vicecaptain'] = $vicecaptain;
							$Json['keeper'][$j]['captain'] = $captain;
													$calculatepoints=(isset($resultmatchesarr[$keeper->pid]))?$resultmatchesarr[$keeper->pid]:[];
							//$calculatepoints= DB::table('result_matches')->where('match_key',$finduserteam->matchkey)->where('player_id',$keeper->pid)->select('starting11','batting_points','bowling_points','fielding_points','negative_points','extra_points')->get();
							if(!empty($calculatepoints)){
								foreach($calculatepoints as $cal){
									$Json['keeper'][$j]['startingpoints'] = $cal->startingpoints;
									$Json['keeper'][$j]['runs'] = $cal->runs;
									$Json['keeper'][$j]['fours'] = $cal->fours;
									$Json['keeper'][$j]['sixs'] = $cal->sixs;
									$Json['keeper'][$j]['strike_rate'] = $cal->strike_rate;
									$Json['keeper'][$j]['halfcenturyPoints'] = $cal->halcentury;
									$Json['keeper'][$j]['century'] = $cal->century;
									$Json['keeper'][$j]['point150'] = $cal->point150;
									$Json['keeper'][$j]['point200'] = $cal->point200;
								    $Json['keeper'][$j]['not_out'] = $cal->not_out;
									$Json['keeper'][$j]['winner_point'] = $cal->winner_point;
									$Json['keeper'][$j]['negative'] = $cal->duck;
									$Json['keeper'][$j]['wickets'] = $cal->wickets;
									$Json['keeper'][$j]['maidens'] = $cal->maidens;
									$Json['keeper'][$j]['economy_rate'] = $cal->economy_rate;
									$Json['keeper'][$j]['runouts'] = $cal->runouts;
									$Json['keeper'][$j]['catch_pt'] = $cal->catch_point;
									$Json['keeper'][$j]['stumping'] = $cal->stumping;
									$Json['keeper'][$j]['total'] = $cal->total;
									$Json['keeper'][$j]['negative'] = $cal->negative;
								}
							}
							$j++;
						}
					}
					if(!empty($allrundetails)){
						$j=0;
						foreach($allrundetails as $allrounder){
							$Json['allrounder'][$j]['id'] = $allrounder->pid;
							$Json['allrounder'][$j]['player_name'] = $allrounder->playername;
							$Json['allrounder'][$j]['role'] = $allrounder->playerrole;
							$Json['allrounder'][$j]['credit'] = $allrounder->playercredit;
							if($allrounder->team==$findmatch->team1){
								$Json['allrounder'][$j]['team'] = 'team1';
								$Json['allrounder'][$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team1color));
								
							}
							if($allrounder->team==$findmatch->team2){
								$Json['allrounder'][$j]['team'] = 'team2';
								$Json['allrounder'][$j]['teamcolor'] = ucwords(str_replace(' ','-',$findmatch->team2color));
							}
							$vicecaptain=0;
							$captain=0;
							if($team->vicecaptain==$allrounder->pid){
								$vicecaptain=1;
							}
							if($team->captain==$allrounder->pid){
								$captain=1;
							}
							if($vicecaptain==1){
								$Json['allrounder'][$j]['points'] = 1.5*$allrounder->playerpoints;
							}
							else if($captain==1){
								$Json['allrounder'][$j]['points'] = 2*$allrounder->playerpoints;
							}else{
								$Json['allrounder'][$j]['points'] = $allrounder->playerpoints;
							}
							$Json['allrounder'][$j]['vicecaptain'] = $vicecaptain;
							$Json['allrounder'][$j]['captain'] = $captain;
													$calculatepoints=(isset($resultmatchesarr[$allrounder->pid]))?$resultmatchesarr[$allrounder->pid]:[];
							//$calculatepoints= DB::table('result_matches')->where('match_key',$finduserteam->matchkey)->where('player_id',$allrounder->pid)->select('starting11','batting_points','bowling_points','fielding_points','negative_points','extra_points')->get();
							if(!empty($calculatepoints)){
								foreach($calculatepoints as $cal){
									$Json['allrounder'][$j]['startingpoints'] = $cal->startingpoints;
									$Json['allrounder'][$j]['runs'] = $cal->runs;
									$Json['allrounder'][$j]['fours'] = $cal->fours;
									$Json['allrounder'][$j]['sixs'] = $cal->sixs;
									$Json['allrounder'][$j]['strike_rate'] = $cal->strike_rate;
									$Json['allrounder'][$j]['halfcenturyPoints'] = $cal->halcentury;
									$Json['allrounder'][$j]['century'] = $cal->century;
									$Json['allrounder'][$j]['point150'] = $cal->point150;
									$Json['allrounder'][$j]['point200'] = $cal->point200;
									$Json['allrounder'][$j]['not_out'] = $cal->not_out;
									$Json['allrounder'][$j]['winner_point'] = $cal->winner_point;
									$Json['allrounder'][$j]['negative'] = $cal->duck;
									$Json['allrounder'][$j]['wickets'] = $cal->wickets;
									$Json['allrounder'][$j]['maidens'] = $cal->maidens;
									$Json['allrounder'][$j]['economy_rate'] = $cal->economy_rate;
									$Json['allrounder'][$j]['runouts'] = $cal->runouts;
									$Json['allrounder'][$j]['catch_pt'] = $cal->catch_point;
									$Json['allrounder'][$j]['stumping'] = $cal->stumping;
									$Json['allrounder'][$j]['total'] = $cal->total;
									$Json['allrounder'][$j]['negative'] = $cal->negative;
								}
							}
							$j++;
						}
					}
				}else{
					$Json['status'] = 0;
				}
				echo json_encode(array($Json));
				die;
			}
			
			public function refreshscores1(){
			$this->accessrules();
			$matchkey = $data['matchkey'] =  $_GET['matchkey'];
			$userid =  $data['userid'] =  $_GET['userid'];
			$listmatchdetail = DB::table('listmatches')->where('matchkey',$_GET['matchkey'])->select('final_status')->first();
			$q =  DB::table('joinedleauges')->join('matchchallenges','matchchallenges.id','=','joinedleauges.challengeid')->where('joinedleauges.matchkey',$matchkey)->where('joinedleauges.userid',$_GET['userid']);
			$q->where(function($query){
				$query->where('matchchallenges.status','=','closed')
				->orWhere('matchchallenges.confirmed_challenge',1);
				});
			$findmyjoinedleauges = $q->select('joinedleauges.teamid','joinedleauges.challengeid','joinedleauges.id as joinid','matchchallenges.*')->groupBy('joinedleauges.challengeid')->get();
			if(!empty($findmyjoinedleauges)){
				$i=0;
				$Json = array();
				foreach($findmyjoinedleauges as $joined){
					if($listmatchdetail->final_status=='winnerdeclared'){
						$Json[$i]['matchstatus'] = 'Winner Declared';
					}
					else if($listmatchdetail->final_status=='IsClosed'){
						$Json[$i]['matchstatus'] = 'Match Closed';
					}
					else if($listmatchdetail->final_status=='IsReviewed'){
						$Json[$i]['matchstatus'] = 'In Review';
					}
					else{
						$Json[$i]['matchstatus'] = 'In process';
					}
					if($joined->name==""){
						$Json[$i]['name'] = ucwords('Win Rs. '.$joined->win_amount);
					}else{
						$Json[$i]['name'] = ucwords($joined->name);
					}
					$Json[$i]['teamid'] = $joined->teamid;
					$Json[$i]['is_private'] = $joined->is_private;
					$Json[$i]['grand'] = $joined->grand;
					$Json[$i]['multi_entry'] = $joined->multi_entry;
					$Json[$i]['joinid'] = $joined->joinid;
					if($joined->joinedusers==$joined->maximum_user){
						$Json[$i]['can_invite'] = false;
					}else{
						$Json[$i]['can_invite'] = true;
					}
					$Json[$i]['matchkey'] = $joined->matchkey;
					$Json[$i]['challenge_id'] = $joined->challengeid;
					$Json[$i]['entryfee'] = $joined->entryfee;
					$Json[$i]['maximum_user'] = $joined->maximum_user;
					$Json[$i]['joinedusers'] = $joined->joinedusers;
					$findpricecards = DB::table('matchpricecards')->where('challenge_id',$joined->challengeid)->orderBY('min_position','ASC')->get();
					if(!empty($findpricecards)){
						$j=0;
						$winners=0;
						foreach($findpricecards as $prc){
							$Json[$i]['price_card'][$j]['id'] = $prc->id;
							$Json[$i]['price_card'][$j]['winners'] = $prc->winners;
							$winners+=$prc->winners;
							$Json[$i]['price_card'][$j]['price'] = $prc->price;
							if($prc->min_position+1!=$prc->max_position){
								$Json[$i]['price_card'][$j]['start_position'] = $prc->min_position+1 .'-'. $prc->max_position;
							}else{
								$Json[$i]['price_card'][$j]['start_position'] = $prc->max_position;
							}
							$Json[$i]['price_card'][$j]['total'] = $prc->total;
							$Json[$i]['price_card'][$j]['description'] = $prc->description;
							$j++;
						}
						$Json[$i]['totalwinners'] = $winners;
						$Json[$i]['status'] = 1;
					}
					else{
						$Json[$i]['totalwinners'] = 1;
						$Json[$i]['price_card'][0]['id'] = 1;
						$Json[$i]['price_card'][0]['winners'] = 1;
						$Json[$i]['price_card'][0]['price'] = $joined->win_amount;
						$Json[$i]['price_card'][0]['start_position'] = 1;
						$Json[$i]['price_card'][0]['total'] = $joined->win_amount;
						$Json[$i]['price_card'][0]['description'] = "";
					}
					$findjoinedteams = DB::table('joinedleauges')->where('challengeid',$joined->challengeid)->join('registerusers','registerusers.id','=','joinedleauges.userid')->join('jointeam','jointeam.id','=','joinedleauges.teamid')->select('registerusers.team','registerusers.email','jointeam.teamnumber','jointeam.points','jointeam.lastpoints','joinedleauges.id as jid','joinedleauges.*')->orderBy('jointeam.points','DESC')->get();
					$findlastpointsleauges = DB::table('joinedleauges')->join('jointeam','jointeam.id','=','joinedleauges.teamid')->where('challengeid',$joined->challengeid)->select('jointeam.lastpoints')->orderBy('jointeam.lastpoints','DESC')->get();
					$gtlastranks = array();
					if(!empty($findlastpointsleauges)){
						foreach($findlastpointsleauges as $pleauges){
							$gtlastranks[] = $pleauges->lastpoints;
						}
					}
					if(!empty($findjoinedteams)){
						$k=0;$userrank = 0;
						foreach($findjoinedteams as $jointeam){
							if($jointeam->team!=""){
								$Json[$i]['jointeams'][$k]['teamname'] = ucwords($jointeam->team);
							}else{
								$Json[$i]['jointeams'][$k]['teamname'] = $jointeam->email;
							}
							$Json[$i]['jointeams'][$k]['teamid'] = $jointeam->teamid;
							$Json[$i]['jointeams'][$k]['teamnumber'] = $jointeam->teamnumber;
							$Json[$i]['jointeams'][$k]['points'] = $jointeam->points;
							$Json[$i]['jointeams'][$k]['lastpoints'] = $jointeam->lastpoints;
							$getuserlastrank = array_search($jointeam->lastpoints,$gtlastranks);
							$getcurrentrank = $k+1;
							if($getuserlastrank<$getcurrentrank){
								$Json[$i]['jointeams'][$k]['arrowname'] = '';
							}
							else if($getuserlastrank==$getcurrentrank){
								$Json[$i]['jointeams'][$k]['arrowname'] = '';
							}
							else if($getuserlastrank>$getcurrentrank){
								$Json[$i]['jointeams'][$k]['arrowname'] = '';
							}
							if($Json[$i]['jointeams'][$k]['lastpoints']==0){
								$Json[$i]['jointeams'][$k]['arrowname'] = "";
							}
							$Json[$i]['jointeams'][$k]['userjoinid'] = $jointeam->jid;
							$Json[$i]['jointeams'][$k]['userid'] = $jointeam->userid;
							if($jointeam->userid==$userid){
								$Json[$i]['jointeams'][$k]['is_show'] = true;
								$userrank = $k+1;
							}else{
								$Json[$i]['jointeams'][$k]['is_show'] = false;
							}
							$k++;
						}
					}
					$Json[$i]['userrank'] = $userrank;
					$Json[$i]['status'] = 1;
				$i++;	
				}
			}else{
				$Json[0]['status'] = 0;
			}
			echo json_encode($Json);
			die;
			
		}
		public function multid_sort($arr, $index) {
			$b = array();
			$c = array();
			foreach ($arr as $key => $value) {
				$b[$key] = $value[$index];
			}
			arsort($b);
			foreach ($b as $key => $value) {
				$c[] = $arr[$key];
			}
			return $c;
		}
		public function searchByValue($products, $field, $value){
			foreach($products as $key => $product)
		   {
			  if ( $product[$field] === $value )
				 return $key;
		   }
		   return false;
		}
		
		// public function mymarathonsget(){
			// $this->accessrules();
			// $matchkey = $data['matchkey'] =  $_GET['matchkey'];
			// $userid =  $data['userid'] =  $_GET['userid'];
			// $findseries = DB::table('listmatches')->where('matchkey',$_GET['matchkey'])->select('series','marathon')->join('marathon','marathon.series','=','listmatches.series')->first();
			// $Json=array();
			// $findmarathondetails = $findseries
			// if(!empty($findseries)){
				// $seriesid = $findseries->series;
				// $bestmatches = $findmarathondetails->bestmatches;
				// $findmatchchallenges = DB::table('matchchallenges')->where('series_id',$seriesid)->where('marathon',1)->get();
				// $chid = array();
				// if(!empty($findmatchchallenges)){
					// foreach($findmatchchallenges as $chl){
						// if($_GET['matchkey']==$chl->matchkey){
							// $challengeid = $chl->id;
						// }
						// $chid[] = $chl->id;
					// }
				// }
				// $findifinthismatch = DB::table('joinedleauges')->where('joinedleauges.challengeid',$challengeid)->where('joinedleauges.userid',$userid)->join('jointeam','jointeam.id','=','joinedleauges.teamid')->select('joinedleauges.*','jointeam.teamnumber')->first();
				// if(empty($findifinthismatch)){
					// $Json[0]['marathon_join_status'] = 0;
					// echo json_encode($Json);
					// die;
				// }
				// else{
					// $j=0;
					// $Json[$j]['teamid'] = $ljoin->teamid;
					// $Json[$j]['joinid'] = $ljoin->id;
					// $Json[$j]['teamnumber'] = $ljoin->teamnumber;
					// $Json[$j]['seriesid'] = $seriesid;
					// $Json[$j]['refercode'] = $ljoin->refercode;
					// $Json[$j]['bestmatches'] = $findmarathondetails->bestmatches;
					// if($findmarathondetails->name==""){
						// if($findmarathondetails->win_amount==0){
							// $Json[$j]['name'] = 'Net Practice';
						// }else{
							// $Json[$j]['name'] = ucwords('Win Rs. '.$findmarathondetails->win_amount);
						// }
					// }
					// else{
						// $Json[$j]['name'] = ucwords('Win Rs. '.$findmarathondetails->win_amount);
					// }
					// if($findmarathondetails->joinedusers==$findmarathondetails->maximum_user){
						// $Json[$j]['can_invite'] = false;
					// }else{	
						// $Json[$j]['can_invite'] = true;
					// }
					// $Json[$j]['joinedusers'] = $findmarathondetails->joinedusers;
					// $Json[$j]['maximum_user'] = $findmarathondetails->maximum_user;
					// $Json[$j]['entryfee'] = $findmarathondetails->entryfee;
					// $Json[$j]['challenge_id'] = $challengeid;
					// $Json[$j]['win_amount'] = $findmarathondetails->win_amount;
					// $Json[$j]['matchkey'] = $matchkey;
					// /* price cards*/
					// $findpricecards = DB::table('marathonpricecards')->where('challenge_id',$findmarathondetails->id)->orderBY('id','ASC')->get();
					// if(!empty($findpricecards)){
						// $k=0;
						// $winners=0;
						// foreach($findpricecards as $prc){
							// $Json[$j]['price_card'][$k]['id'] = $prc->id;
							// $Json[$j]['price_card'][$k]['winners'] = $prc->winners;
							// $winners+=$prc->winners;
							// $Json[$j]['price_card'][$k]['price'] = $prc->price;
							// if($prc->min_position+1!=$prc->max_position){
								// $Json[$j]['price_card'][$k]['start_position'] = $prc->min_position+1 .'-'. $prc->max_position;
							// }else{
								// $Json[$j]['price_card'][$k]['start_position'] = $prc->max_position;
							// }
							// $Json[$j]['price_card'][$k]['total'] = $prc->total;
							// $Json[$j]['price_card'][$k]['description'] = $prc->description;
							// $k++;
						// }
						// $Json[$j]['totalwinners'] = $winners;
						// $Json[$j]['pricecardstatus'] = 1;
					// }
					// else{
						// $Json[$j]['pricecardstatus'] = 0;
						// $Json[$j]['totalwinners'] = 1;
					// }
					// /*end price cards*/
					// /*find all users*/
					// /*allusers*/
						// $findallusers = DB::table('joinedleauges')->whereIn('joinedleauges.challengeid',$chid)->join('registerusers','registerusers.id','=','joinedleauges.userid')->groupBy('joinedleauges.userid')->select('registerusers.team','joinedleauges.userid as uid')->get();
						// if(!empty($findallusers)){
							// $i=0;$udetailsar = array();$ulastdetailsar = array();
							// $userslistsno=-1;
							// foreach($findallusers as $user){
								// if($user->uid==$userid){
									// $udetailsar[$i]['is_show'] = true;
									// $udetailsar[$i]['userno'] =$userslistsno;
								// }else{
									// $udetailsar[$i]['is_show'] = false;
									// $udetailsar[$i]['userno'] =0;
								// }
								// $udetailsar[$i]['teamname'] = $user->team;
								// $finduserpoints = DB::table('joinedleauges')->where('joinedleauges.userid',$user->uid)->whereIn('joinedleauges.challengeid',$chid)->join('jointeam','jointeam.id','=','joinedleauges.teamid')->orderBy('jointeam.points','DESC')->limit($bestmatches)->select('jointeam.points as tpoints')->get();
								// $finduserlastpoints = DB::table('joinedleauges')->where('joinedleauges.userid',$user->uid)->whereIn('joinedleauges.challengeid',$chid)->join('jointeam','jointeam.id','=','joinedleauges.teamid')->orderBy('jointeam.lastpoints','DESC')->limit($bestmatches)->select('jointeam.lastpoints as tpoints')->get();
								// $sumpoints = 0; $sumlastpoints = 0;
								// /* get current scores */
								// if(!empty($finduserpoints)){
									// foreach($finduserpoints as $up){
										// $sumpoints+=$up->tpoints;
									// }
								// }
								// /* get last scores */
								// if(!empty($finduserlastpoints)){
									// foreach($finduserlastpoints as $lp){
										// $sumlastpoints+=$lp->tpoints;
									// }
								// }
								// $udetailsar[$i]['totalpoints'] = $sumpoints;
								// $ulastdetailsar[$i]['lastpoints'] = $sumlastpoints;
								// $ulastdetailsar[$i]['user_id'] = $user->uid;
								// $udetailsar[$i]['user_id'] = $user->uid;
								// $udetailsar[$i]['status'] = 1;
								// $i++;
							// }
							// $this->sortBySubArrayValue($udetailsar, 'totalpoints', 'desc');
							// $this->sortBySubArrayValue($ulastdetailsar, 'lastpoints', 'desc');
							// $gtcurranks = $this->multid_sort($udetailsar, 'totalpoints');
								// if(!empty($gtcurranks)){
									// $getusercurrank=array();
									// $cur=0;$currsno = 0;$plus=0;
									// foreach($gtcurranks as $curnk){
										// if(!in_array($curnk['totalpoints'], array_column($getusercurrank, 'totalpoints'))){ // search value in the array
											// $currsno++;
											// $currsno = $currsno+$plus;
											// $plus=0;
											
										// }
										// else{
											// $plus++;
											
										// }
										// $getusercurrank[$cur]['rank'] = $currsno;
										// $getusercurrank[$cur]['totalpoints'] = $curnk['totalpoints'];
										// $getusercurrank[$cur]['userid'] = $curnk['user_id'];
										// $cur++;
										
									// }
								// }
							// $userslastrank =array();$ulas = 0;$ulastrank = 0;
							// foreach($ulastdetailsar as $ulast){
								// if(in_array($ulast['lastpoints'], array_column($userslastrank, 'lastpoints'))) { // search value in the array
									
								// }else{
									// $ulastrank++;
								// }
								// $userslastrank[$ulas]['userid'] = $ulast['user_id'];
								// $userslastrank[$ulas]['lastpoints'] = $ulast['lastpoints'];
								// $userslastrank[$ulas]['lastrank'] = $ulastrank;
								// $ulas++;
							// }
							// $mainuserrank = 1;
							// $Json[$j]['users_join'] = array();$urank=0;$usno=0;$userslistsno=-1;
							// foreach($udetailsar as $userarrayagain){
								// $getuserindexinglast = $this->searchByValue($userslastrank,'userid',$userarrayagain['user_id']);
								// $getlastrank = $userslastrank[$getuserindexinglast]['lastrank'];
								// $getcurindex = $this->searchByValue($getusercurrank,'userid',$userarrayagain['user_id']);
								// $getcurrank = $urank = $getusercurrank[$getcurindex]['rank'];
								// if($userarrayagain['user_id']==$userid){
									// $mainuserrank = $getcurrank;
									// $Json[$j]['users_join'][$usno]['userno'] =$userslistsno;
								// }else{
									// $Json[$j]['users_join'][$usno]['userno'] = 0;
								// }
								// $Json[$j]['users_join'][$usno]['userrank'] = $getcurrank;
								// $Json[$j]['users_join'][$usno]['lastuserrank'] = $getlastrank;
								// if($getlastrank<$urank){
									// $Json[$j]['users_join'][$usno]['arrowname'] = 'down-arrow';
								// }
								// else if($getlastrank==$urank){
									// $Json[$j]['users_join'][$usno]['arrowname'] = 'equal-arrow';
								// }
								// else if($getlastrank>$urank){
									// $Json[$j]['users_join'][$usno]['arrowname'] = 'up-arrow';
								// }
								// $Json[$j]['users_join'][$usno]['is_show'] = $userarrayagain['is_show'];
								// $Json[$j]['users_join'][$usno]['totalpoints'] = $userarrayagain['totalpoints'];
								// $Json[$j]['users_join'][$usno]['teamname'] = $userarrayagain['teamname'];
								// $Json[$j]['users_join'][$usno]['status'] = 1;
								// if($listmatchdetail->final_status=='winnerdeclared'){
									// $finduserwiningamount = DB::table('finalresults')->where('finalresults.userid',$userarrayagain['user_id'])->where('finalresults.seriesid',$seriesid)->join('transactions','transactions.transaction_id','=','finalresults.transaction_id')->select('transactions.amount')->first();
									// if(!empty($finduserwiningamount)){
										// $Json[$j]['users_join'][$usno]['winingamount'] = round($finduserwiningamount->amount,2);
									// }else{
										// $Json[$j]['users_join'][$usno]['winingamount']="";
									// }
								// }
								// else{
									// $Json[$j]['users_join'][$usno]['winingamount']="";
								// }
								// $usno++;
							// }
							
							// array_multisort(array_column($Json[$j]['users_join'],'userno'),SORT_ASC,array_column($Json[$j]['users_join'],'totalpoints'),SORT_DESC,$Json[$j]['users_join']);
							// $Json[$j]['mainuserrank'] = $mainuserrank;
						// }
						// $Json[$j]['marathon_join_status'] = 1;
					
				// }
				
			// }
			// else{
				
			// }
		// }
		public function mymarathons(){
			$this->accessrules();
			$matchkey = $data['matchkey'] =  $_GET['matchkey'];
			$userid =  $data['userid'] =  $_GET['userid'];
			$listmatchdetail = DB::table('listmatches')->where('matchkey',$_GET['matchkey'])->select('final_status','status','series')->first();
			$challengeid="";
			$findseries = $listmatchdetail; //added by kumar
									$winnerdeclared=($listmatchdetail->final_status=='winnerdeclared')? true : false;
									$cacheExpiresAt = 60*24*30;
									//$findseries =DB::table('listmatches')->where('matchkey',$_GET['matchkey'])->select('series')->first();
			$Json=array();
			if(!empty($findseries)){
				$seriesid = $findseries->series;
				$findmarathondetails = DB::table('marathon')->where('series',$seriesid)->first();
				if(!empty($findmarathondetails)){
					$bestmatches = $findmarathondetails->bestmatches;
					$findmatchchallenges = DB::table('matchchallenges')->where('series_id',$seriesid)->where('marathon',1)->get();
					$chid = array();
					if(!empty($findmatchchallenges)){
						foreach($findmatchchallenges as $chl){
							if($_GET['matchkey']==$chl->matchkey){
								$challengeid = $chl->id;
							}
							$chid[] = $chl->id;
						}
					}
					$findifinthismatch = DB::table('joinedleauges')->where('joinedleauges.challengeid',$challengeid)->where('joinedleauges.userid',$userid)->first();
					if(empty($findifinthismatch)){
						$Json[0]['marathon_join_status'] = 0;
						echo json_encode($Json);
						die;
					}
					$findjoinedleauges = DB::table('joinedleauges')->whereIn('joinedleauges.challengeid',$chid)->where('joinedleauges.userid',$userid)->join('jointeam','jointeam.id','=','joinedleauges.teamid')->select('joinedleauges.*','jointeam.teamnumber')->get();
					if(!empty($findjoinedleauges)){
						$j=0;
						foreach($findjoinedleauges as $ljoin){
							if($ljoin->challengeid==$challengeid){
								$Json[$j]['teamid'] = $ljoin->teamid;
								$Json[$j]['joinid'] = $ljoin->id;
								$Json[$j]['teamnumber'] = $ljoin->teamnumber;
								$Json[$j]['seriesid'] = $seriesid;
								$Json[$j]['refercode'] = $ljoin->refercode;
								$Json[$j]['bestmatches'] = $findmarathondetails->bestmatches;
							}
						}
						if($findmarathondetails->name==""){
							if($findmarathondetails->win_amount==0){
								$Json[$j]['name'] = 'Net Practice';
							}else{
								$Json[$j]['name'] = ucwords('Win Rs. '.$findmarathondetails->win_amount);
							}
						}
						else{
							$Json[$j]['name'] = ucwords('Win Rs. '.$findmarathondetails->win_amount);
						}
						if($findmarathondetails->joinedusers==$findmarathondetails->maximum_user){
							$Json[$j]['can_invite'] = false;
						}else{	
							$Json[$j]['can_invite'] = true;
						}
						$Json[$j]['joinedusers'] = $findmarathondetails->joinedusers;
						$Json[$j]['maximum_user'] = $findmarathondetails->maximum_user;
						$Json[$j]['entryfee'] = $findmarathondetails->entryfee;
						$Json[$j]['challenge_id'] = $challengeid;
						$Json[$j]['win_amount'] = $findmarathondetails->win_amount;
						$Json[$j]['matchkey'] = $matchkey;
						/* price cards*/
						$findpricecards = DB::table('marathonpricecards')->where('challenge_id',$findmarathondetails->id)->orderBY('id','ASC')->get();
						if(!empty($findpricecards)){
						$k=0;
						$winners=0;
						foreach($findpricecards as $prc){
								$Json[$j]['price_card'][$k]['id'] = $prc->id;
								$Json[$j]['price_card'][$k]['winners'] = $prc->winners;
								$winners+=$prc->winners;
								$Json[$j]['price_card'][$k]['price'] = $prc->price;
								if($prc->min_position+1!=$prc->max_position){
									$Json[$j]['price_card'][$k]['start_position'] = $prc->min_position+1 .'-'. $prc->max_position;
								}else{
									$Json[$j]['price_card'][$k]['start_position'] = $prc->max_position;
								}
								$Json[$j]['price_card'][$k]['total'] = $prc->total;
								$Json[$j]['price_card'][$k]['description'] = $prc->description;
								$k++;
						}
						$Json[$j]['totalwinners'] = $winners;
						$Json[$j]['pricecardstatus'] = 1;
						}else{
							$Json[$j]['pricecardstatus'] = 0;
							$Json[$j]['totalwinners'] = 1;
						}
						/*allusers*/
		 $query=DB::table('joinedleauges')->whereIn('joinedleauges.challengeid',$chid)->join('registerusers','registerusers.id','=','joinedleauges.userid')->groupBy('joinedleauges.userid')->select('registerusers.team','joinedleauges.userid as uid');
			  if($winnerdeclared){ 
				  $key='mymarathons_findallusers_'.$matchkey;
				  $findallusers = Cache::remember($key, $cacheExpiresAt, function() use ($query) {
					  return $query->get();
				  });
			  }
			  else{
							$findallusers = $query->get();
			  }
			  //Get find user points added by kumar
			  $key='mymarathons_finduserpoints_'.$matchkey;
			  $finduserpointsArr =[];
			  if (Cache::has($key)) {
				   $finduserpointsArr = Cache::get($key);
			  }
						if(!empty($findallusers)){
							$i=0;$udetailsar = array();$ulastdetailsar = array();
							$userslistsno=-1;
							foreach($findallusers as $user){
								if($user->uid==$userid){
									$udetailsar[$i]['is_show'] = true;
									$udetailsar[$i]['userno'] =$userslistsno;
								}else{
									$udetailsar[$i]['is_show'] = false;
									$udetailsar[$i]['userno'] =0;
								}
								$udetailsar[$i]['teamname'] = $user->team;
$sumpoints = 0; $sumlastpoints = 0;
if(isset($finduserpointsArr[$user->uid])){
$sumpoints=$finduserpointsArr[$user->uid]['sumpoints'];
$sumlastpoints=$finduserpointsArr[$user->uid]['sumlastpoints'];
}else{
$finduserpoints = DB::table('joinedleauges')->where('joinedleauges.userid',$user->uid)->whereIn('joinedleauges.challengeid',$chid)->join('jointeam','jointeam.id','=','joinedleauges.teamid')->orderBy('jointeam.points','DESC')->limit($bestmatches)->select('jointeam.points as tpoints','jointeam.lastpoints')->get();

/* get current scores */
if(!empty($finduserpoints)){
foreach($finduserpoints as $up){
$sumpoints+=$up->tpoints;
	$sumlastpoints+=$up->lastpoints;
}
}
$finduserpointsArr[$user->uid] = ['sumpoints'=>$sumpoints, 'sumlastpoints'=>$sumlastpoints];
}			
$udetailsar[$i]['totalpoints'] = $sumpoints;
$ulastdetailsar[$i]['lastpoints'] = $sumlastpoints;
$ulastdetailsar[$i]['user_id'] = $user->uid;
$udetailsar[$i]['user_id'] = $user->uid;
$udetailsar[$i]['status'] = 1;
$i++;
}
//Putting data into cache code added by kumar
if($winnerdeclared && !Cache::has($key)){ 
Cache::put($key,$finduserpointsArr,$cacheExpiresAt);
}
																	 
$this->sortBySubArrayValue($udetailsar, 'totalpoints', 'desc');
$this->sortBySubArrayValue($ulastdetailsar, 'lastpoints', 'desc');
$gtcurranks = $this->multid_sort($udetailsar, 'totalpoints');
if(!empty($gtcurranks)){
$getusercurrank=array();
$cur=0;$currsno = 0;$plus=0;
foreach($gtcurranks as $curnk){
if(!in_array($curnk['totalpoints'], array_column($getusercurrank, 'totalpoints'))){ // search value in the array
$currsno++;
$currsno = $currsno+$plus;
$plus=0;

}
else{
$plus++;

}
$getusercurrank[$cur]['rank'] = $currsno;
$getusercurrank[$cur]['totalpoints'] = $curnk['totalpoints'];
$getusercurrank[$cur]['userid'] = $curnk['user_id'];
$cur++;

}
}
$userslastrank =array();$ulas = 0;$ulastrank = 0;
foreach($ulastdetailsar as $ulast){
if(in_array($ulast['lastpoints'], array_column($userslastrank, 'lastpoints'))) { // search value in the array

}else{
$ulastrank++;
}
$userslastrank[$ulas]['userid'] = $ulast['user_id'];
$userslastrank[$ulas]['lastpoints'] = $ulast['lastpoints'];
$userslastrank[$ulas]['lastrank'] = $ulastrank;
$ulas++;
}
$mainuserrank = 1;
$Json[$j]['users_join'] = array();$urank=0;$usno=0;$userslistsno=-1;
//Get find user wining amount added by kumar
$key='mymarathons_finduserwiningamount _'.$matchkey;
$finduserwiningamountArr =[];
if (Cache::has($key)) {
  $finduserwiningamountArr = Cache::get($key);
}
																   
							foreach($udetailsar as $userarrayagain){
								$getuserindexinglast = $this->searchByValue($userslastrank,'userid',$userarrayagain['user_id']);
								$getlastrank = $userslastrank[$getuserindexinglast]['lastrank'];
								$getcurindex = $this->searchByValue($getusercurrank,'userid',$userarrayagain['user_id']);
								$getcurrank = $urank = $getusercurrank[$getcurindex]['rank'];
								if($userarrayagain['user_id']==$userid){
									$mainuserrank = $getcurrank;
									$Json[$j]['users_join'][$usno]['userno'] =$userslistsno;
								}else{
									$Json[$j]['users_join'][$usno]['userno'] = 0;
								}
								$Json[$j]['users_join'][$usno]['userrank'] = $getcurrank;
								$Json[$j]['users_join'][$usno]['lastuserrank'] = $getlastrank;
								if($getlastrank<$urank){
									$Json[$j]['users_join'][$usno]['arrowname'] = 'down-arrow';
								}
								else if($getlastrank==$urank){
									$Json[$j]['users_join'][$usno]['arrowname'] = 'equal-arrow';
								}
								else if($getlastrank>$urank){
									$Json[$j]['users_join'][$usno]['arrowname'] = 'up-arrow';
								}
								$Json[$j]['users_join'][$usno]['is_show'] = $userarrayagain['is_show'];
								$Json[$j]['users_join'][$usno]['totalpoints'] = $userarrayagain['totalpoints'];
								$Json[$j]['users_join'][$usno]['teamname'] = $userarrayagain['teamname'];
								$Json[$j]['users_join'][$usno]['status'] = 1;
								if($winnerdeclared){
														   if(isset($finduserwiningamountArr[$userarrayagain['user_id']])){
																$finduserwiningamount = $finduserwiningamountArr[$userarrayagain['user_id']];
															}else{
									$finduserwiningamount = DB::table('finalresults')->where('finalresults.userid',$userarrayagain['user_id'])->where('finalresults.seriesid',$seriesid)->join('transactions','transactions.transaction_id','=','finalresults.transaction_id')->select('transactions.amount')->first();
																   $finduserwiningamountArr[$userarrayagain['user_id']]= $finduserwiningamount;
															}
									if(!empty($finduserwiningamount)){
										$Json[$j]['users_join'][$usno]['winingamount'] = round($finduserwiningamount->amount,2);
									}else{
										$Json[$j]['users_join'][$usno]['winingamount']="";
									}
								}
								else{
									$Json[$j]['users_join'][$usno]['winingamount']="";
								}
								$usno++;
							}
													//Putting data into cache code added by kumar
																	  if($winnerdeclared && !Cache::has($key)){ 
																		   Cache::put($key,$finduserwiningamountArr,$cacheExpiresAt);
																	  }
							array_multisort(array_column($Json[$j]['users_join'],'userno'),SORT_ASC,array_column($Json[$j]['users_join'],'totalpoints'),SORT_DESC,$Json[$j]['users_join']);
							$Json[$j]['mainuserrank'] = $mainuserrank;
						}
						$Json[$j]['marathon_join_status'] = 1;
					}
					else{
						$Json[$j]['marathon_join_status'] = 0;
					}
				}
				else{
					$Json[0]['marathon_join_status'] = 0;
				}
			}
			
			echo json_encode($Json);
			die;
		}
		public function refreshscores(){
			date_default_timezone_set("Asia/Kolkata");
			$today_date = date('Y-m-d H:i:s'); 
			$this->accessrules();
			$matchkey = $data['matchkey'] =  $_GET['matchkey'];
			$userid =  $data['userid'] =  $_GET['userid'];
			$Json=array();
			$listmatchdetail = DB::table('listmatches')->where('matchkey',$_GET['matchkey'])->select('final_status','status','start_date')->first();
			$queryget = DB::table('joinedleauges')->where('joinedleauges.userid',$_GET['userid'])->where('joinedleauges.matchkey',$_GET['matchkey'])->join('matchchallenges','matchchallenges.id','=','joinedleauges.challengeid')->where('matchchallenges.marathon','<>',1);
			$queryget->where(function($query){
				$query->where('matchchallenges.status','=','closed')
				->orWhere('matchchallenges.confirmed_challenge',1);
			});
			$findmychallenges = $queryget->select(DB::raw('SQL_CACHE matchchallenges.win_amount'),'matchchallenges.name','matchchallenges.entryfee','matchchallenges.multi_entry','matchchallenges.confirmed_challenge','matchchallenges.grand','joinedleauges.teamid','joinedleauges.challengeid','joinedleauges.id as joinid','matchchallenges.joinedusers','matchchallenges.maximum_user','matchchallenges.is_private','matchchallenges.pdf_created')->groupBy('matchchallenges.id')->orderBy('matchchallenges.win_amount','DESC')->get();
			
			if(!empty($findmychallenges)){
				$i=0;
													$challarr=[];
													foreach($findmychallenges as $joined){
														$challarr[]=$joined->challengeid;
													}
													$pricecardarr=[];
													$resprice=DB::table('matchpricecards')->whereIn('challenge_id',$challarr)->where('matchkey',$_GET['matchkey'])->orderBY('min_position','ASC')->select('challenge_id','winners','price','max_position','min_position')->get();
													 foreach($resprice as $row){
														$pricecardarr[$row->challenge_id][]=$row;
													}
													
												 $joinedteamsarr=[];
						 if($listmatchdetail->final_status=='winnerdeclared'){
												   $key='joinedteam_'.$matchkey.'_'.$userid;
									   $resjoinedteams = Cache::remember($key, (60*24*30), function() use ($challarr) {
																		   return DB::table('joinedleauges')->whereIn('joinedleauges.challengeid',$challarr)->join('registerusers','registerusers.id','=','joinedleauges.userid')->join('jointeam','jointeam.id','=','joinedleauges.teamid')
						->leftJoin('finalresults',function ($join){
							$join->on('finalresults.joinedid', '=' , 'joinedleauges.id');
						})->orderBy('jointeam.points','DESC')->select('joinedleauges.challengeid','registerusers.team','registerusers.email','jointeam.teamnumber','jointeam.points','jointeam.lastpoints','joinedleauges.id as jid','joinedleauges.userid','joinedleauges.teamid','joinedleauges.refercode','finalresults.amount')->get();
													 });
												 }else{
						$resjoinedteams =  DB::table('joinedleauges')->whereIn('joinedleauges.challengeid',$challarr)->join('registerusers','registerusers.id','=','joinedleauges.userid')->join('jointeam','jointeam.id','=','joinedleauges.teamid')->orderBy('jointeam.points','DESC')->select(DB::raw('SQL_CACHE  joinedleauges.challengeid'),'registerusers.team','registerusers.email','jointeam.teamnumber','jointeam.points','jointeam.lastpoints','joinedleauges.id as jid','joinedleauges.userid','joinedleauges.teamid')->get();
						 }
								 
												 foreach($resjoinedteams as $row){
														$joinedteamsarr[$row->challengeid][]=$row;
													}
								 //print_r($findmychallenges); exit;
				foreach($findmychallenges as $joined){
					$Json[$i]['win_amount'] = 0;
					if($listmatchdetail->final_status=='winnerdeclared'){
						$Json[$i]['matchstatus'] = 'Winner Declared';
						$final_result = DB::table('finalresults')->where('userid', $userid)->where('matchkey', $matchkey)->where('challengeid',$joined->challengeid)->sum('amount');//->orderBY('amount','DESC')->first();
						if(!empty($final_result)) {
							$Json[$i]['win_amount'] = $final_result;
						}
					}

					else if($listmatchdetail->final_status=='IsClosed'){
						$Json[$i]['matchstatus'] = 'Match Closed';
					}
					else if($listmatchdetail->final_status=='IsReviewed' || $listmatchdetail->status=='completed'){
						$Json[$i]['matchstatus'] = 'Under Review';
					}
					else{
						$Json[$i]['matchstatus'] = 'In progress';
					}
					if($joined->name==""){
						$Json[$i]['name'] = ucwords('Win Rs. '.$joined->win_amount);
					}else{
						$Json[$i]['name'] = ucwords($joined->name);
					}
					$Json[$i]['is_private'] = $joined->is_private;
					$Json[$i]['grand'] = $joined->grand;
					$Json[$i]['confirmed'] = $joined->confirmed_challenge;
					$Json[$i]['multi_entry'] = $joined->multi_entry;
					$Json[$i]['teamid'] = $joined->teamid;
					$Json[$i]['joinid'] = $joined->joinid;
					if($joined->joinedusers==$joined->maximum_user){
						$Json[$i]['can_invite'] = false;
					}else{
						$Json[$i]['can_invite'] = true;
					}
					$Json[$i]['challenge_id'] = $joined->challengeid;
					$Json[$i]['entryfee'] = $joined->entryfee;
					$Json[$i]['maximum_user'] = $joined->maximum_user;
					$Json[$i]['joinedusers'] = $joined->joinedusers;
					$Json[$i]['total_winning'] = $joined->win_amount;
					$joined_count = DB::table('joinedleauges')->where('userid', $userid)->where('challengeid', $joined->challengeid)->where('matchkey', $matchkey)->count();
					if($joined_count==1) {
						$team_info = DB::table('jointeam')->join('joinedleauges','jointeam.id','=','joinedleauges.teamid')->where('joinedleauges.challengeid', $joined->challengeid)->where('joinedleauges.userid',$userid)->select('teamnumber','points')->orderBY('points','DESC')->first();
						$joined_with = 'T'.$team_info->teamnumber;
						$team_points = $team_info->points; 
					} else {
						$joined_with = $joined_count.' teams';
						$team_info = DB::table('jointeam')->join('joinedleauges','jointeam.id','=','joinedleauges.teamid')->where('joinedleauges.challengeid', $joined->challengeid)->where('joinedleauges.userid',$userid)->select('teamnumber','points')->orderBY('points','DESC')->first();
						$team_points = $team_info->points;
					}
					$Json[$i]['joined_with'] = $joined_with;
					$Json[$i]['points'] = $team_points;

					$findpricecards = (isset($pricecardarr[$joined->challengeid]))?$pricecardarr[$joined->challengeid]:[];//DB::table('matchpricecards')->where('challenge_id',$joined->challengeid)->where('matchkey',$_GET['matchkey'])->orderBY('min_position','ASC')->select('winners','price','max_position','min_position')->get();
					if(!empty($findpricecards)){
						$j=0;
						$winners=0;
						foreach($findpricecards as $prc){
							$Json[$i]['price_card'][$j]['id'] = 0;
							// $Json[$i]['price_card'][$j]['winners'] = $prc->winners;
							$winners+=$prc->winners;
							$Json[$i]['price_card'][$j]['price'] = $prc->price;
							if($prc->min_position+1!=$prc->max_position){
								$Json[$i]['price_card'][$j]['start_position'] = $prc->min_position+1 .'-'. $prc->max_position;
							}else{
								$Json[$i]['price_card'][$j]['start_position'] = $prc->max_position;
							}
							$j++;
						}
						$Json[$i]['totalwinners'] = $winners;
						$Json[$i]['status'] = 1;
					}else{
						$j=0;
						$Json[$i]['price_card'][$j]['id'] = 0;
						// $Json[$i]['price_card'][$j]['winners'] = 1;
						$Json[$i]['price_card'][$j]['price'] = $joined->win_amount;
						$Json[$i]['price_card'][$j]['start_position'] = 1;
						$Json[$i]['totalwinners'] = 1;
					}
					// find teams of users//
					$joinedidget = $joined->joinid;
					if($listmatchdetail->final_status=='winnerdeclared'){
						$findjoinedteams =  DB::table('joinedleauges')->where('joinedleauges.challengeid',$joined->challengeid)->join('registerusers','registerusers.id','=','joinedleauges.userid')->join('jointeam','jointeam.id','=','joinedleauges.teamid')
						->leftJoin('finalresults',function ($join) use ($joinedidget){
							$join->on('finalresults.joinedid', '=' , 'joinedleauges.id');
						})->orderBy('jointeam.points','DESC')->select('registerusers.team','registerusers.email','jointeam.teamnumber','jointeam.points','jointeam.lastpoints','joinedleauges.id as jid','joinedleauges.userid','joinedleauges.teamid','joinedleauges.refercode','finalresults.amount')->get();
					}
					else{
						$findjoinedteams =  DB::table('joinedleauges')->where('joinedleauges.challengeid',$joined->challengeid)->join('registerusers','registerusers.id','=','joinedleauges.userid')->join('jointeam','jointeam.id','=','joinedleauges.teamid')->orderBy('jointeam.points','DESC')->select('registerusers.team','registerusers.email','jointeam.teamnumber','jointeam.points','jointeam.lastpoints','joinedleauges.id as jid','joinedleauges.userid','joinedleauges.teamid')->get();
					}
					// $findjoinedteams =(isset($joinedteamsarr[$joined->challengeid]))?$joinedteamsarr[$joined->challengeid]:[];
					$gtlastranks = array();
					$getcurrentrankarray = array();
					$ss = 0;
					if(!empty($findjoinedteams)){
						foreach($findjoinedteams as $pleauges){
							$gtlastranks[$ss]['lastpoints'] = $pleauges->lastpoints;
							$gtlastranks[$ss]['userid'] = $pleauges->userid;
							$gtlastranks[$ss]['userjoinid'] = $pleauges->jid;
							$getcurrentrankarray[$ss]['points'] = $pleauges->points;
							$getcurrentrankarray[$ss]['userid'] = $pleauges->userid;
							$getcurrentrankarray[$ss]['userjoinid'] = $pleauges->jid;
							$ss++;
						}
					}
					$gtlastranks = $this->multid_sort($gtlastranks, 'lastpoints');
					if(!empty($gtlastranks)){
						$getuserlastrank=array();
						$lr=0;$lrsno = 0;$uplus=0;
						foreach($gtlastranks as $lrnk){
							if(in_array($lrnk['lastpoints'], array_column($getuserlastrank, 'points'))) { // search value in the array
								$lrsno++;
								$lrsno = $lrsno+$uplus;
								$uplus=0;
							}else{
								$lrsno++;
							}
							$getuserlastrank[$lr]['rank'] = $lrsno;
							$getuserlastrank[$lr]['points'] = $lrnk['lastpoints'];
							$getuserlastrank[$lr]['userid'] = $lrnk['userid'];
							$getuserlastrank[$lr]['userjoinid'] = $lrnk['userjoinid'];
							$lr++;
							
						}
					}
					//get current ranks//
					$gtcurranks = $this->multid_sort($getcurrentrankarray, 'points');
					if(!empty($gtcurranks)){
						$getusercurrank=array();
						$cur=0;$currsno = 0;$plus=0;
						foreach($gtcurranks as $curnk){
							if(!in_array($curnk['points'], array_column($getusercurrank, 'points'))){ // search value in the array
								$currsno++;
								$currsno = $currsno+$plus;
								$plus=0;
								
							}
							else{
								$plus++;
								
							}
							$getusercurrank[$cur]['rank'] = $currsno;
							$getusercurrank[$cur]['points'] = $curnk['points'];
							$getusercurrank[$cur]['userid'] = $curnk['userid'];
							$getusercurrank[$cur]['userjoinid'] = $curnk['userjoinid'];
							$cur++;
							
						}
					}
					if(!empty($findjoinedteams)){
						$k=0;$userrank = 1;$userslistsno=-1; $userrankarray = array();$pdfname="";
						foreach($findjoinedteams as $jointeam){
							if($jointeam->team!=""){
								$Json[$i]['jointeams'][$k]['teamname'] = ucwords($jointeam->team).'(T'.$jointeam->teamnumber.')';
							}
							else{
								$Json[$i]['jointeams'][$k]['teamname'] = $jointeam->email;
							}
							$Json[$i]['jointeams'][$k]['teamid'] = $jointeam->teamid;
							$Json[$i]['jointeams'][$k]['teamnumber'] = $jointeam->teamnumber;
							if($jointeam->userid==$_GET['userid']){
								$Json[$i]['team_number_get'] = $jointeam->teamnumber;
							}
							$Json[$i]['jointeams'][$k]['points'] = $jointeam->points;
							$getuserindexinglast = $this->searchByValue($getuserlastrank,'userjoinid',$jointeam->jid);
							$getlastrank = $getuserlastrank[$getuserindexinglast]['rank'];
							$getuserindexingcurent = $this->searchByValue($getusercurrank,'userjoinid',$jointeam->jid);
							$getcurrentrank = $getusercurrank[$getuserindexingcurent]['rank'];
							$Json[$i]['jointeams'][$k]['getcurrentrank'] = $getcurrentrank;
							if($getlastrank<$getcurrentrank){
								$Json[$i]['jointeams'][$k]['arrowname'] = 'down-arrow';
							}
							else if($getlastrank==$getcurrentrank){
								$Json[$i]['jointeams'][$k]['arrowname'] = 'equal-arrow';
							}
							else if($getlastrank>$getcurrentrank){
								$Json[$i]['jointeams'][$k]['arrowname'] = 'up-arrow';
							}
							$Json[$i]['jointeams'][$k]['userjoinid'] = $jointeam->jid;
							$Json[$i]['jointeams'][$k]['userid'] = $jointeam->userid;
							if($jointeam->userid==$userid){
								$Json[$i]['jointeams'][$k]['is_show'] = true;
								$Json[$i]['jointeams'][$k]['userno'] =$userslistsno;
								if($listmatchdetail->start_date < $today_date) {
									$this->createpdfnew($matchkey);
								}
								if($joined->pdf_created==1 ){
									$pdfname = Config::get('constants.PROJECT_URL').'pdffolders/join-leauges-'.$joined->challengeid.'.pdf';
								}
								$userrankarray[] = $getcurrentrank;
							}
							else{
								$Json[$i]['jointeams'][$k]['is_show'] = false;
								$Json[$i]['jointeams'][$k]['userno'] = 0;
							}
							if(isset($jointeam->amount)){
								if($jointeam->amount!="" && $jointeam->amount!=null){ 
									$Json[$i]['jointeams'][$k]['winingamount']  = $jointeam->amount;
								}else{
									$Json[$i]['jointeams'][$k]['winingamount']="";
								}
							}else{
								$Json[$i]['jointeams'][$k]['winingamount']="";
							}
							$k++;
						}
						array_multisort(array_column($Json[$i]['jointeams'],'userno'),SORT_ASC,array_column($Json[$i]['jointeams'],'points'),SORT_DESC,$Json[$i]['jointeams']);
					}
					if(!empty($userrankarray)){
						$userrank = min($userrankarray);
					}
					$Json[$i]['userrank'] = $userrank;
					$Json[$i]['pdfname'] = $pdfname;
					$Json[$i]['status'] = 1;
					$i++;
				}
			}else{
				$Json[0]['status'] = 0;
			}
			//print_r($Json); exit;
			echo json_encode($Json);
			die;
		}

		public function refreshscores_new() { 
		// Helpers::check_request($_GET['userid'], $_SERVER);
			date_default_timezone_set("Asia/Kolkata");
		$today_date = date('Y-m-d H:i:s');
		$matchkey="";
		if(isset($_GET['matchkey'])){
		    $matchkey = $data['matchkey'] =  $_GET['matchkey'];
		}
		$userid =  $data['userid'] =  $_GET['userid'];
		$Json=array();
		$listmatchdetail = DB::table('listmatches')->where('matchkey',$matchkey)->select('final_status','status','start_date')->first();
		$queryget = DB::table('joinedleauges')->where('joinedleauges.userid',$_GET['userid'])->where('joinedleauges.matchkey',$matchkey)->join('matchchallenges','matchchallenges.id','=','joinedleauges.challengeid');
			// print_r($queryget); exit;
		$queryget->leftJoin('leaderboard', function($join) {
			$join->on('matchchallenges.id','=','leaderboard.challenge_id');
			$join->on('joinedleauges.userid','=','leaderboard.user_id');
		});
		$queryget->leftJoin('finalresults', function($join) {
			$join->on('matchchallenges.id','=','finalresults.challengeid');
			$join->on('joinedleauges.id','=','finalresults.joinedid');
		});
		$queryget->where('matchchallenges.marathon','<>',1);
		$queryget->where(function($query){
			$query->where('matchchallenges.status','=','closed')->where('matchchallenges.status','!=','canceled')
			->orWhere('matchchallenges.confirmed_challenge',1);
		});
		$findmychallenges = $queryget->select(DB::raw('"'.$listmatchdetail->final_status.'" as matchstatus, CONCAT("Win Rs. ", matchchallenges.win_amount) as name, CASE WHEN `matchchallenges`.`joinedusers`= matchchallenges.maximum_user THEN 1 ELSE 0 END AS can_invite, "1" as status, CASE WHEN finalresults.amount IS NULL OR finalresults.amount = "" THEN 0 ELSE finalresults.amount END AS winingamount, CASE WHEN leaderboard.rank IS NULL THEN 1 ELSE leaderboard.rank END AS userrank, CASE WHEN matchchallenges.challenge_type IS NULL THEN "" ELSE matchchallenges.challenge_type END AS challenge_type, CASE WHEN leaderboard.team_number IS NULL THEN 0 ELSE leaderboard.team_number END AS team_number_get, CONCAT(COUNT(joinedleauges.id),"") as join_with'),'matchchallenges.win_amount','matchchallenges.entryfee','matchchallenges.multi_entry','matchchallenges.confirmed_challenge as confirmed','matchchallenges.grand','joinedleauges.teamid','joinedleauges.challengeid as challenge_id','joinedleauges.id as joinid','matchchallenges.joinedusers','matchchallenges.maximum_user','matchchallenges.minimum_user','matchchallenges.is_private','matchchallenges.pdf_created','matchchallenges.winning_percentage','leaderboard.points')->groupBy('matchchallenges.id')->orderBy('leaderboard.points','DESC')->get();
			// print_r($findmychallenges); exit;
		if(!empty($findmychallenges)){
			$i=0;
            $challarr=[];

            foreach($findmychallenges as $joined){
            	if($findmychallenges[$i]->matchstatus=='pending') {
            		$findmychallenges[$i]->matchstatus = 'In progress';	
            	}
            	 
            	$findpricecards=DB::table('matchpricecards')->where('challenge_id',$joined->challenge_id)->where('matchkey',$matchkey)->orderBY('min_position','ASC')->select('challenge_id','winners','price','max_position','min_position')->get(); //print_r($findpricecards); //exit;
            	if($listmatchdetail->start_date < $today_date) {
					$this->createpdfnew($matchkey);
				}
				if($joined->pdf_created==1 ){
					$pdfname = Config::get('constants.PROJECT_URL').'pdffoldedrs/join-leauges-'.$joined->challenge_id.'.pdf';
					$findmychallenges[$i]->pdf = $pdfname;
				}
                if(!empty($findpricecards)){
					$j=0;
					$winners=0;
					foreach($findpricecards as $prc){
						$findmychallenges[$i]->price_card[$j]['id'] = 0;
						// $findmychallenges[$i]['price_card'][$j]['winners'] = $prc->winners;
						$winners+=$prc->winners;
						$findmychallenges[$i]->price_card[$j]['price'] = $prc->price;
						if($prc->min_position+1!=$prc->max_position){
							$findmychallenges[$i]->price_card[$j]['start_position'] = $prc->min_position+1 .'-'. $prc->max_position;
						}else{
							$findmychallenges[$i]->price_card[$j]['start_position'] = $prc->max_position;
						}
						$j++;
					}
					$findmychallenges[$i]->totalwinners = $winners;
					$findmychallenges[$i]->status = 1;
				}else{
					$j=0;
					$findmychallenges[$i]->price_card[$j]['id'] = 0;
					// $findmychallenges[$i]['price_card'][$j]['winners'] = 1;
					$findmychallenges[$i]->price_card[$j]['price'] = $joined->win_amount;
					$findmychallenges[$i]->price_card[$j]['start_position'] = 1;
					$findmychallenges[$i]->totalwinners = 0;
				}
				$i++;
			} 
			echo json_encode($findmychallenges); exit;
		} else {
			$return[0]['status'] = 0;
			echo json_encode($return);
		}
	}

	public function leaderboard() {
		// Helpers::check_request($_GET['userid'], $_SERVER);
		$matchkey="";
		$page_limit = 20;
		if(isset($_GET['matchkey'])){
		    $matchkey = $data['matchkey'] =  $_GET['matchkey'];
		}
		if(isset($_GET['page'])){
		    $page = $data['page'] =  $_GET['page'];
		    $page_offset = (($page - 1) * $page_limit);
		}
		$userid =  $data['userid'] =  $_GET['userid'];
		$challenge_id = $_GET['challenge_id'];
		$queryget = DB::table('leaderboard');
		$queryget->leftJoin('finalresults', function($join) {
			$join->on('finalresults.joinedid','=','leaderboard.join_id');
			$join->on('leaderboard.challenge_id','=','finalresults.challengeid');
		});
		if(isset($page)) {
			$queryget->skip($page_offset)->take($page_limit);
		} else {
			$queryget->skip(0)->take(1000);
		}
		$leaderboard = $queryget->where('leaderboard.challenge_id', $challenge_id)->select(DB::raw('CASE WHEN finalresults.amount IS NULL OR finalresults.amount = "" THEN 0 ELSE finalresults.amount END AS winingamount, CASE WHEN finalresults.rank IS NULL OR finalresults.rank = "" THEN leaderboard.rank ELSE finalresults.rank END AS rank, CASE WHEN finalresults.points IS NULL OR finalresults.points = "" THEN leaderboard.points ELSE finalresults.points END AS points, (SELECT COUNT(leaderboard.id) as joined_count FROM leaderboard WHERE challenge_id = '.$challenge_id.') as joined_count, CONCAT(leaderboard.team_name,"(T",leaderboard.team_number,")") as team_name'),'leaderboard.challenge_id','leaderboard.user_id','leaderboard.team_id','leaderboard.team_number','leaderboard.join_id','leaderboard.arrowname')->groupBy('leaderboard.join_id')->orderBy(DB::raw("FIELD(leaderboard.user_id, ".$userid.")"), 'DESC')->orderBy('leaderboard.rank','ASC')->get();
		if(empty($leaderboard)) {
			$this->league_detail2($matchkey, $challenge_id, $userid);
		} else {
			echo json_encode($leaderboard); exit;
		}
	}

	public function league_detail2($matchkey, $challenge_id, $userid) {
		$league_detail = DB::table('joinedleauges')->join('jointeam','jointeam.id','=','joinedleauges.teamid')->join('registerusers','registerusers.id','=','joinedleauges.userid')->select(DB::raw('1 as `rank` ,"up-arrow" as arrowname, "0" as `points`, "0" as winingamount, (SELECT COUNT(joinedleauges.id) as joined_count FROM joinedleauges WHERE challengeid = '.$challenge_id.') as joined_count,CONCAT(registerusers.team,"(T",jointeam.teamnumber,")") as team_name'),'joinedleauges.id as join_id', 'joinedleauges.challengeid as challenge_id','joinedleauges.teamid as team_id','joinedleauges.userid as user_id','jointeam.teamnumber as team_number')->where('joinedleauges.challengeid', $challenge_id)->orderBy(DB::raw("FIELD(jointeam.userid, ".$userid.")"), 'DESC')->skip(0)->take(1000)->get();
		echo json_encode($league_detail); exit;
	}

		public function league_detail() {
			$matchkey="";
			$page_limit = 20;
			if(isset($_GET['matchkey'])){
			    $matchkey = $data['matchkey'] =  $_GET['matchkey'];
			}
			if(isset($_GET['page'])){
			    $page = $data['page'] =  $_GET['page'];
			    $page_offset = (($page - 1) * $page_limit);
			}
			$userid =  $data['userid'] =  $_GET['userid'];
			$challenge_id = $_GET['challenge_id'];
			$queryget = DB::table('joinedleauges')->join('jointeam','jointeam.id','=','joinedleauges.teamid')->join('registerusers','registerusers.id','=','joinedleauges.userid')->select(DB::raw('"1" as `rank` , "1" as `points`, "0" as winingamount, (SELECT COUNT(joinedleauges.id) as joined_count FROM joinedleauges WHERE challengeid = '.$challenge_id.') as joined_count, CONCAT(registerusers.team, "(T", jointeam.teamnumber,")") as teamname'),'joinedleauges.id as join_id', 'joinedleauges.challengeid as challenge_id','joinedleauges.teamid as teamid','joinedleauges.userid as userid','jointeam.teamnumber as teamnumber')->where('joinedleauges.challengeid', $challenge_id)->orderBy(DB::raw("FIELD(joinedleauges.userid, ".$userid.")"), 'DESC');
			if(isset($page)) {
				$queryget->skip($page_offset)->take($page_limit);
			}
			$league_detail = $queryget->get();

			echo json_encode($league_detail); exit;
		}

		//PDF Code
		public  function createpdfnew($matchdetails){ 
			date_default_timezone_set("Asia/Kolkata");
			$decodematchdetails = DB::table('listmatches')->where('matchkey',$matchdetails)->select('matchkey','title','short_name','start_date','format','name')->first();
			$matchkey = $decodematchdetails->matchkey;
			$findallchallenges = DB::table('matchchallenges')->where('matchkey',$matchkey)->where('pdf_created',0)->where('status','!=','canceled')->select('*')->get();
			$findmatchplayers = DB::table('matchplayers')->where('matchkey',$matchkey)->select('name','playerid')->get();
			$chid = array();
			if(!empty($findallchallenges)){
				foreach($findallchallenges as $chllenge){
					$chid[] = $chllenge->id;
				}
			}
		    if(!empty($chid)){
				$findallusers = DB::table('joinedleauges')->whereIn('challengeid',$chid)->join('registerusers','registerusers.id','=','joinedleauges.userid')->join('jointeam','jointeam.id','=','joinedleauges.teamid')->select('registerusers.team','registerusers.email','jointeam.players','jointeam.captain','jointeam.vicecaptain','jointeam.teamnumber','joinedleauges.id as joinedid','pdfcreate','joinedleauges.challengeid')->get();
				$findallusers = json_decode(json_encode((array) $findallusers), true);
				$countusers = count($findallusers);
				if(!empty($findallusers)){
					foreach($findallchallenges as $chllenge){
						$filterusers=array();
						$filterBy = $chllenge->id;
						$filterusers = array_filter($findallusers, function ($var) use ($filterBy) {
						    return ($var['challengeid'] == $filterBy);
						});
						if(!empty($filterusers)){
						    $this->getPdfDownload($filterusers,$chllenge,$findmatchplayers,$decodematchdetails);
						}
						$challengepdf['pdf_created'] = 1;
						DB::table('matchchallenges')->where('id',$chllenge->id)->update($challengepdf);
					}
				}
				$pdfcreatedata['pdfstatus'] = 1;
				DB::table('listmatches')->where('matchkey',$matchkey)->update($pdfcreatedata);
				return true;
			}
		}

		public function getPdfDownload($findjoinedleauges,$findchallenge,$findmatchplayers,$findmatchdetails){ return true;
		    require_once("./mpdf/mpdf.php");
			$mpdf = new Mpdf();
		
			$mpdf->useSubstitutions = false;
			$mpdf->simpleTables = true;
			$mpdf->SetCompression(true);
			$content="";
			$content='<div class="col-md-12 col-sm-12" style="margin-top:20px;">
				
					<div class="col-md-12 col-sm-12 text-center" style="margin-top:20px;text-align:center">
						<div class="col-md-12 col-sm-12">
							<p> Fanadda Fantasy Cricket </p>
						</div>';
						$content.='<div class="col-md-12 col-sm-12">
							<p> <strong>Pdf Generated On: </strong>'.date('Y-m-d H:i:s a').'</p>
						</div>
					</div>
				</div>';
				$content.='<div class="col-md-12 col-sm-12" style="margin-top:20px;">
							<table style="width:100%" border="1">
							 <tr style="background:#3C7CC4;color:#fff;text-align:center">';
								$challengename = "";
									if($findchallenge->name==""){
										if($findchallenge->win_amount==0){
											$challengename = 'Net Practice';
										}else{
											$challengename = 'Win-'.$findchallenge->win_amount;
										}
									}else{
										$challengename = $findchallenge->name;
									}
								$content.='<th style="color:#fff !important;" colspan="'.(count($findmatchplayers)+1).'">'.$challengename.'( '.$findmatchdetails->short_name.' '.$findmatchdetails->format.' '.$findmatchdetails->start_date.')</th>
								
							  </tr>
							  <tr style="background:#ccc;color:#333;text-align:center">
								<th>Display User Name</th>';
								if(!empty($findmatchplayers)){
										$pn = 1;
										foreach($findmatchplayers as $player1){
											if($pn < 12) {
												//$content.='<th>'.ucwords($player1->name).'</th>';
												$content.='<th>Player '.$pn.'</th>';
											}
											$pn++;
									}
								}
				$content.='</tr>';
				if(!empty($findjoinedleauges)){
					foreach($findjoinedleauges as $joinleauge){
						
						$content.='<tr>
							<td style="text-align:center">';
							if($joinleauge['team']!=""){ 
								$content.=ucwords($joinleauge['team']).'<br> ( '.$joinleauge['teamnumber'].' )';
							} 
							else{
								 $content.= ucwords($joinleauge['email']).'<br> ( '.$joinleauge['teamnumber'].' )';
							}
							$content.='</td>';
							$jointeam = $joinleauge['players'];
							$explodeplayers = explode(',',$jointeam);
							foreach($findmatchplayers as $player2){
								
								
								if(in_array($player2->playerid,$explodeplayers)){
									$content.='<td class="text-center" style="text-align:center;">';
									$content.= $player2->name;
									
									if($player2->playerid==$joinleauge['vicecaptain']){
										$content.= '(VC)';
									}
									if($player2->playerid==$joinleauge['captain']){
										$content.= '(C)';
									}
									$content.='</td>';
								}
								
							} 
						  $content.='</tr>';
					}	
				}
				$content.='</table>
				</div>';
				//echo $content; exit;
				$mpdf->WriteHTML($content);
				$filename = $findchallenge->id;
				//$mpdf->Output();
				$mpdf->Output('pdffolders/join-leauges-'.$filename.'.pdf');
				echo 'join-challenges-'.$filename.'.pdf';
				return 'join-challenges-'.$filename.'.pdf';
		}


		public function countmyleauges(){
			$this->accessrules();
			$matchkey = $data['matchkey'] =  $_GET['matchkey'];
			$userid =  $data['user_id'] =  $_GET['user_id'];
			$findjoinedleauges = DB::table('joinedleauges')->where('joinedleauges.matchkey',$matchkey)->where('joinedleauges.userid',$userid)->select('joinedleauges.id')->groupBy('joinedleauges.challengeid')->get();
			$Json[0]['count'] = count($findjoinedleauges);
			echo json_encode($Json);
			die;
		}
		
		public function leaugeDetails(){
			$this->accessrules();
			$matchkey = $data['matchkey'] =  $_GET['matchkey'];
			$leauge_id = $data['leauge_id']= $_GET['leauge_id'];
			
			$leauge = DB::table('matchchallenges')->where('matchkey',$matchkey)->where('id',$leauge_id)->orderBy('win_amount','DESC')->first();
			
			$resjoined=DB::table('joinedleauges')->where('challengeid',$leauge_id)->join('registerusers','registerusers.id','=','joinedleauges.userid')->join('jointeam','jointeam.id','=','joinedleauges.teamid')->select('registerusers.team','registerusers.email','jointeam.teamnumber','jointeam.points','joinedleauges.teamid','joinedleauges.userid','challengeid')->orderBy('jointeam.points','DESC')->get();
			foreach($resjoined as $joined){
				$jointeamarr[$joined->challengeid][]=$joined;
			}
			 
			$respricecard = DB::table('matchpricecards')->where('challenge_id',$leauge_id)->orderBY('min_position','ASC')->get();
			foreach($respricecard as $joined){
				 $pricecardarr[$joined->challenge_id][]=$joined;
			 }
			 
			 $i=0;
			 
			if($leauge->name==""){
				$Json[$i]['name'] = ucwords('Win Rs. '.$leauge->win_amount);
			}else{
				$Json[$i]['name'] = ucwords($leauge->name);
			}
				//$Json[$i]['teamid'] = $leauge->teamid;
				//$Json[$i]['refercode'] = $leauge->refercode;
				$Json[$i]['winamount'] = $leauge->win_amount;
				$Json[$i]['is_private'] = $leauge->is_private;
				$Json[$i]['ismarathon'] = $leauge->marathon;
				$Json[$i]['grand'] = $leauge->grand;
				$Json[$i]['confirmed'] = $leauge->confirmed_challenge;
				$Json[$i]['multi_entry'] = $leauge->multi_entry;
				//$Json[$i]['joinid'] = $leauge->joinid;
				if($leauge->joinedusers==$leauge->maximum_user){
					$Json[$i]['can_invite'] = false;
				}else{
					$Json[$i]['can_invite'] = true;
				}
				$Json[$i]['joinedusers'] = $leauge->joinedusers;
				$Json[$i]['matchkey'] = $leauge->matchkey;
				$Json[$i]['challenge_id'] = $leauge->id;
				$Json[$i]['entryfee'] = $leauge->entryfee;
				$joinper = ($leauge->joinedusers/$leauge->maximum_user)*100;
				$Json[$i]['getjoinedpercentage'] = $joinper.'%';
				$Json[$i]['maximum_user'] = $leauge->maximum_user;
				
				$findpricecards = (isset($pricecardarr[$leauge->id]))?$pricecardarr[$leauge->id]:[];				//$findpricecards = DB::table('matchpricecards')->where('challenge_id',$joined->challengeid)->orderBY('min_position','ASC')->get();
				
				if(!empty($findpricecards)){
					$j=0;
					$winners=0;
					foreach($findpricecards as $prc){
						$Json[$i]['price_card'][$j]['id'] = $prc->id;
						$Json[$i]['price_card'][$j]['winners'] = $prc->winners;
						$winners+=$prc->winners;
						$Json[$i]['price_card'][$j]['price'] = $prc->price;
						if($prc->min_position+1!=$prc->max_position){
							$Json[$i]['price_card'][$j]['start_position'] = $prc->min_position+1 .'-'. $prc->max_position;
						}else{
							$Json[$i]['price_card'][$j]['start_position'] = $prc->max_position;
						}
						$Json[$i]['price_card'][$j]['total'] = $prc->total;
						$Json[$i]['price_card'][$j]['description'] = $prc->description;
						$j++;
					}
					$Json[$i]['totalwinners'] = $winners;
					$Json[$i]['pricecardstatus'] = 1;
				}
				else{
					$Json[$i]['totalwinners'] = 1;
					$Json[$i]['pricecardstatus'] = 0;
					$Json[$i]['price_card'] = array();
				}
														 //Added by kumar inplace of database query
				$findjoinedteams = (isset($jointeamarr[$joined->id]))?$jointeamarr[$joined->id]:[];
				  
																//$findjoinedteams =DB::table('joinedleauges')->where('challengeid',$joined->challengeid)->join('registerusers','registerusers.id','=','joinedleauges.userid')->join('jointeam','jointeam.id','=','joinedleauges.teamid')->select('registerusers.team','registerusers.email','jointeam.teamnumber','jointeam.points','joinedleauges.teamid','joinedleauges.userid')->orderBy('jointeam.points','DESC')->get();
				$userrank = 0;
				if(!empty($findjoinedteams)){
					$k=0;
					foreach($findjoinedteams as $jointeam){
						if($jointeam->team!=""){
							$Json[$i]['jointeams'][$k]['teamname'] = ucwords($jointeam->team);
						}else{
							$Json[$i]['jointeams'][$k]['teamname'] = $jointeam->email;
						}
						$Json[$i]['jointeams'][$k]['teamid'] = $jointeam->teamid;
						$Json[$i]['jointeams'][$k]['teamnumber'] = $jointeam->teamnumber;
						$Json[$i]['jointeams'][$k]['points'] = $jointeam->points;
						$Json[$i]['jointeams'][$k]['userid'] = $jointeam->userid;
						if($jointeam->userid==$userid){
							$Json[$i]['jointeams'][$k]['is_show'] = true;
							$userrank = $k+1;
						}else{
							$Json[$i]['jointeams'][$k]['is_show'] = false;
						}
						$k++;
					}
				}else{
					$Json[$i]['jointeams']=array();
				}
				$Json[$i]['userrank'] = $userrank;
				$Json[$i]['status'] = 1;
				
			echo json_encode($Json);
			die;
		}
		
		public function myJoinedMatches(){ 
			$this->accessrules();
			$locktime = Carbon::now()->addMinutes(0);
			$geturl = $this->geturl();
			$userid =  $data['userid'] =  $_GET['userid'];
			$findjoinedleauges = DB::table('joinedleauges')->join('listmatches','listmatches.matchkey','=','joinedleauges.matchkey')->join('series','series.id','=','listmatches.series')->join('teams as t1','t1.id','=','listmatches.team1')->join('teams as t2','t2.id','=','listmatches.team2')->join('matchchallenges','matchchallenges.id','=','joinedleauges.challengeid')->where('joinedleauges.userid',$userid)->where('matchchallenges.status','!=','canceled')->select('listmatches.matchkey','listmatches.name','listmatches.short_name','listmatches.team1','listmatches.team2','listmatches.team1display','listmatches.team2display','listmatches.start_date','listmatches.status','listmatches.launch_status','listmatches.final_status','series.name as series','t1.logo as team1logo','t2.logo as team2logo')->selectRaw('COUNT(joinedleauges.userid) as total_joined')->orderBY('listmatches.start_date','desc')->groupBy('joinedleauges.matchkey')->get();
			//print_r($findjoinedleauges); exit;
			if(!empty($findjoinedleauges)){
				$i=0;
				foreach($findjoinedleauges as $match){
					$json[$i]['matchkey'] = $match->matchkey;
					$json[$i]['name'] = $match->name;
					$json[$i]['short_name'] = $match->short_name;
					$json[$i]['team1display'] = strtoupper($match->team1display);
					$json[$i]['team2display'] = strtoupper($match->team2display);
					$joined = 0;
					DB::enableQueryLog();
					$joined = DB::table('joinedleauges')->join('matchchallenges','matchchallenges.id','=','joinedleauges.challengeid')->where('joinedleauges.userid',$userid)->where('matchchallenges.status','!=','canceled')->where('joinedleauges.matchkey', $match->matchkey)->groupBy('joinedleauges.challengeid')->get(); //print_r($joined); exit;
					$json[$i]['joined_count'] = count($joined);
					// $json[$i]['start_date'] = $match->start_date;
					$json[$i]['start_date'] = date('Y-m-d H:i:s', strtotime($match->start_date));
					if($match->start_date<=$locktime){
						$json[$i]['status'] = 'closed';
					}else{
						$json[$i]['status'] = 'opened';
					}
					$json[$i]['launch_status'] = $match->launch_status;
					$json[$i]['final_status'] = $match->final_status;
					$json[$i]['series_name'] = $match->series;
					if($match->team1logo!=""){
						$json[$i]['team1logo'] = $geturl.'uploads/teams/'.$match->team1logo;
					}else{
						$json[$i]['team1logo'] = $geturl.'images/logo.png';
					}
					if($match->team2logo!=""){
						$json[$i]['team2logo'] = $geturl.'uploads/teams/'.$match->team2logo;
					}else{
						$json[$i]['team2logo'] = $geturl.'images/logo.png';
					}
					$i++;
				}
				//print_r($json); exit;
			   echo json_encode($json);
				die;
			}else{
				 $Json = array();
				 echo json_encode($Json);
				  die;
			}
			echo '<pre>'; print_r($findjoinedleauges); die;
		}
		
		public function myjoinedleauges(){
			$this->accessrules();
			$matchkey = $data['matchkey'] =  $_GET['matchkey'];
			$userid =  $data['userid'] =  $_GET['userid'];
			$Json = array();
			$findjoinedleauges = DB::table('joinedleauges')->join('matchchallenges','matchchallenges.id','=','joinedleauges.challengeid')->where('joinedleauges.matchkey',$matchkey)->where('marathon',0)->where('joinedleauges.userid',$userid)->select('joinedleauges.teamid','joinedleauges.challengeid','joinedleauges.id as joinid','joinedleauges.refercode','matchchallenges.*')->groupBy('joinedleauges.challengeid')->orderBy('matchchallenges.win_amount','DESC')->get();
			if(!empty($findjoinedleauges)){
				$i=0;
				$Json = array();
							//Added by kumar 02 May 2018
										 $challarr=$jointeamarr=$pricecardarr=[];
										 foreach($findjoinedleauges as $joined){
											 $challarr[]=$joined->challengeid;
										 }
										 $resjoined=DB::table('joinedleauges')->whereIn('challengeid',$challarr)->join('registerusers','registerusers.id','=','joinedleauges.userid')->join('jointeam','jointeam.id','=','joinedleauges.teamid')->select('registerusers.team','registerusers.email','jointeam.teamnumber','jointeam.points','joinedleauges.teamid','joinedleauges.userid','challengeid')->orderBy('jointeam.points','DESC')->get();
										 foreach($resjoined as $joined){
											 $jointeamarr[$joined->challengeid][]=$joined;
										 }
										 
										$respricecard = DB::table('matchpricecards')->whereIn('challenge_id',$challarr)->orderBY('min_position','ASC')->get();
										foreach($respricecard as $joined){
											 $pricecardarr[$joined->challenge_id][]=$joined;
										 }
										 //end of that
				foreach($findjoinedleauges as $joined){
					if($joined->name==""){
						$Json[$i]['name'] = ucwords('Win Rs. '.$joined->win_amount);
					}else{
						$Json[$i]['name'] = ucwords($joined->name);
					}
					$Json[$i]['teamid'] = $joined->teamid;
					$Json[$i]['refercode'] = $joined->refercode;
					$Json[$i]['winamount'] = $joined->win_amount;
					$Json[$i]['is_private'] = $joined->is_private;
					$Json[$i]['ismarathon'] = $joined->marathon;
					$Json[$i]['grand'] = $joined->grand;
					$Json[$i]['confirmed'] = $joined->confirmed_challenge;
					$Json[$i]['multi_entry'] = $joined->multi_entry;
					$Json[$i]['joinid'] = $joined->joinid;
					if($joined->joinedusers==$joined->maximum_user){
						$Json[$i]['can_invite'] = false;
					}else{
						$Json[$i]['can_invite'] = true;
					}
					$Json[$i]['is_bonus'] = $this->findIfBonus($joined);
					$Json[$i]['joinedusers'] = $joined->joinedusers;
					$Json[$i]['matchkey'] = $joined->matchkey;
					$Json[$i]['challenge_id'] = $joined->challengeid;
					$Json[$i]['entryfee'] = $joined->entryfee;
					$joinper = ($joined->joinedusers/$joined->maximum_user)*100;
					$Json[$i]['getjoinedpercentage'] = $joinper.'%';
					$Json[$i]['maximum_user'] = $joined->maximum_user;
					/* check for marathon */
					if($joined->marathon==1){
						$findmarathon = DB::table('marathon')->where('id',$joined->challenge_id)->first();
						if(!empty($findmarathon)){
							if($findmarathon->joinedusers==$findmarathon->maximum_user){
								$Json[$i]['can_invite'] = false;
							}else{
								$Json[$i]['can_invite'] = true;
							}
							$Json[$i]['joinedusers'] = $findmarathon->joinedusers;
							$findpricecards = DB::table('marathonpricecards')->where('challenge_id',$findmarathon->id)->orderBY('id','ASC')->get();
						}
					}
					else{
														  $findpricecards = (isset($pricecardarr[$joined->challengeid]))?$pricecardarr[$joined->challengeid]:[];				//$findpricecards = DB::table('matchpricecards')->where('challenge_id',$joined->challengeid)->orderBY('min_position','ASC')->get();
					}
					if(!empty($findpricecards)){
						$j=0;
						$winners=0;
						foreach($findpricecards as $prc){
							$Json[$i]['price_card'][$j]['id'] = $prc->id;
							$Json[$i]['price_card'][$j]['winners'] = $prc->winners;
							$winners+=$prc->winners;
							$Json[$i]['price_card'][$j]['price'] = $prc->price;
							if($prc->min_position+1!=$prc->max_position){
								$Json[$i]['price_card'][$j]['start_position'] = $prc->min_position+1 .'-'. $prc->max_position;
							}else{
								$Json[$i]['price_card'][$j]['start_position'] = $prc->max_position;
							}
							$Json[$i]['price_card'][$j]['total'] = $prc->total;
							$Json[$i]['price_card'][$j]['description'] = $prc->description;
							$j++;
						}
						$Json[$i]['totalwinners'] = $winners;
						$Json[$i]['pricecardstatus'] = 1;
					}
					else{
						$Json[$i]['totalwinners'] = 1;
						$Json[$i]['pricecardstatus'] = 0;
					}
															 //Added by kumar inplace of database query
					   $findjoinedteams = (isset($jointeamarr[$joined->challengeid]))?$jointeamarr[$joined->challengeid]:[];
																	//$findjoinedteams =DB::table('joinedleauges')->where('challengeid',$joined->challengeid)->join('registerusers','registerusers.id','=','joinedleauges.userid')->join('jointeam','jointeam.id','=','joinedleauges.teamid')->select('registerusers.team','registerusers.email','jointeam.teamnumber','jointeam.points','joinedleauges.teamid','joinedleauges.userid')->orderBy('jointeam.points','DESC')->get();
					   //print_r($findjoinedteams); exit;
					   $userrank = 0;
					if(!empty($findjoinedteams)){
						$k=0;
						foreach($findjoinedteams as $jointeam){
							if($jointeam->team!=""){
								$Json[$i]['jointeams'][$k]['teamname'] = ucwords($jointeam->team.'(T'.$jointeam->teamnumber.')');
							}else{
								$Json[$i]['jointeams'][$k]['teamname'] = $jointeam->email;
							}
							$Json[$i]['jointeams'][$k]['teamid'] = $jointeam->teamid;
							$Json[$i]['jointeams'][$k]['teamnumber'] = $jointeam->teamnumber;
							$Json[$i]['jointeams'][$k]['points'] = $jointeam->points;
							$Json[$i]['jointeams'][$k]['userid'] = $jointeam->userid;
							if($jointeam->userid==$userid){
								$Json[$i]['jointeams'][$k]['is_show'] = true;
								$userrank = $k+1;
							}else{
								$Json[$i]['jointeams'][$k]['is_show'] = false;
							}
							$k++;
						}
					}
					$Json[$i]['userrank'] = $userrank;
					$Json[$i]['status'] = 1;
				$i++;	
				}
			}
			echo json_encode($Json);
			die;
		}

		public function findIfBonus($findchallengedetails){
		$getbonus = 0;
		if($findchallengedetails->is_private==1 || $findchallengedetails->grand==1){
			$getbonus = 0;
		}
		else{
			if($findchallengedetails->marathon==1){
				$getbonus = 1;
			}
			else{
				if($findchallengedetails->maximum_user>=5){
					$getbonus = 1;
				}else{
					$getbonus = 0;
				}
			}
		}
		return $getbonus;
	}
	
		public function updateteamchallenge(){
			$this->accessrules();
			$matchkey = $data['matchkey'] =  $_GET['matchkey'];
			$teamid = $data['teamid'] =  $_GET['teamid'];
			$joinid = $datau['joinid'] =  $_GET['joinid'];
			$userid = $data['userid'] =  $_GET['userid'];
			$challenge_id = $data['challengeid'] =  $_GET['challenge_id'];
			//match is closed or not//
			$findmatchdetails = DB::table('listmatches')->where('matchkey',$matchkey)->select('start_date')->first();
			if(!empty($findmatchdetails)){
				$getcurrentdate = date('Y-m-d H:i:s');
				$matchremainingdate = date('Y-m-d H:i:s', strtotime('0 minutes', strtotime($findmatchdetails->start_date)));
				if($getcurrentdate>$matchremainingdate){
					$Json[0]['msg'] = 'match closed';
					$Json[0]['status'] = 0;
					echo json_encode($Json);die;
				}
			}
			$findchallenge = DB::table('matchchallenges')->where('id',$challenge_id)->first();
			if(!empty($findchallenge)){
				if($findchallenge->multi_entry==1){
					$findjoinleauge = DB::table('joinedleauges')->where('challengeid',$challenge_id)->where('teamid',$data['teamid'])->first();
					if(!empty($findjoinleauge)){
						$Json[0]['msg'] = 'cannot use this team';
						$Json[0]['status'] = 0;
						echo json_encode($Json);
						die;
					}else{
						DB::table('joinedleauges')->where('id',$joinid)->update($data);
						$findteamnumber = DB::table('jointeam')->where('matchkey',$matchkey)->where('userid',$userid)->where('id',$data['teamid'])->select('teamnumber')->first();
						$Json[0]['msg'] = 'Team Updated';
						$Json[0]['status'] = 1;
						$Json[0]['teamnumber'] = $findteamnumber->teamnumber;
						echo json_encode($Json);
						die;
					}
				}
				else{
						DB::table('joinedleauges')->where('id',$joinid)->update($data);
						$findteamnumber = DB::table('jointeam')->where('matchkey',$matchkey)->where('userid',$userid)->where('id',$data['teamid'])->select('teamnumber')->first();
						$Json[0]['msg'] = 'Team Updated';
						$Json[0]['status'] = 1;
						$Json[0]['teamnumber'] = $findteamnumber->teamnumber;
						echo json_encode($Json);
						die;
					}
			}else{
				$Json[0]['msg'] = 'challenge not exist';
				$Json[0]['status'] = 0;
					echo json_encode($Json);
					die;
			}
		}
		public function createchallenge(){
			    $Json[0]['status'] = 0;
				$Json[0]['message'] = 'Under Maintainance';
				$Json[0]['challengeid'] = 0;
				echo json_encode($Json);
				die;
			$this->accessrules();
			$matchkey = $data['matchkey'] =  $_GET['matchkey'];
			$userid =  $data['created_by'] =  $_GET['userid'];
			$maximum_user = $data['maximum_user'] = $_GET['maximum_user'];
			$win_amount = $data['win_amount'] = $_GET['win_amount'];
			$entryfee = $data['entryfee'] = $_GET['entryfee'];
			$name = $data['name'] = $_GET['name'];
			
			$is_public = $data['is_public'] = $_GET['is_public'];
			
			$is_private = $data['is_private'] = 1;
			if(@$is_public==1) {
				$entryfee = $data['status'] = 'opened';
			} else {
				$entryfee = $data['status'] = 'pending';
			}
			
			
			$data['multi_entry'] = $_GET['multi_entry'];
			
			$matches = DB::table('matchchallenges')->where('matchkey',$matchkey)->where('created_by',$userid)->get();
			
			if(count($matches) > 4){
				$Json[0]['status'] = 0;
				$Json[0]['message'] = false;
				$Json[0]['challengeid'] = 0;
				echo json_encode($Json);
				die;
			}
			
			$challengeid = DB::table('matchchallenges')->insertGetId($data);
			if(isset($_GET['pricecards'])){
				if($_GET['pricecards']!=""){
					$explodesetwinners = explode(',',$_GET['pricecards']);
					$min_position=0;
					$max_position=1;
					if(!empty($explodesetwinners)){
						foreach($explodesetwinners as $setwinners){
							$prcecardata['min_position'] = $min_position;
							$prcecardata['max_position'] = $max_position;
							$prcecardata['winners'] = 1;
							$prcecardata['matchkey'] = $_GET['matchkey'];
							$prcecardata['price'] = $setwinners;
							$prcecardata['total'] = $setwinners;
							$prcecardata['challenge_id'] = $challengeid;
							Db::table('matchpricecards')->insert($prcecardata);
							$min_position = $min_position+1;
							$max_position = $max_position+1;
						}
					}
				}
			}
			$Json[0]['status'] = 1;
			$Json[0]['message'] = true;
			$Json[0]['challengeid'] = $challengeid;
			echo json_encode($Json);
			die;
		}
		public function deductmoney(){
			$findentryfee = 150;
			$finduserbonus = 60;
			$usedbonus=0;$canusedbonus=0;
			$canusedwining=10;
			$canusedbalance=150;
			$reminingfee = $findentryfee;
			$usedbonus = 10;
			if($usedbonus<50){
						if($finduserbonus>0){
							if($finduserbonus>=50){
								$canusedbonus = 50-$usedbonus;
							}else{
								$canusedbonus = $finduserbonus;
							}
							if($canusedbonus>=$findentryfee){
								$remainingbonus1 = $findentryfee-$canusedbonus;
								$remainingbonus = $finduserbonus-$remainingbonus1;
								$dataleft['bonus'] = $remainingbonus;
								$dataused['bonus'] = $findentryfee;
								$reminingfee=0;
							}
							else{
								$reminingfee = $findentryfee-$canusedbonus;
								$remainingbonus = $finduserbonus-$canusedbonus;
								$dataleft['bonus'] = $remainingbonus;
								$dataused['bonus'] = $canusedbonus;
							}
						}
					}
					if($reminingfee>0){
						if($canusedwining>=$reminingfee){
							$reminingwining = $canusedwining-$reminingfee;
							$dataleft['wining'] = $reminingwining;
							$dataused['wining'] = $reminingfee;
							$reminingfee=0;
						}else{
							$dataleft['wining'] = 0;
							$reminingfee = $reminingfee-$canusedwining;
							$dataused['wining'] = $canusedwining;
						}
					}
					if($reminingfee>0){
						if($canusedbalance>=$reminingfee){
							$reminingbalance = $canusedbalance-$reminingfee;
							$dataleft['balance'] = $reminingbalance;
							$dataused['balance'] = $reminingfee;
							$reminingfee=0;
						}
						else{
							$dataleft['balance'] = 0;
							$dataused['balance'] = $reminingfee;
							
						}
					}
		
			
			echo "<pre>";
			print_r($dataleft);
			print_r($dataused);
			die;
		}
		
		public function marathonjoinleauge(){
			$this->accessrules();
			$matchkey = $data['matchkey'] =  $_GET['matchkey'];
			$userid =  $data['userid'] =  $_GET['userid'];
			$challengeid =  $data['challengeid'] = $_GET['challengeid'];
			$teamid =  $data['teamid'] = $_GET['teamid'];
			/* to find user details */
			$userdetailsfind = DB::table('registerusers')->where('id',$userid)->select('username','email')->first();
			
			/* to check the match is closed or not */
			$listmatchestartdate = DB::table('listmatches')->where('matchkey',$matchkey)->join('series','series.id','=','listmatches.series')->select('series.name as seriesname','listmatches.start_date')->first();
			$getcurrentdate = date('Y-m-d H:i:s');
			$matchremainingdate = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($listmatchestartdate->start_date)));
			if($getcurrentdate>$matchremainingdate){
				$Json[0]['message'] = 'match closed';
				echo json_encode($Json);die;
			}
			/* to check from where user belongs */
			$getuserinfo = DB::table('registerusers')->where('id',$_GET['userid'])->select('state')->first();
			if($getuserinfo->state=='Telangana' || $getuserinfo->state=='Orissa' || $getuserinfo->state=='Assam'){
				$Json[0]['message'] = 'ineligible';
				echo json_encode($Json);die;
			}
			/* to check if already joined or not*/
			$findjoinedleauges = DB::table('joinedleauges')->where('challengeid',$_GET['challengeid'])->where('matchkey',$_GET['matchkey'])->where('userid',$_GET['userid'])->select('id')->first();
			if(!empty($findjoinedleauges)){
				$Json[0]['message'] = 'already joined';
				echo json_encode($Json);die;
			}
			/* to create invite code*/
			$Json = array();
			$refercode = $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
			$refercode = '';
			$max = strlen($characters) - 1;
			for ($i = 0; $i < 6; $i++){
				$refercode.= $characters[mt_rand(0, $max)];
			}
			$data['refercode'] = $refercode;
			/* to update user join in marathons */
			$findchallenge = DB::table('matchchallenges')->where('id',$challengeid)->first();
			if($findchallenge->marathon==1){
				$findmarathon = DB::table('marathon')->where('id',$findchallenge->challenge_id)->first();
				if(!empty($findmarathon)){
					$updatedata['joinedusers'] = $findmarathon->joinedusers+1;
					if($updatedata['joinedusers']>$findmarathon->maximum_user){
						$Json[0]['message'] = 'leauge closed';
						echo json_encode($Json);die;
					}
					/*start deduct money */
					$finduserbalance = DB::table('userbalances')->where('user_id',$userid)->first();
					if(!empty($finduserbalance)){
						$findentryfee = $findchallenge->entryfee;
						$dataleft['bonus'] = $finduserbalance->bonus;
						$dataleft['winning'] = $finduserbalance->winning;
						$dataleft['balance'] = $finduserbalance->balance;
						$totalbonus = $finduserbonus = $finduserbalance->bonus;
						$usedbonus = 0;
						$canusedbonus = 0;
						$totalwining = $canusedwining = $finduserbalance->winning;
						$totalbalance = $canusedbalance = $finduserbalance->balance;
						$totalbonususes = $canusedbonus = $finduserbalance->bonus;
						$totbalan = $finduserbalance->bonus + $finduserbalance->winning + $finduserbalance->balance;
						$reminingfee = $findentryfee;
						if($totbalan<$findentryfee){
							$Json[0]['message'] = 'ineligible';
							echo json_encode($Json);die;
						}
						if($reminingfee>0){
							if($canusedbonus>=$reminingfee){
								$reminingbonus = $canusedbonus-$reminingfee;
								$dataleft['bonus'] = $reminingbonus;
								$transactiondata['cons_bonus'] = $dataused['bonus'] = $reminingfee;
								$reminingfee=0;
							}else{
								$dataleft['bonus'] = 0;
								$reminingfee = $reminingfee-$canusedbonus;
								$transactiondata['cons_bonus'] = $dataused['bonus'] = $canusedbonus;
							}
						}
						if($reminingfee>0){
							if($canusedwining>=$reminingfee){
								$reminingwining = $canusedwining-$reminingfee;
								$dataleft['winning'] = $reminingwining;
								$transactiondata['cons_win'] = $dataused['winning'] = $reminingfee;
								$reminingfee=0;
							}else{
								$dataleft['winning'] = 0;
								$reminingfee = $reminingfee-$canusedwining;
								$transactiondata['cons_win'] = $dataused['winning'] = $canusedwining;
							}
						}
						if($reminingfee>0){
							if($canusedbalance>=$reminingfee){
								$reminingbalance = $canusedbalance-$reminingfee;
								$dataleft['balance'] = $reminingbalance;
								$transactiondata['cons_amount'] =  $dataused['balance'] = $reminingfee;
								$reminingfee=0;
							}
							else{
								$dataleft['balance'] = 0;
								$transactiondata['cons_amount'] = $dataused['balance'] = $reminingfee;
								
							}
						}
						//updatewallet table//
						DB::table('userbalances')->where('user_id',$userid)->update($dataleft);
						$findnowamount = DB::table('userbalances')->where('user_id',$userid)->first();
						//end deduct money section//
						// find transaction id//
						$marfindlasttransactionid= DB::table('transactions')->orderBy('id','DESC')->select('id')->first();
						$tranid = 1;
						if(!empty($marfindlasttransactionid)){
							$tranid = $marfindlasttransactionid->id+1;
						}else{
							$tranid = 1;
						}
						//start entry in transaction table//
						$transactiondata['type'] = 'Challenge Joining Fee ';
						$transactiondata['amount'] = $findentryfee;
						$transactiondata['total_available_amt'] = $totbalan - $findentryfee;
						$transactiondata['transaction_id'] = 'Fanadda-JL-'.rand(1000,99999).'-'.$tranid;
						$transactiondata['transaction_by'] = 'wallet';
						$transactiondata['challengeid'] = $challengeid;
						$transactiondata['userid'] = $userid;
						$transactiondata['paymentstatus'] = 'confirmed';
						$transactiondata['bal_bonus_amt'] = $findnowamount->bonus;
						$transactiondata['bal_win_amt'] = $findnowamount->winning;
						$transactiondata['bal_fund_amt'] = $findnowamount->balance;
						$transactiondata['total_available_amt'] = $findnowamount->balance + $findnowamount->winning + $findnowamount->bonus;
						DB::table('transactions')->insert($transactiondata);
						//end entry in transaction table//
						//entry in notification table//
						$notificationdata['userid'] = $userid;
						$notificationdata['title'] = 'Challenge entry fees rs. '.$findentryfee;
						DB::table('notifications')->insert($notificationdata);
						//end entry in notification table//
						//push notifications//
						$titleget = 'Confirmation - joined challenge!';
						Helpers::sendnotification($titleget,$notificationdata['title'],'',$userid);
						//end push notifications//
						//start send mail section//
						$email = $userdetailsfind->email;
						$matchdetails = DB::table('listmatches')->where('matchkey',$matchkey)->first();
						$emailsubject = 'Confirmation - You have joined a marathon challenge !';
						$content='<p><strong>Hello '.ucwords($userdetailsfind->username).'</strong></p>';
						$content.='<p>You have successfully joined the marathon challenge with an entry fee of Rs. '.$findentryfee.' for the '.$listmatchestartdate->seriesname.'. Watch the action unfold on your Fanadda dashboard as your team fight for top spot !!</p>';
						$content.='<p><strong>Rules for this Marathon Challenge:- </strong></p>';
						$content.='<p>1. All matches in this marathon challenge will be from '.$listmatchestartdate->seriesname.' </p>';
						$content.='<p>2. Participation in all matches is NOT mandatory but winner of this challenge will be declared based on total score achieved from your best '.$findmarathon->bestmatches.' performances. </p>';
						$content.='<p>3. Winner of this challenge will be declared only after completion of last match of this series i.e '.$listmatchestartdate->seriesname.'. </p>';
						$content.='<p>4. You will need to create minimum 1 team every match to be able to participate in this challenge. Failing to create a team will give 0 points for that respective match.</p>';
						$content.='<p>5. You will need to create minimum 1 team every match to be able to participate in this challenge. Failing to create a team will give 0 points for that respective match.</p>';
						$content.='<p>6. Abandoned / Cancelled matches will be counted in best score calculations , if applicable.</p>';
						$content.='<p>7. Your best score from less than '.$findmarathon->bestmatches.' matches can also be a winning score if other user’s best score from '.$findmarathon->bestmatches.' matches is less than your score.</p>';
						$content.='<p>8. Fantasy Point system for calculations will be same as that of Standard Challenges.</p>';
						$content.='<p></p>';
						$content.='<p>You may use &quot;download pdf &quot; feature after the match is closed on Fanadda dashboard.</p>';
						$msg = Helpers::mailheader();
						$msg.= Helpers::mailbody($content);
						$msg.= Helpers::mailfooter();
						// Helpers::mailsentFormat($email,$emailsubject,$msg);
						//end send mail section//
						$getinsertid = DB::table('joinedleauges')->insertGetId($data);
						//update matchchallenge //
						DB::table('matchchallenges')->where('id',$challengeid)->update($updatedata);
						DB::table('marathon')->where('id',$findchallenge->challenge_id)->update($updatedata);
						/* updates for previous matches in marathon */
						$fndseriesid = $findchallenge->series_id;
						$locktime = Carbon::now()->addHours(1);
						$finlistmtch = DB::table('listmatches')->where('start_date','>',$locktime)->where('launch_status','launched')->join('jointeam','jointeam.matchkey','=','listmatches.matchkey')->where('jointeam.teamnumber',1)->where('jointeam.userid',$userid)->join('matchchallenges','matchchallenges.matchkey','=','listmatches.matchkey')->where('matchchallenges.series_id',$findchallenge->series_id)->where('matchchallenges.marathon',1)->select('jointeam.id as teamid','listmatches.matchkey','matchchallenges.id as challengeid')->get();
						if(!empty($finlistmtch)){
							foreach($finlistmtch as $lmatch){
								$findleaugejoined = DB::table('joinedleauges')->where('challengeid',$lmatch->challengeid)->where('userid',$userid)->first();
								if(empty($findleaugejoined)){
									$mardata['userid'] = $userid;
									$mardata['challengeid'] = $lmatch->challengeid;
									$mardata['teamid'] = $lmatch->teamid;
									$mardata['matchkey'] = $lmatch->matchkey;
									$mrefercode = $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
									$mrefercode = '';
									$max = strlen($characters) - 1;
									 for ($i = 0; $i < 6; $i++){
										  $mrefercode.= $characters[mt_rand(0, $max)];
									 }
									 $mardata['refercode'] = $mrefercode.rand(10,99);
									 DB::table('joinedleauges')->insert($mardata);
								}
							}
						}
						/* json return */
						$Json[0]['message'] = 'leauge joined';
						$Json[0]['status'] = true;
						$Json[0]['joinedusers'] = $updatedata['joinedusers'];
						$joinper = ($updatedata['joinedusers']/$findchallenge->maximum_user)*100;
						$Json[0]['getjoinedpercentage'] = $joinper.'%';
						if($findchallenge->is_private!="" || $findchallenge->is_private!=0){
							$Json[0]['is_private'] = true;
						}
						else{
							$Json[0]['is_private'] = false;
						}
						if($findchallenge->multi_entry!="" || $findchallenge->multi_entry!=0){
							$Json[0]['multi_entry'] = true;
						}
						else{
							$Json[0]['multi_entry'] = false;
						}
						$Json[0]['refercode'] = $data['refercode'];
						echo json_encode($Json);
						die;
					}
				}
			}
			
		}
		
		public function joinleauge(){ 
		$this->accessrules();
		
		$matchkey = $data['matchkey'] =  $_GET['matchkey'];
		$userid =  $data['userid'] =  $_GET['userid'];
		$listmatchestartdate = DB::table('listmatches')->where('matchkey',$matchkey)->select('start_date')->first();
		$getcurrentdate = date('Y-m-d H:i:s');
		$matchremainingdate = $listmatchestartdate->start_date;
		
		if($getcurrentdate>$matchremainingdate){
			$Json[0]['message'] = 'match closed';
			echo json_encode($Json);die;
		}
		$challengeid =  $data['challengeid'] = $_GET['challengeid'];
		$teamid =  $data['teamid'] = $_GET['teamid'];
		$Json = array();

		$findTeamExist = DB::table('jointeam')->where('userid', $userid)->where('matchkey', $matchkey)->where('id', $teamid)->first();
		if(empty($findTeamExist)) {
			$Json[0]['message'] = 'There is some error please try again.';
			echo json_encode($Json);die;
		}
		// // New condition for max join limit
		// $challange_info = DB::table('matchchallenges')->where('id',$challengeid)->first();
		// $max_joined = $challange_info->maximum_user;
		// $cur_joined = $challange_info->joinedusers;

		// if($max_joined <= $cur_joined) {
		// 	$Json[0]['message'] = 'League already full.';
		// 	echo json_encode($Json);die;	
		// }
		// // New condition for max join limit

		$userdetailsfind = DB::table('registerusers')->where('id',$userid)->where('activation_status','!=','deactivated')->select('username','email','state','email_verify')->first();
		if(empty($userdetailsfind)) {
			$Json[0]['message'] = 'There is some error please login again.';
			echo json_encode($Json);die;
		}
		$refercode = $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$refercode = '';
		$max = strlen($characters) - 1;
		 for ($i = 0; $i < 6; $i++){
			  $refercode.= $characters[mt_rand(0, $max)];
		 }
		 $data['refercode'] = $refercode;
		 $findchallenge = DB::table('matchchallenges')->where('id',$challengeid)->where('matchkey',$matchkey)->first();
		 if(empty($findchallenge)) {
		 	$Json[0]['message'] = 'There is some error please try again.';
			echo json_encode($Json);die;
		 }
		 if($findchallenge->status=='closed'){
				$Json[0]['message'] = 'leauge closed';
				echo json_encode($Json);die;
			}
			if($findchallenge->win_amount>0){
				if($userdetailsfind->state=='Telangana' || $userdetailsfind->state=='Orissa' || $userdetailsfind->state=='Assam'){
					$Json[0]['message'] = 'ineligible';
					echo json_encode($Json);die;
				}
			}
			$cur_joined = $findchallenge->joinedusers;
			//find leauges already exist or not //
			$findexistornot = DB::table('joinedleauges')->where('userid',$userid)->where('challengeid',$challengeid)->where('matchkey',$matchkey)->select('teamid')->get();
			if(!empty($findexistornot)){
				if($findchallenge->multi_entry==0){
					$Json[0]['message'] = 'already joined';
					echo json_encode($Json);die;
				}else{
					foreach($findexistornot as $ff){
						if($ff->teamid==$teamid){
							$Json[0]['message'] = 'team already joined';
							echo json_encode($Json);die;
						}
					}
				}
			}
			//check for leauge closed or not //
			$dataused = array();
			$dataleft = array();
			$updatedata['joinedusers'] = $findchallenge->joinedusers+1;
			// if($findchallenge->challenge_type == 'money'){
			// 	if($updatedata['joinedusers']>$findchallenge->maximum_user){
			// 		$Json[0]['message'] = 'leauge closed';
			// 		echo json_encode($Json);die;
			// 	}
			// }
			
			//start deduct money code for join leauge//
			$finduserbalance = DB::table('userbalances')->where('user_id',$userid)->lockForUpdate()->first();
			if(!empty($finduserbalance)){
				$findentryfee = $findchallenge->entryfee;
				$dataleft['bonus'] = $findbonusforuser = $finduserbalance->bonus;
				$dataleft['winning'] = $finduserbalance->winning;
				$dataleft['balance'] = $finduserbalance->balance;
				$totalbonus = $finduserbonus = $finduserbalance->bonus;
				$usedbonus = 0;
				$canusedbonus = 0;
				$totalwining = $canusedwining = $finduserbalance->winning;
				$totalbalance = $canusedbalance = $finduserbalance->balance;
				$totbalan = $finduserbalance->bonus + $finduserbalance->winning + $finduserbalance->balance;
				$findusablebalance = $finduserbalance->balance+$finduserbalance->winning;
				$reminingfee = $findentryfee;

				$bonus_used = DB::table('leaugestransactions')->where('matchkey', $matchkey)->where('user_id', $userid)->select(DB::raw('SUM(bonus) as bonus'))->first();
				if($bonus_used->bonus && $bonus_used->bonus >= 50) {
					$finduserbonus = 0;
				} else {
					$finduserbonus_pre = 50 - $bonus_used->bonus;
					if($finduserbonus > $finduserbonus_pre) {
						$finduserbonus = $finduserbonus_pre;
					}
				}

				if($userdetailsfind->email_verify!=1) {
					$finduserbonus = 0;
				}

				//find bonus//
				if($findchallenge->bonus == 1){
					$bonus_precent = $findchallenge->bonus_percent;
					$bonus_entry_fee = $findentryfee * $bonus_precent / 100;
					// if(($findusablebalance+($finduserbonus*$bonus_precent/100))<$findentryfee)
					$findusablebalance = $findusablebalance+ $finduserbonus;
					// $finduserbonus = ($finduserbonus * $bonus_precent) / 100;

					if(($finduserbalance->balance+$finduserbalance->winning) < ($findentryfee-$bonus_entry_fee)) {
						$Json[0]['message'] = 'insufficient balance';
						echo json_encode($Json);die;
					}
				}
				//start deduct money section//
				if($findusablebalance<$findentryfee){
					$Json[0]['message'] = 'insufficient balance';
					echo json_encode($Json);die;
				}
				
				if($findchallenge->bonus == 1){
					if($finduserbonus >= $bonus_entry_fee){
						$remainingbonus = $finduserbonus-$bonus_entry_fee;
						$dataleft['bonus'] = $dataleft['bonus'] - $bonus_entry_fee;
						$transactiondata['cons_bonus'] = $dataused['bonus'] = $bonus_entry_fee;
						$reminingfee=$findentryfee - $bonus_entry_fee;
					}else{
						$reminingfee = $findentryfee-$finduserbonus;
						$remainingbonus = 0;
						$dataleft['bonus'] = $dataleft['bonus'] - $finduserbonus;
						$transactiondata['cons_bonus'] = $dataused['bonus'] = $finduserbonus;
					}
				}
				
		
				if($totalbalance >= $reminingfee){
					$dataleft['balance'] = $totalbalance - $reminingfee;
					$transactiondata['cons_amount'] = $dataused['balance'] = $reminingfee;
					$reminingfee=0;
				}else{
					$reminingfee = $reminingfee-$totalbalance;
					$dataleft['balance'] = 0;
					$transactiondata['cons_amount'] = $dataused['balance'] = $totalbalance;
				}
				
				
				if($totalwining >= $reminingfee){
					$dataleft['winning'] = $totalwining - $reminingfee;
					$transactiondata['cons_win'] = $dataused['winning'] = $reminingfee;
					$reminingfee=0;
				}
				else{
					$reminingfee = $reminingfee-$totalwining;
					$dataleft['winning'] = 0;
					$transactiondata['cons_win'] = $dataused['winning'] = $totalwining;
				}
				// yha tk
				$tranid = time();
				// to enter in joined leauges table//
				$data['transaction_id'] = 'FP11-JL-'.$tranid.'-'.$userid;
				// check again//
				// $findexistornot = DB::table('joinedleauges')->where('userid',$userid)->where('challengeid',$challengeid)->where('matchkey',$matchkey)->select('teamid')->get();
				// if(!empty($findexistornot)){
				// 	if($findchallenge->multi_entry==0){
				// 		$Json[0]['message'] = 'already joined';
				// 		echo json_encode($Json);die;
				// 	}
				// 	else{
				// 		foreach($findexistornot as $ff){
				// 			if($ff->teamid==$teamid){
				// 				$Json[0]['message'] = 'team already joined';
				// 				echo json_encode($Json);die;
				// 			}
				// 		}
				// 	}
				// }
				//entry in challenges tables//
				
				//insert leauge entry//
				$data['refercode'] = $refercode.'-'.time().''.$userid;
				// $findchallengeagain = DB::table('matchchallenges')->where('id',$challengeid)->select('status')->first();
				// if($findchallengeagain->status=='closed'){
				// 	$Json[0]['message'] = 'leauge closed';
				// 	echo json_encode($Json);die;
				// }


				// New condition for max join limit
					// $challange_info = DB::table('matchchallenges')->where('id',$challengeid)->first();
					// $max_joined = $challange_info->maximum_user;
					// $joined_list = DB::table('joinedleauges')->where('challengeid',$challengeid)->where('matchkey',$matchkey)->select(DB::raw('SQL_NO_CACHE joinedleauges.teamid'))->get();
					// $cur_joined = count($joined_list);

				// 	if($max_joined <= $cur_joined) {
				// 		if($findchallenge->challenge_type == 'money'){
				// 			$updatedata['status']='closed';
				// 		}
				// 		$updatedata['joinedusers']=$cur_joined;
				// // if($findchallenge->is_running==1){
				// // 	//new duplicate challenge//
				// // 			$newentry['matchkey'] = $findchallenge->matchkey;
				// // 			$newentry['name'] = $findchallenge->name;
				// // 			$newentry['challenge_id'] = $findchallenge->id;
				// // 			$newentry['entryfee'] = $findchallenge->entryfee;
				// // 			$newentry['challenge_type'] = $findchallenge->challenge_type;
				// // 			$newentry['win_amount'] = $findchallenge->win_amount;
				// // 			$newentry['maximum_user'] = $findchallenge->maximum_user;
				// // 			$newentry['bonus'] = $findchallenge->bonus;
				// // 			$newentry['multi_entry'] = $findchallenge->multi_entry;
				// // 			$newentry['confirmed_challenge'] = $findchallenge->confirmed_challenge;
				// // 			$newentry['is_running'] = $findchallenge->is_running;
				// // 			//$getcid = DB::table('matchchallenges')->insertGetId($newentry);
				// // 			$findpricecards = DB::table('matchpricecards')->where('challenge_id',$findchallenge->id)->where('matchkey',$findchallenge->matchkey)->get();
				// // 			if(!empty($findpricecards)){
				// // 				foreach($findpricecards as $pricec){
				// // 				    $pdata = array();
				// // 					$pdata['challenge_id'] = $getcid;
				// // 					$pdata['matchkey'] = $findchallenge->matchkey;
				// // 					$pdata['winners'] = $pricec->winners;
				// // 					$pdata['price'] = $pricec->price;
				// // 					$pdata['min_position'] = $pricec->min_position;
				// // 					$pdata['max_position'] = $pricec->max_position;
				// // 					$pdata['description'] = $pricec->description;
				// // 					$pdata['total'] = $pricec->total;
				// // 					DB::table('matchpricecards');
				// // 					//DB::table('matchpricecards')->insert($pdata);
				// // 				}
				// // 			}
				// // 		}
				// 		if($findchallenge->challenge_type == 'money'){
				// 			DB::table('matchchallenges')->where('id',$challengeid)->update($updatedata);
				// 			$Json[0]['message'] = 'League already full.';
				// 			echo json_encode($Json);die;
				// 		}	
				// 	}
				// 	// New condition for max join limit
				// $result = DB::insert("INSERT INTO `joinedleauges` ( `userid`, `challengeid`, `teamid`, `matchkey`, `refercode`, `transaction_id`, `pdfcreate`, `pdfname`) SELECT ".$userid." , ".$challengeid.", ".$teamid.", '".$matchkey."', '".$data['refercode']."', '".$data['transaction_id']."', '0', '' FROM DUAL WHERE (SELECT COUNT(*) FROM joinedleauges WHERE challengeid=$challengeid) < ".$findchallenge->maximum_user); print_r($result);
				// 		$challenge_insert_id = DB::select('SELECT LAST_INSERT_ID()');
				// 		print_r($challenge_insert_id); exit;
				try {
					if($findchallenge->challenge_type == 'percentage') {
						$result = DB::insert("INSERT INTO `joinedleauges` ( `userid`, `challengeid`, `teamid`, `matchkey`, `refercode`, `transaction_id`, `pdfcreate`, `pdfname`) SELECT ".$userid." , ".$challengeid.", ".$teamid.", '".$matchkey."', '".$data['refercode']."', '".$data['transaction_id']."', '0', '' FROM DUAL");
							$challenge_insert_id = DB::select('SELECT LAST_INSERT_ID()');
					} else {
						$result = DB::insert("INSERT INTO `joinedleauges` ( `userid`, `challengeid`, `teamid`, `matchkey`, `refercode`, `transaction_id`, `pdfcreate`, `pdfname`) SELECT ".$userid." , ".$challengeid.", ".$teamid.", '".$matchkey."', '".$data['refercode']."', '".$data['transaction_id']."', '0', '' FROM DUAL WHERE (SELECT COUNT(*) FROM joinedleauges WHERE challengeid=$challengeid) < ".$findchallenge->maximum_user);
							$challenge_insert_id = DB::select('SELECT LAST_INSERT_ID()');
						} 
						$getinsertid = $challenge_insert_id[0]->{"LAST_INSERT_ID()"}; //echo $getinsertid; exit;
						if(!$getinsertid) {//if(!$getinsertid) {
								//if($findchallenge->loops==0) {
								$newentry['matchkey'] = $findchallenge->matchkey;
								$newentry['name'] = $findchallenge->name;
								$newentry['challenge_id'] = $findchallenge->id;
								$newentry['entryfee'] = $findchallenge->entryfee;
								$newentry['challenge_type'] = $findchallenge->challenge_type;
								$newentry['winning_percentage'] = $findchallenge->winning_percentage;
								$newentry['win_amount'] = $findchallenge->win_amount;
								$newentry['maximum_user'] = $findchallenge->maximum_user;
								$newentry['bonus'] = $findchallenge->bonus;
								$newentry['bonus_precent'] = $findchallenge->bonus_precent;
								$newentry['multi_entry'] = $findchallenge->multi_entry;
								$newentry['confirmed_challenge'] = $findchallenge->confirmed_challenge;
								$newentry['is_running'] = $findchallenge->is_running;
								$newentry['challenge_category_id'] = $findchallenge->challenge_category_id;
								$findifexist = DB::table('matchchallenges')->where('challenge_id',$findchallenge->id)->get();
								if(empty($findifexist)) {
								$getcid = DB::table('matchchallenges')->insertGetId($newentry);
								$findpricecards = DB::table('matchpricecards')->where('challenge_id',$findchallenge->id)->where('matchkey',$findchallenge->matchkey)->get();
								if(!empty($findpricecards)){
									foreach($findpricecards as $pricec){
									    $pdata = array();
										$pdata['challenge_id'] = $getcid;
										$pdata['matchkey'] = $findchallenge->matchkey;
										$pdata['winners'] = $pricec->winners;
										$pdata['price'] = $pricec->price;
										$pdata['min_position'] = $pricec->min_position;
										$pdata['max_position'] = $pricec->max_position;
										$pdata['description'] = $pricec->description;
										$pdata['total'] = $pricec->total;
										DB::table('matchpricecards');
										DB::table('matchpricecards')->insert($pdata);
									}
								}
								}
								//}
								$updatedata_new['status'] = 'closed';
								$updatedata_new['joinedusers'] = $findchallenge->maximum_user; //print_r($findchallenge->id); exit;
								try {
									$affectedRows = DB::table('matchchallenges')->where('id',$findchallenge->id)->update($updatedata_new);
								} catch(\Illuminate\Database\QueryException $ex) {
									dd($ex->getMessage());
								}
								
								$Json[0]['message'] = 'League already full.';
								echo json_encode($Json);die;
						}
				} catch(\Illuminate\Database\QueryException $ex) {
					dd($ex->getMessage());
				}
				
				try {
						DB::beginTransaction();
						
						//entry in leauges transactions//
							$dataused['matchkey'] = $matchkey;
							$dataused['user_id'] = $userid;
							$dataused['challengeid'] = $challengeid;
							$dataused['joinid'] = $getinsertid;
							DB::table('leaugestransactions')->insert($dataused);
							//updatewallet table//
							DB::table('userbalances')->where('user_id',$userid)->update($dataleft);
							$findnowamount = DB::table('userbalances')->where('user_id',$userid)->first();
							//end deduct money section//
							//start entry in transaction table//
							
							$transactiondata['type'] = 'Challenge Joining Fee';
							$transactiondata['amount'] = $findentryfee;
							$transactiondata['total_available_amt'] = $totbalan - $findentryfee;
							$transactiondata['transaction_by'] = 'wallet';
							$transactiondata['challengeid'] = $challengeid;
							$transactiondata['userid'] = $userid;
							$transactiondata['paymentstatus'] = 'confirmed';
							$transactiondata['bal_bonus_amt'] = $findnowamount->bonus;
							$transactiondata['bal_win_amt'] = $findnowamount->winning;
							$transactiondata['bal_fund_amt'] = $findnowamount->balance;
							$transactiondata['transaction_id'] = $data['transaction_id'];
							DB::table('transactions')->insert($transactiondata);
							DB::commit();
				// $getinsertid = DB::table('joinedleauges')->insertGetId($data);
				} catch(\Illuminate\Database\QueryException $ex) { //print_r($ex->getMessage()); exit;
					DB::rollback();
					$message = $ex->getCode();
					if($message==45000) {
				//if(!$getinsertid) {
				
					$newentry['matchkey'] = $findchallenge->matchkey;
					$newentry['name'] = $findchallenge->name;
					$newentry['challenge_id'] = $findchallenge->id;
					$newentry['entryfee'] = $findchallenge->entryfee;
					$newentry['challenge_type'] = $findchallenge->challenge_type;
					$newentry['winning_percentage'] = $findchallenge->winning_percentage;
					$newentry['win_amount'] = $findchallenge->win_amount;
					$newentry['maximum_user'] = $findchallenge->maximum_user;
					$newentry['bonus'] = $findchallenge->bonus;
					$newentry['bonus_percent'] = $findchallenge->bonus_percent;
					$newentry['multi_entry'] = $findchallenge->multi_entry;
					$newentry['confirmed_challenge'] = $findchallenge->confirmed_challenge;
					$newentry['is_running'] = $findchallenge->is_running;
					// $newentry['challenge_category_id'] = $findchallenge->challenge_category_id;
					$findifexist = DB::table('matchchallenges')->where('challenge_id',$findchallenge->id)->get();
					if(empty($findifexist)) {
					$getcid = DB::table('matchchallenges')->insertGetId($newentry);
					$findpricecards = DB::table('matchpricecards')->where('challenge_id',$findchallenge->id)->where('matchkey',$findchallenge->matchkey)->get();
					if(!empty($findpricecards)){
						foreach($findpricecards as $pricec){
						    $pdata = array();
							$pdata['challenge_id'] = $getcid;
							$pdata['matchkey'] = $findchallenge->matchkey;
							$pdata['winners'] = $pricec->winners;
							$pdata['price'] = $pricec->price;
							$pdata['min_position'] = $pricec->min_position;
							$pdata['max_position'] = $pricec->max_position;
							$pdata['description'] = $pricec->description;
							$pdata['total'] = $pricec->total;
							DB::table('matchpricecards');
							DB::table('matchpricecards')->insert($pdata);
						}
					}
					}
					
					$updatedata_new['status'] = 'closed';
					$updatedata_new['joinedusers'] = $findchallenge->maximum_user; //print_r($findchallenge->id); exit;
					try {
						$affectedRows = DB::table('matchchallenges')->where('id',$findchallenge->id)->update($updatedata_new);
					} catch(\Illuminate\Database\QueryException $ex) {
						dd($ex->getMessage());
					}
					
					$Json[0]['message'] = 'League already full.';
					echo json_encode($Json);die;
					}
				} 
				// //Rollback if joined count exceed
				// $joined_list_new = DB::table('joinedleauges')->where('challengeid',$challengeid)->where('matchkey',$matchkey)->select(DB::raw('SQL_NO_CACHE joinedleauges.teamid'))->get();
				// $cur_joined_new = count($joined_list_new);
				// if($max_joined < $cur_joined_new) {
				// 	DB::rollback();
				// 	$Json[0]['message'] = 'League already full.';
				// 		echo json_encode($Json);die;	
				// }

				
				//end entry in transaction table//
				//entry in notification table//
				$notificationdata['userid'] = $userid;
				$notificationdata['title'] = 'Challenge entry fees Rs.'.$findentryfee;
				//DB::table('notifications')->insert($notificationdata);
				$titleget = 'Confirmation - joined challenge!';
				//Helpers::sendnotification($titleget,$notificationdata['title'],'',$userid);
			}
			$updatedata_new = array();

			if(($cur_joined+1)>=$findchallenge->maximum_user){
				//close challenge//
				// print_r($findchallenge); exit;
				$updatedata['status']='closed';
					$updatedata_new['status'] = 'closed';
				if($findchallenge->is_running==1){
					
					//new duplicate challenge//
					$newentry['matchkey'] = $findchallenge->matchkey;
					$newentry['name'] = $findchallenge->name;
					$newentry['challenge_id'] = $findchallenge->id;
					$newentry['entryfee'] = $findchallenge->entryfee;
					$newentry['challenge_type'] = $findchallenge->challenge_type;
					$newentry['win_amount'] = $findchallenge->win_amount;
					$newentry['maximum_user'] = $findchallenge->maximum_user;
					$newentry['bonus'] = $findchallenge->bonus;
					$newentry['bonus_percent'] = $findchallenge->bonus_percent;
					$newentry['multi_entry'] = $findchallenge->multi_entry;
					$newentry['confirmed_challenge'] = $findchallenge->confirmed_challenge;
					$newentry['is_running'] = $findchallenge->is_running;

					// $newentry['challenge_category_id'] = $findchallenge->challenge_category_id;
					$findifexist = DB::table('matchchallenges')->where('challenge_id',$findchallenge->id)->get();
					if(empty($findifexist)) {
					$getcid = DB::table('matchchallenges')->insertGetId($newentry);

					$findpricecards = DB::table('matchpricecards')->where('challenge_id',$findchallenge->id)->where('matchkey',$findchallenge->matchkey)->get();
					if(!empty($findpricecards)){
						foreach($findpricecards as $pricec){
						    $pdata = array();
							$pdata['challenge_id'] = $getcid;
							$pdata['matchkey'] = $findchallenge->matchkey;
							$pdata['winners'] = $pricec->winners;
							$pdata['price'] = $pricec->price;
							$pdata['min_position'] = $pricec->min_position;
							$pdata['max_position'] = $pricec->max_position;
							$pdata['description'] = $pricec->description;
							$pdata['total'] = $pricec->total;
							DB::table('matchpricecards');
							DB::table('matchpricecards')->insert($pdata);
						}
					}
					}
				}
			}
			
			//$updatedata_new['status'] = 'closed';
			$updatedata_new['joinedusers'] = $cur_joined+1;
			DB::table('matchchallenges')->where('id',$challengeid)->update($updatedata_new);
			$Json[0]['message'] = 'leauge joined';
			$Json[0]['status'] = true;
			$Json[0]['grand'] = $findchallenge->grand;
			$Json[0]['joinedusers'] = $updatedata_new['joinedusers'];
			if($findchallenge->is_private!="" || $findchallenge->is_private!=0){
				$Json[0]['is_private'] = true;
			}
			else{
				$Json[0]['is_private'] = false;
			}
			if($findchallenge->multi_entry!="" || $findchallenge->multi_entry!=0){
				$Json[0]['multi_entry'] = true;
			}
			else{
				$Json[0]['multi_entry'] = false;
			}
			$Json[0]['refercode'] = $data['refercode'];
			
			echo json_encode($Json);
			die;
	}
		public function getmatchlist(){ 
			$this->accessrules();
			$geturl = $this->geturl();
			$aftertime = Carbon::now()->addDays(15);
			$beforetime = Carbon::now()->subDays(15);
			$locktime = Carbon::now()->addMinutes(0);
			$query = DB::table('listmatches');
			if(isset($_GET['series'])){
				$series = $_GET['series'];
				$query->where('listmatches.series',$series);
			}
	// 		$findmatches = $query->leftjoin('series','listmatches.series','=','series.id')->join('teams as t1','t1.id','=','listmatches.team1')->join('teams as t2','t2.id','=','listmatches.team2')->select('t1.logo as team1logo','t2.logo as team2logo','t1.short_name as team1key','t2.short_name as team2key','listmatches.id as listmatchid','listmatches.series as seriesid','series.name as seriesname','listmatches.name','listmatches.start_date','listmatches.format','listmatches.matchkey','listmatches.final_status')->Where('listmatches.launch_status','launched')->orderBY('listmatches.start_date','desc')->where('series.series_status','opened')->get();
			$findmatches = $query->leftjoin('series','listmatches.series','=','series.id')->join('teams as t1','t1.id','=','listmatches.team1')->join('teams as t2','t2.id','=','listmatches.team2')->select('t1.logo as team1logo','t2.logo as team2logo','t1.short_name as team1key','t2.short_name as team2key','listmatches.id as listmatchid','listmatches.series as seriesid','series.name as seriesname','listmatches.name','listmatches.start_date','listmatches.format','listmatches.matchkey','listmatches.final_status','listmatches.launch_status')->Where('listmatches.series','!=','0')->orderBY('listmatches.start_date','desc')->where('series.series_status','opened')->get();
			
			$Json=array();
			if(!empty($findmatches)){
				$i=0;$matchshow=0;
				foreach($findmatches as $match){
					$Json[$i]['id'] = $match->listmatchid;
					$Json[$i]['name'] = $match->name;
					$Json[$i]['format'] = $match->format;
					$Json[$i]['series'] = $match->seriesid;
					$Json[$i]['seriesname'] = $match->seriesname;
					$Json[$i]['team1name'] = strtoupper($match->team1key);
					$Json[$i]['team2name'] = strtoupper($match->team2key);
					$Json[$i]['matchkey'] = $match->matchkey;
					$Json[$i]['winnerstatus'] = $match->final_status;
					$Json[$i]['launch_status'] = $match->launch_status;
					if($match->team1logo!=""){
						$Json[$i]['team1logo'] = $geturl.'uploads/teams/'.$match->team1logo;
					}else{
						$Json[$i]['team1logo'] = $geturl.'images/logo.png';
					}
					if($match->team2logo!=""){
						$Json[$i]['team2logo'] = $geturl.'uploads/teams/'.$match->team2logo;
					}else{
						$Json[$i]['team2logo'] = $geturl.'images/logo.png';
					}
					if($match->start_date<=$locktime){
						$Json[$i]['matchopenstatus'] = 'closed';
					} else {
						$matchshow++;
						$Json[$i]['matchopenstatus'] = 'opened';
					}
					if($matchshow==1){
						$Json[$i]['matchindexing'] = $match->listmatchid;
					}else{
						$Json[$i]['matchindexing']="";
					}
					//$Json[$i]['time_start'] = date('Y-m-d H:i:s', strtotime('-30 minutes', strtotime($match->start_date)));
					$Json[$i]['time_start'] = date('Y-m-d H:i:s', strtotime($match->start_date));
					$Json[$i]['locktime'] = $locktime;
					$Json[$i]['timedifference'] =  strtotime($Json[$i]['time_start']) - strtotime(date('Y-m-d H:i:s'));

					if(isset($_GET['userid'])){
						$finduserinfo = DB::table('registerusers')->where('id',$_GET['userid'])->select('id')->first();
						if(!empty($finduserinfo)){
							$getid = $finduserinfo->id;
							$findjointeam = DB::table('jointeam')->where('userid',$getid)->where('matchkey',$match->matchkey)->orderBY('id','DESC')->get();
							if(!empty($findjointeam)){
								$Json[$i]['createteamnumber'] = $findjointeam[0]->teamnumber+1;
							}else{
								$Json[$i]['createteamnumber'] = 1;
							}
						}
					}
					$Json[$i]['status'] = '1';
					$i++;
				}
			}
			else{
				$Json[0]['status'] = '0';
			}
			echo json_encode($Json);
			die;
		}
		
		public function fantasyscorecards(){
			$this->accessrules();
			$getmatchkey = $_GET['matchkey'];
			$findplayers = DB::table('result_points')->join('result_matches','result_matches.id','=','result_points.resultmatch_id')->join('listmatches','listmatches.matchkey','=','result_points.matchkey')->join('players','players.id','=','result_points.playerid')->join('teams','teams.id','=','players.team')->select('result_points.*','players.player_name as playername','listmatches.title','result_matches.innings','result_matches.batting','result_matches.bowling','teams.team as teamname')->where('result_points.matchkey',$getmatchkey)->orderBy('result_matches.innings','ASC')->orderBy('teams.team','ASC')->get();
			$findmatchname = DB::table('listmatches')->where('matchkey',$getmatchkey)->select('title','short_name')->first();
			$json = array();
			// echo "<pre>";
			// print_r($findplayers);
			// die;
			if(!empty($findmatchname)){
				if(!empty($findplayers)){
				$i=0;
				foreach($findplayers as $player){
					if($player->batting==1 || $player->bowling==1){
						$Json[$i]['playername'] = $player->playername;
						$Json[$i]['matchname'] = $player->title;
						$number = $player->innings; 
						$ends = array('th','st','nd','rd','th','th','th','th','th','th');
						if (($number %100) >= 11 && ($number%100) <= 13)
						   $abbreviation = $number. 'th';
						else
						   $abbreviation = $number. $ends[$number % 10];
										
						$Json[$i]['innings'] =ucwords($player->teamname).' '. $abbreviation .' inning';
						$Json[$i]['startingpoints'] = $player->startingpoints;
						$Json[$i]['runs'] = $player->runs;
						$Json[$i]['fours'] = $player->fours;
						$Json[$i]['sixs'] = $player->sixs;
						$Json[$i]['strike_rate'] = $player->strike_rate;
						$Json[$i]['halcentury'] = $player->halcentury;
						$Json[$i]['century'] = $player->century;
						$Json[$i]['point150'] = $player->point150;
						$Json[$i]['point200'] = $player->point200;
						$Json[$i]['wickets'] = $player->wickets;
						$Json[$i]['maidens'] = $player->maidens;
						$Json[$i]['economy_rate'] = $player->economy_rate;
						$Json[$i]['winner_point'] = $player->winner_point;
						$Json[$i]['not_out'] = $player->not_out;
						$Json[$i]['catch'] = $player->catch;
						$Json[$i]['stumping'] = $player->stumping;
						$Json[$i]['bonus'] = $player->bonus;
						$Json[$i]['negative'] = $player->negative;
						$Json[$i]['total'] = $player->total;
						$Json[$i]['matchname'] = $findmatchname->title;
						$Json[$i]['matchshortname'] = $findmatchname->short_name;
						$Json[$i]['batting'] = $player->batting;
						$Json[$i]['bowling'] = $player->bowling;
						$Json[$i]['status'] = 1;
						$i++;
					}
				}
				}else{
					$Json[0]['status'] = 0;
				}
			}else{
				$Json[0]['status'] = 0;
				$i++;
			}
			// echo "<pre>";
			// print_r($Json);
			// die;
			echo json_encode($Json);
			die;
		}
		public function findfantasyplayers(){
			$this->accessrules();
			$matchkey = $_GET['matchkey'];
			$findmatchstatus = DB::table('listmatches')->where('matchkey',$matchkey)->select('final_status')->first();
			if(!empty($findmatchstatus)){
				if($findmatchstatus->final_status=='winnerdeclared'){
					if(!Cache::has('fanatsyplayer'.$matchkey)){
						$findallplayers = DB::table('matchplayers')->where('matchkey',$matchkey)->select('playerid','points')->get();
						$playersid=array();
						$i=0;
						if(!empty($findallplayers)){
							foreach($findallplayers as $players){
								$playersid[$i]['id'] = $players->playerid; 
								$playersid[$i]['points'] = $players->points;
								$i++;				
							}
						}
						$findjoinedleauges = DB::table('jointeam')->where('jointeam.matchkey',$matchkey)->select('jointeam.players')->get();
						$joinplayers = array();
						$joinplayerslist = array();
						$mergeplayers = array();
						if(!empty($findjoinedleauges)){
							foreach($findjoinedleauges as $joinleauges){
								$joinplayers[] = $joinleauges->players;
							}
						}
						if(!empty($joinplayers)){
							foreach($joinplayers as $jp=>$jpkey){
								$joinplayerslist[] = explode(',',$jpkey);
							}
						}
						$aa=array();
						$aab = array();
						foreach($playersid as $pp){
							$cnt=0;
							foreach($joinplayerslist as $jn){
								if(in_array($pp['id'],$jn)){
									$cnt++;
									$aab[$pp['id']]=$cnt;
								}
							}
						}
						// rsort($aab);
						$newarr=array();
						arsort($aab);
						$jncnt=count($joinplayerslist);
						foreach($aab as $kk=>$vv){
							$newarr[$kk]['per']=round(($vv*100)/$jncnt,2);
							$newarr[$kk]['cnt']=$vv;
							$newarr[$kk]['playerid']=$kk;
							$findplayerdetails = DB::table('players')->where('id',$kk)->select('player_name')->first();
							$newarr[$kk]['playername'] = ucwords($findplayerdetails->player_name);
						}
						$newi=0;$Json=array();
						if(!empty($newarr)){
							$newarr = array_slice($newarr,0,4);
							  foreach($newarr as $new){
								  $Json[$newi]['percentage'] = $new['per'];
								  // $Json[$newi]['countsuers'] = $new['cnt'];
								  // $Json[$newi]['playerid'] = $new['playerid'];
								  $findcaptainscount = DB::table('jointeam')->where('captain',$new['playerid'])->groupby('teamnumber')->count();
								  if($findcaptainscount>0){
									  $Json[$newi]['captainper'] =  round(($findcaptainscount*100)/$jncnt,2);
								  }else{
									  $Json[$newi]['captainper'] = 0;
								  }
								  $findvicecount = DB::table('jointeam')->where('vicecaptain',$new['playerid'])->groupby('teamnumber')->count();
								  if($findvicecount>0){
									  $Json[$newi]['vicecaptianper'] =  round(($findvicecount*100)/$jncnt,2);
								  }else{
									  $Json[$newi]['vicecaptianper'] = 0;
								  }
								  $Json[$newi]['playername'] = $new['playername'];
								  $Json[$newi]['status'] = 1;
								  $newi++;
							  }
						}
						else{
							$Json[0]['status'] = 0;
						}
						$getvalue = Cache::forever('fanatsyplayer'.$matchkey, $Json);
					}
					else{
						$Json = Cache::pull('fanatsyplayer'.$matchkey);
					}
				}
				else{
					$Json[0]['status'] = 0;
				}
			}
			else{
				$Json[0]['status'] = 0;
			}
			
			echo json_encode($Json);
			die;
		}
		public function findtopbatsman(){
			$this->accessrules();
			$matchkey = $_GET['matchkey'];
			$findjoinedleauges = DB::table('jointeam')->where('jointeam.matchkey',$matchkey)->select('jointeam.players')->get();
			$joinedlength = count($findjoinedleauges);
			$findallplayers = DB::table('matchplayers')->where('matchkey',$matchkey)->where('role','batsman')->where('points','>','0')->orderBy('points','DESC')->limit(3)->get();
			$joinplayers = array();
			$joinplayerslist = array();
			if(!empty($findjoinedleauges)){
				foreach($findjoinedleauges as $joinleauges){
					$joinplayers[] = $joinleauges->players;
				}
			}
			if(!empty($joinplayers)){
				foreach($joinplayers as $jp=>$jpkey){
					$joinplayerslist[] = explode(',',$jpkey);
				}
			}
			$i=0;
			$Json=array();
			if(!empty($findallplayers)){
				foreach($findallplayers as $player){
					$Json[$i]['status'] = 1;
					$Json[$i]['credits'] = $player->credit;
					$Json[$i]['playername'] = $player->name;
					$Json[$i]['playerid'] = $player->playerid;
					$Json[$i]['points'] = $player->points;
					$cnt=0;
					if(!empty($joinplayerslist)){
						foreach($joinplayerslist as $jn){
							if(in_array($player->playerid,$jn)){
								$cnt++;
								$Json[$i]['countusers']=$cnt;
							}
						}
						$Json[$i]['per']=round(($cnt*100)/$joinedlength,2);
					}else{
						$Json[$i]['countusers'] = 0;
						$Json[$i]['per'] = 0;
					}
					$i++;
					
				}
			}else{
				$Json[$i]['status'] = 0;
			}
			echo json_encode($Json);
			die;
		}
		public function findtopbowlers(){
			$this->accessrules();
			$matchkey = $_GET['matchkey'];
			$findjoinedleauges = DB::table('jointeam')->where('jointeam.matchkey',$matchkey)->select('jointeam.players')->get();
			$joinedlength = count($findjoinedleauges);
			$findallplayers = DB::table('matchplayers')->where('matchkey',$matchkey)->where('role','bowler')->where('points','>','0')->orderBy('points','DESC')->limit(3)->get();
			$joinplayers = array();
			$joinplayerslist = array();
			if(!empty($findjoinedleauges)){
				foreach($findjoinedleauges as $joinleauges){
					$joinplayers[] = $joinleauges->players;
				}
			}
			if(!empty($joinplayers)){
				foreach($joinplayers as $jp=>$jpkey){
					$joinplayerslist[] = explode(',',$jpkey);
				}
			}
			$i=0;
			$Json=array();
			if(!empty($findallplayers)){
				foreach($findallplayers as $player){
					$Json[$i]['status'] = 1;
					$Json[$i]['playername'] = $player->name;
					$Json[$i]['credits'] = $player->credit;
					$Json[$i]['playerid'] = $player->playerid;
					$Json[$i]['points'] = $player->points;
					$cnt=0;
					if(!empty($joinplayerslist)){
						foreach($joinplayerslist as $jn){
							if(in_array($player->playerid,$jn)){
								$cnt++;
								$Json[$i]['countusers']=$cnt;
							}
						}
						$Json[$i]['per']=round(($cnt*100)/$joinedlength,2);
					}else{
						$Json[$i]['countusers'] = 0;
						$Json[$i]['per'] = 0;
					}
					$i++;
					
				}
			}else{
				$Json[$i]['status'] = 0;
			}
			echo json_encode($Json);
			die;
		}
		public function findtopallrounders(){
			$this->accessrules();
			$matchkey = $_GET['matchkey'];
			$findjoinedleauges = DB::table('jointeam')->where('jointeam.matchkey',$matchkey)->select('jointeam.players')->get();
			$joinedlength = count($findjoinedleauges);
			$findallplayers = DB::table('matchplayers')->where('matchkey',$matchkey)->where('role','allrounder')->where('points','>','0')->orderBy('points','DESC')->limit(3)->get();
			$joinplayers = array();
			$joinplayerslist = array();
			if(!empty($findjoinedleauges)){
				foreach($findjoinedleauges as $joinleauges){
					$joinplayers[] = $joinleauges->players;
				}
			}
			if(!empty($joinplayers)){
				foreach($joinplayers as $jp=>$jpkey){
					$joinplayerslist[] = explode(',',$jpkey);
				}
			}
			$i=0;
			$Json=array();
			if(!empty($findallplayers)){
				foreach($findallplayers as $player){
					$Json[$i]['status'] = 1;
					$Json[$i]['playername'] = $player->name;
					$Json[$i]['credits'] = $player->credit;
					$Json[$i]['playerid'] = $player->playerid;
					$Json[$i]['points'] = $player->points;
					$cnt=0;
					if(!empty($joinplayerslist)){
						foreach($joinplayerslist as $jn){
							if(in_array($player->playerid,$jn)){
								$cnt++;
								$Json[$i]['countusers']=$cnt;
							}
						}
						$Json[$i]['per']=round(($cnt*100)/$joinedlength,2);
					}else{
						$Json[$i]['countusers'] = 0;
						$Json[$i]['per'] = 0;
					}
					$i++;
					
				}
			}else{
				$Json[$i]['status'] = 0;
			}
			echo json_encode($Json);
			die;
		}
		public function findtopwk(){
			$this->accessrules();
			$matchkey = $_GET['matchkey'];
			$findjoinedleauges = DB::table('jointeam')->where('jointeam.matchkey',$matchkey)->select('jointeam.players')->get();
			$joinedlength = count($findjoinedleauges);
			$findallplayers = DB::table('matchplayers')->where('matchkey',$matchkey)->where('role','keeper')->where('points','>','0')->orderBy('points','DESC')->limit(1)->get();
			$joinplayers = array();
			$joinplayerslist = array();
			if(!empty($findjoinedleauges)){
				foreach($findjoinedleauges as $joinleauges){
					$joinplayers[] = $joinleauges->players;
				}
			}
			if(!empty($joinplayers)){
				foreach($joinplayers as $jp=>$jpkey){
					$joinplayerslist[] = explode(',',$jpkey);
				}
			}
			$i=0;
			$Json=array();
			if(!empty($findallplayers)){
				foreach($findallplayers as $player){
					$Json[$i]['status'] = 1;
					$Json[$i]['playername'] = $player->name;
					$Json[$i]['credits'] = $player->credit;
					$Json[$i]['playerid'] = $player->playerid;
					$Json[$i]['points'] = $player->points;
					$cnt=0;
					if(!empty($joinplayerslist)){
						foreach($joinplayerslist as $jn){
							if(in_array($player->playerid,$jn)){
								$cnt++;
								$Json[$i]['countusers']=$cnt;
							}
						}
						$Json[$i]['per']=round(($cnt*100)/$joinedlength,2);
					}else{
						$Json[$i]['countusers'] = 0;
						$Json[$i]['per'] = 0;
					}
					$i++;
					
				}
			}else{
				$Json[$i]['status'] = 0;
			}
			echo json_encode($Json);
			die;
		}
		
		public function playerfullinfo(){
			$this->accessrules();
			$geturl = $this->geturl();
			$playerid = $_GET['playerid'];
			$matchkey = $_GET['matchkey'];
			$findseries = DB::table('listmatches')->where('matchkey',$matchkey)->select('series')->first();
			$findplayerdetails = DB::table('matchplayers')->join('players','players.id','=','matchplayers.playerid')->join('playerdetails','playerdetails.player_key','=','players.player_key')->where('matchplayers.matchkey',$matchkey)->where('matchplayers.playerid',$playerid)->select('playerdetails.fullname as playername','matchplayers.credit as playercredit','matchplayers.role as playerrole','players.points as playerpoints','playerdetails.image as playerimage','players.player_key as playerkey','playerdetails.batting_style','playerdetails.bowling_style','playerdetails.country','playerdetails.dob')->first();
			$Json = array();
			$findjoinedleauges = DB::table('jointeam')->where('jointeam.matchkey',$matchkey)->select('jointeam.players')->get();
			$joinedlength = count($findjoinedleauges);
			$joinplayers = array();
			$joinplayerslist = array();
			if(!empty($findjoinedleauges)){
				foreach($findjoinedleauges as $joinleauges){
					$joinplayers[] = $joinleauges->players;
				}
			}
			if(!empty($joinplayers)){
				foreach($joinplayers as $jp=>$jpkey){
					$joinplayerslist[] = explode(',',$jpkey);
				}
			}
			if(!empty($findplayerdetails)){
				$totalpoints = 0;
				$Json['playername'] = $findplayerdetails->playername;
				// $Json['totalpoints'] = "354";
				$Json['playerkey'] = $findplayerdetails->playerkey;
				$Json['playercredit'] = $findplayerdetails->playercredit;
				if($findplayerdetails->playerimage==""){
					$Json['playerimage'] = $geturl.'images/defaultimage.png';
				}else{
					$Json['playerimage'] = $geturl.'uploads/players/'.$findplayerdetails->playerimage;
				}
				$Json['playerpoints'] = $findplayerdetails->playerpoints;
				$Json['battingstyle'] = $findplayerdetails->batting_style;
				$Json['bowlingstyle'] = $findplayerdetails->bowling_style;
				$Json['country'] = $findplayerdetails->country;
				if($findplayerdetails->dob!='0000-00-00'){
					$Json['dob'] = $findplayerdetails->dob;
				}else{
					$Json['dob'] = "";
				}
				
				if($findplayerdetails->playerrole=='allrounder'){
					$Json['playerrole'] = 'All Rounder';
				}
				else if($findplayerdetails->playerrole=='keeper'){
					$Json['playerrole'] = 'Wicket Keeper';
				}
				else{
					$Json['playerrole'] = ucwords($findplayerdetails->playerrole);
				}
				$getteam="";
				$findallteams = DB::table('players')->where('player_key',$findplayerdetails->playerkey)->join('teams','teams.id','=','players.team')->select('teams.team')->get();
				if(!empty($findallteams)){
					foreach($findallteams as $teams){
						$getteam.=$teams->team.', ';
					}
				}
				$Json['teams'] =  rtrim($getteam,', ');
				$cnt=0;
				if(!empty($joinplayerslist)){
					foreach($joinplayerslist as $jn){
						if(in_array($playerid,$jn)){
							$cnt++;
						}
					}
					$Json['per']=round((($cnt*100)/$joinedlength),2);
				}else{
					$Json['per'] = 0;
				}

				$findallmatches = DB::table('listmatches')->join('matchplayers','matchplayers.matchkey','=','listmatches.matchkey')->join('players','players.id','=','matchplayers.playerid')->leftjoin('result_points','result_points.matchkey','=','listmatches.matchkey')->where('result_points.playerid',$playerid)->where('listmatches.series',$findseries->series)->select('listmatches.title','listmatches.matchkey','listmatches.start_date','listmatches.short_name','result_points.total as totalpoints','result_points.id as rid','players.player_name as playername','players.id as player_id','result_points.*')->groupBy('listmatches.matchkey')->get();
				// $findallmatches = DB::table('result_matches')->join('listmatches','listmatches.matchkey','=','result_matches.match_key')->where('result_matches.player_id',$playerid)->where('listmatches.series',$findseries->series)->select('result_matches.*','listmatches.start_date','listmatches.title','listmatches.short_name')->orderBy('listmatches.start_date','DESC')->get();
				if(!empty($findallmatches)){
					$i=0;
					foreach($findallmatches as $player){
						$Json['matches'][$i]['playername'] = $findplayerdetails->playername;
						$Json['matches'][$i]['matchname'] = $player->title;
						$Json['matches'][$i]['short_name'] = $player->short_name;
						$Json['matches'][$i]['total_points'] = $player->totalpoints;
						$totalpoints += $player->totalpoints;
						$finduselectthisplayer = DB::table('jointeam')->where('matchkey',$player->matchkey)->select('players')->get();
						$countlenght = count($finduselectthisplayer);
						$allplayers=array();$countplayer=0;
						if(!empty($finduselectthisplayer)){
							foreach($finduselectthisplayer as $pp){
								$fplayers = array();
								$fplayers = explode(',',$pp->players);
								foreach($fplayers as $fpl){
									$allplayers[] = $fpl;
								}
							}
						}
						if(!empty($allplayers)){
							foreach($allplayers as $pl){
								if($pl==$player->playerid){
									$countplayer++;
								}
							}
						}
						if($countplayer>0){
							$countper = round(($countplayer/$countlenght)*100,2);
						}else{
							$countper = 0;
						}
						$Json['matches'][$i]['selectper'] = $countper.'%';
						$Json['matches'][$i]['matchdate'] = date('d M, Y',strtotime($player->start_date));
						$i++;
					}
					

				}
				$Json['total_points'] = (string)$totalpoints;
				
			}
			echo json_encode(array($Json));
			die;
		}
		
		public function getplayerinfo(){
			$this->accessrules();
			$geturl = $this->geturl();
			$playerkey = $_GET['playerkey'];
			$findplayerdetails = DB::table('playerdetails')->where('player_key',$playerkey)->first();
			$Json = array();
			if(!empty($findplayerdetails)){
				$Json['playername'] = $findplayerdetails->fullname;
				$Json['playerkey'] = $findplayerdetails->player_key;
				if($findplayerdetails->image==""){
					$Json['playerimage'] = $geturl.'images/defaultimage.png';
				}else{
					$Json['playerimage'] = $geturl.'uploads/players/'.$findplayerdetails->image;
				}
				$Json['battingstyle'] = $findplayerdetails->batting_style;
				$Json['bowlingstyle'] = $findplayerdetails->bowling_style;
				$Json['country'] = $findplayerdetails->country;
				if($findplayerdetails->dob!='0000-00-00'){
					$Json['dob'] = $findplayerdetails->dob;
				}else{
					$Json['dob'] = "";
				}
				$getteam="";
				$findallteams = DB::table('players')->where('player_key',$findplayerdetails->player_key)->join('teams','teams.id','=','players.team')->select('teams.team')->get();
				if(!empty($findallteams)){
					foreach($findallteams as $teams){
						$getteam.=$teams->team.', ';
					}
				}
				$Json['teams'] =  rtrim($getteam,', ');
				$Json['status'] =  1;
			}else{
				$Json['status'] =  0;
			}
			echo json_encode(array($Json));
			die;
			
			
			
			
			
			
			
				
					// $Json[0]['test']['batting']['innings'] = $getplayerinfo['stats']['test']['batting']['innings'];
					// $Json[0]['test']['batting']['matches'] = $getplayerinfo['stats']['test']['batting']['matches'];
					// $Json[0]['test']['batting']['fours'] = $getplayerinfo['stats']['test']['batting']['fours'];
					// $Json[0]['test']['batting']['hundreds'] = $getplayerinfo['stats']['test']['batting']['hundreds'];
					// $Json[0]['test']['batting']['sixes'] = $getplayerinfo['stats']['test']['batting']['sixes'];
					// $Json[0]['test']['batting']['runs'] = $getplayerinfo['stats']['test']['batting']['runs'];
					// $Json[0]['test']['batting']['balls'] = $getplayerinfo['stats']['test']['batting']['balls'];
					// $Json[0]['test']['batting']['average'] = $getplayerinfo['stats']['test']['batting']['average'];
					// $Json[0]['test']['batting']['strike_rate'] = $getplayerinfo['stats']['test']['batting']['strike_rate'];
					// $Json[0]['test']['batting']['not_outs'] = $getplayerinfo['stats']['test']['batting']['not_outs'];
					// $Json[0]['test']['batting']['fifties'] = $getplayerinfo['stats']['test']['batting']['fifties'];
					// $Json[0]['test']['batting']['high_score'] = $getplayerinfo['stats']['test']['batting']['high_score'];
					
					// $Json[0]['test']['bowling']['matches'] = $getplayerinfo['stats']['test']['bowling']['matches'];
					// $Json[0]['test']['bowling']['wickets'] = $getplayerinfo['stats']['test']['bowling']['wickets'];
					// $Json[0]['test']['bowling']['ten_wickets'] = $getplayerinfo['stats']['test']['bowling']['ten_wickets'];
					// $Json[0]['test']['bowling']['five_wickets'] = $getplayerinfo['stats']['test']['bowling']['five_wickets'];
					// $Json[0]['test']['bowling']['innings'] = $getplayerinfo['stats']['test']['bowling']['innings'];
					// $Json[0]['test']['bowling']['runs'] = $getplayerinfo['stats']['test']['bowling']['runs'];
					// $Json[0]['test']['bowling']['balls'] = $getplayerinfo['stats']['test']['bowling']['balls'];
					// $Json[0]['test']['bowling']['average'] = $getplayerinfo['stats']['test']['bowling']['average'];
					// $Json[0]['test']['bowling']['strike_rate'] = $getplayerinfo['stats']['test']['bowling']['strike_rate'];
					// $Json[0]['test']['bowling']['economy'] = $getplayerinfo['stats']['test']['bowling']['economy'];
					// $Json[0]['test']['bowling']['best_match_bowling'] = $getplayerinfo['stats']['test']['bowling']['best_match_bowling'];
					// $Json[0]['test']['bowling']['best_innings_bowling'] = $getplayerinfo['stats']['test']['bowling']['best_innings_bowling'];
					//t20//
					// $Json[0]['t20']['batting']['innings'] = $getplayerinfo['stats']['t20']['batting']['innings'];
					// $Json[0]['t20']['batting']['matches'] = $getplayerinfo['stats']['t20']['batting']['matches'];
					// $Json[0]['t20']['batting']['fours'] = $getplayerinfo['stats']['t20']['batting']['fours'];
					// $Json[0]['t20']['batting']['hundreds'] = $getplayerinfo['stats']['t20']['batting']['hundreds'];
					// $Json[0]['t20']['batting']['sixes'] = $getplayerinfo['stats']['t20']['batting']['sixes'];
					// $Json[0]['t20']['batting']['runs'] = $getplayerinfo['stats']['t20']['batting']['runs'];
					// $Json[0]['t20']['batting']['balls'] = $getplayerinfo['stats']['t20']['batting']['balls'];
					// $Json[0]['t20']['batting']['average'] = $getplayerinfo['stats']['t20']['batting']['average'];
					// $Json[0]['t20']['batting']['strike_rate'] = $getplayerinfo['stats']['t20']['batting']['strike_rate'];
					// $Json[0]['t20']['batting']['not_outs'] = $getplayerinfo['stats']['t20']['batting']['not_outs'];
					// $Json[0]['t20']['batting']['fifties'] = $getplayerinfo['stats']['t20']['batting']['fifties'];
					// $Json[0]['t20']['batting']['high_score'] = $getplayerinfo['stats']['t20']['batting']['high_score'];
					
					// $Json[0]['t20']['bowling']['matches'] = $getplayerinfo['stats']['t20']['bowling']['matches'];
					// $Json[0]['t20']['bowling']['wickets'] = $getplayerinfo['stats']['t20']['bowling']['wickets'];
					// $Json[0]['t20']['bowling']['ten_wickets'] = $getplayerinfo['stats']['t20']['bowling']['ten_wickets'];
					// $Json[0]['t20']['bowling']['five_wickets'] = $getplayerinfo['stats']['t20']['bowling']['five_wickets'];
					// $Json[0]['t20']['bowling']['innings'] = $getplayerinfo['stats']['t20']['bowling']['innings'];
					// $Json[0]['t20']['bowling']['runs'] = $getplayerinfo['stats']['t20']['bowling']['runs'];
					// $Json[0]['t20']['bowling']['balls'] = $getplayerinfo['stats']['t20']['bowling']['balls'];
					// $Json[0]['t20']['bowling']['average'] = $getplayerinfo['stats']['t20']['bowling']['average'];
					// $Json[0]['t20']['bowling']['strike_rate'] = $getplayerinfo['stats']['t20']['bowling']['strike_rate'];
					// $Json[0]['t20']['bowling']['economy'] = $getplayerinfo['stats']['t20']['bowling']['economy'];
					// $Json[0]['t20']['bowling']['best_match_bowling'] = $getplayerinfo['stats']['t20']['bowling']['best_match_bowling'];
					// $Json[0]['t20']['bowling']['best_innings_bowling'] = $getplayerinfo['stats']['t20']['bowling']['best_innings_bowling'];
					//one-day//
					// $Json[0]['oneday']['batting']['innings'] = $getplayerinfo['stats']['one-day']['batting']['innings'];
					// $Json[0]['oneday']['batting']['matches'] = $getplayerinfo['stats']['one-day']['batting']['matches'];
					// $Json[0]['oneday']['batting']['fours'] = $getplayerinfo['stats']['one-day']['batting']['fours'];
					// $Json[0]['oneday']['batting']['hundreds'] = $getplayerinfo['stats']['one-day']['batting']['hundreds'];
					// $Json[0]['oneday']['batting']['sixes'] = $getplayerinfo['stats']['one-day']['batting']['sixes'];
					// $Json[0]['oneday']['batting']['runs'] = $getplayerinfo['stats']['one-day']['batting']['runs'];
					// $Json[0]['oneday']['batting']['balls'] = $getplayerinfo['stats']['one-day']['batting']['balls'];
					// $Json[0]['oneday']['batting']['average'] = $getplayerinfo['stats']['one-day']['batting']['average'];
					// $Json[0]['oneday']['batting']['strike_rate'] = $getplayerinfo['stats']['one-day']['batting']['strike_rate'];
					// $Json[0]['oneday']['batting']['not_outs'] = $getplayerinfo['stats']['one-day']['batting']['not_outs'];
					// $Json[0]['oneday']['batting']['fifties'] = $getplayerinfo['stats']['one-day']['batting']['fifties'];
					// $Json[0]['oneday']['bowling']['matches'] = $getplayerinfo['stats']['one-day']['bowling']['matches'];
					// $Json[0]['oneday']['bowling']['wickets'] = $getplayerinfo['stats']['one-day']['bowling']['wickets'];
					// $Json[0]['oneday']['bowling']['ten_wickets'] = $getplayerinfo['stats']['one-day']['bowling']['ten_wickets'];
					// $Json[0]['oneday']['bowling']['five_wickets'] = $getplayerinfo['stats']['one-day']['bowling']['five_wickets'];
					// $Json[0]['oneday']['bowling']['innings'] = $getplayerinfo['stats']['one-day']['bowling']['innings'];
					// $Json[0]['oneday']['bowling']['runs'] = $getplayerinfo['stats']['one-day']['bowling']['runs'];
					// $Json[0]['oneday']['bowling']['balls'] = $getplayerinfo['stats']['one-day']['bowling']['balls'];
					// $Json[0]['oneday']['bowling']['average'] = $getplayerinfo['stats']['one-day']['bowling']['average'];
					// $Json[0]['oneday']['bowling']['strike_rate'] = $getplayerinfo['stats']['one-day']['bowling']['strike_rate'];
					// $Json[0]['one-day']['bowling']['economy'] = $getplayerinfo['stats']['one-day']['bowling']['economy'];
					// $Json[0]['oneday']['bowling']['best_match_bowling'] = $getplayerinfo['stats']['one-day']['bowling']['best_match_bowling'];
				
		}
		
		
		public function livematches(){
			$this->accessrules();
			$findalllivematches = DB::table('listmatches')->where('status','started')->where('launch_status','launched')->select('title','short_name','start_date','matchkey','format')->get();
			$i=0;
			$Json=array();
			if(!empty($findalllivematches)){
				foreach($findalllivematches as $livematch){
					$Json[$i]['status'] = 1;
					$Json[$i]['matchkey'] = $livematch->matchkey;
					$Json[$i]['title'] = $livematch->title;
					$Json[$i]['short_name'] = $livematch->short_name;
					$Json[$i]['format'] = $livematch->format;
					$Json[$i]['start_date'] = date('M d Y h:i:a',strtotime($livematch->start_date));
					$i++;
				}
			}
			else{
				$Json[$i]['status'] = 0;
			}
			echo json_encode($Json);
			die;
		}
		public function completedmatch(){
			$this->accessrules();
			$findalllivematches = DB::table('listmatches')->where('status','completed')->where('launch_status','launched')->select('title','short_name','start_date','matchkey','format','winner_team')->orderBy('start_date','DESC')->get();
			$i=0;
			$Json=array();
			if(!empty($findalllivematches)){
				foreach($findalllivematches as $livematch){
					$Json[$i]['status'] = 1;
					$Json[$i]['matchkey'] = $livematch->matchkey;
					$Json[$i]['title'] = $livematch->title;
					$Json[$i]['short_name'] = $livematch->short_name;
					$Json[$i]['format'] = $livematch->format;
					if($livematch->winner_team!=""){
						$findteamname = DB::table('teams')->where('team_key',$livematch->winner_team)->first();
						if(!empty($findteamname)){
							$Json[$i]['winner_team'] = ucwords($findteamname->team);
						}
					}
					$Json[$i]['start_date'] = date('M d Y h:i:a',strtotime($livematch->start_date));
					$i++;
				}
			}else{
				$Json[$i]['status'] = 0;
			}
			echo json_encode($Json);
			die;
		}
		public function viewscorecard(){
			$this->accessrules();
			$matchkey = $_GET['matchkey'];
			$findalllivematches = DB::table('listmatches')->join('teams as t1','t1.id','=','listmatches.team1')->join('teams as t2','t2.id','=','listmatches.team2')->where('matchkey',$matchkey)->select('listmatches.title','listmatches.short_name','listmatches.start_date','listmatches.matchkey','listmatches.format','listmatches.team1','listmatches.team2','t1.team as team1name','t2.team as team2name','winner_team')->first();
			$Json=array();
			if(!empty($findalllivematches)){
				$team1 = $findalllivematches->team1;
				$team1name = $findalllivematches->team1name;
				$team2 = $findalllivematches->team2;
				$team2name = $findalllivematches->team2name;
				$findinnings = DB::table('result_matches')->where('match_key',$matchkey)->groupBy('innings')->select('innings')->get();
				$countinnings = count($findinnings);
				$team2members = array();
				$team1members = array();
				for($i=0;$i<=$countinnings;$i++){
					$findteam1players = DB::table('result_matches')->where('match_key',$matchkey)->where('innings',$i+1)->join('players','players.id','=','result_matches.player_id')->where('players.team',$team1)->select('players.player_name','result_matches.*')->get();
					$battingi=0;
					$bowlingi=0;
					if(!empty($findteam1players)){
						foreach($findteam1players as $team1player){
							if($team1player->batting==1){
								$Json['inning'][$i]['batting1'][$battingi]['playername'] = $team1player->player_name;
								$Json['inning'][$i]['batting1'][$battingi]['runs'] = $team1player->runs;
								$Json['inning'][$i]['batting1'][$battingi]['balls'] = $team1player->bball;
								$Json['inning'][$i]['batting1'][$battingi]['fours'] = $team1player->fours;
								$Json['inning'][$i]['batting1'][$battingi]['six'] = $team1player->six;
								$Json['inning'][$i]['batting1'][$battingi]['strike_rate'] = $team1player->strike_rate;
								$Json['inning'][$i]['batting1'][$battingi]['dismiss_info'] = $team1player->out_str;
								if(!in_array($team1player->player_name,$team1members)){
									$team1members[] = $team1player->player_name;
								}
								// $team1members.=$team1player->player_name.', ';
								$battingi++;
							}
							if($team1player->bowling==1){
								$Json['inning'][$i]['bowling1'][$bowlingi]['overs'] = $team1player->overs;
								$Json['inning'][$i]['bowling1'][$bowlingi]['playername'] = $team1player->player_name;
								$Json['inning'][$i]['bowling1'][$bowlingi]['maiden_over'] = $team1player->maiden_over;
								$Json['inning'][$i]['bowling1'][$bowlingi]['runs'] = $team1player->grun;
								$Json['inning'][$i]['bowling1'][$bowlingi]['wickets'] = $team1player->wicket;
								$Json['inning'][$i]['bowling1'][$bowlingi]['economy_rate'] = $team1player->economy_rate;
								// $team1members.=$team1player->player_name.', ';
								if(!in_array($team1player->player_name,$team1members)){
									$team1members[] = $team1player->player_name;
								}
								$bowlingi++;
							}
						}
					}
					// for team 2 //
					$findteam2players = DB::table('result_matches')->where('match_key',$matchkey)->where('innings',$i+1)->join('players','players.id','=','result_matches.player_id')->where('players.team',$team2)->select('players.player_name','result_matches.*')->get();
					$battingi=0;
					$bowlingi=0;
					if(!empty($findteam2players)){
						foreach($findteam2players as $team2player){
							if($team2player->batting==1){
								$Json['inning'][$i]['batting2'][$battingi]['playername'] = $team2player->player_name;
								$Json['inning'][$i]['batting2'][$battingi]['runs'] = $team2player->runs;
								$Json['inning'][$i]['batting2'][$battingi]['balls'] = $team2player->bball;
								$Json['inning'][$i]['batting2'][$battingi]['fours'] = $team2player->fours;
								$Json['inning'][$i]['batting2'][$battingi]['six'] = $team2player->six;
								$Json['inning'][$i]['batting2'][$battingi]['strike_rate'] = $team2player->strike_rate;
								$Json['inning'][$i]['batting2'][$battingi]['dismiss_info'] = $team2player->out_str;
								if(!in_array($team2player->player_name,$team2members)){
									$team2members[] = $team2player->player_name;
								}
								$battingi++;
							}
							if($team2player->bowling==1){
								$Json['inning'][$i]['bowling2'][$bowlingi]['overs'] = $team2player->overs;
								$Json['inning'][$i]['bowling2'][$bowlingi]['playername'] = $team2player->player_name;
								$Json['inning'][$i]['bowling2'][$bowlingi]['maiden_over'] = $team2player->maiden_over;
								$Json['inning'][$i]['bowling2'][$bowlingi]['runs'] = $team2player->grun;
								$Json['inning'][$i]['bowling2'][$bowlingi]['wickets'] = $team2player->wicket;
								$Json['inning'][$i]['bowling2'][$bowlingi]['economy_rate'] = $team2player->economy_rate;
								if(!in_array($team2player->player_name,$team2members)){
									$team2members[] = $team2player->player_name;
								}
								$bowlingi++;
							}
						}
					}
				}
				$Json['basicinfo'][0]['team1members'] = implode(', ',$team1members);
				$Json['basicinfo'][0]['team2members'] = implode(', ',$team2members);
				$Json['basicinfo'][0]['matchname'] = $findalllivematches->title;
				$Json['basicinfo'][0]['start_date'] = date('M d Y h:i:a',strtotime($findalllivematches->start_date));
				$Json['basicinfo'][0]['team1'] = $team1name;
				$Json['basicinfo'][0]['team2'] = $team2name;
				if($findalllivematches->winner_team!=""){
					$findteamname = DB::table('teams')->where('team_key',$findalllivematches->winner_team)->first();
					if(!empty($findteamname)){
						$Json['basicinfo'][0]['winner_team'] = ucwords($findteamname->team);
					}
				}
				
				echo json_encode($Json);
				die;
			}
		}

		


		public function viewscorecardandroid(){
			$this->accessrules();
			$matchkey = $_GET['matchkey'];
			$findalllivematches = DB::table('listmatches')->join('teams as t1','t1.id','=','listmatches.team1')->join('teams as t2','t2.id','=','listmatches.team2')->where('matchkey',$matchkey)->select('listmatches.title','listmatches.short_name','listmatches.start_date','listmatches.matchkey','listmatches.format','listmatches.team1','listmatches.team2','t1.team as team1name','t2.team as team2name','winner_team')->first();
			$Json=array();
			if(!empty($findalllivematches)){
				$team1 = $findalllivematches->team1;
				$team1name = $findalllivematches->team1name;
				$team2 = $findalllivematches->team2;
				$team2name = $findalllivematches->team2name;
				$findinnings = DB::table('result_matches')->where('match_key',$matchkey)->groupBy('innings')->select('innings')->get();
				$countinnings = count($findinnings);
				$team2members = array();
				$team1members = array();
				for($i=0;$i<=$countinnings;$i++){
					$findteam1players = DB::table('result_matches')->where('match_key',$matchkey)->where('innings',$i+1)->join('players','players.id','=','result_matches.player_id')->where('players.team',$team1)->select('players.player_name','result_matches.*')->get();
					$battingi=0;
					$bowlingi=0;
					if(!empty($findteam1players)){
						foreach($findteam1players as $team1player){
							if($team1player->batting==1){
								$Json['inning'][$i]['batting1'][$battingi]['playername'] = $team1player->player_name;
								$Json['inning'][$i]['batting1'][$battingi]['runs'] = $team1player->runs;
								$Json['inning'][$i]['batting1'][$battingi]['balls'] = $team1player->bball;
								$Json['inning'][$i]['batting1'][$battingi]['fours'] = $team1player->fours;
								$Json['inning'][$i]['batting1'][$battingi]['six'] = $team1player->six;
								$Json['inning'][$i]['batting1'][$battingi]['strike_rate'] = $team1player->strike_rate;
								$Json['inning'][$i]['batting1'][$battingi]['dismiss_info'] = $team1player->out_str;
								if(!in_array($team1player->player_name,$team1members)){
									$team1members[] = $team1player->player_name;
								}
								// $team1members.=$team1player->player_name.', ';
								$battingi++;
							}
							if($team1player->bowling==1){
								$Json['inning'][$i]['bowling1'][$bowlingi]['overs'] = $team1player->overs;
								$Json['inning'][$i]['bowling1'][$bowlingi]['playername'] = $team1player->player_name;
								$Json['inning'][$i]['bowling1'][$bowlingi]['maiden_over'] = $team1player->maiden_over;
								$Json['inning'][$i]['bowling1'][$bowlingi]['runs'] = $team1player->grun;
								$Json['inning'][$i]['bowling1'][$bowlingi]['wickets'] = $team1player->wicket;
								$Json['inning'][$i]['bowling1'][$bowlingi]['economy_rate'] = $team1player->economy_rate;
								// $team1members.=$team1player->player_name.', ';
								if(!in_array($team1player->player_name,$team1members)){
									$team1members[] = $team1player->player_name;
								}
								$bowlingi++;
							}
						}
					}
					// for team 2 //
					$findteam2players = DB::table('result_matches')->where('match_key',$matchkey)->where('innings',$i+1)->join('players','players.id','=','result_matches.player_id')->where('players.team',$team2)->select('players.player_name','result_matches.*')->get();
					$battingi=0;
					$bowlingi=0;
					if(!empty($findteam2players)){
						foreach($findteam2players as $team2player){
							if($team2player->batting==1){
								$Json['inning'][$i]['batting2'][$battingi]['playername'] = $team2player->player_name;
								$Json['inning'][$i]['batting2'][$battingi]['runs'] = $team2player->runs;
								$Json['inning'][$i]['batting2'][$battingi]['balls'] = $team2player->bball;
								$Json['inning'][$i]['batting2'][$battingi]['fours'] = $team2player->fours;
								$Json['inning'][$i]['batting2'][$battingi]['six'] = $team2player->six;
								$Json['inning'][$i]['batting2'][$battingi]['strike_rate'] = $team2player->strike_rate;
								$Json['inning'][$i]['batting2'][$battingi]['dismiss_info'] = $team2player->out_str;
								if(!in_array($team2player->player_name,$team2members)){
									$team2members[] = $team2player->player_name;
								}
								$battingi++;
							}
							if($team2player->bowling==1){
								$Json['inning'][$i]['bowling2'][$bowlingi]['overs'] = $team2player->overs;
								$Json['inning'][$i]['bowling2'][$bowlingi]['playername'] = $team2player->player_name;
								$Json['inning'][$i]['bowling2'][$bowlingi]['maiden_over'] = $team2player->maiden_over;
								$Json['inning'][$i]['bowling2'][$bowlingi]['runs'] = $team2player->grun;
								$Json['inning'][$i]['bowling2'][$bowlingi]['wickets'] = $team2player->wicket;
								$Json['inning'][$i]['bowling2'][$bowlingi]['economy_rate'] = $team2player->economy_rate;
								if(!in_array($team2player->player_name,$team2members)){
									$team2members[] = $team2player->player_name;
								}
								$bowlingi++;
							}
						}
					}
				}
				$Json['basicinfo'][0]['team1members'] = implode(', ',$team1members);
				$Json['basicinfo'][0]['team2members'] = implode(', ',$team2members);
				$Json['basicinfo'][0]['matchname'] = $findalllivematches->title;
				$Json['basicinfo'][0]['start_date'] = date('M d Y h:i:a',strtotime($findalllivematches->start_date));
				$Json['basicinfo'][0]['team1'] = $team1name;
				$Json['basicinfo'][0]['team2'] = $team2name;
				if($findalllivematches->winner_team!=""){
					$findteamname = DB::table('teams')->where('team_key',$findalllivematches->winner_team)->first();
					if(!empty($findteamname)){
						$Json['basicinfo'][0]['winner_team'] = ucwords($findteamname->team);
					}
				}
	// 			echo '<pre>'; print_r($Json);die;
				$jsonFinal[] = $Json; 
				echo json_encode($jsonFinal);
				die;
			}
		}


		public function firstcheckteam(){
			$this->accessrules();
			$userid = $_GET['user_id'];
			if($userid!=""){
				$checkteam = DB::table('registerusers')->where('id',$userid)->select('team','state','dob')->first();
				if(!empty($checkteam)){
					if($checkteam->team=="" || $checkteam->state=="" || $checkteam->state==null || $checkteam->dob=="0000-00-00"){
						$Json[0]['status'] = "0";
						$Json[0]['team'] = $checkteam->team;
						$Json[0]['state'] = $checkteam->state;
						if($checkteam->dob=="0000-00-00"){
							$Json[0]['dob']="";
						}else{
							$Json[0]['dob'] = $checkteam->dob;
						}
					}
					else{
						$Json[0]['status'] = "1";
					}
				}
			}else{
				$Json[0]['status'] = "0";
			}
			echo json_encode($Json);
			die;
		}
		public function updateteamname(){
			$this->accessrules();
			$userid = $_GET['user_id'];
			$udate['team'] = $teamname = str_replace(" ", "", $_GET['teamname']);
			$restrictarray=array();
			$restrictarray = ['madar','bhosadi','bhosd','aand','jhaant','jhant','fuck','chut','chod','gand','gaand','choot','faad','loda','Lauda','maar','*fuck*','*chut*','*chod*','*gand*','*gaand*','*choot*','*faad*','*loda*','*Lauda*','*maar*'];
			if(in_array($udate['team'],$restrictarray)){
				$Json[0]['status'] = "You cannot use offensive/abusive words";
				echo json_encode($Json);
				die;
			}
			foreach($restrictarray as $raray){
				if (strpos(strtolower($udate['team']), $raray) !== false) {
					$Json[0]['status'] = "You cannot use offensive/abusive words";
					echo json_encode($Json);
					die;
				}
			}
			if(isset($_GET['state'])){
				$udate['state'] = $state = $_GET['state'];
			}
			if(isset($_GET['dob'])){
				if($_GET['dob']!=""){
					$udate['dob'] = $dob = date('Y-m-d',strtotime($_GET['dob']));
				}
			}
			$checkteam = DB::table('registerusers')->where('team','like',$teamname)->select('id')->first();
			if(!empty($checkteam)){
				$Json[0]['status'] = "already exist";
			}else{
				DB::table('registerusers')->where('id',$userid)->update($udate);
				$Json[0]['status'] = "Team Name Updated Successfully";
			}
			echo json_encode($Json);
			die;
		}
		public function seriesdetails(){
			$this->accessrules();
			$seriesid = $_GET['series'];
			$findseriesname = DB::table('series')->where('id',$seriesid)->first();
			$Json=array();
			if(!empty($findseriesname)){
				$Json[0]['status'] = 1;
				$Json[0]['name'] = $findseriesname->name;
				$Json[0]['start_date'] = date('d M, Y',strtotime($findseriesname->start_date));
				$Json[0]['end_date'] = date('d M, Y',strtotime($findseriesname->end_date));
				$findmarathonpresent = DB::table('marathon')->where('series',$seriesid)->select('bestmatches')->first();
				if(empty($findmarathonpresent)){
					$Json[0]['status'] = 0;
				}else{
					$Json[0]['bestmatches'] = $findmarathonpresent->bestmatches;
				}
			}else{
				$Json[0]['status'] = 0;
			}
			echo json_encode($Json);
			die;
		}
		
		public function marathonmartrix(){
			$this->accessrules();
			$seriesid = $_GET['series'];
			$userid = $_GET['userid'];
			$findmarathonpresent = DB::table('marathon')->where('series',$seriesid)->first();
			$Json = array();
			$matches = array();
			if(!empty($findmarathonpresent)){
				$findbestmatch = $findmarathonpresent->bestmatches;
				$findallchallenges = DB::table('matchchallenges')->where('series_id',$seriesid)->where('marathon',1)->select('id')->get();
				$charray = array();
				if(!empty($findallchallenges)){
					foreach($findallchallenges as $ch){
						$charray[] = $ch->id;
					}
				}
				$findjoinedleauges = DB::table('joinedleauges')->whereIn('challengeid',$charray)->where('userid',$userid)->get();
				if(!empty($findjoinedleauges)){
					$Json[0]['marathonstatus'] =1;
					$findallusers = DB::table('joinedleauges')->whereIn('challengeid',$charray)->select('userid')->groupBy('userid')->get();
					$userarray=array();
					if(!empty($findallusers)){
						foreach($findallusers as $user){
							if(!in_array($user->userid,$userarray)){
								$userarray[]= $user->userid;
							}
						}
					}
					/*find best scores*/
					if(!empty($userarray)){
						$usno=0;$usernumberget=-1;$getcurrentrankarray=array();
						foreach($userarray as $uarray){
							$findjoinleauges = DB::table('joinedleauges')->whereIn('joinedleauges.challengeid',$charray)->where('joinedleauges.userid',$uarray)->join('jointeam','jointeam.id','=','joinedleauges.teamid')->orderBy('jointeam.points','DESC')->limit($findbestmatch)->join('registerusers','registerusers.id','=','jointeam.userid')->select('points','registerusers.team as teamname','jointeam.userid as userid')->get();
							$countbestscores = 0;
							if(!empty($findjoinleauges)){
								foreach($findjoinleauges as $jel){
									$countbestscores+=$jel->points;
								}
							}
							$user_array[$usno]['bestscores'] = $countbestscores;
							$user_array[$usno]['username'] = $findjoinleauges[0]->teamname;
							$user_array[$usno]['userid'] = $findjoinleauges[0]->userid;
							$getcurrentrankarray[$usno]['bestscores'] = $countbestscores;
							$getcurrentrankarray[$usno]['userid'] = $findjoinleauges[0]->userid;
							if($findjoinleauges[0]->userid==$userid){
								$user_array[$usno]['usrno'] = $usernumberget;
							}else{
								$user_array[$usno]['usrno'] = 0;
							}
							$usno++;
						}
						$gtcurranks = $this->multid_sort($getcurrentrankarray, 'bestscores');
						if(!empty($gtcurranks)){
							$getusercurrank=array();
							$cur=0;$currsno = 0;$plus=0;
							foreach($gtcurranks as $curnk){
								if(!in_array($curnk['bestscores'], array_column($getusercurrank, 'bestscores'))){ // search value in the array
									$currsno++;
									$currsno = $currsno+$plus;
									$plus=0;
								}
								else{
									$plus++;
									
								}
								$getusercurrank[$cur]['rank'] = $currsno;
								$getusercurrank[$cur]['bestscores'] = $curnk['bestscores'];
								$getusercurrank[$cur]['userid'] = $curnk['userid'];
								$cur++;
								
							}
						}
						array_multisort(array_column($user_array,'usrno'),SORT_ASC,array_column($user_array,'bestscores'),SORT_DESC,$user_array);
						// $this->sortBySubArrayValue($user_array, 'bestscores', 'desc');
						$usno = 0;$urank=0;
						$Json[0]['userdetails'] = array();
						foreach($user_array as $userarrayagain){
							// if(in_array($userarrayagain['bestscores'], array_column($Json[0]['userdetails'], 'bestscores'))) { // search value in the array
								
							// }else{
								// $urank++;
							// }
							$getuserindexingcurent = $this->searchByValue($getusercurrank,'userid',$userarrayagain['userid']);
							$getcurrentrank = $getusercurrank[$getuserindexingcurent]['rank'];
							$Json[0]['userdetails'][$usno]['userrank'] = $getcurrentrank;
							$Json[0]['userdetails'][$usno]['bestscores'] = $userarrayagain['bestscores'];
							$Json[0]['userdetails'][$usno]['username'] = $userarrayagain['username'];
							$Json[0]['userdetails'][$usno]['userid'] = $userarrayagain['userid'];
							$usno++;
						}
						$findallmatches = DB::table('listmatches')->where('launch_status','launched')->where('series',$seriesid)->select('short_name','matchkey','format','name','final_status')->get();
						$m_sno=0;$userpointssss=-1;
						if(!empty($findallmatches)){
							foreach($findallmatches as $mtch){
								$upo = 0;
																										  //Added by kumar 01-05-2018
																												   $ispdf=false;
								foreach($Json[0]['userdetails'] as $udetails){
									$findjointeampoints = DB::table('matchchallenges')->where('marathon',1)->where('series_id',$seriesid)->where('matchchallenges.matchkey',$mtch->matchkey)->join('joinedleauges','joinedleauges.challengeid','=','matchchallenges.id')->where('joinedleauges.userid',$udetails['userid'])->join('jointeam','jointeam.id','=','joinedleauges.teamid')->select('points','joinedleauges.id','joinedleauges.challengeid','matchchallenges.pdf_created','matchchallenges.id as mid')->first();
									if(!empty($findjointeampoints)){
										$Json[0]['matchdetails'][$m_sno]['userpoint'][$upo]['totalpoints'] = $findjointeampoints->points;
										$Json[0]['matchdetails'][$m_sno]['userpoint'][$upo]['userid'] = $udetails['userid'];
																			//Added by kumar 01-05-2018
																																		   if($findjointeampoints->pdf_created==1){
																																					$ispdf=true;
										$Json[0]['matchdetails'][$m_sno]['pdfname'] = Config::get('constants.PROJECT_URL').'pdffolders/join-challenges-'.$findjointeampoints->mid.'.pdf';
										   }
																		   //end
									}
									else{
										$Json[0]['matchdetails'][$m_sno]['userpoint'][$upo]['totalpoints'] = "-";
										$Json[0]['matchdetails'][$m_sno]['userpoint'][$upo]['userid'] = $udetails['userid'];
									}
									$upo++;
								}
																										if($ispdf===false){  //This condition added by kumar
								$findpdfchallenege = DB::table('matchchallenges')->where('matchkey',$mtch->matchkey)->where('marathon',1)->where('series_id',$seriesid)->select('pdf_created','id')->first();
								$Json[0]['matchdetails'][$m_sno]['pdfname']="";
								if(!empty($findpdfchallenege)){
									if($findpdfchallenege->pdf_created==1){
										$Json[0]['matchdetails'][$m_sno]['pdfname'] = Config::get('constants.PROJECT_URL').'pdffolders/join-challenges-'.$findpdfchallenege->id.'.pdf';
									}
								}
																										}
								
								$Json[0]['matchdetails'][$m_sno]['matchname'] = ucwords($mtch->short_name);
								$Json[0]['matchdetails'][$m_sno]['matchkey'] = $mtch->matchkey;
								$Json[0]['matchdetails'][$m_sno]['status'] = ucwords($mtch->final_status);
								
								$m_sno++;
							}
						}
						
					}
					
				}else{
					$Json[0]['marathonstatus'] =0;
				}
			}else{
				$Json[0]['marathonstatus'] =0;
			}
			echo json_encode($Json);
			die;
		}
		
		public function sortBySubArrayValue(&$array, $key, $dir='asc') {
	 
				$sorter=array();
				$rebuilt=array();
			 
				//make sure we start at the beginning of $array
				reset($array);
			 
				//loop through the $array and store the $key's value
				foreach($array as $ii => $value) {
				  $sorter[$ii]=$value[$key];
				}
			 
				//sort the built array of key values
				if ($dir == 'asc') asort($sorter);
				if ($dir == 'desc') arsort($sorter);
			 
				//build the returning array and add the other values associated with the key
				foreach($sorter as $ii => $value) {
				  $rebuilt[$ii]=$array[$ii];
				}
			 
				//assign the rebuilt array to $array
				$array=$rebuilt;
			}
			public static function sortBySubArrayValueStatic(&$array, $key, $dir='asc') {
	 
				$sorter=array();
				$rebuilt=array();
			 
				//make sure we start at the beginning of $array
				reset($array);
			 
				//loop through the $array and store the $key's value
				foreach($array as $ii => $value) {
				  $sorter[$ii]=$value[$key];
				}
			 
				//sort the built array of key values
				if ($dir == 'asc') asort($sorter);
				if ($dir == 'desc') arsort($sorter);
			 
				//build the returning array and add the other values associated with the key
				foreach($sorter as $ii => $value) {
				  $rebuilt[$ii]=$array[$ii];
				}
			 
				//assign the rebuilt array to $array
				$array=$rebuilt;
				return $array;
			}
		
		public function marathonmartrixx(){
			$this->accessrules();
			$seriesid = $_GET['series'];
			$userid = $_GET['userid'];
			$findmarathonpresent = DB::table('marathon')->where('series',$seriesid)->first();
			$Json = array();
			$matches = array();
			if(!empty($findmarathonpresent)){
				$findbestmatch = $findmarathonpresent->bestmatches;
				$listofmatches = DB::table('listmatches')->where('series',$seriesid)->where('launch_status','launched')->select('final_status','format','name','title','matchkey')->get();
				if(!empty($listofmatches)){
					$mt_sno=0;
					foreach($listofmatches as $match){
						$Json['matches_list'][$mt_sno]['name'] = $match->name;
						$Json['matches_list'][$mt_sno]['format'] = $match->format;
						$Json['matches_list'][$mt_sno]['title'] = $match->title;
						$matches[] = $match->matchkey;
						$mt_sno++;
					}
					$mt_status_sno=0;
					foreach($listofmatches as $matchst){
						$Json['matches_status'][$mt_status_sno]['status'] = $matchst->final_status;
						$mt_status_sno++;
					}
					$challengesid = array();
					if(!empty($matches)){
						$findchallenges = DB::table('matchchallenges')->whereIn('matchkey',$matches)->where('marathon',1)->where('series_id',$seriesid)->get();
						if(!empty($findchallenges)){
							foreach($findchallenges as $chl){
								$challengesid[] = $chl->id;
							}
						}
						$usersarray = array();
						$findjoinedusers = DB::table('joinedleauges')->whereIn('challengeid',$challengesid)->select('userid')->get();
						if(!empty($findjoinedusers)){
							foreach($findjoinedusers as $jsusers){
								if(!in_array($jsusers->userid,$usersarray)){
									$usersarray[] = $jsusers->userid;
								}
							}
						}
						$user_sno=0;
						if(!empty($usersarray)){
							foreach($usersarray as $uarray){
								$finduserteamname = DB::table('registerusers')->where('id',$uarray)->select('team')->first();
								$userteamnameget = $finduserteamname->team;
								$mc_seno=0;
								$joinsid = array();
								foreach($listofmatches as $matchget){
									$getchall_match = DB::table('matchchallenges')->where('matchkey',$matchget->matchkey)->where('marathon',1)->where('series_id',$seriesid)->select('matchchallenges.id')->first();
									if(!empty($getchall_match)){
										$getchid = $getchall_match->id;
										$ifjoinuser = DB::table('joinedleauges')->where('joinedleauges.userid',$uarray)->where('joinedleauges.challengeid',$getchid)->join('jointeam','jointeam.id','=','joinedleauges.teamid')->select('jointeam.points','jointeam.id')->first();
										if(!empty($ifjoinuser)){
											$joinsid[] = $ifjoinuser->id;
											$Json['user_points'][$user_sno]['pointsystem'][$mc_seno] = $ifjoinuser->points;
										}
										else{
											$Json['user_points'][$user_sno]['pointsystem'][$mc_seno] = "";
										}
									}
									else{
										$Json['user_points'][$user_sno]['pointsystem'][$mc_seno] = "";
									}
									$mc_seno++;
								}
								$finduserbestscore = DB::table('jointeam')->whereIn('jointeam.id',$joinsid)->where('jointeam.userid',$uarray)->limit($findbestmatch)->orderBy('jointeam.points')->join('registerusers','registerusers.id','=','jointeam.userid')->select('registerusers.team','jointeam.points')->get();
								$findscore=0;
								if(!empty($finduserbestscore)){
									foreach($finduserbestscore as $bscoee){
										$findscore+=$bscoee->points;
									}
									$Json['user_points'][$user_sno]['teamname'] = $finduserbestscore[0]->team;
									$Json['user_points'][$user_sno]['bestscores'] = $findscore;
								}
								$user_sno++;
							}
						}
					}
					$Json['status'] = 1;
				}
				else{
					$Json['status'] = 0;
				}
			}
			else{
				$Json['status'] = 0;
			}
			echo json_encode(array($Json));
			die;
		}
		
		function matchplayerspoints(){
			$matchkey = $_GET['matchkey'];
			$user_id = $_GET['user_id'];
				$players = DB::table('result_points')->join('result_matches','result_matches.id','=','result_points.resultmatch_id')->join('players','players.id','=','result_points.playerid')->leftjoin('teams','players.team','=','teams.id')->where('starting11','!=',0)->where('matchkey',$matchkey)->selectRaw('result_matches.player_id, player_name,players.player_key, players.credit, teams.team,sum(result_points.runs) as runs,sum(result_points.fours) as fours,sum(startingpoints) as startingpoints,sum(result_points.sixs) as sixs, sum(result_points.strike_rate) as strike_rate,sum(halcentury) as halcentury,
			sum(point150) as point150,sum(point200) as point200,sum(century) as century,sum(wickets)as wickets, sum(maidens) as maidens,sum(result_points.economy_rate) as economy_rate,sum(result_points.duck) as duck,sum(result_points.runouts) as runouts,sum(result_points.negative) as negative,sum(result_points.catch) as catch_points,winner_point,sum(not_out) as not_out,sum(stumping) as stumping, sum(total) as total, sum(result_matches.runs) as actual_runs, sum(result_matches.fours) as actual_fours, sum(result_matches.starting11) as actual_startingpoints, sum(result_matches.six) as actual_sixs, sum(result_matches.strike_rate) as actual_strike_rate, sum(result_matches.wicket) as actual_wicket, sum(result_matches.maiden_over) as actual_maidens, sum(result_matches.economy_rate) as actual_economy_rate, sum(result_matches.duck) as actual_duck, sum(result_matches.runouts) as actual_runouts, sum(result_matches.negative_points) as negative_points_actual, sum(result_matches.catch) as actual_catch, sum(result_matches.winning) as actual_winning, sum(result_matches.notout) as actual_notout, sum(stumbed) as actual_stumping')->groupBy('players.player_key')->orderBy('total','desc')->get();

				$total_teams = DB::table('jointeam')->where('matchkey', $matchkey)->select(DB::raw("COUNT(id) as total_count"))->get();
                $totalTeams = $total_teams[0]->total_count;
                
				foreach ($players as $key => $value) {
					//Starting11 YES/NO
					if($players[$key]->actual_startingpoints==1) {
						$players[$key]->actual_startingpoints = 'Yes';
					} else {
						$players[$key]->actual_startingpoints = 'No';
					}
					//Not OUT
					if($players[$key]->actual_notout==1) {
						$players[$key]->actual_notout = 'Yes';
					} else {
						$players[$key]->actual_notout = 'No';
					}

					//Duck
					if($players[$key]->actual_duck==1) {
						$players[$key]->actual_duck = 'Yes';
					} else {
						$players[$key]->actual_duck = 'No';
					}

					//winning
					if($players[$key]->actual_winning >= 1) {
						$players[$key]->actual_winning = 'Yes';
					} else {
						$players[$key]->actual_winning = 'No';
					}

					if($players[$key]->halcentury>0) {
						$players[$key]->actual_halcentury = 1;
					} else {
						$players[$key]->actual_halcentury = 0;
					}

					if($players[$key]->century>0) {
						$players[$key]->actual_century = 1;
					} else {
						$players[$key]->actual_century = 0;
					}

					if($players[$key]->point150>0) {
						$players[$key]->actual_point150 = 1;
					} else {
						$players[$key]->actual_point150 = 0;
					}

					if($players[$key]->point200>0) {
						$players[$key]->actual_point200 = 1;
					} else {
						$players[$key]->actual_point200 = 0;
					}

					$player_id = $value->player_id;
					$choose = DB::table('jointeam')->whereRaw('FIND_IN_SET('.$player_id.',players)')->where('matchkey', $matchkey)->where('userid', $user_id)->get();
					if(empty($choose)) {
						$players[$key]->isSelected = 0;
					} else {
						$players[$key]->isSelected = 1;
					}
					$total_choose = DB::table('jointeam')->whereRaw('FIND_IN_SET('.$player_id.',players)')->where('matchkey', $matchkey)->select(DB::raw("COUNT(id) as total_count"))->get();

					$total_selected = $total_choose[0]->total_count;
					if($totalTeams) {
					$selected_by = round((($total_selected * 100) / $totalTeams), 2);
					$selected_by = $selected_by.' %';
					$players[$key]->selected_by = $selected_by;
					} else{
						$players[$key]->selected_by = 0;
					}

					if($key<11) {
						$players[$key]->is_topplayer = 1;
					} else {
						$players[$key]->is_topplayer = 0;
					}

				}

//echo '<pre>'; print_r($players); die;
			echo json_encode($players);
			die;
		}
		
	}

	?>