<?php
namespace App\Http\Controllers;
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Helpers;
use Carbon\Carbon;
use Hash;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
class AndroidapiController extends Controller {
	public function geturl(){
		return 'https://test.fanadda.com/bbadmin/';
	}
	public function accessrules(){
		header('Access-Control-Allow-Origin: *'); 
		header("Access-Control-Allow-Credentials: true");
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Authorization');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	}
	public function version(){
		$this->accessrules();
		$findlogin = DB::table('androidversion')->first();
		if(!empty($findlogin)){
			$msgg['status'] = $findlogin->version;
			echo json_encode(array($msgg));die;
		}else{
			$msgg['status'] = 0;
			echo json_encode(array($msgg));die;
		}
	}
	
	public function registeruser(Request $request){
		$this->accessrules();
		$email = $_GET['email'];
		$password = $_GET['password'];
		$mobile_no = $_GET['mobile_no'];
		$findlogin = DB::table('register_users')->where('email',$email)->select('id')->first();
		if(!empty($findlogin)){
			$msgg['msg'] = "This email is already registered.";
				$msgg['status'] = 0;
				echo json_encode(array($msgg));die;
		}

		if(strlen($mobile_no)!=10) {
			$msgg['msg'] = "Please enter a valid mobile number.";
			$msgg['status'] = 0;
			echo json_encode(array($msgg));die;
		}

		$findmobile = DB::table('register_users')->where('mobile', $mobile_no)->select('id')->first();
		if(!empty($findmobile)){
			$msgg['msg'] = "This Phone is already registered.";
				$msgg['status'] = 0;
				echo json_encode(array($msgg));die;
		}
		$rand=rand(1000,9999);
		$data['email'] = $_GET['email'];
		$newmailaddress = Helpers::getnewmail($data['email']);
		$finduseremail = DB::table('register_users')->where('email',$newmailaddress)->orWhere('email',$_GET['email'])->select('id')->first();
		if(!empty($finduseremail)){
			$msgg['msg'] = "This email is already registered.";
			$msgg['status'] = 0;
			echo json_encode(array($msgg));die;
		}
		$data['password'] = $_GET['password'];
		if(isset($_GET['dob'])){
		$dob = $data['dob'] = date('Y-m-d',strtotime($_GET['dob']));
		}
		$data['activation_status'] = 'activated';
		$findlastow = DB::table('register_users')->orderBy('id','DESC')->first();
		$numberid = 1;
		if(!empty($findlastow)){
			$numberid = $findlastow->id+1;
		}
		$data['refercode'] = strtoupper(str_replace('@', '',str_replace('.', '', substr($newmailaddress,0,5)))).$rand;
		
		if(isset($_GET['code'])){
			$finsuers = DB::table('register_users')->where('refercode',$_GET['code'])->first();
			if(!empty($finsuers)){
				$data['refer_id'] = $finsuers->id;
				$blns['refer_id'] = $finsuers->id;
			}
		}
		$data['email'] = $newmailaddress;
		DB::table('register_users')->insert($data);
		$blns['user_id'] = $numberid;
		$blns['balance'] = 0;
		DB::table('user_balances')->insert($blns);

		$rand2 = rand(1000,9999);
		$dataupdate['code']=$rand2;
		DB::table('register_users')->where('id',$numberid)->update($dataupdate);
		$txtmsg='Dear Fanadda User Your Mobile Verify OTP is '.$rand2.' Thank You For Choosing Fanadda.';
		Helpers::sendTextSmsNew($txtmsg,$mobile_no);
		
		$msgg['msg'] = "User Registered Successfully.";
		$msgg['userid'] = $numberid;
		$msgg['status'] = 1;
		echo json_encode(array($msgg));die;
	}
	public function logoutuser(){
		$this->accessrules();
		$appdata['userid'] = $_GET['userid'];
		$appid = $appdata['appkey'] = $_GET['appid'];
		$findexist = DB::table('android_app_ids')->where('userid',$_GET['userid'])->where('appkey',$_GET['appid'])->first();
		if(!empty($findexist)){
			DB::table('android_app_ids')->where('id',$findexist->id)->delete();
		}
		$msgg['status'] = 1;
		echo json_encode(array($msgg));die;
	}
	public function loginuser(){
		$this->accessrules();
		$email = $_GET['email'];
		$password = $_GET['password'];
		$newmailaddress = Helpers::getnewmail($email);
		$findlogin = DB::table('register_users')->where(function ($query) use ($email,$newmailaddress) {
			$query->where('email', '=', $email);
			$query->orwhere('email', '=', $newmailaddress);
			$query->orwhere('mobile', '=', $newmailaddress);
			$query->orwhere('mobile', '=', $email);
			$query->orwhere('team', '=', $newmailaddress);
		})->where('password',$_GET['password'])->first();
		if(!empty($findlogin)){
		    
		    if($findlogin->activation_status == 'block'){
		        $msgg['msg'] = "Your account is blocked";
				$msgg['uid'] = 0;
				$msgg['status'] = -1;
				echo json_encode(array($msgg));die;
		    }
		    
				$msgg['msg'] = "User Login Successfully.";
				$msgg['uid'] = $findlogin->id;
				$msgg['status'] = 1;
				if(isset($_GET['appid'])){
					$appid = $appdata['appkey'] = $_GET['appid'];
					$appdata['userid'] = $findlogin->id;
				    $findexist = DB::table('android_app_ids')->where('userid',$findlogin->id)->where('appkey',$appid)->first();
					if(empty($findexist)){
						DB::table('android_app_ids')->insert($appdata);
					}
				}
				echo json_encode(array($msgg));die;
			}
			else{
				$msgg['msg'] = "Invalid Deatils.";
				$msgg['uid'] = 0;
				$msgg['status'] = 0;
				echo json_encode(array($msgg));die;
			}
	}
	public function verifyrefercode(){
		$this->accessrules();
		$appdata['refercode'] = $_GET['refercode'];
		$codeuser = DB::table('register_users')->where('refercode',$_GET['refercode'])->select('id')->first();
		if(!empty($codeuser)){
			$msgg['status'] = 1;
		}else{
			$msgg['status'] = 0;
		}
		echo json_encode(array($msgg));die;
	}
	
	public function sociallogin(){
		$this->accessrules();
		$email = $_GET['email'];
		$name = $_GET['name'];
		$image = $_GET['image'];
		if(isset($_GET['dob'])){
			if($_GET['dob']!=""){
				$dob = date('Y-m-d',strtotime($_GET['dob']));
			}
		}
		$newmailaddress = Helpers::getnewmail($_GET['email']);
		$findlogin = DB::table('register_users')->where('email',$newmailaddress)->orWhere('email',$_GET['email'])->select('id')->first();
		if(!empty($findlogin)){
			$msgg['msg'] = "User Login Successfully.";
			$msgg['uid'] = $findlogin->id;
			$msgg['status'] = 1;
			$msgg['dobstatus'] = 1;
			if(isset($_GET['appid'])){
				$appid = $appdata['appkey'] = $_GET['appid'];
				$appdata['userid'] = $findlogin->id;
				$findexist = DB::table('android_app_ids')->where('userid',$findlogin->id)->where('appkey',$appid)->first();
				if(empty($findexist)){
					DB::table('android_app_ids')->insert($appdata);
				}
			}
			echo json_encode(array($msgg));die;
		}
		else{
			$rand=rand(1000,9999);
			$data['email'] = $newmailaddress;
			$username= $data['username'] = $_GET['name'];
			$data['image'] = $_GET['image'];
			$data['provider'] = $_GET['provider'];
			$data['activation_status'] = 'activated';
			if(isset($_GET['dob'])){
				if($_GET['dob']!=""){
					$dob = $data['dob'] = date('Y-m-d',strtotime($_GET['dob']));
					$msgg['dobstatus'] = 1;
				}
				// else{
					// $msgg['msg'] = "DOB Required";
					// $msgg['uid'] = "";
					// $msgg['status'] = 0;
					// $msgg['dobstatus'] = 0;
					
					// echo json_encode(array($msgg));die;
				// }
			}
		
			 
			// check for refer code//
				if(isset($_GET['code'])){
					$finsuers = DB::table('register_users')->where('refercode',$_GET['code'])->first();
					if(!empty($finsuers)){
						$data['refer_id'] = $finsuers->id;
					}
				}
			// end check for refer code//
			$insertid = DB::table('register_users')->insertGetId($data);
			
			//$updateData['refercode'] = 'BBF'.$rand.$insertid;
			$updateData['refercode'] = strtoupper(str_replace('@', '',str_replace('.', '', substr($newmailaddress,0,5)))).$rand;
			DB::table('register_users')->where('id',$insertid)->update($updateData);
			
			if(isset($_GET['appid'])){
				$appid = $appdata['appkey'] = $_GET['appid'];
				$appdata['userid'] = $insertid;
				$findexist = DB::table('android_app_ids')->where('userid',$insertid)->where('appkey',$appid)->first();
				if(empty($findexist)){
					DB::table('android_app_ids')->insert($appdata);
				}
			}
			
			$blns['user_id'] = $numberid =  $insertid;
			$blns['balance'] = 0;
			DB::table('user_balances')->insert($blns);
			
			$msgg['msg'] = "User Login Successfully.";
			$msgg['uid'] = $insertid;
			$msgg['status'] = 1;
			echo json_encode(array($msgg));die;
		}
		die;
	}
	
	function playerStatus(){
	    $curDate = Carbon::now();
	    $users = DB::table('register_users')->get();
	    foreach($users as $user){
	        $joinedDate = DB::table('joined_leagues')->where('userid',$user->id)->orderBy('created_at','desc')->first();
	        if(!empty($joinedDate)){
	            echo "<pre>";
    	        $lastDate =  $joinedDate->created_at;
    	       
    	        $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $lastDate);
                $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $curDate);
                $days = $to->diffInDays($from);
                
                //echo $user->id.'   :    '.$days;
                
                if($days > 10){
                    $data['playing_status']='deactivated';
	                DB::table('register_users')->where('id',$user->id)->update($data);
                }else{
                    $data['playing_status']='activated';
    	            DB::table('register_users')->where('id',$user->id)->update($data);   
                }
	        }else{
	            $data['playing_status']='deactivated';
	            DB::table('register_users')->where('id',$user->id)->update($data);
	        }
	    }
	}
	
	public function forgetuser(){
		$this->accessrules();
		$email = $_GET['email'];
		$email = Helpers::getnewmail($_GET['email']);
		$findlogin = DB::table('register_users')->where('email',$email)->first();
// 		echo '<pre>'; print_r($findlogin); die;
// 		echo $email; die;
		if(!empty($findlogin)){
			
    			
				$data['password_link'] = base64_encode($email);
				$url = $this->geturl();
				DB::table('register_users')->where('id',$findlogin->id)->update($data);
    			$email = $email;
    			$emailsubject = 'Reset Password - Fanadda';
    			$content='<p><strong>Dear Fanadda User </strong></p>';
    			$content.='<p>Your Forget Passward Link is <strong><a href="'.$url.'api/reset-password?link='.$data['password_link'].'">'. $url.'api/reset-password?link='.$data['password_link'] .'</a></strong> For reset Your password.This link will expire in 24 Hours. Thank You Fanadda.</p>';
    			$msg = Helpers::mailheader();
    			$msg.= Helpers::mailbody($content);
    			$msg.= Helpers::mailfooter();
    			// $datamessage['email'] = $email;
    			// $datamessage['subject'] = $emailsubject;
    			// $datamessage['content'] = $msg;
    			// Helpers::mailsentFormat($email,$emailsubject,$msg);
    			// Helpers::mailSmtpSend($datamessage);
				
				//send mail
				require 'vendor/autoload.php';    
				 $emailGrid = new \SendGrid\Mail\Mail(); 
				 $emailGrid->setFrom("info@fanadda.com", "Fanadda");
				 $emailGrid->setSubject($emailsubject);
				 $emailGrid->addTo($email, "Example User");
				 $emailGrid->addContent(
					"text/html", $msg
				 );
				 $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));

				try {
					 $response = $sendgrid->send($emailGrid);
				} catch (Exception $e) {
					//error message
				}
				
				// end
			
			
			$msgg['msg'] = "Please heck your mail inbox for Link. (In case email is not received please check your spam folder)";
			$msgg['otp'] = 0;
			$msgg['status'] = 1;
			echo json_encode(array($msgg));die;
		}else{
			$msgg['msg'] = "Invalid Deatils.";
			$msgg['otp'] = 0;
			$msgg['status'] = 0;
			echo json_encode(array($msgg));die;
		}
	}

	public function recreate_password(Request $request) {
		if ($request->isMethod('post')){
			$post = Input::all();
			$password = $post['password'];
			$confirm_password = $post['confirm_password'];
			if($password==$confirm_password) {
				$link = $post['link'];
				$findlogin = DB::table('register_users')->where('password_link',$link)->first();
				if(!empty($findlogin)) {
					$data['password_link'] = '';
					$data['password'] = $password;
					DB::table('register_users')->where('id',$findlogin->id)->update($data);
					Session::flash('message', 'Password changed successfully!');
					Session::flash('alert-class', 'alert-success');
					return redirect()->back()->with('flag','success');
				} else {
					Session::flash('message', 'Link expired!');
					Session::flash('alert-class', 'alert-danger');
					return redirect()->back();
				}
			} else {
					Session::flash('message', 'Password and its confirm not match!');
					Session::flash('alert-class', 'alert-danger');
					return redirect()->back();
			}
		} 
		$link = $_GET['link'];
		$findlogin = DB::table('register_users')->where('password_link',$link)->first();
		if(!empty($findlogin)) {
			$data['password_link'] = '';
			//DB::table('register_users')->where('id',$findlogin->id)->update($data);
			return view('password.reset');
		}
	}
	
	public function matchcodeforreset(){
		$this->accessrules();
		$code = $_GET['code'];
		$unique_id = $_GET['token'];
		$findlogin = DB::table('register_users')->where('email',$unique_id)->where('code',$code)->first();
		if(!empty($findlogin)){
			$msgg['msg'] = "Please heck your message inbox for Otp.";
			$msgg['id'] = $findlogin->id;
			$msgg['status'] = 1;
			echo json_encode(array($msgg));die;
		}else{
			$msgg['msg'] = "Otp Not Matched.";
			$msgg['id'] = 0;
			$msgg['status'] = 0;
			echo json_encode(array($msgg));die;
		}
	}
	public function resetpassword(){
		$this->accessrules();
		$data['password'] = $_GET['password'];
		$unique_id = $_GET['token'];
		$findlogin = DB::table('register_users')->where('email',$unique_id)->update($data);
		// $email = $findlogin->email;
		// $emailsubject = 'Password changed - Select2Win.com';
		// $content='<p><strong>Hello </strong></p>';
		// $content.='<p>Your password has been successfully changed.</p>';
		// $msg = Helpers::mailheader();
		// $msg.= Helpers::mailbody($content);
		// $msg.= Helpers::mailfooter();
		// Helpers::mailsentFormat($email,$emailsubject,$msg);
		$msgg['msg'] = "Password Updated Successfully.";
		$msgg['status'] = 1;
		echo json_encode(array($msgg));die;
	}
	public function givefeedback(){
		$this->accessrules();
		if(isset($_GET['userid'])){
		    $data['userid'] = $_GET['userid'];
		}
		$data['name'] = $_GET['name'];
		$data['email'] = $_GET['email'];
		$data['number'] = $_GET['number'];
		$data['review'] = $_GET['review'];
		if(isset($data['reaction'])){
			$data['reaction'] = $_GET['reaction'];
		}
		$email = 'Contact@select2Win.com';
		$emailsubject = 'Feedback - Select2Win.com';
		$content='<p><strong>New Feedback arrived from select2win </strong></p>';
		$content.='<p>Name:- '.ucwords($_GET['name']).'</p>';
		$content.='<p>Email address:- '.$_GET['email'].'</p>';
		$content.='<p>Contact Number:- '.$_GET['number'].'</p>';
		$content.='<p>Review:- '.$_GET['review'].'</p>';
		$msg = Helpers::mailheader();
		$msg.= Helpers::mailbody($content);
		$msg.= Helpers::mailfooter();
		$datamessage['content'] = $msg;
		$datamessage['email'] = $email;
		$datamessage['fromemail'] = $_GET['email'];
		$datamessage['subject'] = $emailsubject;
		Helpers::mailSmtpSend1($datamessage);
		// Helpers::mailsentFormat($email,$emailsubject,$msg);
		DB::table('feedback')->insert($data);
		$Json[0]['status'] = '1';
		echo json_encode($Json);
		die;
		
	}
	public function editprofile(Request $request){
		$this->accessrules();
		$id = $_GET['id'];
		$data['id'] = $_GET['id'];
		$data['username'] = $_GET['username'];
		$data['dob'] = date('Y-m-d',strtotime($_GET['dob']));  //  Dob Must be in Y-m-d
		$data['gender'] = $_GET['gender']; 
		$data['mobile'] = $_GET['mobile']; 
		$data['address'] = $_GET['address']; 
		$data['city'] = $_GET['city']; 
		$data['state'] = $_GET['state']; 
		$data['country'] = $_GET['country']; 
		$data['pincode'] = $_GET['pincode']; 
		$data['team'] = str_replace(' ', '', $_GET['team']);
		$restrictarray = ['madar','bhosadi','bhosd','aand','jhaant','jhant','fuck','chut','chod','gand','gaand','choot','faad','loda','Lauda','maar','*fuck*','*chut*','*chod*','*gand*','*gaand*','*choot*','*faad*','*loda*','*Lauda*','*maar*'];
		$findteamexist = DB::table('register_users')->where('team','like',$data['team'])->where('id','!=',$data['id'])->first();
		if(!empty($findteamexist)){
			echo json_encode(2);die;
		}
		if(in_array($data['team'],$restrictarray)){
			$Json[0]['status'] = '2';
			$Json[0]['msg'] = 'You cannot use offensive/abusive words';
			echo json_encode(3);die;
		}
		foreach($restrictarray as $raray){
			if (strpos(strtolower($data['team']), $raray) !== false) {
				$Json[0]['status'] = '2';
				$Json[0]['msg'] = 'You cannot use offensive/abusive words';
				echo json_encode(3);die;
			}
		}
		$rules = array(
            'username' => 'required'
		);
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			$Json[0]['status'] = '0';
			$Json[0]['msg'] = 'Some problem occur.';
        }else{
			DB::table('register_users')->where('id',$id)->update($data);
			$Json[0]['status'] = '1';
			$Json[0]['msg'] = 'Profile updated successfully.';
		}
		echo json_encode($Json);
		die;
	}
	

}
?>