<?php
namespace App\Http\Controllers;
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Hash;
use Mail;
use Cache;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
class FootballController extends Controller {
  function __construct()
  {
    define('CRICKET_API_URL','https://rest.entitysport.com/'); //live
    define('CRICKET_SECRET_KEY','9552450a61c5ab41840effb8435dc8ff'); //live
    define('CRICKET_ACCESS_KEY','817236b4f04e50b1bc2cd853dbe6be65'); //live
    define('ENTITY_SPORT_API_URL','https://rest.entitysport.com/'); //live
    //define('ENTITY_SPORT_TOKEN_FOOTBALL','127eb18957bac37b7479e3f5b281a488'); //dev
    define('ENTITY_SPORT_TOKEN_FOOTBALL','07a72623ee9f1c147918f1c71c461fe0'); //dev
    define('ENTITY_SPORT_TOKEN_HOCKEY','127eb18957bac37b7479e3f5b281a488'); //live
    define('ENTITY_SPORT_TOKEN_KABADDI','127eb18957bac37b7479e3f5b281a488'); //live
    define('ENTITY_SPORT_TOKEN_BASKETBALL','2e4791fbfbded7ceaa4abcb1f9a38bd1');//Live //dev
  }
	public function accessrules(){
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Credentials: true");
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Authorization');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	}

//  public static function getaccesstoken(){
//    date_default_timezone_set("Asia/Kolkata");
//        $fields = array(
//            'access_key' => '817236b4f04e50b1bc2cd853dbe6be65',
//            'secret_key' => '9552450a61c5ab41840effb8435dc8ff',
//            'app_id' => 'com.fanadda_football',
//            'device_id' => 'developer'
//        );
//    $url = 'https://api.footballapi.com/v1/auth/';
//        $fields_string="";
//    $todate = Carbon::now();
//    // $findtoken = DB::table('apitoken')->whereDate('date','=',date('Y-m-d',strtotime($todate)))->first();
//    $findtoken = array();
//    if(!empty($findtoken)){
//      $match_fields = array(
//        'access_token' => $findtoken->token,
//      );
//    }
//    else{
//     // Cache::forget('football_accesstoken');
//      $match_fields = Cache::remember('football_accesstoken', 1000, function () use ($fields,$fields_string,$url) {
//      foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
//      $fields_string=rtrim($fields_string, '&');
//      $ch = curl_init();
//      curl_setopt($ch, CURLOPT_URL, $url);
//      curl_setopt($ch, CURLOPT_POST, true);
//      curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
//      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//      $result = curl_exec($ch);
//     // echo '<pre>'; print_r($result); die;
//      $result_arrs = json_decode($result, true);
//      $access_token = $result_arrs['auth']['access_token'];
//      $match_fields = array(
//        'access_token' => $access_token,
//      );
//      $matchtoken['token'] = $access_token;
//      // DB::table('apitoken')->where('id',1)->update($matchtoken);
//        return $match_fields;
//      });
//    }
//     return $match_fields;
//    die;
//  }


    public static function getaccesstoken(){
        date_default_timezone_set("Asia/Kolkata");
        $fields = array(
            'access_key' => '817236b4f04e50b1bc2cd853dbe6be65',
            'secret_key' => '9552450a61c5ab41840effb8435dc8ff',
            'app_id' => 'com.fanadda_football',
            'device_id' => 'developer'
        );

        $url = 'https://api.footballapi.com/v1/auth/';
        $fields_string="";
        $todate = Carbon::now();
        $findtoken = DB::table('api_tokens')->whereDate('date','=',date('Y-m-d',strtotime($todate)))->where('sport',2)->first();
        if(!empty($findtoken)){
            $match_fields = array(
                'access_token' => $findtoken->token,
            );
        }
        else{
            foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
            $fields_string=rtrim($fields_string, '&');
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            $result_arrs = json_decode($result, true);
            $access_token = $result_arrs['auth']['access_token'];
            $match_fields = array(
                'access_token' => $access_token,
            );
            $matchtoken['token'] = $access_token;
            DB::table('api_tokens')->where('sport',2)->update($matchtoken);
        }
        return $match_fields;
        die;
    }

	public static function genrateToken() {
		return ENTITY_SPORT_TOKEN_FOOTBALL;
	}

	public static function getMatches(){
		$token = FootballController::genrateToken();
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => ENTITY_SPORT_API_URL."soccer/matches/?token=".$token."&status=1&per_page=50&=&paged=1",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        return $response;

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
        	$result = json_decode($response);
        	// print_r($result); exit;
        	if($result->status=="ok") {
        		$response = $result->response;
            	$data = $response->items;
            	return $data;
        	}
        }
	}

	public static function get_match_squad($match_id, $comp_id) {
		$token = FootballController::genrateToken();
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => ENTITY_SPORT_API_URL."soccer/matches/".$match_id."/newfantasy?token=".$token."&status=1&per_page=100&=&paged=1",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        return $response;

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
        	$result = json_decode($response);
        	if($result->status=="ok") {
        		$response = $result->response;
            	return $response;
        	}
        }
	}

	public static function get_match_details($match_id) {

        $match_fields = FootballController::getaccesstoken();
        $access_token = $match_fields['access_token'];
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.footballapi.com/v1/match/".$match_id."/?access_token=".$access_token."",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        //print_r($response); exit;
        curl_close($curl);
        return $response;
	}

	public static function get_match_score($match_id) {
		$token = FootballController::genrateToken();
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => ENTITY_SPORT_API_URL."/soccer/matches/".$match_id."/newfantasy?token=".$token,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        return $response;
        // echo ENTITY_SPORT_API_URL."/soccer/matches/".$match_id."/newfantasy?token=".$token; exit;
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
        	$result = json_decode($response);
        	if($result->status=="ok") {
        		$response = $result->response;
            	return $response;
        	}
        }
	}

    public static function get_match_playing_players($api_unique_id) {
        $token = FootballController::genrateToken();
        // echo ENTITY_SPORT_API_URL."/soccer/matches/".$api_unique_id."/info?token=".$token.""; exit;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => ENTITY_SPORT_API_URL."/soccer/matches/".$api_unique_id."/info?token=".$token."",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        //print_r($response); exit;
        curl_close($curl);
        return $response;

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $result = json_decode($response);
            if($result->status=="ok" && isset($result->response->items->lineup) && !empty($result->response->items->lineup)) {
                $response = $result->response->items->lineup;
                $playing_players = array();
                if(isset($response->home->lineup->player) && isset($response->away->lineup->player)) {
                    $teama_players = $response->home->lineup->player;
                    $teamb_players = $response->away->lineup->player;
                    $players = array_merge($teama_players, $teamb_players);
                    foreach ($players as $player) {
                        $playing_players[] = $player->pid;
                    }
                }
                return $playing_players;
            }
        }
    }

    public static function get_tournaments() {
      $match_fields = FootballController::getaccesstoken();
      $access_token = $match_fields['access_token'];
      $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.footballapi.com/v1/recent_tournaments/?access_token=".$access_token."",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
		
        return $response;
    }

    public static function get_tournament_matches() {
      $tournaments = self::get_tournaments();
      // print_r($tournaments); exit;
      $tournaments = json_decode($tournaments, true);
      $i = 0;
      // print_r($tournaments); exit;
      if(isset($tournaments['data']['tournaments'])){
          foreach ($tournaments['data']['tournaments'] as $key => $value) {
              $rounds = self::get_rounds($value['key']);
              $rounds = json_decode($rounds, true);
              $tourkey =  $value['key'];
              $tournaments['data']['tournaments'][$i]['rounds'] = $rounds['data']['tournament']['rounds'];
              $tournaments['data']['tournaments'][$i]['teams'] = $rounds['data']['tournament']['teams'];

              $j = 0;
              if(!empty($tournaments['data']['tournaments'][$i]['rounds'])) {
                  foreach ($tournaments['data']['tournaments'][$i]['rounds'] as $key2 => $value2) {
                      $roundkey = $value2['key'];
                      $matches = self::get_rounds_matches($tourkey,$roundkey);
                      $matches = json_decode($matches, true);
                      $tournaments['data']['tournaments'][$i]['rounds'][$j]['matches'] = $matches['data']['round']['matches'];
                      $j++;
                  }
              }
              $i++;
          }
      }
      return $tournaments;
    }

    public static function get_rounds($tour_key) {
      $match_fields = FootballController::getaccesstoken();
      $access_token = $match_fields['access_token'];
      $curl = curl_init();

      curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.footballapi.com/v1/tournament/".$tour_key."/?access_token=".$access_token."",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
              "Cache-Control: no-cache"
          ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);
      //print_r($response); exit;
      curl_close($curl);
      return $response;
    }

    public static function get_rounds_matches($tourkey,$roundkey) {
      $match_fields = FootballController::getaccesstoken();
      $access_token = $match_fields['access_token'];
      $curl = curl_init();

      curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.footballapi.com/v1/tournament/".$tourkey."/round-detail/".$roundkey."/?access_token=".$access_token."",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
              "Cache-Control: no-cache"
          ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);
      //print_r($response); exit;
      curl_close($curl);
      return $response;
    }

    public static function get_tournament_team($tournament,$team) {
      $match_fields = FootballController::getaccesstoken();
      $access_token = $match_fields['access_token'];
      $curl = curl_init();

      curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.footballapi.com/v1/tournament/".$tournament."/team/".$team."/?access_token=".$access_token."",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
              "Cache-Control: no-cache"
          ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);
      //print_r($response); exit;
      curl_close($curl);
        return json_decode($response,true);
    }

    public static function getmatchdetails($matchkey) {

      $match_fields = FootballController::getaccesstoken();
      $access_token = $match_fields['access_token'];

     // echo "https://api.footballapi.com/v1/match/".$matchkey."/?access_token=".$access_token.""; exit;

      $curl = curl_init();

      curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.footballapi.com/v1/match/".$matchkey."/?access_token=".$access_token."",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
              "Cache-Control: no-cache"
          ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);
      //print_r($response); exit;
      curl_close($curl);
      return json_decode($response,true);
    }

    public static function getFootballScore($match_key) {
        $match=static::getMatchDetails($match_key);
//        if($match['status']=='Match Finished'){
//            $getmtdatastatus['status'] = 'completed';
//            $getmtdatastatus['final_status'] = 'IsReviewed';
//            DB::table('list_matches')->where('matchkey',$match_key)->update($getmtdatastatus);
//        }

        $findmatch = DB::table('list_matches')->where('matchkey',$match_key)->first();
        if($match['data']['players']!=NULL){
            $i=0;
            foreach ($match['data']['players'] as $key1 => $value1) {
                $datasvv['player_key'] = $value1['key'];
                $datasvv['starting11'] = 1;
                $datasvv['match_key'] = $match_key;
                $datasvv['sport_type'] = 2;
                $datasvv['innings'] = 1;
                $player = DB::table('match_players')->join('players', 'players.id', '=', 'match_players.playerid')->where('matchkey', $match_key)->where('match_players.sport_type', 2)->where('players.sport_type', 2)->select('match_players.*', 'players.player_key', 'players.role as playerrole' , 'players.team')->where('players.player_key', $value1['key'])->first();

                if (!empty($player)) {
                    $datasvv['player_id'] = $player->playerid;

                    if (!empty($player)) {

                        $goalconceded           = $value1['stats']['goal']['own_goal_conceded'] ? $value1['stats']['goal']['own_goal_conceded'] : 0;
                        $cleansheet             = 0;
                        $playingTime            = $value1['stats']['minutes_played']? $value1['stats']['minutes_played'] : 0;
                        $goals                  = isset($value1['stats']['goals']['total']) ? $value1['stats']['goals']['total'] : 0; //paine
                        $assist                 = $value1['stats']['goal']['assist'] ? $value1['stats']['goal']['assist'] : 0;
                        $passes                 = isset($value1['stats']['passes']) ? $value1['stats']['passes'] : 0;
                        $shots                  = isset($value1['stats']['shot_on_target']) ? $value1['stats']['shot_on_target'] : 0;
                        $saveShots              = isset($value1['stats']['goal']['saved']) ? $value1['stats']['goal']['saved'] : 0;
                        $penaltySave            = isset($value1['stats']['penalties_shootout']['saved']) ? $value1['stats']['penalties_shootout']['saved']  : 0;
                        $tackles                = isset($value1['stats']['tackles'])? $value1['stats']['tackles']: 0;
                        $block = 0;
                        $interceptions          = isset($value1['stats']['interceptions_won']) ? $value1['stats']['interceptions_won'] : 0;
                        $totalclearance = 0;
                        $substitute             = isset($value1['substitute'])=="True" ? 1 : 0;
                        $chanceCreated          = isset($value1['passes']['key']) ? $value1['passes']['key'] : 0; //paine
                        $yellowCard             = $value1['stats']['card']['YC'] ? $value1['stats']['card']['YC'] : 0;
                        $redCard                = $value1['stats']['card']['RC'] ? $value1['stats']['card']['RC'] : 0;

                        $ownGoal                = 0;
                        $penaltyMiss            = $value1['stats']['penalties_shootout']['missed'] ? $value1['stats']['penalties_shootout']['missed'] : 0;

                        if($playingTime>0) {
                            $datasv[$i]['minutesplayed']        = $playingTime;
                            $datasv[$i]['goals']                = $goals;
                            $datasv[$i]['assist']               = $assist;
                            $datasv[$i]['totalpass']            = $passes;
                            $datasv[$i]['shotsontarget']        = $shots;
                            $datasv[$i]['cleansheet']           = $cleansheet;
                            $datasv[$i]['block']                = $block;
                            $datasv[$i]['shotsblocked']         = $saveShots;
                            $datasv[$i]['penaltysave']          = $penaltySave;
                            $datasv[$i]['tacklesuccess']        = $tackles;
                            if($redCard>0)
                                $yellowCard=0;
                            $datasv[$i]['yellowcard']           = $yellowCard;
                            $datasv[$i]['redcard']              = $redCard;
                            $datasv[$i]['substitute']           = $substitute;
                            $datasv[$i]['playing11']            = 1-$substitute;
                            $datasv[$i]['owngoals']             = $ownGoal;
                            $datasv[$i]['penaltymiss']          = $penaltyMiss;
                            $datasv[$i]['goalconceded']         = $goalconceded;
                            $datasv[$i]['interceptionsWon']     = $interceptions;
                            $datasv[$i]['chanceCreated']        = $chanceCreated;
                            $datasv[$i]['totalclearance']       = $totalclearance;
                            $datasv[$i]['sport_type'] = 2;
                            $datasv[$i]['match_key'] = $match_key;
                            $datasv[$i]['player_key'] = $value1['key'];
                            $datasv[$i]['player_id'] = $player->playerid;
                            $datasv[$i]['total_points'] = 0;
                            $datasv[$i]['starting11'] = 1;

                            $findplayerex = DB::table('result_matches')->where('player_key',$value1['key'])->where('match_key',$match_key)->select('id','isLocked')->first();
                            if(!empty($findplayerex)){
                                if($findplayerex->isLocked==0){
                                    DB::table('result_matches')->where('id',$findplayerex->id)->update($datasv[$i]);
                                }
                            }else{
                                DB::table('result_matches')->insert($datasv[$i]);
                            }
                        }
                        $i++;
                    }
                }
            }
            return true;
        }
        return false;
    }


    public static function get_playing_11_football($match_key) {
        $match=static::getMatchDetails($match_key);
        echo "<pre>";
        print_r($match);
        exit();

        date_default_timezone_set('Asia/Kolkata');
        $timeNow = date('Y-m-d H:i:s');
        $findmatch = DB::table('list_matches')->where('matchkey',$match_key)->where('only_admin',0)->first();
        if(!empty($findmatch)){
            $findseries = DB::table('series')->where('id',$findmatch->series)->first();
            if(!empty($findseries)){
                $seriesName=str_replace($findseries->name,"? ","");
                $pos = strpos($findseries->name,"|");
                $seriesName= $pos !== FALSE ? substr($findseries->name, $pos + strlen("|"), strlen($findseries->name)) : "";
                $findnotifications = DB::table('bulk_notifications')->where('matchkey',$match_key)->where('isSent',0)->get();
                if(!empty($findnotifications)){
                    foreach($findnotifications as $notification){
                        if(!empty($notification->minutes)){
                            $update_data = array(
                                "isSent" => 1
                            );
                            if($notification->preToss==0){
                                $timeBefore1 = date('Y-m-d H:i:s',strtotime('-'.$notification->minutes.' minutes', strtotime($findmatch->start_date)));
                                if($timeBefore1<=$timeNow)
                                {
                                    DB::table('bulk_notifications')->where('matchkey', $match_key)->where('id',$notification->id)->update($update_data);
                                    Helpers::sendnotificationBulk($notification->display." minutes to go ⏰ ".$findmatch->short_name,"⚽⚽".$seriesName." ⚽⚽\n".$findmatch->name."\nMatch Time ⏰: ".date('h:i A',strtotime($findmatch->start_date)),'FANADDA2');
                                }
                            }
                            else if($notification->preToss==1 && $findmatch->PreTossAvailable==1 && !empty($findmatch->PreTossClosingTime)){
                                $timeBefore1 = date('Y-m-d H:i:s',strtotime('-'.$notification->minutes.' minutes', strtotime($findmatch->PreTossClosingTime)));
                                if($timeBefore1<=$timeNow)
                                {
                                    DB::table('bulk_notifications')->where('matchkey', $match_key)->where('id',$notification->id)->update($update_data);
                                    Helpers::sendnotificationBulk($notification->display." minutes to go ⏰ PreToss Reminder",$findmatch->short_name."\n⚽⚽".$seriesName." ⚽⚽\nPreToss Deadline ⏰: ".date('h:i A',strtotime($findmatch->PreTossClosingTime)),'FANADDA2');
                                }
                            }
                        }
                    }
                }
            }
        }
        if($match['lineups']!=NULL){
            static::importAfterLineup($match_key,$match);
            $startXI=array();
            foreach ($match['lineups'] as $key4 => $value4) {
                //echo $key4;
                if(isset($match['lineups'][$key4]['startXI']) && !empty($match['lineups'][$key4]['startXI']))
                {
                    foreach ($match['lineups'][$key4]['startXI'] as $key2 => $value2) {
                        if(!empty($value2['player_id'])){
                            $player_details = DB::table('players')->join('match_players','players.id','=','match_players.playerid')->where('players.player_key',$value2['player_id'])->where('match_players.sport_type',2)->where('match_players.matchkey', $match_key)->select('players.id')->first();
                            if(!empty($player_details)) {
                                array_push($startXI,$player_details->id);
                            }
                        }
                    }
                }
            }
            //echo count($startXI);
            if(count($startXI)){
                $insert_data = array(
                    "match_key" => $match_key,
                    "player_ids" => serialize($startXI),
                );
                $findplayerex = DB::table('match_playing11')->where('match_key',$match_key)->where('type', 'main')->first();
                if(!empty($findplayerex)){
                    if($findplayerex->isLocked==0){
                        DB::table('match_playing11')->where('match_key', $match_key)->where('type', 'main')->update($insert_data);
                    }
                }else{
                    DB::table('match_playing11')->insert($insert_data);
                    DB::table('list_matches')->where('matchkey',$match_key)->where('only_admin',0)->first();
                    $timeBefore = date('Y-m-d H:i:s',strtotime('-1 minutes', strtotime($findmatch->start_date)));
                    if(!empty($findmatch) && ($timeBefore>=$timeNow)) {
                        Helpers::sendnotificationBulk("Lineup Out 📢 ".$findmatch->short_name,"⚽⚽".$seriesName." ⚽⚽\n".$findmatch->name."\nMatch Time ⏰: ".date('h:i A',strtotime($findmatch->start_date)),'FANADDA2');
                    }
                }
            }
        }
    }

}
