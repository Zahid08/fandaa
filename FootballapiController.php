<?php
namespace App\Http\Controllers;
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Hash;
use Mail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class FootballapiController extends Controller {
    public function accessrules(){
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Authorization');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
    }

    public static function get_tournament_matches() {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.rglabs.net/api/football/get_tournament_matches",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        // return $response;exit;
        //  $d1 = json_decode($response, true);
        //  print_r($d1);exit;
        $err = curl_error($curl);

        curl_close($curl);

        if (!empty($err)) {
            echo "cURL Error #:" . $err;
            // } else {
            $result = json_decode($response);
            // print_r($result); exit;
            // if($result->status=="ok") {
            //   $response = $result->response;
            //     $data = $response->items;
            //     return $data;
            //  }
        }
        return json_decode($response, true);
    }



    public static function get_tournament_team($tour, $team) {
        $curl = curl_init();
        //echo "http://api.rglabs.net/api/football/get_tournament_team/".$tour."/".$team;
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.rglabs.net/api/football/get_tournament_team/".$tour."/".$team,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);

        // return $response;exit;

        curl_close($curl);
        return json_decode($response, true);
        // return $response;
    }




    public static function getmatchdetails($matchkey) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.rglabs.net/api/football/getmatchdetails/".$matchkey,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return json_decode($response, true);
    }









}

