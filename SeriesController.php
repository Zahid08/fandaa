<?php
namespace App\Http\Controllers;
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\CricketapiController;
class SeriesController extends Controller {
	public function addseries(Request $request){
		if ($request->isMethod('post')){
		$rules = array(
            'name' => 'required|unique:series'
		);
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
				return Redirect::back()
					->withErrors('This series is already exist.')
					->withInput(Input::except('password'));
        } 
		else{
           $inputt = Input::all();
		   $input = $request->input();
		   unset($input['_token']);
		   $input['start_date'] = date('Y-m-d h:i:s',strtotime($input['start_date']));
		   $input['end_date'] = date('Y-m-d h:i:s',strtotime($input['end_date']));
		   DB::table('series')->insert($input);
		   Session::flash('message', 'Successfully added series!');
		   Session::flash('alert-class', 'alert-success');
           return Redirect::back();
		}
	  }
	  return view('series.addseries');
	}
	public function editseries($id,Request $request){
		$id = unserialize(base64_decode($id));
		$series = DB::table('series')->where('id',$id)->first();
		if ($request->isMethod('post')){
			$input = Input::all();
			$catname = $input['name'];
			$findseries = DB::table('series')->where('name',$catname)->where('id','!=',$id)->first();
			if(!empty($findseries)){
				return Redirect::back()
					->withErrors('This series is already exist.')
					->withInput(Input::except('password'));
			}
			unset($input['_token']);
			$input['start_date'] = date('Y-m-d h:i:s',strtotime($input['start_date']));
		    $input['end_date'] = date('Y-m-d h:i:s',strtotime($input['end_date']));
			$rowCOllection = DB::table('series')->where('id',$id)->update($input);
			Session::flash('message', 'Successfully updated series!');
			Session::flash('alert-class', 'alert-success');
            return Redirect::back();
		}
		if(!empty($series)){
			return view('series.editseries')->with('series', $series);
		}
		else{
			return redirect()->action('SeriesController@viewseries')->withErrors('Invalid Id Provided');
		}
	}
	public function viewseries(){
		$query = DB::table('series');
		if(request()->has('name')){
			$name=request('name');
			if($name!=""){
				$query->where('name', 'LIKE', '%'.$name.'%');
			}
		}
		if(request()->has('start_date')){
			$start_date = request('start_date');
			$start_date = date('Y-m-d H:i:s', strtotime('-1 hours', strtotime(request('start_date'))));
			if($start_date!=""){
				$query->whereDate('start_date', '>=',date('Y-m-d h:i:s',strtotime($start_date)));
			}
		}
		if(request()->has('end_date')){
			$end_date = request('end_date');
			if($end_date!=""){
				$query->whereDate('end_date', '<=',date('Y-m-d h:i:s',strtotime($end_date)));
			}
		}
		$getlist = $query->orderBY('end_date','DESC')->paginate(20);
		return view('series.viewseries')->with('getlist', $getlist);
	}
	public function updateseriesstatus($id,$status){
	    $id = unserialize(base64_decode($id));
	    $series = DB::table('series')->where('id',$id)->first();
		if(!empty($series)){
			$data['series_status'] = $status;
			DB::table('series')->where('id',$id)->update($data);
			Session::flash('message', 'Successfully updated series status!');
			Session::flash('alert-class', 'alert-success');
		}
		return redirect()->action('SeriesController@viewseries');
	}
	public function deleteseries($id){
		$id = unserialize(base64_decode($id));
		$series = DB::table('series')->where('id',$id)->first();
		if(!empty($series)){
			$findmatchexist = DB::table('list_matches')->where('series',$series->id)->get();
			if(!empty($findmatchexist)){
				return Redirect::back()
					->withErrors('You cannot delete this series as match is added into this series');
			}else{
				 DB::table('series')->where('id',$id)->delete();
				Session::flash('message', 'Successfully deleted series!');
				Session::flash('alert-class', 'alert-success');
			}
		}
		else{
			return redirect()->action('SeriesController@viewseries')->withErrors('Invalid Id Provided');
		}
	}
}
?>