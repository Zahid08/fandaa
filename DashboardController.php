<?php
namespace App\Http\Controllers;
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
class DashboardController extends Controller {
	public function index(){
	    	$datausers = DB::table('register_users')->select('id')->where('role','<',4)->get();
		$users = count($datausers);
		$datapan = DB::table('pan_cards')->where('status','=',0)->where('hold','=',0)->get();
		$pan = count($datapan);
		$databank = DB::table('banks')->where('status','=',0)->where('hold','=',0)->get();
		$bank = count($databank);
		$datawithdraw = DB::table('withdraws')->where('status','=',0)->where('type','=','')->get();
		$withdraw = count($datawithdraw);
		$datawithdraw2 = DB::table('withdraws')->where('status','=',0)->where('type','<>','')->get();
		$withdraw2 = count($datawithdraw2);
		$datamatches = DB::table('list_matches')->get();
		$matches = count($datamatches);
		$datateams = DB::table('teams')->get();
		$teams = count($datateams);
		$dataplayers = DB::table('players')->get();
		$players = count($dataplayers);
		$dataleauges = DB::table('challenges')->get();
		$leauges = count($dataleauges);
		return view('dashboards.index',compact('users','pan','bank','withdraw','withdraw2','matches','teams','players','leauges'));
		
	}

	public function download() {
		$file = base_path().'/app/APK/Fanadda.apk'; //not public folder
		if (file_exists($file)) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/vnd.android.package-archive');
			header('Content-Disposition: attachment; filename=' . basename($file));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			ob_clean();
			flush();
			readfile($file);
			exit;
		}
	}
}