<?php
namespace App\Http\Controllers;
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Helpers;
use File;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\CricketapiController;
class PlayersController extends Controller {
    public function viewplayers(){
		$findallteams= Db::table('teams')->select('id','team')->get();
		$query = DB::table('player_details')->join('players','players.player_key','=','player_details.player_key')->join('teams','teams.id','=','players.team');
		if(request()->has('playername')){
			$name=request('playername');
			if($name!=""){
				$query->where('player_details.fullname', 'LIKE', '%'.$name.'%');
			}
		}
		if(request()->has('team')){
			$team = request('team');
			if($team!=""){
				$query->where('players.team', '=',$team);
			}
		}
		if(request()->has('role')){
			$role = request('role');
			if($role!=""){
				$query->where('players.role', '=',$role);
			}
		}
        $allplayers = $query->select('teams.team as teamname','player_details.*','player_details.player_key as player_key','players.id as id','players.role','players.credit')->orderBy('fullname','ASC')->groupBy('player_details.player_key')->paginate(50);
        return view('players.viewplayers',compact('allplayers','findallteams'));
    }
	public function saveplayerroles(Request $request){
		if ($request->isMethod('post')){
			$input = Input::all();
			$data['credit'] = $input['credit'];
			$findplayerkey = DB::table('players')->where('id',$input['id'])->select('player_key')->first();
			$playerkey= $findplayerkey->player_key;
			$findplayers = DB::tables('players')->where('player_key',$playerkey)->get();
			if(!empty($findplayers)){
				foreach($findplayers as $pl){
					DB::table('players');
					DB::table('players')->where('id',$pl->id)->update($data);
				}
			}
			echo 1;die;
		}
	}
	public function editplayer($id,Request $request){
		 $id = unserialize(base64_decode($id));
		 $playerskeyget = DB::table('players')->where('id',$id)->select('player_key')->first();
		 $player = DB::table('player_details')->join('players','players.player_key','=','player_details.player_key')->join('teams','teams.id','=','players.team')->select('teams.team as teamname','players.player_key as player_key','player_details.*','players.credit','players.role','players.id as pid')->where('player_details.player_key',$playerskeyget->player_key)->first();
		 if(!empty($player)){
			 if ($request->isMethod('post')){
				$input = Input::all();
				unset($input['_token']);
				$playerdetail['fullname'] = $input['player_name'];
				$playerdetail['country'] = $input['country'];
				$playerdetail['bowling_style'] = $input['bowling_style'];
				$playerdetail['batting_style'] = $input['batting_style'];
				$playerdetail['dob'] = date('Y-m-d',strtotime($input['dob']));
				/*$file = array_filter($input['image']);
			    if(!empty($input['image'])){
					if(!empty($file)){
					  $destinationPath = 'uploads/players'; 
					  $fileName = 'batball-player-'.rand(1000,9999);
					  $imageName = Helpers::imageUpload($file,$destinationPath,$fileName);
					  if($player->image!=""){
						  File::delete($destinationPath.'/'.$player->image);
					  }
					  $playerdetail['image'] = $imageName;
					}
				}*/
				
				DB::table('player_details')->where('player_key',$playerskeyget->player_key)->update($playerdetail);
				/* to update credits and roles */
				$playerd['credit'] = $input['credit'];
				$playerd['role'] = $input['role'];
				$playerd['player_name'] = $input['player_name'];
				$findallplay = DB::table('players')->where('player_key',$playerskeyget->player_key)->get();
				if(!empty($findallplay)){
					foreach($findallplay as $fplay){
						DB::table('players');
						DB::table('players')->where('id',$fplay->id)->update($playerd);
					}
				}
				Session::flash('message', 'Player updated!');
				Session::flash('alert-class', 'alert-success');
				return Redirect::back();
			 } 
			 return view('players.editplayer',compact('player'));
		 }
		 else{
			 return redirect()->action('PlayersController@viewplayers')->withErrors('Invalid Id Provided');
		 }
	}
	public function addplayermanually(Request $request){
		if ($request->isMethod('post')){
			$input = Input::all();
			$matchkikey = $input['matchkey'];
			$sport_type = $input['sport_type'] ? $input['sport_type'] : 1; 
			$findplayerexist = DB::table('players')->where('player_key',$input['player_key'])->where('team',$input['team'])->first();
			$data['player_name'] = $input['player_name'];
			$data['player_key'] = $input['player_key'];
			$data['role'] =  $input['role'];
			$data['sport_type'] =  $sport_type;
			/* insert in player, unique for a player playing for a team*/
			if(empty($findplayerexist)){
				$data['team'] = $input['team'];
				$playerid = DB::table('players')->insertGetId($data); 
				$credit=$input['credit'];
			}
			else{
				$playerid = $findplayerexist->id;
				$credit = $findplayerexist->credit;
			}
			/* insert in player details, unique for a player */
			$findplayerdetails = DB::table('player_details')->where('player_key',$input['player_key'])->first();
			if(empty($findplayerdetails)){
				$fdata['fullname'] = $input['player_name'];
				$fdata['player_key'] = $input['player_key'];
				$fdata['sport_type'] =  $sport_type;
				DB::table('player_details')->insert($fdata);
			}
			// insert players for a match//
			$findplayer1entry = DB::table('match_players')->where('matchkey',$matchkikey)->where('playerid',$playerid)->first();
			if(empty($findplayer1entry)){
				$matchplayerdata['matchkey'] = $matchkikey;
				$matchplayerdata['playerid'] = $playerid;
				$matchplayerdata['role'] = $data['role'];
				$matchplayerdata['name'] = $data['player_name'];
				$matchplayerdata['credit'] = $credit;
				$matchplayerdata['sport_type'] =  $sport_type;
				DB::table('match_players')->insert($matchplayerdata);
			}
			Session::flash('message', 'Successfully added the player!');
			Session::flash('alert-class', 'alert-success'); 
			return Redirect::back(); 
		}
	}
}
?>