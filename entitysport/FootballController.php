<?php
namespace App\Http\Controllers\entitysport;
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Hash;
use Mail;
use Cache;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

define('ENTITY_SPORT_API_URL','https://rest.entitysport.com/'); //live
define('ENTITY_SPORT_TOKEN_FOOTBALL','42f1a5acc62933fca5c8d421695b53bd'); //live
define('ENTITY_SPORT_TOKEN_LIVE','07a72623ee9f1c147918f1c71c461fe0'); //live

class FootballController extends Controller {
    function __construct()
    {

    }
    public function accessrules(){
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Authorization');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
    }

    public static function genrateToken() {
        return ENTITY_SPORT_TOKEN_FOOTBALL;
    }

    /*
     * Get All Matches Informations
     *
    */
    public static function entity_getMatches(){
        $token = FootballController::genrateToken();
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => ENTITY_SPORT_API_URL."soccer/matches/?token=".$token."&status=1&per_page=100&=&paged=1",  //status=1 upcomming
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $result = json_decode($response);
            // print_r($result); exit;
            if($result->status=="ok") {
                $response = $result->response;
                $data = $response->items;
                return $data;
            }
        }
    }


    /*
     * Get All Matches Squad/Player Informations
     *
    */
    public static function entity_get_match_squad($match_id, $comp_id) {
        $token = FootballController::genrateToken();
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => ENTITY_SPORT_API_URL."soccer/matches/".$match_id."/newfantasy?token=".$token."&status=1&per_page=100&=&paged=1",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $result = json_decode($response);
            if($result->status=="ok") {
                $response = $result->response;
                return $response;
            }
        }
    }



    /*
     * Get All Matches Score Informations
     *
    */
    public static function entity_get_match_score($match_id) {
        $token = FootballController::genrateToken();
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => ENTITY_SPORT_API_URL."/soccer/matches/".$match_id."/newfantasy?token=".$token,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $result = json_decode($response);
            if($result->status=="ok") {
                $response = $result->response;
                return $response;
            }
        }
    }

    /*
     * Get All Players Informations
     *
    */
    public static function entity_get_match_playing_players($api_unique_id) {
        $token = FootballController::genrateToken();
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => ENTITY_SPORT_API_URL."/soccer/matches/".$api_unique_id."/info?token=".$token."",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $result = json_decode($response);
            if($result->status=="ok" && isset($result->response->items->lineup) && !empty($result->response->items->lineup)) {
                $response = $result->response->items->lineup;
                $playing_players = array();
                if(isset($response->home->lineup->player) && isset($response->away->lineup->player)) {
                    $teama_players = $response->home->lineup->player;
                    $teamb_players = $response->away->lineup->player;
                    $players = array_merge($teama_players, $teamb_players);
                    foreach ($players as $player) {
                        $playing_players[] = $player->pid;
                    }
                }
                return $playing_players;
            }
        }
    }


    /*
   * Get Single Match informations
   *
  */
    public static function entity_get_match_single_info($matchKey) {
        $token = FootballController::genrateToken();
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => ENTITY_SPORT_API_URL."/soccer/matches/".$matchKey."/info?token=".$token."",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $result = json_decode($response);
            if($result->status=="ok" && isset($result->response->items->lineup) && !empty($result->response->items->lineup)) {
                $response = $result->response->items;
                return $response;
            }
        }
    }

    /*
     * Get All Football Score Informations
     *
    */
    public static function getFootballScore($match_key) {

       $scoreList=self::entity_get_match_score($match_key);

        if($scoreList->items->playerstats!=NULL){
            $i=0;
            $arrayPlayers=array_merge($scoreList->items->playerstats->home,$scoreList->items->playerstats->away);

            foreach ($arrayPlayers as $key1 => $value1) {
                $datasvv['player_key'] = $value1->pid;
                $datasvv['starting11'] = 1;
                $datasvv['match_key'] = $match_key;
                $datasvv['sport_type'] = 2;
                $datasvv['innings'] = 1;
                $player = DB::table('match_players')->join('players', 'players.id', '=', 'match_players.playerid')->where('matchkey', $match_key)->where('match_players.sport_type', 2)->where('players.sport_type', 2)->select('match_players.*', 'players.player_key', 'players.role as playerrole' , 'players.team')->where('players.player_key', $datasvv['player_key'])->first();

                if (!empty($player)) {
                    $datasvv['player_id'] = $player->playerid;

                    if (!empty($player)) {

                        $goalconceded           =   $value1->goalsconceded?$value1->goalsconceded : 0;
                        $cleansheet             = 0;
                        $playingTime            = $value1->minutesplayed? $value1->minutesplayed: 0;
                        $goals                  = $value1->owngoal?$value1->owngoal: 0;
                        $assist                 = $value1->assist ? $value1->assist: 0;
                        $passes                 = $value1->passes? $value1->passes: 0;
                        $shots                  = $value1->shotsontarget? $value1->shotsontarget : 0;
                        $saveShots              = $value1->shotssaved ? $value1->shotssaved : 0;
                        $penaltySave            = $value1->penaltysaved? $value1->penaltysaved : 0;
                        $tackles                = $value1->tacklesuccessful? $value1->tacklesuccessful: 0;
                        $block = 0;
                        $interceptions          = $value1->interceptionwon? $value1->interceptionwon : 0;
                        $totalclearance = 0;
                        $substitute             = $value1->substitute?$value1->substitute: 0;
                        $chanceCreated          = $value1->chancecreated? $value1->chancecreated : 0;
                        $yellowCard             = $value1->yellowcard ? $value1->yellowcard : 0;
                        $redCard                = $value1->redcard ? $value1->redcard : 0;

                        $ownGoal                = 0;
                        $penaltyMiss            = $value1->penaltymissed ? $value1->penaltymissed  : 0;

                        if($playingTime>0) {
                            $datasv[$i]['minutesplayed']        = $playingTime;
                            $datasv[$i]['goals']                = $goals;
                            $datasv[$i]['assist']               = $assist;
                            $datasv[$i]['totalpass']            = $passes;
                            $datasv[$i]['shotsontarget']        = $shots;
                            $datasv[$i]['cleansheet']           = $cleansheet;
                            $datasv[$i]['block']                = $block;
                            $datasv[$i]['shotsblocked']         = $saveShots;
                            $datasv[$i]['penaltysave']          = $penaltySave;
                            $datasv[$i]['tacklesuccess']        = $tackles;
                            if($redCard>0)
                                $yellowCard=0;
                            $datasv[$i]['yellowcard']           = $yellowCard;
                            $datasv[$i]['redcard']              = $redCard;
                            $datasv[$i]['substitute']           = $substitute;
                            $datasv[$i]['playing11']            = 1-$substitute;
                            $datasv[$i]['owngoals']             = $ownGoal;
                            $datasv[$i]['penaltymiss']          = $penaltyMiss;
                            $datasv[$i]['goalconceded']         = $goalconceded;
                            $datasv[$i]['interceptionsWon']     = $interceptions;
                            $datasv[$i]['chanceCreated']        = $chanceCreated;
                            $datasv[$i]['totalclearance']       = $totalclearance;
                            $datasv[$i]['sport_type'] = 2;
                            $datasv[$i]['match_key'] = $match_key;
                            $datasv[$i]['player_key'] = $datasvv['player_key'];
                            $datasv[$i]['player_id'] = $player->playerid;
                            $datasv[$i]['total_points'] = 0;
                            $datasv[$i]['starting11'] = 1;

                            $findplayerex = DB::table('result_matches')->where('player_key',$datasvv['player_key'])->where('match_key',$match_key)->select('id','isLocked')->first();
                            if(!empty($findplayerex)){
                                if($findplayerex->isLocked==0){
                                    DB::table('result_matches')->where('id',$findplayerex->id)->update($datasv[$i]);
                                }
                            }else{
                                DB::table('result_matches')->insert($datasv[$i]);
                            }
                        }
                        $i++;
                    }
                }
            }
            return true;
        }
        return false;
    }

    /*
       * Get All entity_get_playing_11_football
       *
      */
    public static function entity_get_playing_11_football($match_key) {
        $lineUpInformations=self::entity_get_match_single_info($match_key);

        if (!empty($lineUpInformations)){

            date_default_timezone_set('Asia/Kolkata');
            $timeNow = date('Y-m-d H:i:s');

            $findmatch = DB::table('list_matches')->where('matchkey',$match_key)->first();
            $findmatchOnlyAdmin = DB::table('list_matches')->where('matchkey',$match_key)->where('only_admin',0)->first();

            if(!empty($findmatchOnlyAdmin)){
                $findseries = DB::table('series')->where('id',$findmatchOnlyAdmin->series)->first();
                if(!empty($findseries)){
                    $seriesName=str_replace($findseries->name,"? ","");
                    $pos = strpos($findseries->name,"|");
                    $seriesName= $pos !== FALSE ? substr($findseries->name, $pos + strlen("|"), strlen($findseries->name)) : "";
                    $findnotifications = DB::table('bulk_notifications')->where('matchkey',$match_key)->where('isSent',0)->get();
                    if(!empty($findnotifications)){
                        foreach($findnotifications as $notification){
                            if(!empty($notification->minutes)){
                                $update_data = array(
                                    "isSent" => 1
                                );
                                if($notification->preToss==0){
                                    $timeBefore1 = date('Y-m-d H:i:s',strtotime('-'.$notification->minutes.' minutes', strtotime($findmatchOnlyAdmin->start_date)));
                                    if($timeBefore1<=$timeNow)
                                    {
                                        DB::table('bulk_notifications')->where('matchkey', $match_key)->where('id',$notification->id)->update($update_data);
                                        Helpers::sendnotificationBulk($notification->display." minutes to go ⏰ ".$findmatchOnlyAdmin->short_name,"⚽⚽".$seriesName." ⚽⚽\n".$findmatchOnlyAdmin->name."\nMatch Time ⏰: ".date('h:i A',strtotime($findmatchOnlyAdmin->start_date)),'FANADDA2');
                                    }
                                }
                                else if($notification->preToss==1 && $findmatchOnlyAdmin->PreTossAvailable==1 && !empty($findmatchOnlyAdmin->PreTossClosingTime)){
                                    $timeBefore1 = date('Y-m-d H:i:s',strtotime('-'.$notification->minutes.' minutes', strtotime($findmatchOnlyAdmin->PreTossClosingTime)));
                                    if($timeBefore1<=$timeNow)
                                    {
                                        DB::table('bulk_notifications')->where('matchkey', $match_key)->where('id',$notification->id)->update($update_data);
                                        Helpers::sendnotificationBulk($notification->display." minutes to go ⏰ PreToss Reminder",$findmatchOnlyAdmin->short_name."\n⚽⚽".$seriesName." ⚽⚽\nPreToss Deadline ⏰: ".date('h:i A',strtotime($findmatchOnlyAdmin->PreTossClosingTime)),'FANADDA2');
                                    }
                                }
                            }
                        }
                    }
                }
            }


            if (!empty($lineUpInformations->lineup)){
                self::entity_importAfterLineup($match_key,$lineUpInformations);

                $playing_players = array();
                if(isset($lineUpInformations->lineup->home->lineup->player) && isset($lineUpInformations->lineup->away->lineup->player)) {
                    $teama_players = $lineUpInformations->lineup->home->lineup->player;
                    $teamb_players = $lineUpInformations->lineup->away->lineup->player;
                    $players = array_merge($teama_players, $teamb_players);
                    foreach ($players as $player) {
                        $player_details = DB::table('players')->join('match_players','players.id','=','match_players.playerid')->where('players.player_key',$player->pid)->where('match_players.sport_type',2)->where('match_players.matchkey', $match_key)->select('players.id')->first();
                        if(!empty($player_details)) {
                            array_push($playing_players,$player_details->id);
                        }
                    }
                }

                //echo count($startXI);
                if(count($playing_players)){
                    $insert_data = array(
                        "match_key" => $match_key,
                        "player_ids" => serialize($playing_players),
                    );

                    $findplayerex = DB::table('match_playing11')->where('match_key',$match_key)->where('type', 'main')->first();
                    if(!empty($findplayerex)){
                        if($findplayerex->isLocked==0){
                            DB::table('match_playing11')->where('match_key', $match_key)->where('type', 'main')->update($insert_data);
                        }
                    }else{
                        DB::table('match_playing11')->insert($insert_data);
                        DB::table('list_matches')->where('matchkey',$match_key)->where('only_admin',0)->first();
                        $timeBefore = date('Y-m-d H:i:s',strtotime('-1 minutes', strtotime($findmatch->start_date)));
                        if(!empty($findmatch) && ($timeBefore>=$timeNow)) {
                            Helpers::sendnotificationBulk("Lineup Out 📢 ".$findmatch->short_name,"⚽⚽".$seriesName." ⚽⚽\n".$findmatch->name."\nMatch Time ⏰: ".date('h:i A',strtotime($findmatch->start_date)),'FANADDA2');
                        }
                    }
                }
            }

        }
    }


    public static function entity_importAfterLineup($match_key,$match) {

        $teamOneKey                  =$match->match_info[0]->teams->home->tid;
        $teamTwoKey                   =$match->match_info[0]->teams->away->tid;

        if (!empty($match->lineup->home)){
            if (!empty($teamOneKey)){
                $findmatchexist = DB::table('teams')->where('team_key', $teamOneKey)->where('src', 'entity')->select('id')->first();
                if ($findmatchexist) {

                    $teamOnePlayersList = $match->lineup->home->lineup->player;

                    foreach ($teamOnePlayersList as $key => $playerItem) {
                        $find_player = DB::table('players')->where('player_key', $playerItem->pid)->first();

                        $playerRole=strtolower($playerItem->name);

                            if ($playerItem->name=='Forward'){
                            $playerRole='striker';
                        }

                        //Store Players Details Inforamtions
                        if (empty($find_player)) {
                            $player_insert = array();
                            $player_insert['player_name']   = $playerItem->pname;
                            $player_insert['player_key']    = $playerItem->pid;
                            $player_insert['sport_type']    = 2;
                            $player_insert['team']          = $findmatchexist->id;
                            $player_insert['credit']        = 8;
                            $player_insert['role']          = $playerRole;
                            $player_id = DB::table('players')->insertGetId($player_insert);
                        } else {
                            $player_insert = array();
                            $player_insert['player_name']   = $find_player->player_name;
                            $player_insert['player_key']    = $find_player->player_key;
                            $player_insert['team']          = $findmatchexist->id;
                            $player_insert['role']          = $find_player->role;
                            $player_insert['credit']        = $find_player->credit;
                            $player_id = $find_player->id;
                        }


                        //Store Player Details Inforamtions
                        $findp1 = DB::table('player_details')->where('player_key',$player_insert['player_key'])->first();
                        if(empty($findp1)){
                            $plaerdetailsdata['fullname']       = $player_insert['player_name'];
                            $plaerdetailsdata['player_key']     = $player_insert['player_key'];
                            $plaerdetailsdata['sport_type']     = 2;
                            DB::table('player_details')->insert($plaerdetailsdata);
                        }

                        //Store Match Players Details Inforamtions
                        $find_match_player = DB::table('match_players')->where('playerid', $player_id)->where('matchkey', $match_key)->first();
                        if (empty($find_match_player)) {
                            $matchplayerdata['matchkey'] = $match_key;
                            $matchplayerdata['playerid'] = $player_id;
                            $matchplayerdata['role'] = $player_insert['role'];
                            $matchplayerdata['name'] = $player_insert['player_name'];
                            $matchplayerdata['credit'] = $player_insert['credit'];
                            $matchplayerdata['player_key'] = $playerItem->pid;
                            $matchplayerdata['sport_type'] = 2;
                            DB::table('match_players')->insert($matchplayerdata);
                        }

                    }
                }
            }
        }

        if (isset($match->lineup->away)) {
            if ($teamOneKey) {
                $findmatchexist = DB::table('teams')->where('team_key', $teamTwoKey)->where('src', 'entity')->select('id')->first();
                if ($findmatchexist) {
                    $teamOnePlayersList = $match->lineup->away->lineup->player;

                    foreach ($teamOnePlayersList as $key => $playerItem) {
                        $find_player = DB::table('players')->where('player_key', $playerItem->pid)->first();

                        $playerRole=strtolower($playerItem->name);

                        if ($playerItem->name=='Forward'){
                            $playerRole='striker';
                        }

                        //Store Players Details Inforamtions
                        if (empty($find_player)) {
                            $player_insert = array();
                            $player_insert['player_name']   = $playerItem->pname;
                            $player_insert['player_key']    = $playerItem->pid;
                            $player_insert['sport_type']    = 2;
                            $player_insert['team']          = $findmatchexist->id;
                            $player_insert['credit']        = 8;
                            $player_insert['role']          = $playerRole;
                            $player_id = DB::table('players')->insertGetId($player_insert);
                        } else {
                            $player_insert = array();
                            $player_insert['player_name']   = $find_player->player_name;
                            $player_insert['player_key']    = $find_player->player_key;
                            $player_insert['team']          = $findmatchexist->id;
                            $player_insert['role']          = $find_player->role;
                            $player_insert['credit']        = $find_player->credit;
                            $player_id = $find_player->id;
                        }


                        //Store Player Details Inforamtions
                        $findp1 = DB::table('player_details')->where('player_key',$player_insert['player_key'])->first();
                        if(empty($findp1)){
                            $plaerdetailsdata['fullname']       = $player_insert['player_name'];
                            $plaerdetailsdata['player_key']     = $player_insert['player_key'];
                            $plaerdetailsdata['sport_type']     = 2;
                            DB::table('player_details')->insert($plaerdetailsdata);
                        }

                        //Store Match Players Details Inforamtions
                        $find_match_player = DB::table('match_players')->where('playerid', $player_id)->where('matchkey', $match_key)->first();
                        if (empty($find_match_player)) {
                            $matchplayerdata['matchkey']        = $match_key;
                            $matchplayerdata['playerid']        = $player_id;
                            $matchplayerdata['role']            = $player_insert['role'];
                            $matchplayerdata['name']            = $player_insert['player_name'];
                            $matchplayerdata['credit']          = $player_insert['credit'];
                            $matchplayerdata['player_key']      = $playerItem->pid;
                            $matchplayerdata['sport_type']      = 2;
                            DB::table('match_players')->insert($matchplayerdata);
                        }
                    }
                }
            }
        }
    }

    //Store Entity Players Informations
    public static function storeEntityPlayers($matchid = ''){

        if ($matchid) {

            $response=0;
            $matchDetails = FootballController::entity_get_match_squad($matchid, '');

            //Save Team Onc Players
            if (isset($matchDetails->items->teams->home)) {
                $teamOneKey = $matchDetails->items->match_info->teams->home->tid;
                if ($teamOneKey) {
                    $findmatchexist = DB::table('teams')->where('team_key', $teamOneKey)->where('src', 'entity')->select('id')->first();
                    if ($findmatchexist) {
                        $teamOnePlayersList = $matchDetails->items->teams->home;
                        foreach ($teamOnePlayersList as $key => $playerItem) {
                            $find_player = DB::table('players')->where('player_key', $playerItem->pid)->first();

                            $playerRole=strtolower($playerItem->role);

                            if ($playerItem->role=='Forward'){
                                $playerRole='striker';
                            }

                            //Store Players Details Inforamtions
                            if (empty($find_player)) {
                                $player_insert = array();
                                $player_insert['player_name']   = $playerItem->pname;
                                $player_insert['player_key']    = $playerItem->pid;
                                $player_insert['sport_type']    = 2;
                                $player_insert['team']          = $findmatchexist->id;
                                $player_insert['credit']        = 8;
                                $player_insert['role']          = $playerRole;
                                $player_id = DB::table('players')->insertGetId($player_insert);
                            } else {
                                $player_insert = array();
                                $player_insert['player_name']   = $find_player->player_name;
                                $player_insert['player_key']    = $find_player->player_key;
                                $player_insert['team']          = $findmatchexist->id;
                                $player_insert['role']          = $find_player->role;
                                $player_insert['credit']        = $find_player->credit;
                                $player_id = $find_player->id;
                            }


                            //Store Player Details Inforamtions
                            $findp1 = DB::table('player_details')->where('player_key',$player_insert['player_key'])->first();
                            if(empty($findp1)){
                                $plaerdetailsdata['fullname']       = $player_insert['player_name'];
                                $plaerdetailsdata['player_key']     = $player_insert['player_key'];
                                $plaerdetailsdata['sport_type']     = 2;
                                DB::table('player_details')->insert($plaerdetailsdata);
                            }

                            //Store Match Players Details Inforamtions
                            $find_match_player = DB::table('match_players')->where('playerid', $player_id)->where('matchkey', $matchid)->first();
                            if (empty($find_match_player)) {
                                $matchplayerdata['matchkey'] = $matchid;
                                $matchplayerdata['playerid'] = $player_id;
                                $matchplayerdata['role'] = $player_insert['role'];
                                $matchplayerdata['name'] = $player_insert['player_name'];
                                $matchplayerdata['credit'] = $player_insert['credit'];
                                $matchplayerdata['player_key'] = $playerItem->pid;
                                $matchplayerdata['sport_type'] = 2;
                                DB::table('match_players')->insert($matchplayerdata);
                            }

                        }
                    }
                }

                $response=1;
            }

            //Save Team Two  Players
            if (isset($matchDetails->items->teams->away)) {
                $teamTwoKey = $matchDetails->items->match_info->teams->away->tid;
                if ($teamOneKey) {
                    $findmatchexist = DB::table('teams')->where('team_key', $teamTwoKey)->where('src', 'entity')->select('id')->first();
                    if ($findmatchexist) {
                        $teamOnePlayersList = $matchDetails->items->teams->away;
                        foreach ($teamOnePlayersList as $key => $playerItem) {
                            $find_player = DB::table('players')->where('player_key', $playerItem->pid)->first();

                            $playerRole=strtolower($playerItem->role);

                            if ($playerItem->role=='Forward'){
                                $playerRole='striker';
                            }

                            //Store Players Details Inforamtions
                            if (empty($find_player)) {
                                $player_insert = array();
                                $player_insert['player_name']   = $playerItem->pname;
                                $player_insert['player_key']    = $playerItem->pid;
                                $player_insert['sport_type']    = 2;
                                $player_insert['team']          = $findmatchexist->id;
                                $player_insert['credit']        = 8;
                                $player_insert['role']          = $playerRole;
                                $player_id = DB::table('players')->insertGetId($player_insert);
                            } else {
                                $player_insert = array();
                                $player_insert['player_name']   = $find_player->player_name;
                                $player_insert['player_key']    = $find_player->player_key;
                                $player_insert['team']          = $findmatchexist->id;
                                $player_insert['role']          = $find_player->role;
                                $player_insert['credit']        = $find_player->credit;
                                $player_id = $find_player->id;
                            }


                            //Store Player Details Inforamtions
                            $findp1 = DB::table('player_details')->where('player_key',$player_insert['player_key'])->first();
                            if(empty($findp1)){
                                $plaerdetailsdata['fullname']       = $player_insert['player_name'];
                                $plaerdetailsdata['player_key']     = $player_insert['player_key'];
                                $plaerdetailsdata['sport_type']     = 2;
                                DB::table('player_details')->insert($plaerdetailsdata);
                            }

                            //Store Match Players Details Inforamtions
                            $find_match_player = DB::table('match_players')->where('playerid', $player_id)->where('matchkey', $matchid)->first();
                            if (empty($find_match_player)) {
                                $matchplayerdata['matchkey']        = $matchid;
                                $matchplayerdata['playerid']        = $player_id;
                                $matchplayerdata['role']            = $player_insert['role'];
                                $matchplayerdata['name']            = $player_insert['player_name'];
                                $matchplayerdata['credit']          = $player_insert['credit'];
                                $matchplayerdata['player_key']      = $playerItem->pid;
                                $matchplayerdata['sport_type']      = 2;
                                DB::table('match_players')->insert($matchplayerdata);
                            }
                        }
                    }
                }

                $response=1;
            }

            return $response;
        }
    }

}
