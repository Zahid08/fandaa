<?php
namespace App\Http\Controllers;
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Hash;
use Carbon\Carbon;
use PHPExcel_IOFactory;
use PHPExcel_Cell_DataType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
class DatamigrateController extends Controller {
	public function updatestates(Request $request){
		// set_include_path(get_include_path() . PATH_SEPARATOR . 'Classes/');
		require_once("./phpexcel/PHPExcel/IOFactory.php");
		// include 'PHPExcel/IOFactory.php';
		
		if ( isset($_FILES["file"])){
			//if there was an error uploading the file
		
			
			if ($_FILES["file"]["error"] > 0) {
				echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
			}
			else {
				if (file_exists($_FILES["file"]["name"])) {
					unlink($_FILES["file"]["name"]);
				}
				$storagename = "./excelfile/discussdesk.xlsx";
				move_uploaded_file($_FILES["file"]["tmp_name"],  $storagename);
				$uploadedStatus = 1;
				$inputFileName = './excelfile/discussdesk.xlsx'; 
				try {
					$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
				} catch(Exception $e) {
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
				}
				$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
				if(!empty($allDataInSheet)){
				    
					$arrayCount = count($allDataInSheet);
					for($i=2;$i<=$arrayCount;$i++){
						$data=array();
						if($i>$arrayCount){
							die;
						}else{
							$data = array();
							$data['id'] = trim($allDataInSheet[$i]["A"]);
							$data['email'] = trim($allDataInSheet[$i]["B"]);
							$finduserentry = DB::table('register_users')->where('id',$data['id'])->where('email',$data['email'])->first();
							if(!empty($finduserentry)){
								$dataupdate['state'] = trim($allDataInSheet[$i]["C"]);
								DB::table('register_users')->where('id',$data['id'])->where('email',$data['email'])->update($dataupdate);
							}
							
						}
					}
				}
			}
		}
		
		return view('datamigrate.importusers');
	}
	
	
	public function importusers(Request $request){
	    
		// set_include_path(get_include_path() . PATH_SEPARATOR . 'Classes/');
		require_once("./phpexcel/PHPExcel/IOFactory.php");
		// include 'PHPExcel/IOFactory.php';
		
		if ( isset($_FILES["file"])){
			//if there was an error uploading the file
		
			
			if ($_FILES["file"]["error"] > 0) {
				echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
			}
			else {
				if (file_exists($_FILES["file"]["name"])) {
					unlink($_FILES["file"]["name"]);
				}
				$storagename = "./excelfile/discussdesk.xlsx";
				move_uploaded_file($_FILES["file"]["tmp_name"],  $storagename);
				$uploadedStatus = 1;
				$inputFileName = './excelfile/discussdesk.xlsx'; 
				try {
					$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
				} catch(Exception $e) {
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
				}
				$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
				if(!empty($allDataInSheet)){
				    
					$arrayCount = count($allDataInSheet);
					for($i=2;$i<=$arrayCount;$i++){
						$data=array();
						if($i>$arrayCount){
							die;
						}else{
							$data = array();
							$data['id'] = trim($allDataInSheet[$i]["A"]);
							if(trim($allDataInSheet[$i]["B"])!=NULL && trim($allDataInSheet[$i]["B"])!=null){
								$data['refer_id'] = trim($allDataInSheet[$i]["B"]);
							}
							$data['username'] = trim($allDataInSheet[$i]["C"]);
							$data['mobile'] = trim($allDataInSheet[$i]["D"]);
							$data['email'] = trim($allDataInSheet[$i]["E"]);
							if(trim($allDataInSheet[$i]["F"])!=""){
								$data['dob'] = date('Y-m-d',strtotime(trim($allDataInSheet[$i]["F"])));
							}
							$data['gender'] = strtolower(trim($allDataInSheet[$i]["G"]));
							$data['password'] = trim($allDataInSheet[$i]["H"]);
							if(trim($allDataInSheet[$i]["I"])!=null && trim($allDataInSheet[$i]["I"])!="" && trim($allDataInSheet[$i]["I"])!=NULL){
								$data['image'] = 'http://admin.select2win.com/uploads/'.trim($allDataInSheet[$i]["I"]);
							}
							$data['activation_status'] = trim($allDataInSheet[$i]["J"]);
							$data['code'] = trim($allDataInSheet[$i]["K"]);
							$data['address'] = trim($allDataInSheet[$i]["L"]);
							$data['city'] = trim($allDataInSheet[$i]["M"]);
							$data['state'] = trim($allDataInSheet[$i]["N"]);
							$data['pincode'] = trim($allDataInSheet[$i]["O"]);
							$data['team'] = trim($allDataInSheet[$i]["P"]);
							$data['mobile_verify'] = trim($allDataInSheet[$i]["Q"]);
							$data['email_verify'] = trim($allDataInSheet[$i]["R"]);
							$data['pan_verify'] = trim($allDataInSheet[$i]["S"]);
							$data['bank_verify'] = trim($allDataInSheet[$i]["T"]);
							$data['refercode'] = trim($allDataInSheet[$i]["U"]);
							$data['emailbonus'] = trim($allDataInSheet[$i]["V"]);
							$data['mobilebonus'] = trim($allDataInSheet[$i]["W"]);
							$data['panbonus'] = trim($allDataInSheet[$i]["X"]);
							$data['bankbonus'] = trim($allDataInSheet[$i]["Y"]);
							if(trim($allDataInSheet[$i]["E"])!=""){
								DB::table('register_users');
								DB::table('register_users')->insert($data);
							}
						}
					}
				}
			}
		}
		
		return view('datamigrate.importusers');
		
	}
	public function importuserbalances(){
		// set_include_path(get_include_path() . PATH_SEPARATOR . 'Classes/');
		require_once("./phpexcel/PHPExcel/IOFactory.php");
		// include 'PHPExcel/IOFactory.php';
		
		if ( isset($_FILES["file"])){
			//if there was an error uploading the file
			
			if ($_FILES["file"]["error"] > 0) {
				echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
			}
			else {
				if (file_exists($_FILES["file"]["name"])) {
					unlink($_FILES["file"]["name"]);
				}
				$storagename = "./excelfile/discussdesk.xlsx";
				move_uploaded_file($_FILES["file"]["tmp_name"],  $storagename);
				$uploadedStatus = 1;
				$inputFileName = './excelfile/discussdesk.xlsx'; 
				try {
					$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
				} catch(Exception $e) {
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
				}
				$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
				// echo "<pre>";
				// print_r($allDataInSheet);
				// die;
				if(!empty($allDataInSheet)){
					$arrayCount = count($allDataInSheet);
					for($i=2;$i<=$arrayCount;$i++){
						$data=array();
						if($i>$arrayCount){
							die;
						}else{
							
							
							// if(trim($allDataInSheet[$i]["A"])!="" && trim($allDataInSheet[$i]["A"])!=0){
								$data['user_id'] = trim($allDataInSheet[$i]["A"]);
								$data['balance'] = trim($allDataInSheet[$i]["B"]);
								$data['winning'] = trim($allDataInSheet[$i]["C"]);
								$data['bonus'] = trim($allDataInSheet[$i]["D"]);
								DB::table('user_balances');
								DB::table('user_balances')->insert($data);
							// }
						}
					}
				}
			}
		}
		
		return view('datamigrate.importusers');
	}
	public function importpancard(){
		// set_include_path(get_include_path() . PATH_SEPARATOR . 'Classes/');
		require_once("./phpexcel/PHPExcel/IOFactory.php");
		// include 'PHPExcel/IOFactory.php';
		
		if ( isset($_FILES["file"])){
			//if there was an error uploading the file
			
			if ($_FILES["file"]["error"] > 0) {
				echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
			}
			else {
				if (file_exists($_FILES["file"]["name"])) {
					unlink($_FILES["file"]["name"]);
				}
				$storagename = "./excelfile/discussdesk.xlsx";
				move_uploaded_file($_FILES["file"]["tmp_name"],  $storagename);
				$uploadedStatus = 1;
				$inputFileName = './excelfile/discussdesk.xlsx'; 
				try {
					$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
				} catch(Exception $e) {
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
				}
				$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
				// echo "<pre>";
				// print_r($allDataInSheet);
				// die;
				if(!empty($allDataInSheet)){
					$arrayCount = count($allDataInSheet);
					for($i=2;$i<=$arrayCount;$i++){
						$data=array();
						if($i>$arrayCount){
							die;
						}else{
							// if(trim($allDataInSheet[$i]["A"])!="" && trim($allDataInSheet[$i]["A"])!=0){
								
									$data['userid'] = trim($allDataInSheet[$i]["A"]);
									$data['pan_name'] = trim($allDataInSheet[$i]["B"]);
									$data['pan_number'] = trim($allDataInSheet[$i]["C"]);
									if(trim($allDataInSheet[$i]["D"])!="" && trim($allDataInSheet[$i]["D"])!=null){
										$data['pan_dob'] = date('Y-m-d',strtotime(trim($allDataInSheet[$i]["D"])));
									}
									if(trim($allDataInSheet[$i]["E"])!=null && trim($allDataInSheet[$i]["E"])!="" && trim($allDataInSheet[$i]["E"])!=NULL){
										$data['image'] = 'http://admin.select2win.com/uploads/'.trim($allDataInSheet[$i]["E"]);
									}
									$data['status'] = trim($allDataInSheet[$i]["F"]);
									$data['comment'] = trim($allDataInSheet[$i]["G"]);
									DB::table('pan_cards');
									if($data['userid']!=0){
										DB::table('pan_cards')->insert($data);
									}
							// }
						}
					}
				}
			}
		}
		return view('datamigrate.importusers');
	}
	public function importbank(){
		// set_include_path(get_include_path() . PATH_SEPARATOR . 'Classes/');
		require_once("./phpexcel/PHPExcel/IOFactory.php");
		// include 'PHPExcel/IOFactory.php';
		
		if ( isset($_FILES["file"])){
			//if there was an error uploading the file
			
			if ($_FILES["file"]["error"] > 0) {
				echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
			}
			else {
				if (file_exists($_FILES["file"]["name"])) {
					unlink($_FILES["file"]["name"]);
				}
				$storagename = "./excelfile/discussdesk.xlsx";
				move_uploaded_file($_FILES["file"]["tmp_name"],  $storagename);
				$uploadedStatus = 1;
				$inputFileName = './excelfile/discussdesk.xlsx'; 
				try {
					$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
				} catch(Exception $e) {
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
				}
				$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
				// echo "<pre>";
				// print_r($allDataInSheet);
				// die;
				if(!empty($allDataInSheet)){
					$arrayCount = count($allDataInSheet);
					
					for($i=2;$i<=$arrayCount;$i++){
						$data=array();
						if($i>$arrayCount){
							die;
						}else{
							if(trim($allDataInSheet[$i]["A"])!="" && trim($allDataInSheet[$i]["A"])!=0){
								$data['userid'] = trim($allDataInSheet[$i]["A"]);
								// $data['accno'] = $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i,trim($allDataInSheet[$i]["B"]), PHPExcel_Cell_DataType::TYPE_STRING); 
								$data['accno'] = trim($allDataInSheet[$i]["B"]);
								if(trim($allDataInSheet[$i]["C"])!=NULL && trim($allDataInSheet[$i]["C"])!=null){
									$data['ifsc'] = trim($allDataInSheet[$i]["C"]);
								}else{
									$data['ifsc'] = "";
								}
								if(trim($allDataInSheet[$i]["D"])!=NUll && trim($allDataInSheet[$i]["D"])!=null){
									$data['bankname'] = trim($allDataInSheet[$i]["D"]);
								}else{
									$data['bankname'] = "";
								}
								$data['bankbranch'] = trim($allDataInSheet[$i]["E"]);
								if(trim($allDataInSheet[$i]["F"])!='N-A'){
									$data['state'] = trim($allDataInSheet[$i]["F"]);
								}else{
									$data['state']="";
								}
								$data['status'] = trim($allDataInSheet[$i]["G"]);
								if(trim($allDataInSheet[$i]["H"])!="" && trim($allDataInSheet[$i]["H"])!=null && trim($allDataInSheet[$i]["H"])!=NULL){
									$data['image'] = 'http://admin.select2win.com/uploads/'.trim($allDataInSheet[$i]["H"]);
								}else{
									$data['image'] ="";
								}
								$data['comment'] = trim($allDataInSheet[$i]["I"]);
								// echo "<pre>";
								// print_r($data);
								// die;
								DB::table('banks');
								// if($data['accno']!="" && ){
									DB::table('banks')->insert($data);
								// }
							}
						}
					}
					die;
				}
			}
		}
		
		return view('datamigrate.importusers');
	}
	public function importwithdraw(){
		// set_include_path(get_include_path() . PATH_SEPARATOR . 'Classes/');
		require_once("./phpexcel/PHPExcel/IOFactory.php");
		// include 'PHPExcel/IOFactory.php';
		
		if ( isset($_FILES["file"])){
			//if there was an error uploading the file
			
			if ($_FILES["file"]["error"] > 0) {
				echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
			}
			else{
				if (file_exists($_FILES["file"]["name"])) {
					unlink($_FILES["file"]["name"]);
				}
				$storagename = "./excelfile/discussdesk.xlsx";
				move_uploaded_file($_FILES["file"]["tmp_name"],  $storagename);
				$uploadedStatus = 1;
				$inputFileName = './excelfile/discussdesk.xlsx'; 
				try {
					$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
				} catch(Exception $e) {
					die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
				}
				$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
				// echo "<pre>";
				// print_r($allDataInSheet);
				// die;
				if(!empty($allDataInSheet)){
					$arrayCount = count($allDataInSheet);
					for($i=2;$i<=$arrayCount;$i++){
					   $data=array();
						if($i>$arrayCount){
						   die;
						}else{
							if(trim($allDataInSheet[$i]["A"])!="" && trim($allDataInSheet[$i]["A"])!=0){
								$data['user_id'] = trim($allDataInSheet[$i]["A"]);
								$data['amount'] = trim($allDataInSheet[$i]["B"]);
								$data['withdraw_request_id'] = trim($allDataInSheet[$i]["C"]);
								$data['comment'] = trim($allDataInSheet[$i]["D"]);
								if(trim($allDataInSheet[$i]["E"])!="" && trim($allDataInSheet[$i]["E"])!="NULL"){
									$data['approved_date'] = date('Y-m-d h:i:s',strtotime(trim($allDataInSheet[$i]["E"])));
								}
								$data['status'] = trim($allDataInSheet[$i]["F"]);
								$data['created'] = date('Y-m-d h:i:s',strtotime(trim($allDataInSheet[$i]["G"])));
								DB::table('withdraws');
								DB::table('withdraws')->insert($data);
							}
						}
					}
					die;
				}
			}
		}
		
		return view('datamigrate.importusers');
	}
	public function downloadusercsv(){
		$output1 = "";
		$output1 .='"id",';
		$output1 .='"refer_id",';
		$output1 .='"username",';
		$output1 .='"mobile",';
		$output1 .='"email",';
		$output1 .='"dob",';
		$output1 .='"gender",';
		$output1 .='"password",';
		$output1 .='"image",';
		$output1 .='"activation_status",';
		$output1 .='"Code",';
		$output1 .='"address",';
		$output1 .='"city",';
		$output1 .='"state",';
		$output1 .='"pincode",';
		$output1 .='"team",';
		$output1 .='"mobile_verify",';
		$output1 .='"email_verify",';
		$output1 .='"pan_verify",';
		$output1 .='"bank_verify",';
		$output1 .='"refercode",';
		$output1 .='"emailbonus",';
		$output1 .='"mobilebonus",';
		$output1 .='"panbonus",';
		$output1 .='"bankbonus",';
		$output1 .="\n";
		$query = DB::table('register_users')->get();
		if(!empty($query)){
			foreach($query as $qu){
				$output1 .=$qu->id.',';
				$output1 .=$qu->refer_id.',';
				$output1 .=$qu->username.',';
				$output1 .=$qu->mobile.',';
				$output1 .=$qu->email.',';
				$output1 .=$qu->dob.',';
				$output1 .=$qu->gender.',';
				$output1 .=$qu->password.',';
				$output1 .=$qu->image.',';
				$output1 .=$qu->activation_status.',';
				$output1 .=$qu->code.',';
				$output1 .=$qu->address.',';
				$output1 .=$qu->city.',';
				$output1 .=$qu->state.',';
				$output1 .=$qu->pincode.',';
				$output1 .=$qu->team.',';
				$output1 .=$qu->mobile_verify.',';
				$output1 .=$qu->email_verify.',';
				$output1 .=$qu->pan_verify.',';
				$output1 .=$qu->bank_verify.',';
				$output1 .=$qu->refercode.',';
				$output1 .=$qu->emailbonus.',';
				$output1 .=$qu->mobilebonus.',';
				$output1 .=$qu->panbonus.',';
				$output1 .=$qu->bankbonus.',';
				$output1 .="\n";
			}
		}
		$filename =  "all-users-details.csv";
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $output1;
		exit;
	}
	public function downloadwalletcsv(){
		$output1 = "";
		$output1 .='"userid",';
		$output1 .='"unutilized",';
		$output1 .='"winning",';
		$output1 .='"bonus",';
		$output1 .="\n";
		$query = DB::table('user_balances')->get();
		if(!empty($query)){
			foreach($query as $qu){
				$output1 .=$qu->user_id.',';
				$output1 .=$qu->balance.',';
				$output1 .=$qu->winning.',';
				$output1 .=$qu->bonus.',';
				$output1 .="\n";
			}
		}
		$filename =  "all-users-wallet-details.csv";
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $output1;
		exit;
	}
	public function downloadpancsv(){
		$output1 = "";
		$output1 .='"userid",';
		$output1 .='"pan_name",';
		$output1 .='"pan_number",';
		$output1 .='"pan_dob",';
		$output1 .='"image",';
		$output1 .='"status",';
		$output1 .='"comment",';
		$output1 .="\n";
		$query = DB::table('pan_cards')->get();
		if(!empty($query)){
			foreach($query as $qu){
				$output1 .=$qu->userid.',';
				$output1 .=$qu->pan_name.',';
				$output1 .=$qu->pan_number.',';
				if($qu->pan_dob!='0000-00-00'){
					$output1 .=$qu->pan_dob.',';
				}else{
					$output1 .=',';
				}
				$output1 .=$qu->image.',';
				$output1 .=$qu->status.',';
				$output1 .=$qu->comment.',';
				$output1 .="\n";
			}
		}
		$filename =  "all-pan-details.csv";
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $output1;
		exit;
	}public function downloadpendingpancsv(){
		$output1 = "";
		$output1 .='"userid",';
		$output1 .='"pan_name",';
		$output1 .='"pan_number",';
		$output1 .='"pan_dob",';
		$output1 .='"image",';
		$output1 .='"status",';
		$output1 .='"comment",';
		$output1 .="\n";
		$query = DB::table('pan_cards')->where('status',0)->where('image','')->get();
		if(!empty($query)){
			foreach($query as $qu){
				$output1 .=$qu->userid.',';
				$output1 .=$qu->pan_name.',';
				$output1 .=$qu->pan_number.',';
				if($qu->pan_dob!='0000-00-00'){
					$output1 .=$qu->pan_dob.',';
				}else{
					$output1 .=',';
				}
				$output1 .=$qu->image.',';
				$output1 .=$qu->status.',';
				$output1 .=$qu->comment.',';
				$output1 .="\n";
				$pandata['status'] = 2;
				$panuserdata['pan_verify'] = 2;
				DB::table('pan_cards')->where('id',$qu->id)->update($pandata);
				DB::table('register_users')->where('id',$qu->userid)->update($panuserdata);
			}
		}
		$filename =  "all-pending-pan-details.csv";
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $output1;
		exit;
	}
	public function downloadbankcsv(){
		$output1 = "";
		$output1 .='"userid",';
		$output1 .='"accno",';
		$output1 .='"ifsc",';
		$output1 .='"bankname",';
		$output1 .='"bankbranch",';
		$output1 .='"state",';
		$output1 .='"status",';
		$output1 .='"image",';
		$output1 .='"comment",';
		$output1 .="\n";
		$query = DB::table('banks')->get();
		if(!empty($query)){
			foreach($query as $qu){
				$output1 .=$qu->userid.',';
				$output1 .=$qu->accno.',';
				$output1 .=$qu->ifsc.',';
				$output1 .=$qu->bankname.',';
				$output1 .=$qu->bankbranch.',';
				$output1 .=$qu->state.',';
				$output1 .=$qu->status.',';
				$output1 .=$qu->image.',';
				$output1 .=$qu->comment.',';
				$output1 .="\n";
			}
		}
		$filename =  "all-bank-details.csv";
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $output1;
		exit;
	}
	public function downloadpendingbankcsv(){
		$output1 = "";
		$output1 .='"userid",';
		$output1 .='"accno",';
		$output1 .='"ifsc",';
		$output1 .='"bankname",';
		$output1 .='"bankbranch",';
		$output1 .='"state",';
		$output1 .='"status",';
		$output1 .='"image",';
		$output1 .='"comment",';
		$output1 .="\n";
		$query = DB::table('banks')->where('status',0)->where('image','')->get();
		if(!empty($query)){
			foreach($query as $qu){
				$output1 .=$qu->userid.',';
				$output1 .=$qu->accno.',';
				$output1 .=$qu->ifsc.',';
				$output1 .=$qu->bankname.',';
				$output1 .=$qu->bankbranch.',';
				$output1 .=$qu->state.',';
				$output1 .=$qu->status.',';
				$output1 .=$qu->image.',';
				$output1 .=$qu->comment.',';
				$output1 .="\n";
				$bankdata['status'] = 2;
				$bankuserdata['bank_verify'] = 2;
				DB::table('banks')->where('id',$qu->id)->update($bankdata);
				DB::table('register_users')->where('id',$qu->userid)->update($bankuserdata);
			}
		}
		$filename =  "all-pendig-bank-details.csv";
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $output1;
		exit;
	}
	public function downloadwithdrawlcsv(){
		$output1 = "";
		$output1 .='"user_id",';
		$output1 .='"amount",';
		$output1 .='"withdraw_request_id",';
		$output1 .='"comment",';
		$output1 .='"approved_date",';
		$output1 .='"status",';
		$output1 .='"created",';
		$output1 .="\n";
		$query = DB::table('withdraws')->get();
		if(!empty($query)){
			foreach($query as $qu){
				$output1 .=$qu->user_id.',';
				$output1 .=$qu->amount.',';
				$output1 .=$qu->withdraw_request_id.',';
				$output1 .=$qu->comment.',';
				$output1 .=$qu->approved_date.',';
				$output1 .=$qu->status.',';
				$output1 .=$qu->created.',';
				$output1 .="\n";
			}
		}
		$filename =  "all-withdraw-details.csv";
		header('Content-type: application/csv');
		header('Content-Disposition: attachment; filename='.$filename);
		echo $output1;
		exit;
	}
	
}?>