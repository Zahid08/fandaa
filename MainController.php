<?php
namespace App\Http\Controllers;
use App\Registeruser;
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
class MainController extends Controller {
	public function home()
	{
	  $findaintopics = DB::table('categories')->orderBy('name','ASC')->get();
	  $findtags = DB::table('tags')->orderBy('name','ASC')->get();
	  $findsliders = DB::table('sliders')->get();
	  $findallquestions = DB::table('questions')->leftJoin('answers', 'questions.id', '=', 'answers.qid')->join('register_users','register_users.id','=','questions.userid')->select('register_users.fname','register_users.lname','register_users.image','questions.*')->selectRaw('count(answers.qid) as count')->groupBy('questions.id')->orderBy('questions.id','DESC')->limit(5)->get();
	  $findtopexperts = DB::table('register_users')->join('totalratereview', 'register_users.id', '=', 'totalratereview.userid')->select('register_users.unique_id','register_users.fname','register_users.lname','register_users.image','register_users.id')->where('register_users.usertype','consultant')->where('register_users.activation_status','activated')->where('totalratereview.rating','>',3)->limit(5)->get();
	  $findtopexperts = DB::table('topusers')->join('register_users','register_users.id','=','topusers.userid')->select('register_users.*')->get();
	  return view('mains.home',compact('findtopexperts','findaintopics','findtags','findsliders','findallquestions'));
	} 
	public function findsubcategory(Request $request){
		$inputt = Input::all();
		$getslug = $inputt['slug'];
		if($getslug!=""){
			$findcatid = DB::table('categories')->select('id')->where('slug',$getslug)->first();
			$findid = $findcatid->id;
			$findsubcategory = DB::table('subcategories')->where('category',$findid)->orderBy('name','ASC')->get();
			$options='<option value="">Service</option>';
			if(!empty($findsubcategory)){
				foreach($findsubcategory as $subcat){
					$options.='<option value="'.$subcat->slug.'">'.ucwords($subcat->name).'</option>';
				}
			}
			echo $options;die;
		}
	}

	
}?>