<?php 
namespace App\Http\Controllers;
use DB;
use Session;
use bcrypt;
use Config;
use Redirect;
use Helpers;
use Hash;
use Mail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
class OffersController extends Controller {
    
    public function geturl(){
		return 'https://lucky11.co/bbadmin/';
	}
	
	public function accessrules(){
		header('Access-Control-Allow-Origin: *'); 
		header("Access-Control-Allow-Credentials: true");
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Authorization');
		header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
		date_default_timezone_set('Asia/Kolkata'); 
	}
	
    public function getOffersandroid(){
		$this->accessrules();
		$findoffers = DB::table('offers')->where('popular','1')->orderBY('id','Asc')->get();
		$Json=array();
		if(!empty($findoffers)){
			$i=0;
			foreach($findoffers as $post){
				$url = $this->geturl();
				$Json[$i]['id'] = $post->id;
				$Json[$i]['title'] = '';
				$Json[$i]['image'] = $url.'uploads/offers/'.$post->image;
				$i++;
			}
		}
		echo json_encode($Json);
		die;
	}
	
	public function getOffers(){
		$this->accessrules();
		$findoffers = DB::table('offers')->orderBY('id','DESC')->get();
		return view('offers.viewoffers',compact('findoffers'));
	}
	
	public function addOffer(Request $request){
	    if ($request->isMethod('post')){
			$input = Input::all();
            $input['expire_date'] = date('Y-m-d',strtotime($input['expire_date']));  
			
			$photo = $request->file('image');
            $destinationPath = base_path() . '/uploads/offers/';
            $image = $photo->getClientOriginalName();
            $photo->move($destinationPath, $image);
            
            unset($input['_token']);
            $input['image'] = $image;
	        DB::table('offers')->insert($input);
	    }
	    return view('offers.addoffer');
	}
	
	
	public function popular($id){
	    $data = DB::table('offers')->where('id',unserialize(base64_decode($id)))->first();
	    $pop = $data->popular;
	    if($pop == '1')
	        $popular = '0';
	    else
	        $popular = '1';
	    $input['popular'] = $popular;
	    DB::table('offers')->where('id',unserialize(base64_decode($id)))->update($input);
	    return redirect()->action('OffersController@getOffers');
	}
	
	public function editoffer(Request $request,$id){
	    $ofers = DB::table('offers')->where('id',unserialize(base64_decode($id)))->first();
	    if ($request->isMethod('post')){
			$input = Input::all(); 
            $input['expire_date'] = date('Y-m-d',strtotime($input['expire_date']));
			unset($input['_token']);
			if(isset($input['image'])){
    			$photo = $request->file('image');
                $destinationPath = base_path() . '/uploads/offers/';
                $image = $photo->getClientOriginalName();
                $photo->move($destinationPath, $image);
                $input['image'] = $image;
			}
           
	        DB::table('offers')->where('id',unserialize(base64_decode($id)))->update($input);
	    }
	    return view('offers.editoffer',compact('ofers'));
	}
	
	public function deleteoffer($id){
	    DB::table('offers')->where('id',unserialize(base64_decode($id)))->delete();
	    return redirect()->action('OffersController@getOffers');
	}
	
}
?>